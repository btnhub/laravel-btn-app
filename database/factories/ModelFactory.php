<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*List of faker: https://github.com/fzaninotto/Faker
* To test:  $faker = Faker\Factory::create();
*           $faker->name;
*/


$factory->define(App\Assignment::class, function (Faker\Generator $faker) {
    $users = App\User::pluck('id')->all();
    $locations = App\Location::pluck('id')->all();

    $deleted_at = null;
    $declined = $faker->numberBetween(0,1);
    $child_name = null;
    $rate = null;
    $billing_increment = null;

    if ($declined) {
        $deleted_at = $faker->dateTimeThisMonth;
    }

    if ($faker->numberBetween(0,1)) {
        $child_name = $faker->name;
    }

    if ($faker->numberBetween(0,1)) {
        $rate =  $faker->randomNumber(2);
        $billing_increment = $faker->randomElement([30,45,60,75,90]);
    }
   
    return [
        'ref_id' => $faker->numberBetween(10000000, 99999999),
        'start_date' => $faker->dateTimeThisYear,
        'end_date' => $faker->dateTimeThisYear,
        'tutor_id' => $faker->randomElement($users),
        'student_id' => $faker->randomElement($users),
        'child_name' => $child_name,
        'assigned_by' => $faker->randomElement($users),
        'location_id' => $faker->randomElement($locations),
        'declined' => $declined,
        'course' => $faker->sentence(rand(1,3)),
        'rate' => $rate,
        'billing_increment' => $billing_increment,
        'eval_first' => $faker->numberBetween(0,1),
        'eval_end' => $faker->numberBetween(0,1),
        'admin_notes' => $faker->sentence(rand(0,10)),
        'deleted_at' => $deleted_at,
    ];
});

$factory->define(App\TutorSession::class, function (Faker\Generator $faker) {
    $assignments = App\Assignment::whereNotNull('rate')->pluck('id')->all();
    $assignment_id = $faker->randomElement($assignments);

    $assignment = App\Assignment::find($assignment_id);
    $rate = $assignment->rate;

    $duration = $faker->randomFloat(2, 0, 3);
    $amt_charged = number_format($duration * $rate, 2);
    $tutor_pay = number_format($amt_charged * 0.75, 2);
    $btn_pay = number_format($amt_charged * 0.25, 2);
    $discount = number_format($faker->numberBetween(0,1) * $rate * 0.15);

    $student_id = $assignment->student_id;
    $tutor_id = $assignment->tutor_id;

    $outcome = $faker->randomElement(['Met', 'Student Cancelled', 'Tutor Cancelled', 'Free']);
    if ($outcome == 'Tutor Cancelled' || $outcome == 'Free') {
        $amt_charged = $tutor_pay = $btn_pay = $discount = 0;
    }

    return [
        'ref_id' => $faker->numberBetween(10000000, 99999999),
        'assignment_id' => $assignment_id,
        'student_id' => $student_id,
        'tutor_id' => $tutor_id,
        'session_date' => $faker->dateTimeThisMonth,
        'duration' => $duration,
        'outcome' => $outcome,
        'amt_charged' => $amt_charged,
        'tutor_pay' => $tutor_pay,
        'btn_pay' => $btn_pay,
        'discount' => $discount,
        'tutor_payment_date' => $faker->randomElement([null, $faker->dateTimeThisMonth]),
        'admin_notes' =>$faker->randomElement([null, $faker->realText($faker->numberBetween(10,20))]),
        'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth]),
    ];
});


$factory->define(App\MarketTutorRequest::class, function (Faker\Generator $faker) {
    $student_ids = App\User::pluck('id')->all();
    $tutor_ids = App\User::pluck('id')->all();
    $location_ids = App\Location::pluck('id')->all();
    
    $location = App\Location::find($faker->randomElement($location_ids));

    $matched_tutor_id = null;
    $matched_date = null;
    $matched_by_id = null;

    if($faker->numberBetween(0,1))
    {
        $matched_tutor_id = $faker->randomElement($tutor_ids);
        $matched_date = $faker->dateTimeThisMonth;
        $matched_by_id = $faker->randomElement($tutor_ids);
    }

    return [
        'ref_id' => $faker->numberBetween(10000000, 99999999),
        'mkt_visibility' => rand(0,1),
        'student_id' => $faker->randomElement($student_ids),
        'requested_tutor_id' => $faker->randomElement($tutor_ids),
        'requested_tutor_declined' => $faker->numberBetween(0,1),
        'location_id' => $location->id,
        'location' => $location->city,
        'child_name' => $faker->randomElement([null, $faker->name]),
        'start_date' => $faker->dateTimeThisYear,
        'end_date' => $faker->dateTimeThisMonth,
        'urgency' => $faker->randomElement(['Normal', 'High']),
        'level' => $faker->randomElement(['College', 'High School', 'Middle School', 'Elementary School', 'Graduate School', 'Other']),
        'subjects' => $faker->randomElement(['Math', 'Writing', 'Physics', 'Chemistry', 'Biology', 'Integrative Physiology', 'Spanish', 'General']),
        'course' => $faker->randomElement(['Math 1300', "Physics 1110", "Mentoring", "Anatomy", "Writing", "Genetics", "Gen Chem 1", "P-Chem", "HS Algebra", "Middle School LA", "Elementary School Reading"]),
        'session_time' => $faker->randomElement(['Daytime', "Evenings (5-7pm)", "Nights(after 7pm)", "Saturdays", "Sundays"]),
        'max_rate' => $faker->randomElement([20,25,30,40,50,60,80,100,120,150]),
        'frequency' => $faker->randomElement(['Once a week', "Twice a week", "MWF", "Only before midterms", "Only for an upcoming project", "A few sessions before finals"]),
        'scholarship_program' => $faker->randomElement([null, $faker->realText($faker->numberBetween(10,50))]),
        'concerns' => $faker->randomElement([null, $faker->realText($faker->numberBetween(30,100))]),
        'matched_tutor_id' => $matched_tutor_id,
        'matched_date' => $matched_date,
        'matched_by_id' => $matched_by_id,
    ];
});

$factory->define(App\TutorApplicant::class, function (Faker\Generator $faker) {
    
    $company_locations = App\Location::pluck('city')->all();

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'major' => $faker->sentence(rand(1,4)),
        'year' => $faker->randomElement(['Graduate Student', 'Senior', 'Junior', 'Graduated (Masters)', 'Graduated (PhD)', 'Graduated (Bachelors)']),
        'availability' => $faker->sentence(rand(1,3)),
        'company_locations' => $faker->randomElement($company_locations),
        'session_locations' => $faker->sentence(rand(5,10)),
        'courses' => $faker->sentence(rand(5,10)),
        'experience' => $faker->realText($faker->numberBetween(300,500)),
        'concerns' => $faker->realText($faker->numberBetween(30,50)),
        'start_date' => $faker->dateTimeThisYear,
        'end_date' => $faker->dateTimeThisMonth,
        'marketing' => $faker->sentence(rand(0,3)),
        'hired' => $faker->numberBetween(0,1),
        'admin_notes' => $faker->realText($faker->numberBetween(10,50)),
        'access_key' => $faker->numberBetween(10000000, 99999999),
        'access_end_date' => $faker->dateTimeThisMonth,
    ];
});


$factory->define(App\Refund::class, function (Faker\Generator $faker) {
    $session_ids = App\TutorSession::pluck('id')->all();

    $session_id = $faker->randomElement($session_ids);
    $session = App\TutorSession::find($session_id);
    
    $assignment_id = $session->assignment->id;
    $student_id = $session->student->id;
    $tutor_id = $session->tutor->id;
    $requested_amount = $session->amt_charged;
    $deductions = $faker->randomElement([null, $requested_amount*0.25]);

    return [
        'ref_id' => $faker->numberBetween(10000000, 99999999),
        'student_id' => $student_id,
        'tutor_id' => $tutor_id,
        'session_id' => $session_id,
        'assignment_id' => $assignment_id,
        'requested_amount' => $requested_amount,
        'refund_reason' => $faker->randomElement(['First Session Refund', 'Terminate Sessions', 'Balance Refund']),
        'reason_details' => $faker->randomElement([null, $faker->realText($faker->numberBetween(30,100))]),
        'refund_method' => $faker->randomElement(['Stripe', 'PayPal', 'Check', 'Google Wallet', 'Square Cash']),
        'deductions' => $deductions,
        'issue_refund' => $faker->numberBetween(0,1),
    ];
});

$factory->define(App\Evaluation::class, function (Faker\Generator $faker) {
    $assignment_ids = App\Assignment::pluck('id')->all();

    $assignment_id = $faker->randomElement($assignment_ids);
    $assignment = App\Assignment::find($assignment_id);   
    $student_id = $assignment->student->id;
    $tutor_id = $assignment->tutor->id;
 
    return [
        'assignment_id' => $assignment_id,
        'student_id' => $student_id,
        'tutor_id' => $tutor_id,
        'type' => $faker->randomElement(['First', 'End']),
        'satisfied' => $faker->numberBetween(0,1),
        'frequency' => $faker->randomElement(['Only Once', 'Once A Week', 'Multiple Times A Week', 'Only Exam Reviews']),
        'tutor_strength' => $faker->realText($faker->numberBetween(30,100)),
        'tutor_weakness' => $faker->realText($faker->numberBetween(30,100)),
        'website_reference' => $faker->randomElement([null, $faker->realText($faker->numberBetween(100,200))]),
        'school' => $faker->randomElement([null, "CU-Boulder", "CU-Denver", "Denver University", "Metro State", "CSU", "CU-Colorado Springs"]),
        'review_date' => $faker->randomElement([null, $faker->dateTimeThisMonth]),
        'homepage_visible' => $faker->numberBetween(0,1),
        'created_at' => $faker->dateTimeThisYear,
    ];
});

$factory->define(App\ContractorPayment::class, function (Faker\Generator $faker) {
    $amount_withheld = null;
    $withhold_reason = null;

    $users = App\TutorSession::pluck('tutor_id')->all();
    
    $contractor_id = $faker->randomElement($users);
    $first_name = App\User::find($contractor_id)->first_name;
    $last_name = App\User::find($contractor_id)->last_name;

    if ($faker->numberBetween(0,1)) {
         $amount_withheld = $faker->randomNumber(2);
         $withhold_reason = "Unpaid Sessions";
     } 
    return [
        'contractor_id' => $contractor_id,
        'first_name' => $first_name,
        'last_name' => $last_name,
        'amount_paid' => $faker->randomNumber($faker->numberBetween(2,3)),
        'method' => $faker->randomElement(['PayPal', 'Google Wallet', 'Stripe', 'Directly By Student']),
        'amount_withheld' => $amount_withheld,
        'withhold_reason' => $withhold_reason,
        'created_at' => $faker->dateTimeThisYear,
        'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth]),
    ];
});
    