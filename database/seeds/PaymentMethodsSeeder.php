<?php

use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Empty table
        DB::table('payment_methods')->truncate();

        DB::table('payment_methods')->insert(
    		['method' => 'Stripe',
    		'title' => 'Credit / Debit Card (Stripe)']);

        DB::table('payment_methods')->insert(
    		['method' => 'PayPal',
    		'title' => 'PayPal',
            'email_required' => '1']);

        DB::table('payment_methods')->insert(
    		['method' => 'GWallet',
    		'title' => 'Google Wallet',
            'email_required' => '1']);

        DB::table('payment_methods')->insert(
    		['method' => 'Square',
    		'title' => 'Square Cash', 
            'email_required' => '1']);
        
        DB::table('payment_methods')->insert(
    		['method' => 'Check',
    		'title' => 'Check']);
        
        DB::table('payment_methods')->insert(
    		['method' => 'Cash',
    		'title' => 'Cash',
            'refundable' => 0]);
    }
}
