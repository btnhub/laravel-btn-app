<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->truncate();

        DB::table('permissions')->insert(
    		['permission' => 'view-admin', 
    		'label' => 'View Backend/Admin'
    		]);

		DB::table('permissions')->insert(
    		['permission' => 'view-student', 
    		'label' => 'View Student Tools'
    		]);

		DB::table('permissions')->insert(
    		['permission' => 'view-tutor', 
    		'label' => 'View Tutor Tools'
    		]);
    }
}
