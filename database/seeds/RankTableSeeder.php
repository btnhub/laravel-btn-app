<?php

use Illuminate\Database\Seeder;

class RankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Empty table
        DB::table('rank')->truncate();

        DB::table('rank')->insert([
        	'title' => 'positive_end_eval',
        	'weight' => 50
        	]);

        DB::table('rank')->insert([
        	'title' =>'positive_first_eval',
        	'weight' => 20
        	]);

        DB::table('rank')->insert([
        	'title' => 'hours',
        	'weight' => 30
        	]);

        DB::table('rank')->insert([
        	'title' => 'negative_end_eval',
        	'weight' => -100
        	]);

        DB::table('rank')->insert([
        	'title' => 'negative_first_eval',
        	'weight' => -40
        	]);

        DB::table('rank')->insert([
        	'title' => 'session_refunds',
        	'weight' => -60
        	]);
    }
}
