<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Assignment;

class AssignmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //Get existing users' id

        Model::unguard();  	
		Assignment::truncate();  
		factory(Assignment::class, 150)->create();
		Model::reguard();
    }
}
