<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\MarketTutorRequest;

class TutorRequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();  	
		MarketTutorRequest::truncate();  
		factory(MarketTutorRequest::class, 150)->create();
		Model::reguard();
    }
}
