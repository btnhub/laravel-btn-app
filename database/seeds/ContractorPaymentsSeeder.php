<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\ContractorPayment;

class ContractorPaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();  	
		ContractorPayment::truncate();  
		factory(ContractorPayment::class, 175)->create();
		Model::reguard();
    }
}
