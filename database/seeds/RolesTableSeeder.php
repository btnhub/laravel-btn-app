<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();

        DB::table('roles')->insert(
    		['role' => 'super-user', 
    		'label' => 'Super User'
    		]);

       	DB::table('roles')->insert(
    		['role' => 'admin', 
    		'label' => 'Administrator'
    		]);

       	DB::table('roles')->insert(
    		['role' => 'registered', 
    		'label' => 'Registered'
    		]);

       	DB::table('roles')->insert(
    		['role' => 'student', 
    		'label' => 'Student'
    		]);

        DB::table('roles')->insert(
            ['role' => 'music-student', 
            'label' => 'Music Student'
            ]);

       	DB::table('roles')->insert(
    		['role' => 'tutor', 
    		'label' => 'Tutor'
    		]);
        
        DB::table('roles')->insert(
            ['role' => 'music-teacher', 
            'label' => 'Music Teacher'
            ]);

        DB::table('roles')->insert(
            ['role' => 'marketplace-tutor', 
            'label' => 'MarketPlace Tutor'
            ]);

        DB::table('roles')->insert(
            ['role' => 'tutor-applicant', 
            'label' => 'Tutor Applicant'
            ]);

        DB::table('roles')->insert(
            ['role' => 'test-tutor', 
            'label' => 'Test Tutor'
            ]);

        DB::table('roles')->insert(
            ['role' => 'test-student', 
            'label' => 'Test Student'
            ]);

       	DB::table('roles')->insert(
    		['role' => 'banned', 
    		'label' => 'Banned'
    		]);
    }
}
