<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocationsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserAndProfileSeeder::class);
        $this->call(AssignmentsTableSeeder::class);
        $this->call(SessionsTableSeeder::class);
        $this->call(TutorRequestsSeeder::class);
        $this->call(TutorApplicantsSeeder::class);
        $this->call(RefundsTableSeeder::class);
        $this->call(EvaluationsTableSeeder::class);
        $this->call(PaymentMethodsSeeder::class);
        $this->call(ContractorPaymentsSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(MarketSubjectsSeeder::class);
        $this->call(RankTableSeeder::class);
        $this->call(StripeConnectSeeder::class);
        $this->call(MessagesSeeder::class);
    }
}
