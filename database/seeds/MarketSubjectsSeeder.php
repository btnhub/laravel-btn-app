<?php

use Illuminate\Database\Seeder;

class MarketSubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('market_subjects')->truncate();

        DB::table('market_subjects')->insert(
    		['department' => 'Biology', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'Chemistry', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);
        
        DB::table('market_subjects')->insert(
    		['department' => 'Integrative Physiology', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'Physics', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'Math', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'Music: Coursework', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

		DB::table('market_subjects')->insert(
    		['department' => 'Music: Private Instruction', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);        

        DB::table('market_subjects')->insert(
    		['department' => 'Reading', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'Writing', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'Foreign Language', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'General: Mentoring', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'General: Conversational English', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);

        DB::table('market_subjects')->insert(
    		['department' => 'General: Conversational Foreign Language', 
    		'submitted_by' => 62,
    		'approved' => 1
    		]);
    }
}
