<?php

use Illuminate\Database\Seeder;

use Illuminate\Database\Eloquent\Model;
use App\TutorApplicant;

class TutorApplicantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();  	
		TutorApplicant::truncate();  
		factory(TutorApplicant::class, 150)->create();
		Model::reguard();
    }
}
