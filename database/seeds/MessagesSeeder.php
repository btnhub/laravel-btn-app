<?php

use Illuminate\Database\Seeder;

class MessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('msg_threads')->truncate();
        DB::table('msg_participants')->truncate();
        DB::table('msg_messages')->truncate();
        
        $faker = Faker\Factory::create();

        $user_ids = App\User::pluck('id')->toArray();

        //$i = thread id
        for ($i=1; $i < 50; $i++) {
        	$tutor_request = App\MarketTutorRequest::find($i);
        	$student = $tutor_request->student;
        	$tutor = $tutor_request->requested_tutor;

        	$course = $faker->randomElement(['Math', 'Physics 1', 'Trig', 'Piano Lessons', 'Writing', 'General Mentoring', 'P-Chem', 'Language Arts']);

        	$created_at = $faker->dateTimeThisYear;
        	$last_read = $faker->randomElement([null, $faker->dateTimeThisMonth]);
        	//Update tutor requests table with thread id
        	$tutor_request->msg_thread_id = $i;
        	$tutor_request->save();
        	
        	switch ($faker->randomElement(['student_request', 'tutor_proposal'])) {
        		case 'student_request':
        			$subject = "Tutor Requested: $course";

        			DB::table('msg_threads')->insert(
		            [
		            	'subject' => $subject,
		            	'created_at' => $created_at,
		            	'updated_at' => $created_at,
		            	'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth])
		            ]);
    				
    				//Add Student first (the one who initiated the conversation)
		    		DB::table('msg_participants')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $student->id,
				            	'last_read' => $created_at,
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,
				       			'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth])     	
				            ]);

		    		DB::table('msg_participants')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $tutor->id,
				            	'last_read' => $last_read,
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,
				       			'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth])     	
				            ]);

		    		DB::table('msg_messages')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $student->id,
				            	'body' => "{$student->first_name} has expressed interest in working with {$tutor->first_name} in $course.",
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,   	
				            ]);
		    		if ($last_read)
		    		{
		    			DB::table('msg_messages')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $tutor->id,
				            	'body' => $faker->realText($faker->numberBetween(50, 100)),
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,   	
				            ]);
		    		}
        			break;
        		
        		case 'tutor_proposal':
        			$subject = "{$tutor->first_name}'s Proposal For {$course}";

        			DB::table('msg_threads')->insert(
		            [
		            	'subject' => $subject,
		            	'created_at' => $created_at,
		            	'updated_at' => $created_at,
		            	'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth])
		            ]);
    				
    				//Add Tutor first (the one who initiated the conversation)
		    		DB::table('msg_participants')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $tutor->id,
				            	'last_read' => $created_at,
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,
				       			'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth])     	
				            ]);

		    		DB::table('msg_participants')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $student->id,
				            	'last_read' => $last_read,
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,
				       			'deleted_at' => $faker->randomElement([null, $faker->dateTimeThisMonth])     	
				            ]);

		    		DB::table('msg_messages')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $student->id,
				            	'body' => "{$student->first_name} has expressed interest in working with {$tutor->first_name} in $course.",
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,   	
				            ]);
		    		if ($last_read)
		    		{
		    			DB::table('msg_messages')->insert(
				            [
				            	'thread_id' => $i,
				            	'user_id' => $student->id,
				            	'body' => $faker->realText($faker->numberBetween(50, 100)),
				            	'created_at' => $created_at,
				            	'updated_at' => $created_at,   	
				            ]);
		    		}
        			break;
        	}
        	
    		
    		
        }
    }
}
