<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\TutorSession;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();  	
		TutorSession::truncate();  
		factory(TutorSession::class, 500)->create();
		Model::reguard();
    }
}
