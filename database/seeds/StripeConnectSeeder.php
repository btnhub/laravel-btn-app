<?php

use Illuminate\Database\Seeder;

class StripeConnectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stripe_connect_tutors')->truncate();
        DB::table('stripe_customers')->truncate();
        
        $faker = Faker\Factory::create();

        $user_ids = App\User::pluck('id')->toArray();

        for ($i=1; $i < 200; $i++) {
	        $user = App\User::find($faker->randomElement($user_ids));

	        // Stripe Connect Data
	        $stripe_user_id = 'acct_18bw6pJgHpe5erka';
	        $token_type = 'bearer';
	        $stripe_publishable_key = 'pk_test_Hw106t0kxhzoSOhC7Lwd9OcZ';
	        $scope =  'read_write';
	        $livemode = 0;
	        $refresh_token = 'rt_8tjdPUiDHZ5TPvCfxcjgMraIAk17HSwXdTFZDdFIY90eJ7mS';
	        $access_token = 'sk_test_4ne0aOWsMVzhjDw0ZQHkNTdd';

	        //Timestamps
	        $created_at = $faker->dateTimeThisYear;
    		$updated_at = $faker->dateTimeThisMonth;
    		$deleted_at = $faker->randomElement([null, $faker->dateTimeThisMonth]);

	        DB::table('stripe_connect_tutors')->insert(
		            [
		            	'user_id' => $user->id,
				        'first_name' => $user->first_name,
				        'last_name' => $user->last_name,
				        'stripe_user_id' => $stripe_user_id,
				        'token_type' => $token_type,
				        'stripe_publishable_key' => $stripe_publishable_key,
				        'scope' => $scope,
				        'livemode' => $livemode,
				        'refresh_token' => $refresh_token,
				        'access_token' => $access_token,
				        'created_at' => $created_at,
				        'updated_at' => $updated_at,
				        'deleted_at' => $deleted_at,
		            ]);
	        
	        //Customers Table
	        $user = App\User::find($faker->randomElement($user_ids));
	        $stripe_customer_id = $faker->randomElement(['cus_8oGePTGYCuf7dB', 'cus_8iHaSSVQLYSfz4', 'cus_8tsstc7TdmxB6i', 'cus_8tssjMB6QVYTmi']);

	        switch ($stripe_customer_id) {
	        	case 'cus_8oGePTGYCuf7dB':
	        		$card_brand = 'visa';
	        		$card_last_four = 4242;
	        		$exp_month = 3;
	        		$exp_year = 2017;
	        		break;
	        	
	        	case 'cus_8iHaSSVQLYSfz4':
	        		$card_brand = 'MasterCard';
	        		$card_last_four = 8210;
	        		$exp_month = 3;
	        		$exp_year = 2018;
	        		break;

	        	case 'cus_8tsstc7TdmxB6i':
	        		$card_brand = 'MasterCard';
	        		$card_last_four = 4444;
	        		$exp_month = 7;
	        		$exp_year = 2016;
	        		break;

	        	case 'cus_8tssjMB6QVYTmi':
	        		$card_brand = 'Discover';
	        		$card_last_four = 1117;
	        		$exp_month = 7;
	        		$exp_year = 2016;
	        		break;
	        }
	        DB::table('stripe_customers')->insert(
		            [
		            	'user_id' => $user->id,
				        'stripe_customer_id' => $stripe_customer_id,
				        'card_brand' => $card_brand,
				        'card_last_four' => $card_last_four,
				        'exp_month' => $exp_month,
				        'exp_year' => $exp_year,
				        'created_at' => $created_at,
				        'updated_at' => $updated_at,
				        'deleted_at' => $deleted_at,
		            ]);
    	}
    }
}
