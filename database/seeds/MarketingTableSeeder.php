<?php

use Illuminate\Database\Seeder;

class MarketingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marketing')->truncate();

        $faker = Faker\Factory::create();

        $user_ids = App\User::pluck('id')->toArray();
        $user_roles = ['student', 'student', 'student', 'student','student', 'tutor-applicant'];
        $methods = ['chalkboard', 'craigslist', 'facebook', 'flyers', 'friend', 'handshake', 'internet', 'linkedin', 'professor', 'school-advisor', 'school-list', 'tutor-list'];
        
        for ($i=1; $i < 500; $i++) {
        
        	DB::table('marketing')->insert(
    		['user_id' => $faker->randomElement($user_ids), 
    		'role' => $faker->randomElement($user_roles),
    		'method' => $faker->randomElement($methods),
    		'created_at' => $faker->dateTimeBetween('-3 years', 'now'),
    		'updated_at' => $faker->dateTimeThisYear,
    		]);
        }
    }
}
