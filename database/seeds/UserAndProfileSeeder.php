<?php

use Illuminate\Database\Seeder;

//use Carbon;

class UserAndProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('profiles')->truncate();
        DB::table('location_user')->truncate();
        DB::table('role_user')->truncate();
    	
    	$faker = Faker\Factory::create();
    	$avatars = [null, 'default.jpg', 'sp_avatar01.png', 'sp_avatar02.jpg', 'sp_avatar03.png', 'sp_avatar04.gif', 'sp_avatar05.gif', 'sp_avatar06.jpg', 'sp_avatar07.jpg', 'sp_avatar08.jpg'];

    	$locations = App\Location::pluck('id')->all();
    	$roles = App\Role::pluck('id')->all();
    

        for ($i=1; $i < 500; $i++) { 
           	$banned = 0;
           	$created_at = $faker->dateTimeThisYear;
    		$updated_at = $faker->dateTimeThisMonth;
    		$deleted_at = $faker->randomElement([null, null, $faker->dateTimeThisMonth]);
    		
           	if($faker->numberBetween(0,1))
	        	{
		            $banned = 1;
	        	}

	        DB::table('users')->insert(
	            [
	            	'ref_id' => $faker->numberBetween(10000000, 99999999),
			        'first_name' => $faker->firstName,
			        'last_name' => $faker->lastName,
			        'email' => $faker->safeEmail,
			        'phone' => $faker->phoneNumber,
			        'password' => bcrypt('secret'),
			        'remember_token' => str_random(10),
			        'banned' => $banned,
			        'notify' => $faker->numberBetween(0, 1),
			        'last_login_date' => $faker->dateTimeThisYear,
			        'created_at' => $created_at,
			        'updated_at' => $updated_at,
			        'deleted_at' => $deleted_at
	            ]);

	        $instruments = null;
	        $music_styles = null;
	        $rate_min = rand(20,80);
	        $rate_max = $rate_min + rand(0,20);

	        if($faker->numberBetween(0,1))
	        {
	            $instruments = $faker->randomElement(['Violin', 'Piano', 'Viola', 'Guitar', 'Flute']);
	            $music_styles = $faker->realText($faker->numberBetween(10,20));
	        }

		    DB::table('profiles')->insert(
	            [
	                'user_id' => $i,
			        'avatar' => $faker->randomElement($avatars),
			        'tutor_fee' => $faker->randomElement([null, 0.2, 0.25, 0.3, 0.35]),
			        'background_check' => $faker->randomElement([null, $faker->dateTimeThisYear]),
			        'show_profile_admin' => $faker->numberBetween(0,1),
			        'show_profile_tutor' => $faker->numberBetween(0,1),
			        'messaging_privileges' => $faker->numberBetween(0,1),
			        'address' => $faker->address,
			        'major' => $faker->randomElement(['Physics', 'Math', 'Engineering', 'Writing', 'Psychology', 'Sociology']),
			        'year' => $faker->randomElement(['Senior', 'Junior', '2nd Grade', '3rd Year', '10th Grade']),
			        'school' => $faker->randomElement([null, "CU-Boulder", "CU-Denver", "Denver University", "Metro State", "CSU", "CU-Colorado Springs"]),
			        'languages' => $faker->randomElement(['English', 'Spanish', 'French', 'German', 'Russian', 'Arabic']),
			        'tutor_experience' => $faker->realText($faker->numberBetween(1000,2000)),
			        'tutor_education' => $faker->randomElement(['phd', 'masters', 'bachelors', 'grad_student', 'undergrad']),
			        'tutor_status' => rand(0,1),
			        'tutor_availability' => $faker->randomElement(['Daytime', 'Evenings', 'Nights', 'Saturdays', 'Sundays']),
			        'session_locations' => $faker->randomElement(['Student\'s School/Campus', 'Student\'s Home', 'Near Campus', 'Denver', 'Louisville', 'Fort Collins']),
			        'courses' => $faker->realText($faker->numberBetween(500,1000)),
			        'instruments' => $instruments,
			        'music_styles' => $music_styles,
			        'calendar_id' => $faker->randomElement([null, 'rprecar25t23juok0ncnm14fk4@group.calendar.google.com', 'colorado.edu_ojip7bcogop3j0fc06j07t257k@group.calendar.google.com', 'leeyishi@gmail.com', 'bufftutor@gmail.com']),
			        'hours_available' => rand(5,20),
			        'rate_min' => $rate_min,
			        'rate_max' => $rate_max,
			        'rate_details' => $faker->sentence(rand(1,3)),
			        'rate_visibility' => rand(0,1),
			        'tutor_start_date' => $faker->dateTimeThisYear,
			        'tutor_end_date' => $faker->randomElement([null, Carbon\Carbon::now()->addDays($faker->numberBetween(5,180)), Carbon\Carbon::now()->subDays($faker->numberBetween(5,180))]),
			        'created_at' => $created_at,
			        'updated_at' => $updated_at,
			        'deleted_at' => $deleted_at
	        	]);

		    DB::table('location_user')->insert(
	            [
	            	'user_id' => $i,
			        'location_id' => $faker->randomElement($locations),
	            ]);

		    if($faker->numberBetween(0,1))
	        {
	           DB::table('location_user')->insert(
	            [
	            	'user_id' => $i,
			        'location_id' => $faker->randomElement($locations),
	            ]); 
	        }

		    DB::table('role_user')->insert(
	            [
	            	'user_id' => $i,
			        'role_id' => $faker->randomElement($roles)
	            ]);

	        if($faker->numberBetween(0,1))
	        {
		    	DB::table('role_user')->insert(
		            [
		            	'user_id' => $i,
				        'role_id' => $faker->randomElement($roles)
		            ]);
	        }
	    }
    }
}
