<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Empty table
        DB::table('locations')->truncate();

        DB::table('locations')->insert(
    		['city' => 'Boulder', 
    		'company_name' => 'BuffTutor Network',
    		'email' => 'Boulder@BuffTutor.com', 
    		'phone' => '123-456-7890', 
    		'website' => 'www.BuffTutor.com', 
            'timezone' => 'America/Denver'
    		]);
        DB::table('locations')->insert(
    		['city' => 'Denver', 
    		'company_name' => 'BuffTutor Network',
    		'email' => 'Denver@BuffTutor.com', 
    		'phone' => '456-789-0123', 
    		'website' => 'www.BuffTutor.com', 
            'timezone' => 'America/Denver'
    		]);
        DB::table('locations')->insert(
    		['city' => 'Fort Collins', 
    		'company_name' => 'RamTutor Network',
    		'email' => 'FtCollins@RamTutor.com', 
    		'phone' => '789-012-3456', 
    		'website' => 'www.RamTutor.com',
            'timezone' => 'America/Denver' 
    		]);
        DB::table('locations')->insert(
    		['city' => 'Colorado Springs', 
    		'company_name' => 'BuffTutor Network',
    		'email' => 'ColoSprings@BuffTutor.com', 
    		'phone' => '739-294-1849', 
    		'website' => 'www.BuffTutor.com',
            'timezone' => 'America/Denver' 
    		]);
        DB::table('locations')->insert(
            ['city' => 'Seattle', 
            'company_name' => 'BuffTutor Network',
            'email' => 'Seattle@BuffTutor.com', 
            'phone' => '739-294-1849', 
            'website' => 'www.BuffTutor.com',
            'timezone' => 'America/Los_Angeles' 
            ]);
    }
}