<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGeolocationsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('formatted_address')
                    ->nullable()
                    ->after('last_login_date');
            $table->string('city', 100)->nullable()
                    ->after('formatted_address');
            $table->string('region', 100)->nullable()
                    ->after('city');
            $table->string('country', 100)->nullable()
                    ->after('region');
            $table->string('postal_code', 50)->nullable()
                    ->after('country');
            $table->decimal('lat', 10, 8)->nullable()
                    ->after('postal_code');
            $table->decimal('lng', 11, 8)->nullable()
                    ->after('lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('formatted_address');
            $table->dropColumn('city');
            $table->dropColumn('region');
            $table->dropColumn('country');
            $table->dropColumn('postal_code');
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}
