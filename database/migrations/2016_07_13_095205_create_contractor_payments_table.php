<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('contractor_id')->unsigned()
                    ->foreign('contractor_id')
                    ->references('id')
                    -> on('users')->nullable()
                    ->onDelete('cascade');
            $table->string('first_name');
            $table->string('last_name');
            $table->decimal('amount_paid', 10, 2)->unsigned();
            $table->string('method');
            $table->decimal('amount_withheld', 10, 2)->unsigned()->nullable();
            $table->string('withhold_reason')->nullable();
            $table->text('admin_notes')->nullable;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contractor_payments');
    }
}
