<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()
                ->unique()
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->decimal('tutor_fee',5,2)->nullable();
            $table->string('avatar')->nullable();
            $table->datetime('background_check')->nullable();
            $table->datetime('tutor_contract')->nullable();
            $table->boolean('show_profile_admin')->default(1);
            $table->boolean('show_profile_tutor')->default(1);
            $table->boolean('messaging_privileges')->default(1);
            $table->text('address')->nullable();
            $table->text('about')->nullable();
            $table->string('major')->nullable();
            $table->string('year')->nullable();
            $table->string('school')->nullable();
            $table->string('languages')->nullable();
            $table->text('tutor_experience')->nullable();
            $table->string('tutor_education')->nullable();
            $table->boolean('tutor_status')->nullable();
            $table->string('tutor_availability')->nullable();
            $table->text('session_locations')->nullable();
            $table->text('courses')->nullable();
            $table->string('instruments')->nullable();
            $table->string('music_styles')->nullable();
            $table->string('calendar_id')->nullable();
            $table->string('hours_available')->nullable();
            $table->tinyInteger('rate_min')->unsigned()->nullable();
            $table->tinyInteger('rate_max')->unsigned()->nullable();
            $table->text('rate_details')->nullable();
            $table->boolean('rate_visibility')->nullable()->default(1);
            $table->date('tutor_start_date')->nullable();
            $table->date('tutor_end_date')->nullable();
            $table->string('paypal_email')->nullable();
            $table->string('ip')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
