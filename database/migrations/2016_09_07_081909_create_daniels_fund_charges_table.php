<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDanielsFundChargesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daniels_fund_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('df_assignment_id')->unsigned()
                ->foreign('df_assignment_id')
                ->references('id')
                ->on('daniels_fund_scholars')
                ->onDelete('cascade');
            $table->integer('assignment_id')->unsigned()
                ->foreign('assignment_id')
                ->references('id')
                ->on('assignments')
                ->onDelete('cascade');
            $table->integer('session_id')->unsigned()
                ->foreign('session_id')
                ->references('id')
                ->on('tutor_sessions')
                ->onDelete('cascade');
            $table->integer('student_id')->unsigned()
                -> foreign('student_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('tutor_id')->unsigned()
                -> foreign('tutor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->decimal('amt_charged', 10,2)->unsigned()->nullable();
            $table->decimal('tutor_pay', 10,2)->unsigned()->nullable();
            $table->decimal('btn_pay', 10,2)->unsigned()->nullable();
            $table->date('payment_date')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daniels_fund_charges');
    }
}
