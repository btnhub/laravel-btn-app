<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsSentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_notifications_sent', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_request_id')->unsigned()
                    ->foreign('tutor_request_id')
                    ->references('id')
                    ->on('market_tutor_requests')
                    ->onDelete('cascade');
            $table->integer('tutor_id')->unsigned()
                    ->foreign('tutor_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('location_id')->unsigned()
                    ->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade');
            $table->timeStamps();
            $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('market_notifications_sent');
    }
}
