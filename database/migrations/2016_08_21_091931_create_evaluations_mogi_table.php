<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsMogiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations_mogi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assignment_id')->nullable()
                    ->foreign('assignment_id')
                    ->references('id')
                    ->on('assignments')
                    ->onDelete('cascade');
            $table->integer('student_id')->nullable()
                    ->foreign('student_id')
                    ->references('id')
                    ->on('users');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('type')->nullable();
            $table->boolean('satisfied')->nullable();
            $table->string('course')->nullable();
            $table->string('semester')->nullable();
            $table->string('frequency')->nullable();
            $table->text('tutor_strength')->nullable();
            $table->text('tutor_weakness')->nullable();
            $table->text('website_reference')->nullable();
            $table->string('school')->nullable();
            $table->date('review_date')->nullable();
            $table->boolean('homepage_visible')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluations_mogi');
    }
}
