<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_id')->unsigned()
                    ->foreign('tutor_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('location_id')->unsigned()
                    ->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade');
            $table->integer('subject_id')->unsigned()
                    ->foreign('subject_id')
                    ->references('id')
                    ->on('market_subjects')
                    ->onDelete('cascade');
            $table->integer('min_rate')
                    ->unsigned();
            $table->boolean('notify');
            $table->timeStamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('market_notifications');
    }
}
