<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToTutorApplicants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_applicants', function (Blueprint $table) {
            $table->boolean('online')
                    ->after('year')
                    ->nullable();
            $table->text('best_practices')
                    ->after('experience')
                    ->nullable();
            $table->text('worst_practices')
                    ->after('best_practices')
                    ->nullable();
            $table->text('professional_link')
                    ->after('marketing')
                    ->nullable();
            $table->datetime('submitted')
                    ->after('professional_link')
                    ->nullable();
            $table->boolean('offered')
                    ->after('submitted')
                    ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_applicants', function (Blueprint $table) {
            $table->dropColumn('online');
            $table->dropColumn('best_practices');
            $table->dropColumn('worst_practices');
            $table->dropColumn('professional_link');
            $table->dropColumn('submitted');
            $table->dropColumn('offered');
        });
    }
}
