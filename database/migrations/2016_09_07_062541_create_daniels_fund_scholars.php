<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDanielsFundScholars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daniels_fund_scholars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->unsigned()
                -> foreign('student_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('tutor_id')->unsigned()
                -> foreign('tutor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('assignment_id')->unsigned()
                ->foreign('assignment_id')
                ->references('id')
                ->on('assignments')
                ->onDelete('cascade');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('course');
            $table->decimal('rate', 10,2)
                    ->unsigned()
                    ->nullable();
            $table->decimal('hours', 10,2)
                    ->unsigned()
                    ->nullable();
            $table->decimal('total', 10,2)
                    ->unsigned()
                    ->nullable();
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('daniels_fund_scholars');
    }
}
