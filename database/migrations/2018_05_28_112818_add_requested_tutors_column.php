<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRequestedTutorsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->string('requested_tutors')
                    ->nullable()
                    ->after('frequency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->dropColumn('requested_tutors');
        });
    }
}
