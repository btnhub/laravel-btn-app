<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_applicants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable()
                    ->foreign('user_id')
                    ->references('id')
                    ->on('users');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->string('major');
            $table->string('year');
            $table->string('availability')->comments('hours');
            $table->string('company_locations');
            $table->text('session_locations');
            $table->text('courses');
            $table->text('experience');
            $table->text('concerns')->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('marketing');
            $table->boolean('hired')->default(0);
            $table->integer('hired_by_id')->nullable();
            $table->text('admin_notes')->nullable();
            $table->string('access_key')->unique();
            $table->date('access_end_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_applicants');
    }
}
