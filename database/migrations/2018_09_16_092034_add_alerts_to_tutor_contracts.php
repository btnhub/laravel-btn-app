<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAlertsToTutorContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_contracts', function (Blueprint $table) {
            $table->boolean('email_alert')->nullable()
                    ->default(0)
                    ->after('online');
            $table->boolean('sms_alert')->nullable()
                    ->default(0)
                    ->after('email_alert');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_contracts', function (Blueprint $table) {
            $table->dropColumn('email_alert');
            $table->dropColumn('sms_alert');
        });
    }
}
