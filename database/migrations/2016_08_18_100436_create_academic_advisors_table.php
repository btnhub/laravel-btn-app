<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcademicAdvisorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_advisors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id')->unsigned()
                    ->nullable()
                    ->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('school')->nullable();
            $table->string('department')->nullable();
            $table->string('position')->nullable();
            $table->string('source')->nullable();
            $table->string('btn_group')->nullable();
            $table->boolean('unsubscribe')->default(0);
            $table->text('admin_notes')->nullable();
            $table->timestamp('last_contacted')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('academic_advisors');
    }
}
