<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unsigned();
            $table->string('stripe_refund_id')->nullable();
            $table->integer('student_id')->unsigned()
                    ->foreign('student_id')
                    ->references('id')
                    -> on('users')
                    ->onDelete('cascade');
            $table->integer('tutor_id')->unsigned()->nullable()
                    ->foreign('tutor_id')
                    ->references('id')
                    -> on('users')
                    ->onDelete('cascade');
            $table->integer('session_id')->unsigned()->nullable()
                    ->foreign('session_id')
                    ->references('id')
                    -> on('tutor_sessions')
                    ->onDelete('cascade');
            $table->integer('assignment_id')->unsigned()->nullable()
                    ->foreign('assignment_id')
                    ->references('id')
                    -> on('assignments')
                    ->onDelete('cascade');
            $table->decimal('requested_amount', 5, 2)->unsigned();
            $table->string('refund_reason');
            $table->text('reason_details')->nullable();
            $table->string('refund_method')->nullable();
            $table->text('method_details')->nullable();
            $table->decimal('deductions', 5,2)->unsigned()->nullable();
            $table->boolean('issue_refund')->default(0);
            $table->decimal('refunded_to_student', 5, 2)->unsigned()->nullable();
            $table->decimal('refunded_to_account', 5, 2)->unsigned()->nullable();
            $table->decimal('refunded_from_tutor', 5,2)->unsigned()->nullable();
            $table->decimal('refunded_from_btn', 5,2)->unsigned()->nullable();
            $table->date('processed_date')->nullable();
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('refunds');
    }
}
