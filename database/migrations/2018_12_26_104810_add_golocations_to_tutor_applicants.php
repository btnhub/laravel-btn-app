<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGolocationsToTutorApplicants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tutor_applicants', function (Blueprint $table) {
            $table->string('formatted_address')
                    ->nullable()
                    ->after('access_end_date');
            $table->tinyInteger('radius')->unsigned()->nullable()
                    ->after('formatted_address');
            $table->string('radius_unit',10)->nullable()
                    ->after('radius');
            $table->string('city', 100)->nullable()
                    ->after('radius_unit');
            $table->string('region', 100)->nullable()
                    ->after('city');
            $table->string('country', 100)->nullable()
                    ->after('region');
            $table->string('postal_code', 50)->nullable()
                    ->after('country');
            $table->decimal('lat', 10, 8)->nullable()
                    ->after('postal_code');
            $table->decimal('lng', 11, 8)->nullable()
                    ->after('lat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tutor_applicants', function (Blueprint $table) {
            $table->dropColumn('formatted_address');
            $table->dropColumn('radius');
            $table->dropColumn('radius_unit');
            $table->dropColumn('city');
            $table->dropColumn('region');
            $table->dropColumn('country');
            $table->dropColumn('postal_code');
            $table->dropColumn('lat');
            $table->dropColumn('lng');
        });
    }
}
