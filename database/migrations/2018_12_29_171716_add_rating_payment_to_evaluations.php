<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingPaymentToEvaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->tinyInteger('rating')
                    ->unsigned()
                    ->nullable()
                    ->after('satisfied');
            $table->string('payment_method')
                    ->nullable()
                    ->after('website_reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('evaluations', function (Blueprint $table) {
            $table->dropColumn('rating');
            $table->dropColumn('payment_method');
        });
    }
}
