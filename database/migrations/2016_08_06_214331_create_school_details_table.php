<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->nullable();
            $table->string('school_name');
            $table->string('town', 100)->nullable();
            $table->string('state', 50)->nullable();
            $table->string('website')->nullable();
            $table->string('calendar')->nullable();
            $table->string('course_catalog')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('school_details');
    }
}
