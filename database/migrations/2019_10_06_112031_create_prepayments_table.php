<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prepayments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->integer('user_id')
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('assignment_id')->nullable();
            $table->string('stripe_customer_id')->nullable();
            $table->string('transaction_id')->nullable();
            $table->string('reason')->nullable();
            $table->decimal('amount', 10, 2)->unsigned()->nullable();
            $table->decimal('discount_amount', 10,2)->unsigned()->nullable();
            $table->decimal('processing_fee', 10,2)->unsigned()->nullable();
            $table->decimal('total_amount', 10, 2)->unsigned()->nullable();
            $table->string('coupon_id')->nullable();
            $table->string('invoice_id')->nullable();
            $table->tinyInteger('payment_method')->unsigned()->nullable();
            $table->boolean('captured')->default(0);
            $table->boolean('paid')->default(0);
            $table->timestamp('completed_at')->nullable();
            $table->timestamp('refund_requested')->nullable();            
            $table->decimal('refund_amount', 10, 2)->unsigned()->nullable();
            $table->string('refund_reason')->nullable();
            $table->boolean('issue_refund')->nullable();
            $table->integer('refund_id')->nullable();
            $table->string('stripe_refund_id')->nullable();
            $table->timestamp('refund_processed')->nullable();
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prepayments');
    }
}
