<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBackgroundChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('background_checks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                    ->unsigned()
                    ->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->timestamp('payment_date')->nullable();
            $table->string('payment_id')->nullable();
            $table->boolean('paid')->default(0);
            $table->boolean('cleared')->default(0);
            $table->timestamp('cleared_date')->nullable();
            $table->string('candidate_id')->nullable();
            $table->string('invitation_id')->nullable();
            $table->string('report_id')->nullable();
            $table->string('adverse_action_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('background_checks');
    }
}
