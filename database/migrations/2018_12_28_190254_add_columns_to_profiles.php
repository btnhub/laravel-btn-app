<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->boolean('searchable')
                    ->after('show_profile_tutor')
                    ->nullable();
            $table->boolean('featured')
                    ->after('searchable')
                    ->nullable();
            $table->boolean('in_person')
                    ->after('session_locations')
                    ->comment('offers in-person tutoring')
                    ->nullable();
            $table->boolean('online')
                    ->after('in_person')
                    ->comment('offers online tutoring')
                    ->nullable();
            $table->tinyInteger('group_size')
                    ->after('hours_available')
                    ->unsigned()
                    ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('searchable');
            $table->dropColumn('featured');
            $table->dropColumn('in_person');
            $table->dropColumn('online');
            $table->dropColumn('group_size');
        });
    }
}
