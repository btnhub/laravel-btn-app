<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePageVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_visits', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumInteger('user_id')->unsigned()->nullable();
            $table->string('identifier', 15)->nullable();
            $table->string('role', 50)->nullable();
            $table->smallInteger('count')->unsigned()->nullable();
            $table->string('url', 150)->nullable();
            $table->string('url_previous', 150)->nullable();
            $table->string('referer', 150)->nullable();
            $table->string('browser', 150)->nullable();
            $table->string('ip', 50)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('region', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('postal_code', 50)->nullable();
            $table->decimal('lat', 10, 8)->nullable();
            $table->decimal('lng', 11, 8)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_visits');
    }
}
