<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id', 50)->nullable();
            $table->integer('university_id')->nullable();
            $table->integer('university_department_id')->nullable();
            $table->string('course_number', 20)->nullable();
            $table->string('name', 150)->nullable();
            $table->string('short_name', 50)->nullable();
            $table->text('description')->nullable();
            $table->boolean('visible')->default(1)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('university_courses');
    }
}
