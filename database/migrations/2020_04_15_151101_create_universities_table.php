<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('universities', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('location_id')->unsigned()->nullable();
            $table->string('name', 150)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('state_abbr', 50)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->boolean('visible')->nullable()->default(1);
            $table->text('alt_names')->nullable();
            $table->string('website', 100)->nullable();
            $table->string('course_catalog')->nullable();
            $table->string('calendar')->nullable();
            $table->boolean('private')->nullable();
            $table->string('term', 50)->nullable()->comment('semester vs quarter');
            $table->string('google_place_id', 50)->nullable();
            $table->string('state', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('postal_code', 50)->nullable();
            $table->decimal('lat', 10, 8)->nullable();
            $table->decimal('lng', 11, 8)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('universities');
    }
}
