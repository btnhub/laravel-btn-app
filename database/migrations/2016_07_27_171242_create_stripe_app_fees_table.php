<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStripeAppFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stripe_app_fees', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('session_id')->unsigned()
                    ->foreign('session_id')
                    ->references('id')
                    ->on('tutor_sessions')
                    ->onDelete('cascade');
            $table->integer('student_id')->unsigned()
                    ->foreign('student_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->string('stripe_customer_id');
            $table->integer('tutor_id')->unsigned()
                    ->foreign('tutor_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->string('stripe_tutor_id');
            $table->decimal('application_fee', 10,2)->unsigned();
            $table->decimal('stripe_fee', 10,2)->unsigned();
            $table->integer('refund_id')->unsigned()
                    ->foreign('refund_id')
                    ->references('id')
                    ->on('refunds')
                    ->onDelete('cascade');
            $table->string('stripe_refund_id')->nullable();
            $table->decimal('app_refund_amount', 10,2)->unsigned()->nullable();
            $table->datetime('refund_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stripe_app_fees');
    }
}
