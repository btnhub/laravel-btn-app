<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unique()->unsigned();
            $table->integer('tutor_request_id')->unsigned()->nullable();
            $table->integer('tutor_id')->unsigned();
            $table->integer('student_rate')->unsigned()->comment('hourly amount paid by student');
            $table->integer('tutor_rate')->unsigned()->comment('hourly amount earned by tutor');
            $table->datetime('deadline')->nullable();
            $table->datetime('deadline_student')->nullable();
            $table->text('concerns')->nullable()->comment('viewable by tutor');
            $table->boolean('tutor_decision')->nullable();
            $table->boolean('student_decision')->nullable();
            $table->datetime('tutor_decision_date')->nullable();
            $table->datetime('student_decision_date')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('course')->nullable();
            $table->boolean('eval_first')->default(0)->comment('whether reminder has been sent');
            $table->boolean('eval_end')->default(0)->comment('whether reminder has been sent');
            $table->text('tutor_reject_reason')->nullable();
            $table->text('student_reject_reason')->nullable();
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_contracts');
    }
}
