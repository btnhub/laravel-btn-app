<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_tutor_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unique()->unsigned();
            $table->boolean('mkt_visibility')->default(0);
            $table->integer('student_id')
                    ->foreign('student_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('requested_tutor_id')
                    ->foreign('requested_tutor_id')
                    ->references('id')
                    ->on('users')
                    ->nullable()
                    ->onDelete('cascade');
            $table->boolean('requested_tutor_declined')
                    ->default(0);
            $table->integer('msg_thread_id')
                    ->foreign('msg_thread_id')
                    ->references('id')
                    ->on('msg_threads')
                    ->nullable()
                    ->onDelete('cascade');
            $table->integer('location_id')->unsigned()
                    ->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade');
            $table->string('location');
            $table->string('child_name')
                    ->nullable();
            $table->date('start_date');
            $table->date('end_date');
            $table->string('urgency')->default('Normal');
            $table->string('level');
            $table->string('subjects')->nullable();
            $table->string('course');
            $table->string('session_time');
            $table->smallInteger('max_rate')->unsigned();
            $table->string('frequency');
            $table->string('scholarship_program')->nullable();
            $table->text('concerns')->nullable();
            $table->integer('matched_tutor_id')->nullable();
            $table->integer('matched_by_id')
                    ->foreign('matched_by_id')
                    ->references('id')
                    ->on('users')->nullable()
                    ->onDelete('cascade');
            $table->datetime('matched_date')->nullable();
            $table->integer('assignment_id')->unsigned()
                    ->foreign('assignment_id')
                    ->references('id')
                    ->on('assignments')->nullable()
                    ->onDelete('cascade');    
            $table->boolean('unassigned')
                    ->default(0);
            $table->integer('deleted_by')
                    ->foreign('deleted_by')
                    ->references('id')
                    ->on('users');                
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('market_tutor_requests');
    }
}
