<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemindersToTutorRequets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->datetime('email_reminder')->nullable()
                    ->after('sms_alert');
            $table->datetime('sms_reminder')->nullable()
                    ->after('email_reminder');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->dropColumn('email_reminder');
            $table->dropColumn('sms_reminder');
        });
    }
}
