<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Database\Eloquent\SoftDeletes;

class CreateTutorSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_sessions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unique()->unsigned();
            $table->integer('assignment_id')->unsigned()
                ->foreign('assignment_id')
                ->references('id')
                ->on('assignments')
                ->onDelete('cascade');
            $table->integer('student_id')->unsigned()
                ->foreign('student_id')
                ->references('id')
                ->on('users');
            $table->integer('tutor_id')->unsigned()
                ->foreign('tutor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->date('session_date');
            $table->decimal('duration', 5,2)->unsigned();
            $table->string('outcome');
            $table->decimal('amt_charged', 10,2)->unsigned();
            $table->decimal('tutor_pay', 10,2)->unsigned();
            $table->decimal('btn_pay', 10,2)->unsigned();
            $table->decimal('discount', 10,2)->unsigned()->default(0);
            $table->date('tutor_payment_date')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('stripe_payment_id')->nullable();
            $table->string('stripe_transfer_id')->nullable();
            $table->integer('refund_id')->nullable();
            $table->integer('deleted_by')
                    ->foreign('deleted_by')
                    ->references('id')
                    ->on('users')->nullable(); 
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tutor_sessions');
    }
}
