<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('stripe_customer_id')->nullable();
            $table->string('transaction_id');
            $table->decimal('amount', 10, 2)->unsigned();
            $table->decimal('discount_amount', 10,2)->unsigned()->nullable();
            $table->decimal('processing_fee', 10,2)->unsigned()->nullable();
            $table->decimal('total_amount', 10, 2)->unsigned();
            $table->string('coupon_id')->nullable();
            $table->string('invoice_id')->nullable();
            $table->tinyInteger('payment_method')->unsigned()
                ->foreign('payment_method')
                ->references('id')
                ->on('payment_method')
                ->onDelete('cascade');
            $table->boolean('captured')->default(0);
            $table->boolean('paid')->default(0);
            $table->integer('destination_tutor')->nullable();
            $table->string('destination_stripe_id')->nullable();
            $table->string('stripe_transfer_id')->nullable();
            $table->decimal('application_fee', 10, 2)->unsigned()->nullable();
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payments');
    }
}
