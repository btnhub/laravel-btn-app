<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutorProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('market_tutor_proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tutor_request_id')->unsigned()
                    ->foreign('tutor_request_id')
                    ->references('id')
                    ->on('mkt_tutor_requests')
                    ->onDelete('cascade');
            $table->integer('student_id')->unsigned()
                    ->foreign('student_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('tutor_id')->unsigned()
                    ->foreign('tutor_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('msg_thread_id')->unsigned()
                    ->foreign('msg_thread_id')
                    ->references('id')
                    ->on('msg_threads')
                    ->onDelete('cascade');
            $table->integer('location_id')->unsigned()
                    ->foreign('location_id')
                    ->references('id')
                    ->on('locations')
                    ->onDelete('cascade');
            $table->integer('proposed_rate')->unsigned();
            $table->boolean('accepted_proposal')->default(0);
            $table->integer('assignment_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('market_tutor_proposals');
    }
}
