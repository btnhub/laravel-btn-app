<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ref_id')->unique()->unsigned();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('tutor_id')->unsigned()
                -> foreign('tutor_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('student_id')->unsigned()
                -> foreign('student_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('child_name')
                    ->nullable();
            $table->integer('assigned_by')->unsigned()
                -> foreign('assigned_by')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->integer('location_id')->unsigned()
                -> foreign('location_id')
                ->references('id')
                ->on('locations')
                ->onDelete('cascade');
            $table->boolean('declined')->default(0);
            $table->string('course');
            $table->decimal('rate', 10,2)
                    ->unsigned()
                    ->nullable();
            $table->smallInteger('billing_increment')
                    ->unsigned()
                    ->nullable()
                    ->comment('minutes');
            $table->boolean('eval_first')->default(0)->comment('whether reminder has been sent');
            $table->boolean('eval_end')->default(0)->comment('whether reminder has been sent');
            $table->text('admin_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assignments');
    }
}
