<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversityDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('university_departments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('university_id')->nullable();
            $table->string('name', 100)->nullable();
            $table->string('short_name', 50)->nullable();
            $table->string('code', 10)->nullable();
            $table->string('website')->nullable();
            $table->string('course_catalog')->nullable();
            $table->boolean('visible')->default(1)->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('university_departments');
    }
}
