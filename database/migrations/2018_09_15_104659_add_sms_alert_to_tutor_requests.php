<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSmsAlertToTutorRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->boolean('sms_alert')
                    ->nullable()
                    ->after('concerns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->dropColumn('sms_alert');
        });
    }
}
