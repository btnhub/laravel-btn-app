<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchoolToMktTutorRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->string('school', 150)->nullable()
                    ->after('course');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->dropColumn('school');
        });
    }
}
