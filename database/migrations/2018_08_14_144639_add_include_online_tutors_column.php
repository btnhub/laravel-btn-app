<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncludeOnlineTutorsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->boolean('include_online_tutors')
                    ->nullable()
                    ->after('requested_tutors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('market_tutor_requests', function (Blueprint $table) {
            $table->dropColumn('include_online_tutors');
        });
    }
}
