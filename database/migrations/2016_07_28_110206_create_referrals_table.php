<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referrals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('new_student')
                    ->foreign('new_student')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('existing_student')
                    ->foreign('existing_student')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->decimal('credit', 5,2);
            $table->boolean('applied')->default(0);
            $table->text('admin_notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('referrals');
    }
}
