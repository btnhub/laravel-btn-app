<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assignment_id')
                    ->foreign('assignment_id')
                    ->references('id')
                    ->on('assignments')
                    ->onDelete('cascade');
            $table->integer('student_id')
                    ->foreign('student_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('tutor_id')
                    ->foreign('tutor_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->string('type');
            $table->boolean('satisfied');
            $table->string('frequency')->nullable();
            $table->text('tutor_strength')->nullable();
            $table->text('tutor_weakness')->nullable();
            $table->text('website_reference')->nullable();
            $table->string('school')->nullable();
            $table->date('review_date')->nullable();
            $table->boolean('homepage_visible')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluations');
    }
}
