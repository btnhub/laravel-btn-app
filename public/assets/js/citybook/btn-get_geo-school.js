var school_input = document.getElementById('autocomplete-school');
var school_address_options = {'types': ['establishment']/*, 'type': ['doctor']*/};
var autocomplete_school = new google.maps.places.Autocomplete(school_input, school_address_options);

google.maps.event.addListener(autocomplete_school, 'place_changed', function(){
    var place = autocomplete_school.getPlace();
    if (!place.geometry) {
        window.alert("Please select a SCHOOL from the dropdown list");
        return;
    }

    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    
    document.getElementById('school-lat').value = lat;
    document.getElementById('school-lng').value = lng;
    // document.getElementById('formatted_address').value = place.formatted_address;
    document.getElementById('school-name').value = place.name;
    document.getElementById('school-website').value = place.website;
    document.getElementById('school-google_place_id').value = place.place_id;

    var address = place.address_components;

    for (var i = 0; i < address.length; i++) {
        switch(address[i].types[0]) {
          case "locality":
            var city = address[i].long_name;
            document.getElementById('school-city').value = city;
            break;
          case "administrative_area_level_1":
            var state = address[i].long_name;
            var state_abbr = address[i].short_name;
            document.getElementById('school-state').value = state;
            document.getElementById('school-state_abbr').value = state_abbr;
            break;
          case "country":
            var country = address[i].long_name;
            document.getElementById('school-country').value = country;
            break;
          case "postal_code":
            var postal_code = address[i].long_name;
            document.getElementById('school-postal_code').value = postal_code;
            break;
        }
    }
});
