var markersArray = [];
var circlesArray = [];
var marker_count = 0;
var single_map;

function singleMap() {
    var center = $('#singleMap').data('center');
    var map_center = {lat: Number(center['lat']), lng: Number(center['lng'])};

    single_map = new google.maps.Map(document.getElementById('singleMap'), {
        zoom: 12,
        center: map_center,
        scrollwheel: false,
        zoomControl: false,
        mapTypeControl: false,
        scaleControl: false,
        panControl: false,
        navigationControl: false,
        streetViewControl: false,
        styles: [{
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#f2f2f2"
            }]
        }]
    });

    var markerIcon2 = {
        url: location.protocol + "//" + location.hostname + 
      (location.port && ":" + location.port) + "/" + 'citybook-images/marker.png',
    }		

    var zoomControlDiv = document.createElement('div');
    var zoomControl = new ZoomControl(zoomControlDiv, single_map);

    function ZoomControl(controlDiv, single_map) {
        zoomControlDiv.index = 1;
        single_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
        controlDiv.style.padding = '5px';
        var controlWrapper = document.createElement('div');
        controlDiv.appendChild(controlWrapper);
        var zoomInButton = document.createElement('div');
        zoomInButton.className = "mapzoom-in";
        controlWrapper.appendChild(zoomInButton);
        var zoomOutButton = document.createElement('div');
        zoomOutButton.className = "mapzoom-out";
        controlWrapper.appendChild(zoomOutButton);
        google.maps.event.addDomListener(zoomInButton, 'click', function () {
            single_map.setZoom(single_map.getZoom() + 1);
        });
        google.maps.event.addDomListener(zoomOutButton, 'click', function () {
            single_map.setZoom(single_map.getZoom() - 1);
        });
    }

    google.maps.event.addListener(single_map, 'click', function(event) {
        addMarker(event.latLng);
    });

    var bounds = new google.maps.LatLngBounds();

    var geolocations = $('#singleMap').data('geo');
    var prior_geo;

    for (var i = 0; i < geolocations.length; i++) {
        prior_geo = {lat: Number(geolocations[i].lat),
                     lng: Number(geolocations[i].lng)};

        prior_geo_marker = addMarker(prior_geo, geolocations[i].radius);

        addCircle(prior_geo_marker.id);

        if (geolocations.length > 1) {
            bounds.extend(prior_geo_marker.getPosition());
            single_map.fitBounds(bounds);     
        }
    }

    function addMarker(location, marker_radius = 0) {
        marker_count++;

        var new_marker = new google.maps.Marker({
            position: location, 
            map: single_map,
            icon: markerIcon2,
            draggable: true,
            id: Math.floor(Math.random() * 10000) + 1,
            title: "Location " + marker_count
        });

        markersArray.push(new_marker);

        var new_marker_label = document.createElement('div');
        new_marker_label.setAttribute('id', 'label-' + new_marker.id);
        new_marker_label.innerHTML = '<label>Radius ' + marker_count + '<i class="fa fa-map"></i></label>';

        var new_marker_radius = document.createElement('input');
        new_marker_radius.setAttribute('type', 'number');
        new_marker_radius.setAttribute('name', 'radius[' + new_marker.id + ']');
        new_marker_radius.setAttribute('id', 'radius-' + new_marker.id);
        new_marker_radius.setAttribute('min', '0');
        new_marker_radius.setAttribute('value', marker_radius);
        new_marker_radius.setAttribute('onChange', "addCircle(" + new_marker.id + ")");

        var new_marker_lat = document.createElement('input');
        new_marker_lat.setAttribute('type', 'hidden');
        new_marker_lat.setAttribute('name', 'lat[' + new_marker.id + ']');
        new_marker_lat.setAttribute('id', 'lat-' + new_marker.id);
        new_marker_lat.setAttribute('value', new_marker.getPosition().lat());

        var new_marker_lng = document.createElement('input');
        new_marker_lng.setAttribute('type', 'hidden');
        new_marker_lng.setAttribute('name', 'lng[' + new_marker.id + ']');
        new_marker_lng.setAttribute('id', 'lng-' + new_marker.id);
        new_marker_lng.setAttribute('value', new_marker.getPosition().lng());

        var markers_row = document.getElementById('markers');
        markers_row.appendChild(new_marker_label);
        markers_row.appendChild(new_marker_radius);
        markers_row.appendChild(new_marker_lat);
        markers_row.appendChild(new_marker_lng);

        google.maps.event.addListener(new_marker, 'dragend', function (event) {
            document.getElementById('lat-' + new_marker.id).value = this.getPosition().lat();
            document.getElementById('lng-' + new_marker.id).value = this.getPosition().lng();
        });

        google.maps.event.addListener(new_marker, 'dblclick', function (event) {
            var label_div = document.getElementById('label-' + new_marker.id);
            var lat_input = document.getElementById('lat-' + new_marker.id);
            var lng_input = document.getElementById('lng-' + new_marker.id);
            var radius_input = document.getElementById('radius-' + new_marker.id);

            for (var i = 0; i < markersArray.length; i++) {
                if (markersArray[i].id == new_marker.id) {
                    
                    markersArray.splice(i, 1);
                
                    label_div.parentNode.removeChild(label_div);
                    lat_input.parentNode.removeChild(lat_input);
                    lng_input.parentNode.removeChild(lng_input);
                    radius_input.parentNode.removeChild(radius_input);

                    new_marker.setMap(null);
                }
            }

            for (var i = 0; i < circlesArray.length; i++) {
                if (circlesArray[i].id == new_marker.id) {
                    circlesArray[i].setMap(null);
                }
            }
        });

        return new_marker;
    }
}

var single_map = document.getElementById('singleMap');
if (typeof (single_map) != 'undefined' && single_map != null) {
    google.maps.event.addDomListener(window, 'load', singleMap);
}

function addCircle(marker_id){
    //Remove current circle
    for (var i = 0; i < circlesArray.length; i++) {
        if (circlesArray[i].id == marker_id) {
            circlesArray[i].setMap(null);
        }
    }

    var radius = document.getElementById('radius-' + marker_id).value;
    var units = document.getElementsByName('radius_unit');
    var unit;

    for (var i = 0; i < units.length; i++) {
        if (units[i].checked) {
            unit = units[i].value;
        }
    }

    if (unit == "miles") {
        //convert radius miles -> m
        radius = radius * 1609;    
    }
    else{
        //convert radius km -> m
        radius = radius * 1000;
    }
    
    for (var i = 0; i < markersArray.length; i++) {
        if (markersArray[i].id == marker_id) {
            var circle = new google.maps.Circle({
                map: single_map,
                radius: radius, 
                fillColor: '#0055ff',
                fillOpacity: 0.1, 
                strokeColor: "#0055ff", 
                strokeOpacity: 0.5, 
                strokeWeight: 1,
                id: markersArray[i].id
            });
            circle.bindTo('center', markersArray[i], 'position');
            circlesArray.push(circle);
        }
    }
}

function createGeoElements(marker_id){

}