function saveTutor(decision, tutor_ref_id, token){
	$.ajax({
       	type:'POST',
        url: location.protocol + "//" + location.hostname + 
      (location.port && ":" + location.port) + "/" + "save-tutor/" + tutor_ref_id,
       	data:{
       		_token: token, 
       		decision: decision
       	},
       	success:function(data){
        	var saveButtons = document.getElementsByClassName('save-tutor');
            var unsaveButtons = document.getElementsByClassName('unsave-tutor');

            if (decision == 'save') {
                if (saveButtons.length > 0) {
                    for (var i = 0; i < saveButtons.length; i++) {
                    saveButtons[i].style.display = "none";
                }

                    for (var i = 0; i < unsaveButtons.length; i++) {
                        unsaveButtons[i].style.display = "block";
                    }    
                }
                else{
                    //Pages with multiple profiles, change only one 
                    document.getElementById('save-button-' + tutor_ref_id).style.display = "none";
                    document.getElementById('unsave-button-' + tutor_ref_id).style.display = "block";
                }
                
            }
            else{
                if (saveButtons.length > 0) {
                    for (var i = 0; i < saveButtons.length; i++) {
                        saveButtons[i].style.display = "block";
                    }

                    for (var i = 0; i < unsaveButtons.length; i++) {
                        unsaveButtons[i].style.display = "none";
                    }
                }
                else{
                    //Pages with multiple profiles, change only one 
                    document.getElementById('save-button-' + tutor_ref_id).style.display = "block";
                    document.getElementById('unsave-button-' + tutor_ref_id).style.display = "none";
                }
            }
       	},
        error: function(xhr, status, error){
            alert('Sorry, we could not connect to the server. Please try again later.');
        }
    });	
}
