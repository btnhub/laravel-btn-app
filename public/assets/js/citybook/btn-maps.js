(function ($) {
    "use strict";
    var markerIcon = {
        anchor: new google.maps.Point(22, 16),
        // url: 'images/marker.png',
        url: location.protocol + "//" + location.hostname + 
      (location.port && ":" + location.port) + "/" + 'citybook-images/marker.png',
    }

    function btnMainMap(){
        var locations = [];
        var bounds = new google.maps.LatLngBounds();

        function locationData(profileURL, tutorStatus, tutorAvatar, nameEducation, sessionLocations, tutorMajor, tutorAvgRating, tutorReviewsCounter) {
            return ('<div class="map-popup-wrap"><div class="map-popup"><div class="infoBox-close"><i class="fa fa-times"></i></div><div class="map-popup-category">' + tutorStatus + '</div><a href="' + profileURL + '" class="listing-img-content fl-wrap"><img src="' + tutorAvatar + '" alt=""></a> <div class="listing-content fl-wrap"><div class="card-popup-raining map-card-rainting" data-staRrating="' + tutorAvgRating + '"><span class="map-popup-reviews-count">( ' + tutorReviewsCounter + ' reviews )</span></div><div class="listing-title fl-wrap"><h4><a href=' + profileURL + '>' + nameEducation + '</a></h4><span class="map-popup-location-info"><i class="fa fa-graduation-cap"></i>' + tutorMajor + '</span><span class="map-popup-location-phone"><i class="fa fa-map-marker"></i>' + sessionLocations + '</span></div></div></div></div>')
        }

        var tutors = $('#map-main').data('tutors');
        var map_tutor_count = 0;
        for (var i in tutors){
            locations[i] = [locationData(tutors[i]['url'], tutors[i]['tutor_status'], tutors[i]['avatar'], tutors[i]['name_education'], tutors[i]['session_locations'], tutors[i]['major'], tutors[i]['rating'], tutors[i]['reviews']), Number(tutors[i]['lat']), Number(tutors[i]['lng']), Number(tutors[i]['map_position']), markerIcon];
            var location_lat = locations[i][1];
            
            if (locations[i][1]) {
                map_tutor_count++;    
            }
            
        }
        
        var map = new google.maps.Map(document.getElementById('map-main'), {
            zoom: 13,
            scrollwheel: false,
            center: $('#map-main').data('latlng'),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            panControl: false,
            fullscreenControl: true,
            navigationControl: false,
            streetViewControl: false,
            animation: google.maps.Animation.BOUNCE,
            gestureHandling: 'cooperative',
            styles: [{
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#444444"
                }]
            }]
        });

        var boxText = document.createElement("div");
        boxText.className = 'map-box'
        var currentInfobox;
        var boxOptions = {
            content: boxText,
            disableAutoPan: true,
            alignBottom: true,
            maxWidth: 0,
            pixelOffset: new google.maps.Size(-145, -45),
            zIndex: null,
            boxStyle: {
                width: "260px"
            },
            closeBoxMargin: "0",
            closeBoxURL: "",
            infoBoxClearance: new google.maps.Size(1, 1),
            isHidden: false,
            pane: "floatPane",
            enableEventPropagation: false,
        };
        var markerCluster, marker, i;
        var allMarkers = [];
        var clusterStyles = [{
            textColor: 'white',
            url: '',
            height: 50,
            width: 50
        }];


        for (i = 0; i < locations.length; i++) {
            var location_lat = locations[i][1];
             //Only include tutors with a lat-lng location (do not include online tutors on map)
            if (location_lat) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    icon: locations[i][4],
                    id: i
                });

                allMarkers.push(marker);
                var ib = new InfoBox();
                google.maps.event.addListener(ib, "domready", function () {
                    cardRaining()
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        ib.setOptions(boxOptions);
                        boxText.innerHTML = locations[i][0];
                        ib.close();
                        ib.open(map, marker);
                        currentInfobox = marker.id;
                        var latLng = new google.maps.LatLng(locations[i][1], locations[i][2]);
                        map.panTo(latLng);
                        map.panBy(0, -180);
                        google.maps.event.addListener(ib, 'domready', function () {
                            $('.infoBox-close').click(function (e) {
                                e.preventDefault();
                                ib.close();
                            });
                        });
                    }
                })(marker, i));
                
                //Automatically reset zoom and center
                if (map_tutor_count > 1) {
                    bounds.extend(marker.getPosition());
                    map.fitBounds(bounds);     
                }    
            }
            
        }
        var options = {
            imagePath: 'btn/avatars/',
            styles: clusterStyles,
            minClusterSize: 2
        };

        markerCluster = new MarkerClusterer(map, allMarkers, options);
        google.maps.event.addDomListener(window, "resize", function () {
            var center = map.getCenter();
            google.maps.event.trigger(map, "resize");
            map.setCenter(center);
        });

        //CENTER MARKER
        var center_marker = new google.maps.Marker({
                        position: $('#map-main').data('latlng'),
                        map: map});

        $('.nextmap-nav').click(function (e) {
            e.preventDefault();
            map.setZoom(15);
            var index = currentInfobox;
            if (index + 1 < allMarkers.length) {
                google.maps.event.trigger(allMarkers[index + 1], 'click');
            } else {
                google.maps.event.trigger(allMarkers[0], 'click');
            }
        });
        $('.prevmap-nav').click(function (e) {
            e.preventDefault();
            map.setZoom(15);
            if (typeof (currentInfobox) == "undefined") {
                google.maps.event.trigger(allMarkers[allMarkers.length - 1], 'click');
            } else {
                var index = currentInfobox;
                if (index - 1 < 0) {
                    google.maps.event.trigger(allMarkers[allMarkers.length - 1], 'click');
                } else {
                    google.maps.event.trigger(allMarkers[index - 1], 'click');
                }
            }
        });
        $('.map-item').click(function (e) {
            e.preventDefault();
            map.setZoom(15);
            var index = currentInfobox;
            var marker_index = parseInt($(this).attr('href').split('#')[1], 10);
            google.maps.event.trigger(allMarkers[marker_index], "click");
            if ($(this).hasClass("scroll-top-map")){
              $('html, body').animate({
                scrollTop: $(".map-container").offset().top+ "-80px"
              }, 500)                               
            }
            else if ($(window).width()<1064){
              $('html, body').animate({
                scrollTop: $(".map-container").offset().top+ "-80px"
              }, 500)               
            }
        });

        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, map);

        function ZoomControl(controlDiv, map) {
            zoomControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
            controlDiv.style.padding = '5px';
            var controlWrapper = document.createElement('div');
            controlDiv.appendChild(controlWrapper);
            var zoomInButton = document.createElement('div');
            zoomInButton.className = "mapzoom-in";
            controlWrapper.appendChild(zoomInButton);
            var zoomOutButton = document.createElement('div');
            zoomOutButton.className = "mapzoom-out";
            controlWrapper.appendChild(zoomOutButton);
            google.maps.event.addDomListener(zoomInButton, 'click', function () {
                map.setZoom(map.getZoom() + 1);
            });
            google.maps.event.addDomListener(zoomOutButton, 'click', function () {
                map.setZoom(map.getZoom() - 1);
            });
        }
    }

    var map = document.getElementById('map-main');
    if (typeof (map) != 'undefined' && map != null) {
        google.maps.event.addDomListener(window, 'load', btnMainMap);
    }

    var markerIcon2 = {
        // url: 'images/marker.png',
        url: location.protocol + "//" + location.hostname + 
      (location.port && ":" + location.port) + "/" + 'citybook-images/marker.png',
    }

    function tutorMap(){
        var lat, lng, radius, radius_unit;
        var locations = $('#tutorMap').data('locations');
        var bounds = new google.maps.LatLngBounds();

        var tutor_map = new google.maps.Map(document.getElementById('tutorMap'), {
                //zoom: 2,
                //center: {lat: Number(locations[0]['lat']), lng: Number(locations[0]['lng'])},
                scrollwheel: false,
                zoomControl: false,
                mapTypeControl: false,
                scaleControl: false,
                panControl: false,
                navigationControl: false,
                streetViewControl: false,
                styles: [{
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#f2f2f2"
                    }]
                }] 
            });

        for (var i in locations){
            lat = Number(locations[i]['lat']);
            lng = Number(locations[i]['lng']);
            
            var locationLatLng = {lat: lat, lng: lng};

            radius = locations[i]['radius'];
            radius_unit = locations[i]['radius_unit'];

            if (radius_unit == "miles") {
                //convert miles to km
                radius = radius * 1.609;
            }

            radius = radius * 1000; //convert to meters

            var marker = new google.maps.Marker({
                position: locationLatLng,
                map: tutor_map,
                icon: markerIcon2,
            });

            var circle = new google.maps.Circle({
              map: tutor_map,
              radius: radius,    // in metres
              fillColor: '#0055ff',
              fillOpacity: 0.1, 
              strokeColor: "#0055ff", 
              strokeOpacity: 0.5, 
              strokeWeight: 1 
            });
            
            circle.bindTo('center', marker, 'position');
            
            if (locations.length == 1) {
                if (radius > 0) {
                    tutor_map.fitBounds(circle.getBounds());    
                }
                else{
                    tutor_map.setZoom(14);
                    tutor_map.setCenter(marker.getPosition());
                }
            }
            else{
                //Automatically scale map to show all markers
                bounds.extend(marker.getPosition());
                tutor_map.fitBounds(bounds);    
            }    
        }

        var zoomControlDiv = document.createElement('div');
        var zoomControl = new ZoomControl(zoomControlDiv, tutor_map);

        function ZoomControl(controlDiv, tutor_map) {
            zoomControlDiv.index = 1;
            tutor_map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(zoomControlDiv);
            controlDiv.style.padding = '5px';
            var controlWrapper = document.createElement('div');
            controlDiv.appendChild(controlWrapper);
            var zoomInButton = document.createElement('div');
            zoomInButton.className = "mapzoom-in";
            controlWrapper.appendChild(zoomInButton);
            var zoomOutButton = document.createElement('div');
            zoomOutButton.className = "mapzoom-out";
            controlWrapper.appendChild(zoomOutButton);
            google.maps.event.addDomListener(zoomInButton, 'click', function () {
                tutor_map.setZoom(tutor_map.getZoom() + 1);
            });
            google.maps.event.addDomListener(zoomOutButton, 'click', function () {
                tutor_map.setZoom(tutor_map.getZoom() - 1);
            });
        }
    }
    
    var tutor_map = document.getElementById('tutorMap');
    if (typeof (tutor_map) != 'undefined' && tutor_map != null) {
        google.maps.event.addDomListener(window, 'load', tutorMap());
    }
})(this.jQuery);