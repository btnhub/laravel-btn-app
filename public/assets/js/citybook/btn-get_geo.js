var address = document.getElementById('autocomplete-input');
address.required = true; //make location field required
var address_options = {'types': ['(cities)']};
var autocomplete = new google.maps.places.Autocomplete(address, address_options);

google.maps.event.addListener(autocomplete, 'place_changed', function(){
    var place = autocomplete.getPlace();
    if (!place.geometry) {
        window.alert("Please select a location from the dropdown list");
        return;
      }
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    var formatted_address = place.formatted_address;
    var map_center = new google.maps.LatLng(lat, lng);
    
    document.getElementById('lat').value = lat;
    document.getElementById('lng').value = lng;
    document.getElementById('formatted_address').value = formatted_address;

    var address = place.address_components;

    for (var i = 0; i < address.length; i++) {
        switch(address[i].types[0]) {
          case "locality":
            var city = address[i].long_name;
            document.getElementById('city').value = city;
            break;
          case "administrative_area_level_1":
            var region = address[i].long_name;
            document.getElementById('region').value = region;
            break;
          case "country":
            var country = address[i].long_name;
            document.getElementById('country').value = country;
            break;
          case "postal_code":
            var postal_code = address[i].long_name;
            document.getElementById('postal_code').value = postal_code;
            break;
        }
    }
});
