function calcCharge(assign_ref_id, tutor_rate)
{
    var outcome = document.getElementById('outcome-' + assign_ref_id).value; 
    var charge_student = 1;

    if (outcome == 'Free Session' || outcome == 'Tutor Cancelled Without 24 Hours Notice') {
        charge_student = 0;
    }

    var billing_increment = 60; 
    var duration = document.getElementById('duration-' + assign_ref_id).value; 
    
    var tutor_pay = charge_student * tutor_rate * (duration / billing_increment);
    document.getElementById('tutor_pay-' + assign_ref_id).innerHTML = "$" + tutor_pay.toFixed(2); // return to 2 decimal places
}

function showChargeForm(assign_ref_id){
    var charge_rows = document.getElementsByName('charge-row');
    for (var i = 0; i < charge_rows.length; i++) {
        charge_rows[i].style.display = "none";
    }

    var charge_buttons = document.getElementsByName('charge-btn');
    for (var i = 0; i < charge_buttons.length; i++) {
        charge_buttons[i].style.display = "block";
    }

    //Reset Charge Fields
    var charge_outcomes = document.getElementsByName('outcome');
    for (var i = 0; i < charge_outcomes.length; i++) {
        charge_outcomes[i].value = "";
    }
    var charge_durations = document.getElementsByName('duration');
    for (var i = 0; i < charge_durations.length; i++) {
        charge_durations[i].value = "";
    }
    var charge_net_pay = document.getElementsByName('net-pay');
    for (var i = 0; i < charge_net_pay.length; i++) {
        charge_net_pay[i].innerHTML = "$0";
    }
    
    document.getElementById('charge-row-' + assign_ref_id).style.display = "block";

    document.getElementById('charge-show-' + assign_ref_id).style.display = "none";
}

function hideChargeForm(assign_ref_id){
    var charge_rows = document.getElementsByName('charge-row');
    for (var i = 0; i < charge_rows.length; i++) {
        charge_rows[i].style.display = "none";
    }

    //Reset Charge Fields
    var charge_outcomes = document.getElementsByName('outcome');
    for (var i = 0; i < charge_outcomes.length; i++) {
        charge_outcomes[i].value = "";
    }
    var charge_durations = document.getElementsByName('duration');
    for (var i = 0; i < charge_durations.length; i++) {
        charge_durations[i].value = "";
    }
    var charge_net_pay = document.getElementsByName('net-pay');
    for (var i = 0; i < charge_net_pay.length; i++) {
        charge_net_pay[i].innerHTML = "$0";
    }

    document.getElementById('charge-show-' + assign_ref_id).style.display = "block";
}

function disableSubmitButton(button_id){
    console.log(document.getElementById('button-' + button_id));
    document.getElementById('button-' + button_id).disabled = true;
    document.getElementById('button-' + button_id).innerHTML = "Processing ...";

}