function starCount(count){
    document.getElementById('star_rating').value = count;
}
        
function showEvaluation(assign_ref_id, tutor_name, eval_type){
	var csrf_token;
	var meta_tags = document.getElementsByTagName('meta');
	
	for (var i = 0; i < meta_tags.length; i++) {
		if (meta_tags[i].name == "csrf-token") {
			csrf_token = meta_tags[i].content;
		}
	}
	
	//Reset form
	var previous_form = document.getElementById('eval-form');
	while (previous_form.hasChildNodes()) {   
	  previous_form.removeChild(previous_form.firstChild);
	}

	var eval_url = location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/students/evaluations/" + eval_type + "/" + assign_ref_id;

	var name_spans = document.getElementsByName('eval-tutor-name');
	for (var i = 0; i < name_spans.length; i++) {
		name_spans[i].innerHTML = tutor_name;
	}

	var f = document.createElement("form");
	f.setAttribute('method',"post");
	f.setAttribute('action', eval_url);
	f.setAttribute('class', 'custom-form')

	var csrf_field = document.createElement('input');
	csrf_field.setAttribute('type', 'hidden');
	csrf_field.setAttribute('name', '_token');
	csrf_field.setAttribute('value', csrf_token);

	var rating = document.createElement("input"); //input element, text
	rating.setAttribute('type',"hidden");
	rating.setAttribute('name', "star_rating");
	rating.setAttribute('id', "star_rating");

	var frequency_row = document.createElement('div');
	frequency_row.setAttribute('class', 'row');
	frequency_row.innerHTML = "<div class='col-md-6'>How often did you meet with " + tutor_name + "?</div><div class='col-md-6'><input type='text' name='frequency' required></div>";

	var performance_row = document.createElement("div");
	performance_row.setAttribute('class', 'row');
	
	var performance_col_1 = document.createElement("div");
	performance_col_1.setAttribute('class', 'col-md-6');
	var performance_col_2 = document.createElement("div");
	performance_col_2.setAttribute('class', 'col-md-6');

	if (eval_type == 'first') {
		performance_col_1.innerHTML = "Did " + tutor_name + " meet or exceed your expectations during the 1st session or first few sessions? *";

		performance_col_2.innerHTML = "<div class='add-list-media-header'><div class='col-xs-6'><label class='radio inline'><input type='radio' name='satisfied' value='1' required><span>Yes</span></label></div> <div class='col-xs-6'><label class='radio inline'><input type='radio' name='satisfied' value='0' required><span>No</span></label></div></div>";
	}
	else{
		performance_col_1.innerHTML = "Did " + tutor_name + " have a positive impact on your academic performance? Would you recommend" + tutor_name +" to your friends? *";

		performance_col_2.innerHTML = "<div class='add-list-media-header'><div class='col-xs-6'><label class='radio inline'><input type='radio' name='satisfied' value='1' required><span>Yes</span></label></div> <div class='col-xs-6'><label class='radio inline'><input type='radio' name='satisfied' value='0' required><span>No</span></label></div></div>";
		var feedback_row_3 = document.createElement('div');
		feedback_row_3.setAttribute('class', 'row');
		feedback_row_3.style.margin = "20px 0px";
		feedback_row_3.innerHTML = "<textarea name='website_reference' placeholder='Profile Reference (Optional): Tell the world about your experience with " + tutor_name + " (whether good or bad).'></textarea>";
	}	

	performance_row.appendChild(performance_col_1);
	performance_row.appendChild(performance_col_2);

	var payment_row = document.createElement('div');
	payment_row.setAttribute('class', 'row');
	payment_row.style.margin = "20px 0px";
	payment_row.innerHTML = "<div class='col-md-6'>How did you pay " + tutor_name + "? (BuffTutor website, cash, Venmo) <br>Was the payment method easy?</div><div class='col-md-6'><input type='text' name='payment_method' placeholder='Tutor session payment method' required></div>";

	var feedback_row_1 = document.createElement('div');
	feedback_row_1.setAttribute('class', 'row');
	feedback_row_1.style.margin = "20px";
	feedback_row_1.innerHTML = "<b>Optional:</b>  Include constructive feedback to help improve " + tutor_name +"'s performance.";

	var feedback_row_2 = document.createElement('div');
	feedback_row_2.setAttribute('class', 'row');
	feedback_row_2.innerHTML = '<div class="col-md-6"><textarea name="tutor_strength" cols="40" rows="3" placeholder="Strengths: What did ' + tutor_name + ' do well?"></textarea></div><div class="col-md-6"><textarea name="tutor_weakness" cols="40" rows="3" placeholder="Weakness: What should ' + tutor_name + ' improve on?"></textarea></div>';

	var s = document.createElement("input"); //input element, Submit button
	s.setAttribute('type',"submit");
	s.setAttribute('value',"Submit Review");
	s.setAttribute('class', 'btn  big-btn  color-bg flat-btn');

	f.appendChild(csrf_field);
	f.appendChild(rating);
	f.appendChild(performance_row);
	f.appendChild(payment_row);
	if (eval_type == "first") {
		f.appendChild(feedback_row_1);	
	}
	
	f.appendChild(feedback_row_2);

	if (eval_type == "end") {
		f.appendChild(feedback_row_3);				
	}
	f.appendChild(s);

	document.getElementById('eval-form').appendChild(f);
	document.getElementById('eval-div').style.display = "block";
}