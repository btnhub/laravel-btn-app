<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
	use SoftDeletes;
    
    protected $table = 'stripe_customers';

    protected $guarded = [
        'id'
    ];
    protected $dates = ['deleted_at'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function payments(){
        return $this->hasMany('App\Payment', 'stripe_customer_id');
    }
}
