<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DanielsFundCharge extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function tutor_session()
    {	
    	return $this->belongsTo('App\TutorSession', 'session_id');
    }

    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function assigned_tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }
}
