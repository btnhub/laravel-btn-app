<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketSubject extends Model
{
    protected $guarded = [
        'id'
    ];

    public function notifications(){
        return $this->hasMany('App\MarketNotification', 'subject_id');
    }

    /**
     * [createSubjectArray create subjects array for the request form
     * @param  [type] $subject_group String desctibing the subject group
     * @return [type]  $subjects             array
     */
    public static function createSubjectArray($subject_group)
    {
        $subjects = MarketSubject::select('id', 'department')
                                    ->where('department', 'like', "$subject_group: %")
                                    ->orderBy('department')
                                    ->pluck('department', 'id');
        foreach ($subjects as $id => $dept) {
            $subjects[$id] = str_replace("$subject_group: ", "", $dept);
        }                           
        return $subjects;
    }
}
