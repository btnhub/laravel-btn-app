<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Assignment;
use App\User;

class MarketTutorRequest extends Model
{
    use SoftDeletes;

    protected $table = 'market_tutor_requests';
    
    protected $guarded = [
        'id'
    ];
	
    /**
     * Accessor for requested_tutors
     */

    public function getRequestedTutorsAttribute($value){
        $req_tutors = explode(', ', $value);
        $tutors_array = [];

        if(preg_match("/[a-z]/i", $value)){
            return $value;
        }
        else{
            foreach ($req_tutors as $key => $tutor_ref_id) {
                $tutor = User::where('ref_id', $tutor_ref_id)->first();
                if ($tutor) {
                    if (auth()->user() && auth()->user()->isSuperUser()) {
                        /*$tutor_profile = route('profile.index', $tutor->ref_id);
                        $tutor_link = "<a href='$tutor_profile' target='_blank'>$tutor->user_name</a>";
                        array_push($tutors_array, $tutor_link);
                        */
                        array_push($tutors_array, $tutor->full_name);
                    }
                    else{
                        array_push($tutors_array, $tutor->user_name);
                    }
                    
                }
            }    
            return implode(', ', $tutors_array);
        }
    }

	/**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at', 'email_reminder', 'sms_reminder'];


    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function requested_tutor(){
        return $this->belongsTo('App\Tutor', 'requested_tutor_id');
    }

    public function assignment(){
        return $this->belongsTo('App\Assignment', 'assignment_id');
    }

    public function tutor_location(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function tutor_proposal()
    {
        return $this->hasMany('App\MarketTutorProposal', 'tutor_request_id');
    }

    public function tutor_contracts(){
        return $this->hasMany('App\TutorContract', 'tutor_request_id');
    }

    /**
     * Tutor Requests for current semester
     * @param  Model $query
     * @return collection       
     */
    public function scopeCurrentSemester($query)
    {   
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 14');
    }

    /**
     * Determine If Tutor Request Is Duplicate And Session(s) Exist
     */
    public function duplicateRequest()
    {   
        $duplicate = Assignment::current()
                                ->where('student_id', $this->student_id)
                                ->where('course', 'like', "%$this->course%")
                                ->has('tutorSessions')
                                ->count();

        if ($duplicate > 0) 
            return true;
        else 
            return false;
    }

    public function nearest_btn_location(){
        $all_locations = Location::whereNotNull('lat')->get();

        $nearest_btn_location = Location::where('city', 'Other')->first();
        $min_distance = 30;

        foreach ($all_locations as $location) {
            $location_distance = $location->map_distance($this->lat, $this->lng);
            
            if ($location_distance < $min_distance) {
                    $nearest_btn_location = $location;
                    $min_distance = $location_distance;
                }    
        }

        return $nearest_btn_location;
    }
}
