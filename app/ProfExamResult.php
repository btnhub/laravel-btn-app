<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfExamResult extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 
    
    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getExamAttribute($value)
    {
        //Name is between - and .csv
	    $ini = strpos($value, "-") + 1;
	 	
	    $len = strpos($value, ".", $ini) - $ini;
	    
	    return ucwords(str_replace("_", " ", substr($value, $ini, $len)));
    }

    /**
     * Relationships
     */
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

}
