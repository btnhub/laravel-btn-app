<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class TutorContract extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'start_date', 'end_date', 'deadline', 'deadline_student', 'tutor_decision_date', 'student_decision_rate'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 
    
    /**
     * Mutators
     */
    public function setDeadlineAttribute($value)
    {
        //Due at 11pm MST on deadline date
        $this->attributes['deadline'] = Carbon\Carbon::parse($value, 'America/Denver')->addHours(23);
    }

    public function setDeadlineStudentAttribute($value)
    {
        //Due at 11pm MST on deadline date
        $this->attributes['deadline_student'] = Carbon\Carbon::parse($value, 'America/Denver')->addHours(23);
    }

    /**
     * Accessors
     */
    
    public function getDeadlineAttribute($value)
    {
        //Due at 11pm MST on deadline date
        return $value ? Carbon\Carbon::parse($value, 'America/Denver') : null;
    }

    public function getDeadlineStudentAttribute($value)
    {
        return $value ? Carbon\Carbon::parse($value, 'America/Denver') : null;
    }

    public function getLocationAttribute()
    {
    	if ($this->online) {
    		return "Online";
    	}
    	else{
    		if ($this->tutor_request->city) {
    			return $this->tutor_request->city . ", " . $this->tutor_request->region;
    		}
    		else{
    			return $this->tutor_request->location;
    		}
    	}
    }

    /**
     * Relationships
     */
    
    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    public function tutor_request(){
        return $this->belongsTo('App\MarketTutorRequest', 'tutor_request_id');
    }

    public function assignment(){
        return $this->hasOne('App\Assignment', 'tutor_contract_id');
    }

    public function serviceFee($tutor_take_home){
    	$service_fees = [20 => 0.25, 30 => 0.30, 55 => 0.35]; // upper bound of tutor take home => service fee

    	ksort($service_fees);

    	foreach ($service_fees as $service_rate_cutoff => $service_fee) {
	
				if ($tutor_take_home < min(array_keys($service_fees))){
					$btn_fee = $service_fee;
					break;
				}
				elseif($tutor_take_home >= $service_rate_cutoff) {
					$btn_fee = $service_fee;
				}
			}	
		return $btn_fee;
    }

    public function filteredTutors($student_max_rate){
		// MIN & MAX ARE TUTOR TAKE HOME RATES
		$tutorA=(object)['level' => 'undergrad', 'min' => 12, 'max' =>15];
		$tutorB=(object)['level' => 'undergrad', 'min' => 20, 'max' =>40];
		$tutorC=(object)['level' => 'B.S.', 'min' => 20, 'max' =>35];
		$tutorD=(object)['level' => 'B.S.', 'min' => 25, 'max' =>50];
		$tutorE=(object)['level' => 'Grad', 'min' => 30, 'max' =>60];
		$tutorE=(object)['level' => 'PhD', 'min' => 50, 'max' =>80];
		$tutors = collect([$tutorA, $tutorB, $tutorC, $tutorD, $tutorE]);
		
		$level1 = (object)['level' => 'undergrad', 'max_rate' =>  20];
		$level2 = (object)['level' => 'B.S.', 'max_rate' =>  35];
		$level3 = (object)['level' => 'Grad', 'max_rate' =>  50];
		$level3 = (object)['level' => 'PhD', 'max_rate' =>  100];
		$levels_rate = collect([$level1, $level2, $level3]);

		$available_tutors = $tutors->filter(function($tutor) use($student_max_rate)
						{
							return $tutor->min <= $student_max_rate * (1-$this->serviceFee($tutor->min));
						});

		foreach ($available_tutors as $avail_tutor) {
			$education_rate_cutoff = $levels_rate->filter(function($level_rate) use($avail_tutor){return $level_rate->level == $avail_tutor->level;})->first()->max_rate;
			
			$tutor_contract_rate = $tutor_current_rate = min($avail_tutor->min, $education_rate_cutoff); // for determining & iterating tutor take home rate

			$btn_fee = $this->serviceFee($tutor_current_rate);
			$student_contract_rate = $student_current_rate = $tutor_current_rate / (1-$btn_fee); //for determing & iterating student rate

			while ($student_current_rate < $student_max_rate) {
				$student_contract_rate = $student_current_rate;

				if ($tutor_current_rate < min($avail_tutor->max, $education_rate_cutoff))
				{
					$tutor_contract_rate = $tutor_current_rate;
					
					$tutor_current_rate++; //increment tutor take home
					
					//Calculate new student rate 
					$btn_fee = $this->serviceFee($tutor_current_rate);
					$student_current_rate = $tutor_current_rate / (1-$btn_fee);
				}

				else
				{
					$tutor_contract_rate = min($avail_tutor->max, $education_rate_cutoff);
					break;
				}
			}

			$avail_tutor->fee = $btn_fee;
			$avail_tutor->education_rate_cutoff = $education_rate_cutoff;
			$avail_tutor->tutor_contract_rate = number_format(floor($tutor_contract_rate/5)*5, 0);
			$avail_tutor->student_contract_rate = number_format(ceil($student_contract_rate/5)*5,0);
			$avail_tutor->btn_percent = number_format(100 * ($avail_tutor->student_contract_rate - $avail_tutor->tutor_contract_rate)/$avail_tutor->student_contract_rate,0);
		}

		return $available_tutors;
    }
}
