<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $guarded = [];
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer', 'stripe_customer_id');
    }
}
