<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Geolocation extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 

    /**
     * Relationships
     */
    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    /**
     * Methods
     */
    function map_distance($lat1, $lng1, $unit = 'miles'){
        //Haversine Formula
        //returns distance in km or miles (default = miles)
        $lat2 = $this->lat;
        $lng2 = $this->lng;
        $earth_radius = 3959; //miles
        
        if ($unit != 'miles') {
            $unit = 'km';
            $earth_radius = 6371; // km
        }

        $lat1_rad = $lat1 * pi()/180;
        $lat2_rad = $lat2 * pi()/180;
        $delta_lat = ($lat2-$lat1) * pi()/180;
        $delta_lng = ($lng2-$lng1) * pi()/180;

        $a = (sin($delta_lat/2) * sin($delta_lat/2)) + cos($lat1_rad) * cos($lat2_rad) * (sin($delta_lng/2) * sin($delta_lng/2));
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));

        $distance = $earth_radius * $c; 

        return $distance;
    }
}
