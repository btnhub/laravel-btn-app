<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Profile;
use App\Evaluation;
use App\Refund;
use App\TutorSession;
use App\Payment;
use App\Assignment;
use App\Role;
use App\Tutor;
use App\Location;

use Carbon;

use Illuminate\Database\Eloquent\SoftDeletes;
use Cmgmyr\Messenger\Traits\Messagable; //For Laravel Messenger

class User extends Authenticatable
{
    use SoftDeletes;
    use Messagable; 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*protected $fillable = [
        'ref_id', 'first_name', 'last_name', 'email', 'phone', 'password',
    ];*/
    protected $guarded = [
        'id', 'remember_token', 
    ];

    /**
    * To allow soft deletes
    */  

    protected $dates = ['deleted_at', 'last_login_date', 'last_login_date_mobile'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    public function getUserNameAttribute()
    {
        return $this->first_name . " " . substr($this->last_name, 0, 1);
    }

    public function getInitialsAttribute()
    {
        return substr($this->first_name, 0, 1) . "." . substr($this->last_name, 0, 1) . ".";
    }

    public function getAvatarURLAttribute()
    {
        $profile = $this->profile;
        $avatar = '';

        if ($profile) {
            $avatar = $profile->fire_avatar ? $profile->fire_avatar : ( $profile->avatar ? asset("btn/avatars/$profile->avatar") : '');
        }

        return $avatar;
    }

    public function owns($related) {
        return $this->id == $related->user_id;
    }

    public function locations(){
        return $this->belongsToMany('App\Location', 'location_user', 'user_id', 'location_id');
    }

    public function profile(){
        return $this->hasOne('App\Profile', 'user_id');
    }

    public function contractorPayments(){
        return $this->hasMany('App\ContractorPayment', 'contractor_id');
    }
    
    public function customer(){
        return $this->hasOne('App\Customer', 'user_id');
    }

    public function stripeTutor(){
        return $this->hasOne('App\StripeConnectTutor', 'user_id');
    }

    public function payments(){
        return $this->hasMany('App\Payment', 'user_id');
    }

    public function prepayments(){
        return $this->hasMany('App\Prepayment', 'user_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_user', 'user_id', 'role_id');
    }

    public function tutorApplicant(){
        return $this->hasOne('App\TutorApplicant', 'user_id');
    }

    public function backgroundCheck(){
        return $this->hasOne('App\BackgroundCheck', 'user_id');
    }

    public function favorites(){
        return $this->hasMany('App\Favorite', 'user_id');
    }

    public function apiTokens(){
        return $this->hasMany('App\ApiToken', 'user_id');
    }

    public function activeFCMTokens(){
        return $this->hasMany('App\ApiToken', 'user_id')
                    ->where('type', 'fcm')
                    ->where('revoked', NULL);
    }

    public function schools(){
        return $this->belongsToMany('App\School', 'school_user', 'user_id', 'school_id')
                    ->withPivot('default', 'major', 'major_details', 'degree', 'gpa', 'level', 'start_date', 'end_date', 'graduation_date', 'verification_date', 'access_revoked')
                    ->withTimestamps();
    }

    public function courses(){
        return $this->belongsToMany('App\Course', 'course_user', 'user_id', 'course_id')
                    ->withPivot('start_date', 'end_date', 'verification_date', 'access_revoked')
                    ->withTimestamps();
    }

    public function review_sessions(){
        return $this->belongsToMany('App\ReviewSession')
                        ->withTimestamps()
                        ->withPivot('ref_id', 'identifier', 'first_name', 'last_name', 'email', 'payment_id', 'stripe_payment_id', 'refund_id', 'stripe_refund_id', 'price', 'discount', 'amount_charged', 'stripe_fee', 'tutor_pay', 'btn_pay', 'tip_payment_id', 'tip', 'tip_stripe', 'tip_tutor', 'tip_btn', 'assist_with_reservation', 'assistance_discount', 'assistance_discount_issued', 'saved','voted','registered', 'cancellation_deadline', 'cancelled', 'waitlisted', 'removed', 'rating', 'evaluation_reminder', 'tutor_strength', 'tutor_weakness', 'website_reference', 'private_session', 'eval_reviewed', 'notes', 'admin_notes', 'deleted_at')
                        ->wherePivot('deleted_at', NULL);
    }

    public function saved_reviews(){
        return $this->review_sessions()
                    ->wherePivot('saved', '!=', NULL);
    }

    public function voted_reviews(){
        return $this->review_sessions()
                    ->wherePivot('voted', '!=', NULL);
    }

    public function registered_reviews(){
        return $this->review_sessions()->wherePivot('registered', '!=', NULL)
                                        ->wherePivot('cancelled', NULL)
                                        ->wherePivot('removed', NULL)
                                        ->wherePivot('refund_id', NULL)
                                        ->wherePivot('stripe_refund_id', NULL);
    }

    public function cancelled_reviews(){
        return $this->review_sessions()->wherePivot('registered', '!=', NULL)
                                        ->wherePivot('cancelled', '!=', NULL)
                                        ->wherePivot('removed', NULL);
    }

    public function waitlisted_reviews(){
        return $this->review_sessions()->wherePivot('waitlisted', '!=', NULL)
                                        ->wherePivot('removed', NULL);
    }

    public function removed_reviews(){
        return $this->review_sessions()
                    ->wherePivot('removed', '!=', NULL);
    }

    //Sessions of active assignments (including declined assignments)
    public function activeTutorSessions()
    {
        return $this->tutorSessions()
                    ->whereHas('assignment', function($query) {
                        $query->active();
                        })
                    ->orderBy('id', 'DESC');
    }

    public function hasRole($roles) 
    { 
        if (is_array($roles)) {
            foreach ($roles as $role) 
            {
                if ($this->roles->contains('role', $role))
                    return true;
            }
        }
        
        return $this->roles->contains('role', $roles);
    }

    public function isSuperUser()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }

        return $this->hasRole('super-user');
    }

    public function isTutor()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }

        return $this->hasRole(['tutor', 'test-tutor', 'marketplace-tutor', 'music-teacher']);
    }

    public function isStudent()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }
        
        return $this->hasRole(['student', 'test-student', 'music-student']);
    }

    public function isReviewTutor()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }

        return $this->hasRole(['review-tutor']);
    }

    public function isReviewStudent()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }
        
        return $this->hasRole(['review-student']);
    }

    public function isStaff()
    {
        if ($this->hasRole(['banned'])) 
        {
            return false;
        }
        
        return $this->hasRole(['staff', 'admin', 'super-user']);
    }

    public function isActiveTutor(){
        return Tutor::activeTutors()->where('id', $this->id)->count() ? true : false;
    }

    public function addRole($role) 
    { 
        if (!$this->hasRole($role)) {
            return $this->roles()->save(Role::whereRole($role)->firstOrFail());     
        }  
    }

    public function removeRole($role) 
    { 
        if ($this->hasRole($role)) {
            $role_id = Role::whereRole($role)->firstOrFail();
            return $this->roles()->detach($role_id);     
        }  
    }

    public function addLocation($location_id){
        return $this->locations()->syncWithoutDetaching([$location_id]);
    }

    public function removeLocation($location_id){
        return $this->locations()->detach($location_id);
    }

    public function nearest_btn_location(){
        $all_locations = Location::whereNotNull('lat')->get();

        $nearest_btn_location = Location::where('city', 'Other')->first();
        $min_distance = 30;

        if ($this->lat) {
            foreach ($all_locations as $location) {
                $location_distance = $location->map_distance($this->lat, $this->lng);

                if ($location_distance < $min_distance) {
                        $nearest_btn_location = $location;
                        $min_distance = $location_distance;
                    }    
            }
        }
        
        return $nearest_btn_location;
    }

    public function formatPhoneNumber(){
        //RETURN E.164 # for Firebase Storage
        if ($this->phone) {
            $characters = [' ', '-', '.', '+', '(', ')'];

            $country_code = $this->phone[0] == '1' ? '' : "1";

            return "+$country_code" . str_replace($characters, '', $this->phone);
        }

        return null;
    }
}

