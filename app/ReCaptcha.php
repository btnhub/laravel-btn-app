<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReCaptcha extends Model
{
    public function verify($g_recaptcha_response){
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.config('services.google_recaptcha.secret').'&response='.$g_recaptcha_response);

            $responseData = json_decode($verifyResponse); 

            return $responseData->success;
    }
}
