<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Location;

class University extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'start_date', 'end_date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Relationships
     */
    public function location(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function departments(){
        return $this->hasMany('App\UniversityDepartment', 'university_id');
    }

    public function courses(){
        return $this->hasMany('App\UniversityCourse', 'university_id');
    }
    
    /**
     * Methods
     */
    
    public function nearest_btn_location(){
        $all_locations = Location::whereNotNull('lat')->get();

        $nearest_btn_location = Location::where('city', 'Other')->first();
        $min_distance = 30;

        if ($this->lat) {
            foreach ($all_locations as $location) {
                $location_distance = $location->map_distance($this->lat, $this->lng);

                if ($location_distance < $min_distance) {
                        $nearest_btn_location = $location;
                        $min_distance = $location_distance;
                    }    
            }
        }
        
        return $nearest_btn_location;
    }

    /*public function currentSemester(){
        //current semester
        $semester = $this->semesters->where('start_date', '<', Carbon\Carbon::now())
                        ->where('end_date', '>', Carbon\Carbon::now())
                        ->first();

        //if in between semesters, get the upcoming semester
        if (!$semester) {
            $semester = $this->semesters->where('start_date', '>', Carbon\Carbon::now())
                            ->orderBy('start_date')
                            ->first();
        }

        return $semester;
    }

    public function nextSemesters(){
        $nextSemesters = $this->semesters->where('start_date', '>', $this->currentSemester()->start_date)
                                        ->orderBy('start_date')
                                        ->limit(3)
                                        ->get();
        return $nextSemesters;
    }*/
}
