<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketTutorProposal extends Model
{
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    public function thread(){
        return $this->belongsTo('App\Thread', 'msg_thread_id');
    }

    public function assignment(){
        return $this->belongsTo('App\Assignment', 'assignment_id');
    }

    public function tutor_request(){
        return $this->belongsTo('App\MarketTutorRequest', 'tutor_request_id');
    }

    public function location(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    //Proposals corresponding to tutor requests of this semester
    public function scopeCurrentSemester()
    {
        return $this->whereHas('tutor_request', function($query) {
                        $query->currentSemester();
                        });
    }
}
