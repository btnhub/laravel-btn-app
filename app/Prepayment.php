<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prepayment extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'completed_at', 'refund_requested', 'refund_processed'];

	protected $guarded = [];
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer', 'stripe_customer_id');
    }

    public function assignment(){
        return $this->belongsTo('App\Assignment', 'assignment_id');
    }

    /**
     * Methods
     */
    
}
