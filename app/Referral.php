<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    public function existing_student(){
        return $this->belongsTo('App\Student', 'existing_student');
    }

    public function referred_student(){
        return $this->belongsTo('App\Student', 'existing_student');
    }
}
