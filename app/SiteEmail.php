<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteEmail extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'responded'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Relationships
     */
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
