<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackgroundCheck extends Model
{
    protected $guarded = ['id'];

    protected $dates = [
    	'payment_date',
    	'cleared_date'
    ];
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
