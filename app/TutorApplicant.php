<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use DB;

class TutorApplicant extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'submitted'];

	protected $table = 'tutor_applicants';

	protected $guarded = [
        'id'
    ];

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }

    /**
     * Relationships
     */
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function student_references(){
        return $this->hasMany('App\StudentReference', 'tutor_applicant_id');
    }

    public function proficiency_exams(){
        return $this->hasMany('App\ProfExamResult', 'user_id', 'user_id');
    }

    /**
     * Methods
     */

    public function exam_results(){
    	$results = null;

    	if ($this->user) {
    		$results = DB::table('prof_exam_results')->where('user_id', $this->user->id)->get();
    	}
    	
    	else {
    		$user = User::where('email', 'like', $this->email)->first();
    		if ($user) {
    			$results = DB::table('prof_exam_results')->where('user_id', $user->id)->get();
    		}
    	}
    	return $results;
    }

    public function exam_list(){
        $exam_path = base_path("bufftutor/proficiency_exams/Tutor_Proficiency_Exams");

        if ($dh = opendir($exam_path)){

            $exam_files = array();
            $dept = array();
            $exam_list = array();

            while (($file = readdir($dh)) !== false){
                $exam_files[] = $file;
                $dept[] = strchr($file, "-", true);
            }//while

            closedir($dh);
              
            // Sort Exam File Names Alphabetically and then display them in the form
            sort($exam_files);
            $dept = array_unique($dept);
            $i = 0;
            foreach ($exam_files as $exam_name) {
                $exam_dept = strchr($exam_name, "-", true);
                if(strchr($exam_name, ".csv") !== false){    
                    $exam_name_edited = strchr(ucwords(str_replace("_", " ", substr(strchr($exam_name, "-"),1))), ".csv", true);
                    // Strips course topic (characters before -, e.g Chemistry), removes -, replaces _ with spaces, Capitalizes first letter of each word, remove .csv extension 
                    $exam_list[$exam_dept][$exam_name] = $exam_name_edited;
                }
            }// foreach
        }//if

        return $exam_list;
    }
}
