<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class MarketNotification extends Model
{
    
	use SoftDeletes;

    protected $guarded = [
        'id'
    ];
	
	/**
    * To allow soft deletes
    */  
    protected $dates = ['deleted_at'];


    public function subject(){
        return $this->belongsTo('App\MarketSubject', 'subject_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Tutor');
    }

    public function location(){
        return $this->belongsTo('App\Location');
    }
}
