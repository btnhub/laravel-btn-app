<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 
    ];


    public function users(){
        return $this->belongsToMany('App\User');
    }

    public function tutors(){
    	return $this->belongsToMany('App\Tutor', 'location_user', 'location_id','user_id');
    }

    public function active_tutors(){

    	return $this->tutors()->activeTutors();
    }
   
    public function universities(){
        return $this->hasMany('App\University', 'location_id');
    }

    public function assignments(){
        return $this->hasMany('App\Assignment', 'location_id');
    }

    public function active_assignments(){

        return $this->assignments()->active();
    }

   	public function scopeStudentsVisible($query){   
        //Locations visible to students
        return $query->where('students_visible', 1)
                        ->orderBy('website_order');
    } 
    public function scopeTutorsVisible($query){   
        //Locations visible to tutor applicants
        return $query->where('tutors_visible', 1)
                        ->orderBy('website_order');
    } 

    function map_distance($lat1, $lng1, $unit = 'miles'){
        //Haversine Formula
        //returns distance in km or miles (default = miles)
        $lat2 = $this->lat;
        $lng2 = $this->lng;
        $earth_radius = 3959; //miles
        
        if ($unit != 'miles') {
            $unit = 'km';
            $earth_radius = 6371; // km
        }

        $lat1_rad = $lat1 * pi()/180;
        $lat2_rad = $lat2 * pi()/180;
        $delta_lat = ($lat2-$lat1) * pi()/180;
        $delta_lng = ($lng2-$lng1) * pi()/180;

        $a = (sin($delta_lat/2) * sin($delta_lat/2)) + cos($lat1_rad) * cos($lat2_rad) * (sin($delta_lng/2) * sin($delta_lng/2));
        $c = 2 * atan2(sqrt($a), sqrt(1-$a));

        $distance = $earth_radius * $c; 

        return $distance;
    }
}
