<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class DatabaseBackup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buff:db-backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup Of bufftutor2016 Database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*Localhost Variables*/
        /*
        $destination = "bufftutor/db_backups"; //Local destination */

        /* Live Variables */
        $username = config('database.connections.mysql.username');
        $password = config('database.connections.mysql.password');
        $dbname = config('database.connections.mysql.database'); 
        $destination = "bufftutor.com/bufftutor/db_backups"; //Live destination
        
        $filename = $dbname."-".date("Ymd").".sql";

        exec("mysqldump -u $username -p$password $dbname > $destination/$filename");  
    }
}
