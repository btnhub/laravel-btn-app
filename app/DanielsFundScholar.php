<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DanielsFundScholar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function assigned_tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    public function assignments()
    {	
    	return $this->hasMany('App\Assignment', 'id', 'assignment_id');
    }

    public function session_charges()
    {	
    	return $this->hasMany('App\DanielsFundCharge', 'assignment_id', 'assignment_id');
    }

}
