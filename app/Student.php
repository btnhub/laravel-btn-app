<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

//class Student extends Model
class Student extends User
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Relationships
     */
    
    public function payments()
    {
    	return $this->hasMany('App\Payment', 'user_id');	
    }
    
   	public function assignments()
    {	
    	return $this->hasMany('App\Assignment', 'student_id');
    }

    public function danielsFundAssignments()
    {   
        return $this->hasMany('App\DanielsFundScholar', 'student_id');
    }

    public function tutorSessions()
    {
    	return $this->hasMany('App\TutorSession', 'student_id');
    }

    public function tutorRequests()
    {
        return $this->hasMany('App\MarketTutorRequest', 'student_id');
    }

    public function refunds()
    {
    	return $this->hasMany('App\Refund', 'student_id')
    				->orderBy('id', 'DESC');
    }

    public function evaluations()
    {
    	return $this->hasMany('App\Evaluation', 'student_id');
    }

    public function referrals()
    {
        return $this->hasMany('App\Referral', 'existing_student');
    }

    public function danielsFundScholar()
    {
        return $this->hasMany('App\DanielsFundScholar', 'student_id');
    }

    /**
     * Calculate student's current balance
     * @return number Student's account balance
     */
    public function studentBalance()
    {
        $daniels_fund_charges = DanielsFundCharge::where('student_id', $this->id)->sum('amt_charged');
        $session_charges = $this->tutorSessions->sum('amt_charged');

        $student_charges = $session_charges - $daniels_fund_charges; //Charges student is responsible for = Total charges - Scholarship charges

        $balance = $this->payments->where('paid', 1)->sum('amount') + $this->refunds->where('issue_refund', 1)->sum('refunded_to_account') - ($student_charges + $this->refunds->where('issue_refund', 1)->sum('refunded_to_student'));

        return number_format($balance, 2);
    }

    /**
     * Finds all unpaid sessions of the student
     * @return collection   Collection of all unpaid sessions
     */
    public function unpaidSessions()
    {
        $account_balance = $this->studentBalance();
        $student_sessions = $this->tutorSessions->sortByDesc('id');

        $unpaid_sessions = collect([]);

        if ($account_balance < 0) {
            $adj_balance = $account_balance; 

                foreach ($student_sessions as $session) {
                    $unpaid_sessions->push($session);
                    $adj_balance += $session->amt_charged;
                    
                    if ($adj_balance >=0) {
                        break;
                    }
                }
        }

        return $unpaid_sessions;
    }   

}
