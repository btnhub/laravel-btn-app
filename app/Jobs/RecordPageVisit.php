<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\PageVisit;
use Carbon;

class RecordPageVisit extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $user_id, $identifier, $ip, $url, $url_previous, $referer, $browser;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_id, $identifier, $ip, $url, $url_previous, $referer, $browser)
    {
        $this->user_id = $user_id;
        $this->identifier = $identifier;
        $this->ip = $ip;
        $this->url = $url;
        $this->url_previous = $url_previous;
        $this->referer = $referer;
        $this->browser = $browser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user_id = $this->user_id;
        $identifier = $this->identifier;
        $ip = $this->ip;
        $url = $this->url;
        $url_previous = $this->url_previous;
        $referer = $this->referer;
        $browser = $this->browser;

        $role = $city = $region = $country = $postal_code = $lat = $lng = $notes = null;

        if ($user_id) {
            $user = User::find($user_id) ?? null;
            if ($user) {
                $role = $user->roles->pluck('role')->implode(', ');
            }
        }

        //Location data based on IP
        if ($ip) {
            $all_page_visits = PageVisit::where('ip', $ip)->get();
            
            if ($all_page_visits->count()) {
                $page_visit = $all_page_visits->first();
                //reuse location data
                $city = $page_visit->city;
                $region = $page_visit->region;
                $country = $page_visit->country;
                $postal_code = $page_visit->postal_code;
                $lat = $page_visit->lat;
                $lng = $page_visit->lng;
            }

            else{
                //Retrieve location data
                $access_key = config('services.ipstack.key');
                
                // Initialize CURL:
                $ch = curl_init("http://api.ipstack.com/$ip?access_key=$access_key");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Store the data:
                $json = curl_exec($ch);
                curl_close($ch);

                // Decode JSON response:
                $api_result = json_decode($json, true); 

                $city = $api_result['city'] ?? null;
                $region = $api_result['region_code'] ?? null;
                $country = $api_result['country_code'] ?? null;
                $postal_code = $api_result['zip'] ?? null;
                $lat = $api_result['latitude'] ?? null;
                $lng = $api_result['longitude'] ?? null;
                $notes = (isset($api_result['error']) ? $api_result['error']['type'] .". ". $api_result['error']['info'] : null);
            }

            //find recent visits
            $recent_page_visit = PageVisit::where('identifier', $identifier)
                                    ->where('url', $url)
                                    ->where('url_previous', $url_previous)
                                    ->first();

            if ($recent_page_visit) {
                $recent_page_visit->increment('count');
            }
            else{
                PageVisit::create([
                    'user_id' => $user_id,
                    'identifier' => $identifier,
                    'role' => $role,
                    'count' => 1,
                    'url' => $url,
                    'url_previous' => $url_previous,
                    'referer' => $referer,
                    'browser' => $browser,
                    'ip' => $ip,
                    'city' => $city,
                    'region' => $region,
                    'country' => $country,
                    'postal_code' => $postal_code,
                    'lat' => $lat,
                    'lng' => $lng,
                    'notes' => $notes
                    ]);
            }
        } 
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        /*$user = $this->user;
        $location = $this->location;
        $subject = $this->subject;
        $email_view = $this->email_view;
        $email_data = $this->email_data; //array

        Mail::send($email_view, $email_data, function ($m) use ($user, $location, $subject) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("SendGeneralNotification Failed: To $user->email");
                 });*/
     }
}
