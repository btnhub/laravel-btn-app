<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Location;
use Nexmo;
use Mail;

class SendSMSAlert extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $user;
    public $location;
    public $text_body;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Location $location, $text_body)
    {
        $this->user = $user;
        $this->location = $location;
        $this->text_body = $text_body;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $location = $this->location;
        $text_body = $this->text_body;

        $to = \App::environment('production') ? $user->phone : config('services.nexmo.test_phone');
        $to = preg_replace('/\D/', '', $to);

        if (substr($to, 0, 1) != 1) {
            $to = "1" . $to;
        }

        $message = Nexmo::message()->send([
                    'to' => $to,
                    'from' => $location->virtual_phone,
                    'text' => $text_body
                ]);
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $user = $this->user;
        $location = $this->location;
        $text_body = $this->text_body;
        
        Mail::raw($text_body, function ($m) use ($user, $location) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("SendSMSAlert Failed: To $user->full_name ($user->phone)");
                 });
     }
}
