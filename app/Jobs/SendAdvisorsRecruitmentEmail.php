<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Location;
use App\AcademicAdvisor;
use Mail;
use Carbon;

class SendAdvisorsRecruitmentEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $advisor;
    public $location;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(AcademicAdvisor $advisor, Location $location)
    {
        $this->advisor = $advisor;
        $this->location = $location;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $advisor = $this->advisor;
        $location = $this->location;
        
        $logo = null;
        if ($location->city == "Fort Collins") {
            $logo = asset('btn/logo/RamTutor_Logo.png');
        }

        $email_data = ['location' => $location, 'logo' => $logo];
         
        $email_view = "btn.emails.admin.advisors_recruit";
        $subject = "Tutoring Positions For Students";

        Mail::send($email_view, $email_data, function ($m) use ($advisor, $location, $subject) {
                $m->from($location->email, $location->company_name);

                $m->to($advisor->email)->subject($subject);
            }); 
        $advisor->last_contacted = Carbon\Carbon::now();
        $advisor->save();
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $advisor = $this->advisor;
        $location = $this->location;
        
        $logo = null;
        if ($location->city == "Fort Collins") {
            $logo = asset('btn/logo/RamTutor_Logo.png');
        }

        $email_data = ['location' => $location, 'logo' => $logo];         
        $email_view = "btn.emails.admin.advisors_recruit";

        Mail::send($email_view, $email_data, function ($m) use ($location, $advisor) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("AdvisorRecruitment Failed: $advisor->email");
                 });
     }
}
