<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use Mail;
use Carbon;
use App\User;
use App\Role;
use App\Profile;
use App\Location;

use Illuminate\Foundation\Bus\DispatchesJobs;

class CreateNewUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    public $user;
    public $location;
    public $role;
    public $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Location $location, Role $role, $data)
    {
        $this->user = $user;
        $this->location = $location;
        $this->role = $role;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $location = $this->location;
        $role = $this->role;
        $data = $this->data;

        /*  NOW HANDLED IN AuthController
        DB::table('terms_of_use')->insert([
            'user_id' => $user->id,
            'version' => 'terms_of_use',
            'submission_date' => Carbon\Carbon::now()
            ]);
        
        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => $role->id,
            ]);

        DB::table('location_user')->insert([
            'user_id' => $user->id,
            'location_id' => $location->id,
            ]);
        */
       
        /*foreach ($data['marketing'] as $key => $value) {
            DB::table('marketing')->insert([
                    'user_id' => $user->id,
                    'role' => $role->role,
                    'method' => $value,
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now()
                ]);
        }*/

        //Add Referrals
        if ($data['referral_code'] ?? '') {
            if ($existing_student = User::where('ref_id', $data['referral_code'])->first()) {
                $existing_student_id = $existing_student->id;    
            
                DB::table('referrals')->insert([
                    'new_student' => $user->id,
                    'existing_student' => $existing_student_id,
                    'credit' => 5,
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now()
                ]);    
            }     
        }

        $user_role = ucfirst($data['role']);
        $subject = "New $user_role Registered";
        $email_view = "btn.emails.admin.new_user_registered";
        $email_data = ['user' => $user];

        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));
        
        //Get User Location And Update Profile
        /*$city = $state = $country = $ip = null;

        $profile = Profile::find($user->id);

        if ($_SERVER['HTTP_CLIENT_IP'])
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        else if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if($_SERVER['HTTP_X_FORWARDED'])
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        else if($_SERVER['HTTP_FORWARDED_FOR'])
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        else if($_SERVER['HTTP_FORWARDED'])
            $ip = $_SERVER['HTTP_FORWARDED'];
        else if($_SERVER['REMOTE_ADDR'])
            $ip = $_SERVER['REMOTE_ADDR'];
        else
            $ip = "127.0.0.1"; //by default, set IP to localhost

        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

        if (property_exists($details, 'city')) {
            $city = $details->city;
            $state = $details->region;
            $country = $details->country;
        }

        $profile->ip = $ip;
        $profile->city = $city;
        $profile->state = $state;
        $profile->country = $country;
        $profile->save();*/
    }
    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $user = $this->user;
        $location = $this->location;
        $role = $this->role;
        $data = $this->data;
        
        $email_view = "btn.emails.admin.new_user_registered";
        $email_data = ['user' => $user];

        Mail::send($email_view, $email_data, function ($m) use ($location, $user, $role) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("CreateNewUser Failed: $user->full_name, $user->id, $location->city, $role->role, ");
                 });
     }
}
