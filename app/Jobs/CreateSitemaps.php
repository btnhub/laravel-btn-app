<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Location;
use App\University;


use DB;
use Carbon\Carbon;

class CreateSitemaps extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * CUSTOM: Ping Webmasters
     */
    
    public function pingWebmasters($sitemaps_array){
        $webmasters = [
                        'google' => "http://www.google.com/webmasters/sitemaps/ping?sitemap=",
                        'bing_yahoo' => "http://www.bing.com/webmaster/ping.aspx?siteMap="
                    ];

        foreach ($webmasters as $key => $webmaster) {
            foreach ($sitemaps_array as $sitemap) {
                $url = $webmaster.$sitemap;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                // $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);
            }
        }
        //return $httpCode;
        return true;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sitemaps_array = [];
        $locations = Location::studentsVisible()->get();
        $states = array_filter(array_unique($locations->pluck('state')->toArray()));
        $universities = University::where('visible', 1)->with('courses')->get();

        $tutor_variations = ['tutors', 'online_tutoring' /*, 'tutoring'*/];
        $courses_array = DB::table('sitemap_courses')->where('active', 1)->get();

        $folder = public_path("sitemaps");
        if (!file_exists($folder)) {
            mkdir($folder, 0775);
        }   

        $xml_header = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';

        //Sitemap Index
        $sitemap_index = fopen(public_path()."/sitemap_index.xml", "w");
        fwrite($sitemap_index, '<?xml version="1.0" encoding="UTF-8"?>
                                    <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
                                        <sitemap>
                                            <loc>'.url('/sitemap.xml').'</loc>
                                            <lastmod>'.Carbon::now()->format('Y-m-d').'</lastmod>
                                        </sitemap>');
        
        //General Sitemap (No City)
        $sitemap_name = "general_sitemap.xml";
        $general_file = fopen("$folder/$sitemap_name", "w");
        
        fwrite($general_file, $xml_header);
        
        // array_push($tutor_variations, "tutors_near_me");

        foreach ($tutor_variations as $tutor_variation) {
            fwrite($general_file,"
                            <url>
                                <loc>".route('home.general', $tutor_variation).
                                "</loc>
                                <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                                <priority>1.0</priority>
                            </url>");
            foreach ($courses_array as $details) {
                $course = strtolower(str_replace(" ", "_", trim($details->course)));
                fwrite($general_file,"<url>
                        <loc>".route('home.general', "$course-$tutor_variation")."</loc>
                        <changefreq>daily</changefreq>
                        <priority>1.0</priority>
                        </url>");
            }
        }

        // array_pop($tutor_variations);

        fwrite($general_file, '</urlset>');
        fclose($general_file);

        //Add to sitemap_index.xml
        $sitemap_url = url("sitemaps/$sitemap_name");
        fwrite($sitemap_index, "<sitemap>
                                    <loc>".$sitemap_url."</loc>
                                    <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                                </sitemap>");

        array_push($sitemaps_array, $sitemap_url);

        //COURSES TUTORED w/ UNI PAGES SITEMAP
        $sitemap_name = "courses_tutored_sitemap.xml";
        $courses_tutored_file = fopen("$folder/$sitemap_name", "w");

        fwrite($courses_tutored_file, $xml_header);

        foreach ($universities as $uni) {  
            $uni_names_array = [trim($uni->name)];

            if ($uni->alt_names) {
                $more_names = explode(";", $uni->alt_names);
                $uni_names_array = array_merge($uni_names_array, $more_names);
            }

            foreach ($uni_names_array as $uni_name) {
                $uni_name = strtolower(str_replace(" ", "_", trim($uni_name)));

                fwrite($courses_tutored_file,"<url>
                                <loc>".route('university.courses', $uni_name)."</loc>
                                <changefreq>daily</changefreq>
                                <priority>1.00</priority>
                                </url>");
            }
        }

        fwrite($courses_tutored_file, '</urlset>');
        fclose($courses_tutored_file);
        
        //Add to sitemap_index.xml
        $sitemap_url = url("sitemaps/$sitemap_name");
        fwrite($sitemap_index, "<sitemap>
                                    <loc>".$sitemap_url."</loc>
                                    <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                                </sitemap>");

        array_push($sitemaps_array, $sitemap_url);

        //State Sitemaps
        foreach ($states as $state) {   
            $sitemap_name = strtolower($state)."_sitemap.xml";
            $file = fopen("$folder/$sitemap_name", "w");

            fwrite($file, $xml_header);

            foreach ($locations->where('state', $state) as $location) { 
                $city = strtolower(str_replace(" ", "_", trim($location->city)));
                $state = strtolower($state);

                fwrite($file,"<url>
                                <loc>".route('city.courses', [$city, $state])."</loc>
                                <changefreq>daily</changefreq>
                                <priority>1.00</priority>
                                </url>");

                foreach ($tutor_variations as $tutor_variation) {
                    fwrite($file,"<url>
                                    <loc>".route('home.city', "$city-$state-$tutor_variation")."</loc>
                                    <changefreq>daily</changefreq>
                                    <priority>1.00</priority>
                                    </url>");
                    foreach ($courses_array as $details) {
                        $course = strtolower(str_replace(" ", "_", trim($details->course)));
                        fwrite($file,"<url>
                                <loc>".route('home.city', "$city-$state-$course-$tutor_variation")."</loc>
                                <changefreq>daily</changefreq>
                                <priority>1.00</priority>
                                </url>");
                    }
                }
            }
            fwrite($file, '</urlset>');
            fclose($file);

            //Add to sitemap_index.xml
            $sitemap_url = url("sitemaps/$sitemap_name");
            fwrite($sitemap_index, "<sitemap>
                                        <loc>".$sitemap_url."</loc>
                                        <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                                    </sitemap>");

            array_push($sitemaps_array, $sitemap_url);

            //Create College Sitemaps
            $sitemap_name = strtolower($state)."_colleges_sitemap.xml";
            $file = fopen("$folder/$sitemap_name", "w");

            fwrite($file, $xml_header);

            foreach ($universities->where('state_abbr', strtoupper($state)) as $uni) {  
                $uni_names_array = [trim($uni->name)];

                if ($uni->alt_names) {
                    $more_names = explode(";", $uni->alt_names);
                    $uni_names_array = array_merge($uni_names_array, $more_names);
                }

                foreach ($uni_names_array as $uni_name) {
                    $uni_name = strtolower(str_replace(" ", "_", trim($uni_name)));

                    foreach ($tutor_variations as $tutor_variation) {
                        fwrite($file,"<url>
                                        <loc>".route('home.college', "$uni_name-$tutor_variation")."</loc>
                                        <changefreq>daily</changefreq>
                                        <priority>1.00</priority>
                                        </url>");
                        foreach ($courses_array as $details) {
                            $course = strtolower(str_replace(" ", "_", trim($details->course)));
                            fwrite($file,"<url>
                                    <loc>".route('home.college', "$uni_name-$course-$tutor_variation")."</loc>
                                    <changefreq>daily</changefreq>
                                    <priority>1.00</priority>
                                    </url>");
                        }
                    }
                }

                //Create Courses Sitemaps
                $uni_courses = $uni->courses()->where('visible', 1)->get();
                
                if ($uni_courses->count()) {
                    $uni_sitemap_name = strtolower(str_replace(" ", "_", trim($uni->name)))."_courses_sitemap.xml";
                    $courses_file = fopen("$folder/$uni_sitemap_name", "w");
                    fwrite($courses_file, $xml_header);
                    
                    foreach ($uni_courses as $uni_course) {
                        $uni_course_number = strtolower(str_replace(" ", "_", trim($uni_course->course_number)));
                        
                        fwrite($courses_file,"<url>
                                <loc>".route('home.course', [$uni_course->id, strtolower(str_replace([" ", "&"], "_",trim($uni_course->course_number))."-tutors-".str_replace(" ", "_", trim($uni_course->university->name)))])."</loc>
                                <changefreq>daily</changefreq>
                                <priority>1.00</priority>
                                </url>");
                    }

                    fwrite($courses_file, '</urlset>');
                    fclose($courses_file);
                    //Add to sitemap_index.xml
                    $sitemap_url = url("sitemaps/$uni_sitemap_name");
                    fwrite($sitemap_index, "<sitemap>
                                                <loc>".$sitemap_url."</loc>
                                                <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                                            </sitemap>");

                    array_push($sitemaps_array, $sitemap_url);
                }
            }

            fwrite($file, '</urlset>');
            fclose($file);

            //Add to sitemap_index.xml
            $sitemap_url = url("sitemaps/$sitemap_name");
            fwrite($sitemap_index, "<sitemap>
                                        <loc>".$sitemap_url."</loc>
                                        <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                                    </sitemap>");

            array_push($sitemaps_array, $sitemap_url);

        }

        fwrite($sitemap_index, ' </sitemapindex>');
        fclose($sitemap_index);
        array_push($sitemaps_array, url("sitemap_index.xml"));
        array_push($sitemaps_array, url("sitemap.xml"));
        $this->pingWebmasters($sitemaps_array);
    }

    public function failed()
     {
        /*$user = $this->user;
        $location = $this->location;
        $subject = $this->subject;
        $email_view = $this->email_view;
        $email_data = $this->email_data; //array

        Mail::send($email_view, $email_data, function ($m) use ($user, $location, $subject) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("SendGeneralNotification Failed: To $user->email");
                 });*/
     }
}
