<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;
use App\Assignment;
use App\Location;

class NotifyStudentOfAssignment extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $assignment;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Assignment $assignment)
    {
        $this->assignment = $assignment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $assignment = $this->assignment;
        $location = Location::find($assignment->location_id);
        
        Mail::send('btn.emails.students.assignment_created', ['assignment' => $assignment], function ($m) use ($assignment, $location) {
            $m->from($location->email, $location->company_name);

            $m->to($assignment->student->email, $assignment->student->full_name)->subject('Tutor For '.$assignment->course);
        });
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $assignment = $this->assignment;
        $location = Location::find($assignment->location_id);
        
        Mail::send('btn.emails.students.assignment_created', ['assignment' => $assignment], function ($m) use ($assignment, $location) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("NotifyStudentOfAssignment Failed: To {$assignment->student->email}");
                 });
     }
}
