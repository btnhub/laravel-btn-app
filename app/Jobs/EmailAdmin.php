<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Location;
use Mail;

class EmailAdmin extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $location;
    public $subject;
    public $email_view;
    public $email_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Location $location, $subject, $email_view, $email_data)
    {
        $this->location = $location;
        $this->subject = $subject;
        $this->email_view = $email_view;
        $this->email_data = $email_data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $location = $this->location;
        $subject = $this->subject;
        $email_view = $this->email_view;
        $email_data = $this->email_data; //array
         

        Mail::send($email_view, $email_data, function ($m) use ($location, $subject) {
                $m->from($location->email, $location->company_name);

                $m->to($location->email, $location->company_name)->subject($subject);
            }); 
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $location = $this->location;
        $subject = $this->subject;
        $email_view = $this->email_view;
        $email_data = $this->email_data; //array
         

        Mail::send($email_view, $email_data, function ($m) use ($location) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("EmailAdmin Failed");
                 });
     }
}
