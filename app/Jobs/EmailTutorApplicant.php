<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\TutorApplicant;
use App\Location;
use Mail;

class EmailTutorApplicant extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $applicant;
    public $location;
    public $subject;
    public $email_view;
    public $email_data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TutorApplicant $applicant, Location $location, $subject, $email_view, $email_data)
    {
        $this->applicant = $applicant;
        $this->location = $location;
        $this->subject = $subject;
        $this->email_view = $email_view;
        $this->email_data = $email_data; // array
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $applicant = $this->applicant;
        $location = $this->location;
        $subject = $this->subject;
        $email_view = $this->email_view;
        $email_data = $this->email_data; //array
         

        Mail::send($email_view, $email_data, function ($m) use ($applicant, $location, $subject) {
                $m->from($location->email, $location->company_name);

                $m->to($applicant->email, $applicant->first_name." ".$applicant->last_name)->subject($subject);
            }); 
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $applicant = $this->applicant;
        $location = $this->location;
        $subject = $this->subject;
        $email_view = $this->email_view;
        $email_data = $this->email_data; //array
         

        Mail::send($email_view, $email_data, function ($m) use ($applicant, $location) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("EmailTutorApplicant Failed: To $applicant->email");
                 });
     }
}
