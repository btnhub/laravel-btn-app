<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\User;
use App\Location;
use Mail;

class SendWelcomeEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $user;
    public $location;
    public $email_view;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Location $location, $email_view)
    {
        $this->user = $user;
        $this->location = $location;
        $this->email_view = $email_view;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $location = $this->location;
        $email_view = $this->email_view;

        Mail::send($email_view, ['user' => $user], function ($m) use ($user, $location) {
                $m->from($location->email, $location->company_name);

                $m->to($user->email, $user->full_name)->subject("Welcome To BuffTutor!");
            });   
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $user = $this->user;
        $location = $this->location;
        $email_view = $this->email_view;

        Mail::send($email_view, ['user' => $user], function ($m) use ($user, $location) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("SendWelcomeEail Failed: To $user->email");
                 });
     }
}
