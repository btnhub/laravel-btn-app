<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use Mail;
use Carbon;
use App\User;
use App\Location;
use App\Assignment;
use App\MarketTutorRequest;
use App\MarketTutorProposal;

use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Jobs\NotifyStudentOfAssignment;
use App\Jobs\NotifyTutorOfAssignment;
use App\Jobs\NotifyTutorOfAcceptedProposal;
use App\Jobs\SendGeneralNotification;

class CreateAssignment extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    public $assignment;
    public $tutor_request;
    public $tutor_id;
    public $submitted_by;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Assignment $assignment, MarketTutorRequest $tutor_request, $tutor_id, $submitted_by)
    {                
        $this->assignment = $assignment;
        $this->tutor_request = $tutor_request;
        $this->tutor_id = $tutor_id;
        $this->submitted_by = $submitted_by;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $assignment = $this->assignment;
        $tutor_request = $this->tutor_request;
        $tutor_id = $this->tutor_id;
        $submitted_by = $this->submitted_by;
        
        /* Moved Code to Update DB to AssignmentController & MarketPlaceController
        $ref_id = mt_rand(10000000, 99999999);
        $assignment = Assignment::create([
            'ref_id' => $ref_id,
            'start_date' => $tutor_request->start_date,
            'end_date' => $tutor_request->end_date,
            'student_id' => $tutor_request->student_id,
            'child_name' => $tutor_request->child_name,
            'tutor_id' => $tutor_id,
            'assigned_by' => $assigned_by,
            'location_id' => $tutor_request->location_id,
            'course' => $tutor_request->course,
            ]);
        
        //Update Tutor Request and include Match details
        $tutor_request->mkt_visibility = 0;
        $tutor_request->matched_tutor_id = $tutor_id;
        $tutor_request->matched_date = Carbon\Carbon::now();
        $tutor_request->matched_by_id = $assigned_by;
        $tutor_request->assignment_id = $assignment->id;

        $tutor_request->save();*/

        //Email Parties
        switch ($submitted_by) {
            case 'admin':
                $this->dispatch(new NotifyStudentOfAssignment($assignment));
                $this->dispatch(new NotifyTutorOfAssignment($assignment));
                break;
            
            case 'student':
                //Update Tutor Proposals Table If Tutor Submitted Proposal
                /*if($tutor_proposal = MarketTutorProposal::where('tutor_request_id', $tutor_request->id)->where('tutor_id', $tutor_id)->first())
                {
                        $tutor_proposal->accepted_proposal = 1;
                        $tutor_proposal->assignment_id = $assignment->id;
                        
                        $tutor_proposal->save();
                }*/

                //Notify Tutor
                $user = User::find($assignment->tutor->id);
                $location = Location::find($assignment->location_id);
                $subject = "Proposal Accepted By {$assignment->student->first_name} For $assignment->course";
                $email_view = 'btn.emails.tutors.proposal_accepted';
                $email_data = ['assignment' => $assignment];
                
                $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));


                // Send Student Assignment Email  (Payments, Policies)                
                $user = User::find($assignment->student->id);
                $location = Location::find($assignment->location_id);
                $subject = "Working With {$assignment->tutor->first_name} ($assignment->course)";
                $email_view = 'btn.emails.students.assignment_created_by_student';
                $email_data = ['assignment' => $assignment];
                
                $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
                break;

            case 'tutor':
                //Notify Student
                $user = User::find($assignment->student->id);
                $location = Location::find($assignment->location_id);
                $subject = "Tutor Request Accepted By {$assignment->tutor->first_name} For $assignment->course";
                $email_view = 'btn.emails.students.assignment_created_by_tutor';
                $email_data = ['assignment' => $assignment];
                
                $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data)); 
                break;

        }
    }

    /**
      * Execute when the job fails.
      *
      * @return void
      */
     public function failed()
     {
        $assignment = $this->assignment;
        $tutor_request = $this->tutor_request;
        $tutor_id = $this->tutor_id;
        $submitted_by = $this->submitted_by;
        
        $location = Location::find($tutor_request->location_id);

        Mail::raw("Assign ID: $assignment->id, Request ID: $tutor_request->id, Tutor ID: $tutor_id, Submitted By: $submitted_by",  function ($m) use ($location) {
                 $m->from("Support@BuffTutor.com", "BuffTutor Job Failure");

                 $m->to($location->email, $location->company_name)->subject("CreateAssignment Failed");
                 });
     }

}
