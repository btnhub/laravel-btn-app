<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class Assignment extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'start_date', 'end_date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 

    /**
     * Active Assignments: ongoing ones or ones where the end date was less than 14 days ago
     * @param  Model $query
     * @return collection       
     */
    public function scopeActive($query)
    {   
        //We continue to pay tutors for up to 14 days after end date
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 14');
    }

    /**
     * List of current & undeclined assignments 
     *     For Tutors & Students (shows up on list of assigned students/tutors)
     * @return model 
     */
    public function scopeCurrent($query)
    {   
        //Tutors will have 10 days from end date to submit charges
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 10')
                        ->where('declined', 0);
    }

    /**
     * List of ongoing assignments (declined and on-going)
     *     For Admin: for calculating next tutor payment 
     *                 - includes deleted & declined assignments 
     *                     (in case the sessions haven't been paid yet)
     * @return [type] [description]
     */
    public function scopeRecent($query)
    {   
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 30')
                        ->withTrashed();
    }

    /**
     * List of ongoing assignments (declined and on-going)
     *     For Students: to give access to evaluation forms for tutors in the past 6 months (180 days) 
     *       Condition of Date Diff > 10 days excludes assignments that are still classified as CURRENT
     * @return [type] [description]
     */
    public function scopePrevious($query)
    {   
        return $query->whereRaw('DATEDIFF(NOW(),`end_date`) <= 180')
                        ->whereRaw('DATEDIFF(NOW(),`end_date`) > 10')
                        ->where('declined', 0);
    }

    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    public function location(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function evaluation(){
        return $this->hasMany('App\Evaluation', 'assignment_id');
    }

    public function tutorSessions(){
        return $this->hasMany('App\TutorSession', 'assignment_id');
    }

    public function tutorContract(){
        return $this->belongsTo('App\TutorContract', 'tutor_contract_id');
    }

    public function prepayments(){
        return $this->hasMany('App\Prepayment', 'assignment_id')
                    ->where('captured', 1)
                    ->where('paid', 1);
    }

    /**
     * Methods
     */
    
    public function studentSummary(){
        $tutor = $this->tutor;

        return ['id' => $this->ref_id,
                'tutor_id' => $tutor->ref_id,
                'user_id'  => $tutor->ref_id,
                'mobile' => $tutor->hasRole('mobile'),
                'user_profile' => route('profile.index', $tutor->ref_id),
                'course' => $this->course,
                'start_date' => $this->start_date->format('m/d/Y'),
                'end_date' => $this->end_date->format('m/d/Y'),
                'name' => $tutor->full_name,
                'email' => $tutor->email,
                'phone' => $tutor->phone,
                'avatar' => $tutor->avatarURL,
                'initials' => str_replace(".", "", $tutor->initials),
                'rate' => $this->tutorContract->student_rate,
                'type' => 'private',
                'created_at' => $this->created_at
                ];
    }

    public function tutorSummary(){
        $student = $this->student;
        
        return ['id' => $this->ref_id,
                'tutor_id' => $this->tutor->ref_id,
                'user_id'  => $student->ref_id,
                'mobile' => $student->hasRole('mobile'),
                //'user_profile' => route('profile.index', $student->ref_id),
                'course' => $this->course,
                'start_date' => $this->start_date->format('m/d/Y'),
                'end_date' => $this->end_date->format('m/d/Y'),
                'name' => $student->full_name,
                'email' => $student->email,
                'phone' => $student->phone,
                'avatar' => $student->avatarURL,
                'initials' => str_replace(".", "", $student->initials),
                'rate' => $this->tutorContract->tutor_rate,
                'type' => 'private',
                'created_at' => $this->created_at
                ];
    }

    public function prepaid_balance(){
        $total_prepaid = $this->prepayments->sum('total_amount');
        $total_prepaid_refunded = $this->prepayments->where('issue_refund', 1)->sum('refund_amount');

        $total_charged = $this->tutorSessions->sum('amt_charged');

        $tutor_sessions = $this->tutorSessions()->withTrashed()->get();
        
        $total_paid = 0;
        $total_refunded_to_account = 0;
        $total_refunded_to_student = 0;

        foreach ($tutor_sessions as $tutor_session) {
            //Payment
            if ($payment = Payment::where('transaction_id', $tutor_session->stripe_payment_id)->first()) {
                $total_paid += $payment->total_amount;
            }
            
            //Session Refunds
            if ($tutor_session->refund) {
                if ($tutor_session->refund->refunded_to_student) {
                    $total_refunded_to_student += $tutor_session->refund->refunded_to_student;
                }
                /*if ($tutor_session->refund->refunded_to_account) {
                    $total_refunded_to_account += $tutor_session->refund->refunded_to_account;
                }*/
            }
        }

        $balance =  number_format($total_paid + $total_prepaid - $total_prepaid_refunded - $total_charged - $total_refunded_to_student, 2);

        /*return ['total_paid' => $total_paid,
                'total_prepaid' => $total_prepaid,
                'total_prepaid_refunded' => $total_prepaid_refunded,
                'total_refunded_to_student' => $total_refunded_to_student,
                'total_charged' => $total_charged,
                'balance' => $balance
            ];*/
        
        return $balance > 0 ? $balance : 0;
    }

    public function refund_prepaid_balance($refund_amount = null){
        if (auth()->user() && (auth()->user()->id == $this->tutor->id || auth()->user()->isSuperUser())) {

            $remaining_balance = $refund_amount ?? $this->prepaid_balance();
            $amount_refunded = 0;
            
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
            
            foreach ($this->prepayments->sortByDesc('id') as $prepayment) {
                if ($prepayment->payment_method == 1 && $remaining_balance > 0) {
                    $prepayment_balance = $prepayment->total_amount - $prepayment->refund_amount;
                    // For future, determine whether or not to refund processing fee
                    // $prepayment_balance = $prepayment->total_amount - $prepayment->processing_fee - $prepayment->refund_amount;
                    if ($prepayment_balance > 0) {
                        $refund_amount = $prepayment_balance > $remaining_balance ? $remaining_balance : $prepayment_balance;

                        $refund_amount_stripe = number_format($refund_amount, 2) * 100;

                        $reverse_transfer = false;

                        try {
                            $stripe_refund = \Stripe\Refund::create([
                                "charge" => $prepayment->transaction_id,
                                'amount' => $refund_amount_stripe,
                                "reverse_transfer" => $reverse_transfer,
                                "refund_application_fee" => $reverse_transfer
                                ]);

                            if ($stripe_refund) {
                                $prepayment->stripe_refund_id = $stripe_refund->id;
                                $prepayment->issue_refund = 1;
                                $prepayment->refund_amount += $refund_amount;
                                $prepayment->refund_processed = Carbon\Carbon::now();
                                
                                if ($prepayment->admin_notes) {
                                    $prepayment->admin_notes .= "  ";
                                }
                                
                                $prepayment->admin_notes .= "Refund created by " . auth()->user()->full_name . " (ID: " . auth()->user()->id. ") on ". Carbon\Carbon::now()->format("m/d/Y").".";
                                $prepayment->save();

                                $remaining_balance -= $refund_amount;
                                $remaining_balance = number_format($remaining_balance,2);

                                $amount_refunded += $refund_amount;
                            }

                        } catch (\Stripe\Error\ApiConnection $e) {
                           // Network problem, perhaps try again.
                            $e_json = $e->getJsonBody();
                            $error = $e_json['error'];

                            return $error['message'];

                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // You screwed up in your programming. Shouldn't happen!
                            $e_json = $e->getJsonBody();
                            $error = $e_json['error'];
                            
                            return $error['message'];

                        } catch (\Stripe\Error\Api $e) {
                            // Stripe's servers are down!
                            $e_json = $e->getJsonBody();
                            $error = $e_json['error'];
                            
                            return $error['message'];
                        }
                    } 
                }       
            }
            return $amount_refunded;  
        }
        else{
            return "User not logged in.  Refund not issued";
        }
    }
}
