<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TutorSession extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at', 'session_date', 'tutor_payment_date'];
	protected $table = 'tutor_sessions';

    protected $guarded = [
        'id'
    ];
	
    public function assignment(){
        return $this->belongsTo('App\Assignment', 'assignment_id');
    }

    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

    public function refund(){
        return $this->hasOne('App\Refund', 'session_id');
    }

    public function isSoleSession($assignment_id)
    {
        //check if student has met with the tutor only once
    
        $no_of_sessions = TutorSession::where('assignment_id', $assignment_id)->count();

        if ($no_of_sessions > 1) {
            return false;
        }

        return true;
    }

    public function refundRequestSubmitted($assignment_id){
                
        if (Refund::where('assignment_id', $assignment_id)->where('refund_reason', 'like', '%session%')->exists()) {
            return true;
        }
        
        return false;
    }
}
