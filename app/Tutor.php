<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Collection;

use App\User;
use App\Student;
use App\Evaluation;
use App\TutorSession;
use App\Refund;
use App\Profile;
use App\PageVisit;

use DB;
use Carbon;

class Tutor extends User
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Accessor to create full name of user
     * @return string 
     */
    public function getTutorDetailsAttribute()
    {
        return $this->full_name . " -  {$this->profile->major} " . ucfirst($this->profile->tutor_education)." - " . $this->profile->tutor_fee * 100 ."%";
    }

    public function notifications(){
        return $this->hasMany('App\MarketNotification');
    }

    public function assignments()
    {   
        return $this->hasMany('App\Assignment', 'tutor_id');
    }

    public function active_assignments(){

        return $this->assignments()->active();
    }

    public function danielsFundAssignments()
    {   
        return $this->hasMany('App\DanielsFundScholar', 'tutor_id');
    }

    public function marketTutorProposals()
    {   
        return $this->hasMany('App\MarketTutorProposal', 'tutor_id');
    }

    public function currentMarketTutorProposals()
    {   
        return $this->hasMany('App\MarketTutorProposal', 'tutor_id')
                        ->whereHas('tutor_request', function($query) {
                                $query->currentSemester();
                            });
    }

    public function directTutorRequests()
    {   
        return $this->hasMany('App\MarketTutorRequest', 'requested_tutor_id');
    }

    public function currentDirectTutorRequests()
    {   
        return $this->hasMany('App\MarketTutorRequest', 'requested_tutor_id')
                        ->currentSemester();
    }

    //All tutor sessions
    public function tutorSessions()
    {
    	return $this->hasMany('App\TutorSession', 'tutor_id')
                    ->orderBy('id', 'DESC');
    }

    public function refunds()
    {
    	return $this->hasMany('App\Refund', 'tutor_id')
    					->where('refunded_from_tutor', '>', 0)
    					->orderBy('id', 'DESC');
    }

    public function evaluations()
    {
    	return $this->hasMany('App\Evaluation', 'tutor_id')
    					->orderBy('id', 'DESC');
    }

    public function tutor_contracts()
    {
        return $this->hasMany('App\TutorContract', 'tutor_id');
    }

    public function application()
    {
        return $this->hasOne('App\TutorApplicant', 'user_id');
    }

    public function favorites(){
        return $this->hasMany('App\Favorite', 'tutor_id');
    }
    
    public function geolocations()
    {
        return $this->hasMany('App\Geolocation', 'tutor_id');
    }

    public function course_levels()
    {
        return $this->belongsToMany('App\CourseLevel');
    }

    public function searches(){
        return $this->belongsToMany('App\Search');
    } 

    public function proficiency_exam_results(){
        $exam_results = DB::table('prof_exam_results')->where('user_id', $this->id)->get();
        $results = [];
        foreach ($exam_results as $result) {
            $exam = ucwords(str_replace('_', ' ', str_replace(".csv", "", substr(strstr($result->exam, '-'), 1))));

            array_push($results, (object)['exam' => $exam, 'grade' => $result->grade]);
        }
        
        return $results;
    }

    /**
     * Returns active and approved tutors
     *     Profile visible (set by both tutor & admin), 
     *     About/Experience & Courses are completed, 
     *     Users who are tutors/music teachers, not banned
     *     Tutor end date after today
     * @param  query $query Tutors model
     * @return query 
     */
    public function scopeActiveTutors($query)
    {
        $tutor_contract_date = DB::table('settings')->where('field', "tutor_contract_date")->first()->value;
        
        return $query->where('banned',0) 
                        ->whereHas('roles', function ($query) {
                            $query->where('role', '!=', 'banned')
                                    ->where(function ($q) {
                                        $q->where('role', 'tutor')
                                            ->orWhere('role', 'music-teacher')
                                            ->orWhere('role', 'marketplace-tutor');
                                    });                                       
                            })
                        ->whereHas('profile', function ($query) use($tutor_contract_date) {
                            $query->where('show_profile_admin', 1)
                                    ->where('show_profile_tutor', 1)
                                    ->whereRaw('tutor_start_date <= CURDATE()')
                                    ->whereRaw('tutor_end_date > CURDATE()')
                                    /*->whereNotNull('tutor_contract')*/
                                    ->where('tutor_contract', '>', $tutor_contract_date)
                                    ->whereNotNull('background_check')
                                    ->where('tutor_experience', '!=', '')
                                    /*->where('rate_min', '>', '0')
                                    ->where('rate_max', '>', '0')*/
                                    ->where('tutor_education', '!=', '')
                                    ->where('courses', '!=', '');
                            });                         
    }

    public function scopeSearchable($query){
        return $query->whereHas('profile', function ($q) {
                            $q->whereNull('searchable')
                                ->orWhere('searchable', 1);
                        });
    }

    public function newTutor()
    {
        if ($this->assignments()->count() == 0) {
            return true;
        }

        return false;
    }
    /**
     * NO LONGER CALLED IN SEARCH CONTROLLER TO DISPLAY TUTOR PROFILES, but same formula is used
     * Rank Tutors displayed in search based on
     *     Evaluations
     *     Session Hours
     *     Refunds
     * @return number   Weighted score
     */

    public function rank()
    {
        if ($this->id == 63) {
            $score = 1E8;
        }
        
        else
        {
            $summary = DB::table('tutor_rankings')->where('id', $this->id)->first();

            if (!$summary) {
                return 0;
            }

            $weights = DB::table('rank')->pluck('weight', 'title');

            $score =  $summary->positive_end_eval * $weights['positive_end_eval']
                            + $summary->positive_first_eval * $weights['positive_first_eval']
                            + $summary->negative_end_eval * $weights['negative_end_eval']
                            + $summary->negative_first_eval * $weights['negative_first_eval']
                            + $summary->hours * $weights['hours']
                            + $summary->session_refunds * $weights['session_refunds'];
        }
        return $score;
    }

    /**
     * Calculates total earned by the tutor from ongoing assignments or recently ended ones
     *     Only includes ones that have not been paid
     * @return number - Amount earned by the tutor 
     */
    public function nextPaymentEarned()
    {
    	$assignment_ids = $this->assignments()->recent()->pluck('id');
    	$next_payment_earned = TutorSession::whereIn('assignment_id', $assignment_ids)
                                ->where('tutor_payment_date', null)
                                ->where('refund_id', null)
                                ->sum('tutor_pay');

    	return $next_payment_earned;
    }

    /**
     * Finds all sessions that tutor has not been paid by BTN
     *     Includes 
     *         - sessions that students have paid for (not delinquent)
     *         - sessions not paid out to tutor (tutor_payment_date = nul)
     *     
     * @return [type] [description]
     */
    public function paymentEligibleSessions()
    {
        $assignment_ids = $this->assignments()->recent()->pluck('id');
        
        $delinquent_sessions_ids = $this->delinquentSessions()->pluck('id');

        $eligible_sessions = TutorSession::whereIn('assignment_id', $assignment_ids)
                                ->where('tutor_payment_date', null)
                                ->whereNotIn('id', $delinquent_sessions_ids)
                                ->where('tutor_pay', '>', 0)
                                ->get();
        return $eligible_sessions;

    }

    /**
     * Sessions that the student hasn't paid for or didn't have enough funds to cover
     * @return collection  Collection of sessions that the student hasn't paid for
     */
    public function delinquentSessions()
    {
        $student_ids = $this->assignments()->recent()->pluck('student_id');

        $students = Student::whereIn('id', $student_ids)->get();

        $delinquent_sessions = collect();
        foreach ($students as $student) {
            $delinquent_sessions->push($student->unpaidSessions()->where('tutor_id', $this->id));
        }

        return $delinquent_sessions->flatten(); 
        //flatten removes the multi-dimensional layer of the unpaidSessions collection in the new delinquent_sessions collection
    }

    public function recentPreviousAssignments()
    {
        if (!$this->newTutor())
        {
            //Get Assignments of the last semester the tutor worked
            $last_assignment = $this->assignments()->where('end_date', '<', Carbon\Carbon::now())->get()->last();
    
            if ($last_assignment) {
                //Figure out semester
                $last_month = $last_assignment->end_date->month;
                $last_year = $last_assignment->end_date->year;
                
                if ($last_month <= 5) {
                    //Spring
                    $semester_start = Carbon\Carbon::createFromDate($last_year, 1, 1);
                    $semester_end = Carbon\Carbon::createFromDate($last_year, 5, 31);
                }
                elseif ($last_month >5 && $last_month < 9) {
                    //Summer
                    $semester_start = Carbon\Carbon::createFromDate($last_year, 6, 1);
                    $semester_end = Carbon\Carbon::createFromDate($last_year, 8, 15);
                }
                else{
                    //Fall
                    $semester_start = Carbon\Carbon::createFromDate($last_year, 8, 15);
                    $semester_end = Carbon\Carbon::createFromDate($last_year, 12, 31);
                }
        
                //Find All Assignments in Last Semester
                $recent_assignments = $this->assignments()
                                            ->where('end_date', '>=', $semester_start)
                                            ->where('end_date', '<=', $semester_end)
                                            ->get();
        
                return $recent_assignments;
            }
            
        }

        return null;
    }

    public function recentSemester()
    {
        if (!$this->newTutor()) {
           //Get Assignments of the last semester the tutor worked
            $last_assignment = $this->assignments()->where('end_date', '<', Carbon\Carbon::now())->get()->last();

            if ($last_assignment) {
                //Figure out semester

                $last_month = $last_assignment->end_date->month;
                $last_year = $last_assignment->end_date->year;
                
                if ($last_month <= 5) {
                    //Spring
                    $semester = "Spring $last_year";
                }
                elseif ($last_month >5 && $last_month < 9) {
                    //Summer
                    $semester = "Summer $last_year";
                }
                else{
                    //Fall
                    $semester = "Fall $last_year";
                }

                return $semester; 
            }
            
        }

            return "New Tutor";

    }

    public function recentEvalRatio()
    {
        if (!$this->newTutor()) {
            $recent_assignments = $this->recentPreviousAssignments();

            if ($recent_assignments) {
                $assign_with_sessions = $recent_assignments->filter(function ($assign)                    {
                                            return $assign->tutorSessions()->count() > 0;
                                        })
                                        ->count();
                $positive_end = $recent_assignments->filter(function ($assign) {
                                                return $assign->evaluation()->where('type', 'End')->where('satisfied', 1)->count() > 0;
                                            })
                                            ->count();
                
                if ($assign_with_sessions > 0) {
                    return number_format($positive_end / $assign_with_sessions, 2);
                }
                    
            }
        }
        
        return 0;
    }

    public function profileVisits(){
        return PageVisit::where('user_id', '!=', $this->id)
                    ->where('url', 'LIKE', "%".route('profile.index',[$this->ref_id], false)."%")
                    ->get();
    }

    public function nearest_geolocation($lat, $lng, $search_radius = 10, $search_radius_unit = 'miles'){
        //return the closes geolocation
        $conversion = 1.60934; //1 mile = 1.60934km
        $min_distance = 1e8;
        $nearest_location = null;
        //$search_radius = (float)DB::table('settings')->where('field', 'search_radius')->first()->value;
        //$search_radius_unit = DB::table('settings')->where('field', 'search_radius_unit')->first()->value;

        if ($search_radius_unit != 'km') {
            //convert to km
            $search_radius = $search_radius * $conversion;
            $search_radius_unit = "km";
        }

        if ($this->geolocations->count()) {
            foreach ($this->geolocations as $geolocation) {
                $distance = $geolocation->map_distance($lat, $lng, "km");
                
                if ($distance < $min_distance) {
                    $min_distance = $distance;
                    $nearest_geolocation = $geolocation;
                }
            }

            //convert geolocation radius to km
            $nearest_radius = $nearest_geolocation->radius;
            if ($nearest_geolocation->radius_unit != "km") {
                //convert to km
                $nearest_radius = $nearest_geolocation->radius * $conversion;
            }

            if ($min_distance < $search_radius || $min_distance - $nearest_radius < $search_radius) {
                
                $nearest_geolocation->distance = number_format($min_distance, 2);
                $nearest_geolocation->distance_unit = "km";

                return $nearest_geolocation;
            }
            else
            {
                return false;
            }
        }
        else{
            return false;
        }
    }

    public function rating()
    {
        if ($this->id == 63) {
            return 5;
        }
        $eval_count = 0;
        $sum_stars = 0;
        foreach ($this->evaluations as $evaluation) {
            if ($evaluation->rating !== NULL) {
                $weight = $evaluation->type == "End" ? 1 : 0.8;
                $sum_stars += $evaluation->rating * $weight;
                $eval_count++;
            }
        }

        $average_stars = $eval_count ? $sum_stars / $eval_count : 0;
        return $average_stars;
    }
}
