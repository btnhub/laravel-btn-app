<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Profile extends Model
{
	protected $guarded = [
        'id'
    ];
    
    protected $dates = [
    	'background_check',
    	'tutor_contract'
    ];

    /**
     * Relationships
     */
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function education_levels(){
        return $education_levels = [
            null => "Select Education Level", 
            'phd' => "Graduated (PhD)", 
            'masters' => 'Graduated (Master\'s Degree)', 'bachelors' => 'Graduated (Bachelor\'s Degree)', 
            'grad_student' => "Graduate Student", 
            'undergrad' => "Undergraduate Student"];
    }

    public function education(){
        return $this->education_levels()[$this->tutor_education];
    }

    public function tutor_availability_options()
    {
        return $tutor_availability_options = [
            'Daytime' => 'Daytime (before 5pm)',
            'Evenings' => 'Evenings (5-7pm)',
            'Nights' => 'Nights (after 7pm)',
            'Saturdays' => 'Saturdays',
            'Sundays' => 'Sundays'
            ];
    }

}
