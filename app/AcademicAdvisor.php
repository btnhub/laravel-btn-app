<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon;

class AcademicAdvisor extends Model
{
    use SoftDeletes;
    
    protected $guarded = ['id'];

    protected $dates = [
    	'last_contacted'
    ];
    
    public function getFullNameAttribute()
    {
        return $this->first_name . " " . $this->last_name;
    }
    
    public function location(){
        return $this->belongsTo('App\Location', 'location_id');
    }

    public function scopeContactList($query)
    {
        return $query->where('unsubscribe', 0) 
                        ->where(function($q){
                            $q->where('last_contacted',  '<', Carbon\Carbon::now()->subMonths(1))
                                ->orWhereNull('last_contacted');
                        });                   
    }
}
