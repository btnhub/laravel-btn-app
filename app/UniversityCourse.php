<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UniversityCourse extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ]; 

    /**
     * Relationships
     */
    public function university(){
        return $this->belongsTo('App\University', 'university_id');
    }

    public function department(){
        return $this->belongsTo('App\UniversityDepartment', 'university_department_id');
    }
}
