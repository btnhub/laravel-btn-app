<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        /**
         * BTN Added: By Pass Gate checks if user is Super User
         */
        $gate->before(function($user, $ability)
            {
                if($user->isSuperUser())
                    { 
                        return true;
                    }
            });

        /**
         * Admin / Super User access
         */
        $gate->define('admin-only', function($user)
        {
            if ($user->hasRole(['super-user', 'admin']) && !$user->hasRole('banned')) {
                return true;
            }
            
            \Session::flash('error', "You are not authorized to view this page");
            return redirect('/');
        });

        /**
         * User can access own data in the Users Table
         */
        $gate->define('self-access', function($user, $object)
        {
            if ($user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->id;
        });

        /**
         * Any non-banned users can access/edit this page
         */
        $gate->define('user-access', function($user, $object)
        {
            if ($user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->user_id;
        });

        /**
         *Any tutors who have not been banned can access
         */
        $gate->define('tutor-access', function($user, $object)
        {
            if (!$user->hasRole(['tutor', 'marketplace-tutor', 'music-teacher']) || $user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->tutor_id;
        });
        
        /**
         * Any students who have not been banned can access
         */
        $gate->define('student-access', function($user, $object)
        {
            if (!$user->hasRole(['student', 'music-student']) || $user->hasRole('banned')) {
                \Session::flash('error', "You are not authorized to view this page");
                return redirect('/');
            }

            return $user->id == $object->student_id;
        });
    }
}
