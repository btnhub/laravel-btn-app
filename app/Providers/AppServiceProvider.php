<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//For BugSnag
use Illuminate\Contracts\Logging\Log;
use Psr\Log\LoggerInterface;

use Illuminate\Support\Facades\Validator;
use App\ReCaptcha;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('recaptcha', function ($attribute, $value, $parameters, $validator) {
            $recaptcha = new Recaptcha;
            return $recaptcha->verify($value);
        });

        view()->composer('layouts.citybook.partials.navbar_top', function($view){
                $user = null;
                $profile = null;
                $fav_count = 0;

                if ($auth_user = auth()->user()) {
                    $user = \App\User::where('id', $auth_user->id)->with(['profile', 'favorites', 'roles'])->first();
                    $profile = $user->profile;
                    $fav_count = $user->favorites->count();
                }
                else{
                    //USE SESSION DATA
                    $fav_count = \App\Favorite::where('user_id', NULL)->where('identifier', session('identifier'))/*->has('tutor')*/->count();
                }

                $view->with('fav_count', $fav_count)
                        ->with('user', $user)
                        ->with('profile', $profile);
        });

        view()->composer('btn.pages.citybook.home', function($view){
            $reviews = \App\Evaluation::where('homepage_visible', 1)/*->has('student')*/->with('student')->inRandomOrder()->limit(10)->get(); 
            $discount = number_format(\DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100, 0);
            
            $view->with('reviews', $reviews)
                    ->with('discount', $discount);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local')) {
        // register the service provider
        $this->app->register('Barryvdh\Debugbar\ServiceProvider');

        // register an alias
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Debugbar', 'Barryvdh\Debugbar\Facade');
        });


        }

        $this->app->alias('bugsnag.logger', Log::class);
        $this->app->alias('bugsnag.logger', LoggerInterface::class);
    }
}
