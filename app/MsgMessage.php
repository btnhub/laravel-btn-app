<?php

namespace App;

use DB;
use App\User;
use Cmgmyr\Messenger\Models\Models;
use Illuminate\Database\Eloquent\Model;

class MsgMessage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'msg_messages';

    /**
     * The relationships that should be touched on save.
     *
     * @var array
     */
    protected $touches = ['thread'];

    /**
     * The attributes that can be set with Mass Assignment.
     *
     * @var array
     */
    protected $fillable = ['thread_id', 'user_id', 'body'];

    /**
     * Validation rules.
     *
     * @var array
     */
    protected $rules = [
        'body' => 'required',
    ];

    /**
     * {@inheritDoc}
     */
    public function __construct(array $attributes = [])
    {
        $this->table = Models::table('msg_messages');

        parent::__construct($attributes);
    }

    /**
     * BTN Added 
     */

    public function scopeSuspiciousMessages($query){
        //Suspicous terms to look for
        /*$search_terms = ['email', 'e-mail', 'mail',
                            'call', 'phone', 'cell', 'number',
                            'gmail' ,'g-mail', 
                            'yahoo', 'hotmail', 'outlook',
                            'icloud'];   */
        
        //EMAIL & PHONE PATTERN SEARCH
        //Test: https://regex101.com/
        //Source: 
        $phone_regex = '([0-9]{3})[-. )]*([0-9]{3})[-. ]*([0-9]{4})( *x([0-9]+))?\s*'; //matches most properly formatted phone numbers (with one space, (), - or . in them)            

        //Source: http://stackoverflow.com/questions/7165056/regex-to-match-email-addresses-and-common-obfuscations (madjoe's answer)
        //Test: http://regexr.com/31qh4
        //For MySQL, replace \s with [[:space:]]
        $email_regex = '[A-Z0-9\._%+-]+([[:space:]]*@[[:space:]]*|[[:space:]]*[\[|\{|\(]+[[:space:]]*(at|@)[[:space:]]*[\)|\}\]]+[[:space:]]*)([A-Z0-9\.-]+(\.|[[:space:]]*[\[|\{|\(]+[[:space:]]*(dot|\.)[[:space:]]*[\)|\}|\]]+[[:space:]]*))+[a-z]{2,6}';
        
        //Generate SQL Where clause
        /*$sql_where = '';
        foreach ($search_terms as $term) {
            $sql_where .= "body like '%$term%' or ";
        }
        //remove trailing 'or'
        $sql_where = rtrim($sql_where, 'or ');*/

        return $query->/*whereRAW($sql_where)
                    ->or*/WhereRAW("body REGEXP '$email_regex'")
                    ->orWhereRAW("body REGEXP '$phone_regex'");
    }

    /**
     * Thread relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo('App\MsgThread', 'thread_id', 'id');
        /*return $this->belongsTo(Models::classname(Thread::class), 'thread_id', 'id');*/
    }

    /**
     * User relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
        /*return $this->belongsTo(Models::classname(User::class), 'user_id');*/
    }

    /**
     * Participants relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participants()
    {
        return $this->hasMany('App\MsgParticipant', 'thread_id', 'thread_id');
        /*return $this->hasMany(Models::classname(Participant::class), 'thread_id', 'thread_id');*/
    }

    /**
     * Recipients of this message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipients()
    {
        return $this->participants()->where('user_id', '!=', $this->user_id);
    }
}

