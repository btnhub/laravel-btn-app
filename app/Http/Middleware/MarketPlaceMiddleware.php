<?php

namespace App\Http\Middleware;

use Closure;

class MarketPlaceMiddleware
{
    /**
     * Filter users who can access MarketPlace
     * 1) Has appropriate role
     * 2) Not banned
     * 3) End date is after today
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (($request->user()->hasRole('marketplace-tutor') && 
            !$request->user()->hasRole('banned') && 
            strtotime($request->user()->profile->tutor_end_date) > strtotime(date('m/d/Y')))
            || ($request->user()->hasRole('super-user')) ) 
        {
            return $next($request);
        }

        \Session::flash('error', 'You are not authorized to view the selected page.  If you believe you should have access, check your Tutor End Date.');
        
        return redirect()->back();
        
    }
}
