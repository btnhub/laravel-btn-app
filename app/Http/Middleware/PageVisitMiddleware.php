<?php

namespace App\Http\Middleware;

use Closure;
use App\Jobs\RecordPageVisit;

class PageVisitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Dispatch page visit job
        $user_id = auth()->user()->id ?? null;
        
        if ($user_id != 62 && $user_id != 63) {
            if (!session('identifier')) {
                session(['identifier' => strtoupper(str_random(10))]);
            }

            $identifier = session('identifier');
            $ip = \Request::getClientIp(true) ?? null;
            $url = url()->current() ?? null;
            $url_previous = url()->previous() ?? null;
            $referer = $_SERVER['HTTP_REFERER'] ?? null;
            $browser = $_SERVER['HTTP_USER_AGENT'] ?? null;
            
            //do not record spiders & bots
            if (!stripos($browser, 'spider') && !stripos($browser, 'bot')) {
                dispatch(new RecordPageVisit($user_id, $identifier, $ip, $url, $url_previous, $referer, $browser));
            }
        }        

        return $next($request);
    }
}
