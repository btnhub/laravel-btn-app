<?php

namespace App\Http\Middleware;

use Closure;

class RoleMiddleware
{
    /**
     * BTN: Allow access based on the role of the user
     * Redirects back with error message, does not present login page
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $roles = explode("|", $roles);

        if (($request->user()->hasRole($roles) && !$request->user()->hasRole('banned')) || $request->user()->hasRole('super-user'))
        {
           return $next($request);
        }

        \Session::flash('error', 'You are not authorized to view the selected page');
        return redirect()->back();

    }
}
