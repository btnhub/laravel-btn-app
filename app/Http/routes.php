<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {    
    //DYNAMIC HOMEPAGES IN THE 'tutors' ROUTE GROUP
    //return view('btn.pages.home');
    
    /*
    
    $course_levels = App\CourseLevel::orderBy('website_order')->get()->pluck('description', 'id')->toArray();
	
	$course_levels = ['' => "Any Level"] + $course_levels;
	*/
    return view('btn.pages.citybook.home'/*, compact('course_levels')*/);
});

Route::get('/g/{details}', ['as' => 'home.general',function($details){
	//"g" = "general -> no city" 
	//$details = course-tutors
	//1st url = /g/tutors (skip this one...it's array has too few values)
	$details_array = explode("-", $details);

	if (count($details_array) > 1) {
		$course = ucwords(str_replace("_"," ",$details_array[0]));
		
		$online = false;

		$title = "Elite $course Tutors | 10% Off First Hour | BuffTutor";
		$meta = "Dedicated to your success. Experienced & vetted $course tutors who fit YOUR BUDGET and YOUR SCHEDULE! Satisfaction Guaranteed. Est 2009.";

		if (strpos($details, "online")) {
			$online = true;
			$title = "Elite Online $course Tutors | 10% Off First Hour | BuffTutor";
			$meta = "Dedicated to your success. Experienced & vetted online $course tutors who fit YOUR BUDGET and YOUR SCHEDULE! Satisfaction Guaranteed. Est 2009.";
		}

		return view('btn.pages.citybook.home', compact('course', 'online', 'title', 'meta'));
	}
	
	return view('btn.pages.citybook.home');
	
}]);

Route::get('/c/{details}', ['as' => 'home.college',function($details){
	//"c" = "colleges/unis" 
	//$details = college-course-tutors
	
	$details_array = explode("-", $details);

	if (count($details_array) > 1) {
		$university = ucwords(str_replace("_"," ",$details_array[0]));
		$universities = [$university];
		$course = strpos($details_array[1], "tutor") === false ? ucwords(str_replace("_"," ",$details_array[1])) : NULL;
		
		$title = "Elite $course Tutors @ $university | 10% Off First Hour | BuffTutor";
		$meta = "Connect with experienced & vetted $course tutors at $university who fit YOUR BUDGET and YOUR SCHEDULE! Satisfaction Guaranteed. Est 2009.";

		$online = false;

		if (strpos($details, "online")) {
			$online = true;

			if ($course) {
				$title = "Online $course Tutors At $university | 10% Off First Hour | BuffTutor";
				$meta = "Connect with experienced & vetted online $course tutors at $university who fit YOUR BUDGET and YOUR SCHEDULE! Satisfaction Guaranteed. Est 2009.";
			}
			else{
				$title = "Online Tutors For $university Courses | 10% Off First Hour | BuffTutor";
				$meta = "Experienced & affordable online tutors for $university courses. Satisfaction Guaranteed. Est 2009. Get A BuffTutor today!";
			}	
		}

		return view('btn.pages.citybook.home', compact('university', 'universities', 'course', 'online', 'title', 'meta'));
	}
	
	return view('btn.pages.citybook.home');
	
}]);

Route::get('/course/{course_id}/{details}', ['as' => 'home.course',function($course_id, $details){
	//$details => course_number-tutors-university <-- NOT USED
	
	$university = null;
	$universities = [];

	// $details_array = explode("-", $details);

	if ($uni_course = App\UniversityCourse::where('id', $course_id)->with('university')->first()) {
		if ($uni_course && $uni_course->university) {
			$university = $uni_course->university->name;	
			$universities = [$university];
		}
		
		// $course = strtoupper(str_replace("_"," ",$details_array[0]));
		$course = $uni_course->course_number;
		
		$online = strpos($details, "online") ? true : false;
		$title = "Tutors For $course @ $university | BuffTutor";
		$meta = "$course tutors who fit YOUR BUDGET and YOUR SCHEDULE! Get 10% Off 1st Hour. In-Person & Online. Satisfaction Guaranteed.";

		return view('btn.pages.citybook.home', compact('university', 'universities','course', 'online', 'title', 'meta'));
	}
	
	return view('btn.pages.citybook.home');
	
}]);

Route::get('/emails', function () {
    /*$assignment = App\Assignment::first();
    return view('btn.emails.students.eval_reminder_first', ['assignment' => $assignment]);*/
});

Route::get('covid19-response',function (){
	return view('btn.pages.citybook.covid.virus_instructions');
});

Route::group(['prefix' => 'api'], function(){
	//Authorization Routes
	Route::group(['prefix' => 'auth'], function(){
		Route::post('login', [
			'uses' => 'ApiAuthController@loginUser']);
		Route::post('register', [
			'uses' => 'ApiAuthController@registerUser']);
		Route::post('logout', [
			'uses' => 'ApiAuthController@logoutUser']);

	});

	//Authorized Users Only
	Route::group(['middleware' => 'btn-api'], function(){
		Route::group(['prefix' => 'data'], function (){
			Route::post('get-courses', [
				'uses' => 'ApiUsersController@getCourses'
			]);
		});

		Route::group(['prefix' => 'account'], function (){
			Route::post('update-user-details', [
				'uses' => 'ApiUsersController@updateUserDetails']);
			Route::post('fetch-assignments', [
				'uses' => 'ApiUsersController@fetchAssignments']);
		});	

		Route::group(['prefix' => 'tutor-requests'], function (){
			Route::post('submit-tutor-request', [
				'uses' => 'ApiTutorRequestsController@submitTutorRequest']);
			Route::post('fetch-tutor-requests', [
				'uses' => 'ApiTutorRequestsController@fetchTutorRequests']);
			Route::post('fetch-tutor-contracts', [
				'uses' => 'ApiTutorRequestsController@fetchTutorContracts']);
			Route::post('submit-tutor-decision', [
				'uses' => 'ApiTutorRequestsController@submitTutorDecision']);
			Route::post('submit-student-decision', [
				'uses' => 'ApiTutorRequestsController@submitStudentDecision']);
			
		});	

		Route::group(['prefix' => 'tutor-sessions'], function (){
			Route::post('charge-tutor-session', [
				'uses' => "ApiTutorSessionsController@chargeTutorSession"
			]);
			Route::post('delete-tutor-session', [
				'uses' => "ApiTutorSessionsController@deleteTutorSession"
			]);
		});
	
		Route::group(['prefix' => 'evaluations'], function (){
			Route::post('submit-evaluation', [
				'uses' => "ApiTutorSessionsController@submitEvaluation"
			]);
		});

		Route::group(['prefix' => 'payment-settings'], function (){
			Route::post('fetch-card', [
				'uses' => "ApiStripeController@fetchCardDetails"
			]);
			Route::post('store-card', [
				'uses' => "ApiStripeController@storeCardDetails"
			]);
		});
		Route::group(['prefix' => 'notifications'], function (){
			Route::post('save-fcm-token', [
				'uses'=> 'ApiNotificationsController@saveFCMToken'
			]);

			// Route::post('send-fcm-notification', [
			// 	'uses'=> 'ApiNotificationsController@sendFCMNotification'
			// ]);
		});
	});
});

Route::group(['prefix' => 'review-sessions'], function(){
	Route::get('/{type?}', [
			'as' => 'get.review.sessions',
			'uses' => 'ReviewsController@getReviewSessions']);

	Route::get('/my-review-sessions/{user_ref_id}', [
			'as' => 'my.review.sessions',
			'uses' => 'ReviewsController@getMyReviewSessions']);

	Route::get('/vote/{review_session_ref_id}', [
			'as' => 'get.poll.details',
			'uses' => 'ReviewsController@getPollDetails']);

	Route::post('/vote/{review_session_ref_id}', [
			'as' => 'vote.review.time',
			'uses' => 'ReviewsController@voteReviewTime']);

	Route::get('/review/{review_session_ref_id}', [
			'as' => 'get.review.details',
			'uses' => 'ReviewsController@getReviewDetails']);

	Route::get('/book/{review_session_ref_id}', [
			'as' => 'book.review',
			'uses' => 'ReviewsController@bookReviewSession']);

	Route::post('/register/{review_session_ref_id}', [
			'as' => 'register.for.review',
			'uses' => 'ReviewsController@registerForReview']);

	Route::post('/cancel-reservation/{review_session_ref_id}', [
			'as' => 'cancel.review.reservation',
			'uses' => 'ReviewsController@cancelReviewReservation']);

	Route::get('/evaluate/{review_session_ref_id}', [
			'as' => 'get.review.evaluation',
			'uses' => 'ReviewsController@getReviewEvaluation']);

	Route::post('/evaluate/{review_session_ref_id}', [
			'as' => 'submit.review.evaluation',
			'uses' => 'ReviewsController@submitReviewEvaluation']);

	Route::post('/request-review', [
			'as' => 'create.review.request',
			'uses' => 'ReviewsController@createReviewRequest']);

	Route::group(['middleware' => ['auth', 'role:review-tutor|super-user|admin']], function(){
		Route::get('/tutors/create-review', [
				'as' => 'list.group.reviews',
				'uses' => 'ReviewsController@listReviewSessions']);
		Route::post('/tutors/create-review',[
				'as' => 'create.review.session',
				'uses' => 'ReviewsController@createReviewSession']);
	});
});

Route::get('add-stars', [
		'as' => 'get.evals',
		'uses' => 'AdminController@getEvals',
		'middleware' => ['auth', 'role:super-user|admin|staff']]);

Route::post('add-stars', [
		'as' => 'save.stars',
		'uses' => 'AdminController@saveStars',
		'middleware' => ['auth', 'role:super-user|admin|staff']]);

Route::post('/check-availablility', [
	'as' => 'check.availablility',
	'uses' => 'TutorRequestController@checkAvailablility']);

Route::get('/search-tutors', [
	'as' => 'get.search.map',
	'uses' => 'SearchController@getSearchMap']);

Route::post('/search-tutors', [
	'as' => 'search.map',
	'uses' => 'SearchController@searchMap']);

//DO NOT MOVE favorite & save.  Absolute url is used in btn-save_tutor.js
Route::get('favorite-tutors', [
		'as' => 'favorite.tutors',
		'uses' => 'ProfileController@favoriteTutors']);

Route::post('save-tutor/{tutor_ref_id}', [
		'as' => 'save.tutor',
		'uses' => 'ProfileController@saveTutor']);

Route::auth();

Route::get('/calendars/mogi', [
	'as' => 'mogi.calendar',
	function(){
		//return view('btn.admin.mogi_calendar');
		return view('btn.pages.citybook.admin.mogi_calendar');
	}]);

Route::get('/home', 'HomeController@index');

//Assignment Routes

Route::resource('assignments', 'AssignmentController');

//For Admin
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:super-user|admin']], function(){

	//Update Locations
	Route::post('/update-user-location/{user_id}', [
		'as' => 'update.user.location',
		'uses' => 'AdminController@updateUserLocation']);

	Route::post('/update-request-location/{tutor_request_id}', [
		'as' => 'update.request.location',
		'uses' => 'AdminController@updateRequestLocation']);
	
	Route::get('/fix-users-location', [
		'as' => 'users.without.location',
		'uses' => 'AdminController@usersWithoutLocation']);

	Route::post('/fix-users-location', [
		'as' => 'fix.users.location',
		'uses' => 'AdminController@fixUsersLocation']);

	//Create New BTN Location & Add New School
	Route::post('/create-new-location', [
		'as' => 'create.new.location',
		'uses' => 'AdminController@createNewBTNLocation']);

	Route::post('/add-new-university', [
		'as' => 'add.new.university',
		'uses' => 'AdminController@addNewUniversity']);

	//Create Dynamic Sitemaps
	Route::post('/create-sitemaps', [
			'as' => 'create.sitemaps',
			'uses' => 'AdminController@createSitemaps']);

	//Tutor Search
	Route::get('/tutor-profiles', [
		'as' => 'list.tutors',
		'uses' => 'SearchController@getTutors'
	]);
	
	Route::get('/tutor-profiles/search', [
		'as' => 'search.tutors',
		'uses' => 'SearchController@searchTutors'
	]);
		
	//Tutor Contracts
	Route::group(['prefix' => 'tutor-requests'], function(){
		Route::get('/', [
			'as' => 'list.tutor.requests',
			'uses' => 'TutorContractsController@listTutorRequests']);

		Route::post('/duplicate/{tutor_request_id}', [
			'as' => 'duplicate.tutor.request',
			'uses' => 'TutorRequestController@duplicateTutorRequest']);

		//TEMP: Find assignments with out tutor contract and create a contract
		/*Route::get('/fix-assign-contract/', [
			'as' => 'fix.assign.contract',
			'uses' => 'TutorContractsController@tempFixAssignCreateContract']);*/

		Route::post('/create-tutor-contract/{tutor_request_id}', [
			'as' => 'create.tutor.contract',
			'uses' => 'TutorContractsController@createTutorContract']);

		Route::post('/send-match-reminder/{method}/{tutor_request_id}', [
			'as' => 'send.match.reminder',
			'uses' => 'TutorContractsController@sendMatchReminder']);

		Route::post('/renew-tutor-proposal/{tutor_contract_ref_id}', [
			'as' => 'renew.tutor.proposal',
			'uses' => 'TutorContractsController@renewTutorProposal']);

		Route::delete('/tutor-contract/delete/{tutor_contract_id}', [
			'as' =>'delete.tutor.contract',
			'uses' => 'TutorContractsController@deleteTutorContract']);
	});

	//Handle Failed Jobs
	Route::get('/failed_jobs/', [
	'as' => 'failed.jobs',
	'uses' => 'AdminController@viewFailedJobs']);

	Route::post('/failed_jobs/{job_id}/restart', [
	'as' => 'restart.failed.job',
	'uses' => 'AdminController@restartFailedJob']);

	Route::delete('/failed_jobs/{job_id}/delete', [
	'as' => 'delete.failed.job',
	'uses' => 'AdminController@deleteFailedJob']);

	//Marketing Analysis
	Route::get('/marketing', [
	'as' => 'marketing',
	'uses' => 'AdminController@marketingBreakdown']);
	
	//Analytics
	Route::get('/analytics', [
	'as' => 'analytics',
	'uses' => 'AdminController@analytics']);
	
	Route::get('/analytics/tutors', [
	'as' => 'analytics.tutors',
	'uses' => 'AdminController@analyticsTutors']);

	Route::get('/analytics/users', [
	'as' => 'analytics.users',
	'uses' => 'AdminController@analyticsUsers']);

	//Evaluations
	Route::group(['prefix' => 'evaluations'], function(){
		Route::get('/', [
		'as' => 'show.evaluations',
		'uses' => 'AdminController@showEvaluations']);

		Route::get('eval-reminder/{type}', [
		'as' => 'eval.reminder',
		'uses' => 'AdminController@evalReminder']);

		Route::post('mark-as-reminded/{assign_id}/{type}', [
		'as' => 'mark.as.reminded',
		'uses' => 'AdminController@markAsReminded']);

		Route::post('send-eval-reminders/{type}', [
		'as' => 'send.eval.reminders',
		'uses' => 'AdminController@sendEvalReminders']);

		//TEMP: find students to contact about first session & direct payment
		Route::get('first-session-students', [
		'as' => 'first.session.students',
		'uses' => 'AdminController@firstEvalStudents']);
		
		Route::patch('{eval_id}', [
			'as' => 'review.evaluation',
			'uses' => 'AdminController@reviewEvaluation']);
	});

	//Daniels Fund
	Route::group(['prefix' => 'daniels-fund'], function(){
		Route::get('/', [
			'as' => 'daniels.fund',
			'uses' => 'DanielsFundController@index']);
		Route::post('add-daniels-scholar', [
			'as' => 'add.daniels.scholar',
			'uses' => 'DanielsFundController@addScholar']);
		Route::patch('update-payment-date/{df_charge_id}', [
			'as' =>'update.payment.date',
			'uses' => 'DanielsFundController@updatePaymentDate']);
	});

	//User Manager
	Route::group(['prefix' => 'user-manager'], function(){
		Route::get('/', [
			'as' =>'user.manager',
			'uses' => 'AdminController@userManager']);
		
		Route::get('search-users', [
			'as' => 'search.users',
			'uses' => 'AdminController@searchUsers'
		]);

		Route::get('clone-user/{user_id}', [
			'as' => 'clone.user',
			'uses' => 'AdminController@cloneUser'
			]);

		Route::get('clear-applicant/{user_id}', [
			'as' => 'clear.applicant',
			'uses' => 'BackgroundChecksController@clearApplicant'
			]);

		Route::get('user-details/{user_id}', [
			'as' =>'user.details.get',
			'uses' => 'AdminController@userDetails']);
		
		Route::post('user-details/{user_id}/avatar', [
			'as' =>'upload.avatar',
			'uses' => 'AdminController@uploadAvatar']);

		Route::delete('delete/{user_id}', [
			'as' =>'user.delete',
			'uses' => 'AdminController@deleteUser']);
		
		Route::patch('restore/{user_id}', [
			'as' =>'user.restore',
			'uses' => 'AdminController@restoreUser']);
	});

	//Advisors
	Route::group(['prefix' => 'academic-advisors'], function(){
		Route::get('/', [
			'as' =>'list.advisors',
			'uses' => 'AcademicAdvisorsController@listAdvisors']);

		Route::post('/', [
			'as' => 'add.advisor',
			'uses' => 'AcademicAdvisorsController@addAdvisor']);

		Route::post('email-advisors/{location_id}', [
			'as' => 'email.advisors',
			'uses' => 'AcademicAdvisorsController@emailAdvisors']);	

		Route::get('delete-advisor/{id}', [
			'as' =>'delete.advisor',
			'uses' => 'AcademicAdvisorsController@deleteAdvisor']);

		Route::controller('/', 'AcademicAdvisorsController', [
	    'getAdvisorsData'  => 'advisors.data',
	    'getAdvisors' => 'advisors']);
	});

	//Tutor Payments
	Route::get('tutor-payment', [
		'as' =>'tutor.payment',
		'uses' => 'TutorPaymentController@getNextPay']);

	Route::post('tutor-payment/{tutor_id}', [
		'as' =>'submit.payment',
		'uses' => 'TutorPaymentController@submitPayment']);

	//Assignments
	Route::get('unassigned-requests', [
		'as' =>'unassigned.requests',
		'uses' => 'AssignmentController@unassignedRequests'
		]);

	Route::get('declined-requests', [
		'as' =>'declined.requests',
		'uses' => 'AssignmentController@declinedRequests'
		]);

	Route::patch('assignments/make/{tutor_request_id}', [
		'as' =>'make.assignment',
		'uses' => 'AssignmentController@makeAssignment'
		]);
	
	Route::put('assignments/unassign/{tutor_request_id}', [
		'as' =>'unassign.student',
		'uses' => 'AssignmentController@unassignStudent'
		]);

	Route::delete('tutor-request/delete/{tutor_request_id}', [
		'as' =>'delete.tutor.request',
		'uses' => 'TutorRequestController@deleteTutorRequest'
		]);

	//Contractor Taxes
	Route::get('/contractor-taxes', [ 
		'as' =>'contractor.taxes',
		'uses' => 'ContractorPaymentsController@contractorTaxes'
		]);
	
	//Tutor Applicants
	Route::get('applicants/{id}', [
		'as' => 'applicant.show',	
		'uses' => 'TutorApplicantController@show']);

	Route::patch('/applicants/{id}', [
		'as' =>'applicant.update', 
		'uses' => 'TutorApplicantController@update']);

	Route::delete('/applicants/{id}', [
		'as' =>'applicant.remove', 
		'uses' => 'TutorApplicantController@removeApplicant']);

	Route::post('/applicants/offer/{id}', [
		'as' =>'applicant.offer', 
		'uses' => 'TutorApplicantController@offerPosition']);
	
	Route::post('/applicants/email', [
		'as' =>'email.applicants', 
		'uses' => 'TutorApplicantController@emailApplicants']);
	
	Route::post('/applicants/exam-results', [
		'as' =>'applicants.exam.results', 
		'uses' => 'TutorApplicantController@getExamResults']);

	//Bulk Email Users
	Route::get('list-tutors', [
		'as' => 'admin.list.tutors',
		'uses' => 'AdminController@listTutors']);
	Route::post('bulk-email-users', [
		'as' => 'bulk.email.users',
		'uses' => 'AdminController@bulkEmailUsers']);

	//Upload Departments & Courses
	Route::get('upload-courses', [
		'as' => 'list.courses',
		'uses' => 'AdminController@listCourses']);
	Route::post('upload-courses', [
		'as' => 'upload.courses',
		'uses' => 'AdminController@uploadCourses']);
	Route::post('upload-courses/test-regex', [
		'as' => 'test.regex',
		'uses' => 'AdminController@testRegex']);
	
	//Controller route MUST be at end of route list
	Route::controller('/', 'TutorApplicantController', [
    'getApplicantsData'  => 'applicants.data',
    'getApplicants' => 'applicants']);
});

//For Tutors
Route::group(['prefix' =>'tutors'], function() {	
	Route::get('become-tutor', [
		'as' => 'become.tutor',
		function(){
				//return view('btn.pages.become_tutor');
				return view('btn.pages.citybook.become_tutor');
			}]);
	Route::group(['prefix' =>'tutor-application'], function() {	
		Route::get('/', [
			'as' => 'tutor.apply',
			'uses' => 'TutorApplicantController@getTutorApplication'
			]);

		Route::post('/', [
			'as' => 'tutor.apply.submit',
			'uses' => 'TutorApplicantController@submitTutorApplication'
			]);

		Route::get('/tutor-reference/{applicant_ref_id}', [
			'as' => 'get.reference.form',
			'uses' => 'TutorApplicantController@getReferenceForm']);

		Route::post('/tutor-reference/{applicant_ref_id}', [
			'as' => 'submit.reference.form',
			'uses' => 'TutorApplicantController@submitReferenceForm']);

		Route::post('add-prof-link/{applicant_ref_id}', [
			'as' => 'add.prof.link',
			'uses' => 'TutorApplicantController@addProfLink',
			'middleware' => ['auth', 'role:tutor-applicant']]);

		Route::post('submit-package/{applicant_ref_id}', [
			'as' => 'submit.application.package',
			'uses' => 'TutorApplicantController@submitApplicationPackage',
			'middleware' => ['auth', 'role:tutor-applicant']]);
	});
	
	Route::group(['middleware' => ['auth', 'role:tutor-applicant|tutor|marketplace-tutor|music-teacher']], function() {
		Route::get('tutor-checklist', [
			'as' => 'tutor.checklist',
			function () {
			    //return view('btn.pages.tutor_checklist');
			    return view('btn.pages.citybook.tutor_checklist');
			}]);

		/*Route::get('online-tutoring', [
			'as' => 'online.tutoring.details',
			function () {
			    return view('btn.pages.online_tutoring_details');
			}]);*/

		Route::get('bufftutor-payments', [
			'as' => 'bufftutor.payments',
			'uses' =>'ContractorPaymentsController@listPayments'
			]);	

		Route::get('prof-exams/list', [
			'as' => 'prof.exam.list',
			'uses' =>'ProficiencyExamsController@getExamList'
			]);	

		Route::post('prof-exams/exam', [
			'as' => 'load.selected.exam',
			'uses' =>'ProficiencyExamsController@loadSelectedExam'
			]);

		Route::post('prof-exams/grade-exam', [
			'as' => 'grade.exam',
			'uses' =>'ProficiencyExamsController@gradeExam'
			]);		

		Route::get('contracts', [
			'as' => 'get.tutor.contract',
			'uses' =>'ContractsController@getTutorContract'
			]);


		Route::post('contracts', [
			'as' => 'submit.tutor.contract',
			'uses' =>'ContractsController@createTutorContract'
			]);

		//Background Checks
		Route::group(['prefix' => 'background-checks'], function()
		{
			Route::get('/', [
					'as' =>'background.payment', 
					'uses' => 'BackgroundChecksController@backgroundPayment'
				]);

			Route::post('/', [
					'as' =>'background.process.payment', 
					'uses' => 'BackgroundChecksController@processPayment'
				]);

			Route::get('/checkr', [
					'as' =>'show.checkr.url', 
					'uses' => 'BackgroundChecksController@showCheckrURL'
				]);
		});
		//Geolocations
		Route::post('/update-geolocations/{user_ref_id}', [
				'as' => 'update.geolocations',
				'uses' => 'ProfileController@updateGeolocations']);
	});

	Route::group(['middleware' => ['auth', 'role:tutor|music-teacher|marketplace-tutor|test-tutor']], function() {

		Route::group(['prefix' => 'stripe-connect'], function(){
			
			//DO NOT DELETE, needed for link in Tutor Checklist
			Route::get('sign-up', [
				'as' => 'stripe.connect.signup',
				'uses' => 'StripeConnectController@connectSignUp'
				]);

			//Do Not change URL.  It's called by Stripe
			Route::get('authorize', [
				'as' => 'stripe.connect.authorize',
				'uses' => 'StripeConnectController@authorizeConnect'
				]);

		});

		Route::patch('/assign/{ref_id}', [
		'as' =>'date.update', 
		'uses' => 'ProfileController@updateAssignEndDate',
		]);

		Route::post('/assign/{assign_ref_id}', [
		'as' =>'rate.create', 
		'uses' => 'ProfileController@storeRate',
		]);

		Route::patch('/rate/{assign_ref_id}', [
		'as' =>'rate.update', 
		'uses' => 'ProfileController@updateRate',
		]);

		//Tutor Requests/Contracts
		Route::group(['prefix' => 'tutor-requests'], function(){
			Route::get('/{user_ref_id}/review', [
				'as' => 'my.tutor.contracts',
				'uses' => 'TutorContractsController@getMyTutorContracts']);

			Route::post('/response/{tutor_contract_ref_id}', [
				'as' => 'tutor.contract.decision',
				'uses' => 'TutorContractsController@submitTutorDecision']);
		});

		//Find Previous Students
		Route::get('search-students', [
			'as' => 'get.search.students',
			'uses' => 'TutorController@getPreviousStudent'
			]);

		Route::post('search-students', [
			'as' => 'search.students',
			'uses' => 'TutorController@findPreviousStudent'
			]);

		Route::post('add-student/{ref_id}', [
			'as' => 'add.previous.student',
			'uses' => 'TutorController@addPreviousStudent'
			]);

		//For Tutor Sessions (Charge & Delete)
		Route::group(['prefix' => 'tutor-sessions'], function(){
			Route::get('/charge-account/{assign_ref_id}', [
			'as' =>'session.form', 
			'uses' => 'TutorSessionsController@getChargeForm',
			]);

			Route::post('/charge-account/{assign_ref_id}', [
			'as' =>'session.charge', 
			'uses' => 'TutorSessionsController@chargeTutorSession',
			]);

			Route::get('/charge-customer/{assign_ref_id}', [
			'as' =>'session.form.customer', 
			'uses' => 'TutorSessionsController@getCustomerChargeForm',
			]);

			Route::post('/charge-customer/{assign_ref_id}', [
			'as' =>'session.charge.customer', 
			'uses' => 'TutorSessionsController@chargeCustomerForSession',
			]);

			Route::delete('/session/{id}', [
			'as' =>'session.delete', 
			'uses' => 'TutorSessionsController@deleteTutorSession',
			]);

			//Charge Daniels Fund Students
			Route::get('charge-scholar/{df_id}', [
				'as' => 'get.charge.daniels.scholar',
				'uses' => 'DanielsFundController@getChargeForm'
			]);
			
			Route::post('charge-scholar/{df_id}', [
				'as' => 'charge.daniels.scholar',
				'uses' => 'DanielsFundController@chargeScholar']);

		});

		//Tutor Pages
		Route::get('course-catalogs', [
			'as' => 'course.catalogs',
			'uses' => 'TutorController@getCourseCatalog'
			]);
		Route::get('intro-email', [
				'as' => 'intro.email',
				function () {
			    	return view('btn.pages.citybook.introductory_email');
			    	//return view('btn.pages.introductory_email');
					}]);
		/*Route::get('whats-new', [
				'as' => 'whats.new',
				function () {
			    	return view('btn.pages.tutors_whats_new');
					}]);*/

		Route::get('orientation', [
				'as' => 'tutor.orientation',
				function () {
			    	//return view('btn.pages.tutor_orientation');
			    	return view('btn.pages.citybook.tutor_orientation');
					}]);
	});

	/**** Dynamic homepages - MUST BE AT END OF 'tutors' ROUTE GROUP *****/
	Route::get('/{details}', ['as' => 'home.city',function($details){
			//$details = city-state-course-tutors
			
			/*if ($details == "become-tutor") {
				return view('btn.pages.citybook.become_tutor');
			}*/
			
			$details_array = explode("-", $details);
			$city = ucwords(str_replace("_"," ",$details_array[0]));
	
			$btn_location = null;
	
			if ($btn_location = App\Location::studentsVisible()->with('universities')->where('city', $city)->where('state', $details_array[1])->first()) {
				if (count($details_array) >=3) {
					$state = strtoupper($details_array[1]);
					$course = strpos($details_array[2], 'tutor') === false ? ucwords(str_replace("_"," ",$details_array[2])) : '';
					
					$universities = [];

					if ($btn_location->universities->count()) {
						$universities = $btn_location->universities->pluck('name')->toArray();
					}
					
					$btn_name = $btn_location->company_name == 'BuffTutor' ? "BuffTutor - $btn_location->city" : "RamTutor";
					$btn_logo = $btn_location->company_name == 'BuffTutor' ? "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg" : "https://bufftutor.com/btn/logo/RamTutor_Logo_Small.png";
					
					$title = "Elite $course Tutors In $city, $state | 10% Off First Hour | BuffTutor";
					$meta = "Experienced private $course tutors in $city, $state who fit YOUR BUDGET and YOUR SCHEDULE! Satisfaction Guaranteed. Est 2009.";

					$online = false;

					if (strpos($details, "online")) {
						$online = true;

						if ($course) {
							$title = "Online $city $course Tutors | 10% Off First Hour | BuffTutor";
							$meta = "Connect with experienced & affordable online $course tutors in $city. Satisfaction Guaranteed. Est 2009.";
						}
						else{
							$title = "Online $city College & High School Tutors | 10% Off First Hour | BuffTutor";
							$meta = "Experienced & affordable online tutors for college & high school classes. Satisfaction Guaranteed. Est 2009. Get A BuffTutor today!";
						}	
					}
					return view('btn.pages.citybook.home', compact('city', 'state', 'course', 'btn_location', 'universities', 'btn_name', 'btn_logo', 'online', 'title', 'meta'));
				}
			}
			return view('btn.pages.citybook.home');
		}]);
});

//Routes For Customer Cards 
Route::group(['prefix' =>'payment-settings', 'middleware' => 'auth'], function() {
	Route::get('/', [
		'as' => 'payment.settings',
		function () {
    		//return view('btn.stripe.payment_settings');
    		return view('btn.pages.citybook.payment_settings');
		}
	]);

	Route::post('/add-card', [
		'as' => 'store.customer.card',
		'uses' => 'StripeCustomerController@storeCustomerCard'
	]);

	Route::post('update-card', [
		'as' => 'update.customer.card',
		'uses' => 'StripeCustomerController@updateCustomerCard'
	]);
});


//Routes For Payments
/*Route::group(['prefix' =>'payments', 'middleware' => 'auth'], function() {
	Route::get('/', [
		'as' => 'payment.form',
		function () {
    		return view('btn.stripe.payment_form');
	}]);

	Route::post('/card', [
			'as' => 'charge.stripe.card', 
			'uses' => 'StripeBillingController@chargeNewCard'
	]);

});*/

//For Students
Route::group(['prefix' =>'students'], function() {	
	//Tutor Requests
	Route::group(['prefix' =>'my-tutor-requests', 'middleware' => ['auth']], function() {
		Route::get('/{user_ref_id}', [
			'as' => 'my.tutor.requests',
			'uses' => 'TutorContractsController@myTutorRequests']);

		Route::post('/student-response/{tutor_contract_ref_id}', [
			'as' => 'student.contract.decision',
			'uses' => 'TutorContractsController@submitStudentDecision']);

		Route::get('/{tutor_contract_ref_id}/add-card', [
			'as' => 'tutor.contracts.add.card',
			'uses' => 'TutorContractsController@addCard']);

		Route::post('/{tutor_contract_ref_id}/add-card', [
			'as' => 'tutor.contracts.submit.card',
			'uses' => 'TutorContractsController@submitCardCreateMatch']);		
	});

	// Decline Assignment
	Route::get('assignments/decline/{assign_ref_id}',[
		'middleware' => 'auth',
		'as' => 'decline.assignment',
		'uses' => 'AssignmentController@declineAssignment'
		]);

	//For Evaluations
	Route::group(['prefix' => 'evaluations', 'middleware' => ['auth']], function(){
		Route::get('first/{assign_ref_id}', [
				'as' => 'eval.first',
				'uses' => 'EvaluationsController@getFirstEval'
		]);

		Route::post('first/{assign_ref_id}', [
				'as' => 'submit.eval.first',
				'uses' => 'EvaluationsController@submitFirstEval'
		]);

		Route::get('end/{assign_ref_id}', [
				'as' => 'eval.end',
				'uses' => 'EvaluationsController@getEndEval'
		]);
		Route::post('end/{assign_ref_id}', [
				'as' => 'submit.eval.end',
				'uses' => 'EvaluationsController@submitEndEval'
		]);
	});

	//For Refunds
	Route::group(['prefix' => 'refunds', 'middleware' => 'auth'], function(){
		/*Route::get('account', [
				'as' => 'refund.account',
				'uses' => 'RefundsController@getBalance'
		]);

		Route::post('account', [
				'as' => 'refund.account.post',
				'uses' => 'RefundsController@requestBalanceRefund'
		]);*/

		Route::get('/{session_ref_id}', [
				'as' => 'refund.get',
				'uses' => 'RefundsController@getSessionDetails'
		]);

		Route::post('/{session_ref_id}', [
				'as' => 'refund.post',
				'uses' => 'RefundsController@requestSessionRefund'
		]);
	});
});

//For MarketPlace
Route::group(['prefix' => 'marketplace', 'middleware' => 'auth'], function(){
	Route::get('/accept_proposal/{thread_id}', [
		'as' => 'marketplace.accept_proposal',	
		'uses' => 'MarketPlaceController@studentAcceptedProposal'
	]);

	Route::get('/tutor_request/accept/{tutor_request_ref_id}', [
		'as' => 'marketplace.tutor.accept.request',	
		'uses' => 'MarketPlaceController@tutorAcceptedRequest'
	]);
	
	Route::get('/tutor_request/decline/{tutor_request_ref_id}', [
		'as' => 'marketplace.tutor.decline.request',	
		'uses' => 'MarketPlaceController@tutorDeclinedRequest'
	]);

	//Middleware applied in Tutor routes
	/*Route::get('/join', [
		'middleware' => ['auth', 'role:tutor|music-teacher|test-tutor'],
		'as' => 'join.marketplace',	
		'uses' => 'MarketPlaceController@joinMarketPlace'
	]);

	Route::get('/leave', [
		'middleware' => ['auth', 'role:marketplace-tutor'],
		'as' => 'leave.marketplace',	
		'uses' => 'MarketPlaceController@leaveMarketPlace'
	]);*/
	
	/*Route::group(['prefix' => 'subject-notification', 'middleware' => ['auth', 'role:marketplace-tutor']], function(){
			Route::get('/', [
				'as' => 'subject.notifications',	
				'uses' => 'MarketPlaceController@subjectNotifications'
			]);

			Route::post('/', [
				'as' => 'store.subject.notification',	
				'uses' => 'MarketPlaceController@storeSubjectNotification'
			]);

			Route::patch('/update/{notification_id}', [
				'as' => 'update.subject.notification',	
				'uses' => 'MarketPlaceController@updateSubjectNotification'
			]);
			
			 
			// Gives Error Controller method not found
			// Route::get('/update-all/{$notify}', [
			// 	'as' => 'update.all.notifications',	
			// 	'uses' => 'MarketPlaceController@updateAllNotifications'
			// ]);
			
		
			Route::get('/delete/{notification_id}', [
				'as' => 'delete.subject.notification',	
				'uses' => 'MarketPlaceController@deleteSubjectNotification'
			]);
			Route::get('/delete-all', [
				'as' => 'delete.all.notifications',	
				'uses' => 'MarketPlaceController@deleteAllNotifications'
			]);
		});*/

	/*Route::group(['middleware' => ['auth', 'marketplace']], function(){

		Route::get('tutor-request/{ref_id}', [
			'as' => 'tutor_request.show',	
			'uses' => 'MarketPlaceController@showRequestDetails'
		]);

		Route::post('tutor-request/{ref_id}', [
			'as' => 'tutor_request.proposal',	
			'uses' => 'MarketPlaceController@submitProposal'
		]);

		//List all tutor requests on MarketPlace
		//Controller route MUST be at end of route list
		Route::controller('/', 'MarketPlaceController', [
		    'getTutorRequestsData'  => 'marketplace.data',
		    'getTutorRequests' => 'marketplace',
		]);
	});*/
});

//Display & Search Tutors
/*Route::group(['prefix' => 'tutor-profiles'], function(){
	Route::get('/', [
		'as' => 'list.tutors',
		'uses' => 'SearchController@getTutors'
	]);
	Route::get('/search', [
		'as' => 'search.tutors',
		'uses' => 'SearchController@searchTutors'
	]);

	Route::get('/{city}', [
		'as' => 'city.list',
		'uses' => 'SearchController@cityTutorList'
	]);
});

Route::get('/{city}-tutors', [
	'as' => 'city.tutor.list',
	'uses' => 'SearchController@cityTutorList'
]);*/

//For requesting a tutor forms: General form and request specific tutor form
Route::get('request-a-tutor', [
		'as' => 'request.get',
		'uses' => 'TutorRequestController@getRequestForm'
]);

Route::post('request-a-tutor', [
		'as' => 'request.post',
		'uses' => 'TutorRequestController@submitRequestForm'
]);


//For Profile (view profile & contact tutor from profile)
Route::group(['prefix' => 'tutor-profiles'], function(){
	Route::get('/tutor/{ref_id}', [
		'as' =>'profile.index', 
		'uses' => 'ProfileController@getProfile'
		]);
	
	Route::get('/tutor/{ref_id}/reviews', [
		'as' =>'tutor.reviews', 
		'uses' => 'ProfileController@getTutorReviews'
		]);

	//Accessible by all authenticated users
	Route::group(['middleware' => 'auth'], function(){
		/*Route::post('/contact/{ref_id}', [
			'as' =>'contact.tutor', 
			'uses' => 'TutorRequestController@contactTutorDirectly',
		]);*/

		Route::get('/edit/{user_ref_id}', [
			'as' =>'profile.edit', 
			'uses' => 'ProfileController@editProfile',
		]);

		Route::patch('/edit/{user_ref_id}', [
			'as' =>'profile.edit', 
			'uses' => 'ProfileController@updateProfile',
		]);

		Route::patch('/edit_contact/{user_ref_id}', [
			'as' =>'profile.edit_contact', 
			'uses' => 'ProfileController@updateContact',
		]);
	});
});

//For Account
Route::group(['prefix' => 'my-account', 'middleware' => 'auth'], function () {
	Route::get('/', [
		'as' => 'my.account',
		function () {
    		return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
		}]);
	Route::get('/sign-student-contract', [
		'as' =>'sign.student.contract', 
		'uses' => 'ContractsController@signStudentContract',
	]);

	Route::get('/remove/{role}/{user_ref_id}/', [
		'as' => 'remove.role',
		'uses' => 'ProfileController@removeRoleFromUser',
		]);

	Route::get('/{user_ref_id}', [
		'as' =>'profile.account', 
		'uses' => 'ProfileController@getAccount',
	]);	
});
//For Messages Package. 
//Source: https://github.com/cmgmyr/laravel-messenger/blob/master/src/Cmgmyr/Messenger/examples/routes.php
/*Route::group(['prefix' => 'messages', 'middleware' => 'auth'], function () {
    Route::get('/', [
    	'as' => 'messages', 
    	'uses' => 'MessagesController@index'
    ]);
   // Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', [
    	'as' => 'messages.store', 
    	'uses' => 'MessagesController@store'
    ]);
    
    Route::get('{user_ref_id}/{id}', [
    	'as' => 'messages.show', 
    	'uses' => 'MessagesController@show']);

    Route::put('{user_ref_id}/{id}', [
    	'as' => 'messages.update', 
    	'uses' => 'MessagesController@update']);

    Route::delete('destroy/{thread_id}', [
    	'as' => 'message.delete', 
    	'uses' => 'MessagesController@destroy'
    ]);
});*/

//Routes For Pages (About, Policies)
Route::get('/cancellation_policy', [
	'as' => 'cancellation.policy',
	function () {
    	//return view('btn.pages.cancellation_policy');
		return view('btn.pages.citybook.cancellation_policy');
}]);

Route::get('/refund_policy', [
	'as' => 'refund.policy',
	function () {
    	//return view('btn.pages.refund_policy');
		return view('btn.pages.citybook.refund_policy');
}]);

Route::get('/promotions', [
	'as' => 'promotions',
	function () {
    	//return view('btn.pages.promotions');
    	return view('btn.pages.citybook.promotions');
}]);

Route::get('/terms-of-use', [
	'as' => 'terms',
	function () {
    	//return view('btn.pages.terms_of_use');
    	return view('btn.pages.citybook.terms_of_use');
}]);

Route::get('/locations', [
	'as' => 'locations',
	function () {
		$locations = App\University::where('visible', 1)->select('city', 'state', 'state_abbr')->get()->sortBy('state')->unique('city');
		/** MUST send Universities as $locations since they have state_abbr and state **/

		$states = $locations->pluck('state', 'state_abbr')->unique();
		$chunk_size = ceil(count($states) / 4);
    	return view('btn.pages.citybook.locations', compact('locations', 'states', 'chunk_size'));
}]);

Route::get('/courses-tutored', [
	'as' => 'courses',
	function () {
		$courses = collect(DB::table('sitemap_courses')->where('active', 1)->get())->unique('course')->sortBy('course');
		$chunk_size = ceil(count($courses) / 6);
    	return view('btn.pages.citybook.courses', compact('courses', 'chunk_size'));
}]);

// SAME AS '/courses-tutored' route code, uses same view
Route::get('/courses-tutored/{university}', [
	'as' => 'university.courses',
	function ($university) {
		$university = ucwords(str_replace("_"," ", $university));
		$courses = collect(DB::table('sitemap_courses')->where('active', 1)->get())->unique('course')->sortBy('course');
		$chunk_size = ceil(count($courses) / 6);
    	return view('btn.pages.citybook.courses', compact('university','courses', 'chunk_size'));
}]);

Route::get('/courses-tutored/t/{city}-{state}', [
	'as' => 'city.courses',
	function ($city, $state) {
		$city = ucwords(str_replace("_"," ",$city));
		$state = strtoupper($state);
		
		$courses = collect(DB::table('sitemap_courses')->where('active', 1)->get())->unique('course')->sortBy('course');
		$chunk_size = ceil(count($courses) / 6);
    	return view('btn.pages.citybook.courses', compact('city', 'state', 'courses', 'chunk_size'));
}]);

Route::get('/colleges', [
	'as' => 'colleges',
	function () {
		$uni_list = App\University::where('visible', 1)->select('name', 'state', 'state_abbr')->get()->sortBy('state');
		
		$states = $uni_list->pluck('state', 'state_abbr')->unique();

		$chunk_size = ceil(count($states) / 6);
    	
    	return view('btn.pages.citybook.colleges', compact('uni_list', 'states','chunk_size'));
}]);

Route::get('/college-courses', [
	'as' => 'college.courses',
	function () {
		$courses_count = 0;
		$courses = [];
		$uni_list = App\University::where('visible', 1)
									->whereHas('courses', function($q){
										$q->where('visible', 1);
									})
									->with('courses')
									->select('id','name')
									->get()
									->sortBy('name');
		
		foreach ($uni_list as $uni) {
			$uni_courses = [];
			foreach ($uni->courses->filter(function ($course){return $course->visible;})->sortBy('course_number') as $course) {
				array_push($uni_courses, $course);
				$courses_count++;
			}
			$courses[$uni->id] = $uni_courses;
		}
		
		$uni_array = $uni_list->pluck('name', 'id')->toArray();
		
		// $chunk_size = ceil(count($uni_list) / 6);
		$chunk_size = ceil($courses_count / 4);

    	return view('btn.pages.citybook.college_courses', compact('courses', 'uni_array','chunk_size'));
}]);