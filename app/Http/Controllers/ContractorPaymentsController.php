<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\ContractorPayment;
use App\User;

class ContractorPaymentsController extends Controller
{
    public function listPayments()
    {
    	$user = auth()->user();
    	//$user = User::find(365); //FOR TESTING

    	$ytd_payments = ContractorPayment::where('contractor_id', '=', $user->id)
    					->where('created_at', '>', date('Y'))
    					->get();
    	$last_year_payments = ContractorPayment::where('contractor_id', '=', $user->id)
    							->where('created_at', '>', date('Y')-1)
    							->where('created_at', '<', date('Y'))
    							->get();
    	return view('btn.taxes.contractor_payments', compact('ytd_payments', 'last_year_payments'));
    }

    public function contractorTaxes()
    {
        $ytd_payments = ContractorPayment::where('created_at', '>', date('Y'))
                        ->get();
        $last_year_payments = ContractorPayment::where('created_at', '>', date('Y')-1)
                                ->where('created_at', '<', date('Y'))
                                ->get();
        $contractors = User::whereIN('id', $ytd_payments->pluck('contractor_id'))
                                ->orWhereIN('id', $last_year_payments->pluck('contractor_id'))
                                ->orderBy('id', 'ASC')
                                ->get();
        //dd($contractors);
        return view('btn.taxes.admin_contractor_taxes', compact('ytd_payments', 'last_year_payments', 'contractors'));
    }
}
