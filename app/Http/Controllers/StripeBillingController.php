<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\User;
use App\Customer;
use App\Payment;


class StripeBillingController extends Controller
{

	public function chargeNewCard(Request $request){

		$this->validate($request, ["amount" => 'required|numeric|min:1']);

		$customer_id = null;
		$user_id = Auth::user()->id;			

		$amount = number_format($request->input("amount"), 2);
		/*$processing_fee = number_format(($amount * 0.03), 2);*/
		/*$total_amount = number_format(($amount + $processing_fee), 2);*/
		$total_amount = $amount;
		$amount_stripe = $total_amount * 100; //amount in cents

		$payment_method = 1; //1 = stripe

		//Create Payment with default values in case charge fails
		$description = "Payment for BuffTutor Sessions";
		$transaction_id = rand(10000000, 99999999); //set transaction id in case charge fails
		$payment = Payment::create([
				"user_id" => $user_id,
				"transaction_id" => $transaction_id,
				"amount" => $amount,
				/*"processing_fee" => $processing_fee,*/
				"total_amount" => $total_amount,
				"payment_method" => $payment_method,
				"captured" => 0,
			    "paid" => 0
			]);

		// REF: https://stripe.com/docs/charges 06/26/2016

		\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

		// Get the credit card details submitted by the form
		$token = $_POST['stripeToken'];

		// Create the charge on Stripe's servers - this will charge the user's card

		try {
		  
			//Save Customer & Charge Card
			if ($request->input("save_customer")) {
				$customer = $this->makeCustomer($token, $user_id);
				$customer_id = $customer->id;

				// Charge the Customer instead of the card
				$charge = \Stripe\Charge::create(array(
					  "amount" => $amount_stripe, // amount in cents, again
					  "currency" => "usd",
					  "customer" => $customer_id,
					  "description" => $description
			  	));
			}

			//Or just charge card
			else
			{	
			  $charge = \Stripe\Charge::create(array(
				    "amount" => $amount_stripe, // amount in cents, again
				    "currency" => "usd",
				    "source" => $token,
				    "description" => $description,
				    "receipt_email" => Auth::user()->email,
			    ));
			}

		  if ($charge) {
			$payment->transaction_id = $charge->id;
			$payment->stripe_customer_id = $customer_id;
			$payment->captured = $charge->captured;
			$payment->paid = $charge->paid;
			$payment->destination_tutor = $charge->destination;
			$payment->application_fee = $charge->application_fee;
			$payment->completed_at = date("Y-m-d H:i:s", $charge->created); 
			$payment->save();
		  }
		  
		  return view('btn.stripe.success')->with('payment', $payment);

		} catch (\Stripe\Error\ApiConnection $e) {
 		   // Network problem, perhaps try again.
			$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			return view('btn.stripe.error')->with('error', $error);

		} catch (\Stripe\Error\InvalidRequest $e) {
    		// You screwed up in your programming. Shouldn't happen!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			return view('btn.stripe.error')->with('error', $error);

		} catch (\Stripe\Error\Api $e) {
    		// Stripe's servers are down!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			return view('btn.stripe.error')->with('error', $error);

		} catch (\Stripe\Error\Card $e) {
    		// Card was declined.
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			return view('btn.stripe.error')->with('error', $error);
		}
	}

	public function makeCustomer($token, $user_id){
		$user = User::where('id', $user_id)->first();
	
		// Create a Customer
		$customer = \Stripe\Customer::create([
			"source" => $token,
		  	"description" => $user->full_name." - ".$user->id." - ".$user->email,
		  	"email" => $user->email
		  ]
		);

		if ($customer) {
		  	$btn_customer =  Customer::create([
				"user_id" => $user_id,
				"stripe_customer_id" => $customer->id,
				"card_brand" => $customer->sources->data[0]->brand,
				"card_last_four" => $customer->sources->data[0]->last4,
				"exp_month" => $customer->sources->data[0]->exp_month,
				"exp_year" => $customer->sources->data[0]->exp_year
			]);
		}

		  if (!$btn_customer) {
		  	abort(500, 'Could not save card details.  Please try again.');
		  }

		  return $customer;
	}	
}
