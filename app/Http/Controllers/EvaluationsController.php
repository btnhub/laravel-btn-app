<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Assignment;
use App\Evaluation;
use App\User;
use Auth;

use App\Jobs\SendGeneralNotification;

class EvaluationsController extends Controller
{
    public function getFirstEval($assign_ref_id)
    {	
    	$assignment = Assignment::where('ref_id', $assign_ref_id)
    								->select('id', 'ref_id', 'tutor_id', 'student_id', 'course')
    								->first();
		$this->authorize('student-access', $assignment);

        \Session::flash('info', 'Please submit the form under the Evaluations tab below.');
        return redirect()->route('profile.account', $assignment->student->ref_id);
    	//return view('btn.forms.eval_first', compact('assignment'));
    }

    public function OLDsubmitFirstEval(Request $request, $assign_ref_id)
    {	
    	$input = $request->all();
    	$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
    	
        $this->authorize('student-access', $assignment);

    	$input['assignment_id'] = $assignment->id;
    	$input['student_id'] = Auth::user()->id;
    	$input['tutor_id'] = $assignment->tutor_id;
    	$input['type'] = 'First';

    	$flash_type = 'success';
		$flash_msg = 'Evaluation successfully submitted';

    	if(!Evaluation::create($input))
    	{
    		$flash_type = 'error';
			$flash_msg = 'Could not submit evaluation! Please try again.';
    	}

        //E-mail Tutor
        $user = User::find($assignment->tutor_id);
        $location = $assignment->location;
        $subject = "Evaluation Submitted";
        $email_view = 'btn.emails.tutors.eval_submitted';
        $email_data = [];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

		\Session::flash($flash_type, $flash_msg);
    	
    	return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }

    public function getEndEval($assign_ref_id)
    {	
    	$assignment = Assignment::where('ref_id', $assign_ref_id)
							->select('id', 'ref_id', 'tutor_id', 'student_id', 'course')
							->first();
        $this->authorize('student-access', $assignment);

        \Session::flash('info', 'Please submit the form under the Evaluations tab below.');
        return redirect()->route('profile.account', $assignment->student->ref_id);
        
    	return view('btn.forms.eval_end', compact('assignment'));
    }

    public function OLDsubmitEndEval(Request $request, $assign_ref_id)
    {	
    	$input = $request->all();
    	$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
    	
        $this->authorize('student-access', $assignment);

    	$input['assignment_id'] = $assignment->id;
    	$input['student_id'] = Auth::user()->id;
    	$input['tutor_id'] = $assignment->tutor_id;
    	$input['type'] = 'End';
    	$input['school'] = $assignment->student->profile->school;

    	$flash_type = 'success';
		$flash_msg = 'Evaluation successfully submitted';

    	if(!Evaluation::create($input))
    	{
    		$flash_type = 'error';
			$flash_msg = 'Could not submit evaluation! Please try again.';
    	}
        
        //E-mail Tutor
        $user = User::find($assignment->tutor_id);
        $location = $assignment->location;
        $subject = "Evaluation Submitted";
        $email_view = 'btn.emails.tutors.eval_submitted';
        $email_data = [];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

		\Session::flash($flash_type, $flash_msg);
    	
    	return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }

    /*********CITYBOOK************/
    public function submitFirstEval(Request $request, $assign_ref_id)
    {   
        //$input = $request->all();
        $assignment = Assignment::where('ref_id', $assign_ref_id)->first();
        
        $this->authorize('student-access', $assignment);

        $input = $request->except('_token', 'star_rating');
        $input['assignment_id'] = $assignment->id;
        $input['student_id'] = Auth::user()->id;
        $input['tutor_id'] = $assignment->tutor_id;
        $input['type'] = 'First';
        $input['rating'] = $request->star_rating;

        $flash_type = 'success';
        $flash_msg = 'Evaluation successfully submitted';

        if(!Evaluation::create($input))
        {
            $flash_type = 'error';
            $flash_msg = 'Could not submit evaluation! Please try again.';
        }

        //E-mail Tutor
        $user = User::find($assignment->tutor_id);
        $location = $assignment->location;
        $subject = "Evaluation Submitted";
        $email_view = 'btn.emails.tutors.eval_submitted';
        $email_data = [];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash($flash_type, $flash_msg);
        
        return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }

    public function submitEndEval(Request $request, $assign_ref_id)
    {   
        //$input = $request->all();
        $assignment = Assignment::where('ref_id', $assign_ref_id)->first();
        
        $this->authorize('student-access', $assignment);

        $input = $request->except('_token', 'star_rating');
        $input['assignment_id'] = $assignment->id;
        $input['student_id'] = Auth::user()->id;
        $input['tutor_id'] = $assignment->tutor_id;
        $input['type'] = 'End';
        $input['school'] = $assignment->student->profile->school;
        $input['rating'] = $request->star_rating;

        $flash_type = 'success';
        $flash_msg = 'Evaluation successfully submitted';

        if(!Evaluation::create($input))
        {
            $flash_type = 'error';
            $flash_msg = 'Could not submit evaluation! Please try again.';
        }
        
        //E-mail Tutor
        $user = User::find($assignment->tutor_id);
        $location = $assignment->location;
        $subject = "Evaluation Submitted";
        $email_view = 'btn.emails.tutors.eval_submitted';
        $email_data = [];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash($flash_type, $flash_msg);
        
        return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }
}
