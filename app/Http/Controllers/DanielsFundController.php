<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon;
use App\MarketTutorRequest;
use App\User;
use App\Student;
use App\Tutor;
use App\Profile;
use App\Assignment;
use App\TutorSession;
use App\DanielsFundScholar;
use App\DanielsFundCharge;
use App\StripeConnectTutor;

use App\Jobs\EmailAdmin;
use App\Jobs\SendGeneralNotification;



class DanielsFundController extends Controller
{
    public function index() {
    	$scholars = DanielsFundScholar::whereDate('end_date', '>', Carbon\Carbon::now()->subWeeks(2))->get();

    	$scholar_requests = MarketTutorRequest::whereNotNull('scholarship_program')->pluck('student_id');

    	$students = Student::whereIn('id', $scholar_requests)->orderBy('id', 'DESC')->get()->pluck('full_name', 'id')->toArray();
		$students = [null => "Select A Student"] + $students;
    	
    	$tutors = Tutor::activeTutors()->orderBy('first_name', 'ASC')->get()->pluck('full_name', 'id')->toArray();
    	$tutors = [null => "Select A Tutor"] + $tutors;
    	
    	return view('btn.daniels_fund.index', compact('scholars', 'students', 'tutors')); 
    }

    /**
     * Add Approved Daniels Fund Student and corresponding assignment
     */
    public function addScholar(Request $request) {
    	//TO DO: E-mail Tutor instructions, Email Student Instructions
    	//Assignment ID

    	$rate = $request->rate;
    	$hours = $request->hours;
    	$total = $rate * $hours;

    	$scholar = DanielsFundScholar::create([
    		'student_id' => $request->student,
    		'tutor_id' => $request->tutor,
    		'assignment_id' => $request->assignment_id,
    		'start_date' => $request->start_date,
    		'end_date' => $request->end_date,
    		'course' => $request->course,
    		'rate' => $rate,
    		'hours' => $hours,
    		'total' => $total,
    		'notes' => $request->notes
    		]);
    	
    	//Add Daniels Fund Student role
    	$scholar->student->addRole('daniels-fund-student');

    	\Session::flash('success', 'Daniels Fund Scholar Added');

    	return redirect()->back();
    	
    }
    
    /**
     * Update Payment Date When Invoice Was Paid By Daniels Fund
     */
    public function updatePaymentDate($df_charge_id)
    {
    	$df_charge = DanielsFundCharge::find($df_charge_id);

    	$df_charge_set = DanielsFundCharge::whereMonth('created_at', '=', $df_charge->created_at->format('m'))
    					->where('df_assignment_id', $df_charge->df_assignment_id)
    					->whereNull('payment_date')
    					->update(['payment_date' => Carbon\Carbon::now()]);

    	\Session::flash('success', "Payment Date Updated For {$df_charge->created_at->format('F')}'s Invoice For Sessions Between {$df_charge->student->full_name} & {$df_charge->assigned_tutor->full_name}");
    	return redirect()->back();
    }
    
    public function getChargeForm($df_id)
	{
		$df_assignment = DanielsFundScholar::find($df_id);
		$this->authorize('tutor-access', $df_assignment);

		$tutor_fee = Profile::where('user_id', $df_assignment->assigned_tutor->id)->first()->tutor_fee;
		$tutor_rate = Assignment::find($df_assignment->assignment_id)->tutorContract->tutor_rate;

		$session_duration_array = ['' => "Select How Long The Session Lasted",
									30 => "30 min",
									45 => "45 min",
									60 => "1 hour",
									75 => "1 hour & 15 min",
									90 => "1 hour & 30 min",
									105 => "1 hour & 45 min",
									120 => "2 hours",
									135 => "2 hours & 15 min",
									150 => "2 hours & 30 min",
									165 => "2 hours & 45 min",
									180 => "3 hours"						
									];
		return view('btn.forms.session_charge_daniels_fund', compact('df_assignment', 'tutor_fee', 'tutor_rate', 'session_duration_array'));
	}

    public function chargeScholar(Request $request, $df_id)
    {	
		//Charge student's Stripe Customer account
		$df_assignment = DanielsFundScholar::find($df_id);
		$assignment = Assignment::find($df_assignment->assignment_id);

		if ($df_assignment->tutor_id != auth()->user()->id) {
			\Session::flash('error', 'Unauthorized Action');
			return redirect()->back();
		}
		
		$this->validate($request,[
			/*'amt_charged' => 'required|min:0',*/
			'outcome' => 'required',
			// 'tutor_pay' => 'required|min:0',
			]);
		
		$input = $request->all();

		$student_rate = $assignment->tutorContract->student_rate;
		$billing_increment = 60; //All rates are hourly = 60 min

		if ($request->outcome == 'Free Session' || $request->outcome == 'Tutor Cancelled Without 24 Hours Notice') {
			$amt_charged = 0;
			$btn_pay = 0;
			$tutor_pay = 0;
			$discount_percent = 0;
			$discount = 0;
		}
		
		else
		{			
			$tutor_pay = $assignment->tutorContract->tutor_rate * ($request->duration / $billing_increment);

			$amt_charged = $student_rate * ($request->duration / $billing_increment);
			$btn_pay = number_format($amt_charged - $tutor_pay,2);
		}

		$charge = null;

		if ($amt_charged > 0) {
			//Transfer money to tutor from BTN Stripe Account

			//Get Tutor's Stripe Account
			if(!$stripe_user = StripeConnectTutor::where('user_id', auth()->user()->id)->first())
			{
				\Session::flash('error', 'You are not connected to our Platform.  We cannot forward this payment to your bank.');	
				return redirect()->back();
			}

			$stripe_user_id = $stripe_user->stripe_user_id;		

			$tutor_amount = number_format($tutor_pay, 2);
			//$btn_pay = number_format($request->btn_pay, 2);

			$amount_stripe = $tutor_amount * 100; //amount in cents

			$destination = $stripe_user_id;
			$payment_method = 1; //1 = stripe

			$description = "BuffTutor Session: {$df_assignment->assigned_tutor->full_name} & {$df_assignment->student->full_name}";

			\Stripe\Stripe::setApiKey(config('services.stripe.secret'));
			
			
			try {
				//Check Stripe Balance and make sure it's enough to pay tutor
				$balance = \Stripe\Balance::retrieve();

				$available_balance = $balance->available[0]['amount'];

				if($available_balance < $amount_stripe) {
					\Session::flash('error', "We are unable to submit this charge at this moment.  We will contact you and inform you when you can submit this charge.  We apologize for the inconvenience and delay in your payment.");


					//E-mail Admin
					$submitted_by = auth()->user()->full_name;

			        $location = $df_assignment->student->locations->first();
			        $subject = "Daniels Fund Charge Failed: Insufficient Funds";
			        $email_view = 'btn.emails.admin.daniels_fund_charge_failed';
			        $email_data = ['df_assignment' => $df_assignment, 'tutor_pay' => $tutor_amount, 'available_balance' => ($available_balance / 100), 'submitted_by' => $submitted_by];
			        
			        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

					return redirect()->back();
				}

				else {
					try {
						//Transfer Money
						$transfer = \Stripe\Transfer::create(
							[
							  "amount" => $amount_stripe, // amount in cents, again
							  "currency" => "usd",
							  "description" => $description,
							  "destination" => $destination,
					  		]
						);

						$input['stripe_transfer_id'] = $transfer->id;

					} catch (\Stripe\Error\ApiConnection $e) {
			 		   // Network problem, perhaps try again.
						$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];

						\Session::flash('error', $error['message']);
						return redirect()->back();

					} catch (\Stripe\Error\InvalidRequest $e) {
			    		// You screwed up in your programming. Shouldn't happen!
			    		$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];
						
						\Session::flash('error', $error['message']);
						return redirect()->back();

					} catch (\Stripe\Error\Api $e) {
			    		// Stripe's servers are down!
			    		$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];
						
						\Session::flash('error', $error['message']);
						return redirect()->back();

					} catch (\Stripe\Error\Card $e) {
			    		// Card was declined.
			    		$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];
						
						\Session::flash('error', $error['message']);
						return redirect()->back();
					}
				} // else

			} catch (\Stripe\Error\ApiConnection $e) {
	 		   // Network problem, perhaps try again.
				$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];

				\Session::flash('error', $error['message']);
				return redirect()->back();

			} catch (\Stripe\Error\InvalidRequest $e) {
	    		// You screwed up in your programming. Shouldn't happen!
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				\Session::flash('error', $error['message']);
				return redirect()->back();

			} catch (\Stripe\Error\Api $e) {
	    		// Stripe's servers are down!
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				\Session::flash('error', $error['message']);
				return redirect()->back();

			} catch (\Stripe\Error\Card $e) {
	    		// Card was declined.
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				\Session::flash('error', $error['message']);
				return redirect()->back();
			}
			
		}

		//Create Tutor Sessions charge for database
		$input['ref_id'] = mt_rand(10000000, 99999999);
		$input['assignment_id'] = $df_assignment->assignment_id;
		$input['student_id'] = $df_assignment->student_id;
		$input['tutor_id'] = $df_assignment->tutor_id;
		$input['duration'] = $input['duration'] / 60; //convert to hours
		$input['amt_charged'] = $amt_charged;
		$input['tutor_pay'] = $tutor_pay;
		$input['btn_pay'] = $btn_pay;
		$input['tutor_payment_date'] = Carbon\Carbon::now();
		$input['payment_method'] = "stripe-connect";

		if(!$tutor_session = TutorSession::create($input))
		{
			\Session::flash('error', "We've transferred the funds to your Stripe account but could not save the session information to our database! Please e-mail us the student's name, session date and duration and we'll manually create an entry.");	
			return redirect()->back();
		}
	
		//Create Daniels Fund Charge for database
		$daniels_charge = DanielsFundCharge::create([
			'df_assignment_id' => $df_assignment->id,
			'assignment_id' => $df_assignment->assignment_id,
			'session_id' => $tutor_session->id,
			'student_id' => $df_assignment->student_id,
			'tutor_id' => $df_assignment->tutor_id,
			'student_id' => $df_assignment->student_id,
			'amt_charged' => $amt_charged,
			'tutor_pay' => number_format($tutor_pay,2),
			'btn_pay' => $btn_pay
			]);

		$flash_type = 'success';
		$flash_msg = "{$tutor_session->student->first_name}'s charged and you just got paid!  Check your Stripe account.";

		//E-mail Student
        $user = User::find($tutor_session->student_id);
        $location = $tutor_session->assignment->location;
        $subject = "Tutor Session Charged";
        $email_view = 'btn.emails.students.session_charged';
        $email_data = ['tutor_session' => $tutor_session];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

		\Session::flash($flash_type, $flash_msg);

		return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
	}

    public function chargeMissedSession()
    {

    }
}
