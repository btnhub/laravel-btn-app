<?php
//Source: https://github.com/cmgmyr/laravel-messenger/blob/master/src/Cmgmyr/Messenger/examples/MessagesController.php

namespace App\Http\Controllers;
use App\User;
use App\Tutor;
use App\Location;
use App\Assignment;
use App\MarketTutorRequest;
use App\MarketTutorProposal;
use Carbon\Carbon;

use App\MsgThread as Thread;
use App\MsgMessage as Message;
use App\MsgParticipant as Participant;

/*use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;*/
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use App\Events\MessageWasSent;
use App\Jobs\SendGeneralNotification;

class MessagesController extends Controller
{
    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index()
    {
        $currentUserId = Auth::user()->id;

        //BTN ADDED:  Suspicious Threads For Admin Review
        if (Auth::user()->hasRole('super-user')) {

            $threads = Thread::suspiciousThreads()
                                ->orderBy('id', 'DESC')
                                ->get();
            return view('messenger.btn_index_admin', compact('threads', 'currentUserId'));
        }

        // All threads, ignore deleted/archived participants
        //$threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
        $threads = Thread::where('msg_threads.updated_at', '>', Carbon::now()->subMonths(3))->forUser($currentUserId)->latest('updated_at')->get();

        // All threads that user is participating in, with new messages
        //$threads = Thread::forUserWithNewMessages($currentUserId)->latest('updated_at')->get();
        return view('messenger.btn_index', compact('threads', 'currentUserId'));
    }
    /**
     * Shows a message thread.
     *
     * @param $id
     * @return mixed
     */
    public function show($user_ref_id, $id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            //Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            Session::flash('error_message', 'Message was not found.');
            return redirect()->route('messages');
        }
        // show current user in list if not a current participant
        // $users = User::whereNotIn('id', $thread->participantsUserIds())->get();
        // don't show the current user in list
        $userId = Auth::user()->id;

        if (Auth::user()->hasRole('super-user')) {
            return view('messenger.btn_show', compact('thread'));    
        }
        //Check if user is part of conversation
        if (! $thread->hasParticipant($userId)) {
            Session::flash('error_message', 'Unauthorized Action.');
            return redirect()->route('messages');
        }
        //$users = User::whereNotIn('id', $thread->participantsUserIds($userId))->get(); //all other users not involved in thread
        $thread->markAsRead($userId);
        //return view('messenger.show', compact('thread', 'users'));
        return view('messenger.btn_show', compact('thread'));
    }
    /**
     * Creates a new message thread.
     *
     * @return mixed
     */
    public function create()
    {
        $users = User::where('id', '!=', Auth::id())->get();
        return view('messenger.create', compact('users'));
    }
    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store()
    {
        $input = Input::all();
        $thread = Thread::create(
            [
                'subject' => $input['subject'],
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'body'      => $input['message'],
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
                'last_read' => new Carbon,
            ]
        );
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipants($input['recipients']);
        }
        return redirect()->route('messages');
    }
    
    /**
     * Adds a new message to a current thread.
     *
     * @param $id
     * @return mixed
     */
    public function update($user_ref_id, $id)
    {
        try {
            $thread = Thread::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            //Session::flash('error_message', 'The thread with ID: ' . $id . ' was not found.');
            Session::flash('error_message', 'Message was not found.');
            return redirect()->route('messages');
        }
        $thread->activateAllParticipants();
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::id(),
                'body'      => Input::get('message'),
            ]
        );
        // Add replier as a participant
        $participant = Participant::firstOrCreate(
            [
                'thread_id' => $thread->id,
                'user_id'   => Auth::user()->id,
            ]
        );
        $participant->last_read = new Carbon;
        $participant->save();
        // Recipients
        if (Input::has('recipients')) {
            $thread->addParticipants(Input::get('recipients'));
        }


        //BTN ADDED: Contact message participants when new message is created. 
        $sender = Auth::user()->first_name;
        $participant_ids = $thread->participants()->where('user_id', '!=', Auth::user()->id)->pluck('user_id');
        //$msg_participants = User::whereIN('id', $participant_ids)->get(); //Change and add loop if group conversations are ever allowed
        $msg_participants = User::whereIN('id', $participant_ids)->first();
        $msg_body = Input::get('message');
        
        //Notify Recipient
        if ($thread->tutor_request) {
            $location = $thread->tutor_request->tutor_location;
        }
        elseif ($thread->tutor_proposal) {
            $location = $thread->tutor_proposal->tutor_request->tutor_location;
        }
        else
        {
            $location = Location::first();
        }
    
        $user = $msg_participants;
        $subject = "A new message from $sender";
        $email_view = 'btn.emails.new_message_notify_user';
        $email_data = ['user' => $msg_participants, 'body' => $msg_body, 'sender' => $sender];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        return redirect()->route('messages.show', ['id' => $id, 'user_ref_id' => $user_ref_id]);
    }
    /**
     * BTN ADDED: Delete message
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($thread_id)
    {
        $this->authorize('admin-only');

        $thread = Thread::findOrFail($thread_id);
    
        $thread->delete();

        \Session::flash('success', 'Message successfully deleted!');
        return redirect()->back();

    }
}