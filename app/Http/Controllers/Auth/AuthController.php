<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use App\Profile;
use DB;
use Mail;
use Carbon;
use App\Role;
use App\Location;
use App\TutorApplicant;

use Illuminate\Http\Request;

use App\Jobs\CreateNewUser;
use App\Jobs\SendWelcomeEmail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/my-account';
    protected $redirectAfterLogout = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:20',
            'password' => 'required|min:6|regex:/^\S*$/|confirmed',
            /*'city' => 'required',*/
            'role' => 'required',
            /*'major' => 'required|max:255',
            'school' => 'required_if:role,student|max:255',
            'year' => 'required_if:role,student|max:255',*/
            'terms_of_use' => 'required',
            'marketing' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $role_type = $data['role'];

        //$location = Location::where('id', $data['city'])->first();

        $user = User::create([
            'ref_id' => rand(10000000, 99999999),
            'first_name' => ucwords(trim($data['first_name'])),
            'last_name' => ucwords(trim($data['last_name'])),
            'email' => trim($data['email']),
            'phone' => trim($data['phone']),
            'password' => bcrypt($data['password']),
            'last_login_date' => Carbon\Carbon::now(),
            'formatted_address' => $data['formatted_address'] != '' ? $data['formatted_address'] : $data['location'] ?? null,
            'city' => $data['city'] ?? null,
            'region' => $data['region'] ?? null,
            'country' => $data['country'] ?? null,
            'postal_code' => $data['postal_code'] ?? null,
            'lat' => $data['lat'] ?? null,
            'lng' => $data['lng'] ?? null
        ]);

        /*if ($user->lat) {
            $location = $user->nearest_btn_location();
        }
        else{
            $location = $user->nearest_btn_location();
        }*/
        
        $location = $user->nearest_btn_location();

        $profile = Profile::create([
                    'user_id' => $user->id,
                    /*'tutor_fee' => 0.30,*/
                    'marketing' => $data['marketing'],
                    /*'school' => $data['school'],
                    'major' => $data['major'],
                    'year' => $data['year'],*/
            ]);

        DB::table('terms_of_use')->insert([
            'user_id' => $user->id,
            'version' => 'terms_of_use',
            'submission_date' => Carbon\Carbon::now()
            ]);
        
        DB::table('location_user')->insert([
            'user_id' => $user->id,
            'location_id' => $location->id,
            ]);
        
        //Find & Associate Tutor Application
        $tutor_application = TutorApplicant::where('email', 'LIKE', "%".$user->email."%")->first();

        if ($tutor_application) {
            $tutor_application->update(['user_id' => $user->id]);
            $role_type = 'tutor-applicant';
        }
        
        $user->addRole($role_type);

        //$email_view = 'btn.emails.tutors.welcome';

        /*if ($role_type == 'student') {
            $email_view = 'btn.emails.students.welcome';
            $this->dispatch(new SendWelcomeEmail($user, $location, $email_view));
        }*/

        

        //$role = Role::whereRole($role_type)->first();
        //$this->dispatch(new CreateNewUser($user, $location, $role, $data));

        return $user;
    }
}
