<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon;
use App\Location;
use App\AcademicAdvisor;
use Yajra\Datatables\Datatables;

use App\Jobs\SendAdvisorsRecruitmentEmail;

class AcademicAdvisorsController extends Controller
{
    /**
     * Displays Advisors in datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getAdvisors()
    {
        return view('btn.advisors.list_advisors');
    }

    /**
     * Process ajax request for Advisors datatables.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAdvisorsData()
    {
        $advisors = AcademicAdvisor::all();
        return Datatables::of($advisors)
                            ->editColumn('location_id', function($advisors) {
                                return $advisors->location->city;
                                })
                            ->editColumn('first_name', function($advisors) {
                                $full_name = $advisors->full_name;
                                
                                if ($advisors->unsubscribe) {
                                	$full_name = "<span style='color:gray'>".$full_name."<br>".$advisors->admin_notes."</span>";
                                }
                                return $full_name;
                                })
                            ->editColumn('department', function($advisors) {
                                return $advisors->department.
                                		"<br><a href='$advisors->source' target='_blank'>Source</a>";
                                })
                            ->editColumn('last_contacted', function($advisors) {
                                if ($advisors->last_contacted) {
                                	return $advisors->last_contacted->diffForHumans().
                                		'<br><a href='.route('delete.advisor', ['id' => $advisors->id]).">Delete $advisors->first_name</a>";
                                }
                                else
                                	return $advisors->last_contacted;
                                
                                })
                            ->make(true);
    }

    public function listAdvisors()
    {
    	$schools = ['' => 'Select A School',
    					'CU-Boulder' => 'CU-Boulder',
    					'CU-Denver' => 'CU-Denver',
    					'DU' => 'Denver University',
    					'Denver-Metro' => 'Metro State (Denver)',
    					'CSU' => 'CSU (Fort Collins)',
    					'Colorado-College' => 'Colorado College (ColoSprings)',
    					'CU-ColoSprings' => 'CU-Colorado Springs (UCCS)',
                        'ASU' => 'Arizona State (Tempe)',
                        'BYU' => 'Brigham Young (Provo)',
                        'FSU' => 'Florida State (Tallahassee)',
                        'UFL' => 'U of Florida (Gainesville)',
                        'UCF' => 'U of Central Florida (Orlando)',
                        'GMU' => 'George Mason U (Fairfax)',
                        'MSU' => 'Michigan State',
                        'NCSU' => 'North Carolina State (Raleigh)',
                        'UNC' => 'U of North Carolina (Chapel Hill)',
                        'Ole-Miss' => 'U of MS (Ole Miss, Oxford)',
                        'OSU-Corvallis' => 'Oregon State (Corvallis)',
                        'PSU' => 'Penn State (University Park / State College)',
                        'PU' => 'Purdue',
                        'TAMU' => 'Texas A&M (College Station)',
                        'UT' => 'UT Austin',
                        'UVA' => 'U of Virginia',
                        'UW' => 'UW Seattle'
                    ];
    	
    	$advisors = AcademicAdvisor::contactList()->get();

    	$locations = Location::pluck('city', 'id')->toArray();

        $locations = ['' => 'Select A Location'] + $locations;

    	return view('btn.advisors.list_advisors', compact('schools', 'advisors', 'locations'));
    }

    public function addAdvisor(Request $request)
    {
    	$this->validate($request, [
    		'location_id' => 'required',
    		'first_name' => 'required',
    		'email' => 'required|unique:academic_advisors,email',
    		'school' => 'required',
    		'department' => 'required',
    		'position' => 'required',
    		'btn_group' => 'required',
    		'source' => 'required'
    		]);

    	AcademicAdvisor::create([
    		'location_id' => $request->location_id,
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'email' => $request->email,
    		'phone' => $request->phone,
    		'school' => $request->school,
    		'department' => $request->department,
    		'position' => $request->position,
    		'btn_group' => $request->btn_group,
    		'source' => $request->source
    		]);

    	\Session::flash('success', "$request->first_name $request->last_name Added");
    	return redirect()->back();
    }

    public function emailAdvisors($location_id)
    {
    	$location = Location::find($location_id);

    	$advisors = AcademicAdvisor::contactList()
                            ->where('location_id', $location->id)
    						->get();
        $count = 0;
    	foreach ($advisors as $advisor) {   		
    		// Dispatch job to email advisors
    		$this->dispatch(new SendAdvisorsRecruitmentEmail($advisor, $location));
            $count++;
    	}

    	\Session::flash('success', "Emails to $count $location->city advisors scheduled to be sent");
    	return redirect()->back();
    }

    public function deleteAdvisor($id)
    {
    	$advisor = AcademicAdvisor::find($id);
    	$name = $advisor->full_name;

    	$advisor->unsubscribe = 1;
    	$advisor->deleted_at = Carbon\Carbon::now();
    	$advisor->save();

    	\Session::flash('success', "$name successfully unsubscribed");
    	return redirect()->back();
    }
}
