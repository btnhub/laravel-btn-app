<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Profile;
use App\Location;
use App\Tutor;
use App\MarketSubject;
use App\Search;
use App\CourseLevel;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

use Carbon;
use DB;

class SearchController extends Controller
{
	public function __construct()
	{
		$this->education_levels = [ 
			'phd' => "Graduated (PhD)", 
			'masters' => 'Graduated (Master\'s)', 
			'grad_student' => "Graduate Student", 
			'bachelors' => 'Graduated (Bachelor\'s)', 
			'undergrad' => "Undergraduate Student"];

		$this->tutor_availability_options = [
			'Daytime' => 'Daytime (before 5pm)',
			'Evenings' => 'Evenings (5-7pm)',
			'Nights' => 'Nights (after 7pm)',
			'Saturdays' => 'Saturdays',
			'Sundays' => 'Sundays'
			];

		$this->perPage = 10; //Number of tutors listed per page

		$subjects = MarketSubject::all()->pluck('department', 'id')->toArray();
    	$this->subjects = ['' => "Any Subject"] + $subjects;

    	$course_levels = CourseLevel::all()->pluck('description', 'id')->toArray();
    	$this->course_levels = ['' => "Any Level"] + $course_levels;

		$this->search_radius = (float)DB::table('settings')->where('field', 'search_radius')->first()->value;
		$this->search_radius_unit = DB::table('settings')->where('field', 'search_radius_unit')->first()->value;
		$this->tutor_latlng = [];
	}

	public function rankTutors($tutors){
		$weights = DB::table('rank')->pluck('weight', 'title');
		//$summaries = DB::table('tutor_rankings')->whereIn('id', $tutors->pluck('id'))->get();
		
		foreach ($tutors as $tutor) {
			if ($tutor->id == 63) {
				$tutor->score = 1E8;
			}
			else{
				$tutor->score = $tutor->rank() * 100
								+ $tutor->tutorSessions->sum('duration') * $weights['hours']
								+$tutor->refunds->count() * $weights['session_refunds'];
				
				/*foreach ($summaries as $summary) {
					if($tutor->id == $summary->id){
						$tutor->score =  $summary->positive_end_eval * $weights['positive_end_eval']
	                        + $summary->positive_first_eval * $weights['positive_first_eval']
	                        + $summary->negative_end_eval * $weights['negative_end_eval']
	                        + $summary->negative_first_eval * $weights['negative_first_eval']
	                        + $summary->hours * $weights['hours']
	                        + $summary->session_refunds * $weights['session_refunds'];
						break;
					}
				}*/
			}
		}
		return $tutors->sortByDesc('score');
	}

	public function getTutors()
	{
		$tutors = Tutor::with('profile', 'locations', 'tutorSessions', 'tutor_contracts', 'active_assignments')
							->activeTutors()
							->select('id', 'ref_id', 'first_name', 'last_name')
							->get();
							
		$tutors = $this->rankTutors($tutors);

		$perPage = $this->perPage;
		$currentPage = Input::get('page') ?? 1;

		$items = $tutors;
		$total = $tutors->count();
		
		$offset = ($currentPage - 1) * $perPage;

		$tutors_chunk = $items->slice($offset, $perPage)->all(); //slice($startingIndex, $chunkSize)

		$tutors = new LengthAwarePaginator($tutors_chunk, $total, $perPage); 
		$tutors->setPath('tutor-profiles');

		$locations = Location::studentsVisible()->has('active_tutors')->select('id', 'city', 'state')->get();
		$education_levels = $this->education_levels;
		$tutor_availability_options = $this->tutor_availability_options;

		return view('btn.search.tutor_results', compact('tutors', 'locations', 'education_levels', 'tutor_availability_options'));       	
	}

	public function searchTutors(Request $request)
	{
		//$this->validate($request, ['course' => 'required']); //might remove this

		$cities = null;

		$course = $request['course'];
		$stud_min_rate = $request['min_rate'];
		$stud_max_rate = $request['max_rate'];
		$status = $request['status'];
		$tutor_education = $request['tutor_education'];
		$tutor_availability = $request['tutor_availability'];
		$locations = $request['location'];
		$online_location_id = Location::where('city', 'Online')->first()->id;

		$searchValues = preg_split('/\s+/', $course); // split on 1+ whitespace
					
		$profiles = Profile::where(function ($q) use ($searchValues) {
  						foreach ($searchValues as $value) {
    						$q->orWhere('courses', 'like', "%{$value}%");
    						$q->orWhere('instruments', 'like', "%{$value}%");
						}
					});
		if($stud_min_rate){
			$profiles = $profiles->where('rate_max', '>=', $stud_min_rate);
		}

		if($stud_max_rate){
			$profiles = $profiles->where('rate_min', '<=', $stud_max_rate);
		}
		
		if($status != null){
			$profiles = $profiles->where('tutor_status', $status);
		}

		if (!empty($locations)) {
			$user_ids = DB::table('location_user')->whereIn('location_id', $locations)->pluck('user_id');
			
			if (in_array($online_location_id, $locations)) {
				$online_profiles = $profiles;
				$online_profiles_ids = $online_profiles->where('online', 1)->pluck('user_id')->toArray();
				$user_ids = array_unique(array_merge($user_ids, $online_profiles_ids));
			}
			
			$profiles = $profiles->whereIn('user_id', $user_ids);		
			$cities = Location::whereIn('id', $locations)->pluck('city')->implode(', ');
		}
		
		if (!empty($tutor_education)) {
			$profiles = $profiles->whereIn('tutor_education', $tutor_education);		
		}

		if (!empty($tutor_availability)) {
			$profiles = $profiles->where(function($query) use($tutor_availability) {
											foreach ($tutor_availability as $avail) {
												$query->orWhere('tutor_availability', 'like', "%$avail%");
											}
										});
		}

		$profile_ids = $profiles->pluck('user_id');
		
		$tutors = Tutor::with('profile', 'locations', 'tutorSessions', 'tutor_contracts', 'active_assignments')
						->activeTutors()
						->whereIn('id', $profile_ids)
						->select('id', 'ref_id', 'first_name', 'last_name')
						->get();
						
		$tutors = $this->rankTutors($tutors);

		$perPage = $this->perPage;
		$currentPage = Input::get('page') ?? 1;

		$items = $tutors;
		$total = $tutors->count();
		
		$offset = ($currentPage - 1) * $perPage;

		$tutors_chunk = $items->slice($offset, $perPage)->all(); //slice($startingIndex, $chunkSize)

		$tutors = new LengthAwarePaginator($tutors_chunk, $total, $perPage); 
		$tutors->setPath('search');

		$tutors->appends([
						'course' => $course, 
						'min_rate' => $stud_min_rate,
						'max_rate' => $stud_max_rate,
						'status' => $status,
						'tutor_education' => $tutor_education,
						'tutor_availability' => $tutor_availability,
						'location' => $locations
						]);
		
		$education_levels = $this->education_levels;
		$tutor_availability_options = $this->tutor_availability_options;
		$locations = Location::studentsVisible()->has('active_tutors')->select('id', 'city', 'state')->get();

		return view('btn.search.tutor_results', compact('tutors', 'locations', 'education_levels', 'tutor_availability_options', 'request', 'cities'));
	}

	public function cityTutorList($city)
	{
		$locations = $user_ids = null;
		$cities = ucwords(str_replace("-", " ", $city));
		$cities = ucwords(str_replace("_", " ", $cities));

		if ($city) {
			$locations = Location::where('city', 'like', "%$cities%")->pluck('id')->toArray();
		}
		
		if (count($locations)) {
			$user_ids = DB::table('location_user')->whereIN('location_id', $locations)->pluck('user_id');
		}
		
		$tutors = Tutor::with('profile', 'locations', 'tutorSessions')
						->activeTutors()
						->whereIn('id', $user_ids)
						->select('id', 'ref_id', 'first_name', 'last_name')
						->get();
						
		$tutors = $this->rankTutors($tutors);

		$perPage = $this->perPage;
		$currentPage = Input::get('page') ?? 1;

		$items = $tutors;
		$total = $tutors->count();
		
		$offset = ($currentPage - 1) * $perPage;

		$tutors_chunk = $items->slice($offset, $perPage)->all(); //slice($startingIndex, $chunkSize)

		$tutors = new LengthAwarePaginator($tutors_chunk, $total, $perPage); 
		$tutors->setPath('search');

		$tutors->appends(['location' => $locations]);
		
		$request = Request();
		$request->replace(['location' => $locations]);

		$education_levels = $this->education_levels;
		$tutor_availability_options = $this->tutor_availability_options;
		$locations = Location::studentsVisible()->has('active_tutors')->select('id', 'city', 'state')->get();

		return view('btn.search.tutor_results', compact('tutors', 'locations', 'education_levels', 'tutor_availability_options', 'request', 'cities', 'request'));
	}

	/**CITYBOOK**/
	
	public function getSearchMap(){
		$tutor_availability_options = $this->tutor_availability_options;
		//$subjects = $this->subjects;
		$course_levels = $this->course_levels;

		return view('btn.pages.citybook.search_tutors', compact('tutor_availability_options', /*'subjects', */'course_levels'));  

	}

	public function searchMap(Request $request){
		$tutors_array = [];
		$position = 0;
		$conversion = 1.60934; //1 mile = 1.60934km
		$education_levels = $this->education_levels;
		$tutor_availability_options = $this->tutor_availability_options;
		$search_radius = $this->search_radius;
	    $search_radius_unit = $this->search_radius_unit;

		$course = $request->course;
		$course_level_id = $request->course_level_id;
		$tutor_availability = $request->tutor_availability;
		$include_online = $request->include_online;
		$min_rate = $request->min_rate;
		$max_rate = $request->max_rate;
		$status = $request->status;	

		$searchValues = preg_split('/\s+/', $course); // split on 1+ whitespace

		$search_lat = (float)$request->lat;
	    $search_lng = (float) $request->lng;
		$search_location = $request->formatted_address;	
		
		$search_latlng = json_encode(['lat' => $search_lat,  'lng' => $search_lng]);
	    
	    if ($search_radius_unit != 'km') {
	        //convert to km
	        $search_radius = $search_radius * $conversion;
	        $search_radius_unit = "km";
	    }

	    $tutors = Tutor::with('profile', 'geolocations','tutorSessions', 'evaluations')
	    				/*->where('last_login_date', '>', Carbon\Carbon::now()->subMonths(1))*/
						->activeTutors()
						->searchable();
						
		if ($course_level_id) {
			$tutors = $tutors->whereHas('course_levels', function($level) use($course_level_id) {return $level->where('id', $course_level_id);});
		}
		
		$tutors = $tutors->whereHas('profile', function($query) use ($searchValues, $min_rate, $max_rate, $status, $tutor_availability, $search_radius, $search_radius_unit){
					if ($searchValues) {
						$query->where(function($q) use($searchValues) {
							foreach ($searchValues as $value) {
	    						$q->orWhere('courses', 'like', "%{$value}%");
	    						$q->orWhere('instruments', 'like', "%{$value}%");
    						}
						});
					}
					if ($min_rate) {
						$query->where('rate_max', '>=', $min_rate);
					}
					if ($max_rate) {
						$query->where('rate_min', '<=', $max_rate);
					}
					if($status != null){
						$query->where('tutor_status', $status);
					}
					else{
						$query->where('tutor_status', 1);
					}

					if (!empty($tutor_availability)) {
						$query->where(function($q) use($tutor_availability) {
							foreach ($tutor_availability as $avail) {
								$q->orWhere('tutor_availability', 'like', "%$avail%");
							}
						});
					}
				})
				->select('id', 'ref_id', 'first_name', 'last_name')
				->get();

		$tutors = $tutors->filter(function($tutor) use ($search_lat, $search_lng, $search_radius, $search_radius_unit, $include_online){
						$nearest_geolocation = $tutor->nearest_geolocation($search_lat, $search_lng, $search_radius, $search_radius_unit);
						
						if ($nearest_geolocation) {
							$this->tutor_latlng[$tutor->ref_id]['lat'] = (float)$nearest_geolocation->lat;
							$this->tutor_latlng[$tutor->ref_id]['lng'] = (float)$nearest_geolocation->lng;

							return true;
						}
						elseif($include_online && $tutor->profile->online){
							$this->tutor_latlng[$tutor->ref_id]['lat'] = null;
							$this->tutor_latlng[$tutor->ref_id]['lng'] = null;
							return true;
						}
						return false;
					});
		$tutors = $this->rankTutors($tutors);

		$tutors = $tutors->slice(0,12);
		
		$tutors_collection = $this->tutorMapDetails($tutors);
		
		$course_levels = $this->course_levels;

    	//UPDATE DB
    	$search_criteria = array_merge(['identifier' => session('identifier')], $request->only('course', 'course_level_id', 'include_online', 'formatted_address', 'city', 'region', 'country', 'postal_code', 'lat', 'lng'));
    	$search = Search::firstOrNew($search_criteria);
    	$search->count = $search->count + 1;
    	$search->user_id = $search->user_id ?? auth()->user()->id ?? null;
    	if (!$search->formatted_address) {
    		$search->formatted_address = $request->location;
    	}
    	$search->save();

    	foreach ($tutors as $tutor) {
    		$tutor->searches()->sync([$search->id], false); // false = sync without detaching
    	}

    	$this->tutor_latlng = [];

		return view('btn.pages.citybook.search_tutors_results', compact('tutor_availability_options', 'course_levels', 'tutors_collection', 'search_latlng', 'search_radius', 'request' ));    	
	}

	public function tutorMapDetails($tutors){
		$tutors_array = [];

		$weights = DB::table('rank')->pluck('weight', 'title');
		$summaries = DB::table('tutor_rankings')->whereIn('id', $tutors->pluck('id'))->get();
		
		foreach ($tutors as $tutor) {
			if ($tutor->id == 63) {
				$tutor->score = 1E8;
			}
			else{
				foreach ($summaries as $summary) {
					if($tutor->id == $summary->id){
						$tutor->score =  $summary->positive_end_eval * $weights['positive_end_eval']
	                        + $summary->positive_first_eval * $weights['positive_first_eval']
	                        + $summary->negative_end_eval * $weights['negative_end_eval']
	                        + $summary->negative_first_eval * $weights['negative_first_eval']
	                        + $summary->hours * $weights['hours']
	                        + $summary->session_refunds * $weights['session_refunds'];
						break;
					}
				}
			}

			$profile = $tutor->profile;

			$reviews_count = $tutor->evaluations()->where('website_reference', '!=', '')->count();
            
            if ($tutor->id == 63) {
            	$reviews_count += DB::table('evaluations_mogi')->where('website_reference', '!=', '')->count();
            }

            $tutor_avatar = $profile->avatar ?? 'default.jpg';
            
            array_push($tutors_array,(object)[
                'url' => route('profile.index', $tutor->ref_id),
                'tutor_status' => $profile->tutor_status ? "Available" : "UNAVAILABLE",
                'avatar' => 'btn/avatars/'.$tutor_avatar,
                'name_education' => "$tutor->user_name". " - " . $this->education_levels[$profile->tutor_education],
                'session_locations' => $profile->session_locations,
                'major' => $profile->major,
                'rating' => round($tutor->rating()),
                'reviews' => $reviews_count,
                'lat' => $this->tutor_latlng[$tutor->ref_id]['lat'],
                'lng' => $this->tutor_latlng[$tutor->ref_id]['lng'],
                'map_position' => $tutor->ref_id,
                'experience' => mb_strimwidth($profile->tutor_experience, 0, 250, "...")
                ]);
		}
		return collect($tutors_array);
	}
}
