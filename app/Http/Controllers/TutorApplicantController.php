<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Yajra\Datatables\Datatables;
use App\TutorApplicant;
use App\ProfExamResult;
use App\StudentReference;
use App\Location;
use App\User;
use App\Profile;
use App\Role;
use Auth;
use Carbon;
use DB;

use App\Jobs\EmailTutorApplicant;
use App\Jobs\EmailAdmin;

class TutorApplicantController extends Controller
{
    /**
     * Displays Applicants in datatables front end view
     *
     * @return \Illuminate\View\View
     */
    public function getExamResults(Request $request){
        //AJAX Request
        $max_users = 10;
        $passing_grade = 75;

        $user_ids = TutorApplicant::has('proficiency_exams')
                                        ->where('hired', 0)
                                        ->where(function($q){
                                            $q->where('offered', 1)
                                                ->orWhereNull('offered');
                                        })
                                        ->where('created_at', '>', Carbon\Carbon::now()->subMonths(6))
                                        ->where('end_date', '>', Carbon\Carbon::now()->addWeeks(6))
                                        ->orderBy('created_at', 'desc') 
                                        ->pluck('user_id')
                                        ->unique()
                                        ->slice(0, $max_users);

        $exam_results = ProfExamResult::whereIN('user_id', $user_ids)
                                        ->where('created_at', '>', Carbon\Carbon::now()->subMonth())
                                        ->where('grade', '>', $passing_grade)
                                        ->get();
        $results_details = [];

        foreach ($exam_results as $result) {
            $info = "";
            if ($result->user) {
                $applicant = TutorApplicant::where('user_id', $result->user->id)->first();

                $location = $applicant->company_locations;
                                
                if ($location == "Other") {
                    $location = $applicant->city != ''? "$applicant->city, $applicant->region" : $applicant->formatted_address;
                }
                
                $details_object = (object)['id' => $applicant->id, 'date' => $result->created_at->format('m/d/Y'), 'name' => $applicant->fullName, 'location' => $location, 'exam' => $result->exam, 'grade' => $result->grade];
                
                array_push($results_details, $details_object);
            }
        }
        
        return response()->json(['details' => $results_details]);
    }

    public function getApplicants()
    {
        //return view('btn.applications.index');
        // dd($this->getExamResults());
        return view('btn.pages.citybook.admin.tutor_applications');
    }

    /**
     * Process ajax request for Applicants datatables.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getApplicantsData()
    {
        $applicants = TutorApplicant::where('hired', 0)
                                        ->where(function($q){
                                            $q->where('offered', 1)
                                                ->orWhereNull('offered');
                                        })
                                        ->where('created_at', '>', Carbon\Carbon::now()->subMonths(6))
                                        ->where('end_date', '>', Carbon\Carbon::now()->addWeeks(6))
                                        ->orderBy('created_at', 'desc');
                                        
        //create table of all unhired applicants
        return Datatables::of($applicants)
                            ->editColumn('id', function($applicant) {
                                $checkbox = "<br><input type='checkbox' name='applicant_ids[]' value='".$applicant->id."'/>";
                                
                                return $applicant->id . $checkbox;
                                })
                            ->editColumn('first_name', function($applicant) {
                                $status = null;
                                if ($applicant->offered == 1) {
                                    $status = "<small style='color:purple;font-weight:bold'>Offered</small><br>";
                                }
                                elseif($applicant->submitted){
                                    $status = "<small style='color:darkgreen;font-weight:bold'>Submitted</small><br>";
                                }
                                
                                return link_to_route('applicant.show', $applicant->first_name." ".$applicant->last_name, ['id' => $applicant->id])."<br>$status <small>Availability: $applicant->availability</small>";
                                })
                            ->editColumn('year', function($applicant) {
                                
                                $results = null;

                                if ($applicant->exam_results()) {
                                    $results = "<small>";
                                    foreach ($applicant->exam_results() as $result) {
                                        $results .= ucwords(str_replace('-', ' - ',str_replace('_', ' ', str_replace(".csv", "", $result->exam)))) . " - $result->grade% <br>";
                                    }
                                    
                                    $results .= "</small>";
                                }
                                

                                return "$applicant->year <br> $results";
                                })
                            ->editColumn('start_date', function($applicant) {
                                return Carbon\Carbon::parse($applicant->start_date)->format('m/d/Y') . " -> " . Carbon\Carbon::parse($applicant->end_date)->format('m/d/Y');
                                })
                            ->editColumn('company_locations', function($applicant) {
                                $location = $applicant->company_locations;
                                
                                if ($location == "Other") {
                                    $location = $applicant->city != ''? "$applicant->city, $applicant->region" : $applicant->formatted_address;
                                }
                                
                                return $location."<br><small style='color:brown;font-weight:bold'>$applicant->admin_notes</small>";
                                })
                            ->make(true);
    }

    public function show($id)
    {
        $applicant = TutorApplicant::where('id', $id)->firstOrFail();
        //return view('btn.applications.show', compact('applicant'));
        return view('btn.pages.citybook.admin.tutor_applications_show', compact('applicant'));
    }
    
    public function offerPosition(Request $request, $id)
    {
        $applicant = TutorApplicant::findOrFail($id);

        $location = $applicant->user ? $applicant->user->nearest_btn_location() : Location::first();

        $applicant->update(['offered' => $request->offered]);

        $subject = "Tutor Application Updated";
        $email_view = 'btn.emails.tutors.offer_follow_up';
        $email_data = ['applicant' => $applicant];
        $this->dispatch(new EmailTutorApplicant($applicant, $location, $subject, $email_view, $email_data));
        
        \Session::flash('success', "Application Updated");
        return redirect()->route('applicants');
    }

    public function update(Request $request, $id)
    {
        $applicant = TutorApplicant::findOrFail($id);
        //$city = explode(', ', $applicant->company_locations)[0]; //returns the first city listed on application
        //$location = Location::where('city', $city)->first();
        
        if ($applicant->user && $applicant->user->lat) {
            $location = $applicant->user->nearest_btn_location();
        }
        else{
            $location = Location::where('city', 'Other')->first();
        }

        $applicant->update($request->all());

        if (!$applicant->update(['hired' => 1, 'hired_by_id' => Auth::user()->id]))
        {
            \Session::flash('error', 'Could not update application');
            return redirect()->back();
        }

        $subject = "Tutor Application Approved!";
        $email_view = 'btn.emails.tutors.hired_follow_up';
        $email_data = ['applicant' => $applicant];
        $this->dispatch(new EmailTutorApplicant($applicant, $location, $subject, $email_view, $email_data));
        
        \Session::flash('success', "Hired {$applicant->first_name} {$applicant->last_name}!");
        return redirect()->route('applicants');
    }

    /**
     * "Delete" application with reason for not hiring tutor
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function removeApplicant(Request $request, $id)
    {
        $applicant = TutorApplicant::findOrFail($id);
    
        $this->validate($request, ['admin_notes' => 'required']);
        
        if (!$applicant->update(['admin_notes' => $request['admin_notes'], 'hired' => 0, 'hired_by_id' => Auth::user()->id]))
        {
            App::abort(500, 'Could not add admin notes');   
        }

        if(!$applicant->delete())
        {
            App::abort(500, 'Could not delete tutor');
        }

        \Session::flash('info', "Did not hire {$applicant->first_name}{$applicant->last_name}.  Application successfully removed!");
        return redirect()->route('applicants');
    }
    
    public function OLDgetTutorApplication()
    {
        //$locations = Location::tutorsVisible()->select('id', 'city', 'state')->get();

        $locations_collection = Location::tutorsVisible()->get();
        $locations = [];

        foreach ($locations_collection as $location) {
            $state = $location->state ? ", $location->state" : null;
            $locations[$location->id] = "{$location->city}{$state}";

        }
        return view('btn.forms.tutor_application', compact('locations'));
    }

    public function OLDsubmitTutorApplication(Request $request)
    {
        $this->validate($request, [
                'eligibility' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'major' => 'required',
                'year' => 'required',
                'availability' => 'required',
                'locations' => 'required',
                'session_locations' => 'required',
                'courses' => 'required',
                'experience' => 'required',
                'start_date' => 'after:today',
                'end_date' => 'after:start_date',
                'marketing' => 'required',
                'terms' => 'required',
            ]);
        $input = $request->all();

        //$locations = implode(", ", $input['locations']);
        $locations = Location::whereIn('id', $input['locations'])->get()->implode("city", ", ");

        $user_id = null;

        if (auth()->user()) {
            $user = auth()->user();
            $user_id = $user->id;

            if (!$user->hasRole('tutor-applicant')) {
                $user->addRole('tutor-applicant');
            }
        
        }

        $applicant = new TutorApplicant([
            'user_id' => $user_id,
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'major' => $input['major'],
            'year' => $input['year'],
            'availability' => $input['availability'],
            'company_locations' => $locations,
            'session_locations' => $input['session_locations'],
            'courses' => $input['courses'],
            'experience' => $input['experience'],
            'concerns' => $input['concerns'],
            'start_date' => $input['start_date'],
            'end_date' => $input['end_date'],
            'marketing' => $input['marketing'],
            'access_key' => rand(10000000, 99999999),
            'access_end_date' => Carbon\Carbon::now()->addWeeks(2)
            ]);

        if (!$applicant->save()) {
            \Session::flash('error', 'We were unable to submit your application.  Please try again.');
        }

        //$location = Location::where('city', $input['locations'][0])->first(); //uses first location selected
        $location = Location::find($input['locations'][0]); 
        $subject = "Tutor Applicant Next Steps";
        $email_view = 'btn.emails.tutors.tutor_application_submitted';
        $email_data = ['applicant' => $applicant];
        $this->dispatch(new EmailTutorApplicant($applicant, $location, $subject, $email_view, $email_data));

        \Session::flash('success', "Thank you for submitting our application.  We've emailed you a summary of the next steps.   Be sure to check your Spam folder for the e-mail just in case");

        return redirect()->back();
    }

    /*****CITY THEME********/
    public function getTutorApplication()
    {
        return view('btn.pages.citybook.tutor_application');
    }

    public function submitTutorApplication(Request $request)
    {
        /*$this->validate($request, [
                'eligibility' => 'required',
                'first_name' => 'required|min:2|max:255',
                'last_name' => 'required|min:2|max:255',
                'email' => 'required|email',
                'phone' => 'required',
                'major' => 'required',
                'year' => 'required',
                'availability' => 'required',
                'courses' => 'required',
                'experience' => 'required',
                'start_date' => 'after:today',
                'end_date' => 'after:start_date',
                'marketing' => 'required',
                'terms' => 'required',
            ]);*/
        
        $user = auth()->user();
        
        if ($user) {
            //Check if user has the applicant role
            if (!$user->hasRole('tutor-applicant')) {
                $user->addRole('tutor-applicant');
            }
        
        }
        else{
            $this->validate($request, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|regex:/^\S*$/|confirmed'
            ]);
            
            //create new user
            $role = Role::where('role', 'tutor-applicant')->first();

            $user = User::create([
                        'ref_id' => rand(10000000, 99999999),
                        'first_name' => ucwords(trim($request['first_name'])),
                        'last_name' => ucwords(trim($request['last_name'])),
                        'email' => trim($request['email']),
                        'phone' => trim($request['phone']),
                        'password' => bcrypt($request['password']),
                        'last_login_date' => Carbon\Carbon::now(),
                        'formatted_address' => $request->formatted_address != '' ? $request->formatted_address : $request->session_locations ?? null,
                        'city' => $request->city ?? null,
                        'region' => $request->region ?? null,
                        'country' => $request->country ?? null,
                        'postal_code' => $request->postal_code ?? null,
                        'lat' => $request->lat ?? null,
                        'lng' => $request->lng ?? null
                    ]);

            if ($user->lat) {
                $location = $user->nearest_btn_location();
            }
            else{
                $location = Location::where('city', 'Other')->first();
            }
            

            $profile = Profile::create([
                            'user_id' => $user->id,
                            'major' => $request->major,
                            'year' => $request->year,
                            'marketing' => $request->marketing
                        ]);

            DB::table('terms_of_use')->insert([
                'user_id' => $user->id,
                'version' => 'terms_of_use',
                'submission_date' => Carbon\Carbon::now()
                ]);
            
            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => $role->id,
                ]);

            DB::table('location_user')->insert([
                'user_id' => $user->id,
                'location_id' => $location->id,
                ]);

            //Login User
            auth()->loginUsingId($user->id);
        }

        $location = $user->locations->first() ?? $user->nearest_btn_location();
        
        if ($request->formatted_address == '') {
             $request->merge(['formatted_address' => $request->session_locations ?? null]);
        }
        
        // $request->merge(['experience' =>$request->experience . "\r\n \r\nONLINE METHOD \r\n" . $request->online_method]);
        
        $request->request->add(['user_id' => $user->id,
                                'company_locations' => $location->city,
                                'access_key' => rand(10000000, 99999999),
                                'access_end_date' => Carbon\Carbon::now()->addWeeks(2)
                                ]);

        //Check for online tutoring
        if (in_array("online", $request->services)) {
            $request->request->add(['online' => 1]);
            $request->merge(['experience' =>$request->experience . "\r\n \r\nONLINE METHOD \r\n" . $request->online_method]);
        }
        
        $applicant = TutorApplicant::create($request->except('_token', 'terms', 'eligibility', 'services', 'password', 'password_confirmation', 'online_method'));

        $subject = "Tutor Applicant Next Steps";
        $email_view = 'btn.emails.tutors.tutor_application_submitted';
        $email_data = ['applicant' => $applicant];
        //$this->dispatch(new EmailTutorApplicant($applicant, $location, $subject, $email_view, $email_data));

        //\Session::flash('success', "Thank you for submitting our application.  We've emailed you a summary of the next steps.   Be sure to check your Spam folder for the e-mail just in case");

        \Session::flash('success', "Welcome to your dashboard.  Please address the listed items to complete your application.");

        return redirect()->route('profile.account', $user->ref_id);
    }

    public function getReferenceForm($applicant_ref_id){
        $applicant = User::where('ref_id', $applicant_ref_id)->first();

        return view('btn.pages.citybook.tutor_reference', compact('applicant'));
    }

    public function submitReferenceForm(Request $request, $applicant_ref_id){
        $user = User::where('ref_id', $applicant_ref_id)->first();

        if ($user && $user->tutorApplicant) {
            $request->request->add(['tutor_applicant_id' =>$user->tutorApplicant->id]);
            
            $reference = StudentReference::create($request->except('_token', 'confirm'));
            
            $reference->update(['user_id' => auth()->user()->id ?? null]);

            $confirmation_message = "Thank you for submitting this reference.";
        }
        else{
            $confirmation_message = "Unfortunately we were unable to locate the application.  Please notify the applicant.";
        }
        
        return view('btn.pages.citybook.confirmation_page', compact('confirmation_message'));
    }

    public function addProfLink(Request $request, $applicant_ref_id){
        $applicant = User::where('ref_id', $applicant_ref_id)->first();

        $applicant->tutorApplicant->professional_link = $request->professional_link;
        $applicant->tutorApplicant->save();

        \Session::flash('success', 'Link added to application.');
        return redirect()->back();
    }

    public function submitApplicationPackage(Request $request, $applicant_ref_id){
        $user = User::where('ref_id', $applicant_ref_id)->first();

        $application = $user->tutorApplicant;

        $application->submitted = Carbon\Carbon::now();
        $application->save();
        
        $location = $user->locations->first();
        $subject = "New Application - $application->full_name";
        $email_view = 'btn.emails.admin.new_application_submitted';
        $email_data = ['application' => $application];

        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

        \Session::flash('success', 'Please allow 1-3 days to review your application package.');

        return redirect()->back();
    }

    public function emailApplicants(Request $request){
        //Email: exams
        $this->validate($request, [
            'applicant_ids' => 'required'
            ]);
        
        $count = 0;
        $names = [];

        foreach ($request->applicant_ids as $id) {
            if ($applicant = TutorApplicant::find($id)) {
                $location = $applicant->user ? $applicant->user->nearest_btn_location() : Location::first();
                if ($request->email_option == 'proficiency') {    
                    $subject = "Tutor Application - Proficiency Exams";
                    $email_view = 'btn.emails.tutors.applicant_proficiency_exam_reminder';
                    $email_data = ['applicant' => $applicant];
                    $this->dispatch(new EmailTutorApplicant($applicant, $location, $subject, $email_view, $email_data));
                    
                    $applicant->admin_notes .= Carbon\Carbon::now()->format('m/d/Y') . ": Asked to pass exams \n";
                    $applicant->save();

                    $count++;
                    array_push($names, $applicant->full_name);
                }
            }
        }

        $names = implode(', ', $names);
        \Session::flash('success', "$count Applicants Emailed: $names");
        return redirect()->back(); 
    }
}
