<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Tutor;
use App\Student;
use App\Assignment;
use App\Location;
use App\TutorContract;

use Auth;
use DB;
use Carbon;

use App\Jobs\SendGeneralNotification;

class TutorController extends Controller
{

	public function getPreviousStudent()
	{
		//return view('btn.profile.previous_students');
		return view('btn.pages.citybook.previous_students');
	}
	
    public function findPreviousStudent(Request $request)
	{
		$this->validate($request, ['search' => 'required|min:2']);
		$search = $request['search'];
		$tutor = Tutor::find(Auth::user()->id);
		
		//creates an array with key = student id, value = course
		$assignments = Assignment::whereIn('id', $tutor->assignments()->pluck('id'))->pluck('course', 'student_id');
		$student_ids = $tutor->assignments()->pluck('student_id');

		$searchValues = preg_split('/\s+/', $search); // split on 1+ whitespace

		$students = Student::whereIn('id', $student_ids)
					->where(function ($q) use ($searchValues) {
  						foreach ($searchValues as $value) {
    						$q->orWhere('first_name', 'like', "%{$value}%");
    						$q->orWhere('last_name', 'like', "%{$value}%");
						}
						})
					->get();

		//$locations = Auth::user()->locations->pluck('city', 'id');

		//return view('btn.profile.previous_students', compact('students', 'tutor', 'assignments', 'locations'));
		return view('btn.pages.citybook.previous_students', compact('students', 'tutor', 'assignments'/*, 'locations'*/));
	}

	public function addPreviousStudent(Request $request, $ref_id)
	{
		$this->validate($request, [
			'start_date' => 'required|after:yesterday',
			'end_date' => 'required|after:start_date',
			'course' => 'required',
			/*'location' => 'required'*/
			]);

		$input = $request->all();
		
		$student_id = Student::where('ref_id', $ref_id)->first()->id;
		$tutor = Tutor::find(Auth::user()->id);

		//Find Previous Rate
		//Try To Find Previous Contract
		$previous_assignment = Assignment::where('student_id', $student_id)
									->where('tutor_id', Auth::user()->id)
									->whereNotNull('tutor_contract_id')
									->orderBy('id', 'desc')
									->first();
		
		if ($previous_assignment) {
			$previous_contract = TutorContract::find($previous_assignment->tutor_contract_id);
			$student_rate = $previous_contract->student_rate;
			$tutor_rate = $previous_contract->tutor_rate;
			$location_id = $previous_assignment->location_id;
		}

		else {
			$first_assignment = Assignment::where('student_id', $student_id)
									->where('tutor_id', Auth::user()->id)
									->orderBy('id', 'desc')
									->first();

			$student_rate = $first_assignment->rate;
			$tutor_rate = number_format($student_rate * (1-$tutor->profile->tutor_fee),2);
			$location_id = $first_assignment->location_id;
		}
		

		//Create Contract
		$new_contract = TutorContract::create([
    					'ref_id' => mt_rand(10000000, 99999999),
    					'tutor_id' => $tutor->id,
    					'student_rate' => $student_rate,
    					'tutor_rate' => $tutor_rate,
    					'start_date' => $input['start_date'],
    					'end_date' => $input['end_date'],
    					'course' => $input['course'],
    					'tutor_decision' => 1,
    					'student_decision' => 1,
    					'tutor_decision_date' => Carbon\Carbon::now(),
    					'student_decision_date' => Carbon\Carbon::now(),
    					'admin_notes' => "Created By $tutor->full_name"
    				]);

		$assignment = new Assignment([
			'ref_id' => rand(10000000, 99999999),
			'tutor_contract_id' => $new_contract->id,
			'start_date' => $input['start_date'],
			'end_date' => $input['end_date'],
			'tutor_id' => Auth::user()->id,
			'student_id' => $student_id,
			'assigned_by' => Auth::user()->id,
			'location_id' => $location_id,
			'course' => $input['course'],
			]);


		if(!$assignment->save())
		{
			abort(403, "Unable to make this assignment. Please try again.");
		}
		
		//Create Student Email
        $user = User::find($assignment->student_id);
        $location = $assignment->location;
        $subject = "Resuming Tutor Sessions With {$assignment->tutor->full_name}";
        $email_view = 'btn.emails.students.assignment_resume_sessions';
        $email_data = ['assignment' => $assignment];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

		\Session::flash('success', 'Student Successfully Added To Your List.');

		//return redirect()->route('my.account');
		return redirect()->route('profile.account', $tutor->ref_id);
	}

	public function getCourseCatalog()
	{
		$schools = DB::table('school_details')->orderBy('town')->get();
		//return view('btn.pages.school_details', compact('schools'));
		return view('btn.pages.citybook.school_details', compact('schools'));
	}
}
