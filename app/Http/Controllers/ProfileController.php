<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Profile;
use App\Assignment;
use App\Location;
use App\Evaluation;
use App\Refund;
use App\Payment;
use App\ContractorPayment;
use App\MarketTutorRequest;
use App\MsgThread;
use App\MsgMessage;
use App\Tutor;
use App\Student;
use App\Favorite;
use App\TutorApplicant;
use App\ProfExamResult;
use App\CourseLevel;
use App\Geolocation;

use Carbon;
use Auth;
use DB;

use App\Jobs\SendGeneralNotification;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

class ProfileController extends Controller
{
	public function addRoleToUser($role, $user_ref_id )
	{
		$user = User::where('ref_id', $user_ref_id)->first();
        $this->authorize('admin-only'); 
        $user->addRole($role);
        \Session::flash('success', "Role of $role->role successfully added to $user->full_name's account");
        return redirect()->back();
	}

	public function removeRoleFromUser($role, $user_ref_id)
	{
		$user = User::where('ref_id', $user_ref_id)->first();

		$admin_only_roles = ['banned', 'super-user', 'admin', 'registered']; //roles that only and admin can remove from a user
        if (in_array($role, $admin_only_roles)) {
        	$this->authorize('admin-only'); 
        	$user->removeRole($role);
        }
        else{
        	$this->authorize('self-access', $user);	
        	if ($role == 'student' || $role == 'music-student') {
        		//Remove both roles of student & music-student
        		$user->removeRole('student');
        		$user->removeRole('music-student');
        	}
        	else
        		$user->removeRole($role);
        }

        \Session::flash('success', "Role of $role successfully removed from $user->full_name's account");
        return redirect()->back();
	}

	public function OLDgetAccount($user_ref_id)
	{
        $tutor = $student = $terms_array = $tutor_contract_date = $pending_proposals = $unreviewed_contracts = null;

        $is_online_tutor = 0;

        $user = User::where('ref_id', $user_ref_id)->first();
        $this->authorize('self-access', $user);

        if ($user->isTutor())
        {
        	$tutor = Tutor::with('tutorSessions', 'assignments', 'refunds', 'evaluations')->where('ref_id', $user_ref_id)->first();	
        	$this->authorize('self-access', $tutor);

        	$tutor_contract_date = DB::table('settings')->where('field', "tutor_contract_date")->first()->value;

        	$unreviewed_contracts = $tutor->tutor_contracts()
        									->whereNull('tutor_decision')
        									->where('deadline', '>', Carbon\Carbon::now())
        									->count();

        	$is_online_tutor = $tutor->locations()->where('city', 'like', '%online%')->count();
        }
        
        if ($user->isStudent()) {
        	$student = Student::with('tutorSessions', 'assignments', 'refunds', 'evaluations')->where('ref_id', $user_ref_id)->first();	
        	$this->authorize('self-access', $student);
        	
        	$terms_array = DB::table('terms_of_use')->where('version', 'LIKE', '%terms_of_use%')->pluck('user_id');
        	
        	$pending_requests = $student->tutorRequests()->with('tutor_contracts')
				                            ->where('end_date', '>', Carbon\Carbon::now())
				                            ->get();
			$pending_proposals = 0;

			foreach ($pending_requests as $pending_request) {
				$pending_contracts = $pending_request->tutor_contracts->where('tutor_decision', 1)->where('student_decision', null);
				$pending_proposals += $pending_contracts->count();
			}
        }
        
        if ($user->isSuperUser()) {
        	//Unread messages
        	/*$threads = MsgThread::whereHas('participants', function ($participant){
									//Find unread messages that are older than 12 hours
									$participant->whereNull('last_read')
												->where('updated_at', '<', Carbon\Carbon::now()->subHours(12));
								})
								->where('updated_at', '>', Carbon\Carbon::now()->subWeeks(2))
								->orderBy('updated_at', 'desc')
								->get();
			
			$suspicious_threads = MsgThread::suspiciousThreads()->where('updated_at', '>', Carbon\Carbon::now()->subMonth());
			
			$suspicious_messages = MsgMessage::suspiciousMessages()->whereIn('thread_id', $suspicious_threads->pluck('id'))->get();

			$ignored_threads = MsgThread::tutorIgnoredThreads()
									->where('created_at', '>', Carbon\Carbon::now()->subMonth())
									->where('created_at', '<', Carbon\Carbon::now()->subHours(12))
									->get();*/
        }

        $payment_methods = DB::table('payment_methods')->pluck('title', 'id');	

        return view('btn.profile.my_account', compact('user_ref_id', 'tutor', 'student', 'payment_methods', 'terms_array', /*'threads', 'suspicious_messages', 'ignored_threads'*/ 'tutor_contract_date', 'pending_proposals', 'unreviewed_contracts', 'is_online_tutor')); 
	}	

	public function storeRate(Request $request, $assign_ref_id)
	{
		$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
		
		$this->authorize('tutor-access', $assignment);

		$input = $request->all();
		$assignment->rate = $request->rate;
		$assignment->billing_increment = $request->billing_increment;
		$assignment->save();


		//E-mail Student
        $user = User::find($assignment->student_id);
        $location = $assignment->location;
        $subject = "Rate Submitted By {$assignment->tutor->user_name}";
        $email_view = 'btn.emails.students.rate_submitted';
        $email_data = ['assignment' => $assignment];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('success', 'Rate successfully created!');
        return redirect()->back();
	}

	public function updateRate(Request $request, $assign_ref_id)
	{
        $assignment = Assignment::where('ref_id', $assign_ref_id)->first();
     	
     	$this->authorize('tutor-access', $assignment);

        $assignment->update(['rate' => $request['rate'], 'billing_increment' => $request['billing_increment']]);

        
		//E-mail Student
        $user = User::find($assignment->student_id);
        $location = $assignment->location;
        $subject = "Rate Submitted By {$assignment->tutor->user_name}";
        $email_view = 'btn.emails.students.rate_submitted';
        $email_data = ['assignment' => $assignment];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('success', 'Rate successfully updated!');
        return redirect()->back();
	}

    public function updateAssignEndDate(Request $request, $id)
    {
        $assignment = Assignment::find($id);

    	$this->authorize('tutor-access', $assignment);

    	$assignment->end_date = $request->end_date;
    	$assignment->save();

    	//E-mail Student
        $user = User::find($assignment->student_id);
        $location = $assignment->location;
        $subject = "Tutor Assignment End Date Changed";
        $email_view = 'btn.emails.students.end_date_updated';
        $email_data = ['assignment' => $assignment];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('success', 'End date successfully updated!');
        return redirect()->back();
    }

	public function OLDgetProfile($ref_id)
	{
		$user = User::where('ref_id', $ref_id)->first();

		//Safe guard: Only active tutors can have their profiles viewed.  If not active, then only the user can view his/her own profile.
		if (!Tutor::activeTutors()->where('id', $user->id)->count()) {
			$this->authorize('self-access', $user);
		}

		$evaluations = Evaluation::where('tutor_id', $user->id)
									->select('assignment_id', 'type', 'satisfied', 'website_reference', 'school', 'created_at')
									->orderBy('id', 'DESC')
									->get();
		
		//Find most tutor recent request submitted by student/user
		$recent_request = null;
		if (Auth::check()) {
			if($recent_request = MarketTutorRequest::where('student_id', auth()->user()->id)->where('created_at', '>', Carbon\Carbon::now()->subWeek(2))->get()->last())
            {
				$recent_request->location = $recent_request->location_id;
				$recent_request->session_time = explode(", ", $recent_request->session_time);
				if ($recent_request->urgency != 'High') {
					$recent_request->urgency = null;
				}
			}
		}
		
		$music_files = array();
		$user_dir = null;
		
		if($user->hasRole('music-teacher'))
        	{
        		$user_name = $user->first_name.substr($user->last_name, 0, 1);
        		$user_dir = "2195-93085965-SzilviaS";
        		//$user_dir = "$user->id-$user->ref_id-$user_name$";
        		$dir= base_path("public/btn/music/$user_dir");
        		if ($dir_open = opendir($dir)) {
					while ($file = readdir($dir_open)) {
						if ($file != "." && $file != "..")
							array_push($music_files, $file);
					}
        		}
        	}

		return view('btn.profile.index', compact('user', 'evaluations', 'music_files', 'user_dir', 'recent_request'));
	}

	public function OLDeditProfile($user_ref_id)
	{
		$user = User::where('ref_id', $user_ref_id)->first();
		
		$this->authorize('self-access', $user);


		$locations = Location::tutorsVisible()->get();

		$tutor_availability = explode('|*|', $user->profile->tutor_availability);

		return view('btn.profile.edit', compact('user', 'locations', 'tutor_availability'));
	}
	
	public function OLDupdateProfile(Request $request, $user_ref_id)
	{
		$user = User::where('ref_id', $user_ref_id)->first();
		$this->authorize('self-access', $user);

		$user->locations()->sync($request->locations);

		$input = $request->all();
		
		if ($input['tutor_availability'] ?? '') {
			$input['tutor_availability'] = implode($request->tutor_availability, '|*|');
		}
		
		unset($input['locations']);

		$user->profile->update($input);
		
		\Session::flash('success', 'Profile Updated');
		
		return redirect()->back();
	}

	/******CITY BOOK THEME******/
	public function getProfile($ref_id)
	{
		$user = User::where('ref_id', $ref_id)->first();

		//Safe guard: Only active tutors can have their profiles viewed.  If not active, then only the user can view his/her own profile.
		if (!Tutor::activeTutors()->where('id', $user->id)->count()) {
			$this->authorize('self-access', $user);
		}

		$tutor = Tutor::find($user->id);
		$promo_discount = DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100;

		$timezone = "America/Denver";

		if ($tutor->profile->timezone) {
			$timezone = DB::table('timezones')->where('id', $tutor->profile->timezone)->first()->timezone;
		}

	    $references = $tutor->evaluations()->where('website_reference', '!=', '')->orderBy('created_at', 'DESC')->get();
	    $reviews_count = $references->count();
	    $references = $references->splice(0,3);

		$students_assisted = $tutor->assignments->count();
		$total_evaluations = $tutor->evaluations()->where('satisfied', 1)->count();
		$hours_tutored = ceil($tutor->tutorSessions->sum('duration'));
	    
	    if ($tutor->id == 63) {
	    	$additional_eval = DB::table('evaluations_mogi')->count(); 
	    	$reviews_count += DB::table('evaluations_mogi')->where('website_reference', '!=', '')->count();
	    	$students_assisted += $additional_eval * 4;
	    	$total_evaluations += $additional_eval;
	    	$hours_tutored += 1500;
	    }

	    $locations = $tutor->geolocations->toJSON();

		//Check if profile has been saved
		if (auth()->user()) {
			$saved = $user->favorites->where('tutor_id', $tutor->id)->count();
		}
		else{
			$saved = Favorite::where('identifier', session('identifier'))->where('tutor_id', $tutor->id)->count();
		}

		return view('btn.pages.citybook.tutor_profile', compact('tutor', 'promo_discount','timezone', 'references', 'reviews_count', 'students_assisted', 'total_evaluations', 'hours_tutored', 'locations', 'saved'));
	}

	public function getTutorReviews($user_ref_id)
	{
		$tutor = Tutor::where('ref_id', $user_ref_id)->first();
	    $references = $tutor->evaluations()->where('website_reference', '!=', '')->orderBy('created_at', 'DESC')->get();
	    $reviews_count = $references->count();
	    $prior_references = null;

	    if ($tutor->id == 63) {
	    	$prior_references = DB::table('evaluations_mogi')->where('website_reference', '!=', '')->orderBy('id', 'DESC')->get();
	    	
	    	$reviews_count += count($prior_references);
	    }
	   	return view('btn.pages.citybook.tutor_reviews', compact('tutor', 'references', 'reviews_count', 'prior_references')); 
	}

	public function editProfile($user_ref_id)
	{
		$user = User::where('ref_id', $user_ref_id)->first();
		
		$this->authorize('self-access', $user);
		//$user = User::find(498);
		$profile = $user->profile;

		$timezones = DB::table('timezones')->where('active', 1)->pluck('timezone', 'id');
		$timezones = ['' => 'Select Time Zone'] + $timezones;

		$tutor_availability = explode('|*|', $user->profile->tutor_availability);

		$dropbox_avatars = DB::table('settings')->where('field', 'dropbox_avatars')->first()->value;

		$course_levels = CourseLevel::orderBy('website_order')->pluck('description', 'id')->toArray();
		
		$tutor = Tutor::find($user->id);
		$tutor_course_levels = $tutor->course_levels->pluck('id')->toArray();
		
		$tutor_contract_date = DB::table('settings')->where('field', "tutor_contract_date")->first()->value;

		$geolocations = null;
		$geo_array = [];
		$geo_count = 0;
		$radius_unit = 'miles';
		//Tutor Locations
		if ($tutor->geolocations->count()) {
			foreach ($tutor->geolocations as $geolocation) {
				$geo_array[$geo_count] = ['lat' => $geolocation->lat,
							'lng' => $geolocation->lng,
							'radius' => $geolocation->radius,
							'radius_unit' => $geolocation->radius_unit];

				$geo_count++;
			}
			$radius_unit = $geolocation->radius_unit;
		}
		elseif($user->lat){
			$geo_array[0] = ['lat' => $user->lat,
							 'lng' => $user->lng,
							 'radius' => 0,
							 'radius_unit' => 'miles'];
		}
		else{
			foreach ($user->locations as $location) {
				if ($location->lat) {
					$geo_array[$geo_count] = ['lat' => $location->lat,
							 'lng' => $location->lng,
							 'radius' => 0,
							 'radius_unit' => 'miles'];
					$geo_count++;
				}
			}
		}

		$map_center = json_encode($geo_array[0]);
		$geolocations = json_encode($geo_array);

		

		return view('btn.pages.citybook.edit_profile', compact('user', 'tutor_course_levels', 'timezones', 'profile', 'tutor_availability', 'dropbox_avatars', 'course_levels', 'geolocations', 'map_center', 'radius_unit', 'tutor_contract_date'));
	}

	public function updateContact(Request $request, $user_ref_id)
	{	
		$user = User::where('ref_id', $user_ref_id)->first();
		$this->authorize('self-access', $user);

		$this->validate($request, [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'email' => 'required|email|unique:users,email,'.$user->id,
			'phone' => 'required',
			'password' => 'min:6|confirmed',
			]);
		
		$user->first_name = $request['first_name'];
		$user->last_name = $request['last_name'];
		$user->email = $request['email'];
		$user->phone = $request['phone'];

		if (trim($request['password']) != '') {
			$user->password = bcrypt($request['password']);	
		}
		
		$user->save();

		\Session::flash('success', 'Contact Info Updated');
		
		return redirect()->back();
	}

	public function updateProfile(Request $request, $user_ref_id)
	{
		$tutor = Tutor::where('ref_id', $user_ref_id)->first();
		$this->authorize('self-access', $tutor);

		if ($request->course_levels) {
			$tutor->course_levels()->sync($request->course_levels);
		}	

		$input = $request->all();
		
		if ($input['tutor_availability'] ?? '') {
			$input['tutor_availability'] = implode($request->tutor_availability, '|*|');
		}
		
		if (!array_key_exists('in_person', $input)){
			$input['in_person'] = 0;
		}

		if (!array_key_exists('online', $input)){
			$input['online'] = 0;
		}

		unset($input['locations'], $input['course_levels']);

		$tutor->profile->update($input);
		
		\Session::flash('success', 'Profile Updated');
		
		return redirect()->back();
	}

	public function saveTutor(Request $request, $tutor_ref_id){
		$user = auth()->user();		

		$tutor = Tutor::where('ref_id', $tutor_ref_id)->first();
		
		$fav_tutors = session()->has('fav_tutors') ? session('fav_tutors') : array();
		
		if ($request->decision == "save") {
			if (!$user) {
				//ADD TO SESSION ARRAY	        
		        $fav_key = array_search($tutor->id, $fav_tutors);
		        if ($fav_key === FALSE) {
		        	array_push($fav_tutors, $tutor->id);
		        }
		        
		        session(['fav_tutors' => array_unique($fav_tutors)]);
			}

			//Restore softDelete or create a new one
			$deleted_fav = Favorite::withTrashed()
		        					->where('user_id', $user->id ?? null)
		        					->where('tutor_id', $tutor->id)
		        					->whereNotNull('deleted_at')
		        					->first();
		    if ($deleted_fav) {
		    	$deleted_fav->restore();
		    }
		    else
		    {
		    	Favorite::create(['user_id' => $user->id ?? null,
		    				'identifier' => session('identifier'), 
	        				'tutor_id' => $tutor->id,]);	
		    }
		}
		else{
			if ($user) {
				$favorite = Favorite::where('user_id', $user->id)
										->where('tutor_id', $tutor->id)
										->first();

				if ($favorite) {
					$favorite->delete();
				}
			}
			else{
				//REMOVE FROM SESSION ARRAY
				$fav_key = array_search($tutor->id, $fav_tutors);
				if ($fav_key !== FALSE) {
		        	unset($fav_tutors[$fav_key]);
		        }
		        session(['fav_tutors' => array_unique($fav_tutors)]);
		        //UPDATE FROM DB
		       	$favorite = Favorite::where('identifier', session('identifier'))
										->where('tutor_id', $tutor->id)
										->first();
				if ($favorite) {
					$favorite->delete();
				}
			}
		}
		
		return response()->json(array('msg' => json_encode(session('fav_tutors'))), 200);
	}

	public function favoriteTutors(){
		$user = auth()->user() ?? null;

		$education_levels = [ 
			'phd' => "PhD", 
			'masters' => "Master's", 
			'grad_student' => "Graduate Student", 
			'bachelors' => "Bachelor's", 
			'undergrad' => "Undergraduate Student"];

		if ($user) {
			$favorites = $user->favorites;
		}
		else{
			$favorites = Favorite::has('tutor')->where('user_id', NULL)->where('identifier', session('identifier'))->get();
		}

		return view('btn.pages.citybook.favorite_tutors', compact('favorites', 'education_levels'));  
	}

	public function getAccount($user_ref_id)
	{
        $tutor = $student = $tutor_requests = $assignments_students = $assignments_tutors = $tutor_contract_date = $session_duration_array = $pending_proposals = $unreviewed_contracts = $incomplete_users = $incomplete_requests = null;

        $dropbox_transcripts = $dropbox_applicant_ids = $checkr_url = $background_check_fee = $exam_list = $previous_exams = $application = null;
        //$is_online_tutor = 0;

        $user = User::where('ref_id', $user_ref_id)->first();
        $this->authorize('self-access', $user);

        //Applicants
        if ($user->hasRole('tutor-applicant') && !$user->isTutor()) {
        	$dropbox_transcripts = DB::table('settings')->where('field', 'dropbox_transcripts')->first()->value;
        	$dropbox_applicant_ids = DB::table('settings')->where('field', 'dropbox_applicant_ids')->first()->value;
        	$checkr_url = DB::table('settings')->where('field', 'checkr_url')->first()->value;
        	$background_check_fee = DB::table('settings')->where('field', 'background_check_fee')->first()->value;

        	$exam_list = (new TutorApplicant)->exam_list();
			$previous_exams = ProfExamResult::where('user_id', $user->id)->get();
			$application = $user->tutorApplicant; 
			$references = $application->student_references;

        }
        
        if ($user->isTutor())
        {
        	$tutor = Tutor::with('tutorSessions', 'assignments', 'refunds', 'evaluations')->where('ref_id', $user_ref_id)->first();	
        	$this->authorize('self-access', $tutor);

        	$tutor_contract_date = DB::table('settings')->where('field', "tutor_contract_date")->first()->value;

        	$assignments_tutors = $tutor->assignments()->has('tutorContract')->current()->get();
        	
        	$session_duration_array = ['' => "Select Session Duration",
									30 => "30 min",
									45 => "45 min",
									60 => "1 hour",
									75 => "1 hour & 15 min",
									90 => "1 hour & 30 min",
									105 => "1 hour & 45 min",
									120 => "2 hours",
									135 => "2 hours & 15 min",
									150 => "2 hours & 30 min",
									165 => "2 hours & 45 min",
									180 => "3 hours"];

        	$unreviewed_contracts = $tutor->tutor_contracts()
        									->whereNull('tutor_decision')
        									->where('deadline', '>', Carbon\Carbon::now())
        									->where('end_date', '>', Carbon\Carbon::now())
        									->count();

        	//$is_online_tutor = $tutor->locations()->where('city', 'like', '%online%')->count();
        }
        
        if ($user->isStudent()) {
        	$student = Student::with('tutorSessions', 'assignments', 'refunds', 'evaluations')->where('ref_id', $user_ref_id)->first();	
        	$this->authorize('self-access', $student);
        
        	$tutor_requests = $student->tutorRequests()
        							/*->has('tutor_contracts')*/
        							->orderBy('id', 'DESC')
        							->where('end_date', '>', Carbon\Carbon::now())
        							->get();

        	$assignments_students = $student->assignments()->has('tutorContract')->current()->get();

        	$pending_requests = $student->tutorRequests()->with('tutor_contracts')
				                            ->where('end_date', '>', Carbon\Carbon::now())
				                            ->get();
			$pending_proposals = 0;

			foreach ($pending_requests as $pending_request) {
				$pending_contracts = $pending_request->tutor_contracts->where('tutor_decision', 1)->where('student_decision', null);
				$pending_proposals += $pending_contracts->count();
			}
        }
        
        if (auth()->user()->isSuperUser() && $user->id == auth()->user()->id) {
        	$incomplete_users = User::where('created_at', '>', "2019-01-04")->where('city', '')->get();

        	$incomplete_requests = MarketTutorRequest::where('city', '')->get();
        }

        $payment_methods = DB::table('payment_methods')->pluck('title', 'id');	
        return view('btn.pages.citybook.my_account', compact('user', 'student', 'tutor', 'application', 'references', 'dropbox_transcripts', 'dropbox_applicant_ids', 'checkr_url', 'background_check_fee', 'exam_list', 'previous_exams', 'tutor_requests', 'assignments_students', 'assignments_tutors', 'session_duration_array', 'unreviewed_contracts', 'pending_proposals', 'incomplete_users', 'incomplete_requests'));

        /*return view('btn.profile.my_account', compact('user_ref_id', 'tutor', 'student', 'payment_methods', 'terms_array', 'tutor_contract_date', 'pending_proposals', 'unreviewed_contracts', 'is_online_tutor')); */
	}

	public function updateGeolocations(Request $request, $user_ref_id){
		$tutor = Tutor::where('ref_id', $user_ref_id)->first();
		
		$this->authorize('self-access', $tutor);

		//Update session_locations in profiles
		$tutor->profile->update(['session_locations' => $request->session_locations]);

		//Delete all previous locations
		foreach ($tutor->geolocations as $geolocation) {
			$geolocation->forceDelete();
		}

		$marker_radius = $request->radius;
		$market_lat = $request->lat;
		$marker_lng = $request->lng;

		$google_api = config('services.google_maps.key');
		$google_url = "https://maps.googleapis.com/maps/api/geocode/json?key=$google_api&latlng=";
		
		// Get cURL resource
		$curl = curl_init();
		
		foreach ($request->lat as $marker_id => $lat) {
			
			$lng = $marker_lng[$marker_id];
			// Set some options - we are passing in a useragent too here
			curl_setopt_array($curl, array(
			    CURLOPT_RETURNTRANSFER => 1,
			    CURLOPT_URL => $google_url . "$lat,$lng",
			    /*CURLOPT_USERAGENT => 'Codular Sample cURL Request'*/
			));

			// Send the request & save response to $resp
			$geoloc = json_decode(curl_exec($curl), true);
			
			foreach ($geoloc['results'][0]['address_components'] as $key => $geo) {
				if (in_array('locality', $geo['types'])) {
					$city = $geo['long_name'];
				}

				if (in_array('administrative_area_level_1', $geo['types'])) {
					$region = $geo['long_name'];
				}
				if (in_array('country', $geo['types'])) {
					$country = $geo['long_name'];
				}
			}

			Geolocation::create([
				'ref_id' => strtoupper(str_random(15)),
				'tutor_id' => $tutor->id,
				'radius' => $marker_radius[$marker_id],
				'radius_unit' => $request->radius_unit,
				'city' => $city,
				'region' => $region,
				'country' => $country,
				'lat' => $lat,
				'lng' => $lng,
				]);
		}

		// Close request to clear up some resources
		curl_close($curl);

		\Session::flash('success', 'Locations Updated');
		
		return redirect()->back();
	}	
}
