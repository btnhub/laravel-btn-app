<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TutorSession;
use App\Refund;
use App\User;
use App\Student;
use App\Location;
use App\Assignment;
use Auth;
use DB;
use Carbon\Carbon;

use App\Jobs\EmailAdmin;
use App\Jobs\SendGeneralNotification;

class RefundsController extends Controller
{
    public function getSessionDetails($session_ref_id)
    {
    	$refund_methods = DB::table('payment_methods')->where('refundable' , 1)->get();

        $deductions = 0;
    	$tutor_session = TutorSession::where('ref_id', $session_ref_id)->firstOrFail();
        $this->authorize('student-access', $tutor_session);

        $requested_amount = $tutor_session->amt_charged;
    	
        if (!$tutor_session->isSoleSession($tutor_session->assignment_id))
        {
    		// Set refund amount to the rate of billing increment
            //$rate = $tutor_session->assignment->rate;
            $rate = $tutor_session->assignment->tutorContract->student_rate;
            if ($requested_amount > $rate) {
    			$requested_amount = $rate;
    		}
    		//$deductions = number_format($requested_amount * 0.25, 2);
            $deductions = number_format($tutor_session->assignment->tutorContract->student_rate - $tutor_session->assignment->tutorContract->tutor_rate, 2);
    	}

    	//return view('btn.forms.refund_session', compact('tutor_session', 'requested_amount', 'deductions', 'refund_methods'));
        return view('btn.pages.citybook.refund_session', compact('tutor_session', 'requested_amount', 'deductions', 'refund_methods'));
    }

    public function requestSessionRefund(Request $request, $session_ref_id){
    	
        $requested_amount = $request['requested_amount'];
        $deductions = $request['deductions'];

        //Get details about refunded session
        $student = User::find(Auth::user()->id);

        $session = TutorSession::where('ref_id', $session_ref_id)->firstOrFail();
        $this->authorize('student-access', $session);

        $assignment_id = $session->assignment_id;

        $refund = new Refund([
                        'ref_id' => rand(10000000, 99999999),
                        'student_id' => $session->student_id,
                        'tutor_id' => $session->tutor_id,
                        'session_id' =>$session->id,
                        'assignment_id' => $assignment_id,
                        'requested_amount' => $requested_amount,
                        ]);

        $flash_msg = 'Refund request submitted. Refunds are processed within 48 hours. Check your account (Refunds Table) for status updates.';

        //Determine whether it's a first or last session refund and add to Refund model
        if ($session->isSoleSession($assignment_id)) 
        {
            $refunded_to_student = $session->amt_charged;

            $refund->refund_reason = 'First Session Refund';
            $refund->refunded_to_account = $refunded_to_student;
            $refund->refunded_to_student = $refunded_to_student;
            $refund->refunded_from_tutor = $session->tutor_pay;
            $refund->refunded_from_btn = $session->btn_pay;
            
            if (!$refund->save()) 
            {
                abort(500, "Could not submit your refund request.  Please try again.");
            }
        }

        else
        {    
            $refunded_to_student = $requested_amount - $deductions;

            $refund->refund_reason = 'Terminate Sessions';
            $refund->deductions = $deductions;
            $refund->refunded_to_account = $refunded_to_student;
            $refund->refunded_to_student = $refunded_to_student;
            $refund->refunded_from_tutor = $refunded_to_student;
            $refund->refunded_from_btn = NULL;

            if (!$refund->save()) 
            {
                abort(500, "Could not submit your refund request.  Please try again.");
            }

            $flash_msg = 'Refund request submitted.  Refunds are processed within 48 hours.  Check your account (Refunds Table) for status updates.';
        }
        
        //If refund is ultimately going to the student, first refund the student's account (above), then create new refund to refund account balance
/*        if ($request['refund_destination'] == 'student') 
        {    
            //New account balance if session refund is issued
            $new_balance = $student->studentBalance($student->id) + $refunded_to_account;

            $refund2 = new Refund([
                            'ref_id' => rand(10000000, 99999999),
                            'student_id' => $session->student_id,
                            'requested_amount' => $new_balance,
                            'refund_reason' => "Balance Refund",
                            'refund_method' => $request['refund_method'],
                            'method_details' => $request['method_details'],
                            'refunded_to_student' => $new_balance,
                            ]);

            if (!$refund2->save()) 
            {
                abort(500, "Could not submit your refund request.  Please try again.");
            }

            $flash_msg = 'Session and Account Balance refund requests submitted.  Refunds are processed within 48 hours.  Check your account (Refunds Table) for status updates.';
        } */     
        
        //Change assignment end date
        $assignment = Assignment::where('id', $assignment_id)->firstOrFail();
        $assignment->end_date = Carbon::now();
        $assignment->save();

        //Add Refund id to Tutor Session under review
        $session->refund_id = $refund->id;
        $session->save();
        
        //E-mail Admin
        $location = $assignment->location;
        $subject = "Refund Requested By {$assignment->student->full_name}";
        $email_view = 'btn.emails.admin.refund_requested';
        $email_data = ['assignment' => $assignment, 'refund' => $refund];
        
        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

        //E-mail Tutor
        $user = User::find($assignment->tutor_id);
        $email_view = 'btn.emails.tutors.refund_requested';
        $email_data = ['assignment' => $assignment, 'refund' => $refund];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
        \Session::flash('success', $flash_msg);
        
        return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }

    public function getBalance()
    {
        $student = Student::find(Auth::user()->id);
        $refund_methods = DB::table('payment_methods')->where('refundable' , 1)->get();
        return view('btn.forms.refund_balance', compact('refund_methods', 'student'));
    }

    public function requestBalanceRefund(Request $request)
    {
        $student = Student::find(Auth::user()->id);
        $balance = $student->studentBalance();
        $refund = new Refund([
                        'ref_id' => rand(10000000, 99999999),
                        'student_id' => $student->id,
                        'requested_amount' => $balance,
                        'refund_reason' => "Balance Refund",
                        'refund_method' => $request['refund_method'],
                        'method_details' => $request['method_details'],
                        'refunded_to_student' => $balance,
                        ]);

        if (!$refund->save()) 
        {
            abort(500, "Could not submit your refund request.  Please try again.");
        }

        //E-mail Admin
        if(!$location = $student->locations->first())
        {
            $location = Location::first();
        }

        $subject = "Balance Refund Requested By {$refund->student->full_name}";
        $email_view = 'btn.emails.admin.refund_balance_requested';
        $email_data = ['refund' => $refund];
        
        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

        \Session::flash('success', 'Account Balance refund request submitted.  Refunds are processed within 48 hours.  Check your account (Refunds Table) for status updates.');
    
        return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }
}
