<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon;
use DB;

class StripeConnectController extends Controller
{
    public function connectSignUp()
    {
		//REF: https://gist.github.com/amfeng/3507366
  		define('AUTHORIZE_URI', 'https://connect.stripe.com/oauth/authorize');

		$authorize_request_body = array(
		      'response_type' => 'code',
		      'scope' => 'read_write',
		      'client_id' => config('services.stripe.client_id')
		    );
		    $url = AUTHORIZE_URI . '?' . http_build_query($authorize_request_body);
	    
	    return redirect($url);
    }

    public function authorizeConnect()
    {
    	//REF: https://gist.github.com/amfeng/3507366
    	define('TOKEN_URI', 'https://connect.stripe.com/oauth/token');

    	if (isset($_GET['code'])) 
    	{ 
		    $code = $_GET['code'];
		    $token_request_body = array(
		      'client_secret' => config('services.stripe.secret'),
		      'grant_type' => 'authorization_code',
		      'client_id' => config('services.stripe.client_id'),
		      'code' => $code,
		    );
		    $req = curl_init(TOKEN_URI);
		    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($req, CURLOPT_POST, true );
		    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
		    // TODO: Additional error handling
		    $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
		    $resp = json_decode(curl_exec($req), true);
		    curl_close($req);
		    //echo $resp['access_token'];
		    
		    //Store Stripe's response authentication cvredentials ($resp) in database table
		    DB::table('stripe_connect_tutors')->insert(
    		[
    			'user_id' => auth()->user()->id,
    			'first_name' => auth()->user()->first_name,
    			'last_name' => auth()->user()->last_name,
    			'stripe_user_id' => $resp['stripe_user_id'],
    			'token_type' => $resp['token_type'],
    			'stripe_publishable_key' => $resp['stripe_publishable_key'],
    			'scope' => $resp['scope'],
    			'livemode' => $resp['livemode'],
    			'refresh_token' => $resp['refresh_token'],
    			'access_token' => $resp['access_token'],
    			'created_at' => Carbon\Carbon::now(),
    			'updated_at' => Carbon\Carbon::now()
    		]);
		    
		    \Session::flash('success', 'Successfully connected your Stripe account with our Platform');
		    return view('btn.stripe_connect.confirmation');	
		  } 
		  
		  else if (isset($_GET['error'])) { // Error
		    \Session::flash('error', $_GET['error_description']);
		    return view('btn.stripe_connect.failed_to_connect');	
		   } 	
    }
}
