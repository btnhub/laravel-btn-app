<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Tutor;
use App\Role;
use App\Assignment;
use App\MarketSubject;
use App\MarketNotification;
use App\MarketTutorRequest;
use App\MarketTutorProposal;
use App\Location;

use App\MsgThread as Thread;
use App\MsgMessage as Message;
use App\MsgParticipant as Participant;

use App\Jobs\CreateAssignment;
use App\Jobs\SendGeneralNotification;

use Carbon\Carbon;
use Auth;
use Yajra\Datatables\Datatables;

class MarketPlaceController extends Controller
{
    /**
     * Function to create assignment (add to database)
     */
    public function createAssignmentDB(MarketTutorRequest $tutor_request, $tutor_id, $assigned_by, $submitted_by)
    {
        $ref_id = mt_rand(10000000, 99999999);

        $assignment = Assignment::create([
            'ref_id' => $ref_id,
            'start_date' => $tutor_request->start_date,
            'end_date' => $tutor_request->end_date,
            'student_id' => $tutor_request->student_id,
            'child_name' => $tutor_request->child_name,
            'tutor_id' => $tutor_id,
            'assigned_by' => $assigned_by,
            'location_id' => $tutor_request->location_id,
            'course' => $tutor_request->course,
            ]);
        
        if ($assignment) {
            //Update Tutor Request and include Match details
            $tutor_request->mkt_visibility = 0;
            $tutor_request->matched_tutor_id = $tutor_id;
            $tutor_request->matched_date = Carbon::now();
            $tutor_request->matched_by_id = $assigned_by;
            $tutor_request->assignment_id = $assignment->id;
            $tutor_request->unassigned = 0;
            
            $tutor_request->save();
            
            if ($submitted_by == 'student') {
                if($tutor_proposal = MarketTutorProposal::where('tutor_request_id', $tutor_request->id)->where('tutor_id', $tutor_id)->first())
                {
                        $tutor_proposal->accepted_proposal = 1;
                        $tutor_proposal->assignment_id = $assignment->id;
                        
                        $tutor_proposal->save();
                }
            }
            
            return $assignment;
        }

        else
        {
            return false;
        }
    }

    /**
     * Allows tutor to join Marketplace by adding role of marketplace-tutor to user
     * @return [type] [description]
     */
    public function joinMarketPlace()
    {
        //Add middleware or way to authorize who can add to marketplace
        //Tutor in good standing
        
        $user = User::find(Auth::user()->id);

        if (!$user->addRole('marketplace-tutor')) {
            \Session::flash('error', 'Could not sign you up.  Please try again.');    
            return redirect()->back();
        }
        
        \Session::flash('success', 'You have joined the MarketPlace');
        
        return redirect()->back();
    }

    /**
     * Removes tutor from MarketPlace by removing the marketplace-tutor role from the user
     * @return [type] [description]
     */
    public function leaveMarketPlace()
    {
        $user = User::find(Auth::user()->id);

        if (!$user->removeRole('marketplace-tutor')) {
            \Session::flash('error', 'Unable to remove you from the list.  Please try again later.');    
            return redirect()->back();
        }
        
        \Session::flash('success', 'You have left the MarketPlace');
        
        return redirect()->back();
    }

    /**
     * Create array of subjects that the tutor has NOT signed up for notifications
     * @return [type] [description]
     */
    public function subjectArray($department, $existing_subjects){
        $subject_array = MarketSubject::createSubjectArray($department)->reject(function ($value, $key) use ($existing_subjects) {
                return boolval($existing_subjects->search($key));
            });

        return $subject_array;
    }

    /**
     * List subjects that tutor can sign up for notifications.
     * @return [type] [description]
     */
    public function subjectNotifications(){
        $tutor = Tutor::find(Auth::user()->id);

        $existing_subjects = $tutor->notifications->pluck('subject_id');

        $subjects = MarketSubject::whereNotIN('id', $existing_subjects)
                                    ->select('id', 'department')
                                    ->where('department', 'not like', 'General: %')
                                    ->where('department', 'not like', 'Music: %')
                                    ->where('department', 'not like', 'Math: %')
                                    ->where('department', 'not like', 'Languages: %')
                                    ->where('department', 'not like', 'Engineering: %')
                                    ->where('department', 'not like', 'Humanities: %')
                                    ->orderBy('department')
                                    ->pluck('department', 'id');
        
        $subjects_general = $this->subjectArray('General', $existing_subjects);
        $subjects_music = $this->subjectArray('Music', $existing_subjects);
        $subjects_math = $this->subjectArray('Math', $existing_subjects);
        $subjects_languages = $this->subjectArray('Languages', $existing_subjects);
        $subjects_engineering = $this->subjectArray('Engineering', $existing_subjects);
        $subjects_humanities = $this->subjectArray('Humanities', $existing_subjects);
        
        $locations = $tutor->locations()->select('location_id', 'city')->get();
        
        return view('btn/marketplace/subject_notifications', compact('tutor', 'subjects', 'subjects_general', 'subjects_music', 'subjects_math', 'subjects_engineering', 'subjects_languages', 'subjects_humanities', 'locations'));
    }

    /**
     * Store selected subject notifications
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function storeSubjectNotification(Request $request)
    {
        $this->validate($request, [
            'min_rate' => 'required|integer|min:10',
            'subjects' => 'required',
            'locations' => 'required']);

        $tutor = Tutor::find(Auth::user()->id);
        
        $locations = $request['locations'];
        $subjects = $request['subjects'];
        
        foreach ($subjects as $key => $subject_id) {
            foreach ($locations as $location) {
                $notification = new MarketNotification([
                    'tutor_id' => $tutor->id,
                    'location_id' => $location,
                    'subject_id' => $subject_id,
                    'min_rate' => $request['min_rate'],
                    'notify' => 1
                ]);      

                if (!$notification->save()) {
                    \Session::flash('error', "Unable to create your notification");    
                return redirect()->back();
                }
            }   
        }        

        \Session::flash('success', 'Notification(s) Created.');
        
        return redirect()->back();
    }

    /**
     * Update selected subject notification
     * @param  Request $request         [description]
     * @param  [type]  $notification_id [description]
     * @return [type]                   [description]
     */
    public function updateSubjectNotification(Request $request, $notification_id)
    {       
        
        $notification = MarketNotification::findOrFail($notification_id);
    
        $notification->update($request->all());

        \Session::flash('success', 'Notification Updated!');
        
        return redirect()->back();
    }

    public function updateAllNotifications($notify)
    {       
        dd('hello');
        /*  Gives Error: Controller method not found
        $notification = MarketNotification::where('tutor_id', Auth::user()->id)->get();
    
        $notification->notify = $notify;

        $notification->update();

        \Session::flash('success', 'All Notifications Updated!');
        
        return redirect()->back();*/
    }

    /**
     * Delete Selected Subject Notification
     * @param  [type] $notification_id [description]
     * @return [type]                  [description]
     */
    public function deleteSubjectNotification($notification_id)
    {       
        $notification = MarketNotification::findOrFail($notification_id);
    
        $notification->delete();

        \Session::flash('success', 'Notification Removed!');
        
        return redirect()->back();
    }

    /**
     * Delete All Subject Notifications
     * @return [type] [description]
     */
    public function deleteAllNotifications()
    {       
        $notification = MarketNotification::where('tutor_id', Auth::user()->id)->delete();

        \Session::flash('success', 'All Notifications Deleted!');
        
        return redirect()->back();
    }

    /**
     * TWO methods required to create datatable for MarketPlace Tutor Requests table
     * @return datatable Table listing tutor requests for MarketPlace
     */
    public function getTutorRequests()
    {    	
        return view('btn.marketplace.index');
    }

    public function getTutorRequestsData()
    {
    	$tutor_locations = User::find(Auth::user()->id)->locations()->pluck('location_id');

        $tutor_request = MarketTutorRequest::where('mkt_visibility', 1)
    									// ->where('matched_date', null)
                                        ->whereIN('location_id', $tutor_locations)
                                        ->where('student_id', '!=', auth()->user()->id)
                                        ->where('start_date', '>', Carbon::now()->subDays(7))
    									->orderBy('id', 'desc');
        
        if (Auth::user()->isSuperUser()) {
            $tutor_request = MarketTutorRequest::where('mkt_visibility', 1)
                                //->where('matched_date', null)
                                ->where('start_date', '>', Carbon::now()->subDays(7))
                                ->orderBy('id', 'desc');
        }

        //create table of all visible unmatched requests       
        return Datatables::of($tutor_request)
                            ->editColumn('start_date', function($tutor_request) {
                                return Carbon::parse($tutor_request->start_date)->diffForHumans();
                                })
                            ->editColumn('end_date', function($tutor_request) {
                                return Carbon::parse($tutor_request->end_date)->format('m/d/Y');
                                })
                            ->editColumn('student_id', function($tutor_request) {
                                if ($tutor_request->student) {
                                    $student_name = $tutor_request->student->user_name;
                                    return link_to_route('tutor_request.show', $student_name, ['ref_id' => $tutor_request->ref_id]);
                                }
                                else
                                {
                                    $student_name = "Deleted Student";
                                    return $student_name;
                                }
                                })
                            ->editColumn('urgency', function($tutor_request) {
                                $urgency = $tutor_request->urgency;
                                if ($urgency == "High") {
                                    $urgency = "<span style='color:red'>".$urgency."</span>";
                                }

                                return $urgency;
                                }) 
                            ->editColumn('level', function($tutor_request) {
                                return str_replace("_", " ", $tutor_request->level);
                                }) 
                            ->editColumn('max_rate', function($tutor_request) {
                                return "$".$tutor_request->max_rate."/hr";
                                })  
                            ->make(true);      
    }

    /**
     * Shows details of single Tutor Request
     * @param  Integer $ref_id Reference ID of tutor request
     * @return view         Details of tutor request selected
     */
    public function showRequestDetails($ref_id)
    {	
        $tutor_request = MarketTutorRequest::where('ref_id', $ref_id)->firstOrFail();
    	
        return view('btn.marketplace.tutor_request_details', compact('tutor_request'));
    }


    /**
     * Tutor's Rate proposal for selected tutor reqeuest
     * @param  Request $request Form Request: tutor's proposed rate
     * @param  Integer  $request_ref_id  Reference ID of the tutor request
     * @return [type]           [description]
     */
    public function submitProposal(Request $request, $request_ref_id)
    {
    	$this->validate($request, 
            [
                'rate' => 'required|integer',
                'message' => 'required'
            ]);

        $rate = $request->rate;
        $message = $request->message;

    	$tutor_request = MarketTutorRequest::where('ref_id', $request_ref_id)->firstOrFail();
        $tutor = Tutor::find(auth()->user()->id);
        
        //Create Message for Messenger
        $thread = Thread::create(
            [
                'subject' => "{$tutor->first_name}'s Proposal For $tutor_request->course",
            ]
        );
        // Message
        Message::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => $tutor->id,
                'body'      => $message,
            ]
        );
        // Sender
        Participant::create(
            [
                'thread_id' => $thread->id,
                'user_id'   => $tutor->id,
                'last_read' => Carbon::now(),
            ]
        );

        // Recipients (add student to thread)
        $thread->addParticipants([$tutor_request->student_id]);

        //Create MarketPlace Proposal
        $tutor_proposal = MarketTutorProposal::create([
                'tutor_request_id' => $tutor_request->id,
                'student_id' => $tutor_request->student_id,
                'tutor_id' => $tutor->id,
                'msg_thread_id' => $thread->id,
                'location_id' => $tutor_request->location_id,
                'proposed_rate' => $rate,
            ]);

        //Notify Student
        $user = User::find($tutor_request->student_id);
        $location = Location::find($tutor_request->location_id);
        $subject = "Proposal Submitted For $tutor_request->course";
        $email_view = 'btn.emails.students.proposal_submitted';
        $email_data = ['tutor_proposal' => $tutor_proposal];

        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

    	\Session::flash('success', "Proposal Successfully Submitted. Good Luck!");        
    	
    	return redirect()->route('marketplace');
    }

    public function studentAcceptedProposal($thread_id)
    {
        $tutor_request = MarketTutorRequest::where('msg_thread_id', $thread_id)->first();

        if(!$tutor_request)
        {
            $tutor_request_id = MarketTutorProposal::where('msg_thread_id', $thread_id)->first()->tutor_request_id;            
            $tutor_request = MarketTutorRequest::where('id', $tutor_request_id)->first();
        }
        
        if ($tutor_request->student_id != auth()->user()->id) 
        {
            \Session::flash('error', 'You are not authorized to perform this action');
            return redirect()->back();
        }

        $tutor_id = Thread::where('id', $thread_id)->first()->participants()->where('user_id', '!=', auth()->user()->id)->first()->user_id;

        $assigned_by = auth()->user()->id;
        $submitted_by = 'student';
        
        $assignment = $this->createAssignmentDB($tutor_request, $tutor_id, $assigned_by, $submitted_by);

        if ($assignment) {
            //Send Emails
            $this->dispatch(new CreateAssignment($assignment, $tutor_request, $tutor_id,  $submitted_by)); 

            \Session::flash('success', "Tutor Assignment Created! Your tutor's contact information is listed under My Account. We've also e-mailed you further information regarding payments and other policies.");  
        }
        
        else
        {
            \Session::flash('danger', "Oops, we were unable to create this assignment.  Please try again or notify us of this error.");  
        }


        return redirect()->back();
    }

    public function tutorAcceptedRequest($tutor_request_ref_id)
    {
        $tutor_request = MarketTutorRequest::where('ref_id', $tutor_request_ref_id)->first();
        
        if ($tutor_request->requested_tutor->id != auth()->user()->id) 
        {
            \Session::flash('error', 'You are not authorized to perform this action');
            return redirect()->back();
        }

        $tutor_id = $assigned_by = auth()->user()->id;
        
        $submitted_by = 'tutor';
        
        $assignment = $this->createAssignmentDB($tutor_request, $tutor_id, $assigned_by, $submitted_by);

        if ($assignment) {
            //Send Emails
            $this->dispatch(new CreateAssignment($assignment, $tutor_request, $tutor_id,  $submitted_by)); 

            \Session::flash('success', "Tutor Assignment Created. Please refer to the Tutor Checklist for the next steps.");  
        }
        
        else
        {
            \Session::flash('danger', "Oops, we were unable to create this assignment.  Please try again or notify us of this error.");  
        }

        return redirect()->back();
    }

    public function tutorDeclinedRequest($tutor_request_ref_id)
    {
        $tutor_request = MarketTutorRequest::where('ref_id', $tutor_request_ref_id)->first();

        if ($tutor_request->requested_tutor->id != auth()->user()->id) 
        {
            \Session::flash('error', 'You are not authorized to perform this action');
            return redirect()->back();
        }

        $tutor_request->requested_tutor_declined = 1;
        $tutor_request->save();

        $tutor = User::find(auth()->user()->id);

        //E-mail Student
        $user = User::find($tutor_request->student_id);
        $location = Location::find($tutor_request->location_id);
        $subject = "{$tutor->user_name} Declined Your Tutor Request For {$tutor_request->course}";
        $email_view = 'btn.emails.students.request_declined';
        $email_data = ['tutor_request' => $tutor_request];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('info', 'You have declined this tutor request.  If you have not done so already, so please send the student a message below and briefly explain why you cannot help out.');
        return redirect()->back();
    }
}
