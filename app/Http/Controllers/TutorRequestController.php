<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Location;
use App\MarketSubject;
use App\MarketNotification;
use App\MarketTutorRequest;
use App\Tutor;
use App\Student;
use App\User;
use App\Role;
use App\Profile;
use App\Favorite;
use App\SiteEmail;

use App\Jobs\SendWelcomeEmail;
use App\Jobs\EmailAdmin;
use App\Jobs\SendGeneralNotification;

use App\MsgThread as Thread;
use App\MsgMessage as Message;
use App\MsgParticipant as Participant;

/*use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;*/

use DB;
use Auth;
use Mail;
use Carbon;

class TutorRequestController extends Controller
{
    public function OLDgetRequestForm()
    {
        $course_level_options = [
            '' => 'Select Course Level',
            'College' => 'College/University',
            'Graduate_School' => 'Graduate School',
            'High_School' => 'High School',
            'Middle_School' => 'Middle School (Grades 6-8)',
            'Elementary_School' => 'Elementary School (Grades K-5)',
            'Other' => 'Other or Not Applicable'
        ];

        $session_times_options = [
            'Daytime' => 'Weekdays before 5pm',
            'Evenings' => 'Weekday Evenings (between 5-7pm)',
            'Nights' => 'Weeknights (after 7pm)',
            'Saturdays' => 'Saturdays',
            'Sundays' => 'Sundays'
        ];
        
        $locations = Location::studentsVisible()->has('active_tutors')->get();
        $locations_array = null;

        foreach ($locations as $location) {
            $state = $location->state ? ", $location->state" : null;
            $locations_array[$location->id] = "{$location->city}{$state}";

        }
        //$location_options = ["" => "Select Session Location"] + Location::studentsVisible()->has('active_tutors')->get()->pluck('city', 'id')->toArray();

        $location_options = ["" => "Select Session Location"] + $locations_array;

    	return view('btn.pages.request_tutor', compact('course_level_options', 'session_times_options', 'location_options'));
    }


    public function OLDsubmitRequestForm(Request $request)
    {
        $this->validate($request, [
            'location' => 'required',
            'level' => 'required',
            'course' => 'required',
            'session_time' => 'required',
            'frequency' => 'required',
            'max_rate' => 'required',
            ]);

        if (Auth::guest()) {
            $this->validate($request, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:20',
            'password' => 'required|min:6|regex:/^\S*$/|confirmed',
            'marketing' => 'required']);

            //Create User
            $role = Role::where('role', 'student')->first();

            $location = Location::where('id', $request->location)->first();

            $user = User::create([
                        'ref_id' => rand(10000000, 99999999),
                        'first_name' => ucwords(trim($request['first_name'])),
                        'last_name' => ucwords(trim($request['last_name'])),
                        'email' => trim($request['email']),
                        'phone' => trim($request['phone']),
                        'password' => bcrypt($request['password']),
                        'last_login_date' => Carbon\Carbon::now(),
                    ]);

            $profile = Profile::create([
                            'user_id' => $user->id,
                        ]);

            DB::table('terms_of_use')->insert([
                'user_id' => $user->id,
                'version' => 'terms_of_use',
                'submission_date' => Carbon\Carbon::now()
                ]);
            
            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => $role->id,
                ]);

            DB::table('location_user')->insert([
                'user_id' => $user->id,
                'location_id' => $location->id,
                ]);

            foreach ($request['marketing'] as $key => $value) {
                DB::table('marketing')->insert([
                        'user_id' => $user->id,
                        'role' => $role->role,
                        'method' => $value,
                        'created_at' => Carbon\Carbon::now(),
                        'updated_at' => Carbon\Carbon::now()
                    ]);
            }

            $email_view = 'btn.emails.students.welcome';
            
            $this->dispatch(new SendWelcomeEmail($user, $location, $email_view));

            //Login Student
            auth()->loginUsingId($user->id);
        }

        $student = Student::find(Auth::user()->id);
        
        $ref_id = mt_rand(10000000, 99999999);
        
        if (!$student->isStudent()) {
            $student->addRole('student');
        }
        
        $location = Location::where('id', $request['location'])->firstOrFail();

        $session_time = implode(', ', $request['session_time']);
        $requested_tutors = trim($request->requested_tutors) ?? null;

        $start_date = Carbon\Carbon::now()->addDays(2);
        $end_date =  DB::table('settings')->where('field', 'semester_end_date')->first()->value;

        $tutor_request = MarketTutorRequest::create(
            [
                'ref_id' => $ref_id,
                'mkt_visibility' => 0,
                'student_id' => $student->id,
                'location_id' => $location->id,
                'location' => $location->city,
                'start_date' => $start_date,
                'end_date' => $end_date,
                'level' => $request['level'],
                'course' => ucwords(strtolower(trim($request['course']))),
                'session_time' => $session_time,
                'max_rate' => $request['max_rate'],
                'frequency' => $request['frequency'],
                'concerns' => $request['concerns'],
                'sms_alert' => $request['sms_alert'],
                'requested_tutors' => $requested_tutors,
                'include_online_tutors' => $request['include_online_tutors'],
            ]
        );

        //Send Confirmation To Student
        $user = User::find($student->id);
        $subject = "Tutor Request Received: $tutor_request->course";
        $email_view = 'btn.emails.marketplace.general_tutor_request_confirmation';
        $email_data = ['tutor_request' => $tutor_request];

        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
    	
        //Email Admin
        $subject = "New $location->city Tutor Request For $tutor_request->course";
        $email_view = 'btn.emails.marketplace.general_request_notify_admin';
        $email_data = ['tutor_request' => $tutor_request];

        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

    	return view('btn.pages.request_tutor_confirmation', compact('tutor_request')); 

        // \Session::flash('success', "Thank you for requesting a tutor. We'll search our tutor network for a tutor who fits your needs. We've e-mailed you the next steps. The email was sent to: $student->email (be sure to check your Spam folder for the e-mail).");
		
		//return redirect()->back()->withInput($request->except('course', 'requested_tutors', 'concerns'));
    }

    public function deleteTutorRequest(Request $request, $tutor_request_id){
        $tutor_request = MarketTutorRequest::find($tutor_request_id);

        $tutor_request->delete();

        \Session::flash('success', "Tutor Request #$tutor_request->id for {$tutor_request->student->full_name} in $tutor_request->course Deleted.");

        return redirect()->back();
    }

    /*****CITY THEME*********/
    public function getRequestForm()
    {
        $course_level_options = [
            '' => 'Select Course Level',
            'College' => 'College/University',
            'Graduate_School' => 'Graduate School',
            'High_School' => 'High School',
            'Middle_School' => 'Middle School (Grades 6-8)',
            'Elementary_School' => 'Elementary School (Grades K-5)',
            'Other' => 'Other or Not Applicable'
        ];

        $session_times_options = [
            /*'' => 'Preferred Session Time',*/
            'Daytime' => 'Weekdays before 5pm',
            'Evenings' => 'Weekday Evenings (5-7pm)',
            'Nights' => 'Weeknights (after 7pm)',
            'Saturdays' => 'Saturdays',
            'Sundays' => 'Sundays'
        ];
        
        $rate_options = [
            '' => 'Select Max Affordable Rate',
            20 => '$20/hr',
            30 => '$30/hr',
            40 => '$40/hr',
            50 => '$50/hr',
            60 => '$60/hr',
            80 => '$80/hr',
            100 => '$100/hr',
            150 => '$150/hr',
            200 => '$200/hr',
            250 => '$250/hr',
            350 => '$350/hr',
            500 => '$500/hr',
        ];

        $user = auth()->user();

        if ($user) {
            $favorites = $user->favorites;
        }
        else{
            $favorites = Favorite::has('tutor')->where('identifier', session('identifier'))->get();
        }

        return view('btn.pages.citybook.request_tutor', compact('course_level_options', 'session_times_options', 'rate_options', 'favorites'));
    }

    public function submitRequestForm(Request $request)
    {
        if ($request['bot']) {
            \Session::flash('error', 'Suspicious activity.  Be sure to pass the bot test below');

            return redirect()->back()->withInput();
        }

        $this->validate($request, [
            'location' => 'required',
            // 'level' => 'required',
            'course' => 'required',
            'session_time' => 'required',
            'frequency' => 'required',
            'max_rate' => 'required'/*,
            'g-recaptcha-response' => 'required|recaptcha'*/
            ]);

        if (Auth::guest()) {
            //Preventing common bot
            if (strtolower(trim($request['first_name'])) == strtolower(trim($request['last_name']))) {
                $this->validate($request, [
                    'city' => 'required'
                ]);
            }
            
            $this->validate($request, [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:20',
            'password' => 'required|min:6|regex:/^\S*$/|confirmed',
            'marketing' => 'required']);

            //Create User
            //$role = Role::where('role', 'student')->first();

            $user = User::create([
                        'ref_id' => rand(10000000, 99999999),
                        'first_name' => ucwords(trim($request['first_name'])),
                        'last_name' => ucwords(trim($request['last_name'])),
                        'email' => trim($request['email']),
                        'phone' => trim($request['phone']),
                        'password' => bcrypt($request['password']),
                        'last_login_date' => Carbon\Carbon::now(),
                        'formatted_address' => $request->formatted_address != '' ? $request->formatted_address : $request->location ?? null,
                        'city' => $request->city ?? null,
                        'region' => $request->region ?? null,
                        'country' => $request->country ?? null,
                        'postal_code' => $request->postal_code ?? null,
                        'lat' => $request->lat ?? null,
                        'lng' => $request->lng ?? null
                    ]);

            $user->addRole('student');

            if ($user->lat) {
                $location = $user->nearest_btn_location();
            }
            else{
                $location = Location::where('city', 'Other')->first();
            }
            
            $profile = Profile::create([
                            'user_id' => $user->id,
                            'marketing' => $request->marketing,
                            'school' => $request->school
                        ]);

            DB::table('terms_of_use')->insert([
                'user_id' => $user->id,
                'version' => 'terms_of_use',
                'submission_date' => Carbon\Carbon::now()
                ]);

            DB::table('location_user')->insert([
                'user_id' => $user->id,
                'location_id' => $location->id,
                ]);

            //$email_view = 'btn.emails.students.welcome';
            
            //$this->dispatch(new SendWelcomeEmail($user, $location, $email_view));

            //Login Student
            auth()->loginUsingId($user->id);
        }

        $student = Student::find(Auth::user()->id);
        
        $ref_id = mt_rand(10000000, 99999999);
        
        if (!$student->isStudent()) {
            $student->addRole('student');
        }
        
        $location = $student->locations->first() ?? $student->nearest_btn_location();

        $session_time = implode(', ', array_filter($request['session_time']));
        
        $requested_tutors = $request->requested_tutors ? implode(', ', $request['requested_tutors']): null;

        $start_date = Carbon\Carbon::now()->addDays(2);
        $end_date =  DB::table('settings')->where('field', 'semester_end_date')->first()->value;

        $tutor_request = MarketTutorRequest::create(
            [
                'ref_id' => $ref_id,
                'mkt_visibility' => 0,
                'student_id' => $student->id,
                'location_id' => $location->id,
                'location' => $location->city,
                'start_date' => $start_date,
                'end_date' => $end_date,
                // 'level' => $request['level'],
                'level' => "-",
                'school' => $request['school'],
                'course' => ucwords(strtolower(trim($request['course']))),
                'session_time' => $session_time,
                'max_rate' => $request['max_rate'],
                'frequency' => $request['frequency'],
                'concerns' => $request['concerns'],
                'sms_alert' => $request['sms_alert'],
                'requested_tutors' => $requested_tutors,
                'include_online_tutors' => $request['include_online_tutors'],
                'formatted_address' => $request->formatted_address != '' ? $request->formatted_address : $request->location ?? null,
                'city' => $request->city ?? null,
                'region' => $request->region ?? null,
                'country' => $request->country ?? null,
                'postal_code' => $request->postal_code ?? null,
                'lat' => $request->lat ?? null,
                'lng' => $request->lng ?? null
            ]
        );

        //Send Confirmation To Student
        $user = User::find($student->id);
        $subject = "Tutor Request Received: $tutor_request->course";
        $email_view = 'btn.emails.marketplace.general_tutor_request_confirmation';
        $email_data = ['tutor_request' => $tutor_request];

        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
        
        //Email Admin
        $subject = "New $location->city Tutor Request For $tutor_request->course";
        $email_view = 'btn.emails.marketplace.general_request_notify_admin';
        $email_data = ['tutor_request' => $tutor_request];

        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

        \Session::flash('success', "We'll e-mail you when we've found a tutor. Track the progress of your tutor request under the Requests tab.");

        return redirect()->route('profile.account', $user->ref_id)->with('requests_tab', 'visible');
    }

    public function duplicateTutorRequest(Request $request, $tutor_request_id){
        //Only Admin
        $tutor_request = MarketTutorRequest::find($tutor_request_id);

        $new_request = $tutor_request->replicate();
        $new_request->ref_id = mt_rand(10000000, 99999999);
        $new_request->course = $request->new_course;
        $new_request->save();

        $tutor_request->course = $request->edited_course;
        $tutor_request->save();

        \Session::flash('success', 'New Request Created.');
        return redirect()->back();
    }

    public function checkAvailablility(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|max:100',
            ]);

        if ($request->check == "" && trim($request->course) != "" && trim($request->location != "")) {
            $name = trim($request->full_name);
            $name_array = explode(' ', $name);
            $first_name = $name_array[0];
            $last_name = $name_array[1] ?? null;
            $email = trim($request->email);
            $course = trim($request->course);
            $location = trim($request->location);

            $user_id = auth()->user() ? auth()->user()->id : null;
            $identifier = session('identifier');

            $message = "Availability inquiry: $course in $location";

            $site_email = SiteEmail::create([
                            'user_id' => $user_id,
                            'identifier' => $identifier,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'email' => $email,
                            'message' => $message]);

            //Email Admin
            $location = Location::first();
            $subject = "New Availability Inquiry";
            $email_view = 'btn.emails.admin.site_email';
            $email_data = ['site_email' => $site_email];

            $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));
        }
        
        $confirmation_message = "Thank you for submitting this inquiry.  We'll get back to you within 1-2 days.";

        return view('btn.pages.citybook.confirmation_page', compact('confirmation_message'));
    }
}
