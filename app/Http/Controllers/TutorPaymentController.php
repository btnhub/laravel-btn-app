<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon;

use App\Tutor;
use App\Location;
use App\Assignment;
use App\TutorSession;
use App\ContractorPayment;
use App\StripeConnectTutor;

class TutorPaymentController extends Controller
{
    public function getNextPay()
    {
        $assignment_ids = Assignment::recent()->pluck('id');

        $tutor_ids = TutorSession::whereIN('assignment_id', $assignment_ids)
                                ->where('tutor_payment_date', null)
                                ->pluck('tutor_id')
                                ->unique();
        $tutors = Tutor::with('locations')->whereIN('id', $tutor_ids)->get();
        $locations = Location::pluck('city', 'id');

        return view('btn.admin.tutor_payment_next', compact('tutors', 'locations'));

    }

    public function submitPayment(Request $request, $tutor_id)
    {
        $tutor = Tutor::find($tutor_id);
        $withhold_reason = null;
        $eligible_sessions_ids = $request->eligible_sessions_ids; //string delimited by ','

        if($request->amount_withheld > 0 && trim($request->withhold_reason) != '')
        {
            $withhold_reason = $request->withhold_reason;
        }
        elseif($request->amount_withheld > 0)
        {
            $withhold_reason = "Unpaid Sessions";
        }

        $payment = new ContractorPayment([
                    'contractor_id' => $tutor->id,
                    'first_name' => $tutor->first_name,
                    'last_name' => $tutor->last_name,
                    'amount_paid' => $request->amount_paid,
                    'amount_withheld' => $request->amount_withheld,
                    'method' => $request->method,
                    'withhold_reason' => $withhold_reason
            ]);
        
        
        if ($request->method == "Stripe") {
            //Transfer money to tutor from BTN Stripe Account

            //Get Tutor's Stripe Account
            if(!$stripe_user = StripeConnectTutor::where('user_id', $tutor_id)->first())
            {
                \Session::flash('error', "$tutor->full_name is not connected to our Platform."); 
                return redirect()->back();
            }

            $destination = $stripe_user->stripe_user_id;     

            $tutor_amount = number_format($request->amount_paid, 2);

            $amount_stripe = $tutor_amount * 100; //amount in cents

            $payment_method = 1; //1 = stripe

            $description = "Payment To $tutor->fullName From Student Account Balance";

            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

            try {
                //Check Stripe Balance and make sure it's enough to pay tutor
                $balance = \Stripe\Balance::retrieve();

                $available_balance = $balance->available[0]['amount'];

                if($available_balance < $amount_stripe) {
                    \Session::flash('error', "Insufficient Funds in BuffTutor Stripe Account");
                    return redirect()->back();
                }

                else {
                    try {
                        //Transfer Money
                        $transfer = \Stripe\Transfer::create(
                            [
                              "amount" => $amount_stripe, // amount in cents, again
                              "currency" => "usd",
                              "description" => $description,
                              "destination" => $destination,
                            ]
                        );

                    } catch (\Stripe\Error\ApiConnection $e) {
                       // Network problem, perhaps try again.
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];

                        \Session::flash('error', $error['message']);
                        return redirect()->back();

                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // You screwed up in your programming. Shouldn't happen!
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();

                    } catch (\Stripe\Error\Api $e) {
                        // Stripe's servers are down!
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();

                    } catch (\Stripe\Error\Card $e) {
                        // Card was declined.
                        $e_json = $e->getJsonBody();
                        $error = $e_json['error'];
                        
                        \Session::flash('error', $error['message']);
                        return redirect()->back();
                    }
                } // else

            } catch (\Stripe\Error\ApiConnection $e) {
               // Network problem, perhaps try again.
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];

                \Session::flash('error', $error['message']);
                return redirect()->back();

            } catch (\Stripe\Error\InvalidRequest $e) {
                // You screwed up in your programming. Shouldn't happen!
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();

            } catch (\Stripe\Error\Api $e) {
                // Stripe's servers are down!
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();

            } catch (\Stripe\Error\Card $e) {
                // Card was declined.
                $e_json = $e->getJsonBody();
                $error = $e_json['error'];
                
                \Session::flash('error', $error['message']);
                return redirect()->back();
            }

            $payment->admin_notes = $transfer->id;            
        }

        if ($payment->save()) {
            //Grab All Eligible Payments & update payment date
            $eligible_sessions = TutorSession::whereIN('id', explode(',', $eligible_sessions_ids))->get();
            
            foreach ($eligible_sessions as $eligible_session) {
                $eligible_session->tutor_payment_date = Carbon\Carbon::now()->format('Y-m-d');
                $eligible_session->payment_method = 'stripe-transfer';
                $eligible_session->stripe_transfer_id = $transfer->id;

                $eligible_session->save();
            }

            \Session::flash('success', "$tutor->full_name Paid & Payment & Session tables successfully updated! (Session IDs: $eligible_sessions_ids)");
        }
        
        else
        {
            \Session::flash('error', 'Contractor Payments & Tutor Sessions Tables were not updated with this payment!');  
        }

        return redirect()->back();
    }

}
