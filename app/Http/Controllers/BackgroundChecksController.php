<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon;
use DB;

use App\User;
use App\Profile;
use App\BackgroundCheck;

use App\Jobs\SendGeneralNotification;

class BackgroundChecksController extends Controller
{

	public function backgroundPayment() {
		//TODO: conditional to allow only tutors or tutor applicants
		return view('btn.background_check.background_payment_form');
	}

	public function processPayment(){

		$user = User::find(auth()->user()->id);	

		$description = "BuffTutor Background Check For $user->full_name";
		$amount = DB::table('settings')->where('field', 'background_check_fee')->first()->value; //price to run background check

		\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

		// Get the credit card details submitted by the form
		$token = $_POST['stripeToken'];

		try {
			$charge = \Stripe\Charge::create(array(
			    "amount" => $amount * 100, // amount in cents
			    "currency" => "usd",
			    "source" => $token,
			    "description" => $description,
			    "receipt_email" => $user->email
			    ));

			if ($charge) {

				//Store In Database
			    $check = BackgroundCheck::create([
				    	'user_id' => $user->id,
				    	'first_name' => $user->first_name,
				    	'last_name' => $user->last_name,
				    	'payment_date' => date("Y-m-d H:i:s", $charge->created),
				    	'payment_id' => $charge->id,
				    	'paid' => $charge->paid
			    	]);
		  	}

		  	\Session::flash('success', "Click the link in the Background tab to submit the background check.");
		  	\Session::flash('background_tab', 'visible');
			//return redirect()->route('show.checkr.url');
			return redirect()->back();

		} catch (\Stripe\Error\ApiConnection $e) {
 		   // Network problem, perhaps try again.
			$e_json = $e->getJsonBody();
	    	$error = $e_json['error'];

			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\InvalidRequest $e) {
    		// You screwed up in your programming. Shouldn't happen!
    		$e_json = $e->getJsonBody();
	    	$error = $e_json['error'];

			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\Api $e) {
    		// Stripe's servers are down!
    		$e_json = $e->getJsonBody();
	    	$error = $e_json['error'];

			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\Card $e) {
    		// Card was declined.
    		$e_json = $e->getJsonBody();
	    	$error = $e_json['error'];

			\Session::flash('error', $error['message']);
			return redirect()->back();
		}
	}

	public function showCheckrURL() {		
		$user = auth()->user();

		if ($user->backgroundCheck) 
		{
			if ($user->backgroundCheck->paid && $user->backgroundCheck->cleared == 0) {
				return view('btn.background_check.background_url');
			}
			else 
			{
				\Session::flash('error', "You've already passed the background check");
				return redirect()->back();
			}
		}
		
		\Session::flash('error', "Unauthorized. Refer to the Tutor Checklist for instructions on how to submit a background check.");
		return redirect()->back();
	}

	public function clearApplicant($user_id){
		//When background check comes back clean
		$user = User::find($user_id);
		$user->backgroundCheck->cleared_date = Carbon\Carbon::now();
		$user->backgroundCheck->cleared = 1;
		$user->backgroundCheck->save();

		$user->profile->background_check = Carbon\Carbon::now();
		$user->profile->save();

		//E-mail Tutor
        $location = $user->locations->first();
        $subject = "Background Check Passed";
        $email_view = 'btn.emails.tutors.background_passed';
        $email_data = ['user' => $user];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

		\Session::flash('success', "User Background Check Updated To Cleared, Tutor Notified Of Change.");
		return redirect()->back();
	}	

	public function adverseAction(){

	}
}
