<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\TutorSession;
use App\Assignment;
use App\Profile;
use App\Customer;
use App\Payment;
use App\StripeConnectTutor;
use App\Refund;
use App\User;
use Auth;
use DB;

use Carbon;

use App\Jobs\EmailAdmin;
use App\Jobs\SendGeneralNotification;

class TutorSessionsController extends Controller
{
	
	/*public function getChargeForm($assign_ref_id)
	{
		$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
		$this->authorize('tutor-access', $assignment);

		$tutor_fee = Profile::where('user_id', $assignment->tutor->id)->firstOrFail()->tutor_fee;
		$discount_percent = 0.10;

		//Check if student has already met with any tutor 
		if(TutorSession::where('student_id', $assignment->student->id)->exists())
		{
			$discount_percent = 0;
		}

		return view('btn.forms.session_charge', compact('assignment', 'discount_percent', 'tutor_fee'));
	}

	public function chargeTutorSession(Request $request, $assign_ref_id)
	{
		$this->validate($request, [
			'amt_charged' => 'required|min:0',
			'outcome' => 'required',
			'tutor_pay' => 'required|min:0',
		]);

		$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
		$this->authorize('tutor-access', $assignment);

		$input = $request->all();

		$input['ref_id'] = mt_rand(10000000, 99999999);
		$input['assignment_id'] = $assignment->id;
		$input['student_id'] = $assignment->student->id;
		$input['tutor_id'] = $assignment->tutor->id;
		$input['duration'] = $input['duration'] / 60; //convert to hours

		$flash_type = 'success';
		$flash_msg = 'Successfully charged student';
		
		if(!$tutor_session = TutorSession::create($input))
		{
			$flash_type = 'error';
			$flash_msg = 'Could not charge student! Please try again.';
		}

		//E-mail Student
        $user = User::find($tutor_session->student_id);
        $location = $tutor_session->assignment->location;
        $subject = "Tutor Session Charged";
        $email_view = 'btn.emails.students.session_charged';
        $email_data = ['tutor_session' => $tutor_session];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

		\Session::flash($flash_type, $flash_msg);

		return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
	}*/		

	public function chargeAccountBalance(Assignment $assignment, $amt_charged, $discount){
		//Get Tutor's Stripe Account
		if(!$stripe_user = StripeConnectTutor::where('user_id', auth()->user()->id)->first())
		{
			return (object)['error' => 'You are not connected to our Platform.  We cannot forward this payment to your bank.'];
		}
		
		$tutor_rate  = $assignment->tutorContract->tutor_rate;
		$student_rate  = $assignment->tutorContract->student_rate;
		$prepaid_charge = 0;
		$duration_charged = 0;
		$tutor_pay = 0;
		$btn_pay = 0;

		$prepaid_balance = $assignment->prepaid_balance();

		if ($prepaid_balance > 0) {
			$prepaid_charge = $prepaid_balance > $amt_charged ? $amt_charged : $prepaid_balance;
			$duration_charged = number_format(($prepaid_charge + $discount) / $student_rate, 2); //hrs
		}

		$tutor_pay = number_format($duration_charged * $tutor_rate, 2);
		$btn_pay = $prepaid_charge - $tutor_pay;

		//Create Transfer to tutor's account
		$stripe_user_id = $stripe_user->stripe_user_id;		
		$amount_stripe = $tutor_pay * 100; //amount in cents
		$destination = $stripe_user_id;
		$payment_method = 1; //1 = stripe

		$description = "BuffTutor Session: {$assignment->tutor->full_name} & {$assignment->student->full_name}";

		\Stripe\Stripe::setApiKey(config('services.stripe.secret'));
		
		
		try {
			//Check Stripe Balance and make sure it's enough to pay tutor
			if ($assignment->tutor_id != 63) {
				$balance = \Stripe\Balance::retrieve();

				$available_balance = $balance->available[0]['amount'];
				
				if($available_balance < $amount_stripe) {

					//E-mail Admin
					$submitted_by = auth()->user()->full_name;

			        $location = $assignment->student->locations->first();
			        $subject = "Prepay Transfer Failed: Insufficient Funds";
			        $email_view = 'btn.emails.admin.prepaid_transfer_failed';
			        $email_data = ['assignment' => $assignment, 'tutor_pay' => $tutor_pay, 'available_balance' => ($available_balance / 100), 'submitted_by' => $submitted_by];
			        
			        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

					//return redirect()->back();
					return (object)['error' => "We are unable to submit this charge at this moment.  We will contact you and inform you when you can submit this charge.  We apologize for the inconvenience and delay in your payment."];
				}
			}

			
			try {
				$stripe_transfer_id = null;
				// Prevent Transfer if charge is for tutor 63 
				if ($assignment->tutor->id != 63) {
					//Transfer Money
					$transfer = \Stripe\Transfer::create(
						[
						  "amount" => $amount_stripe, // amount in cents, again
						  "currency" => "usd",
						  "description" => $description,
						  "destination" => $destination,
				  		]
					);

					$stripe_transfer_id = $transfer->id;
				}
				
				//Create Tutor Sessions charge for database
				$tutor_session = TutorSession::create([
										'ref_id' => mt_rand(10000000, 99999999),
										'assignment_id' => $assignment->id,
										'student_id' => $assignment->student_id,
										'tutor_id' => $assignment->tutor_id,
										'duration' => $duration_charged,
										'amt_charged' => $prepaid_charge,
										'tutor_pay' => $tutor_pay,
										'btn_pay' => $btn_pay,
										'tutor_payment_date' => Carbon\Carbon::now(),
										'payment_method' => "stripe-connect",
										'stripe_transfer_id' => $stripe_transfer_id]);

				// Prevent database update if charge is for tutor 63 
				if ($assignment->tutor->id != 63) {	
					//Update Stripe App Fees Table
					DB::table('stripe_app_fees')->insert([
							'session_id' => $tutor_session->id,
							'student_id' => $assignment->student_id,
							'stripe_customer_id' => $assignment->student->customer ? $assignment->student->customer->stripe_customer_id : "-",
							'tutor_id' => $assignment->tutor_id,
							'stripe_tutor_id' => $destination,
							'application_fee' => $btn_pay,
							'stripe_fee' => 0, //No fee for transfer
							'created_at' =>Carbon\Carbon::now(),
							'updated_at' =>Carbon\Carbon::now()
			    		]);
				}

				return (object)['prepaid_charge' => $prepaid_charge,
						'tutor_session' => $tutor_session ?? null,
						'prepaid_tutor_pay' => $tutor_pay];

			} catch (\Stripe\Error\ApiConnection $e) {
	 		   // Network problem, perhaps try again.
				$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];

				return (object)['error' => $error['message']];

			} catch (\Stripe\Error\InvalidRequest $e) {
	    		// You screwed up in your programming. Shouldn't happen!
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				return (object)['error' => $error['message']];

			} catch (\Stripe\Error\Api $e) {
	    		// Stripe's servers are down!
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				return (object)['error' => $error['message']];

			} catch (\Stripe\Error\Card $e) {
	    		// Card was declined.
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				return (object)['error' => $error['message']];
			}
			

		} catch (\Stripe\Error\ApiConnection $e) {
 		   // Network problem, perhaps try again.
			$e_json = $e->getJsonBody();
    		$error = $e_json['error'];

			return (object)['error' => $error['message']];

		} catch (\Stripe\Error\InvalidRequest $e) {
    		// You screwed up in your programming. Shouldn't happen!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			return (object)['error' => $error['message']];

		} catch (\Stripe\Error\Api $e) {
    		// Stripe's servers are down!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			return (object)['error' => $error['message']];

		} catch (\Stripe\Error\Card $e) {
    		// Card was declined.
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			
			return (object)['error' => $error['message']];
		}
	}

	public function deleteTutorSession($id)
	{	
		$tutor_session = TutorSession::findOrFail($id);
		$this->authorize('tutor-access', $tutor_session);

		$stripe_refund = $stripe_transfer_reversal = null;

		// if ($charge_id = $tutor_session->stripe_payment_id)
		if ($tutor_session->stripe_payment_id || $tutor_session->stripe_transfer_id)
		{
			$btn_refund = Refund::firstOrNew([
					// 'stripe_refund_id' => $stripe_refund->id,
					'student_id' => $tutor_session->student_id,
					'tutor_id' => $tutor_session->tutor_id,
					'session_id' => $tutor_session->id,
					'assignment_id' => $tutor_session->assignment_id,
					'requested_amount' => $tutor_session->amt_charged,
					'refund_reason' => "Delete Charge", //DO NOT use word "session" in reason
					'refund_method' => "Stripe",
					'issue_refund' => 1,
					// 'refunded_to_student' => $tutor_session->amt_charged,
					'refunded_from_tutor' => $tutor_session->tutor_pay,
					'refunded_from_btn' => $tutor_session->btn_pay,
					'processed_date' => Carbon\Carbon::now()
				]);
			$btn_refund->ref_id = mt_rand(10000000, 99999999);

			\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

			if ($charge_id = $tutor_session->stripe_payment_id){
				$reverse_transfer = $tutor_session->tutor_id != 63 ? true : false;

				try {
					$stripe_refund = \Stripe\Refund::create([
		  				"charge" => $charge_id,
		  				"reverse_transfer" => $reverse_transfer,
		  				"refund_application_fee" => $reverse_transfer
						]);

					$btn_refund->stripe_refund_id = $stripe_refund->id;
					$btn_refund->refunded_to_student = $tutor_session->amt_charged;
					$btn_refund->save();

				} catch (\Stripe\Error\ApiConnection $e) {
		 		   // Network problem, perhaps try again.
					$e_json = $e->getJsonBody();
		    		$error = $e_json['error'];

					\Session::flash('error', $error['message']);
					return redirect()->back();

				} catch (\Stripe\Error\InvalidRequest $e) {
		    		// You screwed up in your programming. Shouldn't happen!
		    		$e_json = $e->getJsonBody();
		    		$error = $e_json['error'];
					
					\Session::flash('error', $error['message']);
					return redirect()->back();

				} catch (\Stripe\Error\Api $e) {
		    		// Stripe's servers are down!
		    		$e_json = $e->getJsonBody();
		    		$error = $e_json['error'];
					
					\Session::flash('error', $error['message']);
					return redirect()->back();
				} 
			}

			else{
				if ($tutor_session->stripe_transfer_id) {
					try {
						$stripe_transfer = \Stripe\Transfer::retrieve($tutor_session->stripe_transfer_id);
						$stripe_transfer->reverse();
						$stripe_transfer_reversal = \Stripe\Transfer::retrieve($tutor_session->stripe_transfer_id); //re-retrieve transfer to get reversal data newly attached to object

						$btn_refund->stripe_refund_id = $stripe_transfer_reversal->reversals->data[0]->id;
						$btn_refund->refunded_to_account = $tutor_session->amt_charged;
						$btn_refund->save();

					} catch (\Stripe\Error\ApiConnection $e) {
			 		   // Network problem, perhaps try again.
						$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];

						\Session::flash('error', $error['message']);
						return redirect()->back();

					} catch (\Stripe\Error\InvalidRequest $e) {
			    		// You screwed up in your programming. Shouldn't happen!
			    		$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];
						
						\Session::flash('error', $error['message']);
						return redirect()->back();

					} catch (\Stripe\Error\Api $e) {
			    		// Stripe's servers are down!
			    		$e_json = $e->getJsonBody();
			    		$error = $e_json['error'];
						
						\Session::flash('error', $error['message']);
						return redirect()->back();
					}
				}
			}

			/*$btn_refund = Refund::create([
					'ref_id' => mt_rand(10000000, 99999999),
					'stripe_refund_id' => $stripe_refund->id,
					'student_id' => $tutor_session->student_id,
					'tutor_id' => $tutor_session->tutor_id,
					'session_id' => $tutor_session->id,
					'assignment_id' => $tutor_session->assignment_id,
					'requested_amount' => $tutor_session->amt_charged,
					'refund_reason' => "Delete Stripe Charge",
					'refund_method' => "Stripe",
					'issue_refund' => 1,
					'refunded_to_student' => $tutor_session->amt_charged,
					'refunded_from_tutor' => $tutor_session->tutor_pay,
					'refunded_from_btn' => $tutor_session->btn_pay,
					'processed_date' => Carbon\Carbon::now()
				]);*/
			if ($tutor_session->tutor_id != 63) {
				DB::table('stripe_app_fees')
						->where('session_id', $tutor_session->id)
						->update([
							'refund_id' => $btn_refund->id,
							'stripe_refund_id' => $stripe_refund ? $stripe_refund->id : $stripe_transfer_reversal->id,
							'app_refund_amount' => $tutor_session->btn_pay,
							'refund_date' => Carbon\Carbon::now(),
							'deleted_at' => Carbon\Carbon::now()
							]);
				$tutor_session->refund_id = $btn_refund->id;
				$tutor_session->deleted_by = auth()->user()->id;
	    		$tutor_session->update();
    		}

		}
    	
        $tutor_session->delete();

        //Create Student Email
        $user = User::find($tutor_session->student_id);
        $location = $tutor_session->assignment->location;
        $subject = "Tutor Session Deleted";
        $email_view = 'btn.emails.students.session_deleted';
        $email_data = ['tutor_session' => $tutor_session];
        
        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('success', 'Session successfully deleted!');
        return redirect()->back();
	}

	public function getCustomerChargeForm($assign_ref_id)
	{
		$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
		$this->authorize('tutor-access', $assignment);

		$session_duration_array = ['' => "Select How Long The Session Lasted",
									30 => "30 min",
									45 => "45 min",
									60 => "1 hour",
									75 => "1 hour & 15 min",
									90 => "1 hour & 30 min",
									105 => "1 hour & 45 min",
									120 => "2 hours",
									135 => "2 hours & 15 min",
									150 => "2 hours & 30 min",
									165 => "2 hours & 45 min",
									180 => "3 hours"						
									];

		return view('btn.forms.session_charge_connect', compact('assignment', 'session_duration_array'));
	}

	public function chargeCustomerForSession(Request $request, $assign_ref_id)
	{	
		//Charge student's Stripe Customer account
		$assignment = Assignment::where('ref_id', $assign_ref_id)->first();
		$this->authorize('tutor-access', $assignment);
		
		$this->validate($request,[
			'outcome' => 'required',
			/*'tutor_pay' => 'required|min:0',*/
			]);
		
		$input = $request->all();
		$student_rate = $assignment->tutorContract->student_rate;
		$billing_increment = 60; //All rates are hourly = 60 min
		$skip_session_entry = false;

		if ($request->outcome == 'Free Session' || $request->outcome == 'Tutor Cancelled Without 24 Hours Notice') {
			$amt_charged = 0;
			$btn_pay = 0;
			$tutor_pay = 0;
			$discount_percent = 0;
			$discount = 0;
		}
		
		else
		{
			if(TutorSession::where('student_id', $assignment->student->id)->exists())
			{
				$discount_percent = 0;
				$discount = 0;
			}
			else
			{
				$discount_percent = (float) DB::table('settings')->where('field', 'first_session_discount')->first()->value;	

				if ($request->duration < $billing_increment) {
					$discount = $discount_percent * $student_rate * ($request->duration / $billing_increment);

				}
				else	
				{
					$discount = $discount_percent * $student_rate;
				}
			}
						
			$tutor_pay = $assignment->tutorContract->tutor_rate * ($request->duration / $billing_increment);

			$amt_charged = $student_rate * ($request->duration / $billing_increment) - $discount;

			if ($assignment->prepaid_balance() > 0) {
				$prepaid = $this->chargeAccountBalance($assignment, $amt_charged, $discount);
				
				if ($prepaid->error ?? false) {
					\Session::flash('error', $prepaid->error);
					return redirect()->back();
				}

				//Update Prepaid Tutor Session With Details
				$prepaid_tutor_session = $prepaid->tutor_session;
				$duration_charged = $prepaid_tutor_session->duration; //hrs

				$prepaid_tutor_session->session_date = $input['session_date'];
				$prepaid_tutor_session->discount = $discount;
				$prepaid_tutor_session->outcome = $request->outcome;
				$prepaid_tutor_session->save();


				//E-mail Student
		        $user = User::find($prepaid_tutor_session->student_id);
		        $location = $assignment->location;
		        $subject = "Tutor Session Charged To Prepaid Balance";
		        $email_view = 'btn.emails.students.session_charged';
		        $email_data = ['tutor_session' => $prepaid_tutor_session];
		        
		        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

				//Calculate Remaining Balance
				$amt_charged -= $prepaid->prepaid_charge;
				$amt_charged = number_format($amt_charged, 2);
				$tutor_pay -= $prepaid->prepaid_tutor_pay;
				$input['duration'] -= ($duration_charged * 60); //min

				$discount = 0; //Reset to $0, discount was applied to prepaid_balance

				if ($amt_charged <= 0) {
					$skip_session_entry = true; //prevent 2nd TutorSession entry with an amount of $0
					$tutor_session = $prepaid_tutor_session;
				}
				
			}

			$btn_pay = number_format($amt_charged - $tutor_pay,2);
		}

		$charge = null;

		if ($amt_charged > 0) {
			//Charge Student's stored payment info
			$customer = Customer::where('user_id', $assignment->student_id)->first();
			if(!$customer)
			{
				\Session::flash('error', 'Student has not added a Credit Card to his/her BuffTutor Account');	
				return redirect()->back();
			}

			//Get Tutor's Stripe Account
			if(!$stripe_user = StripeConnectTutor::where('user_id', auth()->user()->id)->first())
			{
				\Session::flash('error', 'You are not connected to our Platform.  We cannot forward this payment to your bank. Refer to the Tutor Checklist for instructions on how to connect to our Stripe Platform.');	
				return redirect()->back();
			}

			$stripe_user_id = $stripe_user->stripe_user_id;		

			$amount = number_format($amt_charged, 2);

			$amount_stripe = $amount * 100; //amount in cents
			$btn_pay_stripe = $btn_pay * 100; //in cents

			$destination = $stripe_user_id;
			$payment_method = 1; //1 = stripe

			// Prevent transfer if charge is for tutor 63 
			if ($assignment->tutor->id == 63) {
				$destination = null;
				$btn_pay_stripe = null;
			}

			$description = "BuffTutor Session: {$assignment->tutor->full_name} & {$assignment->student->full_name}";
			// REF: https://stripe.com/docs/charges 06/26/2016

			\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

			try {
				//Charge Customer
				$charge = \Stripe\Charge::create(
					[
					  "amount" => $amount_stripe, // amount in cents, again
					  "currency" => "usd",
					  "customer" => $customer->stripe_customer_id,
					  "description" => $description,
					  "destination" => $destination,
					  "application_fee" => $btn_pay_stripe
			  		]
				);

				if ($charge) {
					$payment = Payment::create([
						"user_id" => $customer->user_id,
						"transaction_id" => $charge->id,
						"stripe_customer_id" => $customer->stripe_customer_id,
						"amount" => $amount,
						"total_amount" => $amount,
						"payment_method" => $payment_method,
						"captured" => $charge->captured,
					    "paid" => $charge->paid,
					    "destination_tutor" => $stripe_user->user_id,
					    "destination_stripe_id" => $charge->destination,
					    "stripe_transfer_id" => $charge->transfer,
					    "application_fee" => $btn_pay,
					    "completed_at" => Carbon\Carbon::now()
					]);

					$input['stripe_payment_id'] = $charge->id;
					$input['stripe_transfer_id'] = $charge->transfer;

				}

			} catch (\Stripe\Error\ApiConnection $e) {
	 		   // Network problem, perhaps try again.
				$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];

				\Session::flash('error', $error['message']);
				return redirect()->back();

			} catch (\Stripe\Error\InvalidRequest $e) {
	    		// You screwed up in your programming. Shouldn't happen!
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				\Session::flash('error', $error['message']);
				return redirect()->back();

			} catch (\Stripe\Error\Api $e) {
	    		// Stripe's servers are down!
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				
				\Session::flash('error', $error['message']);
				return redirect()->back();

			} catch (\Stripe\Error\Card $e) {
	    		// Card was declined.
	    		$e_json = $e->getJsonBody();
	    		$error = $e_json['error'];
				

				$user = User::find($assignment->student_id);
		        $location = $assignment->location;
		        $subject = "Credit/Debit Card Declined";
		        $email_view = 'btn.emails.students.card_declined';
		        $email_data = ['tutor' => $stripe_user->user, 'customer' => $customer, 'error_message' => $error['message']];	    		
				$this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
				
				\Session::flash('error', $error['message']."  We've e-mailed $user->first_name this error message.");

				return redirect()->back();
			}
		}

		if (!$skip_session_entry) {
			//Create Tutor Sessions charge for database
			$input['ref_id'] = mt_rand(10000000, 99999999);
			$input['assignment_id'] = $assignment->id;
			$input['student_id'] = $assignment->student->id;
			$input['tutor_id'] = $assignment->tutor->id;
			$input['duration'] = $input['duration'] / 60; //convert to hours
			$input['tutor_payment_date'] = Carbon\Carbon::now();
			$input['payment_method'] = "stripe-connect";
			$input['amt_charged'] = $amt_charged;
			$input['discount'] = $discount;
			$input['tutor_pay'] = $tutor_pay;
			$input['btn_pay'] = $btn_pay;

			if(!$tutor_session = TutorSession::create($input))
			{
				\Session::flash('error', "We've charged your student, but could not save the session information to our database! Please e-mail us the student's name, session date and duration and we'll manually create an entry.");	
				return redirect()->back();
			}

			// Prevent database update if charge is for tutor 63 
			if ($amt_charged > 0 && $assignment->tutor->id != 63) {
				//Update Stripe App Fees Table
				$stripe_transaction = \Stripe\BalanceTransaction::retrieve($charge->balance_transaction);

				DB::table('stripe_app_fees')->insert([
						'session_id' => $tutor_session->id,
						'student_id' => $customer->user_id,
						'stripe_customer_id' => $charge->customer,
						'tutor_id' => $stripe_user->user_id,
						'stripe_tutor_id' => $charge->destination,
						'application_fee' => $btn_pay,
						'stripe_fee' => $stripe_transaction->fee / 100,
						'created_at' =>Carbon\Carbon::now(),
						'updated_at' =>Carbon\Carbon::now()
		    		]);
			}
			
			//E-mail Student
	        $user = User::find($tutor_session->student_id);
	        $location = $tutor_session->assignment->location;
	        $subject = "Tutor Session Charged";
	        $email_view = 'btn.emails.students.session_charged';
	        $email_data = ['tutor_session' => $tutor_session];
	        
	        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
		}

		$flash_type = 'success';
		$flash_msg = "Successfully charged {$tutor_session->student->first_name}'s account and you just got paid!  Check your Stripe account.";

		\Session::flash($flash_type, $flash_msg);

		return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
		
	}
}
