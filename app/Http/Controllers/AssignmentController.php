<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

//BTN Created
use App\User;
use App\Tutor;
use App\Assignment;
use App\Location;
use App\Http\Requests\AssignmentRequest;
use App\MarketTutorRequest;
use DB;
use Carbon;

use App\Jobs\CreateAssignment;
use App\Jobs\SendGeneralNotification;

class AssignmentController extends Controller
{    
    /**
     * Show all items
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('admin-only');
        //Lists Active Assignments  For All Locations
        $assignments = Assignment::active()
                        ->has('tutorContract')
                        ->latest('id')->get();

        $locations = Location::has('active_assignments')
                                ->withCount('active_assignments')
                                ->orderBy('active_assignments_count', 'desc')
                                ->get();
        $loc_without_assign = Location::has('active_tutors')
                                            ->whereNotIn('id', $locations->pluck('id')->toArray())
                                            ->withCount('active_tutors')
                                            ->orderBy('active_tutors_count', 'desc')
                                            ->get();

        return view('btn.assign.index', compact('assignments', 'locations', 'loc_without_assign')); 

    }

    /**
     * Show all unassigned & declined tutor request
     *
     * @return \Illuminate\Http\Response
     */
    public function unassignedRequests()
    {
        $this->authorize('admin-only');
        
        $unassigned_requests = MarketTutorRequest::currentSemester()
                        ->where('unassigned', 1)
                        ->latest('id')->get();

        $locations = Location::pluck('city', 'id'); //creates array with key => value of id=>city
        return view('btn.assign.unassigned_requests', compact('unassigned_requests', 'locations')); 

    }

    /**
     * Show all declined tutor request
     *
     * @return \Illuminate\Http\Response
     */
    public function declinedRequests()
    {
        $this->authorize('admin-only');

        $declined_requests = MarketTutorRequest::currentSemester()
                        ->where('requested_tutor_declined', 1)
                        ->latest('id')->get();

        $locations = Location::pluck('city', 'id'); //creates array with key => value of id=>city
        return view('btn.assign.declined_requests', compact('declined_requests', 'locations')); 

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('admin-only');
        
        $tutors = Tutor::activeTutors()->orderBy('first_name', 'ASC')->get();

        $locations = Location::pluck('city', 'id');

        $tutor_requests = MarketTutorRequest::currentSemester()
                                        ->where('matched_date', null)
                                        ->where('requested_tutor_declined', 0)
                                        ->orderBy('id', 'desc')
                                        ->get();

        return view('btn.assign.create', compact('tutors', 'locations', 'tutor_requests'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function makeAssignment(Request $request, $tutor_request_id)
    {
        //08/14/2017 NOT USED:  Matches made by students or tutors, no longer by admin 
        $this->authorize('admin-only');
        $tutor_id = $request->tutor_id;
        
        $tutor_request = MarketTutorRequest::where('id', $tutor_request_id)->first();
        $assigned_by = auth()->user()->id;
        $submitted_by = 'admin';

        $ref_id = mt_rand(10000000, 99999999);

        $assignment = Assignment::create([
            'ref_id' => $ref_id,
            'start_date' => $tutor_request->start_date,
            'end_date' => $tutor_request->end_date,
            'student_id' => $tutor_request->student_id,
            'child_name' => $tutor_request->child_name,
            'tutor_id' => $tutor_id,
            'assigned_by' => $assigned_by,
            'location_id' => $tutor_request->location_id,
            'course' => $tutor_request->course,
            ]);
        
        //Update Tutor Request and include Match details
        $tutor_request->mkt_visibility = 0;
        $tutor_request->matched_tutor_id = $tutor_id;
        $tutor_request->matched_date = Carbon\Carbon::now();
        $tutor_request->matched_by_id = $assigned_by;
        $tutor_request->assignment_id = $assignment->id;

        $tutor_request->save();

        $this->dispatch(new CreateAssignment($assignment, $tutor_request, $tutor_id,  $submitted_by));

        \Session::flash('success', "Tutor Assignment Created.  Both parties notified.");        

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function unassignStudent(Request $request, $tutor_request_id)
    {
        $this->authorize('admin-only');
        
        $tutor_request = MarketTutorRequest::where('id', $tutor_request_id)->first();

        $tutor_request->unassigned = 1;
        $tutor_request->matched_date = Carbon\Carbon::now();
        $tutor_request->matched_by_id = auth()->user()->id;

        $tutor_request->save();

        //Construct E-mail To Student
        $user = User::find($tutor_request->student_id);       
        $location = Location::find($tutor_request->location_id);
        
        $subject = "No Tutor Available For $tutor_request->course";
        $email_view = "btn.emails.students.unassign_notification";
        $email_data = ['tutor_request' => $tutor_request];

        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('info', 'Student Unassigned, but remains visible on MarketPlace');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function declineAssignment($assign_ref_id)
    {
        $assignment = Assignment::where('ref_id', $assign_ref_id)->first();
        $this->authorize('student-access', $assignment);
        $assignment->declined = 1;
        $assignment->update();

        //Construct E-mail To Tutor
        $user = User::find($assignment->tutor->id);       
        $location = Location::find($assignment->location_id);
        
        $subject = "Assignment Declined";
        $email_view = "btn.emails.tutors.assignment_declined";
        $email_data = ['assignment' => $assignment];

        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        \Session::flash('success', 'Assignment successfully declined. If you still need assistance with this course, feel free to submit another tutor request.');

        return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ref_id)
    {
        $this->authorize('admin-only');
        $assignment = Assignment::where('ref_id', $ref_id)->firstOrFail();
        return view('btn.assign.show', compact('assignment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ref_id)
    {
        $this->authorize('admin-only');
        $assignment = Assignment::where('ref_id', $ref_id)->firstOrFail();
        return view('btn.assign.edit', compact('assignment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AssignmentRequest $request, $id)
    {
        $this->authorize('admin-only');
        $assignment = Assignment::findOrFail($id);
    
        $assignment->update($request->all());

        \Session::flash('success', 'Assignment successfully updated!');
        return redirect('assignments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('admin-only');
        
        $assignment = Assignment::find($id);
        
        if ($assignment) {
            $end_date = Carbon\Carbon::now();
            $message = "";

            $prepaid_balance = $assignment->prepaid_balance();
            
            if ($prepaid_balance > 0) {
                $amount_refunded = $assignment->refund_prepaid_balance();
                
                if (!is_numeric($amount_refunded)) {
                    \Session::flash('error', "REFUND ERROR: $amount_refunded");
                    return redirect()->back();
                }

                if ($prepaid_balance > $amount_refunded) {
                    $status = "info";
                    $message = "NOTE: ONLY $$amount_refunded out of $$prepaid_balance was refunded to student. "; 
                    $end_date = Carbon\Carbon::now()->addDay();
                }
                else{
                    $status = "success";
                    $message = "Entire prepaid balance of $$amount_refunded refunded to student. EMAIL STUDENT and inform of refund. ";    
                    /*if ($assignment->tutorSessions->count()) {
                        $assignment->end_date = Carbon\Carbon::now();
                        $assignment->save();
                    }
                    else{
                       $assignment->delete(); 
                    }*/
                }
            }
            else{
                $status = "success";
                // $message = "Assignment successfully deleted!";    

                /*if ($assignment->tutorSessions->count()) {
                    $assignment->end_date = Carbon\Carbon::now();
                    $assignment->save();
                }
                else{
                   $assignment->delete(); 
                }*/

            }

            $message .= "Assignment end_date updated to today. All assignments must be deleted manually in database.";

            $assignment->end_date = $end_date;
            $assignment->save();

            \Session::flash($status, $message);
        }
        else{
            \Session::flash('error', "Unable to find Assignment # $id");
        }
        
        return redirect()->back();
    }
}
