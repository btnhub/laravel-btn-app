<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Crypt;
use DB;
use Carbon;

class ProficiencyExamsController extends Controller
{
	public function __construct()
	{
		$this->exam_path = base_path("bufftutor/proficiency_exams/Tutor_Proficiency_Exams");	
		$this->middleware('auth');
    	$this->middleware('role:tutor-applicant|tutor|music-teacher|marketplace-tutor|test-tutor');
	}

    public function getExamList()
    {

    	$exam_path = $this->exam_path;
	    if ($dh = opendir($exam_path)){

	    	$exam_files = array();
	      	$dept = array();
	      	$exam_list = array();

		    while (($file = readdir($dh)) !== false){
		        $exam_files[] = $file;
		        $dept[] = strchr($file, "-", true);
		    }//while

		    closedir($dh);
		      
		    // Sort Exam File Names Alphabetically and then display them in the form
		    sort($exam_files);
		    $dept = array_unique($dept);
			$i = 0;
			foreach ($exam_files as $exam_name) {
		        $exam_dept = strchr($exam_name, "-", true);
		        if(strchr($exam_name, ".csv") !== false){    
		            $exam_name_edited = strchr(ucwords(str_replace("_", " ", substr(strchr($exam_name, "-"),1))), ".csv", true);
		           	// Strips course topic (characters before -, e.g Chemistry), removes -, replaces _ with spaces, Capitalizes first letter of each word, remove .csv extension 
		        	$exam_list[$exam_dept][$exam_name] = $exam_name_edited;
		    	}
		    }// foreach
		}//if

		else
		{
			$request->session()->flash('error', "Could not load exams");
			return redirect()->back();
		}
   
    	return view('btn.proficiency_exams.select_exam', compact('exam_list'));
    }

    public function loadSelectedExam(Request $request)
    {
    	$this->validate($request, [
    		'exam' => 'required'
    		]);
    	$exam = $request->exam;

    	$img_dir = asset("btn/Prof_Exam_Images");
    	$questions = $this->examUpload($exam); 
    	//return view('btn.proficiency_exams.prof_exam', compact('questions', 'img_dir', 'exam'));
    	return view('btn.pages.citybook.proficiency_exam', compact('questions', 'img_dir', 'exam'));
    }

    public function gradeExam(Request $request)
    {
    	$user = auth()->user();
    	$min_score = DB::table('settings')->where('field', 'prof_passing_score')->first()->value ?? 0.8; 
    	$exam = $request->exam;
    	$Answers = $request->answers; // Get user submitted answers; just the letter answer is stored in this array
        $Questions = $request->questions; //new Questions array QuestionNo => Question
        $reference = $request->reference;

        $CorrectAnswer = $request->cans; //new array QuestionNo => Correct Answer

        //decrypt Answers
        foreach ($CorrectAnswer as $key => $value) {
        	$CorrectAnswer[$key] = Crypt::decrypt($value);
        }
        

        // Automated question checking
        $score = 0; // counts number of correct answers

        foreach ($CorrectAnswer as $QuestionNo => $Solution){
            if (array_key_exists($QuestionNo, $Answers)) {
                if ($Answers[$QuestionNo] != $Solution){
            
                    $Answers[$QuestionNo] = "<span style='color: red;'>Your Answer: $Answers[$QuestionNo]</span><br>
                    	<em>Correct Answer: <span style='color: green;'>$Solution</span></em>";
                } 
                else {

                    $Answers[$QuestionNo] = "Your/Correct Answer: <span style='color: green;'>$Solution</span>";
                    $score++;
                }
            }
            else $Answers[$QuestionNo] = '<span style="color: red;">You Did Not Select An Answer</span>';
        }

        
        $final_score = $score / count($Questions);
        
        //Insert results into database
        DB::table('prof_exam_results')->insert([
        		'user_id' => $user->id,
        		'first_name' => $user->first_name,
        		'last_name' => $user->last_name,
        		'exam' => $exam,
        		'grade' => round($final_score*100),
        		'created_at' => Carbon\Carbon::now(),
        		'updated_at' => Carbon\Carbon::now()
        	]);

        //return view('btn.proficiency_exams.exam_results', compact('Questions', 'Answers', 'score', 'final_score', 'min_score', 'reference'));
        $grade =  round($final_score*100);

        \Session::flash('info', "You scored {$grade}%. Please take an exam for each course you tutor then proceed to the next step.");

        return redirect()->route('profile.account', ['user_ref_id' => $user->ref_id]);
    }

    public function examUpload($filename){
	    $header = NULL;
	    $data =  array();
	    $question_count = DB::table('settings')->where('field', 'prof_question_count')->first()->value ?? 20;
	    $exam_path = $this->exam_path;
	    $filename = "$exam_path/$filename";
	    $file = fopen($filename, "r");
	    while(($row = fgetcsv($file)) !== FALSE)
	        {
	        if(!$header)
	            $header = $row; // if header is Null, sets $header to first row in file (title row of file)
	        else
	            $data[] = array_combine($header, $row); // appends $data array with new row in file
	        }
	    fclose($file);
	  
	   shuffle($data); //randomizes Test Bank array

	    // Code to clean up $data array's indexes.  
	    //1) change nested array Answers indices from numbers (0,1...) to capital letters (A,B..)
	    foreach ($data as $key => $colheader){
	        $colheader['Answers'] = explode("\n", $colheader['Answers']) ;
	        //shuffle($colheader['Answers']); //randomizes multiple choice 'Answers' column

	        $endLetter = strtoupper(base_convert(count($colheader['Answers'])+9,10,26)); // converts row count in array into a letter.  10 = A, therefore count + 9 to get correct end letter, then converts string into upper case
	        $colheader['Answers'] = array_combine(range("A",$endLetter), $colheader['Answers']); // replaces array index with Alphabet keys

	        $data[$key]['Answers'] = $colheader['Answers'];
	        }

	    //2) Change $data array indices to begin ftom 1 rather than 0
	    $data = array_combine(range(1,count($data)), $data); 
	    $data = array_slice($data,0,$question_count); 
	    return $data;
    }
}
