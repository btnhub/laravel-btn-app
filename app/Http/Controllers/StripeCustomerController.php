<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Customer;

class StripeCustomerController extends Controller
{

	public function storeCustomerCard(){
		$user = User::find(auth()->user()->id);

		\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

		// Get the credit card details submitted by the form
		$token = $_POST['stripeToken'];
		
		$card_type = \Stripe\Token::retrieve($token)->card ? \Stripe\Token::retrieve($token)->card->funding : '';
    	
    	if (!in_array($card_type, ['credit', 'debit'])) {
    		\Session::flash('error', "Invalid Card Type: '$card_type'. Only Credit/Debit cards are permitted, prepaid cards are not allowed. ");
    		return redirect()->back();
    	}
    	
		try {
		  
			// Create a Customer
			$customer = \Stripe\Customer::create([
				"source" => $token,
			  	"description" => $user->full_name." - ".$user->id." - ".$user->email,
			  	"email" => $user->email
			  ]);

			if ($customer) {
			  	Customer::create([
					"user_id" => $user->id,
					"stripe_customer_id" => $customer->id,
					"card_brand" => $customer->sources->data[0]->brand,
					"card_last_four" => $customer->sources->data[0]->last4,
					"exp_month" => $customer->sources->data[0]->exp_month,
					"exp_year" => $customer->sources->data[0]->exp_year
				]);
			}

			\Session::flash('success', 'Card successfully added to your account');
			return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);

		} catch (\Stripe\Error\ApiConnection $e) {
 		   // Network problem, perhaps try again.
			$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
    		\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\InvalidRequest $e) {
    		// You screwed up in your programming. Shouldn't happen!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\Api $e) {
    		// Stripe's servers are down!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\Card $e) {
    		// Card was declined.
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			\Session::flash('error', $error['message']);
			return redirect()->back();
		}

	}

    public function updateCustomerCard(){
		//Update edited Customer CC info with Stripe & in database
		
    	\Stripe\Stripe::setApiKey(config('services.stripe.secret'));

		// Get the new credit card details submitted by the form
    	$token = $_POST['stripeToken'];
    	
    	$card_type = \Stripe\Token::retrieve($token)->card ? \Stripe\Token::retrieve($token)->card->funding : '';
    	
    	if (!in_array($card_type, ['credit', 'debit'])) {
    		\Session::flash('error', "Invalid Card Type: '$card_type'. Only Credit/Debit cards are permitted, prepaid cards are not allowed. ");
    		return redirect()->back();
    	}
    	
	  	$btn_customer =  Customer::where('user_id', auth()->user()->id)->first();

	  	//Retrieve Stripe Customer
	  	$customer = \Stripe\Customer::retrieve($btn_customer->stripe_customer_id);

	  	try {
		  
			//Update with new CC info
	  		$customer->source = $token;
	  		$customer->save();

		  	$input = [
					"card_brand" => $customer->sources->data[0]->brand,
					"card_last_four" => $customer->sources->data[0]->last4,
					"exp_month" => $customer->sources->data[0]->exp_month,
					"exp_year" => $customer->sources->data[0]->exp_year
					];
			$btn_customer->update($input);
			$btn_customer->save();

			\Session::flash('success', 'Card Successfully Updated');
			return redirect()->back();

		} catch (\Stripe\Error\ApiConnection $e) {
 		   // Network problem, perhaps try again.
			$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
    		\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\InvalidRequest $e) {
    		// You screwed up in your programming. Shouldn't happen!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\Api $e) {
    		// Stripe's servers are down!
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			\Session::flash('error', $error['message']);
			return redirect()->back();

		} catch (\Stripe\Error\Card $e) {
    		// Card was declined.
    		$e_json = $e->getJsonBody();
    		$error = $e_json['error'];
			\Session::flash('error', $error['message']);
			return redirect()->back();
		}

	}
}
