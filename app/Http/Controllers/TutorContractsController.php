<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon;
use DB;
use Nexmo;

use App\User;
use App\Tutor;
use App\Student;
use App\Customer;
use App\Location;
use App\Assignment;
use App\Prepayment;
use App\TutorSession;
use App\TutorContract;
use App\MarketTutorRequest;

use App\Jobs\SendFCMNotification;
use App\Jobs\SendGeneralNotification;
use App\Jobs\SendSMSAlert;
use App\Jobs\EmailAdmin;

class TutorContractsController extends Controller
{
    /*public function tempFixAssignCreateContract(){
        $problem_assignments = Assignment::active()->where('tutor_contract_id', NULL)->get();

        $assign_ids = $null_rates = null;

        foreach ($problem_assignments as $problem_assignment) {
            $tutor = Tutor::find($problem_assignment->tutor_id);
            $student_rate = $problem_assignment->rate ?? 0;
            $tutor_rate = number_format($student_rate * (1-$tutor->profile->tutor_fee));

            $original_tutor_request = MarketTutorRequest::where('assignment_id', $problem_assignment->id)->first();
            $original_tutor_request_id = $original_tutor_request ? $original_tutor_request->id : null;

            $new_contract = TutorContract::create([
                'ref_id' => mt_rand(10000000, 99999999),
                'tutor_request_id' => $original_tutor_request_id,
                'tutor_id' => $tutor->id,
                'student_rate' => $student_rate,
                'tutor_rate' => $tutor_rate,
                'tutor_decision' => 1,
                'student_decision' => 1,
                'tutor_decision_date' => Carbon\Carbon::now(),
                'student_decision_date' => Carbon\Carbon::now(),
                'start_date' => $problem_assignment->start_date,
                'end_date' => $problem_assignment->end_date,
                'course' => $problem_assignment->course
            ]);

            $problem_assignment->tutor_contract_id = $new_contract->id;
            $problem_assignment->save();
            $assign_ids .= "$problem_assignment->id, ";
            if ($student_rate == 0) {
                $null_rates .="$problem_assignment->id, ";    
            }
        }

        \Session::flash('success', "The following assignments were updated: $assign_ids.  The following have $0 rates $null_rates");

        return redirect()->route('profile.account', auth()->user()->ref_id);
    }*/

    public function createMatch(TutorContract $tutor_contract){
        $student = Student::find($tutor_contract->tutor_request->student_id);
        $location = Location::find($tutor_contract->tutor_request->location_id);
        
        $ref_id = mt_rand(10000000, 99999999);
        $tutor_id = $tutor_contract->tutor_id;
        $tutor_request = $tutor_contract->tutor_request;

        $prepayment = null;

        
        if (!$student->hasRole('daniels-fund-student')) {
            //Charge First Hour
            if (!$tutor_contract->assignment || ($tutor_contract->assignment && !Prepayment::where('assignment_id', $tutor_contract->assignment->id)->where('captured',1)->where('paid', 1)->first())) {
                //If prepayment does not exist, charge customer (prevent duplicate charges)
                
                $discount = 0;
                $student_rate = $tutor_contract->student_rate;
                $amount = number_format($student_rate,2);

                if(!TutorSession::where('student_id', $student->id)->exists())
                {
                    //Apply first session discount
                    $discount_percent = (float) DB::table('settings')->where('field', 'first_session_discount')->first()->value;   
                    
                    $discount = $student_rate * $discount_percent;

                    $amount= number_format($student_rate - $discount, 2);
                }

                $customer = Customer::where('user_id', $student->id)->first();
                $amount_stripe = $amount * 100;
                $description = "First Hour Prepayment: {$tutor_contract->tutor->full_name} & {$student->full_name}";
                $payment_method = 1; //1 = stripe

                \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

                try {
                    //Charge Customer
                    $charge = \Stripe\Charge::create(
                        [
                          "amount" => $amount_stripe, // amount in cents, again
                          "currency" => "usd",
                          "customer" => $customer->stripe_customer_id,
                          "description" => $description,
                        ]
                    );

                    if ($charge) {    

                        $stripe_transaction = \Stripe\BalanceTransaction::retrieve($charge->balance_transaction);

                        $prepayment = Prepayment::create([
                            "ref_id" => strtoupper(str_random(12)),
                            "user_id" => $customer->user_id,
                            "stripe_customer_id" => $customer->stripe_customer_id,
                            "transaction_id" => $charge->id,
                            "reason" => $description,
                            "amount" => $student_rate,
                            "discount_amount" => $discount,
                            "processing_fee" => $stripe_transaction->fee / 100,
                            "total_amount" => $amount,
                            "payment_method" => $payment_method,
                            "captured" => $charge->captured,
                            "paid" => $charge->paid,
                            "completed_at" => Carbon\Carbon::now()
                        ]);
                    }

                } catch (\Stripe\Error\ApiConnection $e) {
                   // Network problem, perhaps try again.
                    $e_json = $e->getJsonBody();
                    $error = $e_json['error'];

                    return (object)['error' => $error['message']];

                } catch (\Stripe\Error\InvalidRequest $e) {
                    // You screwed up in your programming. Shouldn't happen!
                    $e_json = $e->getJsonBody();
                    $error = $e_json['error'];
                    
                    return (object)['error' => $error['message']];

                } catch (\Stripe\Error\Api $e) {
                    // Stripe's servers are down!
                    $e_json = $e->getJsonBody();
                    $error = $e_json['error'];
                    
                    return (object)['error' => $error['message']];

                } catch (\Stripe\Error\Card $e) {
                    // Card was declined.
                    $e_json = $e->getJsonBody();
                    $error = $e_json['error'];
                    
                    return (object)['error' => $error['message']];
                }
            }
        }

        //Update DB With New Match
        $tutor_contract->student_decision = 1;
        $tutor_contract->student_decision_date = Carbon\Carbon::now();
        $tutor_contract->save();

        $assignment = Assignment::firstOrNew([
            'tutor_contract_id' => $tutor_contract->id,
            'start_date' => $tutor_request->start_date,
            'end_date' => $tutor_request->end_date,
            'student_id' => $tutor_request->student_id,
            'tutor_id' => $tutor_id,
            'assigned_by' => $student->id,
            'location_id' => $tutor_request->location_id,
            'course' => $tutor_contract->course,
            'online' => $tutor_contract->online
            ]);
        $assignment->ref_id = $ref_id;
        $assignment->save();
        
        if ($prepayment) {
            //Update created prepayment object with the assign id
            $prepayment->assignment_id = $assignment->id;
            $prepayment->save();

            //EMAIL Student About Prepayment Charge
            $student = User::find($assignment->student_id);       
            $subject = "Prepayment Confirmation: {$assignment->tutor->user_name} in {$assignment->course}";
            $email_view = "btn.emails.students.prepayment_charged";
            $email_data = ['assignment' => $assignment, 'student_rate' => $student_rate, 'discount' => number_format($discount, 2), 'amount' => $amount];

            $this->dispatch(new SendGeneralNotification($student, $location, $subject, $email_view, $email_data));
        }

        //Update Tutor Request and include Match details
        $tutor_request->mkt_visibility = 0;
        $tutor_request->matched_tutor_id = $tutor_id;
        $tutor_request->matched_date = Carbon\Carbon::now();
        $tutor_request->matched_by_id = $student->id;
        $tutor_request->assignment_id = $assignment->id;

        $tutor_request->save();

        //EMAIL Tutor
        $user = User::find($tutor_contract->tutor_id);       
        $subject = "Match Created: {$tutor_contract->tutor_request->student->user_name} in {$tutor_contract->course}";
        $email_view = "btn.emails.student_tutor_contracts.notify_tutor_contract_accepted";
        $email_data = ['tutor_contract' => $tutor_contract];

        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

        //FCM Tutor (For Mobile App)
        /*if ($user->activeFCMTokens->count()) {
            $title = "Match Created: {$tutor_contract->tutor_request->student->user_name} in {$tutor_contract->course}";
            $body = "";
            
            $notification = ['title' => $title, 'body' => $body];
            $data = ['type'=> 'tutor-proposal-accepted',
                        'tutorContractID' => $tutor_contract->ref_id
                    ];

            $this->dispatch(new SendFCMNotification($user, $location, $notification, $data));  
        }*/
        return true;
    }

    public function listTutorRequests(){
    	$this->authorize('admin-only');
        
        $tutors = Tutor::activeTutors()
                        ->orderBy('first_name', 'ASC')
                        ->get()
                        ->filter(function($tutor){
                            return $tutor->profile->tutor_status == 1;
                        });
        
        $tutor_fees = array();

        $default_tutor_fee = DB::table('settings')->where('field', 'tutor_fee')->first()->value;
        
        $min_btn_percent = 0.2;

        foreach ($tutors as $tutor) {
            $tutor_fees[$tutor->id] = $tutor->profile->tutor_fee ?? $default_tutor_fee;
        } 
        $tutor_fees = json_encode($tutor_fees); //to pass array to javascript

        //$locations = Location::pluck('city', 'id');

        $tutor_requests = MarketTutorRequest::currentSemester()
                                ->where('unassigned', 0)
                                ->where('assignment_id', null)
                                ->doesnthave('assignment')
                                ->orderBy('id');

        $tutor_requests_count = $tutor_requests->count();

        $unaddressed_tutor_requests = $tutor_requests->doesnthave('tutor_contracts')->get();

        //Tutor requests where tutors have accepted a contract, but students haven't responded
        $pending_requests_student = MarketTutorRequest::currentSemester()
                                    ->where('unassigned', 0)
                                    ->doesnthave('assignment')
                                    ->has('tutor_contracts')
                                    ->whereHas('tutor_contracts', function ($contract){
                                        $contract->where('tutor_decision', 1)
                                            ->where('student_decision', null)
                                            ->where('deadline_student', ">=", Carbon\Carbon::now());
                                        })
                                    ->orderBy('id')
                                    ->get();
        
        //Tutor requests where no tutor has accepted a contract, deadline has not passed yet
        $pending_requests_tutors = MarketTutorRequest::currentSemester()
                                    ->where('unassigned', 0)
                                    ->doesnthave('assignment')
                                    ->has('tutor_contracts')
                                    ->whereNotIn('id', $pending_requests_student->pluck('id'))
                                    /*->whereDoesntHave('tutor_contracts', function ($contract){
                                        $contract->where('deadline', '<=', Carbon\Carbon::now());
                                        })*/
                                        ->whereHas('tutor_contracts', function ($contract){
                                        $contract->where('deadline', '>', Carbon\Carbon::now());
                                        })
                                    ->orderBy('id')
                                    ->get();

        //Tutor requests that all tutors missed the deadline or declined 
        $missed_tutor_requests = MarketTutorRequest::currentSemester()
                                    ->where('unassigned', 0)
                                    ->doesnthave('assignment')
                                    ->has('tutor_contracts')
                                    ->whereDoesntHave('tutor_contracts', function ($contract){
                                        $contract->where(function($query){
                                            $query->where('tutor_decision', '=', 1)
                                            ->orWhere('deadline', '>=', Carbon\Carbon::now());
                                        });})
                                    ->orderBy('id')
                                    ->get();
        
        //Tutor requests where tutors have accepted a contract, but students haven't responded
        $student_declined_requests = MarketTutorRequest::currentSemester()
                                    ->where('unassigned', 0)
                                    ->doesnthave('assignment')
                                    ->has('tutor_contracts')
                                    ->whereHas('tutor_contracts', function ($contract){
                                        $contract->where('tutor_decision', '=', 1);
                                        })
                                    ->whereDoesntHave('tutor_contracts', function ($contract){
                                        $contract->where('student_decision', '=', 1)
                                            /*->orWhere(function($q){
                                                $q->where('tutor_decision', '!=', 1)
                                                ->orWhere('deadline_student', '>=', Carbon\Carbon::now());
                                            })*/
                                            ->orWhere('deadline_student', '>=', Carbon\Carbon::now());
                                        })
                                    ->orderBy('id', 'desc')
                                    ->get();

        //Unassigned
        $unassigned_requests = MarketTutorRequest::currentSemester()
                        ->where('unassigned', 1)
                        ->latest('id')->get();
    	//return view('btn.student_tutor_contracts.admin.view_tutor_requests', compact('tutors', 'locations', 'unaddressed_tutor_requests', 'missed_tutor_requests','pending_requests_student', 'pending_requests_tutors', 'student_declined_requests', 'tutor_requests_count', 'tutor_fees', 'min_btn_percent'));
        
        return view('btn.pages.citybook.admin.view_tutor_requests', compact('tutors', /*'locations',*/ 'unaddressed_tutor_requests', 'missed_tutor_requests','pending_requests_student', 'pending_requests_tutors', 'student_declined_requests', 'unassigned_requests', 'tutor_requests_count', 'tutor_fees', 'min_btn_percent'));
    }

    public function createTutorContract(Request $request, $tutor_request_id){
    	$this->authorize('admin-only');

        $this->validate($request, [
            'student_rate.*' => 'required_with:tutors.*',
            'tutor_rate.*' => 'required_with:student_rate.*',
            'deadline.*' => 'required_with:tutors.*',
        ]);
    	
        $tutor_request = MarketTutorRequest::find($tutor_request_id);

        $tutor_request->update(['unassigned' => 0]);

    	$count = 0;

    	foreach ($request->tutors as $key => $tutor_id) {
    		if ($tutor_id) {   			
    			$student_rate = $request->student_rate[$key];
    			$tutor_rate = $request->tutor_rate[$key];
    			$deadline = $request->deadline[$key];
    			$concerns = $request->concerns[$key] ? trim($request->concerns[$key]) : null;

    			//Create Contract
    			$new_contract = TutorContract::create([
    					'ref_id' => mt_rand(10000000, 99999999),
    					'tutor_request_id' => $tutor_request_id,
    					'tutor_id' => $tutor_id,
    					'student_rate' => $student_rate,
    					'tutor_rate' => $tutor_rate,
    					'deadline' => $deadline,
    					'concerns' => $concerns,
    					'start_date' => $tutor_request->start_date,
    					'end_date' => $tutor_request->end_date,
    					'course' => $tutor_request->course
    				]);

    			//EMAIL TUTORS
    			//$user = User::find($tutor_id);       
                //$location = Location::find($tutor_request->location_id);
                $location = $tutor_request->nearest_btn_location();
                $tutor = Tutor::find($tutor_id);

                /*if (!$user->locations->contains($location->id) && $tutor_request->include_online_tutors) {
                    //ONLINE TUTOR
                    $location = Location::where('city', 'like', '%online%')->first();
                    $new_contract->online = 1;
                    $new_contract->save();
                }*/

                if (!$tutor->nearest_geolocation($tutor_request->lat, $tutor_request->lng) && $tutor_request->include_online_tutors) {
                    //ONLINE TUTOR
                    $location = Location::where('city', 'like', '%online%')->first();
                    $new_contract->online = 1;
                    $new_contract->save();
                }


                $subject = "New Tutor Request: $tutor_request->course";
                $email_view = "btn.emails.student_tutor_contracts.notify_tutors_review";
                $email_data = ['tutor_request' => $tutor_request, 'deadline' => $deadline, 'tutor' => $tutor/*, 'city' =>$location->city*/];

                $this->dispatch(new SendGeneralNotification($tutor, $location, $subject, $email_view, $email_data));
                
                //FCM Tutor
                /*if ($tutor->activeFCMTokens->count()) {
                    $title = "New Tutor Request: $tutor_request->course";
                    $body = "";
                    
                    $notification = ['title' => $title, 'body' => $body];
                    $data = ['type'=> 'new-tutor-contract',
                                'tutorContract' => json_encode($new_contract->tutorSummary())
                            ];

                    $this->dispatch(new SendFCMNotification($tutor, $location, $notification, $data));  
                }*/

                $count++;
    		}
    	}
    	
    	\Session::flash('success', "$count Tutors Contacted");

    	return redirect()->back();
    }

    public function sendMatchReminder(Request $request, $method, $tutor_request_id){

        $this->authorize('admin-only');

        $tutor_request = MarketTutorRequest::find($tutor_request_id);
        $user = User::find($tutor_request->student_id);
        $location = Location::find($tutor_request->location_id);

        $remaining_days = $tutor_request->tutor_contracts->where('tutor_decision', 1)->min('deadline_student')->diffInDays();

        if ($method == 'email') {
            
            $no_of_proposals = $tutor_request->tutor_contracts->where('tutor_decision', 1)->count();

            //EMAIL STUDENT    
            $subject = "Tutor Match For {$tutor_request->course} (ACTION REQUIRED)";
            $email_view = "btn.emails.student_tutor_contracts.reminder_student_contracts_submitted";
            $email_data = ['tutor_request' => $tutor_request,
                            'no_of_proposals' => $no_of_proposals,
                            'remaining_days' => $remaining_days];

            $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

            $tutor_request->email_reminder = Carbon\Carbon::now();
            $tutor_request->save();

            $flash_message = "Email Alert Sent To $user->full_name";
        }

        elseif($method =='sms'){
            //SMS Student
            $url = route('my.tutor.requests', $user->ref_id);

            $text_body = 
            "Reminder: $location->company_name Match!\n\nReview your tutor proposal here $url You have $remaining_days more ". str_plural('day', $remaining_days) . " to respond.";

            $this->dispatch(new SendSMSAlert($user, $location, $text_body));  
            
            $tutor_request->sms_reminder = Carbon\Carbon::now();
            $tutor_request->save();

            $flash_message = "SMS Alert Sent To $user->full_name ($user->phone)";
        }
       
        \Session::flash("success", $flash_message);

        return redirect()->back(); 
    }

    public function renewTutorProposal(Request $request, $tutor_contract_ref_id){
        
        $tutor_contract = TutorContract::where('ref_id', $tutor_contract_ref_id)->first();
        $location = Location::find($tutor_contract->tutor_request->location_id);

        //Email Admin
        $subject = "Renew Contract: $tutor_contract->course - {$tutor_contract->tutor->full_name} & {$tutor_contract->tutor_request->student->full_name}";
        $email_view = 'btn.emails.student_tutor_contracts.notify_admin_renew_contract';
        $email_data = ['tutor_contract' => $tutor_contract];

        $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));

        \Session::flash('success', "We'll contact {$tutor_contract->tutor->user_name}. You will hear back from us in 1-2 days.  Be sure to check your Junk/Spam folder for the e-mail.");
        
        return redirect()->back();
    }

    public function myTutorRequests($user_ref_id){
        //for students to view pending tutor requests
        $student = Student::where('ref_id', $user_ref_id)->first();

        $this->authorize('self-access', $student);

        $tutor_requests = $student->tutorRequests()->with('tutor_contracts')
                            ->where('end_date', '>', Carbon\Carbon::now())
                            ->whereHas('tutor_contracts', function($contract){
                                $contract->where('tutor_decision', 1);
                            })
                            ->get();

        $unreviewed_tutor_contracts = collect([]);
        $declined_tutor_contracts = collect([]); 
        $missed_tutor_contracts = collect([]);

        foreach ($tutor_requests as $tutor_request) {    
            foreach ($tutor_request->tutor_contracts()->where('tutor_decision', 1)->get() as $tutor_contract) {

                if ($tutor_contract->student_decision != 1 && $tutor_contract->deadline_student < Carbon\Carbon::now()) 
                {
                    $missed_tutor_contracts->push($tutor_contract);
                }
                elseif($tutor_contract->student_decision === null && $tutor_contract->deadline_student > Carbon\Carbon::now())
                {
                    $unreviewed_tutor_contracts->push($tutor_contract);
                }
                elseif($tutor_contract->student_decision === 0 && $tutor_contract->deadline_student > Carbon\Carbon::now())
                {
                    $declined_tutor_contracts->push($tutor_contract);
                }
            }
        }

        $unreviewed_tutor_contracts = $unreviewed_tutor_contracts->sortBy('tutor_decision_date');
        $missed_tutor_contracts = $missed_tutor_contracts->sortBy('tutor_decision_date');
        $declined_tutor_contracts = $declined_tutor_contracts->sortBy('tutor_decision_date');

        //return view('btn.student_tutor_contracts.students.my_tutor_requests', compact('student', 'tutor_requests', 'unreviewed_tutor_contracts', 'missed_tutor_contracts', 'declined_tutor_contracts'));
        
        return view('btn.pages.citybook.my_tutor_requests', compact('student', 'tutor_requests', 'unreviewed_tutor_contracts', 'missed_tutor_contracts', 'declined_tutor_contracts'));
    }

    public function getMyTutorContracts($user_ref_id){
        $tutor = Tutor::where('ref_id', $user_ref_id)->first();

        $this->authorize('self-access', $tutor);

        $tutor_contracts = TutorContract::has('tutor_request')
                                ->where('tutor_id', $tutor->id)
                                ->get()
                                ->filter(function ($contract, $key){
                                    return $contract->end_date->isFuture(); });
        
        $unreviewed_tutor_contracts = $tutor_contracts->where('tutor_decision', null)->filter(function($contract, $key){return $contract->deadline->isFuture();})->sortBy('id');

        $accepted_tutor_contracts = $tutor_contracts->where('tutor_decision', 1)
                            ->whereIn('student_decision', [null, 0])
                            ->filter(function ($contract, $key){
                                return $contract->deadline_student->isFuture(); })
                            ->sortByDesc('id');
        
        $declined_tutor_contracts = $tutor_contracts->where('tutor_decision', 0)->sortByDesc('id');

        $missed_tutor_contracts = $tutor_contracts->where('tutor_decision', null)
                        ->filter(function($contract, $key){return $contract->deadline->isPast();})
                        ->sortByDesc('id');
        
        //return view('btn.student_tutor_contracts.tutors.my_tutor_contracts', compact('unreviewed_tutor_contracts', 'accepted_tutor_contracts', 'declined_tutor_contracts', 'missed_tutor_contracts'));
        
        return view('btn.pages.citybook.my_tutor_contracts', compact('unreviewed_tutor_contracts', 'accepted_tutor_contracts', 'declined_tutor_contracts', 'missed_tutor_contracts'));

    }

    public function submitTutorDecision(Request $request, $tutor_contract_ref_id){
        $this->validate($request, [
            'decision' => 'required',
            'tutor_reject_reason' => 'required_if:decision,0',
            ]);

        $tutor_contract = TutorContract::where('ref_id', $tutor_contract_ref_id)->first();
        
        $this->authorize('tutor-access', $tutor_contract);

        $tutor_contract->tutor_decision = $request->decision;
        $tutor_contract->tutor_decision_date = Carbon\Carbon::now();
        $tutor_contract->save();

        $student_id = $tutor_contract->tutor_request->student->id;
        $tutors_contacted = TutorContract::where('tutor_request_id', $tutor_contract->tutor_request_id)->count();
        //$location = Location::find($tutor_contract->tutor_request->location_id);
        $location = $tutor_contract->tutor_request->nearest_btn_location();

        if ($request->decision == 1) {
            $user = User::find($student_id);  
            $response_time = DB::table('settings')->where('field', 'response_time')->first()->value; //in days

            $tutor_contract->deadline_student = Carbon\Carbon::now()->addDays($response_time)->format('Y-m-d');
            
            $tutor_contract->save();
            
            $flash_message = "Tutor request accepted.  We'll inform {$tutor_contract->tutor_request->student->first_name} that you can help out.";
            $flash_option = "success";

            //EMAIL STUDENT
            if (!$tutor_contract->email_alert) {
                $subject = "Tutor Match For {$tutor_contract->course}";
                $email_view = "btn.emails.student_tutor_contracts.notify_student_contract_submitted";
                $email_data = ['tutor_contract' => $tutor_contract,
                                'tutors_contacted' => $tutors_contacted];

                $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
                
                $tutor_contract->email_alert = 1;
                $tutor_contract->save();
            }
            

            //SMS STUDENT
            if ($tutor_contract->tutor_request->sms_alert == 1 && !$tutor_contract->sms_alert) {
                
                $url = route('my.tutor.requests', $user->ref_id);

                $text_body = 
                "$location->company_name Match\n\n{$tutor_contract->tutor->user_name} has offered to tutor you! $url Check your email for more details.";

                $this->dispatch(new SendSMSAlert($user, $location, $text_body));  

                $tutor_contract->sms_alert = 1;
                $tutor_contract->save();          
            }

            //FCM Student
            /*if ($user->activeFCMTokens->count()) {
                $title = "Tutor Match For {$tutor_contract->course}";
                $body = "{$tutor_contract->tutor->user_name} has offered to help out and has sent you a proposal to review.";
                
                $notification = ['title' => $title, 'body' => $body];
                $data = ['type'=> 'new-tutor-proposal',
                            'tutorRequestID' => "{$tutor_contract->tutor_request->ref_id}",
                            'tutorContract' => json_encode($tutor_contract->studentSummary())
                        ];

                $this->dispatch(new SendFCMNotification($user, $location, $notification, $data));  
            }*/
        }

        else {
            
            $tutor_contract->tutor_reject_reason = $request->tutor_reject_reason;
            $tutor_contract->save();
            
            $flash_message = "Tutor request rejected. You will have until the listed deadline to change your decision.";

            $flash_option = "info";
            
            //Email Admin
            $subject = "Contract Rejected By Tutor: {$tutor_contract->tutor->user_name} - $tutor_contract->course";
            $email_view = 'btn.emails.student_tutor_contracts.notify_admin_tutor_reject';
            $email_data = ['tutor_contract' => $tutor_contract];

            $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));
        }

        \Session::flash($flash_option, $flash_message);

        return redirect()->back();
    }

    public function submitStudentDecision(Request $request, $tutor_contract_ref_id){
        $this->validate($request, [
            'student_decision' => 'required'
            ]);

        $tutor_contract = TutorContract::where('ref_id', $tutor_contract_ref_id)->first();
        $student = Student::find($tutor_contract->tutor_request->student_id);
        $location = Location::find($tutor_contract->tutor_request->location_id);

        if ($request->student_decision == 1) {
            
            //Check if card exists
            if ($student->customer) {
                $match_results = $this->createMatch($tutor_contract);
                
                if ($match_results->error ?? false) {
                    \Session::flash('error', $match_results->error);
                    return redirect()->back();
                }
                
                // $tutor_contract->student_decision = $request->student_decision;
                // $tutor_contract->student_decision_date = Carbon\Carbon::now();
                // $tutor_contract->save();

                $flash_message = "{$tutor_contract->tutor->first_name}'s contact info is listed below on the Tutor List tab. Reach out to {$tutor_contract->tutor->first_name} and schedule a session.  We also sent {$tutor_contract->tutor->first_name} your contact info. Good luck this semester!";

                $flash_option = "success";

                \Session::flash($flash_option, $flash_message);
                
                return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);
            }
            
            else
            {
                //Redirect to payment form
                return redirect()->route('tutor.contracts.add.card', $tutor_contract->ref_id);
            }
        }

        else {
            
            $tutor_contract->student_decision = $request->student_decision;
            $tutor_contract->student_decision_date = Carbon\Carbon::now();

            $tutor_contract->student_reject_reason = $request->student_reject_reason ? trim($request->student_reject_reason): null;

            $tutor_contract->save() ;
            
            $flash_message = "Tutor Request Rejected.";
            
            $flash_option = "info";

            //Email Tutor
            $user = User::find($tutor_contract->tutor_id);       
            $subject = "Assignment Declined: {$tutor_contract->tutor_request->student->user_name} in {$tutor_contract->course}";
            $email_view = "btn.emails.student_tutor_contracts.notify_tutor_contract_rejected";
            $email_data = ['tutor_contract' => $tutor_contract];

            $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

            //FCM Tutor
            /*if ($user->activeFCMTokens->count()) {
                $title = "Assignment Declined: {$tutor_contract->tutor_request->student->user_name} in {$tutor_contract->course}";
                $body = "";
                
                $notification = ['title' => $title, 'body' => $body];
                $data = ['type'=> 'tutor-proposal-declined',
                            'tutorContractID' => $tutor_contract->ref_id
                        ];

                $this->dispatch(new SendFCMNotification($user, $location, $notification, $data));  
            }*/

            //Email Admin
            $subject = "Contract Rejected By Student: {$tutor_contract->tutor_request->student->user_name} - $tutor_contract->course";
            $email_view = 'btn.emails.student_tutor_contracts.notify_admin_student_reject';
            $email_data = ['tutor_contract' => $tutor_contract];

            $this->dispatch(new EmailAdmin($location, $subject, $email_view, $email_data));
            
            \Session::flash($flash_option, $flash_message);

            return redirect()->back();
        }

        
    }

    public function addCard($tutor_contract_ref_id){
        $tutor_contract = TutorContract::where('ref_id', $tutor_contract_ref_id)->first();
        $student = $tutor_contract->tutor_request->student;
        $daniels_scholar = $student->hasRole('daniels-fund-student') ? true : false;

        return view('btn.pages.citybook.tutor_contract_add_card', compact('tutor_contract', 'daniels_scholar'));
        //return view('btn.student_tutor_contracts.students.add_card', compact('tutor_contract'));

    }

    public function submitCardCreateMatch(Request $request, $tutor_contract_ref_id){
        $tutor_contract = TutorContract::where('ref_id', $tutor_contract_ref_id)->first();

        $user = User::find(auth()->user()->id);

        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));

        // Get the credit card details submitted by the form
        $token = $_POST['stripeToken'];
        
        $card_type = \Stripe\Token::retrieve($token)->card ? \Stripe\Token::retrieve($token)->card->funding : '';
        
        if (!in_array($card_type, ['credit', 'debit'])) {
            \Session::flash('error', "Invalid Card Type: '$card_type'. Only Credit/Debit cards are permitted, prepaid cards are not allowed. ");
            return redirect()->back();
        }
        
        try {
          
            // Create a Customer
            $customer = \Stripe\Customer::create([
                "source" => $token,
                "description" => $user->full_name." - ".$user->id." - ".$user->email,
                "email" => $user->email
              ]);

            if ($customer) {
                Customer::create([
                    "user_id" => $user->id,
                    "stripe_customer_id" => $customer->id,
                    "card_brand" => $customer->sources->data[0]->brand,
                    "card_last_four" => $customer->sources->data[0]->last4,
                    "exp_month" => $customer->sources->data[0]->exp_month,
                    "exp_year" => $customer->sources->data[0]->exp_year
                ]);
            }

            $match_results = $this->createMatch($tutor_contract);
                
            if ($match_results->error ?? false) {
                \Session::flash('error', $match_results->error);
                return redirect()->back();
            }

            \Session::flash('assign_tab', 'visible');
            
            \Session::flash('success', "{$tutor_contract->tutor->first_name}'s contact info is listed below on the Tutors tab. Reach out to {$tutor_contract->tutor->first_name} and schedule a session.  We also sent {$tutor_contract->tutor->first_name} your contact info. Good luck this semester!");

            return redirect()->route('profile.account', ['user_ref_id' => auth()->user()->ref_id]);

        } catch (\Stripe\Error\ApiConnection $e) {
           // Network problem, perhaps try again.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\InvalidRequest $e) {
            // You screwed up in your programming. Shouldn't happen!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Api $e) {
            // Stripe's servers are down!
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            \Session::flash('error', $error['message']);
            return redirect()->back();

        } catch (\Stripe\Error\Card $e) {
            // Card was declined.
            $e_json = $e->getJsonBody();
            $error = $e_json['error'];
            \Session::flash('error', $error['message']);
            return redirect()->back();
        }

    }

    public function deleteTutorContract(Request $request, $tutor_contract_id){
        $this->authorize('admin-only');

        $tutor_contract = TutorContract::find($tutor_contract_id);

        $tutor = User::find($tutor_contract->tutor_id);
        $student = User::find($tutor_contract->tutor_request->student_id);

        $tutor_contract->delete();

        \Session::flash('success', "Contract ID $tutor_contract->id between $student->full_name and $tutor->full_name in $tutor_contract->course.  Please alert the tutor, $tutor->first_name.");

        return redirect()->back();
    }
}
