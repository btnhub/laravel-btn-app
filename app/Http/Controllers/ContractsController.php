<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use PDF;
use DB;
use Carbon;
use App\Profile;
use App\User;
use App\TutorApplicant;

class ContractsController extends Controller
{
    public function signStudentContract()
    {
        $student = User::find(auth()->user()->id);
        $this->authorize('self-access', $student);

        DB::table('terms_of_use')->insert([
            'user_id' => $student->id,
            'version' => 'terms_of_use',
            'submission_date' => Carbon\Carbon::now()
            ]);

        return redirect()->route('profile.account', ['user_ref_id' => $student->ref_id]);
    }

    public function getTutorContract(Request $request)
    {
        $user = User::find(auth()->user()->id);

        if ($user->hasRole('tutor-applicant')) {
            $application = TutorApplicant::where('user_id',$user->id)->first() ?? $application = TutorApplicant::where('email',$user->email)->first();
            
            if($application)
            {
                //Update application in case no user_id is present
                $application->user_id = $user->id;
                $application->save();

                if($application->hired == 1)
                {
                    //\Session::flash('success', 'Review and sign below.');
                    //return view('btn.contracts.sign_contract');
                    return view('btn.pages.citybook.sign_contract');
                }
                else
                {
                    \Session::flash('error', 'You have not been hired.');
                    return redirect()->back();
                }
            }
            else
            {
                \Session::flash('error', 'We could not find your application.');
                return redirect()->back();
            }
        }
        
        elseif($user->isTutor() || $user->isSuperUser()){
            //return view('btn.contracts.sign_contract');
            return view('btn.pages.citybook.sign_contract');
        }

        else
            \Session::flash('error', 'You are not authorized to view this page');
            return redirect()->back();    
    }

    public function createTutorContract(Request $request)
    {
    	/*$this->validate($request, [
                'positions' => 'required'
                ]);*/
        
        $tutor = User::find(auth()->user()->id);

        $positions = $request->positions;
        $address = $request->address;
        $address_contract = str_replace(["\n\r", "\r\n", "\n", "\r"],', ', trim($address)); //replace line breaks with comma
        $tutor_legal_name = preg_replace('!\s+!', ' ', ucwords(strtolower($request->full_name))); // replace multiple spaces with one space
        
        //DOMPDF Ref: https://github.com/barryvdh/laravel-dompdf
		//Options 
		//		->stream() open in browser
		//		->save('path/file_name.pdf') save to server
		//		->download('filename.pdf') force download

        $contract_version = 'v20180529';
        $contract_date = Carbon\Carbon::now();
        $file_name = $contract_version."-".date('Ymd').'-'.$tutor->id.'-'.str_replace([" ", "-", ".", ","], "_", $tutor_legal_name);
    	$file_path = base_path("bufftutor/tutor_contracts/$file_name.pdf");
    	$pdf = PDF::loadView('btn.contracts.tutor_contract_signed', ['tutor_name' => $tutor_legal_name, 'address' => $address_contract, 'contract_date' => $contract_date]);

        //TEMP!  Use silence operator @ after upgrade to php 7.1
        //When package dompdf is upgraded, remove the silencors.
        //if ($pdf->save($file_path)){
        if (@$pdf->save($file_path)){
            DB::table('terms_of_use')->insert([
                'user_id' => $tutor->id,
                'version' => 'tutor_contract-'.$contract_version,
                'submission_date' => $contract_date
                ]);

    		//Update profile with tutor contract submission date
            $tutor->profile->tutor_contract = $contract_date;
            $tutor->profile->address = trim($address);
            $tutor->profile->save();

            //Update tutor's role from applicant to tutor/music-teacher
            /*foreach ($positions as $key => $position) {
                $tutor->addRole($position);
            }*/
            $tutor->addRole('tutor');
            $tutor->removeRole('tutor-applicant');
            
            //return $pdf->stream('bufftutor-contract.pdf');
            return @$pdf->stream('bufftutor-contract.pdf');
        }

        else 
            $request->session()->flash('error', 'Unable to save your contract, please try again.');
            return redirect()->back();
    }
}
