<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Tutor;
use App\Student;
use App\TutorSession;
use App\Location;
use App\Evaluation;
use App\MsgParticipant;
use App\MsgThread;
use App\Assignment;
use App\MarketTutorRequest;

use App\School;
use App\Department;
use App\Course;
use App\University;
use App\UniversityCourse;

use Carbon\Carbon;
use DB;
use Artisan;

use App\Jobs\SendGeneralNotification;
use App\Jobs\CreateSitemaps;

class AdminController extends Controller
{
	public function __construct() {
		$this->colors = [
	            1 => 'rgba(153, 102, 255, 0.2)',
	            2 => 'rgba(54, 162, 235, 0.2)',
	            3 => 'rgba(255, 206, 86, 0.2)',
	            4 => 'rgba(75, 192, 192, 0.2)',
	            5 => 'rgba(255, 99, 132, 0.2)',
	            6 => 'rgba(255, 159, 64, 0.2)',
	            //Repeat
	            7 => 'rgba(153, 102, 255, 0.2)',
	            8 => 'rgba(54, 162, 235, 0.2)',
	            9 => 'rgba(255, 206, 86, 0.2)',
	            10 => 'rgba(75, 192, 192, 0.2)',
	            11 => 'rgba(255, 99, 132, 0.2)',
	            12 => 'rgba(255, 159, 64, 0.2)'
	       ];

	    $border_colors = array();

	    foreach ($this->colors as $key => $color_code) {
	    	$border_code = str_replace("0.2", "1", $color_code);
	    	$this->border_colors[$key] = $border_code;
	    }
	}

	public function getEls(){
		$evaluations = Evaluation::with('tutor')->has('tutor')
									->where('created_at', '>', '2015-08-01')
									->whereNull('rating')
									->where('tutor_strength', '!=', '')
									->orderBy('created_at', 'DESC');
		$eval_count = $evaluations->count();

		$evaluations = $evaluations->limit(20)->get();

		return view('btn.pages.citybook.admin.add_stars', compact('evaluations', 'eval_count'));
	}

	public function saveStars(Request $request){
		$evaluation = Evaluation::find($request->eval_id);

		$evaluation->update(['rating' => $request->rating]);
	
		$msg = "$request->rating Stars For {$evaluation->tutor->user_name} (ID: $evaluation->id)";

		return response()->json(array('msg' => $msg), 200);
	}

	public function viewFailedJobs(){
		$failed_jobs = DB::table('failed_jobs')->orderBy('failed_at', 'desc')->get();

		return view('btn.admin.failed_jobs_list', compact('failed_jobs'));
	}

	public function restartFailedJob($job_id){
		
		Artisan::call('queue:retry', ['id' => [$job_id]]);

		\Session::flash('success', "Job #$job_id Restarted");
		return redirect()->back();
	}

	public function deleteFailedJob($job_id){

		Artisan::call('queue:forget', ['id' => $job_id]);
		
		\Session::flash('success', "Failed Job #$job_id Deleted.");
		return redirect()->back();
	}

	public function userManager()
	{
		$users = User::with('locations', 'roles')->orderBy('first_name', 'ASC')->paginate(10);
		/*$deleted_users = User::onlyTrashed()->orderBy('first_name', 'ASC')->paginate(10);*/
		$deleted_users = User::onlyTrashed()->orderBy('deleted_at', 'DESC')->get();
		return view('btn.admin.user_manager', compact('users', 'deleted_users'));
	}

	public function userDetails(Request $request, $user_id)
	{
		$user = User::find($user_id);
		
		$tutor = Tutor::with('tutorSessions.student', 'tutorSessions.assignment', 'assignments.student', 'refunds.student', 'evaluations', 'contractorPayments')->find($user_id);
		$student = Student::find($user_id);

		return view('btn.admin.user_details', compact('user', 'tutor', 'student'));
	}

	public function uploadAvatar(Request $request, $user_id)
	{
		$this->validate($request, ['image_file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

		$user = User::find($user_id);

		if ($request->hasFile('image_file')) {
            $image = $request->file('image_file');
            $new_filename = $user->ref_id . "-" . str_replace(" ", "_", $user->username) . "-" .Carbon::now()->format("Ymd") . "." . $image->getClientOriginalExtension();

            $file_move = $request->file('image_file')->move(public_path('btn/avatars'), $new_filename);
          
            $user->profile->avatar = $new_filename;
            $user->profile->save();
        }

        \Session::flash('success', 'Avatar Uploaded');
        return redirect()->back();
	}

	public function searchUsers(Request $request)
	{
		$search = $request->search;
		$searchValues = preg_split('/\s+/', $search); // split on 1+ whitespace
					
		$users = User::with('locations', 'roles')->where(function ($q) use ($searchValues) {
  						foreach ($searchValues as $value) {
    						$q->orWhere('first_name', 'like', "%{$value}%");
    						$q->orWhere('last_name', 'like', "%{$value}%");
    						$q->orWhere('email', 'like', "%{$value}%");
						}
					});
				
		$users = $users->orderBy('first_name', 'ASC')->paginate(10);
		$users->appends(['search' => $search]);

		$deleted_users = User::onlyTrashed()->orderBy('deleted_at', 'DESC')->get();

		return view('btn.admin.user_manager', compact('users', 'deleted_users'));
	}

	public function deleteUser(Request $request, $user_id)
	{
		$user = User::find($user_id);
		$name = $user->first_name ." " . $user->last_name;
		
		if(!$user->delete())
		{
			abort(500, "Could not delete $name");
		}

		\Session::flash('success', "$name Successfully Deleted!");

		return redirect()->back();
	}

	public function restoreUser(Request $request, $user_id)
	{
		
		$user = User::onlyTrashed()->find($user_id);
		$name = $user->first_name ." " . $user->last_name;
		
		if(!$user->restore())
		{
			abort(500, "Could not restore $name");
		}

		\Session::flash('success', "$name Successfully Restored!");

		return redirect()->back();
	}

	public function cloneUser($user_id)
	{
		auth()->loginUsingId($user_id);
		$user = auth()->user();
		\Session::flash('success', "Logged In As $user->full_name");
		
		return redirect()->route('profile.account', [$user->ref_id]);
	}

	public function analytics()
	{
       $colors = $this->colors;

       $border_colors = $this->border_colors;

       $years = date('Y') - 2012;

       $revenue = TutorSession::where('created_at', '>', Carbon::now()->subYears($years))
       							->where('tutor_id', '!=', 63)
       							->selectRAW('Year(`session_date`) as year, month(`created_at`) as month, sum(`amt_charged`) as charged, sum(`duration`) as session_count')
								->groupBy('year')
								->groupBy('month')
								->orderBy('year', 'DESC')
								->get();

		$months = range(1,12);
			
		foreach ($revenue as $rev) {
			// build array for chart
			$revenue_data[$rev->year][$rev->month]['charged'] = $rev->charged;
			$revenue_data[$rev->year][$rev->month]['session_count'] = $rev->session_count;
		}

		foreach ($revenue_data as $rev_year => $rev_months) {
			
			foreach ($months as $month) {
				
				if (!isset($rev_months[$month])) {
					//$revenue_data[$rev_year][$month] = 0;
					$revenue_data[$rev_year][$month]['charged'] = 0;
					$revenue_data[$rev_year][$month]['session_count'] = 0;
				}
			}
			ksort($revenue_data[$rev_year]);
		}

		$revenue_cumulative_array = [];

		foreach ($revenue_data as $rev_year => $rev_months) {
			$revenue_charged = 0;
			$revenue_count = 0;

			foreach ($rev_months as $rev_month => $rev) {
				$revenue_charged += $rev['charged'];
				$revenue_count += $rev['session_count'];
				$revenue_cumulative_array[$rev_year][$rev_month]['charged'] = $revenue_charged;
				$revenue_cumulative_array[$rev_year][$rev_month]['session_count'] = $revenue_count;

			}
		}
		
		$revenue = collect($revenue_data);	
		$revenue_cumulative = collect($revenue_cumulative_array);	
		
		$app_fees_data = DB::table('stripe_app_fees')->selectRAW('year(`created_at`) as year, week(`created_at`) as week, (sum(`application_fee`) - sum(`stripe_fee`)) as earned')
					->where('created_at', '>', Carbon::now()->subYears($years))
					->groupBy('year')
					->groupBy('week')
					->orderBy('year', 'DESC')
					->get();
		
		$assignments_data = DB::table('assignments')->selectRAW('year(`created_at`) as year, week(`created_at`) as week, count(0) as assign_count')
					->where('created_at', '>', Carbon::now()->subYears($years))
					->groupBy('year')
					->groupBy('week')
					->orderBy('year', 'DESC')
					->get();

		$tutor_requests_data = DB::table('market_tutor_requests')->selectRAW('year(`created_at`) as year, week(`created_at`) as week, count(0) as request_count')
					->where('created_at', '>', Carbon::now()->subYears($years))
					->groupBy('year')
					->groupBy('week')
					->orderBy('year', 'DESC')
					->get();

		$tutor_sessions_data = DB::table('tutor_sessions')->selectRAW('year(`created_at`) as year, week(`created_at`) as week, count(0) as session_count')
					->where('created_at', '>', Carbon::now()->subYears($years))
					->groupBy('year')
					->groupBy('week')
					->orderBy('year', 'DESC')
					->get();

		$weeks = range(0,53);
		
		//App Fees	
		foreach ($app_fees_data as $app_fee) {
			// build array for chart
			$app_fees_array[$app_fee->year][$app_fee->week]['earned'] = $app_fee->earned;
		}

		foreach ($app_fees_array as $app_fee_year => $app_fee_weeks) {
			
			foreach ($weeks as $week) {
				
				if (!isset($app_fee_weeks[$week])) {
					$app_fees_array[$app_fee_year][$week]['earned'] = 0;
				}
			}
			ksort($app_fees_array[$app_fee_year]);
		}

		//App Fees Cumulative
		$app_fees_cumulative_array = [];

		foreach ($app_fees_array as $app_fee_year => $app_fee_week) {
			$app_cumulative_earned = 0;
			foreach ($app_fee_week as $app_week => $app_earned_array) {
				$app_cumulative_earned += $app_earned_array['earned'];
				$app_fees_cumulative_array[$app_fee_year][$app_week]['earned'] = $app_cumulative_earned;
			}
		}
		
		//Assignments
		foreach ($assignments_data as $assignment) {
			// build array for chart
			$assignments_array[$assignment->year][$assignment->week]['assign_count'] = $assignment->assign_count;
		}

		foreach ($assignments_array as $assign_year => $assign_weeks) {
			
			foreach ($weeks as $week) {
				
				if (!isset($assign_weeks[$week])) {
					$assignments_array[$assign_year][$week]['assign_count'] = 0;
				}
			}
			ksort($assignments_array[$assign_year]);
		}

		//Tutor Requests
		foreach ($tutor_requests_data as $tutor_request) {
			// build array for chart
			$tutor_requests_array[$tutor_request->year][$tutor_request->week]['request_count'] = $tutor_request->request_count;
		}

		foreach ($tutor_requests_array as $request_year => $request_weeks) {
			
			foreach ($weeks as $week) {
				
				if (!isset($request_weeks[$week])) {
					$tutor_requests_array[$request_year][$week]['request_count'] = 0;
				}
			}
			ksort($tutor_requests_array[$request_year]);
		}

		//Tutor Sessions
		foreach ($tutor_sessions_data as $tutor_session) {
			// build array for chart
			$tutor_sessions_array[$tutor_session->year][$tutor_session->week]['session_count'] = $tutor_session->session_count;
		}

		foreach ($tutor_sessions_array as $session_year => $session_weeks) {
			
			foreach ($weeks as $week) {
				
				if (!isset($session_weeks[$week])) {
					$tutor_sessions_array[$session_year][$week]['session_count'] = 0;
				}
			}
			ksort($tutor_sessions_array[$session_year]);
		}


		$app_fees = collect($app_fees_array);
		$app_fees_cumulative = collect($app_fees_cumulative_array);
		$assignments = collect($assignments_array);
		$tutor_requests = collect($tutor_requests_array);
		$tutor_sessions = collect($tutor_sessions_array);

		return view('btn.admin.analytics.summary', compact('revenue', 'revenue_cumulative', 'app_fees', 'app_fees_cumulative', 'assignments', 'tutor_requests', 'tutor_sessions', 'colors', 'border_colors'));
	}

	public function analyticsUsers()
	{
		//New Registered Users
		for ($i=1; $i < 13; $i++) { 
			$registered_students[$i] = User::with('locations')->where('created_at', '>', date('Y'))
				->whereMonth('created_at', '=', $i)
				->whereHas('roles', function($query){ 
					$query->where('role', 'student'); })->get();

			$old_registered_students[$i] = User::with('locations')->where('created_at', '>', date('Y')-1)
				->where('created_at', '<', date('Y'))
				->whereMonth('created_at', '=', $i)
				->whereHas('roles', function($query){ 
					$query->where('role', 'student'); })->get();
		}

		for ($i=1; $i < 13; $i++) { 
			$registered_tutors[$i] = User::with('locations')->where('created_at', '>', date('Y'))
				->whereMonth('created_at', '=', $i)
				->whereHas('roles', function($query){ 
					$query->where('role', 'tutor-applicant'); })->get();
			
			$old_registered_tutors[$i] = User::with('locations')
				->where('created_at', '>', date('Y')-1)
				->where('created_at', '<', date('Y'))
				->whereMonth('created_at', '=', $i)
				->whereHas('roles', function($query){ 
					$query->where('role', 'tutor-applicant'); })->get();	
		}

		$locations = Location::has('active_tutors')->get();

		return view('btn.admin.analytics.summary_users', compact('registered_students', 'registered_tutors', 'old_registered_students', 'old_registered_tutors', 'locations'));
	}

	public function analyticsTutors()
	{
       $active_tutors = Tutor::activeTutors()
       							->with(['locations'])
       							->get()
       							/*->merge(Tutor::whereHas('assignments', function($query){
							       			$query->active();
       								})->get())*/
       							->merge(Tutor::has('active_assignments')->withCount(['active_assignments'])->get())
       							->sortByDesc('active_assignments_count')
       							/*->sortBy('fullName')*/;

		return view('btn.admin.analytics.summary_tutors', compact('active_tutors'));
	}

	public function showEvaluations(){
		$active_tutors = Tutor::activeTutors()->get();
		
		$evaluations = Evaluation::where('created_at', '>', '2016-01-01')->whereNull('review_date')->get();
		return view('btn.admin.evaluations.list_evaluations', compact('evaluations', 'active_tutors'));
	}

	public function reviewEvaluation(Request $request, $eval_id) {
		// mark evaluation as reviewed
		// add problems to the problem_table <-- if one is created
		$evaluation = Evaluation::find($eval_id);
		$evaluation->homepage_visible = $request->homepage_visible;
		$evaluation->review_date = Carbon::now();
		
		$evaluation->save();

		\Session::flash('success', "Evaluation Reviewed!");
		return redirect()->back();
	}

	/**
	 * List all assignments without desired evaluation
	 * and assignments that have not had reminders sent
	 * @return [type] [description]
	 */
	public function evalReminder(Request $request = null, $type) {
		//Default Start & End Dates
		$start_date = Carbon::now()->subMonths(5);
		$end_date = Carbon::now();

		if ($request->start_date) {
			$start_date = Carbon::parse($request->start_date);
		}
		if ($request->end_date) {
			$end_date = Carbon::parse($request->end_date);
		}		

		$assignments = Assignment::where('start_date', '>', $start_date)
									->where('end_date', '<', $end_date)
									->whereDoesntHave('evaluation', function($query)
										{ 
											$query->where('type', 'like', 'End'); 
										});

		if ($type == 'End') {
			$assignments = $assignments->where('eval_end', 0)
									->get()
									->filter(function ($assign)
										{
											return $assign->tutorSessions->count() >= 3;
										});
			return view('btn.admin.evaluations.list_eval_reminder_end', compact('assignments', 'start_date', 'end_date'));
		}
		else {
			$assignments = $assignments->where('eval_first', 0)
									->whereDoesntHave('evaluation', function($query)
										{ 
											$query->where('type', 'like', 'First'); 
										})
									->get()
									->filter(function ($assign)
										{
											return $assign->tutorSessions->count() < 3 && $assign->tutorSessions->count() > 0;
										});
			return view('btn.admin.evaluations.list_eval_reminder_first', compact('assignments', 'start_date', 'end_date'));
		}	
	}

	/**
	 * Send Evaluation Reminders
	 * Required Form Element: Array of assign_ids 
	 * Example: ({!! Form::hidden('assign_ids[]', $assign->id) !!})
	 * @param  Request $request [description]
	 * @param  string  $type    End Semester Vs. First Session Evaluatoin
	 * @return [type]           [description]
	 */
	public function sendEvalReminders(Request $request, $type)
	{
		$assign_ids = $request->assign_ids;

		/*if ($type == 'End') {
			//Remove duplicate students
			$assign_ids = Assignment::whereIn('id', $assign_ids)->get()->unique('student_id')->pluck('id');
		}*/
		foreach ($assign_ids as $assign_id) {
			$assignment = Assignment::find($assign_id);	

			if ($type == 'End') {
				//Send Email for End Of Semester Eval Reminder
				//Create Student Email
		        $user = User::find($assignment->student_id);
		        $location = $assignment->location;
		        $subject = "Please Evaluate {$assignment->tutor->first_name}'s Performance";
		        $email_view = 'btn.emails.students.eval_reminder_end';
		        $email_data = ['assignment' => $assignment];
		        
		        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));

				//Update Assignment
				$assignment->eval_end = 1;
				$assignment->save();
				
			}
			elseif($type == 'First'){
				//Send Email for End Of Semester Eval Reminder
				//Create Student Email
		        $user = User::find($assignment->student_id);
		        $location = $assignment->location;
		        $subject = "Please Evaluate Your First Session With {$assignment->tutor->first_name}";
		        $email_view = 'btn.emails.students.eval_reminder_first';
		        $email_data = ['assignment' => $assignment];
		        
		        $this->dispatch(new SendGeneralNotification($user, $location, $subject, $email_view, $email_data));
				
				//Update Assignment
				$assignment->eval_first = 1;
				$assignment->save();	
			}
		}

		\Session::flash('success', "$type Evaluation Reminder Emails Sent");

		return redirect()->back();
	}

	/**
	 * Method to mark assignment as student has been reminded
	 * to prevent auto reminder from being sent to student
	 * @param  [type] $assign_id [description]
	 * @param  [type] $type      [description]
	 * @return [type]            [description]
	 */
	public function markAsReminded($assign_id, $type) {
		$assignment = Assignment::find($assign_id);
		
		if ($type == 'End') {
			$assignment->eval_end = 1;
			$assignment->save();
		}
		elseif($type == 'First'){
			$assignment->eval_first = 1;
			$assignment->save();	
		}

		\Session::flash('success', "Assignment $assignment->id between {$assignment->tutor->fullName} and {$assignment->student->fullName} Updated, Reminder Email Was NOT sent");

		return redirect()->back();
	}

	//TEMPORARY - find students who have used the Inbox, but without matches or have assignments and no first session eval
	public function firstEvalStudents(){
		//Students who were in contact with tutors through the Inbox, but no assignment
		$inbox_students = [];
		$threads = MsgThread::where('created_at', '>', '2017-01-01')
							->get()
							->filter(function($thread){
								if ($thread->tutor_request && is_null($thread->tutor_request->matched_tutor_id) && $thread->tutor_request->requested_tutor_id != 63) {
									//Direct tutor request with no matches
									return true;
								}
								elseif($thread->tutor_proposal && $thread->tutor_proposal->tutor_request && is_null($thread->tutor_proposal->tutor_request->matched_tutor_id) && $thread->tutor_proposal->tutor_request->requested_tutor_id != 63){
									//General requests with no matches
									return true;

								}
								
							});

		foreach ($threads as $thread) {
			if ($thread->tutor_request) {
				//direct tutor requests without tutor matched
				$course = $thread->tutor_request->course;
			}
			elseif($thread->tutor_proposal){
				
				//General requests with proposals but no without tutor matched
				$course = $thread->tutor_proposal->tutor_request->course;
			}

			foreach ($thread->participants as $participant) {
				$user = $participant->user;

				if ($user && $user->isStudent()) {
					$other_participant = MsgParticipant::where('thread_id', $thread->id)
								->where('user_id', '!=', $user->id)
								->first();

					if ($other_participant && $other_participant->user) {
						//check if user exists and account has not been deleted
						$tutor = $other_participant->user;

						if (array_key_exists($participant->user->id, $inbox_students)) {
							
							array_push($inbox_students[$participant->user->id], "$tutor->full_name - $course");
							
							// remove duplicate entries
							$inbox_students[$participant->user->id] = array_unique($inbox_students[$participant->user->id]);
						}					
						else {
							$inbox_students[$participant->user->id] = array("$tutor->full_name - $course");
						}
					}
				}		
			}
		}
		
		//Students who have (not) submitted a first session evaluation
		$unevaluated_assignments =  Assignment::where('created_at', '>', '2017-01-01')
							->get()
							->filter(function ($assignment){
								if ($assignment->evaluation->count() == 0 && $assignment->tutorSessions->count() < 3) {
										return true;
									}
							});
		
		$students = [];
		foreach ($unevaluated_assignments as $assign) {
				if ($assign->student && $assign->tutor) {
					if (array_key_exists($assign->student->id, $students)) {
						array_push($students[$assign->student->id], "{$assign->tutor->full_name} - $assign->course");

						//remove duplicate entries
						$students[$assign->student->id] =array_unique($students[$assign->student->id]);
					}
					else
						$students[$assign->student->id] = array("{$assign->tutor->full_name} - $assign->course");
				}
				
		}

		//Merge arrays
		foreach ($inbox_students as $student_id => $details) {
			if (array_key_exists($student_id, $students)) {
				foreach ($details as $tutor_course) {
					array_push($students[$student_id], $tutor_course);

					//remove duplicates
					$students[$student_id] = array_unique($students[$student_id]);
				}
				
			}
			else
				$students[$student_id] = $details;
		}
		
		return view('btn.admin.evaluations.first_session_eval_students', compact('students'));

	}

	public function marketingBreakdown(){
		//$marketing_methods = DB::select('select method from marketing group by method');
		$marketing_methods = DB::table('marketing')->select('method')->groupBy('method')->pluck('method');
		
		$student_data = DB::select('select Year(`created_at`) as year, month(`created_at`) as month, method, count(0) as count from marketing where role = "student" group by year, month, method order by year DESC');

		$tutor_data = DB::select('select Year(`created_at`) as year, month(`created_at`) as month, method, count(0) as count from marketing where role = "tutor-applicant" group by year, month, method order by year DESC');
		
		$colors = $this->colors;
	    $border_colors = $this->border_colors;
	    
		$months = range(1,12);

		//Student Data
		foreach ($student_data as $stud) {
			// build array for chart
			$marketing_student[$stud->year][$stud->month][$stud->method] = $stud->count;
		}

		foreach ($marketing_student as $stud_year => $stud_months) {
			
			foreach ($months as $month) {
				if (!isset($stud_months[$month])) {
					foreach ($marketing_methods as $method) {
						$marketing_student[$stud_year][$month][$method] = 0;
					}
				}
				else {
					foreach ($marketing_methods as $method) {
						if (!isset($marketing_student[$stud_year][$month][$method])) {
							$marketing_student[$stud_year][$month][$method] = 0;
						}
					}
				}
			}
			ksort($marketing_student[$stud_year]);
		}

		//Tutor Data
		foreach ($tutor_data as $tutor) {
			// build array for chart
			$marketing_tutor[$tutor->year][$tutor->month][$tutor->method] = $tutor->count;
		}

		foreach ($marketing_tutor as $tutor_year => $tutor_months) {
			
			foreach ($months as $month) {
				if (!isset($tutor_months[$month])) {
					foreach ($marketing_methods as $method) {
						$marketing_tutor[$tutor_year][$month][$method] = 0;
					}
				}
				else {
					foreach ($marketing_methods as $method) {
						if (!isset($marketing_tutor[$tutor_year][$month][$method])) {
							$marketing_tutor[$tutor_year][$month][$method] = 0;
						}
					}
				}
			}
			ksort($marketing_tutor[$tutor_year]);
		}
		
		$student_data = collect($marketing_student);
		$tutor_data = collect($marketing_tutor);

		return view('btn.admin.marketing.summary', compact('marketing_methods', 'student_data', 'tutor_data', 'colors' ,'border_colors'));
	}

	public function updateUserLocation(Request $request, $user_id){
		$user = User::find($user_id);

		$user->update(['city' => $request->city,
						'region' => $request->region,
						'country' => $request->country,
						'postal_code' => $request->postal_code,
						'lat' => $request->lat,
						'lng' => $request->lng]);

		$location = $user->nearest_btn_location();
		$user->locations()->sync([$location->id]);


		\Session::flash('success', "Updated $user->full_name's location");

		return redirect()->back();
	}

	public function updateRequestLocation(Request $request, $tutor_request_id){
		$tutor_request = MarketTutorRequest::find($tutor_request_id);

		$tutor_request->update(['city' => $request->city,
						'region' => $request->region,
						'country' => $request->country,
						'postal_code' => $request->postal_code,
						'lat' => $request->lat,
						'lng' => $request->lng]);

		$location = $tutor_request->nearest_btn_location();

		$tutor_request->update(['location_id' => $location->id,
						'location' => $location->city]);

		\Session::flash('success', "Updated {$tutor_request->student->full_name}'s request location (#$tutor_request->id), $tutor_request->location, $tutor_request->location_id");

		return redirect()->back();
	}

	public function usersWithoutLocation(){
		$location_id = Location::where('city', 'Other')->first()->id;

	    $users = User::with(['locations', 'roles'])
	    					->whereNotNull('lat')
	    					->whereHas('locations', function($q) use($location_id){
	    								$q->where('location_id', $location_id);
	    						})
	    					->orderBy('id', 'desc')
	    					->select('id', 'first_name', 'last_name', 'city', 'region', 'lat', 'lng')
	    					->limit(100)
	    					->get()
	    					->filter(function($user){
	    						return $user->nearest_btn_location()->city != "Other";
	    					});

	    return view('btn.pages.citybook.fix_locations', compact('users'));
	}

	public function fixUsersLocation(Request $request){
		foreach ($request->user_ids as $key => $user_id) {
			$user = User::find($user_id);
			$location = $user->nearest_btn_location();
			$user->locations()->sync([$location->id]);
		}

		\Session::flash('success', 'Users Location Added');
		return redirect()->back();
	}

	public function createNewBTNLocation(Request $request){
		$this->validate($request, ['lat' => 'required']);
		
		$state = explode(", ", $request->formatted_address)[1];

		if ($existing_location = Location::where('city', $request->city)->where('state', $state)->first()) {
			\Session::flash('info', "BTN Location Exists: ID: $existing_location->id $request->city, $state");
			return redirect()->back();
		}
		
		//Get Timezone
		$url = "https://maps.googleapis.com/maps/api/timezone/json?location=$request->lat,$request->lng&timestamp=".time()."&key=".config('services.google_maps.key');
		$json_timezone = file_get_contents($url);
		$timezone = json_decode($json_timezone, true)['timeZoneId'];

		$request->request->add(['state' => $state, 'timezone' => $timezone]);

		$new_location = Location::create($request->except('_token','location', 'formatted_address'));

		$new_location->company_name = "BuffTutor";
		$new_location->email = "Contact@BuffTutor.com";
		$new_location->phone = "765-543-7542";
		$new_location->virtual_phone = "13036320560";
		$new_location->website = "www.BuffTutor.com";
		$new_location->tutors_visible = 1;
		$new_location->save();

		\Session::flash('success', "New Location Created: $new_location->city, $new_location->state.  Ping Google Jobs using link below.");

		return redirect()->back();

	}

	public function addNewUniversity(Request $request){
		$this->validate($request, ['lat' => 'required']);

		if ($existing_uni = University::where('google_place_id', $request->google_place_id)->first()) {
			\Session::flash('info', "University Exists: ID: $existing_uni->id $request->name");
			return redirect()->back();
		}

		$new_uni = University::create($request->except('_token','school-input'));
		$new_uni->location_id = $new_uni->nearest_btn_location()->id;
		$new_uni->save();

		\Session::flash('success', "New Uni Added: $new_uni->name. Create new sitemap & Ping Google using links below.");

		return redirect()->back();
	}

	public function createSitemaps(Request $request){
		$this->dispatch(new CreateSitemaps());

		\Session::flash('success', "CreateSitemaps Job Initiated (includes pinging webmasters)");

		return redirect()->back();
	}

	public function listTutors(Request $request){
		//find tutors within a specified time period
		
		$date = Carbon::now()->subMonths(5);
		$cities = "";
		$location_ids = [];

		if ($request->date) {
			$date = Carbon::parse($request->date);
		}

		$locations = Location::has('tutors')->get()->pluck('city', 'id')->toArray();

		$tutors = Tutor::with('profile', 'roles')
							->whereHas('profile', function ($query) use($date) {
                            	$query->where('tutor_start_date', '<=', $date)
                                    ->where('tutor_end_date','>=', $date)
                                    ->whereNotNull('tutor_contract')
                                    ->whereNotNull('background_check');
                            });
		
		if ($request->location_ids) {
			$location_ids = $request->location_ids;
			$tutors = $tutors->whereHas('locations', function($q) use($location_ids){
				$q->whereIn('location_id', $location_ids);
			});	

			$cities_array = [];

			foreach ($request->location_ids as $location_id) {
				array_push($cities_array, $locations[$location_id]);
			}

			$cities = implode(', ', $cities_array);
			$location_ids = [4];
		}

		$tutors = $tutors->get();
		$tutors = $tutors->sortBy(function ($tutor){
							return $tutor->fullName;
							});
		//$email_body = DB::table('settings')......
		
        return view('btn.admin.bulk_emails.list_tutors', compact('date', 'locations', 'location_ids','cities','tutors'));
        
	}

	public function bulkEmailUsers(Request $request){
		//TODO!!!!!
		dd($request->request);
		$users = User::whereIn('id', $request->tutor_ids)->get();
		$email_body = $request->email_body;
		//TO DO: Set up job to send emails
	}

	public function listCourses(){
		$schools = School::with('departments', 'courses')->get();

		return view('btn.pages.citybook.admin.list_courses', compact('schools'));
	}

	public function uploadCourses(Request $request){
		if(!Course::where('name', 'Other')->count()){
			Course::create(['ref_id' => 999999999,
							'name' => "Other",
							'number' => "Other",
							'description' => 'User defined course (e.g. non-college course, standardized test, general help etc)'
							]);
		}

		$school_name = $request->school_name;
		$school = School::where('name', $school_name)->first();
		$school_id = $school->id;

		$dept_list_url = $request->dept_list_url;
		$regex_dept = $request->regex_dept;
		$regex_course_code = $request->regex_course_code;
		$regex_course_title = $request->regex_course_title;
		$regex_course_description = $request->regex_course_description;

		$dept_page = file_get_contents($dept_list_url);
		preg_match_all($regex_dept, $dept_page, $departments); 

		$dept_array = [];
		$dept_subdomains = [];
		$dept_count = 0;
		$course_count = 0;

		for ($i=0; $i < count($departments[0]); $i++) { 
			$subdomain = $departments[1][$i];
			$dept_code = strtoupper($subdomain);
			$course_catalog = $dept_list_url.$subdomain;		
			
			$department = Department::firstOrCreate(['school_id' => $school_id,
												'name' => trim(explode("(", $departments[2][$i])[0]),
												'code' => $dept_code,
												'course_catalog' => $course_catalog,
											]);
			$dept_count++;

			//Get Courses
			$course_page = file_get_contents($course_catalog);
			preg_match_all($regex_course_code, $course_page, $course_codes);
			preg_match_all($regex_course_title, $course_page, $course_titles);
			
			preg_match_all($regex_course_description, $course_page, $course_descriptions);
			
			for ($j=0; $j < count($course_codes[0]); $j++) { 
				if (!$course = Course::where('school_id', $school_id)
										->where('number', $course_codes[1][$j])->first() ) 
				{
					$course->ref_id = strtoupper(md5(rand()));
					$course->department_id = $department->id;
					$course->description = trim($course_descriptions[1][$j]);
					$course->name = trim(explode(") ", $course_titles[1][$j])[1]);

					$course->save();
				}
				
				$course_count++;
			}
		}

		\Session::flash('success', "$dept_count Departments & $course_count Courses Added");
		return redirect()->back();	
	}

	public function testRegex(Request $request){
		$dept_list_url = $request->dept_list_url;
		$regex_dept = $request->regex_dept;
		$regex_course_code = $request->regex_course_code;
		$regex_course_title = $request->regex_course_title;
		$regex_course_description = $request->regex_course_description;

		$dept_page = file_get_contents($dept_list_url);
		preg_match_all($regex_dept, $dept_page, $departments); 

		$dept_array = [];
		$dept_subdomains = [];

		//for ($i=0; $i < count($departments[0]); $i++) { 
		for ($i=0; $i < 3; $i++) { 
			$subdomain = $departments[1][$i];
			$dept_code = strtoupper($subdomain);
			$course_catalog = $dept_list_url.$subdomain;		
			
			//Get Courses
			//CU
			$course_page = file_get_contents($course_catalog);
			preg_match_all($regex_course_code, $course_page, $course_codes);
			preg_match_all($regex_course_title, $course_page, $course_titles);
			return response()->json(array('regex_results' => $course_titles[1]), 200);
			preg_match_all($regex_course_description, $course_page, $course_descriptions);
			
			$courses_array = [];
			for ($j=0; $j < count($course_codes[0]); $j++) { 
				$courses = (object)['number' => $course_codes[1][$j],
									'name' => trim(explode(") ", $course_titles[1][$j])[1]),
									'description' => trim($course_descriptions[1][$j])
									];	
				array_push($courses_array, $courses);
			}
			$dept = (object)['course_catalog' => $course_catalog,
								'code' => $dept_code,
								'name' => trim(explode("(", $departments[2][$i])[0]),
								'courses' => $courses_array
							];
			array_push($dept_array, $dept);
		}

		$regex_results = $dept_array;
		
		return response()->json(array('regex_results' => $regex_results), 200);
	}
}
