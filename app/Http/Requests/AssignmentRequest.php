<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AssignmentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //CHANGE!  ADD middleware or Gate to determine who can submit the form
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'start_date' => 'required',
            'end_date' => 'required',
            'location_id' => 'required|numeric',
            'course' => 'required',
            
        ];
    }
}
