<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Favorite;

class SaveFavoriteTutors
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        if (session()->has('fav_tutors')) {
            $user = $event->user;

            $favorites = Favorite::where('identifier', session('identifier'))->get();
            $existing_fav = Favorite::where('user_id', $user->id)->get();

            if ($existing_fav->count()) {
                //prevent duplicates
                foreach ($favorites as $favorite) {
                    if (!$existing_fav->contains('tutor_id', $favorite->tutor_id)) {
                        $favorite->user_id = $user->id;
                        $favorite->save();
                    }
                    else{
                        $favorite->delete();
                    }
                }    
            }
            else{
                $favorites->each(function($fav) use($user){
                    $fav->user_id = $user->id;
                    $fav->save();
                });
            }

            //reset session
            session()->forget('fav_tutors');
        }
    }
}
