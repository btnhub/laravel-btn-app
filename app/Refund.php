<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Refund extends Model
{
	use SoftDeletes;
	protected $guarded = [
        'id'
    ];

    /**
    * To allow soft deletes
    */  
   	protected $dates = ['deleted_at'];
   	
   	public function session(){
        return $this->belongsTo('App\TutorSession', 'session_id');
    }

    public function assignment(){
        return $this->belongsTo('App\Assignment', 'assignment_id');
    }

    public function student(){
        return $this->belongsTo('App\Student', 'student_id');
    }

    public function tutor(){
        return $this->belongsTo('App\Tutor', 'tutor_id');
    }

}
