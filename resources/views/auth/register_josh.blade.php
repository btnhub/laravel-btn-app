@extends('layouts.josh-front.default', ['page_title' => "Create An Account"])

@section('title')
    Registration
@stop

@section('meta-description')
Create a BuffTutor account today!
@stop

@section('content')
	<div style="min-height:500px">
    	@include('btn.forms.register', ['action' => url('/register')])
    </div>
@endsection
