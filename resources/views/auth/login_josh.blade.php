@extends('layouts.josh-front.default', ['page_title' => "Login"])

@section('title')
    Login
@stop

@section('meta-description')
Log into your BuffTutor account.
@stop

@section('content')
    <div style="min-height:500px">
    	@include('btn.forms.login', ['action' => url('/login')])
    </div>
@endsection
