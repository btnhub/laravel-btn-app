@extends('layouts.citybook.default')

@section('title')
Login - BuffTutor
@endsection

@section('meta-description')
	Login to your BuffTutor account
@endsection

@section('content')
	<section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
 			<div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- profile-edit-container--> 
                    <div id="qualification" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>Login</h4>
                            {{-- <p>Don't have an account? <a href="{{url('/register')}}">Create An Account</a> </p> --}}
                        </div>
						<div class="custom-form">
					        {!! Form::open(['method' => 'POST', 'url' => 'login']) !!}
					        	<label>Email Address * <i class="fa fa-envelope"></i> <small class="text-danger" style="color:red;text-align: left">{{ $errors->first('email') }}</small></label>
					        	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

					        	    {!! Form::email('email', null, ['placeholder' => 'Email Address', 'autofocus' => 'autofocus', 'onclick' => "this.select()"]) !!}
					        	</div>
					            <label>Password * <small class="text-danger" style="color:red">{{ $errors->first('password') }}</small><i class="fa fa-eye"></i></label>
					            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					                {!! Form::password('password', ['placeholder' => "Password"]) !!}
					            </div>	
					            <button type="submit"  class="log-submit-btn"><span>Log In</span></button>
					            <div class="clearfix"></div>
					        {!! Form::close() !!}
					        <div class="lost_password">
					            <a href="{{ url('/password/reset') }}">Lost Your Password?</a>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection