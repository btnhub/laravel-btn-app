@extends('layouts.citybook.default')

@section('title')
Reset Password 
@endsection

@section('content')
	<section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
 			<div class="row">
                <div class="col-sm-8 col-md-offset-2">
                    <!-- profile-edit-container--> 
                    <div id="qualification" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>Reset Password</h4>
                            <div class="row">
	                        	<div class="col-md-12">
	                        		@foreach ($errors->all() as $error)
									    <p style="color:red">{{ $error }}</p>
									@endforeach
	                        	</div>
	                        </div>
                        </div>
						<div class="custom-form">
					        {!! Form::open(['method' => 'POST', 'url' => '/password/reset']) !!}
					        	
					        	<input type="hidden" name="token" value="{{ $token }}">
					        	
					        	<div class="row">
					        		<div class="col-md-12">
					        			<label>Email Address * <i class="fa fa-envelope"></i> <small class="text-danger" style="color:red">{{ $errors->first('email') }}</small></label>
					        			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					        			    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email Address']) !!}
					        			</div>
					        		</div>
					        	</div>
					        	<div class="row">
					        		<div class="col-md-6">
					        			<label >Password * <i class="fa fa-eye"></i> <small class="text-danger" style="color:red">{{ $errors->first('password') }}</small></label>
                                		<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                		    {!! Form::password('password', ["placeholder" => "Password"]) !!}
                                		</div>	
					        		</div>
					        		<div class="col-md-6">
					        			<label >Confirm Password * <i class="fa fa-eye"></i> <small class="text-danger" style="color:red">{{ $errors->first('password_confirmation') }}</small></label>
					        			<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
					        			    {!! Form::password('password_confirmation', ["placeholder" => "Re-enter Password"]) !!}
					        			</div>
					        		</div>
					        	</div>
                                <button type="submit" class="log-submit-btn"  ><span>Reset Password</span></button>
					        {!! Form::close() !!}
					    </div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection