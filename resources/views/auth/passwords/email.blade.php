@extends('layouts.citybook.default')

@section('title')
Reset Password
@endsection

@section('content')
	<section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
 			<div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <!-- profile-edit-container--> 
                    <div id="qualification" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>Reset Password</h4>
                            @if (session('status'))
		                        <div class="alert alert-success" style="color:green;font-weight:bold">
		                            {{ session('status') }}
		                        </div>
		                    @endif
                        </div>
						<div class="custom-form">
					        {!! Form::open(['method' => 'POST', 'url' => '/password/email']) !!}
					        	<label>Email Address * <i class="fa fa-envelope"></i> <small class="text-danger" style="color:red;text-align: left">{{ $errors->first('email') }}</small></label>
					        	<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

					        	    {!! Form::email('email', null, ['placeholder' => 'Email Address', 'autofocus' => 'autofocus', 'onclick' => "this.select()"]) !!}
					        	</div>	
					            <button type="submit"  class="log-submit-btn"><span>Send Password Reset Link</span></button>
					            <div class="clearfix"></div>
					        {!! Form::close() !!}
					    </div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection