@extends('layouts.josh-front.default', ['page_title' => "My Inbox"])

@section('title')
    Inbox 
@stop
@section('content')
    @if (Session::has('error_message'))
        <div class="alert alert-danger" role="alert">
            {!! Session::get('error_message') !!}
        </div>
    @endif
    
    {{$threads->count()}} Messages
    
    @if (Auth::user()->hasRole('super-user'))
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Thread ID</th>
                                <th>Request / Proposal ID</th>
                                <th>Participants</th>
                                <th></th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Sent</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($threads->count() > 0)
                            @foreach($threads as $thread)
                                @if ($thread->tutor_request || $thread->tutor_proposal)
                                    {{--Check if thread has an existing/undeleted tutor request or proposal --}}
                                    <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
                                    <tr class="media alert {!!$class!!}" onclick="location.href='{{route('messages.show', ['id' => $thread->id, 'user_ref_id' => Auth::user()->ref_id]) }}'" style="cursor: pointer;">
                                        <td> {{$thread->id ?? ''}} </td>
                                        <td> 
                                            @if ($thread->tutor_request)
                                                <b>Req ID:</b> {{$thread->tutor_request->id ?? ''}} <br>
                                            @elseif ($thread->tutor_proposal)
                                                <b>Req ID:</b> {{$thread->tutor_proposal->tutor_request->id ?? ''}} <br>
                                                <b>Prop ID: </b>
                                                {{$thread->tutor_proposal->id ?? ''}} <br>
                                            @endif
                                             </td>
                                        <td>{{ $thread->participantsString(Auth::id()) }}</td>
                                        <td>
                                            @if ((($thread->tutor_request->urgency ?? null) && $thread->tutor_request->urgency == 'High') || (($thread->tutor_proposal->tutor_request->urgency ?? null) && $thread->tutor_proposal->tutor_request->urgency == "High"))
                                                <span class="label label-danger">Urgent</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $thread->subject }}
                                        </td>
                                        <td> 
                                            @if (strlen($thread->latestMessage->body) > 100)
                                                {{ substr($thread->latestMessage->body, 0, 100) }} ...
                                            @else
                                                {{ $thread->latestMessage->body }}
                                            @endif
                                        </td>
                                        <td>{{ $thread->updated_at->format('m/d/Y h:i:sa') }}</td>
                                        <td>
                                            {!! Form::open(['method' => 'DELETE', 'route' => ['message.delete', $thread->id], 'class' => 'form-horizontal', 'id' => "delete_$thread->id"]) !!}
                                            
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <span class="glyphicon glyphicon-trash"></span>
                                                </button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No messages.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            
            </div>  
        </div>  
    @endif
    
    
@stop