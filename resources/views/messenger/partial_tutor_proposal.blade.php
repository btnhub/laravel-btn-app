<div class="row">
    <h3><b>Tutor's Proposed Rate: <span style="color:darkred">${{$thread->tutor_proposal->proposed_rate}}/hr</span></b></h3>
</div>

@if ($thread->tutor_proposal->accepted_proposal == 0)
    <p><b>Since a formal assignment has not been made yet, we have not shared your contact information. </b></p>

    <p>Use our messaging platform to contact each other and should you decide to work together, <b>{{$thread->tutor_proposal->student->first_name}}</b> will need to click the <b>Make Assignment</b> button to make a formal match. You will then have access to each other's contact information (under My Account) and you'll be able to use our online tools to keep track of all session payments, charges and refunds.</p>

    @if (Auth::user()->isStudent())
            <hr>
            @include('messenger.partials.negotiating_tips')
        
        <br><br>
    @endif

@else
    <p><b>A match has been made for this tutor request.  You may find each other's contact information under My Account. </b></p>
@endunless

<hr>

<table class="table">
    <tbody>
        <tr>
            <td><b>Student:</b></td>
            <td>{{$thread->tutor_proposal->student->user_name}}</td>
        </tr>
        <tr>
            <td><b>Tutor:</b></td>
            <td>
                <a href="{{ route('profile.index', $thread->tutor_proposal->tutor->ref_id)}}" target="_blank">
                    {{$thread->tutor_proposal->tutor->user_name}}'s Profile
                </a>
            </td>
        </tr>
        <tr>
            <td><b>Course:</b></td>
            <td>{{$thread->tutor_proposal->tutor_request->course}}</td>
        </tr>
        <tr>
            <td><b>Start Date:</b></td>
            <td>{{$thread->tutor_proposal->tutor_request->start_date}}</td>
        </tr>
        <tr>
            <td><b>End Date:</b></td>
            <td>{{$thread->tutor_proposal->tutor_request->end_date}}</td>
        </tr>
        <tr>
            <td><b>Location:</b></td>
            <td>{{$thread->tutor_proposal->location->city}}</td>
        </tr>
        <tr>
            <td><b>Session Time:</b></td>
            <td>{{$thread->tutor_proposal->tutor_request->session_time}}</td>
        </tr>
        <tr>
            <td><b>Frequency:</b></td>
            <td>{{$thread->tutor_proposal->tutor_request->frequency}}</td>
        </tr>
        <tr>
            <td><b>Max Rate:</b></td>
            <td>${{$thread->tutor_proposal->tutor_request->max_rate}}/hr</td>
        </tr>
       @if ($thread->tutor_proposal->tutor_request->urgency == "High")
        <tr>
            <td><b>Urgency</b></td>
            <td>
                <span style="color:red">{{$thread->tutor_proposal->tutor_request->urgency}} - Student must meet in less than 2 days</span>      
            </td>
        </tr>
    @endif
        <tr>
            <td><b>Notes / Concerns:</b></td>
            <td>
                @if ($thread->tutor_proposal->tutor_request->scholarship_program)
                    Scholarship Program: {{$thread->tutor_proposal->tutor_request->scholarship_program}}<br><br>        
                @endif    
                {{$thread->tutor_proposal->tutor_request->concerns}}
            </td>
        </tr>
    </tbody>
</table>

@if ($thread->tutor_proposal->accepted_proposal != 0)
    <button class="btn btn-info pull-right" disabled>Assignment Created</button> 
@elseif ($thread->tutor_proposal->student->id == Auth::user()->id)
    <a href="{{route('marketplace.accept_proposal', ['thread_id' =>$thread->id])}}"><button type="button" class="btn btn-primary pull-right"> 
        Make Assignment
    </button></a>

@else 
    Only {{$thread->tutor_proposal->student->user_name}} can click this button.
    <button class="btn btn-success pull-right" disabled>Make Assignment</button> 
@endif
