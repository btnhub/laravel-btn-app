@unless ($thread->tutor_request->matched_tutor_id == $thread->tutor_request->requested_tutor->id)
    <p><b>Since a formal assignment has not been made yet, we have not shared your contact information. </b></p>

    <p>Use our messaging platform to contact each other and should you decide to work together, <b>{{$thread->tutor_request->requested_tutor->user_name}}</b> will need to click the <b>Make Assignment</b> button to make a formal match. You will then have access to each other's contact information (under My Account) and you'll be able to use our online tools to keep track of all session payments, charges and refunds.</p>

    @if (Auth::user()->isStudent())
            <hr>
            @include('messenger.partials.negotiating_tips')
        
        <br><br>
    @endif
@else
    <p><b>A match has been made for this tutor request.  You may find each other's contact information under My Account. </b></p>
@endunless

<hr>

<table class="table">
    <tbody>
        <tr>
            <td><b>Student:</b></td>
            <td>{{$thread->tutor_request->student->user_name}}</td>
        </tr>
        <tr>
            <td><b>Tutor:</b></td>
            <td>
                <a href="{{ route('profile.index', $thread->tutor_request->requested_tutor->ref_id)}}" target="_blank">
                    {{$thread->tutor_request->requested_tutor->user_name}}'s Profile
                </a> 
            </td>
        </tr>
        <tr>
            <td><b>Course:</b></td>
            <td>{{$thread->tutor_request->course}}</td>
        </tr>
        <tr>
            <td><b>Start Date:</b></td>
            <td>{{$thread->tutor_request->start_date}}</td>
        </tr>
        <tr>
            <td><b>End Date:</b></td>
            <td>{{$thread->tutor_request->end_date}}</td>
        </tr>
        <tr>
            <td><b>Location:</b></td>
            <td>{{$thread->tutor_request->location}}</td>
        </tr>
        <tr>
            <td><b>Session Time:</b></td>
            <td>{{$thread->tutor_request->session_time}}</td>
        </tr>
        <tr>
            <td><b>Frequency:</b></td>
            <td>{{$thread->tutor_request->frequency}}</td>
        </tr>
        <tr>
            <td><b>Max Rate:</b></td>
            <td>${{$thread->tutor_request->max_rate}}/hr</td>
        </tr>
       @if ($thread->tutor_request->urgency == "High")
        <tr>
            <td><b>Urgency</b></td>
            <td>
                <span style="color:red">{{$thread->tutor_request->urgency}} - Student must meet in less than 2 days</span>      
            </td>
        </tr>
    @endif
        <tr>
            <td><b>Notes / Concerns:</b></td>
            <td>
                @if ($thread->tutor_request->scholarship_program)
                    Scholarship Program: {{$thread->tutor_request->scholarship_program}}<br><br>        
                @endif    
                {{$thread->tutor_request->concerns}}
            </td>
        </tr>
    </tbody>
</table>

<p>
    @if ($thread->tutor_request->requested_tutor_declined)
        <span style="color:red">{{$thread->tutor_request->requested_tutor->user_name}} has declined this tutor request.</span>
    @endif
</p>
@if ($thread->tutor_request->matched_tutor_id == $thread->tutor_request->requested_tutor->id)
    {{-- If Assignment Has been created --}}
    <button class="btn btn-info pull-right" disabled>Assignment Created</button>

@elseif ($thread->tutor_request->requested_tutor->id == Auth::user()->id)
    {{-- Only allow the requested tutor to make an assignment --}} 
    <p>Since the student reached out to you, decide whether or not you can assist this student. <br>
    <span style="color:darkred">If you will not work with this student, please click <b>Decline Request</b> so that we know the student still needs a tutor.</span></p>
    <a href="{{route('marketplace.tutor.accept.request', ['tutor_request_ref_id' => $thread->tutor_request->ref_id])}}"><button type="button" class="btn btn-primary">
        Make Assignment
    </button></a> 

    {{-- Disable Button if Tutor has already declined request --}}
    @if ($thread->tutor_request->requested_tutor_declined)
        <button class="btn btn-danger pull-right" disabled>Request Declined</button> 
    @else
        <a href="{{route('marketplace.tutor.decline.request', ['tutor_request_ref_id' => $thread->tutor_request->ref_id])}}"><button type="button" class="btn btn-danger pull-right">
            Decline Request
        </button></a>     
    @endif
@else
    {{--Disable button on Student's view--}}
    Only {{$thread->tutor_request->requested_tutor->user_name}} can click this button.
    <button class="btn btn-success pull-right" disabled>Make Assignment</button> 
@endif
<br><br>