@extends('layouts.josh-front.default', ['page_title' => "My Inbox"])

@section('title')
    Inbox 
@stop
@section('content')
    @if (Session::has('error_message'))
        <div class="alert alert-danger" role="alert">
            {!! Session::get('error_message') !!}
        </div>
    @endif
    
    <div class="container">
        <div class="row">
    
            <div class="col-md-4 col-sm-4">
                <h3>My Inbox</h3>
                <p>Listed here are your recent tutor requests & conversations.  Click on a message to review the tutor request and continue the conversation.</p>
                <p>Your contact information will not be shared until an assignment is made.  Use our messaging platform to discuss any concerns before an assignment is made.</p>
            </div>
            @if (Auth::user()->banned == 0 && !Auth::user()->hasRole('banned'))              
                <div class="col-md-8 col-sm-8">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>From</th>
                                <th></th>
                                <th>Subject</th>
                                <th>Message</th>
                                <th>Sent</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($threads->count() > 0)
                            @foreach($threads as $thread)
                                @if ($thread->tutor_request || $thread->tutor_proposal)
                                    {{--Check if thread has an existing/undeleted tutor request or proposal --}}
                                    <?php $class = $thread->isUnread($currentUserId) ? 'alert-info' : ''; ?>
                                    <tr class="media alert {!!$class!!}" onclick="location.href='{{route('messages.show', ['id' => $thread->id, 'user_ref_id' => Auth::user()->ref_id]) }}'" style="cursor: pointer;">
                                        <td>{{ $thread->participantsString(Auth::id()) }}</td>
                                        <td>
                                            @if ((($thread->tutor_request->urgency ?? null) && $thread->tutor_request->urgency == 'High') || (($thread->tutor_proposal->tutor_request->urgency ?? null) && $thread->tutor_proposal->tutor_request->urgency == "High"))
                                                <span class="label label-danger">Urgent</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $thread->subject }}
                                        </td>
                                        <td>
                                            @if (strlen($thread->latestMessage->body) > 100)
                                                    {{ substr($thread->latestMessage->body, 0, 100) }} ...
                                                @else
                                                    {{ $thread->latestMessage->body }}
                                                @endif
                                        </td>
                                        <td>{{ $thread->updated_at->diffForHumans() }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No messages.</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
       
            @if (Auth::user()->isStudent())
                <hr>
                 <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                    @include('messenger.partials.negotiating_tips')
                    </div>
                </div>
                <hr>
            @endif
        
    </div>
    
@stop