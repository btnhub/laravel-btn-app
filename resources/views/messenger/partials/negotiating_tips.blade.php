<b><span style="color:darkred">Time to negotiate!</span></b>  Never pay more than you can afford or more than a tutor is worth.  Always feel free to <b>ask for a lower rate</b>.  For a rate guide & negotiating tips please <a data-toggle="collapse" href="#collapseRate" aria-expanded="false" aria-controls="collapseRate">click here</a>.

{{-- Rate Guide Table --}}
<div class="collapse" id="collapseRate">
    <h4 style="color:darkblue">Rate Guide</h4>
    Tutors set their own rate and are able to see what you listed as your max affordable rate.  Based on our years of experience, this is where we believe a tutor's rate should be.  
    <div class="row">
        <div class="card card-block">
            <div class="col-md-offset-1 col-sm-offset-1 col-md-9 col-sm-8">
                @include('btn.forms.partials.rate_guide')    
            </div>
        </div>
    </div>
    

    <b><em>Did you know:</em></b>
    <li>Our tutors get to keep much more of their earnings (65-75%) than most tutoring services.</li>
    <li>If an undergraduate or graduate student worked for the university as a tutor or help-room assistant he/she would earn less than $20/hr.  We pride ourselves in having tutors who are more experienced and effective, but that's worth knowing.</li>
    
    <hr>

    <h4 style="color:darkblue">Negotiating Tips</h4>
    If you think the tutor's asking rate is high or unreasonable, ask for a lower rate! The worst that can happen is he/she will say no, in which case you can determine if it's worth working with this tutor or <a href="{{route('list.tutors')}}" target="_blank">find a different tutor</a> to work with.   <br><br>

    <b>-- Do Your Research</b><br>
    Review this tutor's profile (link below) and focus on:
    <ol>
        <li>Overall teaching/tutoring experience in the Experience section</li>
        <li>How long this tutor has worked for BuffTutor</li>
        <li># of Positive Reviews from students</li>
        <li>References from previous students</li>
    </ol>
    If the tutor is new to BuffTutor, has limited previous experience, has few or no reviews, use this information to ask for a lower rate.

    <br><br>
    <b>-- Multiple Sessions In A Week</b> <br>
    Most tutors are open to lowering their rate if you want to meet multiple times in a week (even twice a week).

    <br><br>

    <b>-- Demand An Explanation</b><br>
    Ask this tutor to explain why his/her rate is so high and what makes him/her worth so much money.  If he/she cannot provide an acceptable answer, ask for a lower rate or <a href="{{route('list.tutors')}}" target="_blank">find a different tutor</a>.
    <br><br>

    <b>-- Contact Us</b><br>
    Feel free to reach out to us for an idea of what we believe this tutor is worth and what he/she typically charges. 

    <h4 style="color:darkgreen">Good Luck Negotiating!</h4>
</div>