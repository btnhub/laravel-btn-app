@extends('layouts.josh-front.default')

@section('title')
    Inbox | {{$thread->subject}}
@stop

@section('content')
    @if (Auth::user()->banned == 0 && !Auth::user()->hasRole('banned'))
    
        <div class="col-md-6 col-sm-6">
            <h1>{{ $thread->subject }}</h1>
            
            <hr>
            
            <div id="conversation" class="col-md-12 col-sm-12">
               
                @foreach($thread->messages as $message)
                    <div class="media">
                        {{--<a class="pull-left" href="#">
                            <img src="//www.gravatar.com/avatar/{{ md5($message->user->email) }}?s=64" alt="{{ $message->user->first_name }}" class="img-circle">
                        </a>--}}
                        <div class="media-body">
                            <h4 class="media-heading"><b>{{ $message->user->user_name }} </b></h4>
                            <p>{!! nl2br(e($message->body)) !!}</p>
                            <div class="text-muted"><small>Posted {{ $message->created_at->diffForHumans() }}</small></div>
                        </div>
                    </div>
                @endforeach
            </div>
                <br>
                
                {!! Form::open(['route' => ['messages.update', 'id' => $thread->id, 'user_ref_id' => Auth::user()->ref_id], 'method' => 'PUT']) !!}
                <!-- Message Form Input -->
                <div class="form-group">
                    {!! Form::textarea('message', null, ['class' => 'form-control', 'size' => '50x4', 'placeholder' => 'Use this space to negotiate details of sessions.  If an agreement is reached, click Make Assignment (bottom right) to access each other\'s contact information.',   'required']) !!}
                    <small style="color:darkred"><b>Do NOT include your contact information (e-mail, phone) in your message </b></small>
                </div>
                <!-- Submit Form Input -->
                <div class="form-group col-md-6">
                    {!! Form::submit('Reply', ['class' => 'btn btn-primary form-control']) !!}
                </div>

                <div class="col-md-12 col-sm-12">
                    <hr>
                    <h4>What Next?</h4>
                    <ol>
                        <li> If you want to work together, click Make Assignment (bottom right corner)</li>
                        <li> The student must then <a href="{{route('payment.settings')}}">add a credit/debit card</a> to his/her account to share each other's contact information.</li>
                    </ol>
                </div>

                @if (Auth::user()->isStudent()) 
                    <div class="form-group col-md-12">
                        <hr>
                        <img src="{{asset('btn/images/no_cash.jpg')}}" height="70px">
                        <span style="color:darkred"><b> Do not pay your tutor directly (cash, check or any other means). </b></span> 
                        <li>Once a match is made, your tutor will charge your card <b>after</b> sessions occur.  </li>
                        <li>This protects students as we will be able to refund disputed charges.  </li>
                        <li>Payment directly to our tutors is a violation of our <a href="{{route('terms')}}">Terms of Use</a> with hefty fines.</li>
                        <li><b><span style="color:red">Please report this tutor if he/she asks you to pay him/her directly.</span></b></li>
                    </div>
          
                @endif
                {!! Form::close() !!}
            
        </div>

        <div class="col-md-6 col-sm-6">
        
            @if ($thread->tutor_request)
                {{-- For Direct Requests (Student Initiated Contact) --}}
                @include('messenger.partial_direct_tutor_request')
            
            @elseif ($thread->tutor_proposal)
                {{-- For Proposals (Tutor Initiated Contact) --}}
                    @include('messenger.partial_tutor_proposal')
            @endif
        </div>
    @endif
    {{--To automatically scroll to bottom of conversation--}}

    <script>
        var objDiv = document.getElementById("conversation");
        
        if (objDiv.offsetHeight > 450) 
        {
            objDiv.style.height = "450px";    
            objDiv.style.overflowY = "scroll"; 
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    </script>
@stop

