<?php $cities = App\Location::studentsVisible()->where('city', 'not like', '%online%')->orderBy('website_order')->pluck('city')->implode(', '); ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta-description', 'Dedicated to providing outstanding academic assistance for college & high school courses. Our network consists of in-person and online tutors for Math, Science, Language, Writing, Algebra, Calculus, Statistics, Chemistry, Physics, Biology, Spanish, French, Physiology, Anatomy, English, and more.')"> {{-- Site / Page Description --}}
    <title>@yield('title', 'BuffTutor - Connecting Students With The Best Tutors')</title>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


    {{--For Google Search Results 06/27/2018--}}
    <script type='application/ld+json'> 
        {
          "@context": "http://www.schema.org",
          "@type": "EducationalOrganization",
          "name": "BuffTutor",
          "url": "https://www.BuffTutor.com",
          "logo": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg",
          "description": "Connecting students with the best tutors! Easily find private tutors for College and High School Math, Chemistry, Physics, Biology, Calculus, Algebra, Statistics, Economics, Spanish and more.  We have a network of in-person and online tutors in {{$cities ?? 'Boulder, Denver, Fort Collins and beyond'}}.",
           "aggregateRating": {
            "@type": "aggregateRating",
            "ratingValue": "4.78",
            "reviewCount": "1347"
          }
        }
    </script>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/lib.css') }}">

    <!--Fonts-->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <!--end of global css-->
    <!--page level css-->
    @yield('header_styles')
    @stack('css')
    @stack('header')
    <!--end of page level css-->
</head>

<body>
    <!-- Header Start -->
    <header>
        <!-- Icon Section Start -->
            {{--@include('layouts.josh-front.partials.header')--}}
        <!-- //Icon Section End -->
        <!-- Nav bar Start -->
            @include('layouts.josh-front.partials.topnavbar')
        <!-- Nav bar End -->
    </header>
    <!-- //Header End -->
    
    <!-- slider / breadcrumbs section -->
    @yield('top')

    <!-- Content -->
    @include('btn.partials.alerts')
    @yield('content')

    <!-- Footer & Copyright Section Start -->
        @include('layouts.josh-front.partials.footer')
    <!-- Footer & Copyright Section End -->
    
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>
    <!--global js starts-->
    <script type="text/javascript" src="{{ asset('assets/js/frontend/lib.js') }}"></script>
    {{-- Switches Skin Colors of Theme--}}
    {{--<script src="{{asset('assets/js/frontend/style-switcher.js')}}" type="text/javascript"></script>--}}
    <!--global js end-->
    <!-- begin page level js -->
    @yield('footer_scripts')
    <!-- end page level js -->

    {{-- jQuery Cannot use this version.  Use Template's jQuery--}}
    {{--    <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script> --}}

    {{--Additional scripts can be pushed onto the template--}}
    @stack('scripts')
</body>

</html>
