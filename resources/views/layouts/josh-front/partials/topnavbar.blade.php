<nav class="navbar navbar-default container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
            <span><a href="#" title="Menu"> <i class="livicon" data-name="responsive-menu" data-size="25" data-loop="true" data-c="#757b87" data-hc="#ccc"></i>
            </a></span>
        </button>
        <a class="navbar-brand" href="/"><img width="70px" src="{{asset('btn/logo/BuffTutor_Logo_Small.jpg')}}" class="logo_position" alt="BuffTutor Logo">
        </a>
        {{--
        <a class="navbar-brand" href="{{ url('/') }}">
              Buff<b>Tutor</b>
        </a>
        --}}
    </div>
    <div class="collapse navbar-collapse" id="collapse">
        @include('btn.layouts.top_nav_links')
    </div>
</nav>