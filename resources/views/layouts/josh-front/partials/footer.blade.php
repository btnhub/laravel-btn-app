<footer>
        <!-- Footer Container Start -->
        {{--<div class="container footer-text">--}}
        <div class="container">
            <!-- Contact Section Start -->
            <div class="col-sm-4">
                <h4>Contact Us</h4>
                <ul class="list-unstyled">
                    <li>1905 15th St. #1932</li>
                    <li>Boulder, CO 80306</li>
                    
                    {{--<li><i class="livicon icon4 icon3" data-name="cellphone" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>Phone:9140 123 4588</li>--}}

                    <li><i class="livicon icon3" data-name="mail-alt" data-size="20" data-loop="true" data-c="#ccc" data-hc="#ccc"></i> Email:<span class="primary">
                        <a href="mailto:contact@bufftutor.com" title="Email Us">Contact@BuffTutor.com</a></span>
                    </li>

                    {{--<h4>Follow Us</h4>
                    <ul class="list-inline">
                        <li>
                            <a href="#"> <i class="livicon" data-name="facebook" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                            </a>
                        </li>
                    </ul>
                    --}}
                </ul>
            </div>
            <!-- //Contact Section End -->

            <!-- About Us Section Start -->
            <div class="col-sm-4">
                <h4>Join Us</h4>
                <p>
                    Love teaching?  We're always looking for experienced & passionate tutors.  <a href="{{route('become.tutor')}}" title="Become A BuffTutor">Apply today!</a>
                </p>
            </div>
            <!-- //About Us Section End-->  
            <!-- Recent post Section Start -->
            <div class="col-sm-4">
                
                <a href="/" title="BuffTutor Home">
                    <img width="200" src="{{asset('btn/logo/BuffTutor_Logo.jpg')}}" class="item" alt="BuffTutor Logo">
                </a>
                
                {{--
                <h4>Recent Posts</h4>
                <div class="media">
                    <div class="media-left media-top">
                        <a href="#">
                            <img class="media-object" src="{{asset('templates/josh-front/images/image_14.jpg')}}" alt="image">
                        </a>
                    </div>
                    <div class="media-body">
                        <p class="media-heading">Lorem Ipsum is simply dummy text of the printing and type setting industry dummy.
                            <br />
                            <div class="pull-right"><i>John Doe</i></div>
                        </p>
                    </div>
                </div>
                <div class="media">
                    <div class="media-left media-top">
                        <a href="#">
                            <img class="media-object" src="{{asset('templates/josh-front/images/image_15.jpg')}}" alt="image">
                        </a>
                    </div>
                    <div class="media-body">
                        <p class="media-heading">Lorem Ipsum is simply dummy text of the printing and type setting industry dummy.
                            <br />
                            <div class="pull-right"><i>John Doe</i></div>
                        </p>
                    </div>
                </div> --}}
            </div>
            <!-- //Recent Post Section End -->
        </div>
        <!-- Footer Container Section End -->
    </footer>
    <!-- //Footer Section End -->
    <!-- Copy right Section Start -->
    <div class="copyright">
        <div class="container">
           <p> <span><a href="{{route('terms')}}" title="Terms Of Use">Terms Of Use</a></span>
            <span class="pull-right">Copyright &copy; BuffTutor / RamTutor, 2009-{{date('Y')}}</span></p>
        </div>
    </div>
    <!-- Copy right Section End -->
    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>