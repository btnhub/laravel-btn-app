<!DOCTYPE HTML>
<html lang="en">
    <head>
        <!--=============== basic  ===============-->
        <meta charset="UTF-8">
        <title>@yield('title', 'BuffTutor - Connecting Students With The Best Tutors')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content="@yield('meta-description', 'Dedicated to providing outstanding academic assistance for college & high school courses. Our network consists of in-person and online tutors for Math, Science, Language, Writing, Algebra, Calculus, Statistics, Chemistry, Physics, Biology, Spanish, French, Physiology, Anatomy, English, and more.')"> {{-- Site / Page Description --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!--=============== css  ===============-->
        <link type="text/css" rel="stylesheet" href="{{asset('assets/css/citybook/reset.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('assets/css/citybook/style.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('assets/css/citybook/plugins.min.css')}}">
        <link type="text/css" rel="stylesheet" href="{{asset('assets/css/citybook/color.min.css')}}">

        <!--=============== favicons ===============-->
        <link rel="shortcut icon" href="{{url('favicon.ico')}}">
        <!--page level css-->
        @yield('header_styles')
        @stack('css')
        @stack('header')
        <!--end of page level css-->
    </head>
    <body>
        <!--loader-->
        <div class="loader-wrap">
            <div class="pin"></div>
            <div class="pulse"></div>
        </div>
        <!--loader end-->

        <!-- Main  -->
        <div id="main">
            <!-- header-->
            <header class="main-header dark-header fs-header sticky">
                @include('layouts.citybook.partials.navbar_top')
            </header>
            <!--header end -->

            <div id="wrapper">
                <div class="content">
                    @include('layouts.citybook.partials.alerts')
                    @yield('content')
                </div>
            </div>
            <!--footer-->
            @include('layouts.citybook.partials.footer')
            <!--footer end-->
            <!--login modal -->
            {{-- @include('layouts.citybook.partials.login_modal') --}}
            <!--login modal end-->
            <a class="to-top"><i class="fa fa-angle-up"></i></a>
        </div>
        <!-- Main end -->

        <!--=============== scripts  ===============-->
        <script type="text/javascript" src="{{asset('assets/js/citybook/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/citybook/plugins.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/js/citybook/scripts.js')}}"></script>

        <!-- begin page level js -->
        @yield('footer_scripts')
        <!-- end page level js -->

        {{--Datatables--}}
        {{-- <script type="text/javascript" charset="utf8" src="{{ URL::asset('btn/DataTables/datatables.js') }}"></script> --}}

        {{--Additional scripts can be pushed onto the template--}}
        @stack('scripts')

        {{--For Google Search Results 06/27/2018--}}
        <script type='application/ld+json'> 
            {
              "@context": "http://www.schema.org",
              "@type": "LocalBusiness",
              "additionalType": ["Tutoring Service","Tutoring Agency", "In-Person Tutoring", "College / University Tutoring", "High School Tutoring", "Online Tutoring"],
              "name": "BuffTutor",
              "image": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg",
              "url": "https://www.BuffTutor.com",
              "email": "Contact(at)BuffTutor.com",
              {{-- "telephone": "+1-765-543-7542", --}}
              "foundingDate": "2009",
              "foundingLocation": "Boulder, CO",
              "logo": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg",
              "description": "Connecting students with the best tutors! Easily find private tutors for College and High School Math, Chemistry, Physics, Biology, Calculus, Algebra, Statistics, Economics, Spanish and more.",
              "aggregateRating": {
                "@type": "aggregateRating",
                "ratingValue": "4.78",
                "ratingCount": "1347",
                "reviewCount": "1347",
                "bestRating": "5",
                "worstRating": "1"
              },
              "address": {
                "@type": "PostalAddress",
                "addressLocality": "Boulder",
                "addressRegion": "Colorado",
                "postalCode": "80306"{{-- ,
                "streetAddress": "20341 Whitworth Institute 405 N. Whitworth" --}}
              },
              {{-- "location": [{!!$location_schema!!}],  --}}
              "sameAs": ["https://www.facebook.com/BuffTutor", "https://www.twitter.com/BuffTutor", "https://www.instagram.com/BuffTutor"],
              "geo": {
                "@type": "GeoCoordinates",
                "latitude": "40.014984",
                "longitude": "-105.270546"
              }
            }
        </script>
    </body>

</html>
