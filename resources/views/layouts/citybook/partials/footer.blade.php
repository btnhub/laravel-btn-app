<?php
    $math_subjects = ['Math', 'Calculus', 'Algebra', 'Geometry', 'Trigonometry'];
    $science_subjects = ['Science', 'Physics', 'Biology', 'Chemistry', 'Engineering'];
    $other_subjects = ['Humanities', 'Writing', 'Spanish', 'College', 'High School'];
?>

<footer class="main-footer dark-footer  ">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>About Us</h3>
                    <div class="footer-contacts-widget fl-wrap">
                        <center style="color:white;font-weight:bold">
                            Dedicated to connecting students with the very best tutors!  
                        </center>
                        <br>
                        {{-- <p>
                            We are devoted to the academic success of our students and go the extra mile to ensure the quality of the tutors in our network and the quality of the service we provide.  
                        </p> --}}
                        <p>
                            Our tutors are current & former instructors, graduate students, teaching assistants and exceptional undergraduates.  All our tutors have at least 1 year of teaching/tutoring experience and must pass our challenging proficiency exams.</p> 
                        <p style="color:white;font-weight:bold">Only the best can be a BuffTutor!</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>Contact Us</h3>
                    <div class="widget-posts fl-wrap">
                        <ul  class="footer-contacts fl-wrap" style="border-top:none;margin-top:0px;padding-top:0px">
                            <li><span><i class="fa fa-envelope-o"></i> </span><a href="mailto:Contact@BuffTutor.com">Contact@BuffTutor.com</a></li>
                            {{-- <li><span><i class="fa fa-phone"></i> SMS :</span><a href="sms://+11234567890">Text Us</a></li> --}}
                            <li><span><i class="fa fa-facebook-official"></i></span><a href="https://www.facebook.com/BuffTutor" target="_blank">BuffTutor</a> <span style="color:white"></span> &nbsp;<span style="color:white">/</span>&nbsp;
                            <a href="https://www.facebook.com/RamTutor" target="_blank">RamTutor</a> <span style="color:white"></span>
                            </li>
                            <li><span><i class="fa fa-instagram"></i></span><a href="https://www.instagram.com/BuffTutor" target="_blank">BuffTutor</a> <span style="color:white"></span> &nbsp;<span style="color:white">/</span>&nbsp;
                            <a href="https://www.instagram.com/RamTutor" target="_blank">RamTutor</a> <span style="color:white"></span>
                            </li>
                            <li><span><i class="fa fa-twitter"></i></span><a href="https://www.twitter.com/BuffTutor" target="_blank">BuffTutor</a> <span style="color:white"></span> &nbsp;<span style="color:white">/</span>&nbsp;
                            <a href="https://www.twitter.com/RamTutor" target="_blank">RamTutor</a> <span style="color:white"></span>
                            </li>
                            <li><span><i class="fa fa-linkedin"></i></span><a href="https://linkedin.com/company/the-bufftutor-program" target="_blank">LinkedIn</a> <span style="color:white"></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3>Get Started</h3>
                    <div class="footer-contacts-widget fl-wrap">
                        <p style="text-align: center">Don't wait until it's too late,  get the help you need today!</p>
                        <a href="{{route('request.get')}}" style="margin-bottom: 3em"><button id="subscribe-button" class="subscribe-button">Request A Tutor</button></a>
                        <p style="padding-top:5em; text-align: center">Think you have what it takes to join BuffTutor?</p>
                        <a href="{{route('become.tutor')}}"><button id="subscribe-button" class="subscribe-button">Become A Tutor</button></a>
                    </div>
                    {{-- <h3>Check Availability</h3>
                    <div class="custom-form">
                        {!! Form::open(['method' => 'POST', 'route' => 'check.availablility', 'id' =>"avail"]) !!}
                            <input class="enteremail" name="full_name" id="full_name" placeholder="Full Name" type="text" required>
                            <input class="enteremail" name="email" id="subscribe-email" placeholder="Email" spellcheck="false" type="text" required>
                            <input class="enteremail" name="course" id="course" placeholder="Course" type="text" required>
                            <input class="enteremail" name="location" id="location" placeholder="City/State" type="text" required>
                            <input type="hidden" name="check">
                            <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fa fa-paper-plane"></i> Check Availability</button>
                            <label for="subscribe-email" class="subscribe-message"></label>
                        
                        {!! Form::close() !!}
                    </div>
                    <br>
                    <p style="color: rgba(255,255,255,0.81);">Alternatively,  
                        <a href="{{route('request.get')}}" style="color:rgb(235,242,254)"><u>request a tutor.</u></a>
                    </p> --}}
                </div>
            </div>
            <div class="col-md-3">
                <div class="footer-widget fl-wrap">
                    <h3> Policies </h3>
                    <div class="footer-widget fl-wrap">
                        <div class="footer-menu fl-wrap">
                            <ul>
                                <li><a href="{{route('refund.policy')}}">Refund</a></li>
                                <li><a href="{{route('cancellation.policy')}}">Cancel</a></li>
                                <li><a href="{{route('terms')}}">Terms</a></li>
                            </ul>
                        </div>
                        <div class="subscribe-widget fl-wrap footer-menu">
                            <a href="{{ url('/') }}"><img src="{{asset('btn/logo/BuffTutor_Logo.jpg')}}" width="100%"></a>
                            <p style="font-weight: bold;text-align:center">Connecting students with the best tutors!</p>
                            <a href="{{route('locations')}}" style="color:#2C3B5A;font-size:0.5em">Locations</a>
                            <a href="{{route('colleges')}}" style="color:#2C3B5A;font-size:0.5em">Colleges</a>
                            <a href="{{route('college.courses')}}" style="color:#2C3B5A;font-size:0.5em">College Courses</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        @if (isset($universities) && count($universities))
            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-widget fl-wrap">
                        <h3>College Tutors</h3>
                        <div class="footer-contacts-widget fl-wrap">
                            <div class="row">
                                <ul  class="footer-contacts fl-wrap" style="border-top:none;margin-top:0px;padding-top:0px">
                                    @foreach ($universities as $uni)
                                        <div class="col-sm-4">
                                            @foreach ($math_subjects as $subject)
                                                <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.college', strtolower(str_replace(" ", "_",$uni)."-".str_replace(" ", "_",$subject)."-tutors"))}}">{{$uni}} {{$subject}} Tutors</span></a></li>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-4">
                                            @foreach ($science_subjects as $subject)
                                                <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.college', strtolower(str_replace(" ", "_",$uni)."-".str_replace(" ", "_",$subject)."-tutors"))}}">{{$uni}} {{$subject}} Tutors</span></a></li>
                                            @endforeach
                                        </div>
                                        <div class="col-sm-4">
                                            @foreach ($other_subjects as $subject)
                                                <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.college', strtolower( str_replace(" ", "_",$uni)."-".str_replace(" ", "_",$subject)."-tutors"))}}">{{$uni}} {{$subject}} Tutors</span></a></li>
                                            @endforeach
                                        </div>
                                        <br><br>
                                    @endforeach
                                    {{-- <li style="padding-left:1em;margin:0px;font-size: 0.85em"><span><a href="{{route('colleges')}}">...more colleges</a></span></li> --}}
                                    <li style="padding-left:1em;margin:0px;font-size: 0.85em"><span><a href="{{route('university.courses', strtolower(str_replace(" ", "_",$uni)))}}">...more courses</a></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <br><br>
        <div class="row">
            <div class="col-md-6">
                <div class="footer-widget fl-wrap">
                    <h3>Top Subjects</h3>
                    <div class="footer-contacts-widget fl-wrap">
                        <div class="row">
                            <ul  class="footer-contacts fl-wrap" style="border-top:none;margin-top:0px;padding-top:0px">
                                @if (isset($city) && $city)
                                    <div class="col-sm-4">
                                        @foreach ($math_subjects as $subject)
                                            <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.city', strtolower(str_replace(" ", "_",$city)."-$state-$subject-tutors"))}}">{{$city}} {{$subject}} Tutors</span></a></li>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-4">
                                        @foreach ($science_subjects as $subject)
                                            <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.city', strtolower(str_replace(" ", "_",$city)."-$state-$subject-tutors"))}}">{{$city}} {{$subject}} Tutors</span></a></li>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-4">
                                        @foreach ($other_subjects as $subject)
                                            <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.city', strtolower(str_replace(" ", "_",$city)."-$state-$subject-tutors"))}}">{{$city}} {{$subject}} Tutors</span></a></li>
                                        @endforeach
                                    </div>
                                    <li style="padding-left:1em;margin:0px;font-size: 0.85em"><span><a href="{{route('city.courses', [strtolower(str_replace(" ", "_",$city)), strtolower($state)])}}">...more {{$city}} subjects</a></span></li>
                                @else
                                    <div class="col-sm-4">
                                        @foreach ($math_subjects as $subject)
                                            <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.general', strtolower(str_replace(" ", "_",$subject)."-tutors"))}}">{{$subject}} Tutors</span></a></li>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-4">
                                        @foreach ($science_subjects as $subject)
                                            <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.general', strtolower(str_replace(" ", "_",$subject)."-tutors"))}}">{{$subject}} Tutors</span></a></li>
                                        @endforeach
                                    </div>
                                    <div class="col-sm-4">
                                        @foreach ($other_subjects as $subject)
                                            <li style="padding:0px;font-size: 0.85em"><span><a href="{{route('home.general', strtolower(str_replace(" ", "_",$subject)."-tutors"))}}">{{$subject}} Tutors</span></a></li>
                                        @endforeach
                                    </div>
                                    <li style="padding-left:1em;margin:0px;font-size: 0.85em"><span><a href="{{route('courses')}}">...more subjects</a></span></li>
                                @endif
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer fl-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="about-widget">
                        {{-- <img src="images/logo.png" alt=""> --}}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="copyright"> &#169; BuffTutor / RamTutor 2009-{{date('Y')}}.  All rights reserved.</div>
                </div>
                <div class="col-md-4">
                    <div class="footer-social">
                        {{-- <a href="{{route('terms')}}" style="color:lightgray">Terms of Use</a> --}}
                        {{-- <ul>
                            <li><a href="https://www.facebook.com/BuffTutor/" target="_blank" ><i class="fa fa-facebook-official"></i></a></li>
                            <li><a href="https://linkedin.com/company/the-bufftutor-program" target="_blank" ><i class="fa fa-linkedin"></i></a></li>
                        </ul> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>