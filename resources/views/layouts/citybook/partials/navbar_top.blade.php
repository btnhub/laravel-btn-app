<div class="header-inner">
    <div class="logo-holder">
        <a href="{{ url('/') }}"><img src="{{asset('btn/logo/BuffTutor_Logo_Small.png')}}" alt="BuffTutor Logo"></a>
    </div>
    @if ($user)
        <div class="header-user-menu">
            <div class="header-user-name">
                @if ($profile && $profile->avatar)
                    <span><img src="/btn/avatars/{{$profile->avatar}}" alt=""></span>
                @endif
                Hello, {{$user->first_name}}
            </div>
            <ul>
                <li><a href="{{route('profile.account', $user->ref_id)}}"> My Account</a></li>
                <li><a href="{{route('profile.edit', $user->ref_id)}}"> Edit Profile</a></li>
                {{-- <li><a href="{{ route('my.review.sessions', ['user_ref_id'=>$user->ref_id]) }}">My Review Sessions</a></li> --}}
                @if ($user->isTutor())
                    <li><a href="{{ route('profile.index', ['ref_id'=>$user->ref_id]) }}"> My Profile</a></li>
                @endif
                @if ($user->isStudent())
                    <li><a href="{{route('my.tutor.requests', $user->ref_id)}}">My Tutor Proposals</a></li>
                    <li><a href="{{ route('payment.settings') }}">Payment Settings</a></li>
                @endif
                <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> Log Out</a></li>
            </ul>
        </div>
    @else
        <div class="nav-holder main-menu">
            <nav>
                <ul>
                    <li><a href="{{url('login')}}"><i class="fa fa-sign-in"></i> Sign In</a></li>    
                </ul>
            </nav>
        </div>
    @endif
    
    
    <!-- nav-button-wrap-->
    {{--Collapsible Menu--}}
    <div class="nav-button-wrap color-bg">
        <div class="nav-button">
            <span></span><span></span><span></span>
        </div>
    </div>
    <!-- nav-button-wrap end-->
    
    <!--  navigation -->
    <div class="nav-holder main-menu">
        <nav>
            <ul>
                <li>
                    <a href="{{route('get.search.map')}}">Search</a>
                </li>
                <li>
                    <a href="{{route('request.get')}}">Request A Tutor</a>
                </li>
                {{-- <li>
                    <a href="{{route('get.review.sessions')}}">Review Sessions <i class="fa fa-caret-down"></i></a>
                    <ul>
                        <a href="{{route('get.review.sessions', 'calendar')}}">Upcoming Reviews</a>
                        <a href="{{route('get.review.sessions', 'vote')}}">Vote For Review</a>
                    </ul>
                </li> --}}
                <li>
                    <a href="{{route('become.tutor')}}">Become A Tutor</a>
                </li>
                @if ($user && $user->isTutor())
                    <li>
                        <a href="#">Tutors <i class="fa fa-caret-down"></i></a>
                        <!--third  level  -->
                        <ul>
                            <li><a href="{{ route('my.tutor.contracts', $user->ref_id) }}">My Tutor Requests</a></li>
                            <li><a href="{{ route('get.search.students') }}">Add Previous Student</a></li>
                            <li><a href="{{ route('tutor.checklist') }}">Tutor Checklist</a></li>
                            <li><a href="{{ route('tutor.orientation') }}">Tutor Orientation</a></li>               
                            {{-- <li><a href="{{ route('online.tutoring.details') }}"><i class="fa fa-desktop"></i> Online Tutoring</a></li> --}}
                            <li><a href="{{ route('course.catalogs') }}">Calendars & Catalogs</a></li>               
                            <li><a href="{{ route('intro.email') }}">Intro Email</a></li>     
                        </ul>
                        <!--third  level end-->
                    </li>
                @endif

                @if ($user && $user->hasRole(['super-user', 'admin']))
                    <li>
                        <a href="#">Admin <i class="fa fa-caret-down"></i></a>
                        <!--third  level  -->
                        <ul>
                            {{-- <li><a href="{{ route('list.courses') }}">Add Courses</a></li> --}}
                            <li><a href="{{ route('list.tutors') }}">Search Tutors</a></li>
                            <li><a href="{{ route('user.manager') }}">User Manager</a></li>
                            <li><a href="{{ route('list.tutor.requests') }}">Tutor Requests</a></li>
                            <li><a href="{{ route('assignments.index') }}">All Assignments</a></li>
                            <li><a href="{{ route('applicants') }}">Tutor Applications</a></li>
                            <li><a href="{{ route('show.evaluations') }}">Evaluations</a></li>
                            <li><a href="{{ route('prof.exam.list') }}">Proficiency Exams</a></li>
                            <li><a href="{{ route('analytics') }}">Analytics</a></li>
                            <li><a href="{{ route('users.without.location') }}">Fix Users Location</a></li>
                            <li><a href="{{ route('admin.list.tutors') }}">Mass Email Tutors</a></li>
                            <li><a href="{{ route('marketing') }}">Marketing</a></li>
                            <li><a href="{{ route('daniels.fund') }}">Daniels Fund</a></li>
                            <li><a href="{{ route('list.advisors') }}">Academic Advisors</a></li>
                            <li><a href="{{ route('failed.jobs') }}">Failed Jobs</a></li>
                            <li><a href="{{ route('tutor.payment') }}">Next Tutor Pay</a></li>
                            <li><a href="{{ route('contractor.taxes') }}">Contractor Taxes</a></li>
                            {{-- <li><a href="{{ route('first.session.students') }}">Issues</a></li> --}}
                        </ul>
                    </li>
                @endif
                <li><a href="{{route('favorite.tutors')}}"><i class="fa fa-heart"></i> <span id="fav-count">{{$fav_count}}</span></a></li>
            </ul>
        </nav>
    </div>
    <!-- navigation  end -->
</div>