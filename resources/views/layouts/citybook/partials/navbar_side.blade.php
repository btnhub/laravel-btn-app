<div class="fixed-bar fl-wrap">
    <div class="user-profile-menu-wrap fl-wrap">
        <!-- user-profile-menu-->
        <div class="user-profile-menu">
            <h3>Main</h3>
            <ul>
            	<li><a href="{{ route('profile.account',Auth::user()->ref_id) }}" {{-- class="user-profile-act" --}}><i class="fa fa-gears"></i>My Account</a></li>
                @if (Auth::user()->isTutor())
                    <li><a href="{{ route('profile.index', ['ref_id'=>Auth::user()->ref_id]) }}"><i class="fa fa-user"></i> My Profile</a></li>
                @endif
                <li><a href="{{route('profile.edit', Auth::user()->ref_id)}}"><i class="fa fa-user-o"></i> Edit profile</a></li>
                @if (Auth::user()->isStudent())
                    <li><a href="{{route('my.tutor.requests', Auth::user()->ref_id)}}"><i class="fa fa-list"></i>My Tutor Proposals</a></li>
                    <li><a href="{{ route('payment.settings') }}"><i class="fa fa-credit-card"></i>Payment Settings</a></li>
                @endif
            </ul>
        </div>
        <!-- user-profile-menu end-->
        <!-- user-profile-menu-->
        @if (Auth::user()->isTutor() || Auth::user()->isSuperUser() || Auth::user()->isStaff())
            <div class="user-profile-menu">
                <h3>Tutor Tools</h3>
                <ul>
                    <li><a href="{{ route('my.tutor.contracts', Auth::user()->ref_id) }}">My Tutor Requests</a></li>
                    <li><a href="{{ route('get.search.students') }}"><i class="fa fa-users"></i>Add Previous Student</a></li>
                    <li><a href="{{ route('tutor.checklist') }}"><i class="fa fa-list"></i> Tutor Checklist</a></li>
                    <li><a href="{{ route('tutor.orientation') }}"><i class="fa fa-map"></i> Orientation & Tour</a></li>               
                    {{-- <li><a href="{{ route('online.tutoring.details') }}"><i class="fa fa-desktop"></i> Online Tutoring</a></li> --}}
                    <li><a href="{{ route('course.catalogs') }}"><i class="fa fa-calendar-check-o"></i>Calendars & Catalogs</a></li>               
                    <li><a href="{{ route('intro.email') }}"><i class="fa fa-file"></i>Introductory Email</a></li>     
                    
                </ul>
            </div>
        @endif
                                      
        <a href="{{url('/logout')}}" class="log-out-btn">Log Out</a>
    </div>
</div>