<span id="alerts-default">
	@if (Session::has('info'))
		<div class="notification waitforreview fl-wrap">
		    <p>{{ Session::get('info') }}</p>
		    <a class="notification-close" href="#"><i class="fa fa-times"></i></a>
		</div>	
	@endif

	@if (Session::has('success'))
		<div class="notification success fl-wrap">
		    <p>{{ Session::get('success') }}</p>
		    <a class="notification-close" href="#"><i class="fa fa-times"></i></a>
		</div>	
	@endif

	@if (Session::has('error'))
		<div class="notification reject fl-wrap">
		    <p>{{ Session::get('error') }}</p>
		    <a class="notification-close" href="#"><i class="fa fa-times"></i></a>
		</div>	
	@endif	
</span>
