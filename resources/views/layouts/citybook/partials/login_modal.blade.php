<div class="main-register-wrap modal">
    <div class="main-overlay"></div>
    <div class="main-register-holder">
        <div class="main-register fl-wrap">
            <div class="close-reg"><i class="fa fa-times"></i></div>
            <h3>Sign In <span>Buff<strong>Tutor</strong></span></h3>
            <div id="tabs-container">
                <ul class="tabs-menu">
                    <li class="current"><a href="#tab-1">Login</a></li>
                    {{-- <li><a href="#tab-2">Register</a></li> --}}
                </ul>
                <div class="tab">
                    <div id="tab-1" class="tab-content">
                        <div class="custom-form">
                            {!! Form::open(['method' => 'POST', 'url' => 'login']) !!}
                                    <label>Email Address * </label>
                                    <input name="email" type="text"   onClick="this.select()" value="" autofocus>
                                    <label >Password * </label>
                                    <input name="password" type="password"   onClick="this.select()" value="" >
                                    <button type="submit"  class="log-submit-btn"><span>Log In</span></button>
                                    <div class="clearfix"></div>
                            {!! Form::close() !!}
                            <div class="lost_password">
                                <a href="{{ url('/password/reset') }}">Lost Your Password?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>