@extends('layouts.citybook.default')
@section('title', "Dashboard")

@push('scripts')
    <script type="text/javascript">
        var old_span = document.getElementById('alerts-default');
        var new_span = document.getElementById('alerts-dashboard');
        while (old_span.childNodes.length > 0) {
            new_span.appendChild(old_span.childNodes[0]);
        }
    </script>
@endpush
@section('content')
	<!--content -->  
    <div class="content">
    	<!--section --> 
        <section id="sec1" style="background-color: #e8e8e8;padding-top: 30px">
            <!-- container -->
            <div class="container">
                <!-- profile-edit-wrap -->
                <div class="profile-edit-wrap">
                    <div class="profile-edit-page-header">
                        <h2>Dashboard - @yield('page_title')</h2>
                    </div>
                    <div class="row">
                        <div class="col-sm-3 hidden-xs">
        					@include('layouts.citybook.partials.navbar_side')
        				</div>
        				<div class="col-sm-9">
                            <!-- profile-edit-container--> 
                            <div class="profile-edit-container">
                                <span id="alerts-dashboard"></span>

                                @yield('dashboard_content') 

                            </div>
                        </div>
        			</div>
        		</div>
			</div>        		
		</section>
		<!-- section end -->
        
        <div class="limit-box fl-wrap"></div>
        <!--section -->
    </div>
@endsection
