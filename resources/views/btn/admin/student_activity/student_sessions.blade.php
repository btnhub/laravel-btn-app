<h3>Student Sessions</h3>
<p><span style="color:red">Deleting a session will initiate a full refund of a Stripe Connect paid session (both from tutor and btn)</span></p>

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Session Date</th>
			<th>Tutor Name</th>
			<th>Course</th>
			<th>Duration</th>
			<th>Outcome</th>
			<th>Amount Charged</th>
			<th>Tutor Pay Date & Method</th>
			<th>Refund ID</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($student->tutorSessions->sortByDesc('id') as $tutor_session)
			<tr>
				<td>{{$tutor_session->id}} / {{$tutor_session->ref_id}}</td>
				<td>{{$tutor_session->session_date->format('m/d/Y')}}</td>
				<td>{{$tutor_session->tutor->full_name}}</td>
				<td>{{$tutor_session->assignment->course}}</td>
				<td>{{$tutor_session->duration}}</td>
				<td>{{$tutor_session->outcome}}</td>
				<td>${{$tutor_session->amt_charged}}</td>
				<td>
					@if ($tutor_session->tutor_payment_date)
						{{$tutor_session->tutor_payment_date->format('m/d/Y')}} <br>
						{{$tutor_session->payment_method}}
					@endif
				</td>
				<td>{{$tutor_session->refund_id}}</td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route' => ['session.delete', $tutor_session->id], 'id' => 'delete_'.$tutor_session->ref_id, 'class' => 'delete']) !!}
						<button type="submit" class="btn btn-sm btn-danger">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
		        	{!! Form::close() !!}
				</td>
			</tr>	
		@endforeach
		
	</tbody>
</table>


