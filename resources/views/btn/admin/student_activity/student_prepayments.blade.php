<h3>Student Prepayments</h3>
<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Payment Date</th>
			<th>Amount</th>
			<th>Reason</th>
			<th>Status</th>
			<th>Refunded</th>
			<th>Transaction ID</th>
			<th>Notes</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($student->prepayments->sortByDesc('id') as $prepayment)
		<tr>
			<td>{{$prepayment->id}} / {{$prepayment->ref_id}}</td>
			
			<td>{{$prepayment->created_at->format('m/d/Y')}}</td>
			<td>
				@if ($prepayment->discount_amount > 0)
					Amount: ${{$prepayment->amount}}<br>
					Discount: -${{$prepayment->discount_amount}}<br>
					Total: ${{$prepayment->total_amount}}<br>
				@else
					${{$prepayment->total_amount}}<br>
				@endif
			</td>
			<td>{{$prepayment->reason}}</td>
			<td>
				@if ($prepayment->captured && $prepayment->paid)
					Completed
				@else
					<span style="color:red">Incomplete Payment</span>
				@endif
			</td>
			<td>
				@if ($prepayment->refund_requested && $prepayment->issue_refund == NULL)
					<span style="color:red">Refund Requested</span><br>
					Amount: ${{$prepayment->refund_amount}}<br>
				@endif

				@if ($prepayment->issue_refund != NULL)
					@if ($prepayment->issue_refund == 1)
						<b>Refund Issued</b> <br>
						Refund ID: {{$prepayment->refund_id}} / {{$prepayment->stripe_refund_id}}<br>
						Date: {{$prepayment->refund_processed->format('m/d/Y')}}<br>
						Amount: ${{$prepayment->refund_amount}}<br>
					@else
						Refund request rejected on {{$prepayment->refund_processed->format('m/d/Y')}}
					@endif
				@endif
			</td>
			<td>{{$prepayment->transaction_id}}</td>
			<td>{{$prepayment->admin_notes}}</td>
		</tr>
		@endforeach
	</tbody>
</table>