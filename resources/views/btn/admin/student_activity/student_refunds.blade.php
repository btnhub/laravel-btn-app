<h3>Student Refunds</h3>

<table class="table">
	<thead>
		<tr>
			<th>ID / Ref ID</th>
			<th>Stripe ID</th>
			<th>Session ID</th>
			<th>Date Requested</th>
			<th>Tutor Name</th>
			<th>Requested Amount</th>
			<th>Refund Reason</th>
			<th>Refund Method</th>
			<th>Deductions</th>
			<th>Issue Refund</th>
			<th>Refund Destination</th>
			<th>Processed Date</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->refunds as $refund)
			<tr>
				<td>{{$refund->id}} / {{$refund->ref_id}}</td>
				<td>{{$refund->stripe_refund_id}}</td>
				<td>{{$refund->session_id}}</td>
				<td>{{$refund->created_at->format('m/d/Y')}}</td>
				<td><a href="{{ route('user.details.get', ['user_id' => $refund->tutor->id])}}">{{$refund->tutor->full_name}}</a></td>
				<td>{{$refund->requested_amount}}</td>
				<td>
					{{$refund->refund_reason}}<br>
					{{$refund->reason_details}}
				</td>
				<td>
					{{$refund->refund_method}}<br>
					{{$refund->method_details}}
				</td>
				<td>{{$refund->deductions}}</td>
				<td>{{$refund->issue_refund}}</td>
				<td>
				@if ($refund->refunded_to_student)
					To Student: {{$refund->refunded_to_student}}
				@else
					To BTN Account: {{$refund->refunded_to_account}}
				@endif
					
				</td>
				<td>{{$refund->refunded_from_tutor}}</td>
				<td>{{$refund->processed_date}}</td>
			</tr>	
		@endforeach
		
	</tbody>
</table>