<div class="col-md-6">
	<h4>All Time</h4>
	<h4>Summary</h4>
	<table class="table">
		<tbody>
			<tr>
				<td><b>Student Balance</b></td>
				<td>${{$student->studentBalance()}}</td>
			</tr>
			<tr>
				<td><b>Total Charged</b></td>
				<td>${{$student->tutorSessions->sum('amt_charged')}}</td>
			</tr>
			<tr>
				<td><b>Total Refunds</b></td>
				<td>
					To Student: ${{($student->refunds->sum('refund_to_student'))}} <br>
					To BTN Account: ${{($student->refunds->sum('refund_to_amount'))}}
				</td>
			</tr>
			<tr>
				<td><b># Assignments</b></td>
				<td>{{$student->assignments->count()}}</td>
			</tr>
			<tr>
				<td><b># Sessions</b></td>
				<td>{{$student->tutorSessions->count()}}</td>
			</tr>
			<tr>
				<td><b># Declined</b></td>
				<td>{{$student->assignments->where('declined', 1)->count()}}</td>
			</tr>
			<tr>
				<td><b>Evaluations (1st / End)</b></td>
				<td>{{$student->evaluations->where('type', 'First')->count()}} / 
					{{$student->evaluations->where('type', 'End')->count()}}
				</td>
			</tr>
		</tbody>
	</table>
</div>
