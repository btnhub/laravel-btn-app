<h3>Student Assignments</h3>

<table class="table">
	<thead>
		<tr>
			<th>ID / Ref ID</th>
			<th>Tutor</th>
			<th>Course</th>
			{{-- <th>Declined</th> --}}
			<th>Rate</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Delete Assignment</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($student->assignments->sortByDesc('id') as $assignment)
			<tr>
				<td>{{$assignment->id}} / {{$assignment->ref_id}}</td>
				<td><a href="{{ route('user.details.get', ['user_id' => $assignment->tutor->id])}}">{{$assignment->tutor->full_name}}</a></td>
				<td>{{$assignment->course}}
					@if ($assignment->prepaid_balance())
						<br> 
						<span style="color:green;font-weight: bold">Prepaid Balance: ${{$assignment->prepaid_balance()}}</span>
					@endif
				</td>
				{{-- <td>{{$assignment->declined}}</td> --}}
				<td>${{$assignment->tutorContract ? $assignment->tutorContract->student_rate : $assignment->rate}} / hr</td>
				<td>{{$assignment->start_date->format('m/d/Y')}}</td>
				<td>{{$assignment->end_date->format('m/d/Y')}}</td>
				<td>
					@if ($assignment->end_date->isFuture())
						{!! Form::open(['method' => 'DELETE', 'route' => ['assignments.destroy', $assignment->id], 'id' => 'delete_'.$assignment->ref_id, 'class' => 'delete']) !!}
							<button type="submit" class="btn btn-sm btn-danger">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
			        	{!! Form::close() !!}

						@if ($assignment->prepaid_balance())
							<small>Prepaid balance will also be refunded</small>
						@endif
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>