<h3>Student Evaluations</h3>

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Tutor Name</th>
			<th>Course</th>
			<th>Type</th>
			<th>Satisfied</th>
			<th>Frequency</th>
			<th>Tutor Stregths</th>
			<th>Tutor Weakness</th>
			<th>Website Reference</th>
			<th>Review Date</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($student->evaluations as $evaluation)
			<tr>
				<td>{{$evaluation->id}}</td>
				<td><a href="{{ route('user.details.get', ['user_id' => $evaluation->tutor->id])}}">{{$evaluation->tutor->full_name}}</a></td>
				<td>{{$evaluation->assignment->course}}</td>
				<td>{{$evaluation->type}}</td>
				<td>
					@if ($evaluation->satisfied == 1)
						<span style="color:green">Yes</span>	
					@else
						<span style="color:red">No</span>	
					@endif
				</td>
				<td>{{$evaluation->frequency}}</td>
				<td>{{$evaluation->tutor_strength}}</td>
				<td>{{$evaluation->tutor_weakness}}</td>
				<td>{{$evaluation->website_reference}}</td>
				<td>{{$evaluation->review_date}}</td>
			</tr>
		@endforeach
	</tbody>
</table>