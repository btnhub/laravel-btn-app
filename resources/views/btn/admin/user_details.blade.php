@extends('btn.layouts.home')

@section('title')
	{{$user->first_name}} Details
@stop

@section('content')
	<h3>{{$user->full_name}} Details</h3>
		<div class="row">
			<div class="media panel-body col-md-6 col-sm-6">
				<div class="col-xs-4">
					@if ($user->profile->avatar)
						@include('btn.partials.avatar', ['width' => '170px', 'radius' => '0%'])	
					@endif
					<br>
					{!! Form::open(['method' => 'POST', 'route' => ['upload.avatar', $user->id], 'class' => 'form-horizontal media-left', "files" => "true"]) !!}
					    <div class="form-group{{ $errors->has('image_file') ? ' has-error' : '' }}">
					        {!! Form::file('image_file', ['required' => 'required']) !!}
					        <small class="text-danger">{{ $errors->first('image_file') }}</small>
					    </div>
					
					    <div class="btn-group pull-left">
					        {!! Form::submit("Upload", ['class' => 'btn btn-primary']) !!}
					    </div>
					
					{!! Form::close() !!}
				</div>
				<div class="col-xs-8">
					<div class="media-body">
						<table>
							<tr>
								<td><b>ID / Ref ID</b></td>
								<td>{{$user->id}} / {{$user->ref_id}}</td>
							</tr>
							<tr>
								<td><b>Email</b></td>
								<td>{{$user->email}}</td>
							</tr>
							<tr>
								<td><b>Phone</b></td>
								<td>{{$user->phone}}</td>
							</tr>
							<tr>
								<td><b>Member Since: </b></td>
								<td>{{ $user->created_at->format('F jS Y') }}</td>
							</tr>
							<tr>
								<td><b>Last Visit:</b></td>
								<td>{{$user->last_login_date ? $user->last_login_date->diffForHumans() : $user->last_login_date_mobile->diffForHumans()}}</td>
							</tr>
							<tr>
								<td><b>Locations</b></td>
								<td>{{$user->locations->implode('city', ', ')}}</td>
							</tr>
							<tr>
								<td><b>Roles</b></td>
								<td>{{$user->roles->implode('label', ', ')}}</td>
							</tr>						
						
							@if ($user->isTutor())
								<tr>
										<td><b>Tutor Start Date</b></td>
										<td>{{$user->profile->tutor_start_date}}</td>
									</tr>
									<tr>
										<td><b>Tutor End Date</b></td>
										<td>{{$user->profile->tutor_end_date}}</td>
									</tr>
									<tr>
										<td><b>Active Tutor</b></td>
										<td>{!! $user->isActiveTutor() ? "Yes" : "<span style='color:red'>No</span> <small>(Click Edit Profile For Details)</small>" !!}
										</td>
									</tr>
								@if ($user->profile->tutor_status == 1)
									<tr><td colspan="2"><span style="color: green">{{$user->first_name}} Is Accepting Students</span></td></tr>
								@else 
									<tr><td colspan="2"><span style="color: red">{{$user->first_name}} Is No Longer Accepting Students</span></td></tr>
								@endif	
							
								<br>		
							@endif
						</table>
					</div>	
				</div>
				
				
			</div>
			<div class="col-md-4 col-sm-4">
				<table class="table">
					<tbody>
						<tr>
							<td>
								<a href="{{ route('profile.index', ['ref_id' => $user->ref_id])}}" target="_blank">{{$user->user_name}}'s Profile</a>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="{{ route('profile.edit', ['user_ref_id' => $user->ref_id]) }}" type="button" class="btn btn-sm btn-success">
									Edit {{ $user->first_name }}'s Profile
								</a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="{{ route('profile.account', ['ref_id' => $user->ref_id])}}" target="_blank">{{$user->user_name}}'s Account</a>
							</td>
						</tr>
						<tr>
							<td>
								<a href="{{ route('clone.user', ['user_id' => $user->id])}}" target="_blank">Log In As {{$user->user_name}}</a>
							</td>
						</tr>
						<tr>
							<td>
								@if (($user->backgroundCheck ?? '') && $user->backgroundCheck->cleared == 1)
									Background Check Cleared On {{$user->backgroundCheck->cleared_date->format('m/d/Y')}}

								@elseif(($user->backgroundCheck ?? '') && $user->backgroundCheck->paid == 1 && $user->backgroundCheck->cleared == 0)
									Background Check paid on: {{$user->backgroundCheck->payment_date->format('m/d/Y')}}<br>
									<a href="{{ route('clear.applicant', ['user_id' => $user->id])}}"><button class="btn btn-primary pull-left">Clear {{$user->first_name}}</button></a>
									<a href="#"><button class="btn btn-danger pull-right" disabled>Failed Check</button></a>
								@else
									Background Check Not Submitted
								@endif
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	
	@if ($user->isTutor())
		<div id="tutor_info">
			@include('btn.admin.tutor_activity')	
		</div>	
	@endif
	
	@if ($user->isStudent())
		<div id="student_info">
			@include('btn.admin.student_activity')	
		</div>	
	@endif
@stop

@push('scripts')
	<script>
	    $(".delete").on("submit", function(){
	        return confirm("Are you sure you want to delete this session?");
	    });
	</script>
@endpush
