@extends('layouts.josh-front.default', ['page_title' => "BuffTutor Analytics"])

@section('title', "Analytics")

@section('content')			
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>
	<div class="col-md-12">
		<a href="{{route('analytics.tutors')}}">Tutor Activity Data</a><br>
		<a href="{{route('analytics.users')}}">New Registered Users Summary</a>
	</div>

	<div class="col-md-12">
		<div class="col-md-6">
			@include('btn.admin.analytics.app_fees')		
		</div>
		<div class="col-md-6">
			@include('btn.admin.analytics.app_fees_cumulative')		
		</div>
		<div class="col-md-6">
			@include('btn.admin.analytics.weekly_assignments_created')	
		</div>
		<div class="col-md-6">
			@include('btn.admin.analytics.weekly_tutor_requests')
		</div>
		<div class="col-md-6">
			@include('btn.admin.analytics.weekly_tutor_sessions')
		</div>
	</div>

	<div class="col-md-12">
		@include('btn.admin.analytics.sessions_revenue')
	</div>
@stop