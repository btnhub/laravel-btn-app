@extends('layouts.josh-front.default', ['page_title' => "BuffTutor Analytics"])

@section('title', "Tutor Analytics")

@section('content')			
	<div class="col-md-12">
		@include('btn.admin.analytics.tutor_activity')	
	</div>
@stop