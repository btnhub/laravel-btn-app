<h3>New Registered Users ({{date('Y')}})</h3>
<small>Previous year, {{date('Y')-1}}, data in parentheses</small>
<br>
<div class="col-md-6">
	<table class="table">
		<thead>
			<tr>
				<th>Month</th>
				<th>Students</th>
				<th>Tutor Applicants</th>
			</tr>
		</thead>
		<tbody>
			@for ($i = 1; $i < 7; $i++)
				<tr>
					<td>{{$i}}</td>
					<td>
						@foreach ($locations as $location)
							<?php 

								$new_students_count = $registered_students[$i]->filter(function($student) use($location) {
									foreach ($student->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $student;	
									}
									
								})->count();

								$old_students_count = $old_registered_students[$i]->filter(function($student) use($location) {
									foreach ($student->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $student;	
									}
									
								})->count();
								$difference = $new_students_count - $old_students_count;
							?>

							{{$location->city}}:
							{{$new_students_count}} 

							&nbsp;&nbsp;&nbsp;
							
							<span style="color:gray">({{$old_students_count}})</span>
							
							&nbsp;&nbsp;&nbsp;

							@if ($difference != 0 & $old_students_count != 0)
								<span style="{{$difference > 0 ? 'color:green' : 'color:red'}};font-weight: bold">{{$difference > 0 ? "+" : ''}}{{number_format($difference / $old_students_count * 100)}}%</span>
							@endif
							<br>
						@endforeach
					</td>
					<td>
						@foreach ($locations as $location)
							<?php 
								$new_tutors_count = $registered_tutors[$i]->filter(function($tutor) use($location) {
									foreach ($tutor->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $tutor;	
									}
									
								})->count();

								$old_tutors_count = $old_registered_tutors[$i]->filter(function($tutor) use($location) {
									foreach ($tutor->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $tutor;	
									}
									
								})->count();
								$difference = $new_tutors_count - $old_tutors_count;
							?>
							{{$location->city}}:
							{{$new_tutors_count}} 	

							&nbsp;&nbsp;&nbsp;

							<span style="color:gray">
								({{$old_tutors_count}})
							</span>				
							&nbsp;&nbsp;&nbsp;

							@if ($difference != 0 & $old_tutors_count != 0)
								<span style="{{$difference > 0 ? 'color:green' : 'color:red'}};font-weight: bold">{{$difference > 0 ? "+" : ''}}{{number_format($difference / $old_tutors_count * 100)}}%</span>
							@endif
							<br>
						@endforeach
					</td>
				</tr>	
			@endfor
		</tbody>
	</table>
</div>
<div class="col-md-6">
	<table class="table">
		<thead>
			<tr>
				<th>Month</th>
				<th>Students</th>
				<th>Tutor Applicants</th>
			</tr>
		</thead>
		<tbody>
			@for ($i = 7; $i < 13; $i++)
				<tr>
					<td>{{$i}}</td>
					<td>
						@foreach ($locations as $location)
							<?php 

								$new_students_count = $registered_students[$i]->filter(function($student) use($location) {
									foreach ($student->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $student;	
									}
									
								})->count();

								$old_students_count = $old_registered_students[$i]->filter(function($student) use($location) {
									foreach ($student->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $student;	
									}
									
								})->count();
								$difference = $new_students_count - $old_students_count;
							?>

							{{$location->city}}:
							{{$new_students_count}} 

							&nbsp;&nbsp;&nbsp;
							
							<span style="color:gray">({{$old_students_count}})</span>
							
							&nbsp;&nbsp;&nbsp;

							@if ($difference != 0 & $old_students_count != 0)
								<span style="{{$difference > 0 ? 'color:green' : 'color:red'}};font-weight: bold">{{$difference > 0 ? "+" : ''}}{{number_format($difference / $old_students_count * 100)}}%</span>
							@endif
							<br>
						@endforeach
					</td>
					<td>
						@foreach ($locations as $location)
							<?php 
								$new_tutors_count = $registered_tutors[$i]->filter(function($tutor) use($location) {
									foreach ($tutor->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $tutor;	
									}
									
								})->count();

								$old_tutors_count = $old_registered_tutors[$i]->filter(function($tutor) use($location) {
									foreach ($tutor->locations()->get() as $loc){
										if ($loc->id == $location->id)
											return $tutor;	
									}
									
								})->count();
								$difference = $new_tutors_count - $old_tutors_count;
							?>
							{{$location->city}}:
							{{$new_tutors_count}} 	

							&nbsp;&nbsp;&nbsp;

							<span style="color:gray">
								({{$old_tutors_count}})
							</span>				
							&nbsp;&nbsp;&nbsp;

							@if ($difference != 0 & $old_tutors_count != 0)
								<span style="{{$difference > 0 ? 'color:green' : 'color:red'}};font-weight: bold">{{$difference > 0 ? "+" : ''}}{{number_format($difference / $old_tutors_count * 100)}}%</span>
							@endif
							<br>
						@endforeach
					</td>
				</tr>	
			@endfor
		</tbody>
	</table>
</div>