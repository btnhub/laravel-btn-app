<h2>Tutor Activity</h2>
{{count($active_tutors)}} Tutors
<table class="table table-striped table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>Tutor</th>
			<th>Assigned</th>
			<th># Met</th>
			<th>Sessions</th>
			<th>Contracts</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($active_tutors as $tutor)
			<tr>
				<td>{{$tutor->id}}</td>
				<td><a href="{{ route('user.details.get', ['user_id' => $tutor->id])}}" target="_blank">{{$tutor->fullName}}</a>
					@if ($tutor->profile->tutor_end_date < Carbon\Carbon::now()->addMonth())
						<br>
						<span style="color:red"> 
							@if ($tutor->profile->tutor_end_date < Carbon\Carbon::now())
								Left
							@else
								Leaving
							@endif
							{{Carbon\Carbon::parse($tutor->profile->tutor_end_date)->diffForHumans()}}
						</span>
					@endif
					<br>
					{{$tutor->locations->pluck('city')->implode(', ')}}
				</td>
				{{-- <td>{{$tutor->assignments()->active()->count()}}</td> --}}
				<td>{{$tutor->active_assignments_count ?? 0}}</td>
				<td>{{$tutor->activeTutorSessions->unique('assignment_id')->values()->count()}}</td>
				<td>{{$tutor->activeTutorSessions->count()}} / ${{$tutor->activeTutorSessions->sum('amt_charged')}}</td>
				<td>
					<?php $recent_contracts = $tutor->tutor_contracts()->where('end_date', '>', Carbon\Carbon::now()->format('Y-m-d'))->get()?>
					@if ($contracts_count = $recent_contracts->count())
						{{$contracts_count}} Recent Tutor Contracts <br>
						@if ($missed_contracts = $recent_contracts->where('tutor_decision', NULL)->count())
							<span style="color:red">{{$missed_contracts}} Missed Tutor Contracts </span><br>
						@endif
						
						@if ($declined_contracts = $recent_contracts->where('tutor_decision', 0)->count())
							<span style="color:darkred">{{$declined_contracts}} Declined Tutor Contracts </span>
						@endif
					@endif
				</td>
			</tr>	
		@endforeach
		
	</tbody>
</table>