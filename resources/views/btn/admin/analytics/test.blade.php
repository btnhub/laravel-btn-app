<!DOCTYPE html>
<html>
<head>
	<title>Chart</title>
</head>
<body>
	<canvas id="myChart"></canvas>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>

	<script>
		var ctx = document.getElementById("myChart");
		var data = {
		    labels: ["January", "February", "March", "April", "May", "June", "July"],
		    datasets: [
		        {
		            label: "My First dataset",
		            fill: true, // fill area under curve
		            lineTension: 0.1, // 0 = straigt lines.  If removed, draws curvy lines between data points
		            backgroundColor: "rgba(75,192,192,0.4)", //color of fill under graph
		            borderColor: "rgba(75,192,192,1)", //color of line
		            borderCapStyle: 'butt', //CAP Style of line https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineCap
		            borderDash: [], //length and spacing of dashes https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setLineDash
		            borderDashOffset: 0.0, //offset of line dashes 
		            borderJoinStyle: 'miter', //line joint style https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineJoin
		            pointBorderColor: "rgba(75,192,192,1)", // color of data points
		            pointBackgroundColor: "#fff", //fill color for points
		            pointBorderWidth: 1, //width of point border
		            pointHoverRadius: 5, //radius of point when hovered
		            pointHoverBackgroundColor: "rgba(75,192,192,1)", //point background color when point is hovered
		            pointHoverBorderColor: "rgba(220,220,220,1)", //point borfer color when point is hovered
		            pointHoverBorderWidth: 2,
		            pointRadius: 1,
		            pointHitRadius: 10,
		            data: [65, 59, 80, 81, 56, 55, 40], //data to plot
		            /*showline: false  // if false then line is not drawn for dataset*/
		            spanGaps: false, //If true, lines will be drawn between points with no or null data
		        }
		    ]
		};

		var options = {
		        scales: {
		            yAxes: [{
		                ticks: {
		                    beginAtZero:true
		                }
		            }]
		        }
		    }
		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: data,
		    /*options: options*/
		});
	</script>
</body>
</html>