@extends('layouts.josh-front.default', ['page_title' => "BuffTutor Analytics"])

@section('title', "User Analytics")

@section('content')	
	<div class="col-md-12">
		@include('btn.admin.analytics.users')	
	</div>
@stop