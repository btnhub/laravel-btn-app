
<h3>Assignments Created</h3>

<canvas id="assign_created"></canvas>
<script>
	<?php $i = 0; ?>
	var ctx = document.getElementById("assign_created");
	var data = {
	    labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53],
	    datasets: [
   	    	@foreach ($assignments as $year => $weeks)
				<?php $i++; ?>
				{
		            label: {{$year}},
		            data: [{!!implode(', ', array_column($weeks, 'assign_count'))!!}],
		            fill: true, // fill area under curve
		            lineTension: 0.1,
		            backgroundColor: "{{$colors[$i]}}",
        			borderColor: "{{$border_colors[$i]}}",
	        	},	        	
	        @endforeach
	    ]
	};

	var options = {
        title: {
        	display: true,
        	text: "Weekly Assignments Created"
        }
    };

	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: data,
	    options: options
	});
</script>
