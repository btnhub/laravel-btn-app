
<h3>Tutor Sessions Summary</h3>

{{-- Total Charged--}}
<div class="col-md-6">
	<canvas id="amount_charged"></canvas>
</div>
<div class="col-md-6">
	<canvas id="amount_cumulative"></canvas>
</div>
{{-- Total Sessions--}}
<div class="col-md-6">
	<canvas id="session_count"></canvas>
</div>
<div class="col-md-6">
	<canvas id="session_cumulative"></canvas>
</div>
<script>
	<?php $i = 0; ?>
	var ctx = document.getElementById("amount_charged");
	var data = {
	    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	    datasets: [
	    	@foreach ($revenue as $year => $months)
				<?php $i++; ?>
				{
		            label: {{$year}},
		            data: [{!!implode(', ', array_column($months, 'charged'))!!}],
		            fill: true, // fill area under curve
		            lineTension: 0.1,
		            backgroundColor: "{{$colors[$i]}}",
        			borderColor: "{{$border_colors[$i]}}",
	        	},	        	
	        @endforeach
	    ]
	};
	var options = {
        title: {
        	display: true,
        	text: "Monthly Total Amount Charged"
        }
    };

	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: data,
	    options: options
	});
</script>

<script>
	<?php $i = 0; ?>
	var ctx = document.getElementById("amount_cumulative");
	var data = {
	    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	    datasets: [
	    	@foreach ($revenue_cumulative as $year => $months)
				<?php $i++; ?>
				{
		            label: {{$year}},
		            data: [{!!implode(', ', array_column($months, 'charged'))!!}],
		            fill: true, // fill area under curve
		            lineTension: 0.1,
		            backgroundColor: "{{$colors[$i]}}",
        			borderColor: "{{$border_colors[$i]}}",
	        	},	        	
	        @endforeach
	    ]
	};
	var options = {
        title: {
        	display: true,
        	text: "Cumulative Total Amount Charged"
        }
    };

	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: data,
	    options: options
	});
</script>

<script>
	<?php $i = 0; ?>
	var ctx = document.getElementById("session_count");
	var data = {
	    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	    datasets: [
	    	@foreach ($revenue as $year => $months)
				<?php $i++; ?>
				{
		            label: {{$year}},
		            data: [{!!implode(', ', array_column($months, 'session_count'))!!}],
		            fill: true, // fill area under curve
		            lineTension: 0.1,
		            backgroundColor: "{{$colors[$i]}}",
        			borderColor: "{{$border_colors[$i]}}",
	        	},	        	
	        @endforeach
	    ]
	};
	var options = {
        title: {
        	display: true,
        	text: "Monthly Hours of Sessions"
        }
    };

	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: data,
	    options: options
	});
</script>

<script>
	<?php $i = 0; ?>
	var ctx = document.getElementById("session_cumulative");
	var data = {
	    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	    datasets: [
	    	@foreach ($revenue_cumulative as $year => $months)
				<?php $i++; ?>
				{
		            label: {{$year}},
		            data: [{!!implode(', ', array_column($months, 'session_count'))!!}],
		            fill: true, // fill area under curve
		            lineTension: 0.1,
		            backgroundColor: "{{$colors[$i]}}",
        			borderColor: "{{$border_colors[$i]}}",
	        	},	        	
	        @endforeach
	    ]
	};
	var options = {
        title: {
        	display: true,
        	text: "Cumulative Session Hours"
        }
    };

	var myChart = new Chart(ctx, {
	    type: 'line',
	    data: data,
	    options: options
	});
</script>