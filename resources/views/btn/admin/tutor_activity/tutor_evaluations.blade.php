<h3>Tutor Evaluations</h3>

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Student Name</th>
			<th>Course</th>
			<th>Type</th>
			<th>Satisfied</th>
			<th>Frequency</th>
			<th>Tutor Stregths</th>
			<th>Tutor Weakness</th>
			<th>Website Reference</th>
			<th>Review Date</th>
		</tr>
	</thead>
	<tbody>	
			@foreach ($tutor->evaluations as $evaluation)
				<tr>
					<td>{{$evaluation->id}}</td>
					<td>
						@if ($evaluation->student)
							<a href="{{ route('user.details.get', ['user_id' => $evaluation->student->id])}}">{{$evaluation->student->full_name}}</a>
						@endif
						
					</td>
					<td>
						@if ($evaluation->assignment)
							{{$evaluation->assignment->course}}	
						@endif
						
					</td>
					<td>{{$evaluation->type}}</td>
					<td>
						@if ($evaluation->satisfied == 1)
							<span style="color:green">Yes</span>	
						@else
							<span style="color:red">No</span>	
						@endif
					</td>
					<td>{{$evaluation->frequency}}</td>
					<td>{{$evaluation->tutor_strength}}</td>
					<td>{{$evaluation->tutor_weakness}}</td>
					<td>{{$evaluation->website_reference}}</td>
					<td>{{$evaluation->review_date}}</td>
				</tr>
			@endforeach
	</tbody>
</table>