	<h3>Tutor Sessions</h3>
<p><span style="color:red">Deleting a session will initiate a full refund of a Stripe Connect paid session (both from tutor and btn)</span></p>

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Session Date</th>
			<th>Student Name</th>
			<th>Course</th>
			<th>Duration</th>
			<th>Outcome</th>
			<th>Amount Charged</th>
			<th>Tutor Pay</th>
			<th>Tutor Pay Date</th>
			<th>Method</th>
			<th>Refund ID</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php $tutor->tutorSessions->sortByDesc('id'); ?>
		@foreach ($tutor->tutorSessions as $tutor_session)
			<tr>
				<td>{{$tutor_session->id}}</td>
				<td>{{$tutor_session->session_date->format('m/d/Y')}}</td>
				<td>
					@if ($tutor_session->student)
						{{$tutor_session->student->full_name}}
					@endif
				</td>
				<td>
					@if ($tutor_session->assignment)
						{{$tutor_session->assignment->course}}
					@endif
					
				</td>
				<td>{{$tutor_session->duration}} hr</td>
				<td>{{$tutor_session->outcome}}</td>
				<td>{{$tutor_session->amt_charged}}</td>
				<td>{{$tutor_session->tutor_pay}}</td>
				<td>
					@if ($tutor_session->tutor_payment_date)
						{{$tutor_session->tutor_payment_date->format('m/d/Y')}} <br>
					@endif
				</td>
				<td>
					{{$tutor_session->payment_method}}<br>
					@if ($tutor_session->stripe_payment_id)
						{{$tutor_session->stripe_payment_id}} / {{$tutor_session->stripe_transfer_id}}
					@endif
					
				</td>
				<td>{{$tutor_session->refund_id}}</td>
				<td>
					{!! Form::open(['method' => 'DELETE', 'route' => ['session.delete', $tutor_session->id], 'id' => 'delete_'.$tutor_session->ref_id, 'class' => 'delete']) !!}
						<button type="submit" class="btn btn-sm btn-danger">
							<span class="glyphicon glyphicon-trash"></span>
						</button>
		        	{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
