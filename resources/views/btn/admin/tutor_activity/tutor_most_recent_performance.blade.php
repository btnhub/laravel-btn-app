<h3>Most Recent Performace</h3>

<?php $recent_assignments = $tutor->recentPreviousAssignments(); ?>
<table class="table">
	<tbody>
		<tr>
			<td>Semester</td>
			<td>{{$tutor->recentSemester()}}</td>
		</tr>
		<tr>
			<td># Assignments</td>
			<td>
				Total: {{$total = $recent_assignments->count()}}<br>
				With Sessions: {{$recent_assignments->filter(function ($assign)			 		 {
                                        return $assign->tutorSessions()->count() > 0;
                                    })
                                    ->count()}}<br>
				Without Sessions: {{$recent_assignments->filter(function ($assign) {
                                        return $assign->tutorSessions()->count() == 0;
                                    })
                                    ->count()}}
			</td>
		</tr>
		<tr>
			<td># Sessions</td>
			<td>
				<?php $sessions_count = 0;?>
				@foreach ($recent_assignments as $assign)
					<?php $sessions_count += $assign->tutorSessions()->count();
					?>
				@endforeach
					{{$sessions_count}}
			</td>
		</tr>
		<tr>
			<td># First Eval</td>
			<td>
				Positive: {{$recent_assignments->filter(function ($assign)			 		 {
                                        return $assign->evaluation()->where('type', 'First')->where('satisfied', 1)->count() > 0;
                                    })
                                    ->count()}}<br>
				Negative: {{$recent_assignments->filter(function ($assign)			 		 {
                                        return $assign->evaluation()->where('type', 'First')->where('satisfied', 0)->count() > 0;
                                    })
                                    ->count()}}<br>
			</td>
		</tr>
		<tr>
			<td># End Eval</td>
			<td>
				Positive: {{$positive_end = $recent_assignments->filter(function ($assign)			 		 {
                                        return $assign->evaluation()->where('type', 'End')->where('satisfied', 1)->count() > 0;
                                    })
                                    ->count()}}<br>
				Negative: {{$recent_assignments->filter(function ($assign)			 		 {
                                        return $assign->evaluation()->where('type', 'End')->where('satisfied', 0)->count() > 0;
                                    })
                                    ->count()}}<br>
				
			</td>
		</tr>
		<tr>
			<td>
				Positive Eval / Assignments
			</td>
			<td>
				{{$tutor->recentEvalRatio() *100 }} %
				<br>
				{!! $tutor->recentEvalRatio() < 0.3 ? "<span style='color:red'><b>Tutor Cannot Return <br>Until 30% Is Reached</b></span>" : "" !!}
			</td>
		</tr>

	</tbody>
</table>