<div class="col-md-6">
	<h4>All Time</h4>
	<h4>Summary</h4>
	<table class="table">
		<tbody>
			<tr>
				<td><b>Total Earned</b></td>
				<td>${{$tutor->tutorSessions->sum('tutor_pay')}}</td>
			</tr>
			<tr>
				<td><b>Total Paid</b></td>
				<td>${{$tutor->contractorPayments->sum('amount_paid')}}</td>
			</tr>
			<tr>
				<td><b>Total Refunds</b></td>
				<td>{!!($tutor->refunds->sum('refunded_from_tutor')) ? "<span style='color:red'>$".$tutor->refunds->sum('refunded_from_tutor')."</span":""!!}</td>
			</tr>
			<tr>
				<td><b># Assignments</b></td>
				<td>{{$tutor->assignments->count()}}</td>
			</tr>
			<tr>
				<td><b># Met</b></td>
				<td>{{$tutor->tutorSessions->unique('assignment_id')->count()}}</td>
			</tr>
			<tr>
				<td><b># Sessions</b></td>
				<td>{{$tutor->tutorSessions->count()}}</td>
			</tr>
			<tr>
				<td><b># Declined</b></td>
				<td>{{$tutor->assignments->where('declined', 1)->count()}}</td>
			</tr>
			<tr>
				<td><b># Deleted</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b>Evaluations (1st / End)</b></td>
				<td>{{$tutor->evaluations->where('type', 'First')->count()}} / 
					{{$tutor->evaluations->where('type', 'End')->count()}}
					</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="col-md-6">
	<h4>Active Sessions</h4>  
	<h4> Next Earned: {!! ($tutor->nextPaymentEarned()) ? "<span style='color:blue'>$".$tutor->nextPaymentEarned()."</span>" : "$0" !!} &nbsp;&nbsp;&nbsp;
	 Next Withheld: {!! ($tutor->delinquentSessions()->sum('tutor_pay')) ? "<span style='color:red'>$".$tutor->delinquentSessions()->sum('tutor_pay')."</span>" : "$0" !!}</h4>
	<table class="table">
		<tbody>
			<tr>
				<td><b>Total Earned</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b>Total Paid</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b>Total Refunds</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b># Assignments</b></td>
				<td>{{$tutor->assignments()->active()->count()}}</td>
			</tr>
			<tr>
				<td><b># Met</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b># Sessions</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b># Declined</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b># Deleted</b></td>
				<td></td>
			</tr>
			<tr>
				<td><b>Evaluations (1st / End)</b></td>
				<td></td>
			</tr>
		</tbody>
	</table>
</div>

@if (!$tutor->newTutor() && $tutor->recentPreviousAssignments())
	<div class="row">
		<div class="col-md-6">
			<h4>Most Recent Semester</h4>
			@include('btn.admin.tutor_activity.tutor_most_recent_performance')
		</div>	
	</div>
@endif

