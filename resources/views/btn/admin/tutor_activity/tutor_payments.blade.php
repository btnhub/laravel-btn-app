<h3>Tutor Payments</h3>
<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Payment Date</th>
			<th>Tutor Name</th>
			<th>Amount Paid</th>
			<th>Method</th>
			<th>Amount Withheld</th>
			<th>Withhold Reason</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->contractorPayments as $tutor_payment)
		<tr>
			<td>{{$tutor_payment->id}}</td>
			<td>{{$tutor_payment->created_at->format('m/d/Y')}}</td>
			<td>{{$tutor_payment->first_name}} {{$tutor_payment->last_name}}</td>
			<td>${{$tutor_payment->amount_paid}}</td>
			<td>{{$tutor_payment->method}}</td>
			<td>{{($tutor_payment->amount_withheld) ? "$".$tutor_payment->amount_withheld : ""}}</td>
			<td>{{$tutor_payment->withhold_reason}}</td>
		</tr>
		@endforeach
	</tbody>
</table>