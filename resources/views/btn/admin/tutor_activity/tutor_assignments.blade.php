<h3>Tutor Assignments</h3>

<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Student Name</th>
			<th>Course</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Active</th>
			<th>Rate</th>
			<th>Student Balance</th>
			<th>Assigned By</th>
			<th>Delete</th>
			{{-- <th>Declined</th> --}}
			{{-- <th>Reassigned</th> --}}
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->assignments->sortByDesc('id') as $assignment)
			<tr>
				<td>{{$assignment->id}}</td>
				<td>
					@if ($assignment->student)
						<a href="{{ route('user.details.get', ['user_id' => $assignment->student->id])}}">{{$assignment->student->full_name}}</a>
					@endif
				</td>
				<td>{{$assignment->course}}
					@if ($assignment->prepaid_balance())
						<br> 
						<span style="color:green;font-weight: bold">Prepaid Balance: ${{$assignment->prepaid_balance()}}</span>
					@endif
				</td>
				<td>{{$assignment->start_date->format('m/d/Y')}}</td>
				<td>{{$assignment->end_date->format('m/d/Y')}}</td>
				<td>
				 {{($tutor->assignments()->active()->pluck('id')->contains($assignment->id)) ? "Yes" : "No"}}
				</td>
				<td>
					@if ($assignment->tutorContract)
						${{$assignment->tutorContract->student_rate}} / hr
						<br>
						${{$assignment->tutorContract->tutor_rate}} / hr
					@else
						${{$assignment->rate}} / hr
					@endif
				</td>
				<td>
					@if ($assignment->student && $assignment->student->studentBalance() < 0)
							{!!"<span style='color:red'> $".$assignment->student->studentBalance()."</span>"!!}
					@elseif ($assignment->student && $assignment->student->studentBalance() > 0)
						{!!"<span style='color:blue'> $".$assignment->student->studentBalance()."</span>"!!}
					@endif	
					
				</td>
				<td>{{App\User::find($assignment->assigned_by)->fullName}}</td>
				<td>
					@if ($assignment->end_date->isFuture())
						{!! Form::open(['method' => 'DELETE', 'route' => ['assignments.destroy', $assignment->id], 'id' => 'delete_'.$assignment->ref_id, 'class' => 'delete']) !!}
							<button type="submit" class="btn btn-sm btn-danger">
								<span class="glyphicon glyphicon-trash"></span>
							</button>
			        	{!! Form::close() !!}

						@if ($assignment->prepaid_balance())
							<small>Prepaid balance will also be refunded</small>
						@endif
					@endif
				</td>
				{{-- <td>{!!($assignment->declined) ? "<span style='color:red'>Yes</span>": "No"!!}</td> --}}
				{{-- <td>{{$assignment->reassigned_to}}</td> --}}
			</tr>
		@endforeach
	</tbody>
</table>