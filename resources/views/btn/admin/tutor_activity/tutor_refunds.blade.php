<h3>Tutor Refunds</h3>

<table class="table">
	<thead>
		<tr>
			<th>ID / Ref ID</th>
			<th>Stripe ID</th>
			<th>Session ID</th>
			<th>Date Requested</th>
			<th>Student Name</th>
			<th>Requested Amount</th>
			<th>Refund Reason</th>
			<th>Refunded From Tutor</th>
			<th>Processed Date</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->refunds as $refund)
			<tr>
				<td>{{$refund->id}} / {{$refund->ref_id}}</td>
				<td>{{$refund->stripe_refund_id}}</td>
				<td>{{$refund->session_id}}</td>
				<td>{{$refund->created_at->format('m/d/Y')}}</td>
				<td><a href="{{ route('user.details.get', ['user_id' => $refund->student->id])}}">{{$refund->student->full_name}}</a></td>
				<td>{{$refund->requested_amount}}</td>
				<td>
					{{$refund->refund_reason}}<br>
					{{$refund->reason_details}}
				</td>
				<td>{{$refund->refunded_from_tutor}}</td>
				<td>{{$refund->processed_date}}</td>
			</tr>	
		@endforeach
		
	</tbody>
</table>