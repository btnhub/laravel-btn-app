@extends('layouts.josh-front.default', ['page_title' => "Mogi's Calendar"])

@section('title', "Mogi's Calendar")

@section('content')
	<div class="card card-block" align="center">
		@include('btn.profile.calendar', ['timezone' => "America/Denver", 'calendar_id' => "bufftutor@gmail.com"])
	</div>

@stop