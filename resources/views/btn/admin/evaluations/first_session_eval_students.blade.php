@extends('layouts.josh-front.default', ['page_title' => "Students For First Session Email"])

@section('title', "Students For First Session Email")

@section('content')			
	<h2>Students w/o Matches or First Session Eval</h2>
	Lists students who contacted tutors using the Inbox/MarketPlace but have no assignment and students who have an assignment, but not first session eval (only students with 0-2 sessions listed).

<br><br>
	<b>{{count($students)}} Students</b>
	<table class="table">
		<thead>
			<tr>
				<th>Student</th>
				<th>Student Contact</th>
				<th>Details</th>

			</tr>
		</thead>
		<tbody>
			@foreach ($students as $student_id => $details)
				<tr>
					<td><a href="{{route('user.details.get', ['user_id' => $student_id])}}">{{App\User::find($student_id)->full_name}}</a></td>
					<td>{{App\User::find($student_id)->email}} / {{App\User::find($student_id)->phone}}</td>
					<td>
						@foreach ($details as $key => $detail)
							{{$detail}}
							<br>
						@endforeach
					</td>
				</tr>
			@endforeach
			
		</tbody>
	</table>
@stop