@extends('layouts.josh-front.default', ['page_title' => "End Evaluations Reminders"])

@section('title', "End Evaluations Reminders")

@section('content')
	<a href="{{route('show.evaluations')}}"><-- Back To Evaluations</a><br>
	<h3>Assignments Without End Semester Evaluation</h3>

	{{-- Form To Select Date Range --}}
	Select a range of dates (i.e. narrow down semester)
	<div class="row">
		{!! Form::open(['method' => 'GET', 'route' => ['eval.reminder', 'type' => 'End'], 'class' => 'form-horizontal']) !!}
		
			<div class="row">
				<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }} col-md-2">
			        {!! Form::label('start_date', 'Start Date') !!}
			        {!! Form::date('start_date', request('start_date') ?? Carbon\Carbon::now()->subMonths(5), ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('start_date') }}</small>
			    </div>

			    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }} col-md-2	">
			        {!! Form::label('end_date', 'End Date') !!}
			        {!! Form::date('end_date', request('end_date') ?? Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('end_date') }}</small>
			    </div>	
			</div>
		
		    <div class="btn-group pull-left">
		        {!! Form::submit("Search", ['class' => 'btn btn-success']) !!}
		    </div>
		
		{!! Form::close() !!}
	</div>
	
	<hr>
	
	{{$assignments->count()}} Assignments between <span style="color:blue; font-weight:bold">{{$start_date->format('m/d/Y')}} and {{$end_date->format('m/d/Y')}}</span> have not been reminded.
	
	{{-- Form To Send Email Reminders --}}
	<div class="row">
		{!! Form::open(['method' => 'POST', 'route' => ['send.eval.reminders', 'type' => 'End'], 'class' => 'form-horizontal']) !!}
				@foreach ($assignments as $assign)
					{!! Form::hidden('assign_ids[]', $assign->id) !!}
				@endforeach	
		    @if ($assignments->count() == 0)
		    	<div class="btn-group pull-right">
			        {!! Form::submit("Send Out Email Reminders ({$assignments->count()})", ['class' => 'btn btn-warning', 'disabled' => 'true']) !!}
			    </div>
			@else
				<div class="btn-group pull-right">
			        {{-- {!! Form::submit("Send Out Email Reminders ({$assignments->unique('student_id')->count()} Students)", ['class' => 'btn btn-primary']) !!} --}}
			        {!! Form::submit("Send Out Email Reminders ({$assignments->count()} Students)", ['class' => 'btn btn-primary']) !!}
			    </div>
		    @endif
		    

		{!! Form::close() !!}
	</div>
	
	<table class="table">
		<thead>
			<tr>
				<th>Assign Details</th>
				<th>Location</th>
				<th>Tutor Name</th>
				<th>Student Name</th>
				<th>Course</th>
				<th># of Sessions</th>
				<th>First Eval</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($assignments->sortBy(function ($q_assign){
						return $q_assign->tutor->fullName;
						}) 
						as $assign)
				<tr>
					<td>
						<b>ID:</b> {{$assign->id}}<br>
						<b>Start:</b> {{$assign->start_date->format('m/d/Y')}}<br>
						<b>End:</b> {{$assign->end_date->format('m/d/Y')}}
					</td>
					<td>{{$assign->location->city}}</td>
					<td><a href="{{ route('user.details.get', ['user_id' => $assign->tutor_id])}}">{{$assign->tutor->fullName}}</a></td>
					<td><a href="{{ route('user.details.get', ['user_id' => $assign->student_id])}}">{{$assign->student->fullName}}</a></td>
					<td>{{$assign->course}}</td>
					<td>{{$assign->tutorSessions->count()}}</td>
					<td>
						@if ($assign->evaluation->where('type', 'First')->count() > 0)
							{!!$assign->evaluation->where('type', 'First')->first()->satisfied ? '<span style="color:green">Satisfied</span>' : '<span style="color:red">Not Satisfied</span>'!!}
						@endif
					</td>
					<td>
						{!! Form::open(['method' => 'POST', 'route' => ['mark.as.reminded', 'type' => 'End', 'assign_id' => $assign->id], 'class' => 'form-horizontal']) !!}
						    <div class="btn-group pull-right">
						        {!! Form::submit("Do Not Remind", ['class' => 'btn btn-danger']) !!}
						    </div>
						{!! Form::close() !!}
					</td>
				</tr>	
			@endforeach
		</tbody>
	</table>
@endsection