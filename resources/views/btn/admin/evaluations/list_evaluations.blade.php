@extends('layouts.josh-front.default', ['page_title' => "Tutor Evaluations"])

@section('title', "Evaluations")

@section('content')			
	<a href="{{route('eval.reminder', ['type' => 'First'])}}">First Session Eval Reminder</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="{{route('eval.reminder', ['type' => 'End'])}}">End Semester Eval Reminder</a>
	<h2>Active Tutors Recent Evaluation Performance</h2>
	30% cutoff for positive evaluations
	
	<table class="table">
		<thead>
			<tr>
				<th>Tutor</th>
				<th>Previous Semester</th>
				<th># Assignments (Met/Total)</th>
				<th>% Positive Eval</th>
				<th># Negative Reviews</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($active_tutors as $tutor)
				<tr>
					<td>{{$tutor->fullName}}</td>
					<td>{{$tutor->recentSemester()}}</td>
					<td> 
						@if ($tutor->newTutor())
							- 	
						@else
							@if ($tutor->recentPreviousAssignments())
								{{$tutor->recentPreviousAssignments()->filter(function ($assign)                    
									{
	                                 return $assign->tutorSessions()->count() > 0;
	                             	})->count()}} 
	                             / {{$tutor->recentPreviousAssignments()->count()}}
							@endif
						@endif
						
					</td>
					<td>
						@if ($tutor->newTutor())
							-
						@else
							@if ($tutor->recentEvalRatio() > 0.3)
								{{$tutor->recentEvalRatio() * 100}}%
							@else
								<span style="color:red">
									{{$tutor->recentEvalRatio() * 100}}%
								</span>
							@endif
						@endif
						
						
					</td>
					<td>
						@if ($tutor->newTutor())
							-
						@else
							<?php 
								$negative_count = $tutor->recentPreviousAssignments()? $tutor->recentPreviousAssignments()->filter(function ($assign)			 		 {
	                                        return $assign->evaluation()->where('type', 'End')->where('satisfied', 0)->count() > 0;
	                                    })
	                                    ->count() : 0;
							?>
							{!!$negative_count > 0 ? "<span style='color:red'>$negative_count</span>" : $negative_count!!}
						@endif
						
					</td>
				</tr>
			@empty
				<tr>
					<td> No Tutors This Semester</td>
				</tr>
			@endforelse
			
		</tbody>
	</table>
	<hr>
	<h2>List of Unreviewed Tutor Evaluations</h2>
	{{$evaluations->count()}} Unreviewed Evaluations
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Tutor</th>
				<th>Student</th>
				<th>Course</th>
				<th>Eval Type</th>
				<th>Satisfied</th>
				<th>Frequency</th>
				<th>Strengths</th>
				<th>Weakness</th>
				<th>Reference</th>
				<th>Add To Homepage</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($evaluations as $eval)
				<tr>
					<td>{{$eval->id}}</td>
					<td>{{$eval->tutor ? $eval->tutor->fullName : ''}}</td>
					<td>{{$eval->student ? $eval->student->fullName : ''}}</td>
					<td>{{$eval->assignment ? $eval->assignment->course : ''}}</td>
					<td>{{$eval->type}}</td>
					<td>
						@if ($eval->satisfied)
							Yes
						@else
							<span style="color:red">No</span>
						@endif
					</td>
					<td>{{$eval->frequency}}</td>
					<td>{{$eval->tutor_strength}}</td>
					<td>{{$eval->tutor_weakness}}</td>
					<td>{{$eval->website_reference}}</td>
					<td>
						{!! Form::open(['method' => 'PATCH', 'route' => ['review.evaluation', $eval->id], 'id' => 'review_'.$eval->id]) !!}
							<div class="form-group">
							    <div class="checkbox{{ $errors->has('homepage_visible') ? ' has-error' : '' }}">
							        <label for="homepage_visible">
							            {!! Form::checkbox('homepage_visible', '1', null) !!} Add To Homepage
							        </label>
							    </div>
							    <small class="text-danger">{{ $errors->first('homepage_visible') }}</small>
							</div>

							<button type="submit" class="btn btn-sm btn-primary">
								<span class="glyphicon glyphicon-check"></span>
							</button>
							
			        	{!! Form::close() !!}
					</td>
				</tr>
			@endforeach
			
		</tbody>
	</table>
@stop