@extends('layouts.josh-front.default', ['page_title' => "Marketing Breakdown"])

@section('title', "Marketing Breakdown")

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script>

{{--Students Breakdown--}}
	<h2>Students</h2>
	@include('btn.admin.marketing.breakdown_students')
	<hr>

{{--Tutors Breakdown--}}
	<h2>Tutors</h2>
	@include('btn.admin.marketing.breakdown_tutors')
@endsection