@foreach ($tutor_data as $year => $months)
	<h2>{{$year}}</h2>
	
	<canvas id="tutors{{$year}}"></canvas>

	<script>
		<?php $i = 0; ?>
		var ctx = document.getElementById("tutors{{$year}}");
		var data = {
		    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		    datasets: [
		    	@foreach ($marketing_methods as $method)
		    		<?php $i = ($i + 1 <= count($colors)) ? $i+1 : 1; ?>
					{
			            label: "{{$method}}",
			            data: [{!!implode(', ', array_column($months, $method))!!}],
			            fill: true, // fill area under curve
			            lineTension: 0.1,
			            backgroundColor: "{{$colors[$i]}}",
	        			borderColor: "{{$border_colors[$i]}}",
		        	},	        	
		        @endforeach
		    ]
		};
		var options = {
	        title: {
	        	display: true,
	        	text: "{{$year}} Marketing Breakdown - Tutors"
	        }
	    };

		var myChart = new Chart(ctx, {
		    type: 'line',
		    data: data,
		    options: options
		});
	</script>
@endforeach
