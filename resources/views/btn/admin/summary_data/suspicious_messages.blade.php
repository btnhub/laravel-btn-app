Suspicious messages sent in the last month.  Navigate to Inbox to view and delete message threads.<br>

{{$suspicious_messages->count()}} Suspicious Messages
<table class="table">
	<tbody>
		@foreach ($suspicious_messages as $message)
			<tr>
				<td>
					<b>Msg ID: </b> {{$message->id}} <br>
					<b>Thread ID: </b> {{$message->thread_id}}<br>
					<b>Assign ID: </b> 
					@if ($message->thread->tutor_request)
						{!!$message->thread->tutor_request->assignment_id ?? "<span style='color:darkred'>None</span>"!!}
					@elseif ($message->thread->tutor_proposal)
						{{$message->thread->tutor_proposal->assignment_id}}
					@endif
				</td>
				<td>
					@foreach ($message->participants as $participant)
						{{$participant->user->full_name}}<br>
					@endforeach
				</td>
				<td>
					<b>Sent By {!!$message->user->isTutor() ? "<span style='color:red'>{$message->user->full_name}</span>":$message->user->full_name!!} on {{$message->created_at->format('m/d/Y')}} </b><br>
					{{$message->body}}
				</td>
				<td>
					<button type="button" class="btn btn-danger">Delete Message # {{$message->id}} (TO DO)</button>
				</td>
			</tr>	
		@endforeach
		
	</tbody>
</table>