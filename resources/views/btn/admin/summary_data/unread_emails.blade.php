Inbox emails sent within the last 2 weeks listed have unread messages sent over 12 hours ago.<br>

{{$threads->count()}} Unread Emails.  

<table class="table">
	<thead>
		<tr>
			<th>Thread ID</th>
			<th>Location</th>
			<th>Details</th>
			<th>Creator</th>
			<th>Unviewed By<br>
				<small>(Red = Tutor)</small>
			</th>
			<th># of Messages</th>
			<th>Remind Recipient</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($threads as $thread)
			<?php 
					$unread_message = $thread->participants->where('last_read', null)->first();
					$unread_by = $unread_message->user; ?>
			<tr>
				<td>{{$thread->id}}</td>

				<td>{{$thread->tutor_request ? $thread->tutor_request->tutor_location->city : null}}
					{{$thread->tutor_proposal ? $thread->tutor_proposal->location->city : null}}
				</td>
				<td>
					{{$thread->subject}}<br>
					First Msg: {{$thread->created_at->format('m/d/Y')}}<Br>

				</td>
				<td>{{$thread->creator()->full_name}}<Br>
				</td>
				<td>	
					{!!$unread_by->isTutor() ? "<span style='color:darkred'>$unread_by->full_name</span>" : $unread_by->full_name !!}<Br>
					Last Msg Sent {{$unread_message->updated_at->diffForHumans()}}
				</td>
				<td>{{$thread->messages->count()}}</td>
				<td><button type="button" class="btn btn-primary">Remind {{$unread_by->full_name}} (TO DO)</button></td>
			</tr>
		@endforeach
		
	</tbody>
</table>