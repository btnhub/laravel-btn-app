{{$ignored_threads->count()}} Initial direct requests ignored by tutor.
<table class="table">
	<thead>
		<tr>
			<th>Thread ID</th>
			<th>Tutor</th>
			<th>Student</th>
			<th>Course</th>
			<th>Request Date</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($ignored_threads as $ignored_thread)
			<tr>
				<td>{{$ignored_thread->id}}</td>
				<td>
					<?php 
						$other_participant = $ignored_thread->participants()->where('user_id', '!=',$ignored_thread->creator()->id)->first();
						$other_user = $other_participant ? $other_participant->user : null;
					?>
					{{ $other_user ? $other_user->full_name : "User Deleted"}}
					<br>
					<small>{{$ignored_thread->tutor_request ? $ignored_thread->tutor_request->tutor_location->city : null}}</small>
				</td>
				<td>
					{{$ignored_thread->creator()->full_name}}
				</td>
				<td>
					{{$ignored_thread->tutor_request ? $ignored_thread->tutor_request->course : 'deleted tutor request'}}
				</td>
				<td>{{$ignored_thread->created_at->format('m/d/Y')}}
					<br>
					<small>{{$ignored_thread->created_at->diffForHumans()}}</small>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>