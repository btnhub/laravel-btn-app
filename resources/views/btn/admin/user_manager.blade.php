@extends('btn.layouts.home')

@section('title')
	User Manager
@stop

@section('content')

<h3>Active Users</h3>

<div class="row">
	<div>
	@include("btn.forms.search_users")	<a href="{{route('user.manager')}}">List All Users</a>
	</div>
	<div class="col-md-offset-3 col-md-4">
		<table class="table">
			<tbody>
				<tr>
					<td><b># Users:</b> </td>
					<td>{{$users->total()}} </td>
				</tr>

				<tr>
					<td><b># Banned Users:</b> </td>
					<td> <span style="color:red">TO DO</span></td>
				</tr>
				<tr>
					<td><b># Deleted Users:</b> </td>
					<td>{{$deleted_users->count()}} </td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	{{$users->links()}}
</div>

<div class="row">
	{{$users->total()}} {{str_plural('User', $users->total())}}
</div>

<table class="table">
	<thead>
		<tr>
			<th>User ID</th>
			<th>User Name</th>
			<th>Email</th>
			<th>Locations</th>
			<th>Account / Profile</th>
			<th>Roles</th>
			<th>Last Visit</th>
			<th>Registration Date</th>
			<th>Delete User</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $user)
			<tr>
				<td>{{$user->id}}</td>
				<td>
					<a href="{{ route('user.details.get', ['user_id' => $user->id])}}">{{$user->full_name}}</a>
				</td>
				<td>{{$user->email}}</td>
				<td>{{$user->locations->implode('city', ', ')}}</td>
				<td>
					<a href="{{ route('profile.account', ['ref_id' => $user->ref_id])}}" target="_blank">{{$user->user_name}}'s Account</a>
					{{--@if ($user->hasRole('tutor') || $user->hasRole('music_teacher') || $user->hasRole('mkt_tutor'))--}}
						 <br> <a href="{{ route('profile.index', ['ref_id' => $user->ref_id])}}" target="_blank">{{$user->user_name}}'s Profile</a>
					{{--@endif--}}

				</td>
				<td>{{$user->roles->implode('label', ', ')}}</td>
				<td>
					{{$user->last_login_date ? $user->last_login_date->diffForHumans() : $user->last_login_date_mobile->diffForHumans()}}
				</td>
				<td>{{$user->created_at->format('m/d/Y')}}</td>
				<td>
					@include('btn.admin.forms.user_delete')
				</td>
			</tr>
		@endforeach
		
	</tbody>
</table>
{{$users->links()}}

<h3>Trashed Users</h3>

{{--$deleted_users->links()--}}
<table class="table">
	<thead>
		<tr>
			<th>User ID</th>
			<th>User Name</th>
			<th>Email</th>
			<th>Location</th>
			<th>Role</th>
			<th>Last Visit</th>
			<th>Registration</th>
			<th>Deleted At</th>
			<th>Restore User</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($deleted_users as $user)
			<tr>
				<td>{{$user->id}}</td>
				<td>{{$user->full_name}} </td>
				<td>{{$user->email}}</td>
				<td>{{$user->locations->implode('city', ', ')}}</td>
				<td>{{$user->roles->implode('label', ', ')}}</td>
				<td>{{$user->last_login_date->diffForHumans()}}</td>
				<td>{{$user->created_at->format('m/d/Y')}}</td>
				<td>{{$user->deleted_at->format('m/d/Y')}}</td>
				<td>@include('btn.admin.forms.user_restore')</td>
			</tr>
		@endforeach
		
	</tbody>
</table>
{{--$deleted_users->links()--}}

@stop

@push('scripts')
	<script>
	    $(".delete").on("submit", function(){
	        return confirm("Are you sure you want to delete this user?");
	    });
	</script>
@endpush