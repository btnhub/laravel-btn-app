@extends('layouts.josh-front.default', ['page_title' => "Failed Jobs"])

@section('title')
	Failed Jobs
@stop

@section('content')

	<div class="container">
	<h2>Failed Jobs</h2>
		<div class="row">
			<div class="col-md-12">
				{{count($failed_jobs)}} Failed Jobs
				<table class="table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Trigger Job</th>
							<th>Details</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($failed_jobs as $failed_job)
							<tr>
								<?php $job_details = (object)(json_decode($failed_job->payload)); 
								?>
								<td>
									<b>Job ID:</b> {{$failed_job->id}}<br>
									<b>When: </b> {{$failed_job->failed_at}}<br>
									<b>Job: </b> {{$job_details->data->commandName}}<br>

								</td>
								<td>
									{!! Form::open(['method' => 'POST', 'route' => ['restart.failed.job', $failed_job->id], 'class' => 'form-horizontal']) !!}			

									    <div class="btn-group pull-right">
									        {!! Form::submit("Restart Job $failed_job->id", ['class' => 'btn btn-primary']) !!}
									    </div>
									
									{!! Form::close() !!}

									<br><br><br><br><br><br><br><br>

									{!! Form::open(['method' => 'DELETE', 'route' => ['delete.failed.job', $failed_job->id], 'class' => 'delete']) !!}
									    <div class="btn-group pull-right">
									        {!! Form::submit("Delete Job $failed_job->id", ['class' => 'btn btn-danger']) !!}
									    </div>
									
									{!! Form::close() !!}

								</td>
								<td>
									<span>
										<pre>{{var_export(unserialize($job_details->data->command))}}</pre> 
									</span>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection