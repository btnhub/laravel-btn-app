<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#summary">Tutor Summary</a></li>
  <li><a data-toggle="tab" href="#assign">Tutor Assignments</a></li>
  <li><a data-toggle="tab" href="#sessions">Tutor Sessions</a></li>
  <li><a data-toggle="tab" href="#payments">Tutor Payments</a></li>
  <li><a data-toggle="tab" href="#refunds">Tutor Refunds</a></li>
  <li><a data-toggle="tab" href="#eval">Tutor Evaluations</a></li>
</ul>

<div class="tab-content">
  <div id="summary" class="tab-pane fade in active">
	@include('btn.admin.tutor_activity.tutor_summary')   
  </div>
  <div id="assign" class="tab-pane fade">
	@include('btn.admin.tutor_activity.tutor_assignments')
  </div>
  <div id="sessions" class="tab-pane fade">
    @include('btn.admin.tutor_activity.tutor_sessions')
  </div>
  <div id="payments" class="tab-pane fade">
    @include('btn.admin.tutor_activity.tutor_payments')
  </div>
  <div id="refunds" class="tab-pane fade">
    @include('btn.admin.tutor_activity.tutor_refunds')
  </div>
  <div id="eval" class="tab-pane fade">
    @include('btn.admin.tutor_activity.tutor_evaluations')
  </div>
</div>