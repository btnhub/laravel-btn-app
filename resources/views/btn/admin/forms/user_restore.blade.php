{!! Form::open(['method' => 'PATCH', 'route' => ['user.restore', $user->id], 'class' => 'form-horizontal', 'id' => "restore_$user->id"]) !!}
    <button type="submit" class="btn btn-sm btn-success">
			<span class="glyphicon glyphicon-plus-sign"></span>
	</button>
{!! Form::close() !!}