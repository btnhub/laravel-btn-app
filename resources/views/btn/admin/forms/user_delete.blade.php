<div class="col-md-6 text-right">
    {!! Form::open(['method' => 'DELETE', 'route' => ['user.delete', $user->id], 'id' => "delete_$user->id", 'class' => 'delete']) !!}
    	{{--
    	<div class="form-group{{ $errors->has('admin_notes') ? ' has-error' : '' }}">
    	    {!! Form::label('admin_notes', 'Reason') !!}
    	    {!! Form::text('admin_notes', null, ['class' => 'form-control', 'required' => 'required']) !!}
    	    <small class="text-danger">{{ $errors->first('admin_notes') }}</small>
    	</div>
    	--}}
	    <button type="submit" class="btn btn-sm btn-danger">
			<span class="glyphicon glyphicon-trash"></span>
		</button>
    {!! Form::close() !!}
</div>