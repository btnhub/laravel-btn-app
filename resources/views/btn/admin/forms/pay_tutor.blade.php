{!! Form::open(['method' => 'POST', 'route' => ['submit.payment', $tutor->id], 'class' => 'form-horizontal', 'id' => "pay_$tutor->id"]) !!}

    <div class="form-group{{ $errors->has('amount_paid') ? ' has-error' : '' }}">
        {!! Form::label('amount_paid', 'Payment Amount') !!}
        {!! Form::text('amount_paid', $next_payment, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('amount_paid') }}</small>
    </div>

    <div class="form-group{{ $errors->has('amount_withheld') ? ' has-error' : '' }}">
        {!! Form::label('amount_withheld', 'Deducted Amount') !!}
        {!! Form::number('amount_withheld',$withheld, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('amount_withheld') }}</small>
    </div>

    <div class="form-group{{ $errors->has('withhold_reason') ? ' has-error' : '' }}">
        {!! Form::label('withhold_reason', 'Withhold Reason - Fill In for Unique Reasons (Not Unpaid Sessions)') !!}
        {!! Form::text('withhold_reason', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('withhold_reason') }}</small>
    </div>

    <div class="radio{{ $errors->has('method') ? ' has-error' : '' }}">
        <b>Payment Method: </b> &nbsp;&nbsp;&nbsp;
        <label>
            {!! Form::radio('method', 'Stripe',  1, ['id' => 'radio_id']) !!} Stripe &nbsp;&nbsp;&nbsp;&nbsp;
        </label>
        <label>{!! Form::radio('method', 'PayPal',  null, ['id' => 'radio_id']) !!} PayPal</label> 
        <small class="text-danger">{{ $errors->first('method') }}</small>
    </div>
    
    <br>
    
    <div class="form-group{{ $errors->has('eligible_sessions_ids') ? ' has-error' : '' }}">
        {!! Form::label('eligible_sessions_ids', 'Session Ids') !!}
        {!! Form::text('eligible_sessions_ids', $eligible_sessions_ids, ['class' => 'form-control', 'required' => 'required']) !!}
        <small class="text-danger">{{ $errors->first('eligible_sessions_ids') }}</small>
    </div> 
    
    <div class="btn-group pull-right">
        {!! Form::submit("Submit Payment", ['class' => 'btn btn-success']) !!}
    </div>

{!! Form::close() !!}