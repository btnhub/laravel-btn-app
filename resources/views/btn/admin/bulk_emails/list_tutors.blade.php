@extends('layouts.josh-front.default', ['page_title' => "Bulk Email Tutors"])

@section('title', "Bulk Email Tutors")

@section('content')
	<h3>Tutors Within Specified Time Period</h3>

	{{-- Form To Select Date Range --}}
	Select a date to narrow down semester (e.g. day during finals week to find tutors working at the end of the semester of interest)
	<div class="row">
		{!! Form::open(['method' => 'GET', 'route' => ['admin.list.tutors'], 'class' => 'form-horizontal']) !!}
			
			<div class="form-group{{ $errors->has('location_ids[]') ? ' has-error' : '' }} col-md-3">
			    {!! Form::label('location_ids[]', 'Locations(s)') !!}
			    {!! Form::select('location_ids[]', $locations, 2, ['id' => 'location_ids[]', 'class' => 'form-control', 'multiple']) !!}
			    <small class="text-danger">{{ $errors->first('location_ids[]') }}</small>
			</div>
			<div class="row">
				<div class="form-group{{ $errors->has('date') ? ' has-error' : '' }} col-md-2">
			        {!! Form::label('date', 'Date') !!}
			        {!! Form::date('date', request('date') ?? Carbon\Carbon::now()->subMonths(5), ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('date') }}</small>
			    </div>	
			</div>
		
		    <div class="btn-group pull-left">
		        {!! Form::submit("Search", ['class' => 'btn btn-success']) !!}
		    </div>
		
		{!! Form::close() !!}
	</div>
	
	<hr>
	
	{{$tutors->count()}} {{$cities}} Tutors on <span style="color:blue; font-weight:bold">{{$date->format('m/d/Y')}}</span>
	
	{{-- Form To Send Email Reminders --}}
	<br><br>
	<p id="selected-tutor-emails">{{implode(', ', $tutors->pluck('email')->toArray())}}</p>

	{!! Form::open(['method' => 'POST', 'route' => ['bulk.email.users'], 'class' => 'form-horizontal']) !!}
			{{-- @foreach ($tutors as $tutor)
				{!! Form::hidden('user_ids[]', $tutor->id) !!}
			@endforeach	 --}}
	    @if ($tutors->count() == 0)
	    	<div class="btn-group pull-right">
		        {!! Form::submit("Send Out Email", ['class' => 'btn btn-warning', 'disabled' => 'true']) !!}
		    </div>
		@else
			<div class="btn-group pull-right">
		        {!! Form::submit("Send Out Email", ['class' => 'btn btn-primary']) !!}
		    </div>
	    @endif
		    
		<table class="table">
			<thead>
				<tr>
					<th>
					<th>Tutor Details</th>
					<th>Assignments</th>
					<th>Evaluations</th>
					<th>Contracts</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($tutors as $tutor)
					<tr>
						<td><div class="form-group">
						    <div class="checkbox{{ $errors->has('user_ids[]') ? ' has-error' : '' }}">
						        <label for="user_ids[]">
						            {!! Form::checkbox('user_ids[]', $tutor->id, $tutor->hasRole('banned') || $tutor->banned ? 0 : 1, ['id' => $tutor->email, $tutor->hasRole('banned') || $tutor->banned ? 'disabled' : 'enabled', 'onclick' => "updateEmails()"]) !!} 
						        </label>
						    </div>
						    <small class="text-danger">{{ $errors->first('user_ids[]') }}</small>
						</div></td>
						<td>
							<b><a href="{{ route('user.details.get', ['user_id' => $tutor->id])}}" target="_blank">{{$tutor->fullName}}</a></b> (<a href="{{ route('profile.index', ['ref_id' => $tutor->ref_id])}}" target="_blank">profile</a>)<br>
							@if ($tutor->hasRole('banned') || $tutor->banned)
								<span style="color:red">BANNED</span><br>
							@endif

							{{$tutor->locations->implode('city', ', ')}}<br>
							<b>Member Since:</b> {{$tutor->created_at->format('m/d/Y')}}<br>
							<b>ID:</b> {{$tutor->id}}<br>
							<b>Rank:</b> {{$tutor->rank()}}<br>
							<b>Major:</b> {{$tutor->profile ? $tutor->profile->major : ''}}<br>
							<b>Dates:</b> {{$tutor->profile ? $tutor->profile->tutor_start_date : ''}} -> {{$tutor->profile ? $tutor->profile->tutor_end_date : ''}}<br>
							
						</td>
						<td>
							@if ($tutor->assignments->count())
								
							
								{{$tutor->assignments->count()}} Assignments<br>
								{{$tutor->assignments->filter(function ($assign) {
		                                        return $assign->tutorSessions()->count() == 0;
		                                    })
		                                    ->count()}} Assignments w/o Sessions<br>
								{{$tutor->active_assignments->count()}} Current Assignments<br>

							@else
								<b>No Assignments</b>
							@endif
						</td>
						<td>
							@if (!$tutor->assignments->count())
								<b>No Assignments</b>
							@else
								{{$tutor->tutorSessions->count()}} Tutor Sessions<br>
								{{$tutor->evaluations->where('type', 'End')->count()}} End Eval<br>
								@if ($neg_end_eval = $tutor->evaluations->where('type', 'End')->where('satisfied', 0)->count())
									<span style="color:red">{{$neg_end_eval}} Negative End Eval</span><br>
								@endif
								
								{{$tutor->evaluations->where('type', 'First')->count()}} First Eval <br> 
								@if ($neg_first_eval = $tutor->evaluations->where('type', 'First')->where('satisfied', 0)->count())
									<span style="color:red">{{$neg_first_eval}} Negative First Eval</span>
								@endif
							@endif
						</td>
						<td>
							{{$tutor->tutor_contracts->count()}} Tutor Contracts <br>
							@if ($missed_contracts = $tutor->tutor_contracts->where('tutor_decision', NULL)->count())
								<span style="color:darkred">{{$missed_contracts}} Missed Tutor Contracts </span><br>
							@endif
							
							@if ($declined_contracts = $tutor->tutor_contracts->where('tutor_decision', 0)->count())
								<span style="color:darkred">{{$declined_contracts}} Declined Tutor Contracts </span>
							@endif
						</td>
					</tr>	
				@endforeach
			</tbody>
		</table>
	{!! Form::close() !!}
@endsection

@push('scripts')
	<script type="text/javascript">
		function updateEmails(){
			var emails = document.getElementsByName('user_ids[]');
			var selected_tutor_emails = "";
			var count = 0;

			for (var i = 0; i < emails.length; i++) {
				if (emails[i].checked == true) {
					selected_tutor_emails += emails[i].id;
					if (i < emails.length - 1) {
						selected_tutor_emails += ", ";
					}	
					count++;
				}
			}
			document.getElementById('selected-tutor-emails').innerHTML = count + " Tutors Selected <br>" + selected_tutor_emails;
		}
	</script>
@endpush