@extends('btn.layouts.home')

@section('title')
	Tutor Next Payment
@stop

@section('content')
<h3>Submit Tutor Payment</h3>
<p>{{$tutors->count()}} Tutors</p>
<table class="table">
	<thead>
		<tr>
			<th>Tutor ID</th>
			<th>Tutor Name</th>
			<th>Pay Tutor</th>
			<th>Total Earned</th>
			<th>Total Deducted</th>
			<th>Difference</th>
			<th>Next Payment</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutors as $tutor)		
			<?php $amount_earned = $tutor->nextPaymentEarned()?>

			@if($amount_earned > 0)
				<?php 
						$eligible_sessions = $tutor->paymentEligibleSessions();
						$next_payment = $eligible_sessions->sum('tutor_pay');
						$eligible_sessions_ids = implode(',', $eligible_sessions->pluck('id')->toArray());
						$withheld = $tutor->delinquentSessions()->sum('tutor_pay');		
				?>
			
				<tr>
					<td>{{ $tutor->id }}</td>
					<td> 
						<a href="{{ route('user.details.get', ['user_id' => $tutor->id])}}" target="_blank">{{ $tutor->first_name }} {{ $tutor->last_name }} </a> 
						<br>
						{{ $tutor->locations->implode('city', ', ') }}

						@if ($next_payment > 0)
							<div class="collapse" id="pay_tutor_{{$tutor->id}}">
								@include('btn.admin.forms.pay_tutor')
							</div>
						@endif
					</td>
					<td>
						@if ($next_payment > 0)

						<a href="#" data-toggle="collapse" data-target="#pay_tutor_{{$tutor->id}}" type="button" class="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
							</a>
						@endif
					</td>
					<td>{{ $amount_earned }}</td>
					<td>{{ $withheld }}</td>
					<td>{{ number_format($amount_earned - $withheld, 2) }}</td>
					<td> {{$next_payment}}</td>
				</tr>
			@endif
		@endforeach
	</tbody>
</table>
@stop