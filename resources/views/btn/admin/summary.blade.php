<?php $failed_jobs = DB::table('failed_jobs')->count();?>

@if ($failed_jobs)
	<h2>Failed Jobs</h2>
	{{$failed_jobs}} Failed Jobs.  <a href="{{route('failed.jobs')}}">View Details</a>	
@endif

<hr>

{{-- @if ($threads->count())
	<h2>Unread Emails</h2>
	@include('btn.admin.summary_data.unread_emails')
@endif

<hr>

@if ($suspicious_messages->count())
	<h2>Suspicious Emails</h2>
	@include('btn.admin.summary_data.suspicious_messages')	
@endif

<hr>

@if ($ignored_threads->count())
	<h2>Ignored Emails</h2>
	@include('btn.admin.summary_data.ignored_messages')	
@endif
 --}}