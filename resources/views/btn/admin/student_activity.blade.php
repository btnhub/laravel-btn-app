<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#summary">Student Summary</a></li>
  <li><a data-toggle="tab" href="#student-assign">Assignments</a></li>
  <li><a data-toggle="tab" href="#student-sessions">Sessions</a></li>
  <li><a data-toggle="tab" href="#student-prepayments">Prepayments</a></li>
  <li><a data-toggle="tab" href="#student-refunds">Refunds</a></li>
  <li><a data-toggle="tab" href="#student-eval">Evaluations</a></li>
</ul>

<div class="tab-content">
  <div id="summary" class="tab-pane fade in active">
	@include('btn.admin.student_activity.student_summary')	
  </div>
  <div id="student-assign" class="tab-pane fade">
	@include('btn.admin.student_activity.student_assignments')
  </div>
  <div id="student-sessions" class="tab-pane fade">
    @include('btn.admin.student_activity.student_sessions')
  </div>
  <div id="student-prepayments" class="tab-pane fade">
    @include('btn.admin.student_activity.student_prepayments')
  </div>
  <div id="student-refunds" class="tab-pane fade">
    @include('btn.admin.student_activity.student_refunds')
  </div>
  <div id="student-eval" class="tab-pane fade">
    @include('btn.admin.student_activity.student_evaluations')
  </div>
</div>