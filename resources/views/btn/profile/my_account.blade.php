@extends('layouts.josh-front.default', ['page_title' => "My Account"])

@section('title')
	My Account
@stop

@section('content')
  <div class="row">
        <h3>Welcome To Your BuffTutor Account!</h3>
        @if ($tutor && $student)
          Looks like you're signed up as a student and a tutor.  The top half of this document has the tools and tables you'll need as a tutor and the bottom half are the tools you'll need as a student.
          <br>
          If you are not a student, <a href="{{route('remove.role', ['user_ref_id' => $student->ref_id, 'role' => 'student'])}}">click here</a>.
          <hr>

        @endif
  </div>
  @if (Auth::user()->isSuperUser() && Auth::user()->ref_id == $user_ref_id)
    @include('btn.admin.summary')
  @endif

  @if ($tutor)    
    @if (Auth::user()->isSuperUser() && Auth::user()->id != $tutor->id)
      <div class="row">
          <h3>{{$tutor->full_name}}'s Account</h3>      
      </div>
    @endif

    @if (!$tutor->isActiveTutor())
      <p><b style="color:darkred">Your profile does not show up on our list.</b>  <a href="{{route('profile.edit', $tutor->ref_id)}}">Edit your profile</a> to provide the missing information.</p>
    @endif
    
    @unless($tutor->profile->tutor_contract > $tutor_contract_date)
      You must sign our new <a href="{{route('get.tutor.contract')}}">tutor contract</a> and pass a background check run before you can access your account and receive students.
    @else
      <div class="row">
        <center><b>Carefully review the <a href="{{route('tutor.orientation')}}">Tutor Orientation</a> page for a summary of your account and the tools available.</b></center>
        <br>
        @if (!$is_online_tutor)
          <center><b><span style="color:darkred">NEW!</span> Sign Up For Online Tutoring.  <a href="{{route('online.tutoring.details')}}">Learn More</a>.</b></center>
          <br>
        @endif
        <center></center>
        <center>Simply click the links to submit forms or to update values on this page.  <b>Note:  Your student will be e-mailed the moment you submit any form listed on this page!</b></center>

        @if ($unreviewed_contracts)
          <p><b style="font-size: 1.25em">You have <a href="{{route('my.tutor.contracts', $tutor->ref_id)}}">{{$unreviewed_contracts}} new tutor {{str_plural('request', $unreviewed_contracts)}}</a>!</b></p>
        @endif
      </div>
      
      <br>

      {{-- Student Contact Info, Payments, Charges, Refunds --}}
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Student List</a></li>
        <li><a data-toggle="tab" href="#menu1">Sessions</a></li>
        <li><a data-toggle="tab" href="#menu2">Evaluations</a></li>
        <li><a data-toggle="tab" href="#menu3">Refunds</a></li>
      </ul>

      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
         
          @include('btn.tables.assignments_students')	
        </div>
        <div id="menu1" class="tab-pane fade">
     		 @include('btn.tables.sessions_tutor')	
        </div>
        <div id="menu2" class="tab-pane fade">
          @include('btn.tables.evaluations_tutor')  
        </div>
        <div id="menu3" class="tab-pane fade">
          @include('btn.tables.refunds_tutor')  
        </div>
      </div>
    @endunless
    <hr>
  @endif

  @if ($student ?? '') 
      
    @if (Auth::user()->isSuperUser())
      <div class="row">
          <h3>{{$student->full_name}}'s Account</h3>      
      </div>
    @endif

    @unless(in_array($student->id, $terms_array))
      <p>Please review and agree to our new student terms of use to access your account information.</p>
      <div class="col-md-12 col-sm-12" style="overflow-y: scroll; height:300px;">
       @include('btn.pages.partials.terms_of_use')
      </div>
      
      <div class="row col-md-12 col-sm-12">
      <br>
        <p><label><input type="checkbox"> I have reviewed and agree to the terms of use.  I understand that <span style="color:red">I <b>cannot</b> pay my tutor directly for sessions</span> and that doing so may result in fines. </label></p>
      
      <a href="{{route('sign.student.contract')}}"><button type="button" class="btn btn-primary">Sign Contract</button></a>
      </div>
    @else
        {{--Show Student's Account Balance--}}
      <div class="row">
        <div class="col-md-7 col-sm-7">
          <p>On this page you will find a summary of your assigned tutors, session charges, payments and refunds.  </p>
          
          @if ($pending_proposals)
            <p><b style="font-size: 1.25em">You have <a href="{{route('my.tutor.requests', $student->ref_id)}}">{{$pending_proposals}} new tutor {{str_plural('proposal', $pending_proposals)}}</a>!</b></p>
          @endif

          <p>Please review our <a href="{{ route('cancellation.policy') }}">cancellation</a> & <a href="{{ route('refund.policy') }}">refund</a> policies.</p>
          
          {{--Links For GOOGLE REVIEW--}}
          @if ($student->evaluations->where('satisfied', 1)->count())
            <br>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star" aria-hidden="true"></i>
            <i class="fa fa-star-half-o" aria-hidden="true"></i>
            @unless ($student->locations->where('company_name', 'RamTutor')->count())
              {{--BuffTutor URL--}}
              <span> 
                <a href="https://goo.gl/XfhaZq" target="_blank">Review BuffTutor</a> On Google! It only takes a second, but helps us immensely.
              </span>
            @else
              {{--RamTutor URL--}}
              <span>
                <a href="https://goo.gl/m2x5ms" target="_blank"> Review RamTutor</a> On Google! It only takes a second, but helps us immensely.
              </span>
            @endunless
          @endif
          
        </div>
      </div>
      <br>

      {{-- Tutor Contact Info, Payments, Charges, Refunds --}}
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home_student">Tutor List</a></li>
        <li><a data-toggle="tab" href="#menu1_student">Sessions</a></li>
        <li><a data-toggle="tab" href="#menu2_student">Refunds</a></li>
        @if ($student->assignments()->previous()->count() > 0)
          <li><a data-toggle="tab" href="#menu4_student">Previous Tutors</a></li>
        @endif
      </ul>

      <div class="tab-content">
        <div id="home_student" class="tab-pane fade in active">
        	@include('btn.tables.assignments_tutors')
        </div>
        <div id="menu1_student" class="tab-pane fade">
     		 @include('btn.tables.sessions_student')	
        </div>
        <div id="menu2_student" class="tab-pane fade">
         @include('btn.tables.refunds_student') 
        </div>
        <div id="menu4_student" class="tab-pane fade">
          @include('btn.tables.assignments_previous_tutors')
        </div>
      </div>
    @endunless
  @endif

  @if (!$tutor && !$student && !Auth::user()->isSuperUser() && Auth::user()->hasRole('tutor-applicant'))
    <center><b>Tutor Applicants: Please proceed to the <a href="{{route('tutor.checklist')}}">Tutor Checklist</a></b></center>
  @endif
@stop