@extends('layouts.josh-front.default', ['page_title' => $user->user_name."'s Profile"])

@section('title')
	{{$user->user_name}}'s Profile
@stop
@section('content')
	<div class="row">
		<div class="col-md-7">
		{{-- User Information --}}
			<div class="media panel-body">
				@if ($user->profile->avatar)
					@include('btn.partials.avatar', ['width' => '170px', 'radius' => '0%'])	
				@endif
				
				<div class="media-body">
					@if (auth()->user() && auth()->user()->isStaff())
						<h3 class="media-heading"><a href="{{ route('user.details.get', ['user_id' => $user->id])}}">{{$user->full_name}}</a></h3>	
					@else
						<h3 class="media-heading">{{$user->user_name}}</h3>	
					@endif
					
					@if ($user->id == 63)
						<b>BuffTutor Director</b><br>
					@endif
					@if ($user->profile->tutor_status == 1)
						<span style="color: green">{{$user->first_name}} Is Accepting Students</span>
					@else 
						<span style="color: red">{{$user->first_name}} Is No Longer Accepting Students</span>
					@endif
					<br>
					@include('btn.partials.hours_worked', ['tutor' => App\Tutor::find($user->id)])
					{{--If A Music Instructor--}}
					@if ($user->profile->instruments)
						Instrument: {{$user->profile->instruments}}<br>
						Genre/Style: {{$user->profile->music_styles}}<br>
					@endif
					
					
					{{$user->locations->implode('city', ', ')}} Tutor
					<br>
					
					Member Since: {{ $user->created_at->format('F Y') }}<br>
					
					Last Login: {{$user->last_login_date->diffForHumans()}}
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<br>
			<table class="table">
				<tbody>
					@unless (isset($evaluations))
						<tr><td>No Evaluations Submitted For This Tutor</td></tr>
					@else
					<tr>
						<td># of Positive <span data-toggle="tooltip" title="Evaluation submitted at end of semester">End Of Semester Reviews</span></td>
						<td>{{ $evaluations->where('type', 'End')->where('satisfied', 1)->count() }}</td>
					</tr>
					<tr>
						<td># of Negative End Of Semester Reviews</td>
						<td>{{ $evaluations->where('type', 'End')->where('satisfied', 0)->count() }}</td>
					</tr>
					<tr>
						<td># of Positive 1st Session Reviews</td>
						<td>{{ $evaluations->where('type', 'First')->where('satisfied', 1)->count() }}</td>
					</tr>
					<tr>
						<td># of Negative 1st Session Reviews</td>
						<td>{{ $evaluations->where('type', 'First')->where('satisfied', 0)->count() }}</td>
					</tr>
					@endunless
				</tbody>
			</table>
		</div>
	</div>

	<div class="row">
		<center><b>If you'd like to work with {{$user->first_name}}, <a href="{{route('request.get')}}">request a tutor</a> and list {{$user->user_name}} as a preferred tutor.</b></center>
		<br>
	</div>
	<div class="row">
		<div class="col-md-10">
			@include('btn.profile.tutor_details')	
		</div>
	</div>

	<div class="row">
		<center><b>If you'd like to work with {{$user->first_name}}, <a href="{{route('request.get')}}">request a tutor</a> and list {{$user->user_name}} as a preferred tutor.</b></center>
		<br>
	</div>

@stop