<div class="container">
	<div class="well">
		<p>
			<h4>Experience</h4>	
			{!! nl2br(e($user->profile->tutor_experience)) !!}
		</p>
	</div>

	@if ($user->profile->calendar_id)
		<p><button class="btn btn-primary" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> Show/Hide {{ $user->first_name }}'s Calendar </button></p>
		<div class="collapse" id="collapseExample">
			<div class="card card-block" align="center">
				<span style='color:red'><b>*** You cannot add events to the calendar.  Contact {{ $user->first_name }} about scheduling sessions ***
				@include('btn.profile.calendar', ['timezone' => urlencode($user->locations->first()->timezone), 'calendar_id' => urlencode($user->profile->calendar_id)])</b></span>
			</div>
		</div>	
	@endif
	
	<ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#sectionA">Tutor Info</a></li>
	    <li><a data-toggle="tab" href="#sectionB">Reviews/References ({{$user->id != 63 ? $evaluations->filter(function ($eval){ return $eval->website_reference!='';})->count() : ($evaluations->filter(function ($eval){ return $eval->website_reference!='';})->count()+ DB::table('evaluations_mogi')->orderBy('id', 'DESC')->count())}})</a></li>
	</ul>
	<div class="tab-content" style="min-height:200px";>
	    <div id="sectionA" class="tab-pane fade in active">
	       <table class="table">
			<tbody>
				<tr>
					<td colspan="2"><b>{{$user->first_name}} can meet in the following location(s): </b>{{$user->profile->session_locations}}</td>
				</tr>
				<tr>
					<td>Major:</td><td>{{ $user->profile->major }}</td>
				</tr>
				<tr>
					<td>Education:</td><td>{{ $user->profile->education() }}</td>
				</tr>
				{{-- @if ($user->profile->rate_visibility == 1) --}}
				@if (Auth::user() && Auth::user()->isStaff())
					<tr>
						<td>Min Rate:</td><td>${{ $user->profile->rate_min }} / hr</td>
					</tr>
					<tr>
						<td>Max Rate:</td><td>${{ $user->profile->rate_max }} / hr</td>
					</tr>
					@if ($user->profile->rate_details)
						<tr>
							<td>Rate Details:</td><td>{!! nl2br(e($user->profile->rate_details)) !!}</td>
						</tr>
					@endif
					
				{{-- @else
					<tr>
						<td>Rate:</td><td>Please contact {{ $user->first_name }} about the rate</td>
					</tr> --}}
				@endif
				<tr>
					<td>Languages Spoken:</td><td>{{ $user->profile->languages }}</td>
				</tr>
				<tr>
					<td>Availability:</td><td>{{ str_replace("|*|",", " , $user->profile->tutor_availability) }}</td>
				</tr>
				<tr>
					<td>Courses:</td><td>{!! nl2br(e($user->profile->courses)) !!}</td>
				</tr>
			</tbody>
		</table>	

	    </div>
	    <div id="sectionB" class="tab-pane fade">
	        <h3>References From Previous Students</h3>
			References are submitted by students who've worked with the tutor on a regular basis, not just once.  
	        
        	@include('btn.tables.reviews_tutor')
	        
	        @if ($user->id == 63)
	        	@include('btn.tables.reviews_tutor_mogi', ['mogi_evaluations' => DB::table('evaluations_mogi')->orderBy('id', 'DESC')->get()])
	        @endif
	    </div>
	</div>
	
</div>
