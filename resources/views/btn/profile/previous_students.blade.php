@extends('layouts.josh-front.default', ['page_title' => "Add Previous Student"])

@section('title')
	Add Previous Student
@stop

@section('content')
	
	<h3>Add Previous Student</h3>
	
	@include('btn.forms.search_previous_students')
	
	@if (isset($students))
	<p>Simply click on the student and submit the form.</p>

	<table class="table">
		<thead>
			<tr>
				<th>Student Name</th>
				<th>Old Course</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($students as $student)	
			<tr>
				<td>{{$student->first_name}} {{$student->last_name}} &nbsp;&nbsp;&nbsp;&nbsp;
					<a href="#"data-toggle="collapse" data-target="#add_student_{{$student->ref_id}}">
								Add {{$student->first_name}}
					</a>
					<div class="collapse" id="add_student_{{$student->ref_id}}">	
						<div class="col-md-4">		
							{!! Form::open(['method' => 'POST', 'route' => ['add.previous.student', 'ref_id' => $student->ref_id], 'class' => 'form-horizontal', 'id' => "add_student_{{$student->_ref_id}}"]) !!}
						        <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
						            {!! Form::label('course', 'New Course') !!}
						            {!! Form::text('course', null, ['class' => 'form-control', 'required' => 'required']) !!}
						            <small class="text-danger">{{ $errors->first('course') }}</small>
						        </div>
						        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
						            {!! Form::label('start_date', 'Start Date') !!}
						            &nbsp;&nbsp;An estimate is sufficient
						            {!! Form::date('start_date', Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
						            <small class="text-danger">{{ $errors->first('start_date') }}</small>
						        </div>
						        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
						            {!! Form::label('end_date', 'End Date') !!}
						            &nbsp;&nbsp;An estimate is sufficient
						            {!! Form::date('end_date', null, ['class' => 'form-control', 'min' => Carbon\Carbon::now()->addWeek()->format('Y-m-d'), 'required' => 'required']) !!}
						            <small class="text-danger">{{ $errors->first('end_date') }}</small>
						        </div>

						        <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
						            {!! Form::label('location', 'Location') !!}
						            {!! Form::select('location',$locations, null, ['id' => 'location', 'class' => 'form-control', 'required' => 'required']) !!}
						            <small class="text-danger">{{ $errors->first('location') }}</small>
						        </div>

						        {!! Form::submit("Add $student->first_name", ['class' => 'btn btn-primary']) !!}
							{!! Form::close() !!}
						</div>
					</div>
				</td>
				<td>{{$assignments[$student->id]}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	@endif
@stop