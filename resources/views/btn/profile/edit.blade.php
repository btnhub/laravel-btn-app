@extends('layouts.josh-front.default', ['page_title' => "Edit My Account"])

@section('title')
	Edit BuffTutor Account
@stop

@section('content')
	<div class="container">
		
		@if ($user->isTutor() && !$user->isActiveTutor())
	    	<div class="row">
	    		<div class="col-md-6" style="border:2px solid gray;border-radius:3px;margin-bottom: 15px;padding:10px">
	    			<b style="color:darkred">Your profile is not complete and does not show up on our list of tutors.</b>  
					<ol>
						@if (!$user->profile->show_profile_tutor)
							<li>Your profile is listed as "Hidden", please e-mail us to change this status.</li>
						@endif
						@if ($user->profile->tutor_contract == NULL)
							<li>You need to <a href="{{route('get.tutor.contract')}}">sign our contract</a>.</li>
						@elseif($user->profile->tutor_contract < '2018-06-01')
							<li>You need to <a href="{{route('get.tutor.contract')}}">sign the updated contract</a>.</li>
						@endif
						@if ($user->profile->background_check == NULL || $user->profile->background_check == "")
							<li>You must pass the background check.  Refer to the Tutor Checklist.</li>
						@endif
						@if ($user->profile->tutor_experience == '')
							<li>You must fill in the Tutor Experience section.</li>
						@endif
						@if ($user->profile->tutor_education == '')
							<li>You must select an option in the Current Education field.</li>
						@endif
						@if ($user->profile->courses == '')
							<li>You must list the courses you can tutor.</li>
						@endif
						@if ($user->profile->tutor_start_date > Carbon\Carbon::now() || $user->profile->tutor_end_date < Carbon\Carbon::now())
							<li>Your profile will be visible from <b>{{$user->profile->tutor_start_date}} to {{$user->profile->tutor_end_date}}</b>.  Change your Start/End dates to indicate when you will be available to tutor.  <b>You MUST complete the semester/quarter you begin working.</b></li>
						@endif
					</ul>	
	    		</div>
				
			</div>
		@endif

		<ul class="nav nav-tabs">
		    <li class="active"><a data-toggle="tab" href="#sectionA">Contact Information</a></li>
		     @if ($user->isTutor())
		    	<li><a data-toggle="tab" href="#sectionB">Tutor Details</a></li>
		    @endif
		</ul>
		<div class="tab-content">
		    <div id="sectionA" class="tab-pane fade in active">
		    	@include ('btn.forms.profile.edit_contact_info')		
		    </div>
		    @if ($user->isTutor() || $user->isSuperUser())
		    	<div id="sectionB" class="tab-pane fade">
		        	@include ('btn.forms.profile.edit_profile_details', ['profile' => $user->profile])	
		    	</div>	
		    @endif
		    
		</div>
			
	</div>



@stop
