<!-- Left Side Of Navbar -->
<ul class="nav navbar-nav">
    
    {{-- <li><a href="{{ route('list.tutors') }}">Tutor Profiles</a></li> --}}
    
    <li><a href="{{ route('request.get') }}">Request A Tutor</a></li>

    @unless (Auth::check() && Auth::user()->isTutor())
        <li><a href="{{ route('become.tutor') }}">Become A Tutor</a></li>    
    @endunless

    @if (Auth::check() && (Auth::user()->isTutor() || Auth::user()->hasRole(['tutor-applicant', 'super-user', 'admin'])) && Auth::user()->banned == 0 && !Auth::user()->hasRole('banned'))
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tutors<span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('tutor.checklist') }}">Tutor Checklist</a></li>
                
                @if (Auth::check() && (Auth::user()->isTutor() || Auth::user()->hasRole(['super-user', 'admin'])))
                    <li><a href="{{ route('my.tutor.contracts', Auth::user()->ref_id) }}">My Tutor Requests</a></li>
                    {{-- <li><a href="{{ route('whats.new') }}">What's New</a></li> --}}
                    <li><a href="{{ route('tutor.orientation') }}">Tutor Orientation</a></li>               
                    {{-- <li><a href="{{ route('online.tutoring.details') }}">Online Tutoring</a></li>   --}}             
                    <li><a href="{{ route('course.catalogs') }}">Calendars & Catalogs</a></li>               
                    <li><a href="{{ route('intro.email') }}">Introductory Email</a></li>               
                    <li><a href="{{ route('get.search.students') }}">Add Previous Student</a></li>  
                    {{-- <li><a href="{{ route('bufftutor.payments') }}">BuffTutor Payments</a></li> --}}              
                @endif

            </ul>
        </li>
    @endif    
    
</ul>

<!-- Right Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
    <!-- Authentication Links -->
    @if (Auth::guest())
        <li><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
    @else
        <li><a href="{{ route('profile.account', ['user_ref_id' => Auth::user()->ref_id]) }}">My Account</a></li>
        @if (Auth::user()->hasRole(['super-user', 'admin']))
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Admin<span class="caret"></span>
                </a>

                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ route('user.manager') }}">User Manager</a></li>
                    {{-- <li><a href="{{ route('fix.assign.contract') }}">TEMP Assoc Assign & Contract</a></li> --}}
                    <li><a href="{{ route('list.tutor.requests') }}">Tutor Requests</a></li>
                    <li><a href="{{ route('assignments.index') }}">All Assignments</a></li>
                    <li><a href="{{ route('applicants') }}">Tutor Applications</a></li>
                    <li><a href="{{ route('show.evaluations') }}">Evaluations</a></li>
                    <li><a href="{{ route('prof.exam.list') }}">Proficiency Exams</a></li>
                    <li><a href="{{ route('analytics') }}">Analytics</a></li>
                    
                    <li><a href="{{ route('marketing') }}">Marketing</a></li>
                    <li><a href="{{ route('daniels.fund') }}">Daniels Fund</a></li>
                    <li><a href="{{ route('list.advisors') }}">Academic Advisors</a></li>
                    <li><a href="{{ route('failed.jobs') }}">Failed Jobs</a></li>
                    <li><a href="{{ route('tutor.payment') }}">Next Tutor Pay</a></li>
                    <li><a href="{{ route('contractor.taxes') }}">Contractor Taxes</a></li>
                    <li><a href="{{ route('first.session.students') }}">Issues</a></li>
                </ul>
            </li>     
        @endif
        

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->first_name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                @if (Auth::user()->banned == 0 && !Auth::user()->hasRole('banned'))
                    <li><a href="{{ route('profile.account', ['user_ref_id' => Auth::user()->ref_id]) }}">My Account</a></li>
                    <li><a href="{{ route('profile.edit', ['user_ref_id'=>Auth::user()->ref_id]) }}">Edit Profile</a></li>
                    @if (Auth::user()->isTutor() || Auth::user()->hasRole(['super-user', 'admin']))
                        <li><a href="{{ route('profile.index', ['ref_id'=>Auth::user()->ref_id]) }}">My Profile</a></li>
                        <li><a href="{{ route('my.tutor.contracts', Auth::user()->ref_id) }}">My Tutor Requests</a></li>
                    @endif

                    @if (Auth::user()->isStudent() || Auth::user()->hasRole(['super-user', 'admin']))
                        <li><a href="{{ route('payment.settings') }}">Payment Settings</a></li>
                        <li><a href="{{route('my.tutor.requests', Auth::user()->ref_id)}}">My Tutor Proposals</a></li>
                    @endif
                @endif
                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
            </ul>
        </li>

    @endif
</ul>