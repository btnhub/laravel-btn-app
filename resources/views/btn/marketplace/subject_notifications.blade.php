@extends('layouts.josh-front.default', ['page_title' => "Subject Notifications"])

@section('title')
	Subject Notifications
@stop

@section('content')
	<h3>Existing Notifications</h3>
	<div class="container">
		<div class="row">
	
			<div class="col-xs-12">
				@if ($tutor->notifications->count() == 0)		
					You have not signed up for e-mail notifications.  Add a notification below.
				@else
					You have {{$tutor->notifications->count()}} {{str_plural('notification', $tutor->notifications->count())}}.  Edit a notification by clicking the subject name.  To add more notifications, scroll down to bottom of the page.

					{{-- Gives error: Controller method not found
					<p>
						<span style="color:red">FIX!!!</span>  <a href="{{route('update.all.notifications', ['notify' => 1])}}">Turn On All </a> / 
						<a href="{{route('update.all.notifications', ['notify' => 0])}}">Turn Off All </a>
					</p>
					--}}			
					<table class="table">
						<thead>
							<tr>
								<th>Subject</th>
								<th>Location</th>
								<th>Min Rate</th>
								<th>Status</th>
								<th>Delete Notification</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($tutor->notifications as $notification)
								<tr>
									<td>
										<a href="#"data-toggle="collapse" data-target="#edit_notification_{{$notification->id}}">
											{{$notification->subject->department}}
										</a>
										<div class="collapse" id="edit_notification_{{$notification->id}}">
											@include('btn.marketplace.forms.subject_notification_edit')
										</div>
									</td>
									<td>{{$notification->location->city}}</td>
									<td>${{$notification->min_rate}}/hr</td>
									<td>
										@if ($notification->notify)
											On
										@else
											<span style="color:red">Off</span>
										@endif
									</td>
									<td>
										<a href="{{route('delete.subject.notification',['notification_id' => $notification->id])}}"><button type="button" class="btn btn-danger">
	  										<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
										</button></a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
					<p class="pull-left">Click here to <a href="{{route('delete.all.notifications')}}">Delete All Notifications</a></p>
				@endif
			</div>
	
		</div>
<hr>
		<h3>Add A Notification</h3>
		<div class="row">
			<div class="col-sm-12">
			<p>Add subject notifications to receive an e-mail the moment a tutor request for a course in that subject is submitted.  Select the subjects, location(s) and min rate for the notification. </p>
			<p>You will not receive an e-mail notification for a tutor request where the student's maximum affordable rate is less than your notification minimum rate, <b>however</b> you will still be able to view the tutor request and submit a proposal on the MarketPlace.</p> 
				<div class="col-md-11 col-sm-11 col-md-offset-1 col-sm-offset-1">
					{!! Form::open(['method' => 'POST', 'route' => ['store.subject.notification'], 'class' => 'form-horizontal']) !!}
			            {{--Select Subject--}}
			            <div class="row">
				            <div class="col-md-4 col-sm-5">
				                <small class="text-danger">{{ $errors->first('subjects') }}</small>
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'Subjects') !!}
				                    {!! Form::select('subjects[]',$subjects, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
				            <div class="col-md-4 col-sm-5 col-md-offset-1 col-sm-offset-1">
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'Math') !!}
				                    {!! Form::select('subjects[]',$subjects_math, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
			            </div>
			            <div class="row">
				            <div class="col-md-4 col-sm-5">
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'Engineering') !!}
				                    {!! Form::select('subjects[]',$subjects_engineering, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
				            <div class="col-md-4 col-sm-5 col-md-offset-1 col-sm-offset-1">
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'Humanities') !!}
				                    {!! Form::select('subjects[]',$subjects_humanities, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
			            </div>
			            <div class="row">
				            <div class="col-md-4 col-sm-5">
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'Languages') !!}
				                    {!! Form::select('subjects[]',$subjects_languages, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
				            <div class="col-md-4 col-sm-5 col-md-offset-1 col-sm-offset-1">
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'Music') !!}
				                    {!! Form::select('subjects[]',$subjects_music, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
			            </div>
			            <div class="row">
				            <div class="col-md-4 col-sm-5">
				                <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
				                    {!! Form::label('subjects[]', 'General') !!}
				                    {!! Form::select('subjects[]',$subjects_general, null, ['class' => 'form-control', 'multiple']) !!}
				                </div>
				            </div>
				            <div class="col-md-4 col-sm-5 col-md-offset-1 col-sm-offset-1">
				                <div class="form-group">
							        <div class="checkbox{{ $errors->has('locations[]') ? ' has-error' : '' }}">
							            @foreach ($locations as $location)
								            <label>
								                {!! Form::checkbox('locations[]',$location->location_id, null, ['checked']) !!} {{$location->city}}
								            </label>
							            @endforeach
							        </div>
							        <small class="text-danger">{{ $errors->first('locations[]') }}</small>
							    </div>

							    <div class="col-sm-6 form-group{{ $errors->has('min_rate') ? ' has-error' : '' }}">
							        {!! Form::label('min_rate', 'Min Rate ($/hr)') !!}
							        {!! Form::number('min_rate', null, ['class' => 'form-control col-sm-4', 'required', 'min' => 10]) !!}
							        <small class="text-danger">{{ $errors->first('min_rate') }}</small>
							    </div>

							    <div class="form-inline btn-group col-sm-offset-1">
							        {!! Form::submit("Add Subjects", ['class' => 'btn btn-success']) !!}
							    </div>
				            </div>
			            </div>
			        {!! Form::close() !!}
		        </div>
			</div>
		</div>
	</div>
@stop

