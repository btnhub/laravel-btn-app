@extends('layouts.josh-front.default', ['page_title' => "Tutor Requests"])

@section('title')
	Tutor Requests
@stop

@section('content')
	@if (Auth::user()->banned == 0 && !Auth::user()->hasRole('banned'))
		<p>Listed below are recent requests from students who have not specified a tutor that they wish to work with.  Click on the student name and carefully review the request details.  Then simply submit a proposal to the student in the included form.</p>
		
		@if (!Auth::user()->hasRole(['admin', 'super-user']))
			<table class="table table-bordered" id="requests-table">
				<thead>
					<tr>
						<th>Ref ID</th>
						<th>Location</th>
						<th>Urgency</th>
						<th>Student Name</th>
						<th>Course</th>
						<th>Level</th>
						<th>Start Date</th>
						<th>End Date</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		@else
			<table class="table table-bordered" id="requests-table-admin">
				<thead>
					<tr>
						<th>Ref ID</th>
						<th>Location</th>
						<th>Urgency</th>
						<th>Student Name</th>
						<th>Course</th>
						<th>Level</th>
						<th>Max Rate</th>
						<th>Start Date</th>
						<th>End Date</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		@endif
	@endif
@stop

@push('scripts')

<script>
	$(function() {
	    $('#requests-table').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{!! route('marketplace.data') !!}',
	        columns: [
	            { data: 'ref_id', name: 'ref_id' },
	            { data: 'location', name: 'location' },
	            { data: 'urgency', name: 'urgency' },
	            { data: 'student_id', name: 'student_id' },
	            { data: 'course', name: 'course' },
	            { data: 'level', name: 'level' },
	            { data: 'start_date', name: 'start_date' },
	            { data: 'end_date', name: 'end_date' }
	        ]
	    });
	});

	$(function() {
	    $('#requests-table-admin').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: '{!! route('marketplace.data') !!}',
	        columns: [
	            { data: 'ref_id', name: 'ref_id' },
	            { data: 'location', name: 'location' },
	            { data: 'urgency', name: 'urgency' },
	            { data: 'student_id', name: 'student_id' },
	            { data: 'course', name: 'course' },
	            { data: 'level', name: 'level' },
	            { data: 'max_rate', name: 'max_rate' },
	            { data: 'start_date', name: 'start_date' },
	            { data: 'end_date', name: 'end_date' }
	        ]
	    });
	});
</script>
@endpush