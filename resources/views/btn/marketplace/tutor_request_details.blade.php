@extends('layouts.josh-front.default', ['page_title' => "Tutor Request Details"])

@section('title')
	Tutor Request Details
@stop

@section('content')
	<h2>{{$tutor_request->student->first_name." ".substr($tutor_request->student->last_name, 0, 1)}} - {{$tutor_request->course}}</h2>
	<table class="table table-striped">
		<tbody>
			<tr>
				<td><b>Ref ID</b></td>
				<td>{{$tutor_request->ref_id}}</td>
			</tr>
			<tr>
				<td><b>Location</b></td>
				<td>{{$tutor_request->location}}</td>
			</tr>
			<tr>
				<td><b>Start / End Date</b></td>
				<td>
					<b>Start: {{Carbon\Carbon::parse($tutor_request->start_date)->format('m/d/Y')}} <br>
				 	
				 	End: {{Carbon\Carbon::parse($tutor_request->end_date)->format('m/d/Y')}}</b></td>
			</tr>
			
			@if ($tutor_request->urgency == "High")
				<tr>
					<td><b>Urgency</b></td>
					<td>
						<span style="color:red">{{$tutor_request->urgency}} - Student must meet in less than 2 days</span>		
					</td>
				</tr>
			@endif
				<td><b>Max Rate</b></td>
				<td><span style="color:red">${{$tutor_request->max_rate}}/hr</span></td>
			</tr>
			<tr>
				<td><b>Course Level</b></td>
				<td>{{$tutor_request->level}}</td>
			</tr>
			<tr>
				<td><b>Course Subjects</b></td>
				<td>{{$tutor_request->subjects}}</td>
			</tr>
			<tr>
				<td><b>Session Time</b></td>
				<td>{{$tutor_request->session_time}}</td>
			</tr>
			<tr>
				<td><b>Session Frequency</b></td>
				<td>{{$tutor_request->frequency}}</td>
			</tr>
			@if($tutor_request->scholarship_program)
				<tr>
					<td><b>Scholarship</b></td>
					<td>{{$tutor_request->scholarship_program}}</td>
				</tr>
			@endif

			@if($tutor_request->concerns)
				<tr>
					<td><b>Other Concerns</b></td>
					<td>{{$tutor_request->concerns}}</td>
				</tr>
			@endif
		</tbody>
	</table>

	<h3>Submit Proposal</h3>
	@if ($tutor_request->tutor_proposal()->exists() && $tutor_request->tutor_proposal->first()->tutor->id == Auth::user()->id)
		<p>You've already submitted a proposal to this student</p>
		<p><b>Rate:</b> ${{$tutor_request->tutor_proposal->first()->proposed_rate}}/hr </p>
		<p><b>Date:</b> {{$tutor_request->tutor_proposal->first()->created_at}} </p>
	@else
		<p>If you would like to work with this student, submit the following form:</p>
		{!! Form::open(['method' => 'POST', 'route' => ['tutor_request.proposal', 'ref_id' => $tutor_request->ref_id], 'class' => 'form-horizontal']) !!}
			<div class="row">
				<div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
				    <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
				        {!! Form::label('rate', 'Proposed Rate ($/hr)') !!}
				        {!! Form::number('rate', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('rate') }}</small>
				    </div>
					
				    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
				        {!! Form::label('message', 'Add a short message with your proposal') !!}
				        <br><small>DO <b>NOT</b> INCLUDE YOUR PERSONAL CONTACT INFO (phone, email, etc).  </small>
				        {!! Form::textarea('message', null, ['class' => 'form-control','size' => '50x4', 'placeholder' => 'This is your first impression, make it a good one.']) !!}
				        <small class="text-danger">{{ $errors->first('message') }}</small>
				        <small><span style='color:red'>WARNING: Including your contact info in your initial proposal above will be viewed as an attempt to steal students</span></small>
				    </div>
			    </div>

			    <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
				    <br>
				    <div class="btn-group">
				        {!! Form::submit("Submit Proposal", ['class' => 'btn btn-primary']) !!}
				    </div>
				    
				    <br><br>

				    <p>After you submit this proposal, you'll be able to communicate with this student through our messaging app (Inbox). </p>
			    </div>
		    </div>
		{!! Form::close() !!}
	@endif
@stop