{!! Form::model($notification, ['method' => 'PATCH', 'route' => ['update.subject.notification', 'notification_id' => $notification->id], 'class' => 'form-horizontal', 'id' => "update_$notification->id"]) !!}

    <div class="col-sm-5 form-group{{ $errors->has('min_rate') ? ' has-error' : '' }}">
        {!! Form::label('min_rate', 'Min Rate ($/hr)') !!}
        {!! Form::number('min_rate', null, ['class' => 'form-control col-sm-4']) !!}
        <small class="text-danger">{{ $errors->first('min_rate') }}</small>
    </div>

    <div class="radio{{ $errors->has('notify') ? ' has-error' : '' }}">
        <label>
            {!! Form::radio('notify', 1,  null) !!} Turn On &nbsp;&nbsp;&nbsp;&nbsp;
        </label>
        <label>
            {!! Form::radio('notify', 0,  null) !!} Turn Off
        </label>
        <small class="text-danger">{{ $errors->first('notify') }}</small>
    </div>

    <div class="form-inline btn-group col-sm-offset-1">
        {!! Form::submit("Update", ['class' => 'btn btn-primary']) !!}
    </div>

{!! Form::close() !!}