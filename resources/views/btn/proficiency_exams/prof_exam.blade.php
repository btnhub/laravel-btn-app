@extends('layouts.josh-front.default', ['page_title' => "Proficiency Exam"])

@section('title')
  Proficiency Exam
@stop

@section('content')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

<b>Instructions</b>
<li>This is a proficiency test so please DO NOT GUESS. You  may submit each exam ONCE, all other attempts will be disregarded.</li>
<li>You may use other resources, e.g. textbooks, during the exam.  You may use the internet to quickly look up equations or definitions, but do not search the entire question for an answer.</li>
<li>For each problem, recognize the underlying concepts covered in the question, consider what students will likely struggle with and what you would do to assist students grasp the concepts or tricks of the problem.</li>

<b>For Math Exams</b>
<li>do NOT use a calculator, unless absolutely necessary. </li>
<li>Math equations and formulas may take a few moments to load.  To enlarge them: Right-Click the equation --> Math Settings --> Scale All Math, set to desired size. </li>

<br>
<b>If there is an error in a question statement, equations or grading, please notify us via e-mail.  Include the question's reference number and a brief description of the problem.</b>
<center><b><span style="color: blue">You must receive an 70% or more to tutor this course.</span></b></center>

<hr>

{!! Form::open(['method' => 'POST', 'route' => 'grade.exam', 'class' => 'form-horizontal']) !!}
    <ol type="1">
        @foreach ($questions as $QuestionNo => $Value)
            <li>
                <b><?php 
                        $reference= $Value['School'].$Value['Semester'].substr($Value['Year'], -2).$Value['Exam'].$Value['Exam_Question_No'];
                        $Question_Stmt = $Value['Question'];

                        if ($Value['Question_Image'] !== "") { // if an image is present
                            $img_location = "$img_dir/".$Value['Question_Image'];
                            $img_tag = "<br><center><img src='$img_location' alt='Please inform us that you were unable to view this image.  Include the reference number for this question.'></center>";
                            $Question_Stmt .= $img_tag;
                        }
                        $Question_Stmt .= "<br>(Ref #: ".$reference.")";
                        echo $Question_Stmt;
                    ?></b>

                    <input type="hidden" name="questions[<?php echo $QuestionNo; ?>]" value="<?php echo $Question_Stmt;?>"/>
                    {!! Form::hidden("cans[$QuestionNo]", Crypt::encrypt($Value['CorrectAnswer'])) !!}
                    {!! Form::hidden('reference[]', $reference) !!}

                <?php 
                foreach ($Value['Answers'] as $Letter => $Answer){ 
                    $Label = 'question-'.$QuestionNo.'-answers-'.$Letter; ?>
                    <div>
                    <input type="radio" name="answers[<?php echo $QuestionNo; ?>]" id="<?php echo $Label; ?>" value="<?php echo $Answer; ?>" />
                    <label for="<?php echo $Label; ?>"><?php echo $Letter; ?>) <?php echo $Answer; ?> </label>                          
                    </div>
                <?php } /*foreach*/ ?>
               <br>
            </li>
        @endforeach

    </ol>
    {!! Form::hidden('exam', $exam) !!}
    <div class="btn-group pull-right">
        {!! Form::submit("Grade Exam", ['class' => 'btn btn-primary']) !!}
    </div>

{!! Form::close() !!}
@stop