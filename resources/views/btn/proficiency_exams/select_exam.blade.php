@extends('layouts.josh-front.default', ['page_title' => "Exam List"])

@section('title')
  Proficiency Exam List
@stop

@section('content')

<h3>Eligibility Criteria </h3>
    You must meet ALL of the criteria below to join our network.
    
    <ol type="1">
    <li>I am a U.S. citizen or I am legally permitted to work in the USA (J-1 and F-1 international students are ineligible); I understand that this is an <b>off-campus position</b>. </li>
    <li>I have at least <b>one year</b> of teaching/tutoring experience of <b>high school or college level courses</b>; I understand that simply helping a friend or classmate with homework or studying does NOT count as teaching experience.</li>
    <li>I have a 3.0 major GPA or better and have received a B+ or better in the courses I wish to tutor. </li>
    <li>I have excellent communication skills; English is <b>NOT</b> a language barrier for me (this also applies to foreign language tutors).</li>
    <li>I will be available to assist students until the <b>end of finals week</b> for the semester that I am employed and I will not be out of town for more than  7 days in the semester (excluding school holidays).</li>
    <li>I will not tutor the specific course that I am <b>currently</b> employed as a teaching assistant, learning assistant, grader, or any other position where I have influence over grades.</li>
    

    </ol>

    <h3>Student/Work Status</h3>
    Select the one that applies to you: <br>
    <input type="radio" name="studentStatus" required>I am a <b>graduate student</b> and I have permission to work off-campus. (Be sure to check with your graduate or research advisors for any conflicts or restrictions associated with a grant or assistantship that you have <b>before</b> applying) <br>
    <input type="radio" name="studentStatus" required>I am an <b>undergraduate student</b>, I am at least a junior in my field of study and I have completed at least ONE year at my current academic institution.<br>
    <input type="radio" name="studentStatus" required>I am neither an undergraduate nor graduate student, but have <b>graduated with at least a bachelor's degree</b>. My current work situation does not restrict my ability to hold a part-time position.<br><br>


  <h3>Take A Proficiency Exam:</h3>
  To demonstrate your proficiency in the courses you wish to tutor, take the following exams.  You will have 3 hours to complete the exam.  ***If you leave this page open for longer than 3 hours, refresh the page before you start an exam or else you will receive an error message. 
  
  {{-- 
  <br><br>
  If a lower level (freshman/sophomore) course you wish to tutor is not listed below, inform us when you schedule an interview.  We may supply you with a different exam to complete. --}}
  
  @if (Carbon\Carbon::now() < Carbon\Carbon::create(2017,10,31,23,59,59,'America/Denver'))
    <hr>
    <p><b>09/21/2017</b></p>
    <p><b> Due to a high volume of requests we have received this semester, please take ONE exam per subject you wish to tutor and then schedule an interview.</b>  Take the highest level exam for the subject area you wish to tutor.  For example, if you wish to tutor the entire Calculus series, take the Calc 3 exam.  </p>
    <b>Note to Chemistry Tutors:</b> If you wish to tutor General Chemistry and Organic Chemistry, please take ONE exam from each series before scheduling an interview.
    <hr>
  @endif
  
  <p><center><b> You must receive an 70% or better on each exam you take </b></center></p><br>

{!! Form::open(['method' => 'POST', 'route' => 'load.selected.exam', 'class' => 'form-horizontal', 'target' => '_blank']) !!}
    @foreach (collect($exam_list)->chunk(3) as $exam_chunk)
      <div class="row">
        @foreach ($exam_chunk as $dept => $exams)
          <div class="col-md-4 col-sm-4">        
            <b>{{ucfirst($dept)}}</b>
            <div class="radio{{ $errors->has('exam') ? ' has-error' : '' }}">
                @foreach ($exams as $exam => $exam_name)
                  <label>
                      {!! Form::radio('exam', $exam,  null, ['required']) !!} {{$exam_name}}
                  </label>
                  <br>
                @endforeach
                <small class="text-danger">{{ $errors->first('exam') }}</small>
            </div>
          </div>
        @endforeach
      </div>
    
    @endforeach
    
    <div class="btn-group col-md-offset-5">
        {!! Form::submit("Start Exam", ['class' => 'btn btn-primary']) !!}
    </div>

{!! Form::close() !!}


@stop