@extends('layouts.josh-front.default', ['page_title' => "Exam Results"])

@section('title')
  Proficiency Exam Results
@stop

@section('content')
<script type="text/javascript" async
  src="//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-MML-AM_CHTML">
</script> 

<h1>Proficiency Exam Results</h1>
        <b>Name: {{Auth::user()->full_name}}<br>
        Final Score: {{$score}} / {{count($Questions)}} = {{round($final_score*100)}}% </b>
        <br>

        @if ($final_score >= $min_score)
            <center><b><span style= "color: blue;">Congratulations, your score is high enough to tutor this course!</span></b></center></b> <br> 
                Be sure to take ALL the proficiency exams for ALL the courses you wish to tutor before scheduling an interview.<br><br>
            
        @else
            <b><span style= "color: red;">Your score is not high enough to tutor this course.</span></b> <br>
                Please do not retake this exam! Since the answers are provided, if you resubmit this exam we will consider it cheating.
        @endif
        <br><br>


        <b>If there is an error in a question or the grading, please e-mail us the reference number and a brief explanation of the problem.</b><br>

        <hr>

        <ol type="1">
            @foreach ($Questions as $QuestionNo => $Question)
                <li>
                    <b>{!!$Question!!}</b><br>
                    {!!$Answers[$QuestionNo]!!}
                </li>
                <br>
            @endforeach
        </ol>

@stop