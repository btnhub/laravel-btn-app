<?php $cities = $cities ?? null; ?>

@extends('layouts.josh-front.default', ['page_title' => "{$cities} Tutor Profiles"])

@section('title')
	{{$cities ? "$cities " : ""}}Tutor Profiles
@stop
@section('meta-description')
List of private tutors for College & High School Math, Chemistry, Physics, Science, Economics, and more.  We have a network of exceptional tutors in {{App\Location::where('students_visible', 1)->pluck('city')->implode(', ')}}.
@stop
@section('content')
	<div class="row" >
		<h3>Search our list for the perfect tutor for you!</h3> 
		<p>We've made finding the right tutor as easy as possible for you.  We're not just another tutor list, all our tutors have:
			<ol>
				<li>at least a 3.0 GPA</li>
				<li>at least 1 year of teaching/tutoring experience</li>
				<li>passed our proficiency exams (Math & Science tutors)</li>
				<li>passed a background check.</li>
			</ol>
		</p>
		<br>
		<h4 style="font-weight:bold">To narrow our list and find the right tutor for you, <a class="" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"> click here </a>.  </h4>
		<p>The tutors are <b>ranked</b> based on their performance (positive & negative evaluations, number of hours worked, complaints).  Be sure to read the reviews listed on each tutor's profile. </p>
		{{-- <p><b style="color:red">10/27/2017:</b> At this point in the semester, most of <b style="color:darkred">our Math & Science tutors are fully booked</b>. We apologize for any delay or inconvenience.</p> --}}
		<div class="collapse" id="collapseExample">
			<div class="card card-block" align="center">
				<div class="col-md-10 col-sm-10">
					@include('btn.forms.search_tutors', ['locations' => $locations])	
				</div>
			</div>
		</div>	
	</div>
<br><br>
	<div class="container">
		<div class="row" style="min-height:500px">
			@if (!$tutors->isEmpty())
				<div class="row">
					<h4>{{$tutors->total()}} {{$cities ?? ''}} {{ str_plural('Tutor', $tutors->total()) }}</h4>
				</div>
				<table class="table table-striped table-hover">
					<tbody>
						@foreach ($tutors as $tutor)			
							<tr onclick="location.href='{{ route('profile.index', $tutor->ref_id)}}'" style="cursor: pointer;">
								<td width="25%">
									<div class="media">
										<div class="media-body">
											@if (auth()->user() && auth()->user()->isStaff())
												<a href="{{ route('profile.index', $tutor->ref_id)}}"><h4 class="media-heading">{{$tutor->full_name}}</h4></a>
											@else
												<a href="{{ route('profile.index', $tutor->ref_id)}}"><h4 class="media-heading">{{$tutor->username}}</h4></a>
											@endif

											@if ($tutor->id == 63)
												<b>BuffTutor Director</b><br>
											@endif
											{{$tutor->locations->implode('city', ', ')}} Tutor<br>
											@if ($tutor->profile->instruments)
												{{$tutor->profile->instruments}} Instructor<br>
											@endif						
											{{$tutor->profile->education()}}<br>
											{{$tutor->profile->major}}<br>
											
											@include('btn.partials.hours_worked')

											@if ($tutor->profile->tutor_status == 1)
												<span style="color: green">Accepting Students</span>
											@else 
												<span style="color: darkred">No Longer Accepting Students</span>
											@endif
											@if (auth()->user() && auth()->user()->isStaff())
												<br><br>
												<span style="color:darkblue">${{$tutor->profile->rate_min}} - ${{$tutor->profile->rate_max}}</span><br>

												@if ($assign_count = $tutor->active_assignments->count())
													<span style="color:purple;font-weight:bold">{{$assign_count}} Active Assignments</span><br>
												@endif
												
												@if ($contracts_count = $tutor->tutor_contracts->count())
													{{$contracts_count}} Tutor Contracts <br>
													
													@if ($missed_contracts = $tutor->tutor_contracts->where('tutor_decision', NULL)->count())
														<span style="color:red">{{$missed_contracts}} Missed Tutor Contracts </span><br>
													@endif
													
													@if ($declined_contracts = $tutor->tutor_contracts->where('tutor_decision', 0)->count())
														<span style="color:darkred">{{$declined_contracts}} Declined Tutor Contracts </span>
													@endif
												@endif
											@endif
										</div>
									</div>
								</td>
								<td>
									<b>{{$tutor->first_name}} can meet: {{$tutor->profile->session_locations}}</b><br>
									{{substr($tutor->profile->tutor_experience, 0, 1000)}}...
									@if (auth()->user() && auth()->user()->isSuperUser())
				                        @foreach ($tutor->proficiency_exam_results() as $exam_result)
				                            <br>
				                            <span @if ($exam_result->grade < 75)
				                                style="color:red"
				                                @else
				                                style="color:blue"
				                            @endif>{{$exam_result->exam}} - {{$exam_result->grade}}%</span>
				                        @endforeach
				                    @endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				{!! $tutors->render()!!}
			@else
				<h4>{{$tutors->total()}} {{$cities ?? ''}} {{ str_plural('Tutor', $tutors->total()) }}</h4>
				No tutors found.
			@endif
		</div>
	</div>
@stop