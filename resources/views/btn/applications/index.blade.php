@extends('layouts.josh-front.default', ['page_title' => "Tutor Applications"])

@section('content')
    <h1>List of Tutor Applications</h1>
    <table class="table table-bordered" id="applicants-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Applicant Name</th>
                <th>Location</th>
                <th>Major</th>
                <th>Year</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Created At</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
@stop

@push('scripts')
	<script>
		$(function() {
		    $('#applicants-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!! route('applicants.data') !!}',
		        columns: [
		            { data: 'id', name: 'id' },
		            { data: 'first_name', name: 'first_name' },
		            { data: 'company_locations', name: 'company_locations' },
		            { data: 'major', name: 'major' },
		            { data: 'year', name: 'year' },
		            { data: 'start_date', name: 'start_date' },
		            { data: 'end_date', name: 'end_date' },
		            { data: 'created_at', name: 'created_at' },
		        ]
		    });
		});
	</script>
@endpush