@extends('layouts.josh-front.default')

@section('content')
	<a href="{{ route('applicants')}}"><-- Back to Applicants</a><br>
	<div class="col-md-8">
		<h2>{{$applicant->first_name}} {{$applicant->last_name}}'s Application</h2>
		{{$applicant->email}}  <br>
		{{$applicant->phone}}<br>
		
		<table class="table">
			<tbody>
				<tr>
					<td><b>Hired:</b></td>
					<td>@if ($applicant->hired)
						<span style="color:green">Yes</span>
						@else
						<span style="color:red">No</span>
						@endif
					</td>
				</tr>
				<tr>
					<td><b>Start / End Dates:</b></td>
					<td>{{$applicant->start_date}} / {{$applicant->end_date}}</td>
				</tr>
				<tr>
					<td><b>Location:</b></td>
					<td>{{$applicant->company_locations}}</td>
				</tr>
				<tr>
					<td><b>Session Locations:</b></td>
					<td>{{$applicant->session_locations}}</td>
				</tr>
				<tr>
					<td><b>Major/Year:</b></td>
					<td>{{$applicant->major}} / {{$applicant->year}}</td>
				</tr>
				<tr>
					<td><b>Availability:</b></td>
					<td>{{$applicant->availability}}</td>
				</tr>
				<tr>
					<td><b>Courses:</b></td>
					<td>{!! nl2br(e($applicant->courses)) !!}</td>
				</tr>
				<tr>
					<td><b>Experience:</b></td>
					<td>{!! nl2br(e($applicant->experience)) !!}</td>
				</tr>
				<tr>
					<td><b>Concerns:</b></td>
					<td>{{$applicant->concerns}}</td>
				</tr>
				<tr>
					<td><b>Marketing:</b></td>
					<td>{{$applicant->marketing}}</td>
				</tr>
				<tr>
					<td><b>Notes:</b></td>
					<td>{{$applicant->admin_notes}}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-4">
		<h3>Exam Results</h3>
		<table class="table">
			<thead>
				<tr>
					<th>Exam</th>
					<th>Grade</th>
				</tr>
			</thead>
			<tbody>
				@if ($applicant->exam_results())
					@foreach ($applicant->exam_results() as $result)
						<tr>
							<td>{{$result->exam}}</td>
							<td>{{$result->grade}}</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
	

<table class="table">
	<tbody>
		<tr>
			<td>
				{!! Form::open(['method' => 'PATCH', 'route' => ['applicant.update', 'id' => $applicant->id], 'class' => 'form-horizontal']) !!}	
		   			 <div class="btn-group pull-left">
		        		{!! Form::submit("Hire Tutor", ['class' => 'btn btn-primary']) !!}
		    		</div>
				{!! Form::close() !!}
			</td>
			<td>
				{!! Form::model($applicant, ['method' => 'DELETE', 'route' => ['applicant.remove', 'id' => $applicant->id], 'class' => 'form-horizontal']) !!}	
				    <div class="form-group{{ $errors->has('admin_notes') ? ' has-error' : '' }}">
				        {!! Form::label('admin_notes', 'Notes (Reason For Not Hiring Tutor)') !!}
				        {!! Form::textarea('admin_notes', null, ['class' => 'form-control', 'size' => "10x3"]) !!}
				        <small class="text-danger">{{ $errors->first('admin_notes') }}</small>
				    </div>
				    <div class="btn-group pull-right">
				        {!! Form::submit("Do Not Hire", ['class' => 'btn btn-danger']) !!}
				    </div>
				{!! Form::close() !!}	
			</td>
		</tr>
	</tbody>
</table>

@stop