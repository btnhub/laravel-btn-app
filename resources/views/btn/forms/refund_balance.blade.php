@extends('layouts.josh-front.default', ['page_title' => "Refund My Balance"])

@section('title')
	Refund Balance
@stop

@section('content')
	<h3>Refund Account Balance Of ${{$student->studentBalance()}}</h3>
	<div class="row">
		<a href="{{route('profile.account', ['user_ref_id' => $student->ref_id])}}"> <-- Back</a>
	</div>

	<div class="col-md-6"> 
		{!! Form::open(['method' => 'POST', 'route' => 'refund.account.post', 'class' => 'form-horizontal']) !!}
						
			<div id="refund_details">
				
				<div class="radio{{ $errors->has('refund_method') ? ' has-error' : '' }}">
				<p><b>Select How You Would Like To Be Refunded</b></p>
				<p>Credit / Debit card refunds can only be issued if you paid using that method. Payments completed by PayPal or Square Cash cannot be refunded directly to your card.</p>
				    @foreach ($refund_methods as $refund_method)
					    <label>
					        {!! Form::radio('refund_method', $refund_method->method,  null, ['id' =>"$refund_method->method" , 'required' => 'required', 'onclick' => "refundDetails($refund_method->email_required)"]) !!} {{$refund_method->title}}
					    </label>
					    &nbsp;&nbsp;&nbsp;&nbsp;
				    @endforeach
				    <small class="text-danger">{{ $errors->first('refund_method') }}</small>
				</div>
				<br>
				<div id="email" style="display: none">
					<div class="form-group{{ $errors->has('method_details') ? ' has-error' : '' }}">
					    {!! Form::label('method_details', 'Email Address Associated With The Selected Method') !!}
					    {!! Form::text('method_details', $student->email, ['class' => 'form-control']) !!}
					    <small class="text-danger">{{ $errors->first('method_details') }}</small>
					</div>
				</div>

				<div id="address" style="display: none">
					<div class="form-group{{ $errors->has('check_address') ? ' has-error' : '' }}">
					    {!! Form::label('check_address', 'Address To Send Check') !!}. The check will be written and sent to {{$student->first_name}}, however if it must be written and sent to {{$student->first_name}}'s parent/guardian, include the parent/guardian's name below. 
					    {!! Form::textarea('check_address', null, ['class' => 'form-control', 'size' => '40x3']) !!}
					    <small class="text-danger">{{ $errors->first('check_address') }}</small>
					</div>
				</div>
			</div>

		    <div class="btn-group pull-left">
		        {!! Form::submit("Request Refund", ['class' => 'btn btn-primary']) !!}
		    </div>
		
		{!! Form::close() !!}
	</div>
@stop

@push('scripts')
<script type="text/javascript">

	function refundDetails(show_email)
	{		
		if (show_email && document.getElementById('Check').checked === false) {
	        document.getElementById('email').style.display = 'block';
	        document.getElementById('address').style.display = 'none';
	    } 

	    else if (document.getElementById('Check').checked)
	    {
	    	document.getElementById('email').style.display = 'none';
	        document.getElementById('address').style.display = 'block';
	    }

	    else {
	        document.getElementById('email').style.display = 'none';
	        document.getElementById('address').style.display = 'none';
	    }
	}
</script>
@endpush