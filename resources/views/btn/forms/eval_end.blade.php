@extends('layouts.josh-front.default', ['page_title' => "End Of Semester Evaluation"])

@section('title')
	Evaluation: End Of Semester
@stop

@section('content')
 	<h3>End Of Semester Evaluation </h3>
 	<p>Use this form to let us know how the semester went with {{$assignment->tutor->first_name}}. </p>
 	<b>Tutor</b>: {{$assignment->tutor->full_name}}<br>
 	<b>Course</b>: {{$assignment->course}}
 	
 	@unless (count($assignment->evaluation->where('type', 'End')))
		<div class="container">
		    <div class="row">
		        <div class="col-md-8 col-md-offset-2">
		            <div class="panel panel-info">
		                <div class="panel-heading">Evaluate Overall Experience</div>
		                <div class="panel-body"> 	
						 	{!! Form::open(['method' => 'POST', 'route' => ['submit.eval.end', 'assign_ref_id' => $assignment->ref_id], 'class' => 'form-horizontal']) !!} 	


						 		<div class="radio{{ $errors->has('frequency') ? ' has-error' : '' }}">
						 		    <b>How often did you meet with {{$assignment->tutor->first_name}}?* </b>
						 		    <br>
						 		    <label>
						 		        {!! Form::radio('frequency', 'Once A Week',  null) !!} Once A Week
						 		    </label>
						 		    <br>
						 		    <label>
						 		        {!! Form::radio('frequency', 'Multiple Times A Week',  null) !!} Multiple Times A Week
						 		    </label>
						 		    <br>
						 		    <label>
						 		        {!! Form::radio('frequency', 'Only For Project Help',  null) !!} Only For Project Help
						 		    </label>
						 		    <br>
						 		    <label>
						 		        {!! Form::radio('frequency', 'Only For Midterm Reviews',  null) !!} Only For Midterm Reviews <br>
						 		    </label>
						 		    <br>
									<label>
						 		        {!! Form::radio('frequency', 'Only For Final Review',  null) !!} Only For Final Review
						 		    </label>

						 		    <small class="text-danger">{{ $errors->first('frequency') }}</small>
						 		</div>

						 		<hr>
						 		
						 		<p><b>Please be brutally honest in your comments below, this is your education we're talking about! Please try to address the following:</b></p>
						 
								<li> Explain what made your tutor's teaching style effective.  Would you want to work with him/her again in the future?  Why?</li>
								<li> Was he/she patient with you? Did you feel comfortable asking ANY question?</li>
								<li> Was your tutor professional and always prepared for sessions?</li>
								<li> Did your tutor frequently reschedule or cancel sessions? Was your tutor frequently late?</li>
								<li> Was your tutor's rate reasonable? Was working with this tutor more beneficial than using the free services provided by the school, e.g. office hours and helprooms? Why?</li>

						 	    <hr>
						 	    
						 	    <div class="form-group{{ $errors->has('tutor_strength') ? ' has-error' : '' }}">
						 	        {!! Form::label('tutor_strength', 'How did your tutor exceed your expectations? What would you like to see your tutor do more of?*') !!}
						 	        {!! Form::textarea('tutor_strength', null, ['class' => 'form-control', 'size' => '40x3']) !!}
						 	        <small class="text-danger">{{ $errors->first('tutor_strength') }}</small>
						 	    </div>


						 	    <div class="form-group{{ $errors->has('tutor_weakness') ? ' has-error' : '' }}">
						 	        {!! Form::label('tutor_weakness', 'What should your tutor improve on? What should your tutor do less of? How did your tutor fail to meet your expectations?*') !!}
						 	        {!! Form::textarea('tutor_weakness', null, ['class' => 'form-control', 'size' => '40x3']) !!}
						 	        <small class="text-danger">{{ $errors->first('tutor_weakness') }}</small>
						 	    </div>
						 	    
						 	    <div class="radio{{ $errors->has('satisfied') ? ' has-error' : '' }}">
						 	        <b>Would you recommend {{$assignment->tutor->first_name}} to your friends?*</b><br>
						 	        <label>
						 	            {!! Form::radio('satisfied', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;&nbsp;
						 	        </label>
						 	        <label>
						 	            {!! Form::radio('satisfied', 0,  null) !!} No
						 	        </label>
						 	        <small class="text-danger">{{ $errors->first('satisfied') }}</small>
						 	    </div>
						 	    <br>

						 	    <div class="col-md-12 form-group{{ $errors->has('website_reference') ? ' has-error' : '' }}">
						 	        {!! Form::label('website_reference', "Website Review (Optional): ") !!}
						 	        <br>
						 	        Tell the world about your experience with {{$assignment->tutor->first_name}} (whether good or bad).  The following comments will show up on {{$assignment->tutor->first_name}}'s profile.
						 	        {!! Form::textarea('website_reference', null, ['class' => 'form-control', 'size' => '40x2']) !!}
						 	        <small class="text-danger">{{ $errors->first('website_reference') }}</small>
						 	    </div>

						 	    <br>

						 	    <div class="btn-group pull-right">
						 	        {!! Form::submit("Submit Evaluation", ['class' => 'btn btn-primary']) !!}
						 	    </div>
						 	
						 	{!! Form::close() !!}
					 	</div>
				 	</div>
			 	</div>
		 	</div>
	 	</div>
 	@else
 		You have already submitted an evaluation of {{$assignment->tutor->first_name}}.
 	@endunless
@stop