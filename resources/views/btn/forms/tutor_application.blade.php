@extends('layouts.josh-front.default', ['page_title' => "Tutor Application"])

@section('title')
	Tutor Application
@stop

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-9 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Tutor Application</div>
        <div class="panel-body">
			{!! Form::open(['method' => 'POST', 'route' => 'tutor.apply.submit', 'class' => 'form-horizontal']) !!}
				<div class="form-group">
	                <div class="checkbox-inline">
	                    <div class="checkbox{{ $errors->has('eligibility') ? ' has-error' : '' }}">
	                        <label for="eligibility">
	                            {!! Form::checkbox('eligibility', '1', null, ['id' => 'eligibility']) !!} I have reviewed the <a href="{{route('become.tutor')}}">eligibility criteria</a> and I am eligible to work as a tutor for BuffTutor.
	                        </label>
	                    </div>
	                    <small class="text-danger">{{ $errors->first('eligibility') }}</small>
	                </div>
	            </div>

				<div class="col-md-12">
				    <div class="col-md-5 mol-sm-5 form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
				        {!! Form::label('first_name', 'First Name') !!}
				        {!! Form::text('first_name', (Auth::user()->first_name ?? ''), ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('first_name') }}</small>
				    </div>

				    <div class="col-md-5 mol-sm-5 col-md-offset-1 col-sm-offset-1 form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
				        {!! Form::label('last_name', 'Last Name') !!}
				        {!! Form::text('last_name', (Auth::user()->last_name ?? ''), ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('last_name') }}</small>
				    </div>
				</div>
				<div class="col-md-12">
				    <div class="col-md-5 mol-sm-5 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
				        {!! Form::label('email', 'Email') !!}
				        {!! Form::text('email', (Auth::user()->email ?? ''), ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('email') }}</small>
				    </div>	

				    <div class="col-md-5 mol-sm-5 form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
				        {!! Form::label('phone', 'Phone') !!}
				        {!! Form::text('phone', (Auth::user()->phone ?? ''), ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('phone') }}</small>
				    </div>
			    </div>

			    <div class="col-md-12">
				    <div class="col-md-5 mol-sm-5 form-group{{ $errors->has('major') ? ' has-error' : '' }}">
				        {!! Form::label('major', 'Major') !!}
				        {!! Form::text('major', (Auth::user()->profile->major ?? ''), ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('major') }}</small>
				    </div>

				    <div class="col-md-5 mol-sm-5 form-group{{ $errors->has('year') ? ' has-error' : '' }}">
				        {!! Form::label('year', 'Current Education Level') !!}
				        {!! Form::text('year', null, ['class' => 'form-control', 'placeholder' => '2nd Year Grad Student, Senior, Graduated with M.S.']) !!}
				        <small class="text-danger">{{ $errors->first('year') }}</small>
				    </div>
			    </div>
			    <div class="row">
			    	<div class="col-md-12">
					    <div class="col-md-9 col-sm-9">
						    <div class="form-group{{ $errors->has('availability') ? ' has-error' : '' }}">
						        {!! Form::label('availability', 'Availability') !!}
						        <small>How many hours a week are you available?</small>
						        {!! Form::text('availability', null, ['class' => 'form-control']) !!}
						        <small class="text-danger">{{ $errors->first('availability') }}</small>
						    </div>	
					    </div>
				    </div>	
			    </div>
			    

			    <div class="row">
				    <div class="col-md-12 form-group">
				        {{-- <div class="checkbox{{ $errors->has('locations') ? ' has-error' : '' }}">
				            <p><b>Select location(s) where you'd like to work.</b></p>
				            @foreach ($locations as $location)
					            <label>
					                {!! Form::checkbox('locations[]', $location->id, null) !!} {{$location->city}}{{$location->state ? ", $location->state": null}}        &nbsp; &nbsp; &nbsp; &nbsp;
					            </label>
				            @endforeach
				        </div>
				        <small class="text-danger">{{ $errors->first('locations') }}</small> --}}
				        <div class="col-md-6 col-sm-6">
					    	<div class="{{ $errors->has('locations[]') ? ' has-error' : '' }}">
					    	    <p><b>Select location(s) where you'd like to work.</b></p>
					    	    {!! Form::select('locations[]', $locations, null, ['id' => 'locations[]', 'class' => 'form-control', 'required' => 'required', 'multiple', 'size' => 6]) !!}
					    	    <small class="text-danger">{{ $errors->first('locations[]') }}</small>
					    	</div>
				    	</div>
				    </div>
				</div>
				<div class="col-md-12">
				    <div class="form-group{{ $errors->has('session_locations') ? ' has-error' : '' }}">
				        {!! Form::label('session_locations', 'Where can you meet for sessions') !!}
				        <small>CU-Boulder Campus only, Downtown Fort Collins, Louisville, 10 mile radius around Denver</small>
				        {!! Form::text('session_locations', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('session_locations') }}</small>
				    </div>
			    </div>

			    <div class="col-md-12">
				    <div class="form-group{{ $errors->has('courses') ? ' has-error' : '' }}">
				        {!! Form::label('courses', 'Courses') !!}
				        <br><small>List courses you are proficient in AND can teach, not just ones you did well in.  AVOID vague statements like "Any Physics course" because that is a false and useless statment. </small>
				        {!! Form::textarea('courses', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('courses') }}</small>
				    </div>
			    </div>

			    <div class="col-md-12">
				    <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
				        {!! Form::label('experience', 'Past Teaching/Tutoring Experience') !!}
				        <br><small>Detail your past teaching and tutoring experience.  Be sure to include 
				        <br> 1) Duration at each position (1 semester, 1 year, 6 months)
				        <br> 2) Challenges you faced and how you handled them
				        <br> 3) How your past experience has influenced your teaching/tutoring approach.</small>
				        {!! Form::textarea('experience', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('experience') }}</small>
				    </div>
			    </div>

			    <div class="col-md-12">
				    <div class="form-group{{ $errors->has('concerns') ? ' has-error' : '' }}">
				        {!! Form::label('concerns', 'Concerns') !!}
				        <br><small>Any thing we need to keep in mind when reviewing your application.  For example, "I am job searching and will leave mid-semester when I get my dream job".  Do NOT add questions here, you can pose questions in the interview.</small>
				        {!! Form::text('concerns', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('concerns') }}</small>
				    </div>
			    </div>

			    <div class="col-md-12">
				    <div class="col-md-5 col-sm-5 form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
				        {!! Form::label('start_date', 'Start Date') !!}
				        {!! Form::date('start_date',Carbon\Carbon::now()->addDays(7), ['class' => 'form-control', 'required' => 'required']) !!}
				        <small class="text-danger">{{ $errors->first('start_date') }}</small>
				    </div>

				    <div class="col-md-5 col-sm-5 form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
				        {!! Form::label('end_date', 'End Date') !!}
				        {!! Form::date('end_date',Carbon\Carbon::now()->addMonths(4), ['class' => 'form-control', 'required']) !!}
				        <small class="text-danger">{{ $errors->first('end_date') }}</small>
				    </div>
			    </div>	

			    <div class="col-md-12">
				    <div class="form-group{{ $errors->has('marketing') ? ' has-error' : '' }}">
				        {!! Form::label('marketing', 'How did you hear about us?') !!}
				        {!! Form::text('marketing', null, ['class' => 'form-control']) !!}
				        <small class="text-danger">{{ $errors->first('marketing') }}</small>
				    </div>
			    </div>

			    <div class="form-group">
                    <div class="checkbox-inline">
                        <div class="checkbox{{ $errors->has('terms') ? ' has-error' : '' }}">
                            <label for="terms">
                                {!! Form::checkbox('terms', '1', null, ['id' => 'terms']) !!} I agree to the terms of use.
                            </label>
                        </div>
                        <small class="text-danger">{{ $errors->first('terms') }}</small>
                    </div>
                </div>

			    <div class="btn-group pull-right">
			        {!! Form::submit("Submit Application", ['class' => 'btn btn-success']) !!}
			    </div>
			
			{!! Form::close() !!}
		</div>
	  </div>
	</div>
  </div>
</div>
@stop