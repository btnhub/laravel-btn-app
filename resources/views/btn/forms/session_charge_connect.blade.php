@extends('layouts.josh-front.default', ['page_title' => "Submit A Charge"])

@section('title')
	Charge Session
@stop

@section('content')
	
	<h3>Charge {{$assignment->student->full_name}} - {{$assignment->course}}</h3>

	<h4><b>Rate:</b> ${{$assignment->tutorContract->tutor_rate}} / hour</h4>
	<div class="container">
	    <div class="row">
	        <div class="col-md-6 col-md-offset-2">
	            <div class="panel panel-info">
	                <div class="panel-heading">Charge {{$assignment->student->first_name}}'s Card</div>
	                <div class="panel-body">
						{!! Form::open(['method' => 'POST', 'route' => ['session.charge.customer', 'assign_id' => $assignment->ref_id], 'class' => 'form-horizontal', 'onSubmit' => 'calcCharge()']) !!}
							<div class="col-md-12 form-group{{ $errors->has('session_date') ? ' has-error' : '' }}">
							    {!! Form::label('session_date', 'Session Date') !!}
							    {!! Form::date('session_date', Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
							    <small class="text-danger">{{ $errors->first('session_date') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
							    {!! Form::label('duration', 'Session Duration') !!}
							    {!! Form::select('duration',$session_duration_array, null, ['id' => 'duration', 'class' => 'form-control', 'required' => 'required', 'onChange' => 'calcCharge()']) !!}
							    <small class="text-danger">{{ $errors->first('duration') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('outcome') ? ' has-error' : '' }}">
							    {!! Form::label('outcome', 'Session Outcome') !!}
							    {!! Form::select('outcome',[null=> 'Select An Outcome', 'Student & Tutor Met As Scheduled' => 'Student & Tutor Met As Scheduled', 'Student Cancelled Without 24 Hours Notice' => 'Student Cancelled Without 24 Hours Notice', 'Tutor Cancelled Without 24 Hours Notice' => 'Tutor Cancelled Without 24 Hours Notice', 'Free Session' => 'Free Session'], null, ['id' => 'outcome', 'class' => 'form-control', 'required' => 'required', 'onChange' => 'calcCharge()']) !!}
							    <small class="text-danger">{{ $errors->first('outcome') }}</small>
							</div>
					
							<div class="col-md-12 form-group{{ $errors->has('tutor_pay') ? ' has-error' : '' }}">
							    {!! Form::label('tutor_pay', 'Your Earnings') !!}
							    {!! Form::text('tutor_pay', null, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
							    <small class="text-danger">{{ $errors->first('tutor_pay') }}</small>
							</div>

							<div class="btn-group pull-right">
						        {!! Form::submit("Charge {$assignment->student->first_name}'s CC", ['class' => 'btn btn-primary']) !!}
						    </div>

						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@push('scripts')
	<script type="text/javascript">
		function calcCharge()
		{
			var outcome = document.getElementById('outcome').value; 
			var charge_student = 1;

			if (outcome == 'Free Session' || outcome == 'Tutor Cancelled Without 24 Hours Notice') {
				charge_student = 0;
			}
	
			var billing_increment = 60; 
			var duration = document.getElementById('duration').value; 
			var tutor_rate = {{$assignment->tutorContract->tutor_rate}};
			var tutor_pay = charge_student * tutor_rate * (duration / billing_increment);
			document.getElementById('tutor_pay').value = tutor_pay.toFixed(2); // return to 2 decimal places
		}
	</script>
@endpush