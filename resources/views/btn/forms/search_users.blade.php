{!! Form::open(['method' => 'GET', 'route' => 'search.users', 'class' => 'navbar-form navbar-left']) !!}

    <div class="form-group{{ $errors->has('search') ? ' has-error' : '' }}">
        {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Find User']) !!}
    </div>

{!! Form::close() !!}