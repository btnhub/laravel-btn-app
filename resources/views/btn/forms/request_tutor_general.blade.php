<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {{-- <div class="panel panel-default"> --}}
            <div class="panel panel-info">
                <div class="panel-heading">Student Contact Info</div>
                <div class="panel-body">                        
                    {!! Form::open(['method' => 'POST', 'route' => 'request.post', 'class' => 'form-horizontal']) !!}

                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Student Info</legend>
                            @if (Auth::user())
                                <h4><b>{{Auth::user()->full_name}}</b></h4> 
                                <h4>{{Auth::user()->email}}</h4> 
                                <h4>{{Auth::user()->phone}}</h4> 
                                If the above is incorrect, please <a href="{{route('profile.edit', Auth::user()->ref_id)}}">update your information</a>.
                                <br><br>
                            @else
                                <h4>Please Create An Account</h4>
                                <p>If you already have an account, please first <a href="{{url('/login')}}">login</a> or <a href="{{url('password/reset')}}">reset your password</a>. If not, let's create one!</p> 
                                
                                    @include('btn.forms.partials.student_registration_fields')    
                                
                                <br><br>
                            @endif
                        </fieldset>
                </div> {{--panel-body--}}
            </div> {{--panel--}}
            <div class="panel panel-primary">
                <div class="panel-heading">Tutor Request Details</div>
                <div class="panel-body">
                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Course & Session Details</legend>
                            <div class="col-md-5 col-sm-5">
                                {{--Select Education Level--}}
                                <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
                                    <b>Course Level.</b><br>
                                    {!! Form::select('level',$course_level_options, null, ['id' => 'level', 'class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('level') }}</small>
                                </div>
                                
                            </div>

                            {{-- Type In Course Name --}}
                            <div class="col-md-5 col-sm-5 form-group{{ $errors->has('course') ? ' has-error' : '' }} col-sm-offset-1">
                                    <b>Course Name & Number</b> 
                                {!! Form::text('course', null, ['class' => 'form-control', 'placeholder' => 'Calc 1 - MATH 1300']) !!}
                                <small><span style="color:darkred">Please list only ONE course, submit the form again for additional courses</span></small>
                                <small class="text-danger">{{ $errors->first('course') }}</small>                                
                            </div>

                            <div class="row">
                                <div class="col-md-5 col-sm-5">
                                <div class="form-group{{ $errors->has('session_time[]') ? ' has-error' : '' }}">
                                    <b>Session Time</b><br>
                                    When are you able to meet.
                                    {!! Form::select('session_time[]',$session_times_options, null, ['id' => 'session_time[]', 'class' => 'form-control', 'required' => 'required', 'multiple', 'size' => 6]) !!}
                                    <small class="text-danger">{{ $errors->first('session_time[]') }}</small>
                                </div>
                                
                            </div>

                            <div class="col-md-5 col-sm-5 col-sm-offset-1">
                                {{-- Select Frequency Of Sessions --}}
                                <div class="form-group{{ $errors->has('frequency') ? ' has-error' : '' }}">
                                    <b>Session Frequency</b>
                                    <div> How often would you like to meet.</div>
                                    {!! Form::text('frequency', null, ['class' => 'form-control', 'placeholder' => 'Twice a week, Only before exams, Mondays']) !!}
                                    <small class="text-danger">{{ $errors->first('frequency') }}</small>
                                </div>
                                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                    <b>Location</b>  
                                    <div>Select the closest town/city to you:</div>
                                    {!! Form::select('location',$location_options, null, ['id' => 'location', 'class' => 'form-control', 'required' => 'required']) !!}
                                    <small class="text-danger">{{ $errors->first('location') }}</small>
                                </div>
                                <div class="form-group" style="color:black">
                                    <div class="checkbox{{ $errors->has('include_online_tutors') ? ' has-error' : '' }}">
                                        <label style="color:black">
                                            {!! Form::checkbox('include_online_tutors', '1', null, ['id' => 'include_online_tutors']) !!}<div> Match me with an online tutor if in-person tutors are not available.</div>
                                        </label>
                                    </div>
                                </div>
                            </div>    
                            </div>
                            

                            <div class="row">
                                <div class="col-md-10 col-sm-10">
                                {{-- Select Max Rate --}}
                                <div class="form-group">
                                    <b>Max Affordable Hourly Rate:</b> <br>
                                    Each tutor sets his/her own rate, let us know what you can afford and we'll match you with tutors in your budget. Review <a data-toggle="collapse" href="#collapseRate" aria-expanded="false" aria-controls="collapseRate">rate guide</a> to see what tutors typically charge.<br>

                                    {{-- Rate Guide Table --}}
                                    <div class="collapse" id="collapseRate">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="card card-block">
                                                    @include('btn.forms.partials.rate_guide')
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                
                                <div class="col-md-8 col-xs-8">
                                    @include('btn.forms.partials.rate_options')    
                                </div>
                                
                                </div>    
                            </div> 
                            
                        </fieldset>

                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Additional Details <small>(optional)</small></legend>
                            <div class="col-md-10">
                                <div class="form-group{{ $errors->has('requested_tutors') ? ' has-error' : '' }}">
                                    <b>Preferred Tutors.</b> List 1-3 tutors you'd like to work with (in order of preference). If these tutors are not available, you will be matched with a different tutor.<br>
                                    Search our <a href="{{route('list.tutors')}}" target='_blank'>tutor list</a> for the best tutor for you. 
                                    {!! Form::text('requested_tutors', null, ['class' => 'form-control']) !!}
                                    <small class="text-danger">{{ $errors->first('requested_tutors') }}</small>
                                </div>
                            </div>

                            {{-- Additional Concerns --}}
                            <div class="col-md-11 col-sm-11">
                                <div class="form-group{{ $errors->has('concerns') ? ' has-error' : '' }}">
                                    <b>Other Requests Or Concerns</b> &nbsp;&nbsp;&nbsp;
                                    <a data-toggle="collapse" href="#concerns-examples" aria-expanded="false" aria-controls="concerns-examples">Examples</a>

                                    <div class="collapse" id="concerns-examples">
                                        <li>If you prefer to meet in your home, please include the town and neighborhood or the nearest intersection.  <b>DO NOT include your full address.</b> </li>
                                        <li>If a scholarship is covering the costs, describe the paperwork necessary and who is paying the tutor.  For example, does the program pay the tutor or do you pay the tutor and get reimbursed by the program.  Also include tutoring cost limits if you know them.</li>
                                        <li>If this request is atypical (e.g. not for a course/class), please include details below.</li>
                                        <li><b>Urgent Requests:</b> If you must meet immediately (in less than 48 hours), please include details about what you are working on, the deadline, and how soon you'd like to meet.  </li>
                                    </div>
                                    {!! Form::textarea('concerns', null, ['class' => 'form-control', 'size' => '50x3', 'placeholder' => 'I prefer to meet in my house in Louisville. My scholarship program is paying for tutoring. I need to meet before Thursday!']) !!}
                                    <small class="text-danger">{{ $errors->first('concerns') }}</small>
                                </div>
                            </div>
                        </fieldset>
                </div> {{--panel-body--}}
            </div>{{--panel--}}
            <div class="panel panel-danger">
                <div class="panel-heading">SMS Alerts</div>
                <div class="panel-body">
                    <fieldset class="form-group col-md-12 col-sm-12">
                        <legend>Sign Up For Text Notifications</legend>
                        <p>Since e-mails can be mistakenly marked as Junk or Spam, we've introduced text alerts. Sign up and we'll send you a text the moment we have a tutor for this request.  </p>
                        <p><b>We will only text you about tutor matches, nothing else!</b></p>
                        <div class="form-group">
                            <div class="checkbox{{ $errors->has('sms_alert') ? ' has-error' : '' }}">
                                <label for="sms_alert">
                                    {!! Form::checkbox('sms_alert',1,1, ['id' => 'sms_alert']) !!} Text Me When A Match Is Found
                                </label>
                            </div>
                            <small class="text-danger">{{ $errors->first('sms_alert') }}</small>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">How To Pay Your Tutor</div>
                <div class="panel-body">
                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Paying For Sessions</legend>
                            <p>After you've been matched with a tutor, to pay for sessions, simply add a credit/debit card to your account and your card will be charged <b>after</b> each session.</p>
                            {{-- <ol>
                                <li>Add a credit/debit card to your account.</li>
                                <li>Your card will be charged <b>after</b> each session using our platform.</li>
                            </ol> --}}
                            <small>We use <a href="https://stripe.com/" target="_blank">Stripe</a> to securely process payments.  We do not store your CC data in our database.</small>
                            <br><br>
                            <b>Please do not pay tutors directly by cash, check, PayPal, Venmo or any other means.</b> This is for your own protection!  If you're not satisfied with a tutor's performance, we are happy to issue a refund, but we cannot do so if you pay the tutor directly. <br>
                            
                            
                        </fieldset>
                        @if (Auth::guest())
                            <div class="form-group">
                                <div class="col-md-12">
                                     <div class="checkbox{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">
                                        <label for="terms_of_use">
                                            {!! Form::checkbox('terms_of_use', '1', null, ['id' => 'terms_of_use', 'required']) !!} I agree to the <a href="{{route('terms')}}">Terms of Use</a> and I understand that <span style="color:darkred">I <b>cannot</b> pay my tutor directly for sessions</span>. 
                                        </label>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('terms_of_use') }}</small>
                                </div>   
                            </div>
                        @endif
                        {{-- Submit Buttons--}}
                        <div class="pull-right">
                            <div class="btn-group">
                                {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="btn-group">
                                {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                            </div>
                        </div>
                    
                    {!! Form::close() !!}                
                </div> {{--panel-body--}}
            </div> {{--panel--}}
        </div>
    </div>
</div>
