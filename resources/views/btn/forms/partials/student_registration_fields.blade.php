<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-sm-6">
    <label for="first_name">First Name</label>
    <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required>

    @if ($errors->has('first_name'))
        <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-sm-6">
    
    <label for="last_name" class="col-sm-offset-1">Last Name</label>
    <input id="last_name" type="text" class="form-control col-sm-offset-1" name="last_name" value="{{ old('last_name') }}" required>

    @if ($errors->has('last_name'))
        <span class="help-block">
            <strong>{{ $errors->first('last_name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-sm-6">
    <label for="email">E-Mail Address</label>
    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

    @if ($errors->has('email'))
        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif

</div>


<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-sm-6">
    <label for="phone" class="col-sm-offset-1">Phone Number</label>
    <input id="phone" type="phone" class="form-control col-sm-offset-1" name="phone" value="{{ old('phone') }}" required>

    @if ($errors->has('phone'))
        <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
    @endif
</div>


<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-sm-6">
    <label for="password">Password</label>
    <input id="password" type="password" class="form-control" name="password" required>

    @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
</div>

<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} col-sm-6">
    <label for="password-confirm" class="col-sm-offset-1">Confirm Password</label>
    <input id="password-confirm" type="password" class="form-control col-sm-offset-1" name="password_confirmation">

    @if ($errors->has('password_confirmation'))
        <span class="help-block">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
    @endif
</div>

<div class="col-sm-6 col-sm-offset-3">
    @include('btn.forms.partials.student_marketing_options')    
</div>
