<?php
	$student_marketing_options = [
			'flyers' => "Flyers",
			'chalkboard' => "Chalkboard In Class",
			'internet' => "Internet Search (e.g. Google)",
			'friend' => 'Recommended By A Friend/Classmate',
			'professor' => "Recommended Byf Professor/TA",
			'tutor-list' => "Tutor List",
			'facebook' => "Facebook",
			'craigslist' => "Craigslist"
			];
?>

<div class="form-group{{ $errors->has('marketing[]') ? ' has-error' : '' }}">
    {!! Form::label('marketing[]', 'How did you hear about us?') !!}
    {!! Form::select('marketing[]', $student_marketing_options, null, ['id' => 'marketing[]', 'class' => 'form-control', 'required' => 'required', 'multiple']) !!}
    <small class="text-danger">{{ $errors->first('marketing[]') }}</small>
</div>

