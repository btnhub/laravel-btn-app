<?php
	$rate_options = [
			'' => 'Select Max Affordable Rate',
			/*20 => '$20/hr',*/
			30 => '$30/hr',
			40 => '$40/hr',
			50 => '$50/hr',
			60 => '$60/hr',
			80 => '$80/hr',
			100 => '$100/hr',
			150 => '$150/hr',
			200 => '$200/hr',
			/*250 => '$250/hr',
			300 => '$300/hr',*/
		];
?>

<div class="form-group{{ $errors->has('max_rate') ? ' has-error' : '' }}">
    {!! Form::select('max_rate', $rate_options, null, ['id' => 'max_rate', 'class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('max_rate') }}</small>
</div>