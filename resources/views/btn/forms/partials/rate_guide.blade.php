<table class="table table-striped">
    <thead>
        <tr>
            <th>Tutor's Education Level</th>
            <th>Hourly Rate</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Undergraduate Student</td>
            <td>$20-35</td>
        </tr>
        <tr>
            <td>Graduate Student or Graduated with Bachelors</td>
            <td>$45-60</td>
        </tr>
        <tr>
            <td>Graduated with M.S. / M.A / PhD</td>
            <td>$60-80</td>
        </tr>
        <tr>
            <td>Full Time / Professional Tutor</td>
            <td>$100-150+</td>
        </tr>
    </tbody>
</table>
