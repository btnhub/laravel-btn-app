@extends('layouts.josh-front.default', ['page_title' => "First Session Evaluation"])

@section('title')
	Evaluation: First Session
@stop

@section('content')
 	<h3>First Session Evaluation</h3>
 	<p>Use this form to let us know how the first session went with {{$assignment->tutor->first_name}}. </p>
 	<b>Tutor</b>: {{$assignment->tutor->full_name}}<br>
 	<b>Course</b>: {{$assignment->course}}

	@unless (count($assignment->evaluation->where('type', 'First')))
		<div class="container">
		    <div class="row">
		        <div class="col-md-8 col-md-offset-2">
		            <div class="panel panel-info">
		                <div class="panel-heading">Evaluate First Session</div>
		                <div class="panel-body">
						 	{!! Form::open(['method' => 'POST', 'route' => ['submit.eval.first', 'assign_ref_id' => $assignment->ref_id], 'class' => 'form-horizontal']) !!} 	
						 	
						 	    <div class="radio{{ $errors->has('satisfied') ? ' has-error' : '' }}">
						 	        Did {{$assignment->tutor->first_name}} meet or exceed your expectations during the 1st session or first few sessions?*<br>
						 	        <label>
						 	            {!! Form::radio('satisfied', 1,  null) !!} Yes &nbsp;&nbsp;&nbsp;
						 	        </label>
						 	        <label>
						 	            {!! Form::radio('satisfied', 0,  null) !!} No
						 	        </label>
						 	        <small class="text-danger">{{ $errors->first('satisfied') }}</small>
						 	    </div>

						 	    <hr>
						 	    <b>Optional:</b>  Include constructive feedback to help your tutor improve his/her performance.
						 	    <div class="col-md-12 form-group{{ $errors->has('tutor_strength') ? ' has-error' : '' }}">
						 	        {!! Form::label('tutor_strength', 'What did your tutor do well?') !!}
						 	        {!! Form::textarea('tutor_strength', null, ['class' => 'form-control', 'size' => '40x2']) !!}
						 	        <small class="text-danger">{{ $errors->first('tutor_strength') }}</small>
						 	    </div>

						 	    <div class="col-md-12 form-group{{ $errors->has('tutor_weakness') ? ' has-error' : '' }}">
						 	        {!! Form::label('tutor_weakness', 'What should your tutor improve on?') !!}
						 	        {!! Form::textarea('tutor_weakness', null, ['class' => 'form-control', 'size' => '40x2']) !!}
						 	        <small class="text-danger">{{ $errors->first('tutor_weakness') }}</small>
						 	    </div>

						 	    <div class="btn-group pull-left">
						 	        {!! Form::submit("Submit Evaluation", ['class' => 'btn btn-primary']) !!}
						 	    </div>
						 	
						 	{!! Form::close() !!}
					 	</div>
				 	</div>
			 	</div>
		 	</div>
	 	</div>

	@else
 		You have already submitted an evaluation of {{$assignment->tutor->first_name}}.
 	@endunless
@stop