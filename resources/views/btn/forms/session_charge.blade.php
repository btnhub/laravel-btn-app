@extends('layouts.josh-front.default', ['page_title' => "Submit A Charge"])

@section('title')
	Charge Session
@stop

@section('content')
	
	<h3>Charge {{$assignment->student->full_name}} - {{$assignment->course}}</h3>
	
	<span style="color:red">{{$assignment->student->first_name}} is a returning student and has unused prepaid hours.</span>  You'll have to use up {{$assignment->student->first_name}}'s balance before you can charge {{$assignment->student->first_name}}'s CC and get paid directly through our Platform.

	<h4><b>Rate:</b> ${{$assignment->rate}} per {{number_format($assignment->billing_increment / 60, 2)}} hour</h4>
	<div class="container">
	    <div class="row">
	        <div class="col-md-6 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Charge {{$assignment->student->first_name}}'s Account Balance</div>
	                <div class="panel-body">

						{!! Form::open(['method' => 'POST', 'route' => ['session.charge', 'assign_id' => $assignment->ref_id], 'class' => 'form-horizontal']) !!}
							<div class="col-md-12 form-group{{ $errors->has('session_date') ? ' has-error' : '' }}">
							    {!! Form::label('session_date', 'Session Date') !!}
							    {!! Form::date('session_date', Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
							    <small class="text-danger">{{ $errors->first('session_date') }}</small>
							</div>
							<div class="col-md-12 form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
							    {!! Form::label('duration', 'Total Session Duration In MINUTES') !!}
							    {!! Form::text('duration',$assignment->billing_increment, ['class' => 'form-control', 'required' => 'required', 'onChange' => 'calcCharge()']) !!}
							    <small class="text-danger">{{ $errors->first('duration') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('outcome') ? ' has-error' : '' }}">
							    {!! Form::label('outcome', 'Session Outcome') !!}
							    {!! Form::select('outcome',[null=> 'Select An Outcome', 'Student & Tutor Met As Scheduled' => 'Student & Tutor Met As Scheduled', 'Student Cancelled Without 24 Hours Notice' => 'Student Cancelled Without 24 Hours Notice', 'Tutor Cancelled Without 24 Hours Notice' => 'Tutor Cancelled Without 24 Hours Notice', 'Free Session' => 'Free Session'], null, ['id' => 'outcome', 'class' => 'form-control', 'required' => 'required', 'onChange' => 'calcCharge()']) !!}
							    <small class="text-danger">{{ $errors->first('outcome') }}</small>
							</div>
						
							<div class="col-md-12 form-group{{ $errors->has('amt_charged') ? ' has-error' : '' }}">
							    {!! Form::label('amt_charged', 'Amount Charged') !!}
							    {!! Form::text('amt_charged', null, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
							    <small class="text-danger">{{ $errors->first('amt_charged') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
							    {!! Form::label('discount', 'First Session Discount') !!}
							    {!! Form::text('discount', null, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
							    <small class="text-danger">{{ $errors->first('discount') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('tutor_pay') ? ' has-error' : '' }}">
							    {!! Form::label('tutor_pay', 'Tutor Earnings') !!}
							    {!! Form::text('tutor_pay', null, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
							    <small class="text-danger">{{ $errors->first('tutor_pay') }}</small>
							</div>

							{!! Form::hidden('btn_pay', null, ['id' => 'btn_pay']) !!}
							
						    <div class="col-md-12 btn-group pull-right">
						        {!! Form::submit("Charge {$assignment->student->first_name}'s Account", ['class' => 'btn btn-primary']) !!}
						    </div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@push('scripts')
	<script type="text/javascript">
		function calcCharge()
		{
			var outcome = document.getElementById('outcome').value; 
			var charge_student = 1;

			if (outcome == 'Free Session' || outcome == 'Tutor Cancelled Without 24 Hours Notice') {
				charge_student = 0;
			}
	
			var discount_percent = {{$discount_percent}};
			var billing_increment = {{$assignment->billing_increment}}; 
			var duration = document.getElementById('duration').value; 
			var rate = {{$assignment->rate}};

			if (duration < billing_increment) {
				discount = charge_student * discount_percent * rate * (duration / billing_increment);

			}
			else	
			{
				discount = charge_student * discount_percent * rate;
			}

			var amt_charged = charge_student * rate * (duration / billing_increment) - discount;
			var tutor_pay = charge_student * rate * (duration / billing_increment) * (1 - {{$tutor_fee}});
			var btn_pay = charge_student * (amt_charged - tutor_pay);

			document.getElementById('discount').value = discount.toFixed(2); // return to 2 decimal places
			document.getElementById('amt_charged').value = amt_charged.toFixed(2); // return to 2 decimal places
			document.getElementById('tutor_pay').value = tutor_pay.toFixed(2); // return to 2 decimal places
			document.getElementById('btn_pay').value = btn_pay.toFixed(2); // return to 2 decimal places
		}
	</script>
@endpush