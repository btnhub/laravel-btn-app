@if (Auth::guest())
	Please first <a href="{{url('/login')}}">login</a> or <a href="{{url('/register')}}">register</a> to create an account.  
@else
	{!! Form::model($recent_request, ['method' => 'POST', 'route' => ['contact.tutor', 'ref_id' => $user->ref_id], 'class' => 'modal-body']) !!}

	<fieldset class="form-group">
	After you submit this form, you will be able to discuss your academic needs with {{$user->first_name}} using our messaging application (Inbox) without sharing your contact information.
	<hr>
			{{-- Location Select --}}
	    <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
		    <b>Location</b>  
			<div>Select where you can meet or the closest town/city to you:</div>
		        @foreach ($user->locations as $location)   
			        <label for="location_{{$location->id}}">
			            {!! Form::radio('location',$location->id,  in_array($location->id, Auth::user()->locations->pluck('id')->toArray()), ['id' => "location_$location->id", 'required']) !!} {{$location->city}} &nbsp;&nbsp;&nbsp;&nbsp;
			        </label>
		        @endforeach                
		        <small class="text-danger">{{ $errors->first('location') }}</small>
	    </div>
	</fieldset>

	<fieldset class="form-group">
	<legend>Course Details</legend>
	    	{{--Select Education Level--}}
    <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
        <b>Course Level.</b>  Please select the level of the course/class (not of the student):<br>
        <label class="radio-inline">
            {!! Form::radio('level', 'College',  null, ['required']) !!} College/University
        </label>
        <label class="radio-inline">
            {!! Form::radio('level', 'Graduate_School',  null) !!} Graduate School
        </label>
        <label class="radio-inline">
            {!! Form::radio('level', 'High_School',  null) !!} High School
        </label>
        <label class="radio-inline">
            {!! Form::radio('level', 'Middle_School',  null) !!} Middle School (Grades 6-8)
        </label>
        <label class="radio-inline">
            {!! Form::radio('level', 'Elementary_School',  null) !!} Elementary School (Grades K-5)
        </label>
        <label class="radio-inline">
            {!! Form::radio('level', 'Other',  null) !!} Other or Not Applicable
        </label>
        <small class="text-danger">{{ $errors->first('level') }}</small>
    </div>

	    {{-- Type In Course Name --}}
	    <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
	        <b>Course Name</b>
	       <div>Please include Course Name <b>AND</b> the Course Number. </div>
	        {!! Form::text('course', null, ['class' => 'form-control', 'placeholder' => 'Algebra Based Physics 1 (PHYS 2010), Private Piano Lessons, 10th Grade Algebra', 'required']) !!}
	        <small class="text-danger">{{ $errors->first('course') }}</small>
	    </div>
	</fieldset>

	<fieldset class="form-group">
		<legend>Tutor Session Details</legend>
	    {{-- Select Start Date --}}
	   	<div class="row">
	   		<div class="col-md-5 col-sm-5">
			    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
			        <b>Start Date</b>
			        <div>  Start date for tutor sessions. An estimation is sufficient.</div>
			        {!! Form::date('start_date',Carbon\Carbon::now()->addDays(2), ['class' => 'form-control', 'required']) !!}
			        <small class="text-muted">Default set to 2 days from today</small>
			        <small class="text-danger">{{ $errors->first('start_date') }}</small>
			    </div>
			    </div>
			    {{-- Select Start Date --}}
			    <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
				    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
				        <b>End Date</b>
				         <div>  When does the class/project end. An estimation is sufficient.</div>
				        {!! Form::date('end_date','2018-08-10', ['class' => 'form-control', 'required']) !!}
				        {{--<small class="text-muted">Default set to 3 months from today</small>--}}
				        <small class="text-danger">{{ $errors->first('end_date') }}</small>
				    </div>
			    </div>	
	   	</div>
	    {{-- Select Frequency Of Sessions --}}
	    <div class="form-group{{ $errors->has('frequency') ? ' has-error' : '' }}">
	        <b>Session Frequency</b>
	        {!! Form::text('frequency', null, ['class' => 'form-control', 'placeholder' => 'Once a week, Mondays and Thursdays, Only before exams', 'required']) !!}
	        <small class="text-danger">{{ $errors->first('frequency') }}</small>
	    </div>

	    {{-- Select Preferred Session Time --}}
	    <div class="form-group">
	        <div class="checkbox{{ $errors->has('session_time') ? ' has-error' : '' }}">
	        <b>Preferred Session Time (Required).</b>  The times refer to weekdays, i.e. Evenings means Weekdays 5-7pm.<br>
	            <label>
	                {!! Form::checkbox('session_time[]', 'Daytime', null) !!} Daytime (before 5pm)
	            </label>
	            <br>
	            <label>
	                {!! Form::checkbox('session_time[]', 'Evenings', null) !!} Evenings (5-7pm)
	            </label>
	            <br>
	            <label>
	                {!! Form::checkbox('session_time[]', 'Nights', null) !!} Nights (after 7pm)
	            </label>
	            <br>
	            <label>
	                {!! Form::checkbox('session_time[]', 'Saturdays', null) !!} Saturdays
	            </label>
	            <br>
	            <label>
	                {!! Form::checkbox('session_time[]', 'Sundays', null) !!} Sundays
	            </label>
	        </div>
	        <small class="text-danger">{{ $errors->first('session_time[]') }}</small>
	    </div>

	    {{-- Select Max Rate --}}
		<div class="radio{{ $errors->has('max_rate') ? ' has-error' : '' }}">
		    <b>Max Hourly Rate:</b> Tell {{$user->first_name}} what you can afford. <br>
		    Tutors sets their own rate, however, for a rate guide, please <a data-toggle="collapse" href="#collapseRate" aria-expanded="false" aria-controls="collapseRate">click here</a>.<br>

		    {{-- Rate Guide Table --}}
			<div class="collapse" id="collapseRate">
				<div class="card card-block">
					@include('btn.forms.partials.rate_guide')
				</div>
			</div>
		    
		    <label>
		        {!! Form::radio('max_rate',20,  null, ['required']) !!} $20 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',25,  null) !!} $25 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',30,  null) !!} $30 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',40,  null) !!} $40 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',50,  null) !!} $50 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',60,  null) !!} $60 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',80,  null) !!} $80 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',100,  null) !!} $100 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',125,  null) !!} $125 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',150,  null) !!} $150 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',175,  null) !!} $175 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <label>
		        {!! Form::radio('max_rate',200,  null) !!} $200 &nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
		    <small class="text-danger">{{ $errors->first('max_rate') }}</small>
		</div>
	</fieldset>

	<fieldset class="form-group">
	<legend>Additional Details</legend>
		<b><span style="color:darkred">Do NOT include your contact information (phone, email, physical address, etc) below.</span></b>
		{{-- Mark As Urgent --}}
		<div class="form-group">
		    <div class="checkbox{{ $errors->has('urgency') ? ' has-error' : '' }}">
		        <label for="urgency">
		            {!! Form::checkbox('urgency','High', null, ['id' => 'urgency']) !!} <b>Urgent Request!</b>  I must meet with a tutor in less than 48 hours.
		        </label>
		    </div>
		    <small class="text-danger">{{ $errors->first('urgency') }}</small>
		</div>

		{{-- Scholarship Program Name --}}
		<div class="form-group{{ $errors->has('scholarship_program') ? ' has-error' : '' }}">
		    <b>Scholarship Program</b>
			    <a data-toggle="collapse" href="#scholar-info" aria-expanded="false" aria-controls="scholar-info">More Info</a>

                <div class="collapse" id="scholar-info">
                    <div>If tutoring will be covered by your scholarship or other program, please include the name of the program below <b>and</b> then include details on how the required next steps in the "Other Requests Or Concerns" section below.</div>
                </div>
		    {!! Form::text('scholarship_program', null, ['class' => 'form-control', 'placeholder' => 'VA Tutorial Assistance, Daniels Fund']) !!}
		    <small class="text-danger">{{ $errors->first('scholarship_program') }}</small>
		</div>

		{{-- Additional Concerns --}}
		<div class="form-group{{ $errors->has('concerns') ? ' has-error' : '' }}">
		    <b>Other Requests or Concerns</b>
		    <a data-toggle="collapse" href="#concerns-examples" aria-expanded="false" aria-controls="concerns-examples">Examples</a>

            <div class="collapse" id="concerns-examples">
                <li>If you prefer to meet in your home, please include the town and neighborhood or the nearest intersection.  <b>DO NOT include your full address.</b> </li>
                <li>If a scholarship is covering the costs, describe the paperwork necessary and who is paying the tutor.  For example, does the program pay the tutor or do you pay the tutor and get reimbursed by the program.  Also include tutoring cost limits if you know them.</li>
                <li>If this request is atypical (e.g. not for a course/class), please include details below.</li>
                <li><b>Urgent Requests:</b> If you must meet immediately (in less than 48 hours), please include details about what you are working on, the deadline, and how soon you'd like to meet.  </li>
            </div>
		    
		    {!! Form::textarea('concerns', null, ['class' => 'form-control', 'placeholder' => 'I prefer to meet on the (school_name) campus / in downtown Louisville / at my house near 18th & Broadway in Denver. My scholarship will cover the cost and requires monthly reports', 'size' => '50x3']) !!}
		    <small class="text-danger">{{ $errors->first('concerns') }}</small>
		</div>
	</fieldset>
	<fieldset class="form-group col-md-12 col-sm-12">
	    <legend>How To Pay Your Tutor</legend>
	    Once you've been matched with a tutor, you must add a credit/debit card to your BuffTutor account.  Your selected tutor will then charge your card using our system <b>after</b> each session occurs.  This protects students as we can easily reverse and refund any disputed sessions/charges.<br><br>

	    Paying tutors directly (by cash, check, credit/debit card in person, PayPal, or other means) is a violation of our <a href="{{route('terms')}}">Terms of Use</a>.  <b>You must report any tutor who asks you to pay him/her directly. </b>
	</fieldset>
	{{--Agree To Terms--}}
	{{--
	<div class="form-group">
	    <div class="checkbox-inline">
	        <div class="checkbox{{ $errors->has('terms') ? ' has-error' : '' }}">
	            <label for="terms">
	                {!! Form::checkbox('terms', '1', null, ['id' => 'terms', 'required']) !!} I agree to the <a href="{{route('terms')}}">Terms of Use</a>.
	            </label>
	        </div>
	        <small class="text-danger">{{ $errors->first('terms') }}</small>
	    </div>
	</div>
	--}}

	{{-- Submit Buttons--}}
	<div class="pull-right">
	    <div class="btn-group">
	        {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
	    </div>
	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	    <div class="btn-group">
	        {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
	    </div>
	</div>


	{!! Form::close() !!}

@endif