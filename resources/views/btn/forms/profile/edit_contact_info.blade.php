{!! Form::model($user, ['method' => 'PATCH', 'route' => ['profile.edit_contact', 'user_ref_id' => $user->ref_id], 'class' => 'form-horizontal']) !!}

    <div class="row">
        <div class="col-md-5 col-sm-5">
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                {!! Form::label('first_name', 'First Name') !!}
                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('first_name') }}</small>
            </div>
            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                {!! Form::label('last_name', 'Last Name') !!}
                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('last_name') }}</small>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                {!! Form::label('phone', 'Phone') !!}
                {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('phone') }}</small>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password (Leave blank if you do not want to change your password)') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('password') }}</small>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                {!! Form::label('password_confirmation', 'Confirm Password') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-11 col-sm-11">
            <div class="btn-group pull-right">
               {!! Form::submit('Update Contact Info', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>  
    </div>
{!! Form::close() !!}
