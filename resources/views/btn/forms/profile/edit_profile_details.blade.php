<div class="container">
    {!! Form::model($profile, ['method' => 'PATCH', 'route' => ['profile.edit', 'user_ref_id' => $profile->user->ref_id], 'class' => 'form-horizontal']) !!}

            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#details">Details</a></li>
                <li><a data-toggle="tab" href="#availability">Availability</a></li>
                <li><a data-toggle="tab" href="#rate">Rate Details</a></li>
                <li><a data-toggle="tab" href="#experience">Experience</a></li>
                <li><a data-toggle="tab" href="#courses">Course List</a></li>
            </ul>
            <div class="tab-content">
                <div id="details" class="tab-pane fade in active">
                    {{--Company Locations--}}       
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="form-group{{ $errors->has('languages') ? ' has-error' : '' }}">
                                {!! Form::label('languages', 'Languages Spoken (languages you are fluent in and can teach in)') !!}
                                {!! Form::text('languages', "English", ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('languages') }}</small>
                            </div>
                            <div class="form-group{{ $errors->has('major') ? ' has-error' : '' }}">
                                {!! Form::label('major', 'Major(s) & Minor(s)') !!}
                                {!! Form::text('major', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('major') }}</small>
                            </div>
                            
                            <div class="form-group{{ $errors->has('tutor_education') ? ' has-error' : '' }}">
                                {!! Form::label('tutor_education', 'Current Education Level') !!}
                                {!! Form::select('tutor_education', $profile->education_levels(), null, ['id' => 'tutor_education', 'class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('tutor_education') }}</small>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                            <div class="form-group">
                                <div class="checkbox{{ $errors->has('locations') ? ' has-error' : '' }}">
                                    <p><b>Add yourself to the tutor lists for the following locations:</b></p>
                                    @foreach ($locations as $location)      
                                        <label>
                                            {!! Form::checkbox('locations[]',$location->id, in_array($location->id, $user->locations->pluck('id')->toArray())) !!} {{$location->city}}
                                        </label> &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endforeach
                                </div>
                                <small class="text-danger">{{ $errors->first('locations') }}</small>
                            </div>
                            <div class="form-group{{ $errors->has('session_locations') ? ' has-error' : '' }}">
                                <b>{!! Form::label('session_locations', 'Where you can meet for sessions.') !!}</b>
                                <br><small>E.g. Online, CU-Boulder Campus, Downtown Louisville, 10 mile radius around Fort Collins, Student's Home, Student's School Campus</small>
                                {!! Form::text('session_locations', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('session_locations') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="form-group{{ $errors->has('tutor_start_date') ? ' has-error' : '' }}">
                                {!! Form::label('tutor_start_date', 'Tutor Start Date') !!}
                                {!! Form::date('tutor_start_date', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('tutor_start_date') }}</small>
                            </div>

                            <div class="form-group{{ $errors->has('tutor_end_date') ? ' has-error' : '' }}">
                                {!! Form::label('tutor_end_date', 'Tutor End Date') !!}<small>Do not leave empty or else your profile will not show up on our list.</small>
                                {!! Form::date('tutor_end_date', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('tutor_end_date') }}</small>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                            
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                {!! Form::label('address', 'Full Mailing Address.') !!}  
                                {!! Form::textarea('address', null, ['class' => 'form-control', 'size' => '40x3']) !!}
                                <small class="text-danger">{{ $errors->first('address') }}</small>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div id="availability" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <div class="form-group{{ $errors->has('tutor_status') ? ' has-error' : '' }}">
                                {!! Form::label('tutor_status', 'Accepting Students') !!}<br>
                                 The second you want to stop receiving student referrals, change your status to “No Longer Accepting Students” immediately! 
                                {!! Form::select('tutor_status',[1 => 'Accepting Students', 0 => 'Not Accepting Students'], null, ['id' => 'tutor_status', 'class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('tutor_status') }}</small>
                            </div>

                            <div class="form-group{{ $errors->has('calendar_id') ? ' has-error' : '' }}">
                                {!! Form::label('calendar_id', 'Google Calendar ID') !!}
                                <br>
                                Create a Google Calendar, <b>make it Public</b>, and include the Calendar ID below. <br> 
                                <a href="https://support.google.com/calendar/answer/37083" target="_blank">How To Create A Public Calendar</a> and
                                <a href="https://support.appmachine.com/hc/en-us/articles/203645966-Find-your-Google-Calendar-ID-for-the-Events-block" target="_blank">Finding Your Google Calendar ID</a>
                                {!! Form::text('calendar_id', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('calendar_id') }}</small>
                            </div>
                        </div>
                    
                        <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                            <div class="form-group">
                                <div class="checkbox{{ $errors->has('tutor_availability') ? ' has-error' : '' }}">
                                <b>When you can meet for sessions</b><br>
                                    @foreach ($profile->tutor_availability_options() as $key => $value)
                                        <label>
                                            {!! Form::checkbox('tutor_availability[]', $key, in_array($key, $tutor_availability), []) !!} {{$value}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                    @endforeach
                                </div>
                                <small class="text-danger">{{ $errors->first('tutor_availability') }}</small>
                            </div>

                            <div class="form-group{{ $errors->has('hours_available') ? ' has-error' : '' }}">
                                {!! Form::label('hours_available', 'Hours Available This Semester') !!}
                                {!! Form::text('hours_available', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('hours_available') }}</small>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="rate" class="tab-pane fade">
                      {{--Rate Details--}}
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <br>
                            <p><b style="color:darkred">Please enter your desired take home earnings (your net income after our service fee has been deducted).</b></p>
                            <div class="form-group{{ $errors->has('rate_min') ? ' has-error' : '' }}">
                                {!! Form::label('rate_min', 'Min Rate ($/hr)') !!}
                                {!! Form::text('rate_min', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('rate_min') }}</small>
                            </div>

                            <div class="form-group{{ $errors->has('rate_max') ? ' has-error' : '' }}">
                                {!! Form::label('rate_max', 'Max Rate ($/hr)') !!}
                                {!! Form::text('rate_max', null, ['class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('rate_max') }}</small>
                            </div>
                            
                            {{-- <div class="form-group{{ $errors->has('rate_visibility') ? ' has-error' : '' }}">
                                {!! Form::label('rate_visibility', 'Rate Visibility On Profile') !!}
                                {!! Form::select('rate_visibility',[1 => 'Show Rate On Profile', 0 => 'Do Not Show Rate On Profile'], null, ['id' => 'rate_visibility', 'class' => 'form-control']) !!}
                                <small class="text-danger">{{ $errors->first('rate_visibility') }}</small>
                            </div> --}}
                        </div>

                        <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                            <div class="form-group{{ $errors->has('rate_details') ? ' has-error' : '' }}">
                                {!! Form::label('rate_details', 'Rate Details (Optional)') !!}<br>
                                <li>Provide some information on how you vary your rate. </li>
                                <li>If you can tutor more than one student in a session, explain how your rate will vary.</li>
                                <b>Examples </b><br>
                                1) My range is $20-$30.  $20/hr if the student wants to meet multiple times in a week, $30/hr for the following upper division courses: ...
                                <br>
                                2) My group rate is $20/hr per student for a group of 2 and $15/hr per student for groups of  3 or more."
                                <br>
                                3) My rate for online tutoring is $15-20/hr.
                                <br>

                                {!! Form::textarea('rate_details', null, ['class' => 'form-control', 'size'=> '40x4']) !!}
                                <small class="text-danger">{{ $errors->first('rate_details') }}</small>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="experience" class="tab-pane fade">
                    <div class="col-md-11 col-sm-11 form-group{{ $errors->has('tutor_experience') ? ' has-error' : '' }}">
                        {!! Form::label('tutor_experience', 'Experience') !!}<br>
                        <p>Briefly describe your teaching/tutoring experience and academic accomplishments.  Use this section to demostrate your proficiency in the areas you wish to tutor.  Write this in 3rd person, avoid "I", "you", etc.  Be sure to include any experience as a test prep (e.g. SAT) instructor or tutor.  </p>

                        <p>Keep this section professional and relevant.  <b>Do NOT include your full name, contact info or external links of any kind</b>.  We will shut down any account that includes this information in the profile.  </p>

                        <p>If you offer online tutoring, briefly describe how you teach online and describe the platform or tools (e.g. Skype) that you use.</p>

                        {!! Form::textarea('tutor_experience', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('tutor_experience') }}</small>
                    </div>
                </div>
                
                <div id="courses" class="tab-pane fade">
                    <div class="col-md-11 col-sm-11 form-group{{ $errors->has('courses') ? ' has-error' : '' }}">
                        {!! Form::label('courses', 'Course List') !!}<br>

                         <li>Make sure that you list only the courses you have passed our online proficiency test in.  If a course was not included on our exams, include courses that you are proficient in and can teach, not just courses you did well in.</li>  
                         <span style="color:blue">For College Courses</span>
                         <li>There is a Course Catalog under the Tutors menu</li>
                         <li> Include both the course number and course name.  </li>
                         <li>List the course number as the 2-4 letter department and 3-4 digit course number (do not include section numbers).  Add a space between the department letters and course number.  <b>Example: CHEM 1133 - General Chemistry 2</b> </li>
                        <li>OPTIONAL: You may also include commonly used names of the courses, e.g. MATH 161  Calculus for Physical Scientists II (Calc 2)</li>
                        <br>

                        <span style="color:blue">For High/Middle/Elementary School Classes</span>
                        <li>Include the class level and a description of the class. For example:  High School Physics, Chemistry, Algebra, Middle School Writing, Elementary School Reading</li>
                        <li>Do not simply state High School Math, instead: High School Algebra, Geometry, Precalculus.  </li>
                        <br>

                        <span style="color:blue">For Other Tutoring Areas</span>
                        <li>If you are proficient in a software package or programming language and can teach it, be sure to include that.  For example "PHP, C++, MatLab, Mathematica, R"</li>
                        <li>Include standardized tests, e.g. SAT Writing, GRE Chemistry, if you are familiar and proficient in both the content and test taking  procedures / strategies.</li>
                        <li>Feel free to add General Tutoring/Mentoring if you wish to do that.  We have received requests for tutors who can help with mentoring, organization, study skills.</li>
                        <br>
                        {!! Form::textarea('courses', null, ['class' => 'form-control']) !!}
                        <small class="text-danger">{{ $errors->first('courses') }}</small>
                    </div>
                </div>

                <div id="section5" class="tab-pane fade">

                </div>
            </div>
    <div class="row">
        <div class="col-md-11 col-sm-11">
            <div class="btn-group pull-right">
               {!! Form::submit('Update Profile', ['class' => 'btn btn-success']) !!}
            </div>
        </div>
    </div>

    {!! Form::close() !!}
</div>