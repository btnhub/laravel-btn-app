{!! Form::model($assignment, ['method' => 'PATCH', 'route' => ['date.update', $assignment->id], 'class' => 'form-horizontal']) !!}


	<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
	    <div class="col-md-9 col-sm-9">
	    {!! Form::date('end_date', null, ['class' => 'form-control', 'min' => Carbon\Carbon::now()->format('Y-m-d')]) !!}
	    <small class="text-danger">{{ $errors->first('end_date') }}</small>
	    </div>
	</div>
    <div class="btn-group pull-left">      
        {!! Form::submit('Update', ['class' => 'btn btn-primary btn-xs form-control']) !!}
    </div>
{!! Form::close() !!}		
