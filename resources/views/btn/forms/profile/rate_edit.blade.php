{!! Form::model($assignment, ['method' => 'PATCH', 'route' => ['rate.update', 'assign_ref_id' => $assignment->ref_id], 'class' => 'form-horizontal', 'id'=> 'rate_edit_'.$assignment->ref_id]) !!}

<div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
    {!! Form::label('rate', 'Rate ($)') !!}
    {!! Form::number('rate', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('rate') }}</small>
</div>

<div class="form-group{{ $errors->has('billing_increment') ? ' has-error' : '' }}">
    {!! Form::label('billing_increment', 'Billing Increment (in minutes)') !!}
    {!! Form::number('billing_increment', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('billing_increment') }}</small>
</div>

    <div class="btn-group pull-right">
        {!! Form::submit("Update", ['class' => 'btn btn-xs btn-success']) !!}
    </div>
{!! Form::close() !!}