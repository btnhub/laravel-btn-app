{!! Form::open(['method' => 'POST', 'route' => ['rate.create', 'assign_ref_id' => $assignment->ref_id], 'class' => 'form-horizontal', 'id'=> 'rate_create_'.$assignment->ref_id]) !!}

<div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }}">
    {!! Form::label('rate', 'Rate ($)') !!}
    {!! Form::number('rate', null, ['class' => 'form-control', 'required' => 'required']) !!}
    <small class="text-danger">{{ $errors->first('rate') }}</small>
</div>

<div class="form-group{{ $errors->has('billing_increment') ? ' has-error' : '' }}">
    {!! Form::label('billing_increment', 'Billing Increment (in minutes)') !!}
    {!! Form::number('billing_increment', 60, ['class' => 'form-control', 'required' => 'required', 'size' => '10']) !!}
    <small class="text-danger">{{ $errors->first('billing_increment') }}</small>
</div>

    <div class="btn-group pull-right">
        {!! Form::submit("Submit", ['class' => 'btn btn-xs btn-success']) !!}
    </div>
{!! Form::close() !!}