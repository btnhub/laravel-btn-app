<div class="row col-md-offset-1">
{!! Form::open(['method' => 'POST', 'route' => 'search.students', 'class' => 'form-horizontal', 'id' => 'search_students']) !!}

    <div class="col-md-3 form-group{{ $errors->has('search') ? ' has-error' : '' }}">
        {!! Form::label('search', 'Name of Previous Student') !!}
        {!! Form::text('search', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('search') }}</small>
    </div>

{!! Form::close() !!}

</div>