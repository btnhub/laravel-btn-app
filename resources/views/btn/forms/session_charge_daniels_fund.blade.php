@extends('layouts.josh-front.default', ['page_title' => "Charge Scholarship Student"])

@section('title')
	Charge Scholarship Student
@stop

@section('content')
	
	<h3>Charge {{$df_assignment->student->full_name}} - {{$df_assignment->course}}</h3>

	<small><span style="color:red">You cannot use this form to charge a missed session.  Please contact us if a student cancelled without 24 hours notice or did not show up.</span>***</small>
	<br><br>
	<div class="container">
	    <div class="row">
	        <div class="col-md-6 col-md-offset-2">
	            <div class="panel panel-default">
	                <div class="panel-heading">Charge {{$df_assignment->student->first_name}}'s Scholarship Program</div>
	                <div class="panel-body">
						{!! Form::open(['method' => 'POST', 'route' => ['charge.daniels.scholar', 'df_id' => $df_assignment->id], 'class' => 'form-horizontal']) !!}
							<div class="col-md-12 form-group{{ $errors->has('session_date') ? ' has-error' : '' }}">
							    {!! Form::label('session_date', 'Session Date') !!}
							    {!! Form::date('session_date', Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
							    <small class="text-danger">{{ $errors->first('session_date') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
							    {!! Form::label('duration', 'Session Duration') !!}
							    {!! Form::select('duration',$session_duration_array, null, ['id' => 'duration', 'class' => 'form-control', 'required' => 'required', 'onChange' => 'calcCharge()']) !!}
							    <small class="text-danger">{{ $errors->first('duration') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('outcome') ? ' has-error' : '' }}">
							    {!! Form::label('outcome', 'Session Outcome') !!}
							    {!! Form::select('outcome',[null=> 'Select An Outcome', 'Student & Tutor Met As Scheduled' => 'Student & Tutor Met As Scheduled', 'Tutor Cancelled Without 24 Hours Notice' => 'Tutor Cancelled Without 24 Hours Notice', 'Free Session' => 'Free Session'], null, ['id' => 'outcome', 'class' => 'form-control', 'required' => 'required', 'onChange' => 'calcCharge()']) !!}
							    <small class="text-danger">{{ $errors->first('outcome') }}</small>
							</div>

							<div class="col-md-12 form-group{{ $errors->has('tutor_pay') ? ' has-error' : '' }}">
							    {!! Form::label('tutor_pay', 'Tutor Earnings') !!}
							    {!! Form::text('tutor_pay', null, ['class' => 'form-control', 'required' => 'required', 'readonly']) !!}
							    <small class="text-danger">{{ $errors->first('tutor_pay') }}</small>
							</div>

							<div class="btn-group pull-right">
						        {!! Form::submit("Charge {$df_assignment->student->first_name}", ['class' => 'btn btn-primary']) !!}
						    </div>

						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@push('scripts')
	<script type="text/javascript">
		function calcCharge()
		{
			var outcome = document.getElementById('outcome').value; 
			var charge_student = 1;

			if (outcome == 'Free Session' || outcome == 'Tutor Cancelled Without 24 Hours Notice') {
				charge_student = 0;
			}
	
			var duration = document.getElementById('duration').value; 
			var billing_increment = 60;
			var tutor_rate = {{$tutor_rate}};
			var tutor_pay = charge_student * tutor_rate * (duration / billing_increment);
			document.getElementById('tutor_pay').value = tutor_pay.toFixed(2); // return to 2 decimal places
		}
	</script>
@endpush