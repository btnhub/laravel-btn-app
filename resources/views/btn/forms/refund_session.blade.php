@extends('layouts.josh-front.default', ['page_title' => "Refund Session"])

@section('title')
	Refund Session
@stop

@section('content')
	<h3>Request A Refund</h3>
	<div class="col-md-6">
	<table class="table table-striped">
		<tbody>
			<tr>
				<td><b>Session Ref ID:</b></td>
				<td>{{$tutor_session->ref_id}}</td>
			</tr>
			<tr>
				<td><b>Session Date:</b></td>
				<td>{{date('m/d/Y', strtotime($tutor_session->session_date))}}</td>
			</tr>
			<tr>
				<td><b>Tutor:</b></td>
				<td>{{$tutor_session->tutor->first_name}} {{$tutor_session->tutor->last_name}}</td>
			</tr>
			<tr>
				<td><b>Amount Charged:</b></td>
				<td>${{$tutor_session->amt_charged}}</td>
			</tr>
			@if ($tutor_session->isSoleSession($tutor_session->assignment_id))
			
				<tr>
					<td><b>Refund Amount:</b></td>
					<td><b>${{$requested_amount}}</b></td>
				</tr>
			@else
				<tr>
					<td><b>Refundable Amount:</b></td>
					<td>${{$requested_amount}}</td>
				</tr>
				<tr>
					<td><b>Processing Fee:</b></td>
					<td>${{$deductions}}</td>
				</tr>
				<tr>
					<td><b>Refund Amount:</b></td>
					<td><b>${{number_format($requested_amount - $deductions, 2)}}</b></td>
				</tr>
				<tr><td colspan="2">Since you have met with {{$tutor_session->tutor->first_name}} more than once, the full amount charged may not be refundable.  Please review our <a href="{{route('refund.policy')}}">Refund Policy</a> for details.</td></tr>
			@endif
			
		</tbody>
	</table>
	</div>	

	@unless($tutor_session->refundRequestSubmitted($tutor_session->assignment_id))
		
		<div class="col-md-6"> 
		<p>After you submit this request, please evaluate {{$tutor_session->tutor->first_name}}'s performance.</p>
		{!! Form::open(['method' => 'POST', 'route' => ['refund.post', 'session_ref_id' => $tutor_session->ref_id], 'class' => 'form-horizontal']) !!}
			
			{{--
			<div class="radio{{ $errors->has('refund_destination') ? ' has-error' : '' }}">
			    <div>	
				    <label>
				        {!! Form::radio('refund_destination','account',  null, ['required' => 'required', 'id' => 'refund_to_account', 'onclick' => 'refundMethods()']) !!} Refund To My Account <br>
				        <small class="text-muted">To be applied to future sessions</small><br>
				    </label>
			    </div>
			    <div>
				    <label>
				        {!! Form::radio('refund_destination','student',  null, ['id' => 'refund_to_student', 'onclick' => 'refundMethods()']) !!} Refund To Self
				    </label>
			    </div>
			    <small class="text-danger">{{ $errors->first('refund_destination') }}</small>
			</div>

			<br>


			<div id="refund_details" style="display:none">
				
				<div class="radio{{ $errors->has('refund_method') ? ' has-error' : '' }}">
				<p><b>Select How You Would Like To Be Refunded</b></p>
				<p>Credit / Debit card refunds can only be issued if you paid using that method. Payments completed by PayPal or Square Cash cannot be refunded directly to your card.</p>
				    @foreach ($refund_methods as $refund_method)
					    <label>
					        {!! Form::radio('refund_method', $refund_method->method,  null, ['id' =>"$refund_method->method" , 'required' => 'required', 'onclick' => "refundDetails($refund_method->email_required)"]) !!} {{$refund_method->title}}
					    </label>
					    &nbsp;&nbsp;&nbsp;&nbsp;
				    @endforeach
				    <small class="text-danger">{{ $errors->first('refund_method') }}</small>
				</div>
				<br>
				<div id="email" style="display: none">
					<div class="form-group{{ $errors->has('method_details') ? ' has-error' : '' }}">
					    {!! Form::label('method_details', 'Email Address Associated With The Selected Method') !!}
					    {!! Form::text('method_details', Auth::user()->email, ['class' => 'form-control']) !!}
					    <small class="text-danger">{{ $errors->first('method_details') }}</small>
					</div>
				</div>

				<div id="address" style="display: none">
					<div class="form-group{{ $errors->has('check_address') ? ' has-error' : '' }}">
					    {!! Form::label('check_address', 'Address To Send Check') !!}. The check will be written and sent to {{Auth::user()->first_name}}, however if it must be written and sent to {{Auth::user()->first_name}}'s parent/guardian, include the parent/guardian's name below. 
					    {!! Form::textarea('check_address', null, ['class' => 'form-control', 'size' => '40x3']) !!}
					    <small class="text-danger">{{ $errors->first('check_address') }}</small>
					</div>
				</div>
			</div>
			--}}

			{!! Form::hidden('deductions', $deductions) !!}
		    {!! Form::hidden('requested_amount', $requested_amount) !!}

		    <div class="btn-group pull-left">
		        {!! Form::submit("Request Refund", ['class' => 'btn btn-primary']) !!}
		    </div>
		
		{!! Form::close() !!}
	</div>
	@else
		<button type="button" class="btn btn-primary" disabled>Refund Has Already Been Requested</button>
	@endunless
@stop

{{--
@push('scripts')
<script type="text/javascript">
	function refundMethods() {
	    if (document.getElementById('refund_to_student').checked) {
	        document.getElementById('refund_details').style.display = 'block';
	    } 
	    else {
	        document.getElementById('refund_details').style.display = 'none';
	    }
	}

	function refundDetails(show_email)
	{		
		if (show_email && document.getElementById('Check').checked === false) {
	        document.getElementById('email').style.display = 'block';
	        document.getElementById('address').style.display = 'none';
	    } 

	    else if (document.getElementById('Check').checked)
	    {
	    	document.getElementById('email').style.display = 'none';
	        document.getElementById('address').style.display = 'block';
	    }

	    else {
	        document.getElementById('email').style.display = 'none';
	        document.getElementById('address').style.display = 'none';
	    }
	}
</script>
@endpush
--}}