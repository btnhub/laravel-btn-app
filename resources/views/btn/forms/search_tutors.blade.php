{!! Form::open(['method' => 'GET', 'route' => 'search.tutors', 'class' => 'form-horizontal']) !!}
    <div class="row form-group">
        <div class="col-sm-2 control-label">
           <b>City</b>
        </div>
        <div class="col-md-6 col-sm-10">
            @foreach ($locations as $location)   
                <label>
                   <input name="location[]" type="checkbox" value="{{$location->id}}" @if ($request->location ?? '') {{ in_array($location->id, $request->location) ? "checked" : ''}} @elseif (!Auth::guest() && in_array($location->id, Auth::user()->locations->pluck('id')->toArray())) checked @endif>{{$location->city}}{{$location->state ? ", $location->state": null}}                   
               </label> &nbsp;&nbsp;&nbsp;&nbsp;                   
            @endforeach                
        </div>
    </div>

    <div class="row">
        <label for="course" class="col-sm-2 control-label"><b>Course</b></label>
        <div class="col-sm-4 col-md-4">
            <input type="text" class="form-control" name="course" @if ($request ?? '') value="{{$request->course}}" @endif >    
            <small>Replace a space with an underscore "_" (APPM_1350 or Physics_2).</small>
        </div>


    </div>

    <div class="row">
        {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-4 col-md-4">
            {!! Form::select('status',['' => 'No Preference', '1' => 'Accepting Students', '0' => 'Not Accepting Students'], ($request ?? '' ? $request->status : ''), ['id' => 'status', 'class' => 'form-control']) !!}
        </div>
    </div>   

    @if (Auth::user() && Auth::user()->isStaff())
        <div class="row">
            {!! Form::label('min_rate', 'Min Rate ($/hr)', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-2">
                <input type="number" class="form-control" name="min_rate" @if ($request ?? '') value="{{$request->min_rate}}" @endif >
            </div>
        </div>

        <div class="row">
            {!! Form::label('max_rate', 'Max Rate ($/hr)', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-2">
                <input type="number" class="form-control" name="max_rate" @if ($request ?? '') value="{{$request->max_rate}}" @endif >
            </div>
        </div>
        
        {{-- <div class="row">
            <div class="form-group">
                <div class="col-sm-2 control-label">
                       <b>Tutor Education</b>
                </div>
                
                <div class="col-sm-7">
                    @foreach ($education_levels as $key => $value)
                        <label>
                            <input name="tutor_education[]" type="checkbox" value="{{$key}}" @if ($request->tutor_education ?? '') {{ in_array($key, $request->tutor_education) ? "checked" : ''}} @endif>{{$value}} &nbsp;&nbsp; 
                        </label>

                    @endforeach
                </div>

            </div>
        </div>
         --}}
    @endif
    
    
    <div class="row">
        <div class="form-group">
            <div class="col-sm-2 control-label">
               <b>Availability</b>
            </div>
            <div class="col-sm-9">
                @foreach ($tutor_availability_options as $key => $value)
                    <label>
                        <input name="tutor_availability[]" type="checkbox" value="{{$key}}" @if ($request->tutor_availability ?? '') {{ in_array($key, $request->tutor_availability) ? "checked" : ''}} @endif>{{$value}} &nbsp;&nbsp; 
                    </label>
                @endforeach
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="btn-group pull-middle">
            {!! Form::submit("Search Tutors", ['class' => 'btn btn-primary']) !!}
        </div>
        {{--<div class="btn-group pull-right" style="display:inline" >
                            <a href="{{route('list.tutors')}}"><button class="btn btn-warning">List All Tutors</button></a>
                        </div>--}}
    </div>   
    
{!! Form::close() !!}
