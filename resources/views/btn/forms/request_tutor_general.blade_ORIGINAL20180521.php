<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Request A Tutor</div>
                <div class="panel-body">                        
                    {!! Form::model($recent_request, ['method' => 'POST', 'route' => 'request.post', 'class' => 'form-horizontal']) !!}
                    
                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Student Info</legend>
                            <h4><b>Name: {{Auth::user()->first_name}} {{Auth::user()->last_name}}</b></h4> 
                            If you are the Parent/Guardian of the student, and your name is displayed above, <a href="#add_student" data-toggle="collapse">click here</a> to include the student's name.
                            <div id="add_student" class="collapse">                    
                                <div class="form-group{{ $errors->has('child_first_name') ? ' has-error' : '' }}">
                                    <div class="col-md-4 col-sm-4">
                                        <b>Student's Name</b>
                                        {!! Form::text('child_name', null, ['class' => 'form-control']) !!}
                                        <small class="text-danger">{{ $errors->first('child_first_name') }}</small>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                    <b>Location</b>  
                                    <div>Select where you can meet or the closest town/city to you:</div>
                                    @foreach ($locations as $location)   
                                        <label>
                                            {!! Form::radio('location',$location->id,  in_array($location->id, Auth::user()->locations->pluck('id')->toArray())) !!} {{$location->city}} &nbsp;&nbsp;&nbsp;&nbsp;
                                        </label>
                                    @endforeach                
                                    <small class="text-danger">{{ $errors->first('location') }}</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Course Details</legend>
                            <div class="col-md-12 col-sm-12">
                                {{--Select Education Level--}}
                                <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
                                    <b>Course Level.</b>  Please select the level of the course/class:<br>
                                    <label class="radio-inline">
                                        {!! Form::radio('level', 'College',  'checked') !!} College/University
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('level', 'Graduate_School',  null) !!} Graduate School
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('level', 'High_School',  null) !!} High School
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('level', 'Middle_School',  null) !!} Middle School (Grades 6-8)
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('level', 'Elementary_School',  null) !!} Elementary School (Grades K-5)
                                    </label>
                                    <label class="radio-inline">
                                        {!! Form::radio('level', 'Other',  null) !!} Other or Not Applicable
                                    </label>
                                    <small class="text-danger">{{ $errors->first('level') }}</small>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12">
                                {{--Select Subject Area--}}
                                <p><b>Select the subject(s) related to this course.</b> Tutors for the selected subject(s) will be immediately notified of your request.</p> 
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'Subjects') !!}
                                        {!! Form::select('subjects[]',$subjects, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'Math') !!}
                                        {!! Form::select('subjects[]',$subjects_math, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'Engineering') !!}
                                        {!! Form::select('subjects[]',$subjects_engineering, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'Humanities') !!}
                                        {!! Form::select('subjects[]',$subjects_humanities, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'Languages') !!}
                                        {!! Form::select('subjects[]',$subjects_languages, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'Music') !!}
                                        {!! Form::select('subjects[]',$subjects_music, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-group{{ $errors->has('subjects') ? ' has-error' : '' }}">
                                        {!! Form::label('subjects[]', 'General') !!}
                                        {!! Form::select('subjects[]',$subjects_general, null, ['class' => 'form-control', 'multiple']) !!}
                                    </div>
                                </div>

                                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                                    <label>
                                        {!! Form::checkbox('subjects[]', "Other", null) !!} Other or Not Applicable
                                    </label>
                                    <br><br>
                                    <small class="text-danger">{{ $errors->first('subjects') }}</small>
                                </div>
                                
                            </div>


                            {{-- Type In Course Name --}}
                            <div class="col-md-12 form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                                <b>Course Name</b>
                               
                               <div>Please include Course Name <b>AND</b> the Course Number. </div>
                                {!! Form::text('course', null, ['class' => 'form-control', 'placeholder' => 'Calc 1 - MATH 1300, Algebra Based Physics 1 (PHYS 2010), Private Piano Lessons']) !!}
                                <small>Please list only ONE course.  We will prefill this form if you need a tutor for another course.</small>
                                <small class="text-danger">{{ $errors->first('course') }}</small>
                            </div>
                        </fieldset>

                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Tutor Session Details</legend>
                            <div class="col-md-5 col-sm-5">
                                {{-- Select Start Date --}}
                                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                                    <b>Start Date</b>
                                    <div>  When you would like to begin sessions. An estimation is sufficient.</div>
                                    <div class="col-md-8 col-sm-8">
                                        {!! Form::date('start_date',Carbon\Carbon::now()->addDays(2), ['class' => 'form-control']) !!}
                                        
                                        <small class="text-muted">Default set to 2 days from today</small>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('start_date') }}</small>
                                </div>
                            </div>

                            <div class="col-md-5 col-sm-5 col-md-offset-1">
                                {{-- Select End Date --}}
                                <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                                    <b>End Date</b>
                                     <div>  When does the course/class/project ends. An estimation is sufficient.</div>
                                    <div class="col-md-8 col-sm-8">
                                    {!! Form::date('end_date','2018-08-10', ['class' => 'form-control']) !!}
                                    {{--<small class="text-muted">Default set to 3 months from today</small>--}}
                                    <small class="text-danger">{{ $errors->first('end_date') }}</small>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12">
                                <div class="col-md-5 col-sm-5">
                                    {{-- Select Preferred Session Time --}}
                                    <div class="form-group">
                                        <div class="checkbox{{ $errors->has('session_time') ? ' has-error' : '' }}">
                                        <b>Preferred Session Time.</b>  <br>
                                        Times refer to weekdays, i.e. Evenings means Weekdays 5-7pm.<br>
                                            <label>
                                                {!! Form::checkbox('session_time[]', 'Daytime', null) !!} Daytime (before 5pm)
                                            </label>
                                            <br>
                                            <label>
                                                {!! Form::checkbox('session_time[]', 'Evenings', null) !!} Evenings (5-7pm)
                                            </label>
                                            <br>
                                            <label>
                                                {!! Form::checkbox('session_time[]', 'Nights', null) !!} Nights (after 7pm)
                                            </label>
                                            <br>
                                            <label>
                                                {!! Form::checkbox('session_time[]', 'Saturdays', null) !!} Saturdays
                                            </label>
                                            <br>
                                            <label>
                                                {!! Form::checkbox('session_time[]', 'Sundays', null) !!} Sundays
                                            </label>
                                        </div>
                                        <small class="text-danger">{{ $errors->first('session_time') }}</small>
                                    </div>
                                </div>

                                <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1">
                                    {{-- Select Frequency Of Sessions --}}
                                    <div class="form-group{{ $errors->has('frequency') ? ' has-error' : '' }}">
                                        <b>Session Frequency</b>
                                        <div> How often would you like to meet? </div>
                                        {!! Form::text('frequency', null, ['class' => 'form-control', 'placeholder' => 'Once a week, Mondays & Thursdays, Only before exams']) !!}
                                        <small class="text-danger">{{ $errors->first('frequency') }}</small>
                                    </div>
                                </div>
                            </div>
                            {{-- Select Max Rate --}}
                            <div class="col-md-12 col-sm-12">
                                <div class="radio{{ $errors->has('max_rate') ? ' has-error' : '' }}">
                                    <b>Max Hourly Rate:</b> To make sure you get matched with a tutor in your price range, tell us what you can afford. <br>
                                    Tutors sets their own rate, however, for a rate guide, please <a data-toggle="collapse" href="#collapseRate" aria-expanded="false" aria-controls="collapseRate">click here</a>.<br>

                                    {{-- Rate Guide Table --}}
                                    <div class="collapse" id="collapseRate">
                                        <div class="row">
                                            <div class="col-md-8 col-sm-8">
                                                <div class="card card-block">
                                                    @include('btn.forms.partials.rate_guide')
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <label>
                                        {!! Form::radio('max_rate',20,  null) !!} $20 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',25,  null) !!} $25 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',30,  null) !!} $30 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',40,  null) !!} $40 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',50,  null) !!} $50 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',60,  null) !!} $60 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',80,  null) !!} $80 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',100,  null) !!} $100 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',125,  null) !!} $125 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',150,  null) !!} $150 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',175,  null) !!} $175 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <label>
                                        {!! Form::radio('max_rate',200,  null) !!} $200 &nbsp;&nbsp;&nbsp;&nbsp;
                                    </label>
                                    <small class="text-danger">{{ $errors->first('max_rate') }}</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>Additional Details <small>(optional)</small></legend>
                            <b><span style="color:darkred">Do NOT include your contact information (phone, email, physical address, etc) below.</span></b>
                            <div class="col-md-5 col-sm-5">
                                {{-- Mark As Urgent --}}
                                <div class="form-group">
                                    <div class="checkbox{{ $errors->has('urgency') ? ' has-error' : '' }}">
                                        <b>Urgent Requests</b> 
                                        <a data-toggle="collapse" href="#urgent-info" aria-expanded="false" aria-controls="urgent-info">More Info</a>

                                        <div class="collapse" id="urgent-info">
                                            <div>If you must meet with a tutor in less than 48 hours (e.g. if an assignment deadline or an exam is tomorrow), mark this request as urgent.</div>
                                        </div>
                                        <label for="urgency">
                                            {!! Form::checkbox('urgency','High', null, ['id' => 'urgency']) !!} I must meet with a tutor in less than 48 hours.
                                        </label>
                                    </div>
                                    <small class="text-danger">{{ $errors->first('urgency') }}</small>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {{-- Scholarship Program Name --}}
                                <div class="form-group{{ $errors->has('scholarship_program') ? ' has-error' : '' }}">
                                    <b>Scholarship Program</b>
                                    <a data-toggle="collapse" href="#scholar-info" aria-expanded="false" aria-controls="scholar-info">More Info</a>

                                    <div class="collapse" id="scholar-info">
                                        <div>If tutoring will be covered by your scholarship or other program, please include the name of the program below <b>and</b> then include details on how the required next steps in the "Other Requests Or Concerns" section below.</div>
                                    </div>
                                    {!! Form::text('scholarship_program', null, ['class' => 'form-control', 'placeholder' => 'VA Tutorial Assistance, Daniels Fund']) !!}
                                    <small class="text-danger">{{ $errors->first('scholarship_program') }}</small>
                                </div>
                            </div>

                            {{-- Additional Concerns --}}
                            <div class="col-md-11 col-sm-11">
                                <div class="form-group{{ $errors->has('concerns') ? ' has-error' : '' }}">
                                    <b>Other Requests Or Concerns</b>
                                    <a data-toggle="collapse" href="#concerns-examples" aria-expanded="false" aria-controls="concerns-examples">Examples</a>

                                    <div class="collapse" id="concerns-examples">
                                        <li>If you prefer to meet in your home, please include the town and neighborhood or the nearest intersection.  <b>DO NOT include your full address.</b> </li>
                                        <li>If a scholarship is covering the costs, describe the paperwork necessary and who is paying the tutor.  For example, does the program pay the tutor or do you pay the tutor and get reimbursed by the program.  Also include tutoring cost limits if you know them.</li>
                                        <li>If this request is atypical (e.g. not for a course/class), please include details below.</li>
                                        <li><b>Urgent Requests:</b> If you must meet immediately (in less than 48 hours), please include details about what you are working on, the deadline, and how soon you'd like to meet.  </li>
                                    </div>
                                    {!! Form::textarea('concerns', null, ['class' => 'form-control', 'placeholder' => 'I prefer to meet on the (school_name) campus / in downtown Louisville / at my house near 18th & Broadway in Denver. My scholarship requires the following paperwork...', 'size' => '50x3']) !!}
                                    <small class="text-danger">{{ $errors->first('concerns') }}</small>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="form-group col-md-12 col-sm-12">
                            <legend>How To Pay Your Tutor</legend>
                            Once you've been matched with a tutor, you must add a credit/debit card to your BuffTutor account.  Your selected tutor will then charge your card using our system <b>after</b> each session occurs.  This protects students as we can easily reverse and refund any disputed sessions/charges.<br><br>

                            Paying tutors directly (by cash, check, credit/debit card in person, PayPal, or other means) is a violation of our <a href="{{route('terms')}}">Terms of Use</a>.  <b>You must report any tutor who asks you to pay him/her directly. </b>
                        </fieldset>
                        {{--Agree To Terms--}}
                        {{--
                        <div class="form-group">
                            <div class="checkbox-inline">
                                <div class="checkbox{{ $errors->has('terms') ? ' has-error' : '' }}">
                                    <label for="terms">
                                        {!! Form::checkbox('terms', '1', null, ['id' => 'terms']) !!} I agree to the <a href="{{route('terms')}}">Terms of Use</a>.
                                    </label>
                                </div>
                                <small class="text-danger">{{ $errors->first('terms') }}</small>
                            </div>
                        </div>
                        --}}
                        {{-- Submit Buttons--}}
                        <div class="pull-right">
                            <div class="btn-group">
                                {!! Form::submit("Submit", ['class' => 'btn btn-primary']) !!}
                            </div>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="btn-group">
                                {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                            </div>
                        </div>
                    
                    {!! Form::close() !!}                
                </div>
            </div>
        </div>
    </div>
</div>
