<?php $locations = App\Location::tutorsVisible()->pluck('city', 'id');?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <h3>Why Register?</h3>
             <li>Easily pay for sessions using our online payment system.</li>
             <li>Easily keep track of all sessions and payments.</li>
             <li>If you don't love your tutor, easily request and receive a refund for unsatifactory performance.</li>
             <br>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{$action}}">
                        {{ csrf_field() }}

                        <center> Already have an account?  <a href="{{url('/login')}}">Login here</a></center><br>
                        
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone Number</label>

                            <div class="col-md-6">
                                <input id="phone" type="phone" class="form-control" name="phone" value="{{ old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<hr>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                 <div class="checkbox{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <p><b>Select The City Closest To You</b></p>
                                    @foreach ($locations as $location_id => $city)
                                        <label>
                                            {!! Form::radio('city', $location_id, null) !!} {{$city}}
                                        </label>
                                    @endforeach
                                    
                                </div>
                                <small class="text-danger">{{ $errors->first('city') }}</small>
                            </div>   
                        </div>    

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                 <div class="checkbox{{ $errors->has('role') ? ' has-error' : '' }}">
                                    <p><b>Register As A Student or A Tutor</b></p>
                                    <label>
                                        {!! Form::radio('role', 'student', null, ['id' => 'student', 'onclick' => "showStudentDetails()", 'checked']) !!} Student
                                    </label>
                                    <label>
                                        {!! Form::radio('role', 'tutor-applicant', null, ['id' => 'tutor', 'onclick' => "showStudentDetails()"]) !!} Tutor / Music Teacher
                                    </label>
                                </div>
                                <small class="text-danger">{{ $errors->first('role') }}</small>
                            </div>   
                        </div>    
                        <div id="student_details">
                            <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }}">
                                <label for="school" class="col-md-4 control-label">School</label>

                                <div class="col-md-6">
                                    <input id="school" type="text" class="form-control" name="school" value="{{ old('school') }}">

                                    @if ($errors->has('school'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('school') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                                <label for="year" class="col-md-4 control-label">Year In School</label>

                                <div class="col-md-6">
                                    <input id="year" type="text" class="form-control" name="year" value="{{ old('year') }}">

                                    @if ($errors->has('year'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('major') ? ' has-error' : '' }}">
                                <label for="major" class="col-md-4 control-label">Major</label>

                                <div class="col-md-6">
                                    <input id="major" type="text" class="form-control" name="major" value="{{ old('major') }}">

                                    @if ($errors->has('major'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('major') }}</strong>
                                        </span>
                                    @endif
                                </div>
                        </div>
                        
                        {{-- <div id="referral">
                            <div class="form-group{{ $errors->has('referral_code') ? ' has-error' : '' }}">
                                <label for="referral_code" class="col-md-4 control-label">Referral Code</label>

                                <div class="col-md-6">
                                    <input id="referral_code" type="text" class="form-control" name="referral_code" value="{{ old('referral_code') ?? ($_GET['referral_code'] ?? '')  }}">
                                    <small>If referred by a friend, incude the referral code and your friend will receive a $5 credit after your first session</small>
                                    @if ($errors->has('referral_code'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('referral_code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div> --}}

                        <div class="form-group" id="student_marketing">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox{{ $errors->has('marketing') ? ' has-error' : '' }}">
                                <b>How did you hear about us?</b><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'flyers', null) !!} Flyers
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'chalkboard', null) !!} Chalkboard in class
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'internet', null) !!} Internet Search (e.g. Google)
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'friend', null) !!} Recommended by a friend or classmate
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'professor', null) !!} Recommended by professor or TA
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'tutor-list', null) !!} Tutor List
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'facebook', null) !!} Facebook
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'craigslist', null) !!} Craigslist
                                    </label><br>
                                </div>
                            </div>
                            <small class="text-danger">{{ $errors->first('marketing') }}</small>
                        </div>

                        <div class="form-group" id="tutor_marketing" style="display:none">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox{{ $errors->has('marketing') ? ' has-error' : '' }}">
                                <b>How did you hear about us?</b><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'flyers', null) !!} Flyers
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'internet', null) !!} Internet Search
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'friend', null) !!} Recommended by a friend
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'school-advisor', null) !!} Department E-mail
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'school-list', null) !!} School's Career Center/Website
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'facebook', null) !!} Facebook
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'linkedin', null) !!} LinkedIn
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'handshake', null) !!} Handshake
                                    </label><br>
                                    <label>
                                        {!! Form::checkbox('marketing[]', 'craigslist', null) !!} Craigslist
                                    </label><br>
                                </div>
                            </div>
                            <small class="text-danger">{{ $errors->first('marketing') }}</small>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                 <div class="checkbox{{ $errors->has('terms_of_use') ? ' has-error' : '' }}">
                                    <label for="terms_of_use">
                                        {!! Form::checkbox('terms_of_use', '1', null, ['id' => 'terms_of_use']) !!} I have read and agree to the <a href="{{route('terms')}}">Terms of Use</a>.  {{-- I understand that <span style="color:red">I <b>cannot</b> pay my tutor directly for sessions</span> and that doing so may result in fines.  --}}
                                    </label>
                                </div>
                                <small class="text-danger">{{ $errors->first('terms_of_use') }}</small>
                            </div>   
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript">

        function showStudentDetails()
        {       
            if (document.getElementById('tutor').checked) {
                document.getElementById('student_details').style.display = 'none';
                document.getElementById('student_marketing').style.display = 'none';
                document.getElementById('tutor_marketing').style.display = 'block';
                /*document.getElementById('referral').style.display = 'none';*/

            } 

            else {
                document.getElementById('student_details').style.display = 'block';
                document.getElementById('student_marketing').style.display = 'block';
                document.getElementById('tutor_marketing').style.display = 'none';
                /*document.getElementById('referral').style.display = 'block';*/
            }
        }
    </script>
@endpush
