<?php $hours = $tutor->tutorSessions->sum('duration');?>
@if ($tutor->id == 63)
	<b><em>5000+ Hours</em></b><br>
@elseif($hours > 0)
	@if ($hours <= 100)
		<?php 
			$lower = number_format(floor($hours/10)*10,0);
			$upper = number_format(ceil($hours/10)*10,0);
		?>
	@elseif($hours >100 && $hours <=1000)
		<?php 
			$lower = number_format(floor($hours/100)*100,0);
			$upper = number_format(ceil($hours/100)*100,0);
		?>
	@elseif($hours >1000)
		<?php 
			$lower = number_format(floor($hours/1000)*1000,0);
			$upper = number_format(ceil($hours/1000)*1000,0);
		?>
	@endif
	<b><em>
			{{$lower}}  - {{$upper}} Hours
		</em></b> 
	<br>
@endif
@if ($hours == 0)
	<b><em>New Tutor</em></b><br>
@endif