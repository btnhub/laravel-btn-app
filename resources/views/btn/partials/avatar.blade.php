{{--Avatar for profile or comments.  $user, $radius, $width need to be provided --}}
<a class="media-left" href="{{ route('profile.index', $user->ref_id)}}">
	<img class="media-object" src="/btn/avatars/{{$user->profile->avatar}}" width="{{$width}}" style="border-radius: {{$radius}}" alt="Profile Avatar">
</a>