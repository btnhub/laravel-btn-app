@extends('btn.emails.general_notification')

@section('email_body')

{{$tutor_contract->tutor->first_name}},
<br>
Good news, {{$tutor_contract->tutor_request->student->user_name}} has agreed to work with you in {{$tutor_contract->course}}!   
<br><br>
<b>What Next?</b>
<ol>
	<li>Login to <a href="{{url('/login')}}" target="_blank">your account</a> to access {{$tutor_contract->tutor_request->student->first_name}}'s contact information (under the Student List tab). </li>
	<li>Contact {{$tutor_contract->tutor_request->student->first_name}} <b>immediately</b> to set up a tutor session.  There is a sample introductory email on our website under the Tutors menu.</li>
	<li>Get to work!  Please note that the rate with {{$tutor_contract->tutor_request->student->first_name}} has already been negotiated.  Your take home earnings will be ${{$tutor_contract->tutor_rate}}/hr.</li>
</ol>

<b>Prepare, Prepare, Prepare!</b><br>
At least a day or two <b>before each session</b> contact {{$tutor_contract->tutor_request->student->first_name}} to figure out what will be covered in the upcoming session.  Review that material!
<br><br>
Consider asking {{$tutor_contract->tutor_request->student->first_name}} for the course syllabus, calendar or link to the course website.  If there's an assignment (e.g. exam review) that {{$tutor_contract->tutor_request->student->first_name}} wants to focus on, ask for a copy and review it before the session.

<br><br>

<b>How To Get Paid</b><br>
<b>After each session</b>, click the "Charge {{$tutor_contract->tutor_request->student->first_name}}'s Card" under {{$tutor_contract->tutor_request->student->first_name}}'s name and contact information. Your earnings will immediately be transferred to your Stripe account. That's It!

<br><br>

<b>Prepaid First Session</b><br>
<b style="color:darkred">{{$tutor_contract->tutor_request->student->first_name}} has already prepaid us for the first hour with you.</b> After the first hour, charge {{$tutor_contract->tutor_request->student->first_name}} as usual and your earnings will be transferred to you. If you meet for more than 1 hour, the remaining balance will be charged to {{$tutor_contract->tutor_request->student->first_name}}'s card.<br>

<p><b><em>All sessions must be charged using our platform. It is a violation of your contract to ask for or accept direct payment from your students e.g. by cash, check, PayPal, Venmo, etc.  </em></b></p>

<b>Canceling An Assignment</b><br>
If you cannot take on this student, please inform us immediately!  Be sure to clearly explain why you are unable to accommodate this student.
@stop