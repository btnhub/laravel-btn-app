@extends('btn.emails.general_notification')

@section('email_body')

{{$tutor_contract->tutor_request->student->first_name}},
<br>
Good News!  One of our tutors, {{$tutor_contract->tutor->user_name}}, has offered to assist you with {{$tutor_contract->tutor_request->course}}.  
<br><br>
<b>What Next?</b>
<ol>
	<li>Review <a href="{{route('my.tutor.requests', $tutor_contract->tutor_request->student->ref_id)}}" target="_blank">{{$tutor_contract->tutor->first_name}}'s proposal</a> (including {{$tutor_contract->tutor->first_name}}'s profile and rate).</li>
	
	<li>Indicate whether or not you would like to work with {{$tutor_contract->tutor->first_name}}.  <b>You have until {{$tutor_contract->deadline_student->format('m/d/Y')}} to respond.</b></li>
	
	<li>If you choose to work with this tutor, {{$tutor_contract->tutor->first_name}}'s contact information will show up on <a href="{{route('profile.account', $tutor_contract->tutor_request->student->ref_id)}}" target="_blank">your account</a> under the Tutor List tab.  
		@if (!$tutor_contract->tutor_request->student->customer)
			<em>You will be prompted to add a credit/debit card to your account to access {{$tutor_contract->tutor->first_name}}'s contact information.</em>
		@endif
	</li>
	<li>Reach out to {{$tutor_contract->tutor->first_name}} to schedule the first session.  We will also share your contact information with {{$tutor_contract->tutor->first_name}}.</li>
	
	<li><b>Paying For Sessions:</b> Your credit/debit card will be charged after each session.  <b>Please do not pay tutors directly by cash, check, PayPal, Venmo or any other means.</b> This is for your own protection! If you're not satisfied with a tutor's performance, we are happy to issue a refund, but we cannot do so if you pay the tutor directly. </li>

</ol>

@if ($tutors_contacted > 1)
	
	<p><em>We reached out to {{$tutors_contacted}} tutors regarding this tutor request. You may receive another e-mail notification if another tutor is available to assist you. You are welcome to try out and work with as many tutors as you'd like to.</em></p>
@else
	<p><em>Please note that at the moment, {{$tutor_contract->tutor->first_name}} is the only tutor we have available to assist with this course.</em></p>
@endif

<p>Good luck this semseter!</p>

@stop