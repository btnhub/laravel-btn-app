@extends('btn.emails.general_notification')

@section('email_body')
{{$tutor->first_name}},<br>

We are alerting you to a new {{-- {{$city}} --}} tutor request: <b>{{$tutor_request->subjects}}</b>

<br><br>
<b>Student:</b> {{$tutor_request->student->user_name}}<br>
<b>Course:</b> {{$tutor_request->course}}<br>
<b>Response Deadline: {{Carbon\Carbon::parse($deadline)->format('m/d/Y')}}</b>

<br><br>

<b>What's Next?</b>
<ol>
	<li>Review the <a href="{{route('my.tutor.contracts', $tutor->ref_id)}}" target="_blank">tutor request details</a> and respond indicating whether or not you can help out.</li>
	<li>If you can help, we'll forward your profile to {{$tutor_request->student->first_name}} to review.</li>
	<li>If {{$tutor_request->student->first_name}}  selects you to work with, you will receive an e-mail alert and {{$tutor_request->student->first_name}} will immediately show up on your list of students.</li>
	<li>If {{$tutor_request->student->first_name}} does not select you, you will receive an e-mail notification as well.</li>
</ol>

@stop