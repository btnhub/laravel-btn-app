@extends('btn.emails.general_notification')

@section('email_body')
<p>{{$tutor_contract->tutor_request->student->full_name}} has requested a deadline extension. Please notify {{$tutor_contract->tutor->first_name}}.</p>
<p>Student proposals page: <a href="{{route('my.tutor.requests', $tutor_contract->tutor_request->student->ref_id)}}" target="_blank">{{route('my.tutor.requests', $tutor_contract->tutor_request->student->ref_id)}}</a> </p>

<b>Contract Details</b><br>

<b>Tutor:</b> {{$tutor_contract->tutor->full_name}}<br>
<b>Tutor Email:</b> {{$tutor_contract->tutor->email}}<br>
<b>Location: </b> 
	@if ($tutor_contract->online)
		Online  <br>
		<b>Original Location: </b>
	@endif
	{{$tutor_contract->tutor_request->tutor_location->city}} <br>
<b>Course:</b> {{$tutor_contract->tutor_request->course}}<br><br>

<b>Request ID: </b> {{$tutor_contract->tutor_request->id}} ({{$tutor_contract->tutor_request->ref_id}})<br>
<b>Contract ID: </b> {{$tutor_contract->id}} ({{$tutor_contract->ref_id}})<br>
<b>Tutor Rate:</b> ${{$tutor_contract->tutor_rate}}/hr<br>
<b>Student Max Rate:</b> ${{$tutor_contract->tutor_request->max_rate}}/hr<br>
<b>Student Rate:</b> ${{$tutor_contract->student_rate}}/hr<br><br>

<b>Start Date:</b> {{date('m/d/Y', strtotime($tutor_contract->tutor_request->start_date))}}<br>
<b>End Date:</b> {{date('m/d/Y', strtotime($tutor_contract->tutor_request->end_date))}}<br>
<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
<b>Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
<b>Concerns:</b> <br>
{{$tutor_contract->tutor_request->concerns}}<br>

<b>Tutor Concerns:</b> <br>
{{$tutor_contract->concerns}}<br>

@stop