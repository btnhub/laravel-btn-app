@extends('btn.emails.general_notification')

@section('email_body')

{{$tutor_request->student->first_name}},

<p>This is just a friendly reminder that you have <a href="{{route('my.tutor.requests', $tutor_request->student->ref_id)}}" target="_blank">{{$no_of_proposals}} tutor {{str_plural('proposal', $no_of_proposals)}}</a> for your recent tutor request for {{$tutor_request->course}}. Please review your {{str_plural('proposal', $no_of_proposals)}} and select a tutor to work with. <b>You have {{$remaining_days}} more {{str_plural('day', $remaining_days)}} to review and accept the {{str_plural('proposal', $no_of_proposals)}}.</b></p>

@stop