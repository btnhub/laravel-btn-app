@extends('btn.emails.general_notification')

@section('email_body')
Unfortunately, {{$tutor_contract->tutor_request->student->user_name}} has indicated that he/she would not like to work with you in {{$tutor_contract->tutor_request->course}}
	@if ($tutor_contract->student_reject_reason)
		stating:
		<p>"{{$tutor_contract->student_reject_reason}}"</p>
	@endif
@stop