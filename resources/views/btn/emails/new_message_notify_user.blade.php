@extends('btn.emails.general_notification')

@section('email_body')

You've received a new message from {{$sender}}.  Log into your BuffTutor account to read the message.
<br><br>
<center>"{{substr($body, 0, 50)}}..."</center>

@stop