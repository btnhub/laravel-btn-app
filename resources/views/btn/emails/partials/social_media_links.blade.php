@if (isset($ramtutor))
	<a href="https://www.instagram.com/RamTutor" target="_blank" style="padding-right:0.5em"><img src="{{asset('btn/images/social_media/instagram.png')}}" width="5%"/></a>
	<a href="https://www.facebook.com/RamTutor" target="_blank" style="padding:0px 0.5em"><img src="{{asset('btn/images/social_media/facebook.png')}}" width="5%"/></a>
	<a href="https://www.twitter.com/RamTutor" target="_blank" style="padding:0px 0.5em"><img src="{{asset('btn/images/social_media/twitter.png')}}" width="5%"/></a>
@else
	<a href="https://www.instagram.com/BuffTutor" target="_blank" style="padding-right:0.5em"><img src="{{asset('btn/images/social_media/instagram.png')}}" width="5%"/></a>
	<a href="https://www.facebook.com/BuffTutor" target="_blank" style="padding:0px 0.5em"><img src="{{asset('btn/images/social_media/facebook.png')}}" width="5%"/></a>
	<a href="https://www.twitter.com/BuffTutor" target="_blank" style="padding:0px 0.5em"><img src="{{asset('btn/images/social_media/twitter.png')}}" width="5%"/></a>
@endif
<a href="https://www.linkedin.com/company/the-bufftutor-program" target="_blank"><img src="{{asset('btn/images/social_media/linkedin.png')}}" width="5%" style="padding-left:0.5em"/></a>