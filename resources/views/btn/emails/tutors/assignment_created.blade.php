@extends('btn.emails.general_notification')

@section('email_body')
<h3>{{$assignment->tutor->first_name}},</h3>

You have just received a new student assignment: <br>
<b>Student Name:</b> {{$assignment->student->user_name}}<br>
<b>Course:</b> {{$assignment->course}}<br>

<br>
<h2>Contact {{$assignment->student->first_name}}!</h2>
{{$assignment->student->first_name}}'s contact information is listed under My Account, please contact your student immediately, within the next 24 hours.  Refer to the Checklist listed under My Account for the next steps (how to report the rate, charge a session, etc).
<br>
<b>Note:</b>
<li>There is a sample introductory e-mail online under Introductory Email; please use this as a guide for your first e-mail.</li>
<li>If you do not wish to receive more students, update your profile status to "Not Accepting Students" immediately!</li>
<li>If you cannot take on this student, contact us and the student IMMEDIATELY and explain why.</li>
<li>Students may present you with a 15% off coupon or mention the discount, if so, please let them know that the discount will automatically be deducted from the first session charge.  The system, not you, will determine who is eligible. You do not need to collect the coupon from the student.</li>
<br>
@stop