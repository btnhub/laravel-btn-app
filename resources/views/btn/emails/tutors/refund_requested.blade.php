@extends('btn.emails.general_notification')

@section('email_body')

{{$assignment->tutor->first_name}}, 
<br><br>
This is just an alert that {{$assignment->student->full_name}} has requested the following refund:<br><br>

<b>Course: </b> {{$assignment->course}}<br>
<b>Session Date:</b> {{$refund->session->session_date->format('m/d/Y')}}<br>
<b>Duration:</b> {{$refund->session->duration}} hrs<br>
<b>Outcome: </b> {{$refund->session->outcome}}<br>
<b>Amount Charged: </b> ${{$refund->session->amt_charged}}<br>
<br>
We will review the request and let you know if we decide to refund this session.  We may reach out to you for more information about what happened with {{$assignment->student->first_name}}.
@stop