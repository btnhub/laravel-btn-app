@extends('btn.emails.general_notification')

@section('email_body')
<h3>{{$user->first_name}},</h3>

{{-- <p>Your background check has come back clean.  Please proceed with the next steps on the Tutor Checklist, such as connecting to our Stripe platform and editing your profile.</p> --}}
<p>Your background check has come back clean.  Please <a href="{{route('profile.account', $user->ref_id)}}">log into your account</a> and proceed with the next steps of the application.</p>
@stop