@extends('btn.emails.general_notification')

@section('email_body')
<h3>{{$applicant->first_name}},</h3>

Thank you for expressing interest in becoming a tutor. Our organization is dedicated to helping students quickly connect with experienced and passionate tutors.  Founded in 2009 in Boulder, over the years, we have evolved into a fast and effective tutor referral service.  By carefully hand-picking tutors who join our network, thousands of students and parents have turned to us as a trusted source of knowledgeable and passionate tutors.  
<br><br>
We are always looking for experienced tutors from any field of study and would like to meet with you to discuss your qualifications and our program in more detail.  

<hr>

<h2>Next Steps</h2>
<b>1) Review Program Details</b><br>
{{-- Please review the structure of our program; we will provide more details about our online tools during the interview: --}}
<h3>Flexible Part-Time Position</h3>
Our tutors are allowed to set their own schedule and there isn't a minimum or maximum number of hours that they are required to work.  Feel free to meet with students at any time and place that work for both parties.  You may choose to commute, however you will not be forced to do so. Tutors who work with us tend to earn more than those who work through similar tutoring companies.  Our tutors typically to make $12-40/hr, depending on factors such as the tutor's education, experience, hours reported and feedback from students.
<br><br>

{{-- <h3>Set Your Own Rate</h3>
You are allowed to set your own hourly rate, all we ask is that you charge a fair amount and provide a service worth every penny.  To pay for our service <b><em>30% of your earnings will be deducted</em></b>, well under industry standard that can be as high as 60-70%.  Though the rate is determined by the students and tutors, rates are anywhere from $15-60+/hr.  
<br><br> --}}

<h3>Easily Connect With Students</h3>
As students submit tutor requests, we'll pass along the requests that you are best suited for.  Upon reviewing the request details (schedule, location, rate/earnings) you may accept or reject the assignment. Once you've been matched with a student, you can meet with him/her whenever, wherever and as often as works for both parties. 

<br><br>

<h3>Easy Payment System</h3>
Students connect a credit/debit card to their account and you charge them after each session.  We then use Stripe to transfer the amount you earned in the session to your bank account.  While we release the funds immediately, Stripe takes 2-7 days to deposit them into your bank account.  
<br><br>

<h3>Easy-To-Use Online Tools</h3>
We have provided easy to use online forms and tables to help tutors keep track of charges and payments. 
<br><br>

<h3>Commitment To Students</h3>
Tutors <b>must</b> be able to meet with assigned students through the end of the semester, and cannot be out of town or unavailable to their students for more than 7 days in a semester (excluding university holidays). Tutors cannot be out of town or unavailable to their assigned students during midterms or finals week.
<br><br>

<b>2) Pass Our Proficiency Exams</b><br>
Take our <a href="{{route('prof.exam.list')}}" target="_blank">proficiency exam</a> for each course you wish to tutor (you will need to <a href="{{url('/register')}}" target="_blank">create an account</a> to access the exams). The exams are primarily for core Math & Science courses, however be sure to check the list for the classes that you offer. If there isn't a proficiency exam for any of the courses you wish to tutor, skip this step.<br><br>
<b>NOTE:</b> If you wish to tutor a course that you did not receive a B+ or better in, pass our proficiency exam and we'll take that into consideration.
<br><br>
 
<b>3) Schedule An Interview</b><br>
After you have passed the proficiency exam for each course you wish to tutor, please contact us to schedule a 30-45 minute with one of our recruiters.  The interview will be conducted by video conferencing, via Google Hangouts.
<li>E-mail us a few days and times that work for you (include your time zone). </li>
<li>In your e-mail, include an electronic copy of your most recent transcript.  If the courses you wish to tutor are not listed on this transcript, also include the transcripts that show your performance in these courses. High School transcripts are not necessary.
<li>Schedule an interview AFTER you have figured out your school/work schedule and other time commitments.</li>

<br>
<b>4) Background Checks</b><br>
After you are hired, a background check must be performed.  You will be responsible for paying the ${{DB::table('settings')->where('field', 'background_check_fee')->first()->value}} fee to run the background check.
<hr>

Feel free to e-mail us with any questions or concerns that you may have.  We look forward to hearing from you!
@stop