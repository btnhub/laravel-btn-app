@extends('btn.emails.general_notification')

@section('email_body')

An evaluation of your performance has just been submitted!  All evaluations are listed under My Account.

@stop