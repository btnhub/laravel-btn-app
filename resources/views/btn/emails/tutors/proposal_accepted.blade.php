@extends('btn.emails.general_notification')

@section('email_body')
<h3>{{$assignment->tutor->first_name}},</h3>

<p>Congratulations!  {{$assignment->student->first_name}} has just accepted your proposal to tutor {{$assignment->course}}.  {{$assignment->student->first_name}} now shows up on your list of assigned students under My Account.  Refer to the Tutors Checklist for the next steps, such as reporting the agreed rate, how to submit a charge, etc.</p>

<p>If you are unable to work with this student, contact the student <b>immediately</b> and explain why you are unable to assist.  Also notify us and we will attempt to reassign this student to another tutor.</p>
@stop