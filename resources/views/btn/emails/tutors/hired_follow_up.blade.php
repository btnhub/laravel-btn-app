@extends('btn.emails.general_notification')

@section('email_body')
{{$applicant->first_name}},<br>
Congratulations, we've approved your tutor application and added you into our network as tutor! Here are your next steps:<br>

<ol>
	<li>Log into <a href="{{route('profile.account', $applicant->user->ref_id)}}">your account</a> and complete the last step, signing the contract.</li>
	<li>Then, take care of the <a href="{{route('tutor.checklist')}}">Tutor Checklist</a> items to set up your account and profile.</li>
</ol>

If you have any questions or concerns, please do not hesitate to contact us.
@stop