@extends('btn.emails.general_notification')

@section('email_body')
{{$applicant->first_name}},
<br><br>
We have received your application to become a tutor.  Before we can move forward, please pass our proficiency exams for the courses you wish to tutor. Simply log into your account and select the exam(s) you wish to take.

@stop