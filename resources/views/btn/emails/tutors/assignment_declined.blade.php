@extends('btn.emails.general_notification')

@section('email_body')
{{$assignment->student->user_name}} has declined to work with you in {{$assignment->course}} and will no longer show up on your list of students.  Please do not contact this student further.
@stop
