@extends('btn.emails.general_notification')

@section('email_body')
{{$applicant->first_name}},<br>
We have reviewed your application package and 

@if ($applicant->offered)
	we would like to offer you a position!  Please <a href="{{route('profile.account', $applicant->user->ref_id)}}">log into your account</a> and complete the last steps of the application process.
@else
  unfortunately we will not move forward with your application.
@endif

@stop