@extends('btn.emails.general_notification')

@section('email_body')
<h3>{{$tutor_request->student->first_name}}, </h3>
We have sent your tutor request for {{$tutor_request->course}} to {{$tutor->user_name}}.
<br><br>

<b>What's Next?</b><br>
You haven't been assigned to {{$tutor->first_name}}, so we have not shared your contact information.
<ol>
	<li>Communicate with {{$tutor->first_name}} through our online messaging application (Inbox) to discuss your academic needs, what you can afford and any other concerns you may have.</li>
	<li>If you & {{$tutor->first_name}} decide to work together, {{$tutor->first_name}} will accept your tutor request by clicking the "Make Assignment" button in the message.</li>
	<li>Add a credit/debit card to your account.  {{$tutor->first_name}} will charge tutor sessions to your card <b>after</b> they occur.</li>
	<li>Get to work! Start working with {{$tutor->first_name}} as soon as you want.</li>
</ol>
Your contact information will not be shared with {{$tutor->first_name}} until your tutor request is accepted by {{$tutor->first_name}} and you add a credit/debit card to your account.  <b>Please do not share your contact information while you are still discussing/negotiating with a tutor through the Inbox (before an assignment is made). </b>
<br><br>
If you do not receive a response from {{$tutor->first_name}} in a timely manner, feel free to reach out to a different tutor. If you have any questions or concerns, do not hesitate to contact us.
<br><br>

<b>Tutor Request Details</b><br>
<b>Course:</b> {{$tutor_request->course}}<br>
<b>Time of Day:</b> {{$tutor_request->session_time}}<br>
<b>Max affordable rate:</b> ${{$tutor_request->max_rate}}<br>
<b>Concerns:</b> <br>
{{$tutor_request->concerns}}

<br><br>
<small><em>Once you start working with one of our tutors, he/she will charge sessions to the credit/debit card on your account after they occur.  <b>Please do not pay a tutor for sessions directly </b>(this is a violation of our terms of use); please inform us if your tutor asks you to pay for sessions directly or in person (e.g. by cash, check, PayPal, Venmo, etc).  </em></small>

<br><br>
Good luck this semester!
@stop