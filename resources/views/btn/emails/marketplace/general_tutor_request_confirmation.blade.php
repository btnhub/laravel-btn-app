@extends('btn.emails.general_notification')

@section('email_body')
<h3>{{$tutor_request->student->first_name}}, </h3>
<p>Thank you for choosing BuffTutor, we will review your tutor request immediately! We are usually able to match students with tutors within 48 hours, and even if we do not have a tutor available to help out, we'll still respond to you in a timely manner.<p>  

<b>What Next?</b>

<ol>
	<li>We'll reach out to our tutors and find up to 3 tutors who fit your budget and schedule.</li>
	<li>Once we have a tutor for you, you will receive an e-mail with a summary of the tutor's experience and rate.  You can then decide whether or not you want to work with that tutor.</li>
</ol>

<p>If you have any questions, concerns or do not receive a response from us or one of our tutors in the next 48 hours, please contact us immediately.</p>

<b>Tutor Request Details</b><br>
<b>Course:</b> {{$tutor_request->course}}<br>
<b>Time of Day:</b> {{$tutor_request->session_time}}<br>
<b>Max affordable rate:</b> ${{$tutor_request->max_rate}}<br>
(if no one is available at or under this rate, we may assign you or recommend a tutor whose rate is $5-10 higher)<br>
@if ($tutor_request->concerns)
	<b>Concerns:</b> <br>
	{{$tutor_request->concerns}}
	<br>
@endif

@stop