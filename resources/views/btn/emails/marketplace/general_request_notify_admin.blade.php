@extends('btn.emails.general_notification')

@section('email_body')
The following request was received in {{$tutor_request->tutor_location->city}}.
@if ($tutor_request->include_online_tutors)
	(Include Online Tutors)
@endif

<br><br>
<b>Student:</b> {{$tutor_request->student->full_name}}<br>
<b>Email:</b> {{$tutor_request->student->email}}<br>
<b>Course:</b> {{$tutor_request->course}}<br>
@if ($tutor_request->urgency == "High")
	<b>Urgency:</b>
	<span style="color:red">{{$tutor_request->urgency}} - Student must meet in less than 2 days</span>
	<br>
@endif
<b>Start Date:</b> {{date('m/d/Y', strtotime($tutor_request->start_date))}}<br>
<b>End Date:</b> {{date('m/d/Y', strtotime($tutor_request->end_date))}}<br>
<b>Preferred Session Time:</b> {{$tutor_request->session_time}}<br>
<b>Max Rate:</b> ${{$tutor_request->max_rate}}/hr<br>
<b>Frequency:</b> {{$tutor_request->frequency}}<br>
<b>Scholarship:</b> {{$tutor_request->scholarship_program}}<br>
@if ($tutor_request->concerns)
	<b>Concerns:</b> <br>
	{{$tutor_request->concerns}}<br>
@endif

@if ($tutor_request->requested_tutors)
	<b>Requested Tutors:</b> {{$tutor_request->requested_tutors}} <br>
@endif

@stop