@extends('btn.emails.general_notification')

@section('email_body')
We are alerting you to a new {{$tutor_request->tutor_location->city}} tutor request for the following subject(s): <b>{{$tutor_request->subjects}}</b>

<br><br>
<b>Student:</b> {{$tutor_request->student->user_name}}<br>
<b>Course:</b> {{$tutor_request->course}}<br>
@if ($tutor_request->urgency == "High")
    <b>Urgency:</b>
        <span style="color:red">{{$tutor_request->urgency}} - Student must meet in less than 2 days</span>
    <br>
@endif
<b>Start Date:</b> {{date('m/d/Y', strtotime($tutor_request->start_date))}}<br>
<b>End Date:</b> {{date('m/d/Y', strtotime($tutor_request->end_date))}}<br>
<b>Preferred Session Time:</b> {{$tutor_request->session_time}}<br>
<b>Max Rate:</b> ${{$tutor_request->max_rate}}/hr<br>
<b>Frequency:</b> {{$tutor_request->frequency}}<br>

<br>

For more information about this request, simply log into your BuffTutor account and navigate to the MarketPlace.  Review the tutor request in detail and reach out to the student as soon as possible.

<br><br>

To turn off e-mail notifications, edit your settings under the Notifications menu.
@stop