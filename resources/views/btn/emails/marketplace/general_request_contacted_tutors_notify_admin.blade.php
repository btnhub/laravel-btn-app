@extends('btn.emails.general_notification')

@section('email_body')
The following request was received in {{$tutor_request->tutor_location->city}} and {{$tutors_contacted->count()}} tutor(s) were contacted.
<hr>
<b>Contacted Tutors:</b>
<br>
@foreach ($tutors_contacted as $tutor)
	{{$tutor->full_name}} - {{$tutor->email}}<br>
@endforeach
<hr>
<b>Student:</b> {{$tutor_request->student->full_name}}<br>
<b>Course:</b> {{$tutor_request->course}}<br>
@if ($tutor_request->urgency == "High")
	<b>Urgency:</b>
	<span style="color:red">{{$tutor_request->urgency}} - Student must meet in less than 2 days</span>
	<br>
@endif
<b>Start Date:</b> {{date('m/d/Y', strtotime($tutor_request->start_date))}}<br>
<b>End Date:</b> {{date('m/d/Y', strtotime($tutor_request->end_date))}}<br>
<b>Preferred Session Time:</b> {{$tutor_request->session_time}}<br>
<b>Max Rate:</b> ${{$tutor_request->max_rate}}/hr<br>
<b>Frequency:</b> {{$tutor_request->frequency}}<br>
<b>Scholarship:</b> {{$tutor_request->scholarship_program}}<br>
<b>Concerns:</b> <br>
{{$tutor_request->concerns}}<br>
@stop