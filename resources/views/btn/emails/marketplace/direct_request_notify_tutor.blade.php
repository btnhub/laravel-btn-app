@extends('btn.emails.general_notification')

@section('email_body')
{{$tutor->first_name}},
<br>

Good News!  A student in {{$tutor_request->tutor_location->city}} expressed interest in working with you.  
<br><br>

<b>Student:</b> {{$tutor_request->student->user_name}}<br>
<b>Course:</b> {{$tutor_request->course}}<br>
@if ($tutor_request->urgency == "High")
    <b>Urgency:</b>
        <span style="color:red">{{$tutor_request->urgency}} - Student must meet in less than 2 days</span>
    <br>
@endif
<b>Start Date:</b> {{$tutor_request->start_date}}<br>
<b>End Date:</b> {{$tutor_request->end_date}}<br>
<b>Preferred Session Time:</b> {{$tutor_request->session_time}}<br>
<b>Max Rate:</b> ${{$tutor_request->max_rate}}/hr<br>
<b>Frequency:</b> {{$tutor_request->frequency}}<br>
@if ($tutor_request->scholarship_program)
    <b>Scholarship Program:</b> {{$tutor_request->scholarship_program}}<br>
@endif
@if ($tutor_request->concerns)
    <b>Additional Info / Concerns:</b> <br>
    {{$tutor_request->concerns}}<br>
@endif        

<br>
Contact {{$tutor_request->student->first_name}} through our messaging app (under Inbox)!  <b>You have not been matched with this student yet!</b>  {{$tutor_request->student->first_name}} will not show up on your list of students until you click the "Make Assignment" button.<br><br>

<b>Note:</b><br>
<li><b>Do not ignore this student's tutor request.</b>  If you cannot work with {{$tutor_request->student->first_name}}, use our messaging app (Inbox) to contact {{$tutor_request->student->first_name}} immediately and explain why you cannot be of assistance.  Also click the "Decline Request" button to inform us that {{$tutor_request->student->first_name}} still needs a tutor.</li>
<li><b>Do not include your personal contact information in your messages to {{$tutor_request->student->first_name}}.</b>  This will be viewed as an attempt to steal students.  </li>
<li>If you are no longer accepting students, update your profile <b>immediately</b> to reflect your status and to deactivate the direct tutor request button on your profile.</li>
<br>

<small><span style="color:darkred">Failure to add {{$tutor_request->student->first_name}} to your list of students and charge <b>every tutor session</b> through our website is a violation of your contract and will result in legal action (including hefty fines).</span></small>
@stop