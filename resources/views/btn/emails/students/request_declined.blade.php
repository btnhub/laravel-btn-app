@extends('btn.emails.general_notification')

@section('email_body')

{{$tutor_request->student->first_name}},
<br>
Unfortunately, your requested tutor, {{$tutor_request->requested_tutor->user_name}}, has indicated that he/she cannot help you with {{$tutor_request->course}}.  Please feel free to reach out to our other tutors by either searching our <a href="{{route('list.tutors')}}" target="_blank">tutor list</a> for a tutor you want to work with or submitting our <a href="{{route('request.get')}}" target="_blank">tutor request form</a> to reach out to all our tutors.

@stop