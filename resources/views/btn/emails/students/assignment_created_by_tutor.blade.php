@extends('btn.emails.students.assignment_template')

@section('assign_intro')

<h3>{{$assignment->student->first_name}},</h3>
Good news, {{$assignment->tutor->first_name}} has accepted your tutor request for {{$assignment->course}}! 

@stop