@extends('btn.emails.general_notification')

@section('email_body')

{{$assignment->student->first_name}},
<br><br>

Thank you for choosing {{$assignment->location->company_name}}.  Please take a moment to tell us about your experience with {{$assignment->tutor->first_name}} in {{$assignment->course}} by <a href="{{route('profile.account', $assignment->student->ref_id)}}">evaluating {{$assignment->tutor->first_name}}'s performance</a> (under the Evaluations tab).

Your feedback is incredibly important and helps us keep the best tutors in our network.  <b>No feedback reflects poorly on {{$assignment->tutor->first_name}}.</b>  
<br><br>
{{--Links For GOOGLE REVIEW--}}
<center>
	@unless ($assignment->student->locations->where('company_name', 'RamTutor')->count())
	  {{--BuffTutor URL--}}
	    <b><a href="https://g.page/bufftutor/review?rc" target="_blank">Review Us On Google!</a></b><br><br>
	    <b>
	    	Follow Us: <br><br>
	    	@include("btn.emails.partials.social_media_links")
	    </b>
	@else
	  {{--RamTutor URL--}}
	    <b><a href="https://g.page/ramtutor/review?rc" target="_blank"> Review Us On Google!</a></b><br><br>
	    <b>
	    	Follow Us: <br><br>
	    	@include("btn.emails.partials.social_media_links", ['ramtutor' => true])
	    </b>
	@endunless
</center>
<br>

{{-- <b>Please report bad tutors!</b><br>
If you had a bad experience with {{$assignment->tutor->first_name}} or he/she was not helpful, please let us know! It's not about whether {{$assignment->tutor->first_name}} is a nice person, but whether he/she had a positive impact.  

Report any unprofessional behavior such as:
<li>Inability to clearly explain material</li>
<li>Repeatedly cancelling sessions or not showing up to sessions</li>
<li>Failure to respond to emails in a timely manner</li>
<li>Failure to accommodate you during midterms or finals</li>
<li>Demanding direct payment (cash, check, PayPal, Venmo, etc).</li>

...or anything else that made you feel uncomfortable or negatively affected your academic performance.

<br><br> --}}
Your feedback is greatly appreciated!  Please consider us in the future for your academic needs.
@endsection