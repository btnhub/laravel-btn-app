@extends('btn.emails.general_notification')

@section('email_body')
{{$assignment->tutor->first_name}} submitted the following rate:
<br><br>

<b>Course:</b> {{$assignment->course}}<br>
<b>Agreed Rate:</b> ${{$assignment->rate}} / {{number_format($assignment->billing_increment / 60, 2)}} hr <br>
<br>

If this information is correct, then no further action is necessary. If this information is not correct, please either ask {{$assignment->tutor->first_name}} to correct it, or e-mail us (RE: Incorrect Tutor Rate).
@stop