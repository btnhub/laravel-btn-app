@extends('btn.emails.general_notification')

@section('email_body')

{{$tutor_proposal->student->first_name}},
<br>
Good News!  One of our tutors, {{$tutor_proposal->tutor->first_name}}, has offered to assist you with {{$tutor_proposal->tutor_request->course}}.  {{$tutor_proposal->tutor->first_name}}'s proposal is listed under in your Inbox.  We have not shared your contact information with {{$tutor_proposal->tutor->first_name}}, so use our messaging app to reach out, discuss your academic needs and determine if you'd like to work with {{$tutor_proposal->tutor->first_name}}.	

@stop