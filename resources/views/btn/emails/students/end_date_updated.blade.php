@extends('btn.emails.general_notification')

@section('email_body')
{{$assignment->tutor->first_name}} changed the end date of the assignment with you:
<br><br>

<b>Course:</b> {{$assignment->course}}<br>
<b>Start Date:</b> {{$assignment->start_date->format('m/d/Y')}}<br>
<b>New End Date:</b> {{$assignment->end_date->format('m/d/Y')}}
<br><br>

If this information is correct, then no further action is necessary. If this information is not correct, please either ask {{$assignment->tutor->first_name}} to correct it, or e-mail us (RE: Incorrect Tutor Assignment End Date).
@stop