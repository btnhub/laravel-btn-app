@extends('btn.emails.general_notification')

@section('email_body')
{{$customer->user->first_name}},
<br><br>

Your tutor, {{$tutor->first_name}}, attempted to charge your card, however your card was declined.<br>

<b>Card:</b> xxxx {{str_pad($customer->card_last_four, 4, 0, STR_PAD_LEFT)}}<br>
<b>Exp: </b> {{ str_pad($customer->exp_month, 2, 0, STR_PAD_LEFT) }} / {{ $customer->exp_year }}<br>
<b>Error Message:</b> {{$error_message}}<br>
<br>

Please either contact your bank or add a different card to your account (under <a href="{{route('payment.settings')}}" target="_blank">Payment Settings</a>).

@stop