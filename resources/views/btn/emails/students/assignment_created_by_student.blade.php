@extends('btn.emails.students.assignment_template')

@section('assign_intro')

<h3>{{$assignment->student->first_name}},</h3>
As you've just selected {{$assignment->tutor->first_name}} to assist you with {{$assignment->course}}, please review the following about tutor payments and our policies.  We've also taken down your tutor request, so you will not receive additional proposals from tutors.  If you receive a message from a tutor who has already submitted a proposal to you, simply let him/her know that you have decided to work with a different tutor.

@stop