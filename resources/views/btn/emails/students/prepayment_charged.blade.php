@extends('btn.emails.general_notification')

@section('email_body')
{{$assignment->student->first_name}}, 
<br><br>

We have received your prepayment to work with {{$assignment->tutor->username}} in {{$assignment->course}}: <br>
<br>

@if($discount > 0)
	<b>First Hour:</b> ${{$student_rate}}<br>
	<b>First Session Discount:</b> ${{$discount}}<br>
@endif

<b>Amount Charged:</b> ${{$amount}}

<br><br>

<b>Paying For Future Sessions</b><br>
This payment will cover the first hour with {{$assignment->tutor->first_name}}.  Each subsequent session will be charged to your credit/debit card using our system <u>after</u> it occurs. <b>Please do not pay your tutor directly, e.g. by cash, check, PayPal, Venmo, etc.</b>  This is a violation of our terms of use and, more importantly, if you experience an issue with {{$assignment->tutor->first_name}}, we cannot refund your money.  Please let us know if {{$assignment->tutor->first_name}} asks you to pay for sessions directly, so that we may resolve the issue.
<br><br>

<b>Refund of Prepayment</b><br>
If you are unable to contact or meet with {{$assignment->tutor->first_name}} (for example, if your tutor does not respond to your emails/texts, or your tutor's schedule does not work with yours), you have <b>2 weeks</b> from today to request a refund of the prepaid first hour.  Simply e-mail us, let us know the problem you're experiencing with {{$assignment->tutor->first_name}} and we'll initiate the refund. You are always welcome to try another tutor in our network if you still need assistance.

<br><br>
Good luck this semester!

@stop