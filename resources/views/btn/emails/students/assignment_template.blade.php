@extends('btn.emails.students.welcome_template')

@section('welcome_intro')
  
  @yield('assign_intro')


  <p>If you want to work with a different tutor or no longer need assistance in this course, please  
  <a href="{{route('decline.assignment', ['assign_ref_id' => $assignment->ref_id])}}" target="_blank">Decline Assignment</a>. You'll need to be signed into your account to decline the assignment.</p>
  
  <hr>

   <h2>Get Started!</h2>
    <h3>My Account</h3>
    Log into your BuffTutor account and under My Account, you will be able to access {{$assignment->tutor->first_name}}'s contact information and all your payments, session charges, refunds.  Reach out to {{$assignment->tutor->first_name}} at your convenience to schedule a session.
    

    <br />
    <br />
    <h2>Payments</h2>
    <h4>Add a credit card to your account</h4>
    Please <a href="{{route('payment.settings')}}" target="_blank">add a credit/debit card</a> to your account immediately and your tutor will charge your card after each session.  <b>Do not pay {{$assignment->tutor->first_name}} directly by cash/check or any other means.</b>  This is a violation of our Terms of Use and we cannot refund sessions that were paid directly to tutors. <span style="color:darkred">Please inform us if {{$assignment->tutor->first_name}} asks you for direct payment.</span>

    {{-- @if ($assignment->student->studentBalance() > 0)
      <br><br>Your current BuffTutor account balance is: ${{$assignment->student->studentBalance()}}.  Your tutor will charge your account until your balance is $0 or insufficient to cover the next session.  Please add a credit/debit card to your account to avoid disruption of service.
    @elseif($assignment->student->studentBalance() <0)
      <br><br>
      Your current BuffTutor account balance is: <span style="color:red">${{$assignment->student->studentBalance()}}.</span>  Please <a href="{{route('payment.form')}}" target="_blank">submit a payment</a> immediately for this amount.  Do not add additional funds, instead add a credit/debit card to your account.  Your tutor will charge your card after each session occurs.
    @endif --}}
    <br><h4>We use Stripe to safely process and store your card information.  Your card information will never be visible to us or to your tutor.</h4>

    <br>
    <h2>Policies</h2>
    Please review our <a href="{{url('/cancellation_policy')}}" target="_blank">Cancellation</a> & <a href="{{url('/refund_policy')}}" target="_blank">Refund</a> policies.  Contact your tutor with at least 24 hours notice to cancel a scheduled session and avoid penalties.  
  @stop