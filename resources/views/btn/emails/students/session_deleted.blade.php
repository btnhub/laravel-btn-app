@extends('btn.emails.general_notification')

@section('email_body')
{{$tutor_session->tutor->first_name}} deleted the following tutor session charge:<br>
<br>

<b>Date:</b> {{$tutor_session->session_date->format('m/d/Y')}}<br>
<b>Tutor Name:</b> {{$tutor_session->tutor->full_name}}<br>
<b>Course:</b> {{$tutor_session->assignment->course}}<br>
<b>Session Duration:</b> {{number_format($tutor_session->duration, 2)}} hr<br>
<b>Reason for charge:</b> {{$tutor_session->outcome}}<br>
<b>Amount Charged:</b> ${{number_format($tutor_session->amt_charged, 2)}}<br>
@if ($tutor_session->discount > 0)
	<b>First Session Discount Applied:</b> ${{number_format($tutor_session->discount, 2)}}<br>
@endif
<br>

@stop