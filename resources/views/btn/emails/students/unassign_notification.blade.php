@extends('btn.emails.general_notification')

@section('email_body')

{{$tutor_request->student->first_name}},
<br><br>
Unfortunately we do not currently have a tutor available for {{$tutor_request->course}}.  We are always recruiting and hiring talented tutors, if we hire a suitable tutor in the near future, we will reach out to you immediately.  
<br>
{{-- <b>Tutor Proficiency Exams</b><br>
For years, tutor proficiency exams provided by <b><a href="https://examstash.com/" target="_blank">ExamStash</a></b> has helped us weed out bad tutors.  Consider creating your own proficiency exams to help you find knowledgeable tutors. <a href="https://examstash.com/proficiency-exams" target="_blank">Learn More</a>  --}}
@stop