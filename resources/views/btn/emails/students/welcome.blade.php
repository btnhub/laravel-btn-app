@extends('btn.emails.students.welcome_template')

@section('welcome_intro')
  <h1 style="color:#202020 !important;display:block;font-family:Helvetica;font-size:26px;font-style:normal;font-weight:bold;line-height:100%;letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;text-align:left;">Welcome To BuffTutor!</h1>

  {{$user->first_name}},
  <br>
  Thank you for creating a BuffTutor account.  Finding your perfect tutor couldn't be easier, simply search our <a href="{{route('list.tutors')}}" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#EB4102;" >tutor list</a> for someone you want to work with and then <a href="{{route('request.get')}}" target="_blank" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;color:#EB4102;" >request a tutor</a>. That's it!
@stop