@extends('btn.emails.general_notification')

@section('email_body')
{{$tutor_session->student->first_name}}, 
<br><br>

The following tutor session was charged:<br>
<br>

<b>Date:</b> {{$tutor_session->session_date->format('m/d/Y')}}<br>
<b>Tutor Name:</b> {{$tutor_session->tutor->full_name}}<br>
<b>Course:</b> {{$tutor_session->assignment->course}}<br>
<b>Session Duration:</b> {{number_format($tutor_session->duration, 2)}} hr<br>
<b>Reason for charge:</b> {{$tutor_session->outcome}}<br>
<b>Amount Charged:</b> ${{number_format($tutor_session->amt_charged, 2)}}<br>
@if ($tutor_session->discount > 0)
	<b>First Session Discount Applied:</b> ${{number_format($tutor_session->discount, 2)}}<br>
@endif
<br>

If this information is not correct, please let us know immediately by simply replying to this e-mail.
<br><br>

@if(!count($tutor_session->student->evaluations->where('assignment_id', $tutor_session->assignment->id)->where('type', 'First')))
	<b>Tutor Evaluation</b><br>
	Let us know how your tutor is doing! Please <a href="{{route('eval.first', ['assign_id' => $tutor_session->assignment->ref_id])}}" target="_blank">evaluate {{$tutor_session->tutor->first_name}}'s performance</a> after the first or first few sessions. Be brutally honest, we want to keep only the best tutors in our network!
	<br><br>
@endif

<b>Session Refund</b><br>
Unsatisfied with your tutor's performance? You have 7 days to <a href="{{route('refund.get',$tutor_session->ref_id)}}" target="_blank">request a refund</a> for this session. If you would like to work with a different tutor, feel free to <a href="{{route('request.get')}}" target="_blank">request another tutor</a>.

<br><br>

{{-- <b>Practice Exams</b><br>
Create customizable practice exams to prepare for quizzes and exams.  Visit <a href="https://examstash.com/" target="_blank">ExamStash</a> to learn more.
<br><br> --}}

{{-- @if ($tutor_session->student->evaluations->where('satisfied', 1)->count()) --}}
	{{--Links For GOOGLE REVIEW--}}
	<center>
	@unless ($tutor_session->student->locations->where('company_name', 'RamTutor')->count())
	  {{--BuffTutor URL--}}
	    <b><a href="https://g.page/bufftutor/review?rc" target="_blank">Review Us On Google!</a></b><br><br>
	    <b>
	    	Follow Us: <br><br>
	    	@include("btn.emails.partials.social_media_links")
	    </b>
	@else
	  {{--RamTutor URL--}}
	    <b><a href="https://g.page/ramtutor/review?rc" target="_blank"> Review Us On Google!</a></b><br><br>
	    <b>
	    	Follow Us: <br><br>
	    	@include("btn.emails.partials.social_media_links", ['ramtutor' => true])
	    </b>
	@endunless
	</center>
{{-- @endif --}}
@stop