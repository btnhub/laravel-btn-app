@extends('btn.emails.general_notification')

@section('email_body')

<h3>{{$assignment->student->first_name}},</h3>
Your tutor, {{$assignment->tutor->first_name}}, has just informed us that the two of you will resume tutor sessions in {{$assignment->course}}.

<p>As before, {{$assignment->tutor->first_name}} will use our platform to charge sessions to your card.  To update your credit/debit card information, visit <a href="{{route('payment.settings')}}" target="_blank">Payment Settings</a></p>

<p>If you want to work with a different tutor or no longer need assistance in this course, please  
  <a href="{{route('decline.assignment', ['assign_ref_id' => $assignment->ref_id])}}" target="_blank">Decline Assignment</a>. You'll need to be signed into your account to decline the assignment.</p>

{{-- <b>Practice Exams</b><br>
Create customizable practice exams to prepare for quizzes and exams.  Visit <a href="https://examstash.com/" target="_blank">ExamStash</a> to learn more. --}}

<center>
	@unless ($assignment->student->locations->where('company_name', 'RamTutor')->count())
	  {{--BuffTutor URL--}}
	    <b><a href="https://g.page/bufftutor/review?rc" target="_blank">Review Us On Google!</a></b><br><br>
	    <b>
	    	Follow Us: <br><br>
	    	@include("btn.emails.partials.social_media_links")
	    </b>
	@else
	  {{--RamTutor URL--}}
	    <b><a href="https://g.page/ramtutor/review?rc" target="_blank"> Review Us On Google!</a></b><br><br>
	    <b>
	    	Follow Us: <br><br>
	    	@include("btn.emails.partials.social_media_links", ['ramtutor' => true])
	    </b>
	@endunless
</center>
@stop