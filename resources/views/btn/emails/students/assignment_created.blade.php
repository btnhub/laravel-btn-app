@extends('btn.emails.students.assignment_template')

@section('assign_intro')

<h3>{{$assignment->student->first_name}},</h3>
We have reviewed your tutor request for {{$assignment->course}} and would like to set you up with the following tutor:
<br>
<b>Tutor Name:</b> {{$assignment->tutor->full_name}}<br>
<b>Major:</b> {{$assignment->tutor->profile->major}}<br>
<b>Education Level:</b> {{$assignment->tutor->profile->education()}}<br>
<a href="{{ route('profile.index', ['ref_id' => $assignment->tutor->ref_id]) }}" target="_blank"><button>{{$assignment->tutor->first_name}}'s Profile</button></a>

@stop