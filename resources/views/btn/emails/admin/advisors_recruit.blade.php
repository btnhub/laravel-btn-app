@extends('btn.emails.general_notification')

@section('email_body')

<h3>Dear Graduate & Undergraduate Advisors,</h3>
<p>{{$location->company_name}} is a network of experienced tutors from all fields of study with one goal: providing exceptional academic assistance to College and High School students. Since 2009, we have proven to be a very effective method for tutors to connect with students.  We are recruiting and hiring tutors for this semester and would like to invite your students to join our organization. Please forward the following e-mail to your graduate students, upper classmen undergraduate students, and recent graduates.</p>

<p><b>NOTE: Unfortunately, J-1 or F-1 international students (or any non-immigrants who do not have work permission) cannot be a part of {{$location->company_name}}. </b></p>
<hr>

<h3>Dear Students,</h3>
<p>{{$location->company_name}} is a network of experienced tutors from all fields of study with one goal: providing exceptional academic assistance to College and High School students. We are currently recruiting tutors for this semester and would like to invite you to join our program. While we are primarily looking for math, science, economics/business and language tutors, we consist of tutors from all fields of study. Please visit <a href="http://{{$location->website}}" target="_blank">our website</a> to learn more about us.</p>

Benefits of joining our network:
<li> Quickly and easily connect with students</li>
<li> Set your own rate and your own schedule</li>
<li> Take on only as many students as you can want or can handle.</li>


<p>If you are interested in becoming a tutor, please follow the Become A Tutor link on our website for more details about the services that we provide to tutors and for eligibility rules. Then simply submit the Tutor Application form.
<p>If you are interested in becoming a tutor, please review our eligibility criteria and <a href="{{route('become.tutor')}}" target="_blank">apply online</a>.</p>

<p><b> NOTE: Unfortunately, J-1 or F-1 international students (or any non-immigrants who do not have work permission) cannot be a part of {{$location->company_name}}. </b></p>

<p>Feel free to contact us at {{$location->email}} with any questions.</p>
@stop