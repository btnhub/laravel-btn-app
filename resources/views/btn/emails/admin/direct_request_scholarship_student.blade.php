@extends('btn.emails.general_notification')

@section('email_body')

A direct tutor request was sent and the student indicated that a scholarship program will cover the costs. <br><br>

<b>Student:</b> {{$tutor_request->student->full_name}}<br>
<b>Requested Tutor:</b> {{$tutor_request->requested_tutor->full_name}}<br>
<b>Location:</b> {{$tutor_request->tutor_location->city}}<br>
<b>Course:</b> {{$tutor_request->course}}<br>
@if ($tutor_request->urgency == "High")
	<b>Urgency:</b>
	<span style="color:red">{{$tutor_request->urgency}} - Student must meet in less than 2 days</span>
	<br>
@endif
<b>Start Date:</b> {{date('m/d/Y', strtotime($tutor_request->start_date))}}<br>
<b>End Date:</b> {{date('m/d/Y', strtotime($tutor_request->end_date))}}<br>
<b>Preferred Session Time:</b> {{$tutor_request->session_time}}<br>
<b>Max Rate:</b> ${{$tutor_request->max_rate}}/hr<br>
<b>Frequency:</b> {{$tutor_request->frequency}}<br>
<b>Scholarship:</b> {{$tutor_request->scholarship_program}}<br>
<b>Concerns:</b> <br>
{{$tutor_request->concerns}}<br>
@stop