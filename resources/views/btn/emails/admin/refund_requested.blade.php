@extends('btn.emails.general_notification')

@section('email_body')

{{$assignment->student->full_name}} has requested the following refund:<br><br>

<b>Refund ID: </b> {{$refund->id}}<br>
<b>Tutor</b>: {{$assignment->tutor->full_name}}<br>
<b>Course: </b> {{$assignment->course}}<br>
<b>Session Date:</b> {{$refund->session->session_date->format('m/d/Y')}}<br>
<b>Duration:</b> {{$refund->session->duration}} hrs<br>
<b>Outcome: </b> {{$refund->session->outcome}}<br>
<b>Amount Charged: </b> ${{$refund->session->amt_charged}}<br>
<br>

<b>Requested Amount</b>: {{$refund->requested_amount}}<br>
<b>Refunded To Student: </b> ${{$refund->refunded_to_student}}<br>
<b>Refunded From Tutor: </b> ${{$refund->refunded_from_tutor}}<br>
<b>Refunded From BTN: </b> ${{$refund->refunded_from_btn}}<br>
<b>Refund Reason</b>: {{$refund->refund_reason}}<br>
<b>Refund Method</b>: {{$refund->refund_method}}<br>
<b>Details</b>: {{$refund->method_details}}<br>

@stop