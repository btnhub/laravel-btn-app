@extends('btn.emails.general_notification')

@section('email_body')

{{$refund->student->full_name}} has requested the following refund:<br><br>

<b>Refund ID: </b> {{$refund->id}}<br>

<b>Requested Amount</b>: ${{$refund->requested_amount}}<br>
<b>Refunded To Student: </b> ${{$refund->refunded_to_student}}<br>
<b>Refund Reason</b>: {{$refund->refund_reason}}<br>
<b>Refund Method</b>: {{$refund->refund_method}}<br>
<b>Details</b>: {{$refund->method_details}}<br>

@stop