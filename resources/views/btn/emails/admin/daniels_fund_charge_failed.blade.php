@extends('btn.emails.general_notification')

@section('email_body')

Daniels fund charge failed due to insufficient funds:
<br><br>

<b>Date Submitted:</b> {{date('m/d/Y h:i A')}}  by {{$submitted_by}}<br>
<b>Tutor:</b> {{$df_assignment->assigned_tutor->full_name}} <br>
<b>Student:</b> {{$df_assignment->student->full_name}} <br>
<b>Course:</b> {{$df_assignment->course}} <br>

<hr>

<b>Tutor Pay</b>: ${{$tutor_pay}}<br>
<b>Stripe Balance</b>: ${{$available_balance}} <br>

@stop