@extends('btn.emails.general_notification')

@section('email_body')

Prepayment transfer failed due to insufficient funds:
<br><br>

<b>Date Submitted:</b> {{date('m/d/Y h:i A')}}  by {{$submitted_by}}<br>
<b>Tutor:</b> {{$assignment->tutor->full_name}} <br>
<b>Student:</b> {{$assignment->student->full_name}} <br>
<b>Course:</b> {{$assignment->course}} <br>

<hr>

<b>Tutor Pay</b>: ${{$tutor_pay}}<br>
<b>Stripe Balance</b>: ${{$available_balance}} <br>

@stop