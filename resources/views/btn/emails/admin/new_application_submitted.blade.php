@extends('btn.emails.general_notification')

@section('email_body')

A new application has been submitted:<br><br>

<b>Name:</b> {{$application->full_name}}<br>
<b>Major:</b> {{$application->major}}, {{$application->year}}<br>
<b>Location:</b> {{$application->session_locations}}<br>
@if ($application->link)
	<b>Link</b>: <a href="{{$application->professional_link}}" target="_blank">{{$application->professional_link}}</a><br>
@endif

<b>Courses:</b> <br>
{{$application->courses}}<br><br>
<b>Experience: </b> <br>
{{$application->experience}}<br><br>

<a href="{{route('applicant.show', $application->id)}}" target="_blank">Review Application</a>

@stop