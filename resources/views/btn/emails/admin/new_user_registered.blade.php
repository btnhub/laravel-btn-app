@extends('btn.emails.general_notification')

@section('email_body')

A new user has signed up:<br><br>

<b>Name:</b> {{$user->full_name}}<br>
<b>Email:</b> {{$user->email}}<br>
<b>Phone:</b> {{$user->phone}}<br>

@stop