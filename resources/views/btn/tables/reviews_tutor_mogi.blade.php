<table class="table">
	<tbody>
		@foreach ($mogi_evaluations as $evaluation)
			@if ($evaluation->website_reference)
			<tr>
				<td>
					<p><b>Course: {{$evaluation->course}}</b> {!!($evaluation->satisfied)? "<span style='color:green'>Satisfactory Performance</span>": "<span style='color:red'>Unsatisfactory Performance</span>"!!}</p>
					<p>{{$evaluation->website_reference}}</p>
					
					<p><b><em>{{$evaluation->first_name}} {{substr($evaluation->last_name, 0,1)}} 
						@if ($evaluation->school)
						 	- {{ $evaluation->school}}
						@endif

					, {{ date('F Y', strtotime($evaluation->created_at)) }}
				
					</em></b></p>

				</td>
			</tr>
			@endif
		@endforeach
	</tbody>
</table>