<h3>List Of All Refunds</h3>

<table class="table">
	<thead>
		<tr>
			<th>Ref ID</th>
			<th>Tutor Name</th>
			<th>Requested Amount</th>
			<th>Refund Reason</th>
			<th>Refund Method</th>
			<th>Deductions</th>
			<th>Refunded Amount</th>
			<th>Refunded To</th>
			<th>Status</th>
			<th>Refund Date</th>
		</tr>
	</thead>
	<tbody>
		@if (isset($student->refunds))
			@foreach ($student->refunds as $refund)
				<tr>
					<td>{{$refund->ref_id}}</td>
					@if ($refund->tutor)
						<td>{{$refund->tutor->first_name}} {{$refund->tutor->last_name}}</td>
					@else
						<td> <center>-</center> </td>
					@endif
					
					
					<td>${{$refund->requested_amount}} </td>
					<td>{{$refund->refund_reason}} </td>
					<td>{{$refund->refund_method}} </td>
					<td>{{($refund->deductions) ? "$".$refund->deductions : ""}} </td>
					@if ($refund->refunded_to_student)
						<td>${{$refund->refunded_to_student}} </td>
						<td>Refunded To Student</td>
					@elseif($refund->refunded_to_account)		
						<td>${{$refund->refunded_to_account}} </td>
						<td>BTN Account</td>
					@else
						<td>Refund Under Review </td>
						<td>Student</td>
					@endif
					<td>{{($refund->issue_refund) ? "Refund Issued" : "Refund Not Issued"}} </td>
					<td>{{$refund->processed_date}} </td>

				</tr>
			@endforeach
		@endif
	</tbody>
</table>