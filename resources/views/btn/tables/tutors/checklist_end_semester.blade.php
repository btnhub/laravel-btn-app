<div class="row">
	<a data-toggle="collapse" href="#evaluations" aria-expanded="false" aria-controls="evaluations"><b>Evaluations</b></a>

	<div id="evaluations" class="collapse">
		<table class="table">
		<tbody>
			<tr>
				<td>
					<b>Evaluation Of Last Session</b><br>
					Ask your student to evaluate your overall performance after the last session with you. A link to the evaluation form will show up on your student's account 1 week before the assignment end date. 
					{{-- 
					<li><b>We no longer send out reminders to students about evaluations.</b>  It is your responsibility to remind your students to submit an evalution.  </li> --}}
					<li>Your students will be able to access the evaluation form for 6 months after the assignment end date, <b>however</b> encourage your student to submit an evaluation as soon as possible. </li>
					<li>This affects your ranking on our tutor list.</li>

				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>

<div class="row">
	<a data-toggle="collapse" href="#tutor_dates" aria-expanded="false" aria-controls="tutor_dates"><b>Update Start & End Date</b></a>

	<div id="tutor_dates" class="collapse">
	<table class="table">
		<tbody>
			<tr>
				<td>
					Update your tutor End Date under <a href="{{route('profile.edit', ['user_ref_id' => Auth::user()->ref_id])}}">Edit Profile</a>. <br><br>

					<b>For Returning Tutors</b><br>
					<li>Change the End Date to the last day of <b>next</b> semester. (<a href="{{ route('course.catalogs') }}">Academic Calendars</a>)</li>
					<li>If you are returning next semester but are not able to begin tutoring within the first 2 weeks of the semester, also edit your Start Date to indicate when you will be able to resume tutoring.</li>
					<br>

					We receive tutor requests over the break, so make sure your start and end date for next semester is accurate.  This way students can still reach out to you over the break and reserve a spot with you for next semester.	
					<br><br>

					<b>For Non-returning Tutors</b><br>
					If you are not returning next semester to tutor, simply change the end date to the last day of this semester.   
					
				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>

<div class="row">
	<a data-toggle="collapse" href="#taxes" aria-expanded="false" aria-controls="taxes"><b>Taxes</b></a>

	<div id="taxes" class="collapse">
	<table class="table">
		<tbody>
			<tr>
				<td>
					Stripe creates tax documents (1099-K) for all charges instantly transferred to you.  <a href="https://support.stripe.com/questions/will-i-receive-a-1099-k-and-what-do-i-do-with-it" target="_blank">More information</a>
					<br><br>
					{{-- <a href="{{route('bufftutor.payments')}}">List of Tutor Payments</a> --}}
				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
