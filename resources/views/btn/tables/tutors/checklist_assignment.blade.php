<div class="row">
	<a data-toggle="collapse" href="#new_student" aria-expanded="false" aria-controls="new_student"><b>When Assigned A New Student</b></a>

	<div id="new_student" class="collapse">
		<table class="table">
			<tbody>
				<tr>
					<td>
						<b>Contact Your Student Immediately!</b><br>
						E-mail or call your student immediately! This will increase the chance that your student will respond and schedule a session with you. If you e-mail a student, refer to "Introductory E-mail" (under Tutors menu) for tips and a sample e-mail.
					</td>
				</tr>
				{{-- <tr>
					<td>
						<p><b>Submit Agreed Rate</b></p>
						<p><b>BEFORE THE FIRST SESSION</b>, discuss your rate with your student and report the agreed upon rate by simply clicking the "Add Rate" link next to your student's name on the Assignments table.  </p>
						<li>Though most rates are hourly, you can specify the billing increment for your rate.  For example, if you are a music teacher who provides 45 minute lessons, you can submit your rate for the 45 minute lesson.</li>
					</td>
				</tr> --}}
			</tbody>
		</table>
	</div>
</div>

<div class="row">
	<a data-toggle="collapse" href="#before_session" aria-expanded="false" aria-controls="before_session"><b>Before Each Session</b></a>

	<div id="before_session" class="collapse">
	<table class="table">
		<tbody>
			<tr>
				<td>
					{{-- <li><b>BEFORE THE FIRST SESSION,</b> make sure your student has a Cred/Debit card attached to his/her account.</li> --}}
					<li>Ask your student what he/she wants to cover in the session and review the material. </li>
					<li>Remind your student of an upcoming scheduled session.</li>
					<li><b>Come prepared to each session.</b>  Refresh yourself on the topics to be covered, bring pens, paper, tablet/computer or whatever tools you need.  <b>Do not expect the student to provide these.</b></li>
					<br>
					<span style="color:red">Cancelling A Session:</span> 
					<li>If you need to cancel a session, contact your student immediately.  Review our <a href="{{ url('/cancellation_policy') }}">cancellation policy</a> for penalties if you do not provide at least 24 hours notice to cancel a session.</li>
					<li>If your student cancels a session without 24 hours notice, you may choose whether or not to charge the student for the missed session.</li>
					<li>You must wait 15 minutes for a student to arrive to a session.  In that time, if your student does not reach out to you to inform you that he/she is on the way, you may cancel the session and you may choose whether or not to charge the student for the missed session.</li>
					
				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>

<div class="row">
	<a data-toggle="collapse" href="#after_session" aria-expanded="false" aria-controls="after_session"><b>After Each Session</b></a>

	<div id="after_session" class="collapse">
	<table class="table">
		<tbody>
			<tr>
				<td>
					<b>Charge The Student </b><br>
					After each session (not before), click the "Charge Student" link next to your student's name on the Assignments table and submit the form.  
					
					<li>Charge a student's account <b>immediately</b> after each individual session.</li>  
					<li>Do not let multiple sessions or days pass before charging the session(s). That is a violation of your contract.</li>
					{{--
					<li>If a student requests and receives an Account Balance refund before you charge the session, you will not be paid for that session.</li>
					<li>If a student has multiple tutors and has an unpaid session, the tutor who submitted the last charge will have his/her pay deducted regardless of when the session actually occured.</li>
					<br>
					<p><b>In Summary:</b> Charge the student for the session IMMEDIATELY after the session occurs to ensure you get paid for the session.</p>
					--}}

				</td>
			</tr>
			<tr>
				<td>
					<b>Evaluation Of 1st Session</b><br>
					After the first session or after the first few sessions, ask your student to evaluate your performance.<br>

					<li>A link to the evaluation form will show up on your student's account after you charge the first session.</li>
					
					{{-- <li><b>We no longer send out reminders to students about evaluations.</b>  It is your responsibility to remind your students to submit an evalution.  </li> --}}
				
					<li>This affects your ranking on our tutor list.  If you are a new tutor or do not have an End Of Semester evaluation, these evaluations can improve your ranking on our tutor list.  </li>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>

<div class="row">
	<a data-toggle="collapse" href="#last_session" aria-expanded="false" aria-controls="last_session"><b>After The Last Session</b></a>

	<div id="last_session" class="collapse">
	<table class="table">
		<tbody>
			<tr>
				<td>
					<b>Evaluation Of Last Session</b><br>
					Ask your student to evaluate your overall performance after the last session with you. A link to the evaluation form will show up on your student's account 1 week before the assignment end date. 
					
					{{-- <li><b>We no longer send out reminders to students about evaluations.</b>  It is your responsibility to remind your students to submit an evalution.  </li> --}}
					<li>Your students will only be able to access the evaluation form for 6 months after the assignment end date, <b>however</b> encourage your student to submit an evaluation as soon as possible. </li>
					<li>This affects your ranking on our tutor list.</li>

				</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
