<table class="table">
	<thead>
		<tr>
			<th>Item</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		{{-- @if (Auth::user()->hasRole('tutor-applicant'))
			<tr>
				<td>Tutor Application</td>
				<td>
						<a href="{{route('become.tutor')}}">Submit Application</a>	<br>
						Carefully review our eligibility criteria and submit our application form.
				</td>
			</tr>	
			<tr>
				<td>Proficiency Exams</td>
				<td>
						<a href="{{route('prof.exam.list')}}">Take Proficiency Exam</a>	<br>
						Take and pass our proficiency exams for EACH course you want to tutor
				</td>
			</tr>	
			<tr>
				<td>Schedule Interview</td>
				<td>After you've passed our exams, schedule an interview</td>
			</tr>
		@endif --}}
		@if (Auth::user()->hasRole('tutor-applicant') || Auth::user()->isTutor())
			<tr>
				<td>Tutor Contract</td>
				<td>
					@if (Auth::user()->profile->tutor_contract)
						Contract Submitted On {{Auth::user()->profile->tutor_contract->format('m/d/Y')}}
					@else
							<a href="{{route('get.tutor.contract')}}">Sign Contract Now</a>	<br>
							<span style="color:red">You must sign the contract to get paid</span> 						
					@endif
				</td>
			</tr>
			<tr>
				<td>Background Check</td>
				<td>
					@if ((Auth::user()->backgroundCheck ?? '') && Auth::user()->backgroundCheck->cleared == 1)
						Submitted On {{Auth::user()->backgroundCheck->cleared_date->format('m/d/Y')}}

					@elseif((Auth::user()->backgroundCheck ?? '') && Auth::user()->backgroundCheck->paid == 1 && Auth::user()->backgroundCheck->cleared == 0)
						<p>Payment submitted on {{Auth::user()->backgroundCheck->payment_date->format('m/d/Y')}}</p>

						<a href="https://checkr.com/apply/bufftutor/751e61081157">Click here</a> to submit your background check.  We will notify you if there is a problem with your report.  

					@else
						We use <a href="https://checkr.com/" target="_blank">Checkr</a> to perform background checks.  Tutors are responsible for paying the background check fee.  
						<br><br>

						<b>After you have been hired</b>, <a href="{{route('background.payment')}}">click here</a> to initiate a background check.
						<br>
					@endif
				</td>
			</tr>
		@endif

		@if	(Auth::user()->isTutor())
			<tr>
				<td>Payment Method</td>
				<td>
					@unless (Auth::user()->stripeTutor)
						We use Stripe to charge students and to pay tutors.  Please create a Stripe account and then connect to our platform: <br><br>
						@if (Auth::user()->profile->tutor_contract && ((Auth::user()->backgroundCheck ?? '') && Auth::user()->backgroundCheck->cleared == 1))
							
							<a href="{{route('stripe.connect.signup')}}" class="stripe-connect"><span>Connect with Stripe</span></a> 
							<br><br>
							<b>PLEASE NOTE</b><br>
							<li>When prompted for <b>Business information</b>, this is your information. You are an independent contractor connecting with the BuffTutor platform.  The information you provide to set up a Stripe account is your business or personal information.</li>
							<li>When prompted for a <b>Business website</b>, if you do not already have a business website,  use your BuffTutor profile: {{route('profile.index', ['ref_id' => Auth::user()->ref_id])}}</li>
							
							
						@else
							You must sign the contract & pass a background check before you can connect to our Stripe Platform.
						@endif	
					@else
						You are connected to our Stripe Platform.
					@endunless
				</td>
					
			</tr>
			<tr>
				<td>Complete Profile</td>
				<td>
					<a href="{{route('profile.edit', ['user_ref_id' => Auth::user()->ref_id])}}">Edit Profile</a>
					<br>
					<li>Review the <a href="{{route('course.catalogs')}}">Academic Calendars</a> and make sure you will be present and available during the entire academic term.  <b><em>Inform us if you will be out of town or unavailable for more than 7 days in a semester.</em></b></li>
					<li>Search the <a href="{{route('course.catalogs')}}">Course Catalogs</a> for specific course name & numbers for the college courses you tutor.</li>
					{{-- <li>After you complete your profile, make sure your profile shows up on the Tutor List.  Also, perform a search and make sure you show up in the results.</li> --}}
				</td>
			</tr>
			{{-- <tr>
				<td>Start  & End Date</td>
				<td> 
					@if(Auth::user()->profile->tutor_start_date)
						{{date('m/d/Y',strtotime(Auth::user()->profile->tutor_start_date))}}
					@else
						Edit your profile to add a start date
					@endif
					 ---> 
					@if(Auth::user()->profile->tutor_end_date)
						{{date('m/d/Y', strtotime(Auth::user()->profile->tutor_end_date))}}
					@else
						Edit your profile to add an end date
					@endif
					Your start and end date is the period of time when you are available to tutor and when your profile will show up on our list of tutors.
				</td>
			</tr> --}}
			{{-- <tr>
				<td>Add Card To Account</td>
				<td>
					Why?  Unfortunately previous users have violated the contract, and, in response, we now require that all tutors add a credit/debit card to their account.  <b>Only fines that result from contract violations will be charged.</b>  The easiest way to avoid a fine: <u>abide by the contract and do not violate the terms.</u>
					<br>
					<a href="{{route('payment.settings')}}">Add A Card</a>
				</td>
			</tr> --}}
			<tr>
				<td>Orientation</td>
				<td>
					Check out the <a href="{{route('tutor.orientation')}}">Tutor Orientation</a> page to review our cancellation & refund policies, and to learn how to use our platform and tools.
				</td>
			</tr>
			<tr>
				<td>New Student Checklist</td>
				<td>Review the new student checklist on this page.</td>
			</tr>
			<tr>
				<td><b style="color:blue">Tell A Friend!</b></td>
				<td><b>We are always hiring exceptional tutors like you. If you know of other great tutors & educators, let them know about us!</b></td>
			</tr>
		@endif			
	</tbody>
</table>

@push('css')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('btn/css/stripe-connect.css') }}">
@endpush