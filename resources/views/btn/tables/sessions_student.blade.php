<h3>Sessions</h3>
<p>If a charge listed below is incorrect, please contact your tutor and ask him/her to correct the mistake.</p>
<table class="table">
	<thead>
		<tr>
			<th>Ref ID</th>
			<th>Session Date</th>
			<th>Tutor Name</th>
			<th>Course</th>
			<th>Duration (hrs)</th>
			<th>Outcome</th>
			<th>Amt Charged</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach ($student->activeTutorSessions as $session)	
			@if ($session->tutor && $session->assignment)
				<tr>		
					<td>{{ $session->ref_id }}</td>
					<td>{{date('m/d/Y', strtotime($session->session_date))}}</td>
					<td>{{$session->tutor->first_name}} {{$session->tutor->last_name}}</td>
					<td>{{ $session->assignment->course }}</td>
					<td>{{ $session->duration }}</td>
					<td>{{ $session->outcome }}</td>
					<td>${{ $session->amt_charged }}</td>
					<td>
						{{--Students have 1 week to request a refund. 1 week = 604800 seconds--}}
						@if(strtotime($session->session_date) > strtotime(date('m/d/Y')) - 7*24*60*60 && $session->amt_charged > 0)
							@unless($session->refundRequestSubmitted($session->assignment_id))
								<a href="{{ route('refund.get', ['ref_id' => $session->ref_id])}}" style="color:darkblue;text-decoration: underline">Refund This Session</a>
							@else 
								Refund Already Requested
							@endunless

						@endif
					</td>
				</tr>
			@endif
		@endforeach
	</tbody>
</table>
