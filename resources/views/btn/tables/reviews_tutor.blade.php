<table class="table">
	<tbody>
		@foreach ($evaluations as $evaluation)
			@if (!empty($evaluation->website_reference) && $evaluation->assignment ?? '')
			<tr>
				<td>
					<p><b>Course: {{$evaluation->assignment->course}}</b> {!!($evaluation->satisfied)? "<span style='color:green'>Satisfactory Performance</span>": "<span style='color:red'>Unsatisfactory Performance</span>"!!}</p>
					<p>{{$evaluation->website_reference}}</p>
					
					<p><b><em>{{$evaluation->assignment->student->first_name}} {{substr($evaluation->assignment->student->last_name, 0,1)}} 
						@if ($evaluation->school)
						 	- {{ $evaluation->school}}
						@endif

					, {{ date('F Y', strtotime($evaluation->created_at)) }}
				
					</em></b></p>

				</td>
			</tr>
			@endif
		@endforeach
	</tbody>
</table>