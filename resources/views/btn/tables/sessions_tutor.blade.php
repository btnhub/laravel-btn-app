<h3>Sessions</h3>
<p>You have 24 hours to delete an incorrect charge.  After that, please contact us to delete the charge.  Provide the Reference ID of the session charge.</p>
<table class="table">
	<thead>
		<tr>
			<th>Ref ID</th>
			<th>Session Date</th>
			<th>Student Name</th>
			<th>Course</th>
			<th>Duration (hrs)</th>
			<th>Outcome</th>
			<th>Earnings</th>
			<th>Delete Charge</th>
			{{-- <th>Paid To Tutor On</th> --}}
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->activeTutorSessions as $session)	
			@if ($session->student && $session->assignment)
				<tr>		
					<td>{{ $session->ref_id }}</td>
					<td>{{date('m/d/Y', strtotime($session->session_date))}}</td>
					<td>{{$session->student->first_name}} {{$session->student->last_name}}</td>
					<td>{{ $session->assignment->course }}</td>
					<td>{{ $session->duration }}</td>
					<td>{{ ucwords($session->outcome) }}</td>
					<td>${{ $session->tutor_pay }}</td>
					<td>
						@if ($session->stripe_payment_id || $session->stripe_transfer_id || $session->tutor_id == 63)
							{{-- Tutor can only delete charges with Stripe payments or transfers --}}
							@unless (strtotime($session->created_at) < strtotime(date('m/d/Y')) - 24*30*30)
								{{-- Tutors have 24 hours to delete a session --}}
								@unless ($session->refundRequestSubmitted($session->assignment_id))
									{{-- Tutors cannot delete a charge if a refund has been requested --}}
									{!! Form::open(['method' => 'DELETE', 'route' => ['session.delete', $session->id], 'id' => 'delete_'.$session->ref_id, 'class' => 'delete']) !!}
										<button type="submit" class="btn btn-sm btn-danger">
											<i class="fa fa-trash fa-lg" style="color:red"></i>
										</button>
						        	{!! Form::close() !!}
						        @else
						        	Refund Under Review	
								@endunless
								
							@endunless
						@endif
					</td>
					{{-- <td>{{ $session->tutor_payment_date ? $session->tutor_payment_date->format('m/d/Y') : ''}}</td> --}}
				</tr>
			@endif
		@endforeach
	</tbody>
</table>

@push('scripts')
	<script>
	    $(".delete").on("submit", function(){
	        return confirm("Are you sure you want to delete this session?");
	    });
	</script>
@endpush

