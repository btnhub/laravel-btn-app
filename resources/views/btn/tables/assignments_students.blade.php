{{-- YOU MUST PASS $tutor WHEN INCLUDING THIS VIEW--}}

<h3>Student Assignments</h3>
	<p>Click here to <a href="{{ route('search.students') }}">add a returning student</a></p>
	
	@if($tutor->assignments()->has('tutorContract')->current()->count())
	<div class="table-responsive">
		You have {{ $tutor->assignments()->has('tutorContract')->current()->count() }} {{ str_plural('Student', $tutor->assignments()->has('tutorContract')->current()->count()) }}.  Click on the student's name to submit a charge.  Click on the End Date to edit/update.
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Student Name</th>
					<th>Course</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Tutor Earnings</th>
					<th>Balance *</th>
					<th>Ref ID</th>
				</tr>
			</thead>
			<tbody>
		
		<?php $i=1;?>
			@foreach ($tutor->assignments()->has('tutorContract')->current()->get() as $assignment)
				<?php 
					$df_assignment = $assignment->student->danielsFundAssignments->where('assignment_id', $assignment->id)->where('tutor_id', $assignment->tutor->id)->first();
				?>
				@if ($assignment->student)
					<tr>
						<td>{{$i++}}</td>

						<td>{{date('m/d/Y', strtotime($assignment->start_date))}}</td>
						<td>
							<a href="#" data-toggle="collapse" data-target="#end_date_{{$assignment->ref_id}}">
								{{date('m/d/Y', strtotime($assignment->end_date))}}
							</a>
								<div class="collapse" id="end_date_{{$assignment->ref_id}}">
									@include('btn.forms.profile.end_date_edit')
								</div>

							@if ($assignment->end_date->diffInDays(Carbon\Carbon::now()) < 14)
								<br>
								<em><span style="color:red">Ending Soon!</span></em>
							@endif

						</td>
						<td>
						<div class="row">						
							@if ($assignment->student->customer)
								{{$assignment->student->full_name}}
								@if ($assignment->online)
									<em>(Online)</em>
								@endif
							 	
								@if ($assignment->child_name)
								 	<br>
								 	{{$assignment->child_name}}
								@endif	
							@else
								{{$assignment->student->user_name}}
							@endif
							
							
							@if (!$df_assignment)
									<br>
									@if ($assignment->student->customer)
										<a href="{{route('session.form.customer', ['assign_id' => $assignment->ref_id])}}">Charge {{$assignment->student->first_name}}'s Credit Card</a>
									@else
										<span style="color:red">No Card Attached</span>	
									@endif
							@else
								<br>
									<a href="{{route('get.charge.daniels.scholar', ['df_id' => $df_assignment->id])}}">Charge {{$assignment->student->first_name}}'s Scholarship</a>
									<br>	
							@endif
								
						</div>
						
						</td>
						<td>{{$assignment->course}}</td>
						@if ($assignment->student->customer)
							<td>{{$assignment->student->email}}</td>
							<td>{{$assignment->student->phone}}</td>	
						@else
							<td colspan="2">
								<center><em><b>
									{{$assignment->student->first_name}} Must Add A Card <br>
									For You To Access This Information	
								</b></em></center>
							</td>
						@endif
						
						
						<td> 
							@if (!$df_assignment)
								${{$assignment->tutorContract->tutor_rate}}/hr
							@else
								@if ($df_assignment->rate)
									${{$assignment->tutorContract->tutor_rate}} / hr
								@else
									Pending Approval
								@endif
								
							@endif

						</td>
						<td>
							@if($df_assignment)
								<?php $df_balance = $df_assignment->total - $df_assignment->session_charges->sum('amt_charged'); 
									$remaining_hours = $df_balance / $df_assignment->rate;
								?>
								{{-- {!!($df_balance < 0) ? "<span style='color:red'>-$".abs($df_balance)."</span>" : "<span style='color:green'>$".$df_balance."</span>"!!} --}}
								{!!($remaining_hours < 0) ? "<span style='color:red'>-".number_format($remaining_hours,2)." Hours</span>" : "<span style='color:green'>".number_format($remaining_hours,2)." Hours</span>"!!}
							@endif
						</td>
						<td>{{$assignment->ref_id}}</td>
					</tr>
				@endif
			@endforeach

			</tbody>
		</table>
		* For scholarship students
	</div>	
	@else
		You have no assignments
	@endunless