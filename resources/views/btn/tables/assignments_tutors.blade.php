<h3>Tutor Assignments</h3>

	@if($student->assignments()->has('tutorContract')->current()->count())
	<div class="table-responsive">
		You have {{ $student->assignments()->has('tutorContract')->current()->count() }} {{ str_plural('Tutor', $student->assignments()->has('tutorContract')->current()->count()) }}
		<br>

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Tutor Name</th>
					<th>Course</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Rate</th>
					@if ($student->hasRole('daniels-fund-student'))
						<th>Scholarship Balance</th>
					@endif
					<th>Ref ID</th>
				</tr>
			</thead>
			<tbody>
			<?php $i=1;?>
			@foreach ($student->assignments()->has('tutorContract')->current()->get() as $assignment)
				@if ($assignment->tutor)
					<tr>
						<td>{{$i++}}</td>
						<td>{{date('m/d/Y', strtotime($assignment->start_date))}}</td>
						<td>
							{{date('m/d/Y', strtotime($assignment->end_date))}}							
							
							@if ($assignment->end_date->diffInDays(Carbon\Carbon::now()) < 14)
								<br>
								<em><span style="color:red"> Ending Soon! </span></em>
							@endif
							
						</td>
						<td>
							@if ($student->customer)
								{{$assignment->tutor->full_name}}
							@else
								{{$assignment->tutor->user_name}}
							@endif
							@if ($assignment->online)
								<br>
								<em>(Online)</em>
							@endif				
							<br>
							
							{{-- Show End of Semester Evaluation 1 week before the end date,  Show 1st semester eval after 1st session --}}
							@unless($student->tutorSessions()->where('assignment_id', $assignment->id)->count() == 0)

								@if (strtotime(date('m/d/Y')) > (strtotime($assignment->end_date) - 7*24*3600) && $student->tutorSessions()->where('assignment_id', $assignment->id)->count() >= 3 && !count($assignment->evaluation->where('type', 'End')) )
									
									<a href="{{route('eval.end', ['assign_id' => $assignment->ref_id])}}">End Of Semester Evaluation</a> 
								@elseif ($student->tutorSessions()->where('assignment_id', $assignment->id)->count() >= 1 && !count($student->evaluations->where('assignment_id', $assignment->id)->where('type', 'First')) && !count($assignment->evaluation->where('type', 'End')))
									<a href="{{route('eval.first', ['assign_id' => $assignment->ref_id])}}">Evaluate {{$assignment->tutor->first_name}}'s 1st Session</a>

								@else 
									<small class="text-muted">Evaluation Submitted</small>
								@endif
							@endunless
						</td>
						<td>{{$assignment->course}}</td>
						
						@if ($student->customer)
							<td>{{$assignment->tutor->email}}</td>
							<td>{{$assignment->tutor->phone}}</td>	
						@else
							<td colspan="2">
								<center><em><b>
								To Access
								Your Tutor's Contact Information<br>
								<a href="{{route('payment.settings')}}">Add A Credit/Debit Card</a> </b></em></center>
							</td>
						@endif
						
						<td> 
							@if ($student->hasRole('daniels-fund-student') && $df_assignment = $student->danielsFundAssignments->where('assignment_id', $assignment->id)->where('tutor_id', $assignment->tutor->id)->first())
								${{$df_assignment->rate}} / hr
							@else 
								${{$assignment->tutorContract->student_rate}} / hr<br>
							@endif
						</td>
						@if ($student->hasRole('daniels-fund-student'))
							<td>
								@if ($df_assignment = $student->danielsFundAssignments->where('assignment_id', $assignment->id)->where('tutor_id', $assignment->tutor->id)->first())
									${{$df_assignment->total - $df_assignment->session_charges->sum('amt_charged')}}
								@endif
							</td>
						@endif
						<td>{{ $assignment->ref_id }}</td>
					</tr>
				@endif
			@endforeach

			</tbody>
		</table>
	</div>	
	@else
		You have no tutor assignments.  Search our <a href="{{route('list.tutors')}}">tutor list</a> for the right tutor for you and then <a href="{{route('request.get')}}">request a tutor</a>.
	@endunless