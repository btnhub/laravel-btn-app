<table class="table">
	<thead>
		<tr>
			<th>Date</th>
			<th>Amount</th>
			<th>Processing Fee</th>
			<th>Total Amount</th>
			<th>Payment Method</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody>
		<?php $payments = $student->payments->sortByDesc('id');?>

		@foreach ($payments as $payment)		
			<tr>
				<td>{{date('m/d/Y', strtotime($payment->created_at))}}</td>
				<td>${{$payment->amount}}</td>
				<td>{{$payment->processing_fee ? "$".$payment->processing_fee : ""}}</td>
				<td>${{$payment->total_amount}}</td>
				<td>{{$payment_methods[$payment->payment_method]}}</td>
				<td>{!!($payment->paid)? "Payment Completed" : "<span style='color:red'>Incomplete Payment</span>"!!}</td>
			</tr>
		@endforeach
	</tbody>
</table>