<h3>Previous Tutor Assignments</h3>
	Below is a list of tutors you worked with in the past 6 months.  
	<br>
	If you have not already done so, <b>please submit an evaluation of your tutor's performance</b>. A link will show up under the tutor's name if you met at least 3 times.
	<br><br>
	
	<div class="table-responsive">
		You have {{ $student->assignments()->previous()->count() }} {{ str_plural('Tutor', $student->assignments()->previous()->count()) }}
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Tutor Name</th>
					<th>Course</th>
				</tr>
			</thead>
			<tbody>
			<?php $i=1;?>
			@foreach ($student->assignments()->previous()->get() as $assignment)
				@if ($assignment->tutor)
					<tr>
						<td>{{$i++}}</td>
						<td>{{date('m/d/Y', strtotime($assignment->start_date))}}</td>
						<td>{{date('m/d/Y', strtotime($assignment->end_date))}}</td>
						<td>
							{{$assignment->tutor->first_name}} {{$assignment->tutor->last_name}}						
							<br>
							
							{{-- Show End of Semester Evaluation --}}
							@if($student->tutorSessions()->where('assignment_id', $assignment->id)->count() >=3)
								@if(!count($assignment->evaluation->where('type', 'End')) )
									<a href="{{route('eval.end', ['assign_id' => $assignment->ref_id])}}">End Of Semester Evaluation</a> 
								@else 
									<small class="text-muted">Evaluation Submitted</small>
								@endif
							@endif
						</td>
						<td>{{$assignment->course}}</td>
					</tr>
				@endif
			@endforeach

			</tbody>
		</table>
	</div>	