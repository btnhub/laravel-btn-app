<h3>Evaluations</h3>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Date</th>
			<th>Type*</th>
			<th>Course</th>
			<th>Satisfied With Tutor</th>
			<th>Strengths</th>
			<th>Weaknesses</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->evaluations as $evaluation)
			<tr>
				<td>{{ date('m/d/Y', strtotime($evaluation->created_at)) }} </td>
				<td>{{$evaluation->type}} </td>
				<td>{{$evaluation->assignment ? $evaluation->assignment->course : ''}} </td>
				<td>{{($evaluation->satisfied) ? "Yes": "No"}} </td>
				<td>{{$evaluation->tutor_strength}} </td>
				<td>{{$evaluation->tutor_weakness}} </td>
			</tr>
		@endforeach
	</tbody>
</table>

<p>* Type = First Session Evaluation vs. End Of Semester Evaluation</p>