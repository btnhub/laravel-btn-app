<table class="table">
	<thead>
		<tr>
			<th>Date</th>
			<th>Amount</th>
			<th>Payment Method</th>
			<th>Amount Withheld</th>
			<th>Reason</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor->contractorPayments->sortByDesc('created_at') as $payment)		
			<tr>
				<td>{{date('m/d/Y', strtotime($payment->created_at))}}</td>
				<td>${{$payment->amount_paid}}</td>
				<td>{{$payment->method}}</td>
				<td>{{($payment->amount_withheld) ? "$".$payment->amount_withheld : ""}}</td>
				<td>{{$payment->withhold_reason}}</td>
			</tr>
		@endforeach
	</tbody>
</table>	