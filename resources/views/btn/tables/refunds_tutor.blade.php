<h3>List Of All Refunds</h3>

<table class="table">
	<thead>
		<tr>
			<th>Ref ID</th>
			<th>Date Requested</th>
			<th>Student Name</th>
			<th>Refunded From Tutor</th>
			<th>Refund Reason</th>
			<th>Refund Date</th>
		</tr>
	</thead>
	<tbody>
		@if (isset($tutor->refunds))
			@foreach ($tutor->refunds as $refund)
				@if ($refund->student)
					<tr>
						<td>{{$refund->ref_id}}</td>
						<td>{{date('m/d/Y', strtotime($refund->created_at))}}</td>
						<td>{{$refund->student->first_name}} {{$refund->student->last_name}}</td>
						<td>{{($refund->refunded_from_tutor) ? "$".$refund->refunded_from_tutor : ""}} </td>
						<td>{{$refund->refund_reason}} </td>
						<td>{{$refund->processed_date}} </td>
					</tr>

				@endif
			@endforeach
		@endif
	</tbody>
</table>