<table class="table">
	<thead>
		<tr>
			<th>Details</th>
			<th>Decision</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor_contracts as $tutor_contract)
			<tr>
				<td width="50%">
					<h4>{{$tutor_contract->tutor_request->student->user_name}} - {{$tutor_contract->course}}</h4>
					<b>Ref ID:</b> {{$tutor_contract->ref_id}}<br>
					{{-- <b>Course Level:</b> {{$tutor_contract->tutor_request->level}} <br> --}}
					<b>Location:</b> {{$tutor_contract->location}}
						@if ($tutor_contract->online)
							({{$tutor_contract->tutor_request->city}}, {{$tutor_contract->tutor_request->region}})
						@endif
					<br>
					<b>School:</b> {{$tutor_contract->tutor_request->school}}<br>
					<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
					<b>Session Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
					<b>Start Date:</b> {{$tutor_contract->start_date->format('m/d/Y')}} ({{$tutor_contract->start_date->diffForHumans()}})<br>
					<b>End Date:</b> {{$tutor_contract->end_date->format('m/d/Y')}} ({{$tutor_contract->end_date->diffForHumans()}})<br>
					@if ($tutor_contract->concerns)
						<b style="color:darkred">Concerns:<br>
						 {{$tutor_contract->concerns}}</b><br>
					@endif
				</td>
				<td>

					<b>Net Earnings: </b><br>
					<b style="font-size:2em;padding:20px">${{$tutor_contract->tutor_rate}} / hr</b>	
					<br><br>
				
						You accepted this request on {{$tutor_contract->tutor_decision_date->format('m/d/Y')}}.  
						<br><br>
						<b>Status: </b>
						@if ($tutor_contract->student_decision == 1)
							{{$tutor_contract->tutor_request->student->first_name}} accepted your proposal.
						@elseif($tutor_contract->student_decision === 0)
							{{$tutor_contract->tutor_request->student->first_name}} declined to work with you
								@if ($tutor_contract->student_reject_reason)
									, stating "{{$tutor_contract->student_reject_reason}}"
								@endif
							.
						@else
							{{$tutor_contract->tutor_request->student->first_name}} has not responded to this match. <br>
							<b>Student Response Deadline: </b> {{$tutor_contract->deadline_student->format('m/d/Y')}}
						@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>