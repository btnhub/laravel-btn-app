@extends('layouts.josh-front.default', ['page_title'=>'My Tutor Contracts'])

@section('title', 'My Tutor Contracts')

@section('content')

	<div class="container">
		<div class="row">
	
			<div class="col-sm-12">
				<p>Here are the students we've found for you. Review respond to each request.</p>

				<p><b>If you can help out,</b> select "Yes" and we'll send the student a link to your profile to review .  The student will then determine whether he/she would like to work with you.</p>

				<p><b>If you cannot help,</b> select "No" and briefly explain why (e.g. schedule conflict, not a course you offer, rate too low, etc).</p>

				<p><b>Please do not ignore these requests!</b> We may stop sending them to you if you do not respond in a timely manner.</p>
				
				<p><b style="color:darkgreen">Net Earnings are your take home earnings.  Our service fee has already been deducted. </b></p>

				<hr>
				<ul class="nav nav-tabs">
			        <li class="active"><a data-toggle="tab" href="#home">Unreviewed Requests ({{$unreviewed_tutor_contracts->count()}})</a></li>
			        <li><a data-toggle="tab" href="#menu1">Declined Requests ({{$declined_tutor_contracts->count()}})</a></li>
			        <li><a data-toggle="tab" href="#menu2">Accepted Requests ({{$accepted_tutor_contracts->count()}})</a></li>
			        <li><a data-toggle="tab" href="#menu3">Missed Requests ({{$missed_tutor_contracts->count()}})</a></li>
			    </ul>

			    <div class="tab-content">
			        <div id="home" class="tab-pane fade in active">
			         
			          @include('btn.student_tutor_contracts.tutors.table_unreviewed_requests', ['tutor_contracts' => $unreviewed_tutor_contracts])	
			        </div>
			        <div id="menu1" class="tab-pane fade">
			     		 @include('btn.student_tutor_contracts.tutors.table_declined_requests', ['tutor_contracts' => $declined_tutor_contracts])	
			        </div>
			        <div id="menu2" class="tab-pane fade">
			          	@include('btn.student_tutor_contracts.tutors.table_accepted_requests', ['tutor_contracts' => $accepted_tutor_contracts])
			        </div>
			        <div id="menu3" class="tab-pane fade">
			          @include('btn.student_tutor_contracts.tutors.table_missed_requests', ['tutor_contracts' => $missed_tutor_contracts])
			        </div>
			    </div>				
			</div>
	
		</div>
	</div>
@endsection
