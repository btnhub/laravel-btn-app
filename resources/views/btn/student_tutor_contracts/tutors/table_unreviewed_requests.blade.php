<?php $first_contract = $tutor_contracts->where('tutor_decision', null)->filter(function($contract, $key){return $contract->deadline > Carbon\Carbon::now();})->sortBy('id')->first(); ?>

<table class="table">
	<thead>
		<tr>
			<th>Details</th>
			<th>Decision</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor_contracts->where('tutor_decision', null)->filter(function($contract, $key){return $contract->deadline > Carbon\Carbon::now();})->sortBy('id') as $tutor_contract)
			<tr>
				<td width="50%">
					<h4>{{$tutor_contract->tutor_request->student->user_name}} - {{$tutor_contract->course}}</h4>
					<b>Ref ID:</b> {{$tutor_contract->ref_id}}<br>
					{{-- <b>Course Level:</b> {{$tutor_contract->tutor_request->level}} <br> --}}
					<b>Location:</b> {{$tutor_contract->location}}
						@if ($tutor_contract->online)
							({{$tutor_contract->tutor_request->city}}, {{$tutor_contract->tutor_request->region}})
						@endif
					<br>
					<b>School:</b> {{$tutor_contract->tutor_request->school}}<br>
					<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
					<b>Session Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
					<b>Start Date:</b> {{$tutor_contract->start_date->format('m/d/Y')}} ({{$tutor_contract->start_date->diffForHumans()}})<br>
					<b>End Date:</b> {{$tutor_contract->end_date->format('m/d/Y')}} ({{$tutor_contract->end_date->diffForHumans()}})<br>
					@if ($tutor_contract->concerns)
						<b style="color:darkred">Concerns:<br>
						 {{$tutor_contract->concerns}}</b><br>
					@endif
					
				</td>
				<td>
					<b>Net Earnings: </b><br>
					 	<b style="font-size:2em;padding:20px">${{$tutor_contract->tutor_rate}} / hr</b>	
					<br><br>
				
						Can you assist {{$tutor_contract->tutor_request->student->user_name}} with "{{$tutor_contract->course}}"?<br>
						
					{!! Form::open(['method' => 'POST', 'route' => ['tutor.contract.decision', $tutor_contract->ref_id], 'class' => 'form-horizontal', 'id' => "form-decision-$tutor_contract->ref_id"]) !!}
					
					    <div class="{{ $errors->has('decision') ? ' has-error' : '' }}">
						    <label>
						        <input type="radio" name="decision" value="1" onClick='showHideReason(1)' @if ($tutor_contract != $first_contract)
								         	disabled
								         @endif> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    </label>

						    <label>
						        <input type="radio" name="decision" value="0" onClick='showHideReason(0)' @if ($tutor_contract != $first_contract)
								         	disabled
								         @endif> No
						    </label>
						    <br>
						    <small class="text-danger">{{ $errors->first('decision') }}</small>
						    <small class="text-danger">{{ $errors->first('tutor_reject_reason') }}</small>
						</div>
						<div id="reject_reason" style="display: none">
							<div class="form-group{{ $errors->has('tutor_reject_reason') ? ' has-error' : '' }}">
							    {!! Form::textarea('tutor_reject_reason', null, ['class' => 'form-control', 'size' => '20x3', 'placeholder' => 'Briefly explain why you cannot help out']) !!}
							    <small class="text-danger">{{ $errors->first('tutor_reject_reason') }}</small>
							</div>	
						</div>
						
					    <div class="btn-group pull-right">
					        {{-- {!! Form::submit("Submit Decision", ['class' => 'btn btn-success', 'id' => 'submit_button']) !!} --}}
					    </div>
					    <input type="submit" value="Submit Decision" name="submit_button" id="submit_button"
								         class="btn btn-success"
				         @if ($tutor_contract != $first_contract)
				         	disabled
				         @endif
				        >

				        <div id="sms-alert" style="display:none">
				        	@if ($tutor_contract->tutor_request->sms_alert)
					        	<br>
					        	<small><b>{{$tutor_contract->tutor_request->student->first_name}} requested a text alert.  Please accept the assignment at a <span style="color:blue">reasonable hour</span>.</b></small>	
					        @endif	
				        </div>

				    	@if ($tutor_contract != $first_contract)
			         	<br><small>Please respond to the first request listed above.</small>
			         	@endif
					{!! Form::close() !!}
					<br>
					<small><b>Response Deadline: {{$tutor_contract->deadline->format('m/d/Y ga')}}</b></small>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@push('scripts')
	<script>
		function showHideReason(decision){
			if (decision == 1) {
				document.getElementById('reject_reason').style.display = 'none';

				document.getElementById('submit_button').value = "Accept Assignment";

				document.getElementById('sms-alert').style.display = 'block';
			}
			else
			{
				document.getElementById('reject_reason').style.display = 'block';
				document.getElementById('submit_button').value = "Reject Assignment";
				document.getElementById('sms-alert').style.display = 'none';
			}
		}

	</script>
@endpush