<table class="table">
	<thead>
		<tr>
			<th>Details</th>
			<th>Decision</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor_contracts as $tutor_contract)
			<tr>
				<td width="50%">
					<h4>{{$tutor_contract->tutor_request->student->user_name}} - {{$tutor_contract->course}}</h4>
					<b>Ref ID:</b> {{$tutor_contract->ref_id}}<br>
					{{-- <b>Course Level:</b> {{$tutor_contract->tutor_request->level}} <br> --}}
					<b>Location:</b> {{$tutor_contract->location}}
						@if ($tutor_contract->online)
							({{$tutor_contract->tutor_request->city}}, {{$tutor_contract->tutor_request->region}})
						@endif
					<br>
					<b>School:</b> {{$tutor_contract->tutor_request->school}}<br>
					<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
					<b>Session Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
					<b>Start Date:</b> {{$tutor_contract->start_date->format('m/d/Y')}} ({{$tutor_contract->start_date->diffForHumans()}})<br>
					<b>End Date:</b> {{$tutor_contract->end_date->format('m/d/Y')}} ({{$tutor_contract->end_date->diffForHumans()}})<br>
					@if ($tutor_contract->concerns)
						<b style="color:darkred">Concerns:<br>
						 {{$tutor_contract->concerns}}</b><br>
					@endif
					
				</td>
				<td>

					<b>Net Earnings: </b><br>
				 	<b style="font-size:2em;padding:20px">${{$tutor_contract->tutor_rate}} / hr</b>	
					<br><br>
				
						You declined this request on {{$tutor_contract->tutor_decision_date->format('m/d/Y')}}.  
						<br><br>
						@if ($tutor_contract->deadline->isFuture())
							
							Would you like to change your decision and assist {{$tutor_contract->tutor_request->student->user_name}} with "{{$tutor_contract->course}}"?<br>
							<b>Response Deadline: {{$tutor_contract->deadline->format('m/d/Y')}} ({{$tutor_contract->deadline->diffForHumans()}})</b>
							{!! Form::open(['method' => 'POST', 'route' => ['tutor.contract.decision', $tutor_contract->ref_id], 'class' => 'form-horizontal', 'id' => "form-accept-$tutor_contract->ref_id"]) !!}
							
							    <div class="{{ $errors->has('decision') ? ' has-error' : '' }}">
								    <label>
								        {!! Form::radio('decision', 1,  null, []) !!} Yes, I'd now like to assist with this course &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								    </label>
								    <br>
								    <small class="text-danger">{{ $errors->first('decision') }}</small>
								</div>
								
							    <div class="btn-group">
							        {!! Form::submit("Accept Request", ['class' => 'btn btn-primary']) !!}
							    </div>
								@if ($tutor_contract->tutor_request->sms_alert)
						        	<br>
						        	<small><b>{{$tutor_contract->tutor_request->student->first_name}} requested a text alert.  Please accept the request at a <span style="color:blue">reasonable hour</span>.</b></small>	
						        @endif
							{!! Form::close() !!}
							
						@endif
				
				</td>
			</tr>
		@endforeach
	</tbody>
</table>