<table class="table">
	<thead>
		<tr>
			<th>Details</th>
			<th>Decision</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor_contracts as $tutor_contract)
			<tr>
				<td width="50%">
					<h4>{{$tutor_contract->tutor_request->student->user_name}} - {{$tutor_contract->course}}</h4>
					<b>Ref ID:</b> {{$tutor_contract->ref_id}}<br>
					{{-- <b>Course Level:</b> {{$tutor_contract->tutor_request->level}} <br> --}}
					<b>Location:</b> {{$tutor_contract->location}}
						@if ($tutor_contract->online)
							({{$tutor_contract->tutor_request->city}}, {{$tutor_contract->tutor_request->region}})
						@endif
					<br>
					<b>School:</b> {{$tutor_contract->tutor_request->school}}<br>
					<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
					<b>Session Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
					<b>Start Date:</b> {{$tutor_contract->start_date->format('m/d/Y')}} ({{$tutor_contract->start_date->diffForHumans()}})<br>
					<b>End Date:</b> {{$tutor_contract->end_date->format('m/d/Y')}} ({{$tutor_contract->end_date->diffForHumans()}})<br>
					@if ($tutor_contract->concerns)
						<b style="color:darkred">Concerns:<br>
						 {{$tutor_contract->concerns}}</b><br>
					@endif
					
				</td>
				<td>

					<b>Net Earnings: </b><br>
					 	<b style="font-size:2em;padding:20px">${{$tutor_contract->tutor_rate}} / hr</b>	
					<br><br>
				
					You did not respond to this request before the deadline on {{$tutor_contract->deadline->format('m/d/Y')}}.
				
				</td>
			</tr>
		@endforeach
	</tbody>
</table>