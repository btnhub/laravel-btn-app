<table class="table">
	<thead>
		<tr>
			<th>Details</th>
			<th>Decision</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor_contracts as $tutor_contract)
			<tr>
				<td width="30%">
					<b>Ref ID:</b> {{$tutor_contract->ref_id}}<br>
					<b>Course:</b> {{$tutor_contract->course}}<br>
					<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
					<b>Session Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
					<b>Dates:</b> {{$tutor_contract->start_date->format('m/d/Y')}}  to {{$tutor_contract->end_date->format('m/d/Y')}}<br>
					<b>Max Rate:</b> ${{$tutor_contract->tutor_request->max_rate}}/hr
				</td>
				<td>
					<p style="font-size:1.25em;font-weight:bold;color:black"> <a href="{{route('profile.index', $tutor_contract->tutor->ref_id)}}" target="_blank">{{$tutor_contract->tutor->user_name}}</a> - {{$tutor_contract->tutor->profile->education()}} in {{$tutor_contract->tutor->profile->major}}</p>
					<p style="font-size:1.25em;font-weight:bold;color:black">Rate: ${{$tutor_contract->student_rate}}/hr</p>
						
					@if ($tutor_contract->online)
						<p style="color:black"><b><em>Sessions with {{$tutor_contract->tutor->user_name}} would be conducted online.</em></b></p>
					@endif
					
					<br>
					You did not accept this assignment before the deadline on {{$tutor_contract->deadline->format('m/d/Y')}}.
					<br><br>

					@if ($tutor_contract->tutor->profile->tutor_status)
						{!! Form::open(['method' => 'POST', 'route' => ['renew.tutor.proposal', $tutor_contract->ref_id]]) !!}
					
						    If you'd like to work with {{$tutor_contract->tutor->first_name}}, please click below. 
						    <br><br>

						    <div class="pull-left">
						        {!! Form::submit("Check If {$tutor_contract->tutor->user_name} Is Available", ['class' => 'btn btn-info']) !!}
						        <br>
						        <small><b>If {{$tutor_contract->tutor->user_name}} is not available, we'll reach out to other available tutors in our network.</b></small>
						        <br><br>
						    </div>
						
						{!! Form::close() !!}
					@else
						Unfortunately, {{$tutor_contract->tutor->user_name}} is no longer available.  Feel free to submit another <a href="{{route('request.get')}}">tutor request</a>.
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>