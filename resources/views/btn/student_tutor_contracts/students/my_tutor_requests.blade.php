@extends('layouts.josh-front.default', ['page_title' => "My Tutor Requests"])

@section('title')
  My Tutor Requests
@stop

@section('content')
<a href="{{route('profile.account', Auth::user()->ref_id)}}"><-- Back To My Account</a>

<h3>Pending Tutor Proposals</h3>
<p>Below are tutors we've found for your recent tutor requests. Please review each tutor's profile and indicate whether or not you'd like to work with the tutor. </p>

<center><b style="color:darkblue">Feel free to try out or work with as many tutors as you'd like!</b></center>

@unless($student->customer)
<p><b>Please note, you will need to <a href="{{route('payment.settings')}}">add a credit/debit card</a> to your account to access a tutor's contact information.</b></p>

@endunless

<ul class="nav nav-tabs">
	<li class="active"><a data-toggle="tab" href="#home">Unreviewed Proposals <b>({{$unreviewed_tutor_contracts->count()}})</b></a></li>
	<li><a data-toggle="tab" href="#menu1">Declined Proposals <b>({{$declined_tutor_contracts->count()}})</b></a></li>
	<li><a data-toggle="tab" href="#menu2">Missed Proposals <b>({{$missed_tutor_contracts->count()}})</b></a></li>
</ul>

<div class="tab-content">
	<div id="home" class="tab-pane fade in active">
	 	@include('btn.student_tutor_contracts.students.table_unreviewed_proposals', ['tutor_contracts' => $unreviewed_tutor_contracts])
	</div>
	<div id="menu1" class="tab-pane fade">
		@include('btn.student_tutor_contracts.students.table_declined_proposals', ['tutor_contracts' => $declined_tutor_contracts]) 
	</div>
	<div id="menu2" class="tab-pane fade">
	  	@include('btn.student_tutor_contracts.students.table_missed_proposals', ['tutor_contracts' => $missed_tutor_contracts]) 
	</div>
</div>

@endsection