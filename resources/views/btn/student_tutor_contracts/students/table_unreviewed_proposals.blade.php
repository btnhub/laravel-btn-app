<?php
	$first_contract = $tutor_contracts->first();
?>
<table class="table">
	<thead>
		<tr>
			<th>Details</th>
			<th>Decision</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($tutor_contracts as $tutor_contract)
			<tr>
				<td width="30%">
					<b>Ref ID:</b> {{$tutor_contract->ref_id}}<br>
					<b>Course:</b> {{$tutor_contract->course}}<br>
					<b>Session Time:</b> {{$tutor_contract->tutor_request->session_time}}<br>
					<b>Session Frequency:</b> {{$tutor_contract->tutor_request->frequency}}<br>
					<b>Dates:</b> {{$tutor_contract->start_date->format('m/d/Y')}}  to {{$tutor_contract->end_date->format('m/d/Y')}}<br>
					<b>Max Rate:</b> ${{$tutor_contract->tutor_request->max_rate}}/hr
				</td>
				<td>
					<p style="font-size:1.25em;font-weight:bold;color:black"> <a href="{{route('profile.index', $tutor_contract->tutor->ref_id)}}" target="_blank">{{$tutor_contract->tutor->user_name}}</a> - {{$tutor_contract->tutor->profile->education()}} in {{$tutor_contract->tutor->profile->major}}</p>
					<p style="font-size:1.25em;font-weight:bold;color:black">Rate: ${{$tutor_contract->student_rate}}/hr</p>

					@if ($tutor_contract->online)
						<p style="color:black"><b><em>Sessions with {{$tutor_contract->tutor->user_name}} would be conducted online.</em></b></p>
					@endif
					
					Would you like to work with {{$tutor_contract->tutor->user_name}}?<br>
					
					
					{!! Form::open(['method' => 'POST', 'route' => ['student.contract.decision', $tutor_contract->ref_id], 'class' => 'form-horizontal', 'id' => "form-decision-$tutor_contract->ref_id"]) !!}
					
					    <div class="{{ $errors->has('student_decision') ? ' has-error' : '' }}">
						    <label>
						        <input type="radio" name="student_decision" value="1" onClick='showHideReason(1)' @if ($tutor_contract != $first_contract)
								         	disabled
								         @endif> Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						    </label>
						    <label>
						        <input type="radio" name="student_decision" value="0" onClick='showHideReason(0)' @if ($tutor_contract != $first_contract)
								         	disabled
								         @endif> No
						    </label>
						    <br>
						    <small class="text-danger">{{ $errors->first('student_decision') }}</small>
						    <small class="text-danger">{{ $errors->first('student_reject_reason') }}</small>
						</div>
						<div id="reject_reason" style="display: none">
							<div class="form-group{{ $errors->has('student_reject_reason') ? ' has-error' : '' }}">
							    {!! Form::textarea('student_reject_reason', null, ['class' => 'form-control', 'size' => '20x3', 'placeholder' => "Briefly explain why you do not want to work with {$tutor_contract->tutor->first_name}.  For example, the rate is too high, schedule conflict, etc."]) !!}
							    <small class="text-danger">{{ $errors->first('student_reject_reason') }}</small>
							</div>	
						</div>
						
					    <div class="btn-group">
					        <input type="submit" value="Submit Decision" name="submit_button" id="submit_button"
					         class="btn btn-success"
					         @if ($tutor_contract != $first_contract)
					         	disabled
					         @endif
					        >
					    	@if ($tutor_contract != $first_contract)
				         	<br><br><small style="color:darkred">Please respond to the first proposal listed above.</small>
				        @endif
					    </div>
						
						
					{!! Form::close() !!}
					
					<small>Response Deadline: {{$tutor_contract->deadline_student->format('m/d/Y ga')}} ({{$tutor_contract->deadline_student->diffForHumans()}})</small>													
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

@push('scripts')
	<script>
		function showHideReason(student_decision){
			if (student_decision == 1) {
				document.getElementById('reject_reason').style.display = 'none';

				document.getElementById('submit_button').value = "Accept Assignment";
			}
			else
			{
				document.getElementById('reject_reason').style.display = 'block';
				document.getElementById('submit_button').value = "Reject Assignment";
			}
		}

	</script>
@endpush