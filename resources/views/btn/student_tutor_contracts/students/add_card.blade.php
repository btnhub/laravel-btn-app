@extends('layouts.josh-front.default', ['page_title' => "Add Credit/Debit Card"])

@section('title')
  Add Credit/Debit Card
@stop

@section('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
@stop

@section('content')
		
<div class="container">
  <div class="row">
    <div class="col-md-9 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Add A Card</div>
        <div class="panel-body">
          <p>To complete this assignment and access {{$tutor_contract->tutor->user_name}}'s contact information, please add a card to your account.  Your card will only be charged <b>after</b> each session with {{$tutor_contract->tutor->user_name}}.</p>
          {!! Form::open(['method' => 'POST', 'route' => ['tutor.contracts.submit.card', $tutor_contract->ref_id], 'id' => 'payment-form','class' => 'form-horizontal']) !!}

            <div class="col-sm-6 col-md-6 col-sm-offset-2 col-md-offset-2">
              @include('btn.stripe.card_form_partial')
            </div>

            <div class="col-sm-4 col-md-4">      
                <br><br>
                {!! Form::submit("Add Card", ['class' => 'btn btn-primary']) !!}
            </div>
            
          {!! Form::close() !!}
        </div>  
      </div>
    </div>
  </div>
</div> 

@stop