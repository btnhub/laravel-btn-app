{{$tutor_requests->count()}} Pending Requests<br>

<div class="row">
	<div>
		<h3>Summary</h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Request ID</th>
					<th>Details</th>
					<th>Contracts</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<col width="10%">
		  		<col width="30%">
		  		<col width="45%">
		  		<col width="15%">
				@foreach ($tutor_requests as $tutor_request)
					<tr>
						<td><a href="#{{$tutor_request->id}}" onclick="showRequestDetails({{$tutor_request->id}})">{{$tutor_request->id}}</a></td>
						<td><span style="font-size:1.25em;"><a href="{{route('user.details.get', $tutor_request->student->id)}}" target="_blank">{{$tutor_request->student->full_name}}</a>  
							<br>
							{{$tutor_request->course}} @ ${{$tutor_request->max_rate}}/hr</span>
							<br>

							<span style="color:darkred;">
								<b>{{$tutor_request->city ? "$tutor_request->city, $tutor_request->region" : $tutor_request->location}}
								@if ($tutor_request->include_online_tutors)
									(Include Online Tutors)
								@endif
								</b>
							</span>
							@if ($tutor_request->email_reminder)
								<br>
								<b><em>Email reminder sent {{$tutor_request->email_reminder->diffForHumans()}}</em></b>
							@endif
							@if ($tutor_request->sms_reminder)
								<br>
								<b><em>Text reminder sent {{$tutor_request->sms_reminder->diffForHumans()}}</em></b>
							@endif
						</td>
						<td>
							@foreach ($tutor_request->tutor_contracts as $tutor_contract)

								<a href="{{ route('user.details.get', ['user_id' => $tutor_contract->tutor->id])}}" target="_blank">{{$tutor_contract->tutor->full_name}} </a>
									@if ($tutor_contract->online)
										(Online)
									@endif
									
								@ ${{$tutor_contract->student_rate}}/hr (${{$tutor_contract->tutor_rate}}/hr) <br>

								@if ($tutor_contract->tutor_decision == 1)
									<b><em style="color:darkgreen">Tutor Accepted on {{$tutor_contract->tutor_decision_date->format('m/d/Y')}}</em></b>
								@elseif($tutor_contract->tutor_decision === 0)
									<b><em style="color:darkred">Rejected on {{$tutor_contract->tutor_decision_date->format('m/d/Y')}}</em></b><br>
									{{$tutor_contract->tutor_reject_reason}}
								@else
									<b>Deadline: </b>  {!!$tutor_contract->deadline->isFuture() ? $tutor_contract->deadline->diffForHumans() : "<span style='color:red'>{$tutor_contract->deadline->diffForHumans()}</span>"!!} 
								@endif
								<br>
								@if ($tutor_contract->student_decision === 0)
									<b><span style="color:darkorange">Rejected By Student</span></b><br>
									{{$tutor_contract->student_reject_reason}}
								@elseif($tutor_contract->student_decision === null  && $tutor_contract->deadline_student !==null)
									<b>Student Deadline: </b> 
									@if ($tutor_contract->deadline_student->isPast())
										<span style="color:darkred">{{$tutor_contract->deadline_student->diffForHumans()}}</span><br>
									@else
										<span style="color:darkgreen">{{$tutor_contract->deadline_student->diffForHumans()}}</span><br>
									@endif
								@elseif($tutor_contract->student_decision === 1)
									<b><span style="color:purple">ACCEPTED By Student</span></b><br>
								@endif
								<br>
							@endforeach
						</td>
						<td>
							{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'unassign', 'id' => 'unassign-'.$tutor_request->id]) !!}

							    <div class="btn-group pull-right">
							        {!! Form::submit("Unassign", ['class' => 'btn btn-warning']) !!}
							    </div>

							{!! Form::close() !!}

							{!! Form::open(['method' => 'DELETE', 'route' =>['delete.tutor.request', $tutor_request->id], 'class' => 'delete']) !!}
							    <div class="btn-group pull-left">
							        <button type="submit" class="btn btn-sm btn-danger">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
							    </div>
							
							{!! Form::close() !!}
							<br>
							@if ($tutor_request->tutor_contracts->count() && $tutor_request->tutor_contracts->where('tutor_decision', 1)->count())
								<br>
								{!! Form::open(['method' => 'POST', 'route' => ['send.match.reminder', 'method' => 'email', 'tutor_request_id' => $tutor_request->id], 'class' => 'email-reminder', 'id' => 'email_reminder-'.$tutor_request->id]) !!}
									<div class="btn-group pull-left">
								        <button {{-- type="submit" --}} class="btn btn-sm btn-primary" title="Email Reminder">
											<span class="glyphicon glyphicon-envelope"></span>
										</button>
								    </div>
								{!! Form::close() !!}
								
								@if ($tutor_request->sms_alert)
									
									{!! Form::open(['method' => 'POST', 'route' => ['send.match.reminder', 'method' => 'sms', 'tutor_request_id' => $tutor_request->id], 'class' => 'sms-reminder', 'id' => 'email_reminder-'.$tutor_request->id]) !!}

										<div class="btn-group pull-left" style="padding-left: 20px">
									        <button {{-- type="submit" --}} class="btn btn-sm btn-info" title="Text Reminder">
												<span class="glyphicon glyphicon-phone"></span>
											</button>
									    </div>
								    {!! Form::close() !!}
								@endif	
							@endif
							
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<table class="table">
	<thead>
		<tr>
			<th>Request Details</th>
			<th>Contracts</th>
			<th>Send To Another Tutor</th>
		</tr>
	</thead>
	<tbody>
		<col width="30%">
  		<col width="20%">
  		<col width="50%">
		@foreach ($tutor_requests as $tutor_request)
			<tr id="{{$tutor_request->id}}" class="request-details" style="display: none">
				<td>
					<h4><b>{{$tutor_request->student->full_name}} - {{$tutor_request->course}}</b></h4>
					@if ($tutor_request->requested_tutors)
						<b><h4 style="color:purple">Requested Tutors: {!!$tutor_request->requested_tutors!!}</h4></b>
					@endif

					@include('btn.student_tutor_contracts.admin.partials.tutor_request_details')
					<br><br>
					{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'unassign', 'id' => 'unassign-'.$tutor_request->id]) !!}

					    <div class="pull-left">
					        {!! Form::submit("Unassign", ['class' => 'btn btn-warning']) !!}
					    </div>

					{!! Form::close() !!}
				</td>
				<td>
					@foreach ($tutor_request->tutor_contracts as $tutor_contract)
						<b>Contract ID:</b> {{$tutor_contract->id}}

						<br>
						<b>Tutor:</b> <a href="{{ route('user.details.get', ['user_id' => $tutor_contract->tutor->id])}}" target="_blank">{{$tutor_contract->tutor->full_name}} </a>
							@if ($tutor_contract->online)
								(Online)
							@endif
							<br>
						<b>Student Rate:</b>  ${{$tutor_contract->student_rate}} / hr<br>
						<b>Tutor Rate: </b>  ${{$tutor_contract->tutor_rate}}/ hr <br>
						<b>BTN Rate: </b>  ${{$tutor_contract->student_rate - $tutor_contract->tutor_rate}} / hr <br>
						@if($tutor_request->concerns)
							<b>Tutor Concerns</b><br>
							{{$tutor_contract->concerns}} <br>
						@endif
						
						@if ($tutor_contract->tutor_decision == 1)
							<b><em style="color:darkgreen">Tutor Accepted on {{$tutor_contract->tutor_decision_date->format('m/d/Y')}}</em></b>
						@elseif($tutor_contract->tutor_decision === 0)
							<b><em style="color:darkred">Rejected on {{$tutor_contract->tutor_decision_date->format('m/d/Y')}}</em></b><br>
							{{$tutor_contract->tutor_reject_reason}}
						@else
							<b>Deadline: </b>  {!!$tutor_contract->deadline->isFuture() ? $tutor_contract->deadline->diffForHumans() : "<span style='color:red'>{$tutor_contract->deadline->diffForHumans()}</span>"!!} 
						@endif
						<br>
						@if ($tutor_contract->student_decision === 0)
							<b><span style="color:darkorange">Rejected By Student</span></b><br>
							{{$tutor_contract->student_reject_reason}}
						@elseif($tutor_contract->student_decision === null  && $tutor_contract->deadline_student !==null)
							<b>Student Deadline: </b> 
							@if ($tutor_contract->deadline_student->isPast())
								<span style="color:darkred">{{$tutor_contract->deadline_student->diffForHumans()}}</span><br>
							@else
								<span style="color:darkgreen">{{$tutor_contract->deadline_student->diffForHumans()}}</span><br>
							@endif
						@elseif($tutor_contract->student_decision === 1)
							<b><span style="color:purple">ACCEPTED By Student</span></b><br>
						@endif
						
						{!! Form::open(['method' => 'DELETE', 'route' =>['delete.tutor.contract', $tutor_contract->id], 'class' => 'delete']) !!}
						    <div class="btn-group pull-right">
						        <button type="submit" class="btn btn-sm btn-danger">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
						    </div>
						
						{!! Form::close() !!}
						<br><br><br>
					@endforeach
				</td>
				<td>
					{!! Form::open(['method' => 'POST', 'route' => ['create.tutor.contract', $tutor_request->id], 'class' => 'form-horizontal', 'id' => "form-$tutor_request->ref_id"]) !!}
					
						@include('btn.student_tutor_contracts.admin.partials.form_select_tutor_for_contract', ['i' => 0])
					
						<div class="btn-group pull-right col-md-4">
					        {!! Form::submit("Send Contract For {$tutor_request->student->first_name}", ['class' => 'btn btn-primary']) !!}
					    </div>
					
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>