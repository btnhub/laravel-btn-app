<b>ID:</b> {{$tutor_request->id}} / {{$tutor_request->ref_id}}<br>
<span style="color:darkred"><b>Requested:</b> {{$tutor_request->created_at->diffForHumans()}}</span><br>
<b style="color:darkblue">Max Rate: ${{$tutor_request->max_rate}}</b><br>
<b>Location:</b> {{$tutor_request->city ? "$tutor_request->city, $tutor_request->region" : $tutor_request->location}}
@if ($tutor_request->include_online_tutors)
	(Include Online Tutors)
@endif
<br>
<b>School:</b> {{$tutor_request->school}} <br>
{{-- <b>Course Level:</b> {{$tutor_request->level}}<br> --}}
<b>Session Time:</b> {{$tutor_request->session_time}}<br>
<b>Session Frequency:</b> {{$tutor_request->frequency}}<br>
@if ($tutor_request->concerns)
	<b>Concerns:</b> {{$tutor_request->concerns}}<br>
@endif
@if ($tutor_request->scholarship_program)
	<span style="color:darkblue"><b>Scholarship:</b> {{$tutor_request->scholarship_program}}</span><br>
@endif
<b>Start Date:</b> {{Carbon\Carbon::parse($tutor_request->start_date)->format('m/d/Y')}} ({{Carbon\Carbon::parse($tutor_request->start_date)->diffForHumans()}})<br>
<b>End Date:</b> {{Carbon\Carbon::parse($tutor_request->end_date)->format('m/d/Y')}} ({{Carbon\Carbon::parse($tutor_request->end_date)->diffForHumans()}})<br>	
<b>Marketing:</b> {{$tutor_request->student->profile->marketing}}<br>