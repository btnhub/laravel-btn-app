<?php
	//Filters tutors collection by location and return array for select field
	/*$location_id = $tutor_request->location_id;
	$tutors_array = $tutors->filter(function($tutor) use ($location_id) {
							return $tutor->locations->pluck('id')->contains($location_id) ? $tutor : null ;
						})
					->pluck('tutor_details', 'id')->toArray();

	$tutors_array = [null => "Select A $tutor_request->location Tutor"] + $tutors_array;*/
	$lat = $tutor_request->lat;
	$lng = $tutor_request->lng;

	$tutors_array = $tutors->filter(function($tutor) use ($lat, $lng) {
							return $tutor->nearest_geolocation($lat, $lng);
						})
					->pluck('tutor_details', 'id')->toArray();

	$tutors_array = [null => "Select $tutor_request->city Tutor"] + $tutors_array;

	$panel_options = [0 => 'primary', 1 => 'success', 2 => 'danger'];

	if ($tutor_request->include_online_tutors) {
		/*$online_location_id = App\Location::where('city', 'like', '%online%')->first()->id;
		$online_tutors_array = $tutors->filter(function($tutor) use ($online_location_id) 			{
							return $tutor->locations->pluck('id')->contains($online_location_id) ? $tutor : null ;
						})
					->pluck('tutor_details', 'id')->toArray();*/
		$online_tutors_array = $tutors->filter(function($tutor){
							return $tutor->profile->online;
						})
					->pluck('tutor_details', 'id')->toArray();

		$tutors_array = $tutors_array + [' ' => "-----ONLINE TUTORS-----"] + $online_tutors_array;
	}
?>

<div class="panel panel-{{$panel_options[$i]}}">
    <div class="panel-heading">Tutor {{$i+1}}</div>
    <div class="panel-body">
    	<div class="form-group{{ $errors->has('tutors.'.$i) ? ' has-error' : '' }} col-md-4">
	        {!! Form::label("tutors[$i]", "Tutor") !!}
	        {!! Form::select("tutors[$i]",$tutors_array, null, ['id' => "tutors-$tutor_request->id-$i", 'class' => 'form-control', 'onChange' => "updateTutorRate($tutor_request->id, $i);calcBtnAmt($tutor_request->id, $i)"]) !!}
	        <small class="text-danger">{{ $errors->first('tutors.'.$i) }}</small>
	    </div>
		
		<div class="form-group{{ $errors->has('student_rate.'.$i) ? ' has-error' : '' }} col-md-4">
		    {!! Form::label("student_rate[$i]", 'Student Rate ($/hr)') !!}
		    {!! Form::number("student_rate[$i]", null, ['class' => 'form-control', 'min' => 15, 'id' => "student_rate-$tutor_request->id-$i", 'onkeyup' => "updateTutorRate($tutor_request->id, $i);calcBtnAmt($tutor_request->id, $i)"]) !!}
		    <small class="text-danger">{{ $errors->first('student_rate.'.$i) }}</small>
		</div>
		<div class="form-group{{ $errors->has('tutor_rate.'.$i) ? ' has-error' : '' }} col-md-4">
		    {!! Form::label("tutor_rate[$i]", 'Tutor Rate ($/hr)') !!}
		    {!! Form::number("tutor_rate[$i]", null, ['class' => 'form-control', 'id' => "tutor_rate-$tutor_request->id-$i",'onkeyup' => "calcBtnAmt($tutor_request->id, $i)"]) !!}
		    <small class="text-danger">{{ $errors->first('tutor_rate.'.$i) }}</small>
		    <div id="tutor_fee-{{$tutor_request->id}}-{{$i}}">
		    	
		    </div>
		    <div id="btn_amt-{{$tutor_request->id}}-{{$i}}">
		    	
		    </div>
		</div>
		<div class="form-group{{ $errors->has('deadline.'.$i) ? ' has-error' : '' }} col-md-4">
		    {!! Form::label("deadline[$i]", 'Acceptance Deadline') !!}
		    {!! Form::date("deadline[$i]",Carbon\Carbon::now()->addDay(), ['class' => 'form-control', 'min' => Carbon\Carbon::now()->format('Y-m-d')]) !!}
		    <small>Default: Tomorrow @ 11pm</small><br>
		    <small class="text-danger">{{ $errors->first('deadline.'.$i) }}</small>
		</div>
		<div class="form-group{{ $errors->has('concerns.'.$i) ? ' has-error' : '' }} col-md-4">
		    {!! Form::label("concerns[$i]", 'Concerns') !!}
		    {!! Form::textarea("concerns[$i]",$tutor_request->concerns, ['class' => 'form-control', 'size' => '20x1']) !!}
		    <small class="text-danger">{{ $errors->first('concerns.'.$i) }}</small>
		</div>
		<div class="form-group{{ $errors->has('btn_percent.'.$i) ? ' has-error' : '' }} col-md-4">
		    {!! Form::label("btn_percent[$i]", 'BTN Percent') !!}
		    {!! Form::number("btn_percent[$i]", null, ['class' => 'form-control', 'readonly', 'id' => "btn_percent-$tutor_request->id-$i"]) !!}
		    <small class="text-danger">{{ $errors->first('btn_percent.'.$i) }}</small>
		</div>
	</div>
</div>