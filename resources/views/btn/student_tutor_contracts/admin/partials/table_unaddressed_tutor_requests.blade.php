<h2>Unaddressed Tutor Requests</h2>

{{$tutor_requests->count()}} Tutor Requests
<div class="row">
	<div>
		<h3>Summary</h3>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Request ID</th>
					<th>Student</th>
					<th>Details</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<col width="10%">
		  		<col width="30%">
		  		<col width="45%">
		  		<col width="15%">
				@foreach ($tutor_requests as $tutor_request)
					<tr>
						<td><a href="#{{$tutor_request->id}}" onclick="showRequestDetails({{$tutor_request->id}})">{{$tutor_request->id}}</a></td>
						<td><span style="font-size:1.25em;"><a href="{{route('user.details.get', $tutor_request->student->id)}}" target="_blank">{{$tutor_request->student->full_name}}</a>  
							
							<br>
							{{$tutor_request->course}} @ ${{$tutor_request->max_rate}}/hr</span>
							<br>
							<span style="color:darkred;">
								<b>{{$tutor_request->city ? "$tutor_request->city, $tutor_request->region" : $tutor_request->location}}
								@if ($tutor_request->include_online_tutors)
									(Include Online Tutors)
								@endif
								</b>
							</span>
							<br>
							<b>Created: {{$tutor_request->created_at->format('m/d/Y')}} ({{$tutor_request->created_at->diffForHumans()}})</b>

							<br><br>
							<a href="#duplicate-{{$tutor_request->id}}" onclick="showDuplicateForm({{$tutor_request->id}})">Duplicate Request</a>
							<div class="row duplicate" id="duplicate-{{$tutor_request->id}}" style="display: none">

								<div class="col-sm-12">
									{!! Form::open(['method' => 'POST', 'route' => ['duplicate.tutor.request', $tutor_request->id], 'class' => 'form-horizontal']) !!}
									
									    <div class="form-group{{ $errors->has('edited_course') ? ' has-error' : '' }}">
									        {!! Form::label('edited_course', 'Edited First Course') !!}
									        {!! Form::text('edited_course',$tutor_request->course, ['class' => 'form-control', 'required' => 'required']) !!}
									        <small class="text-danger">{{ $errors->first('edited_course') }}</small>
									    </div>
										
										<div class="form-group{{ $errors->has('new_course') ? ' has-error' : '' }}">
										    {!! Form::label('new_course', 'New Request Course') !!}
										    {!! Form::text('new_course', null, ['class' => 'form-control', 'required' => 'required']) !!}
										    <small class="text-danger">{{ $errors->first('new_course') }}</small>
										</div>
									    <div class="btn-group pull-left">
									        {!! Form::submit("Duplicate", ['class' => 'btn btn-success']) !!}
									    </div>
									{!! Form::close() !!}
								</div>
							</div>
						</td>
						<td>
							{{$tutor_request->session_time}} - {{$tutor_request->frequency}}<br>
							@if ($tutor_request->requested_tutors)
								<b style="color:purple">Requested Tutors:</b> {!!$tutor_request->requested_tutors!!}
								<br>
							@endif
							@if ($tutor_request->concerns)
								<b>Concerns:</b> {{$tutor_request->concerns}}	
								<br>
							@endif
						</td>
						<td>
							{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'unassign', 'id' => 'unassign-'.$tutor_request->id]) !!}

							    <div class="btn-group pull-right">
							        {!! Form::submit("Unassign", ['class' => 'btn btn-warning']) !!}
							    </div>

							{!! Form::close() !!}

							{!! Form::open(['method' => 'DELETE', 'route' =>['delete.tutor.request', $tutor_request->id], 'class' => 'delete']) !!}
							    <div class="btn-group pull-left">
							        <button type="submit" class="btn btn-sm btn-danger">
										<span class="glyphicon glyphicon-trash"></span>
									</button>
							    </div>
							
							{!! Form::close() !!}
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@foreach ($tutor_requests as $tutor_request)

<div class="row request-details" id="{{$tutor_request->id}}" style="display: none">

	<div class="col-sm-12">
	
		<h4><b>{{$tutor_request->student->user_name}} - {{$tutor_request->course}} - ${{$tutor_request->max_rate}}/hr</b></h4>
		@if ($tutor_request->requested_tutors)
			<b><h4 style="color:purple">Requested Tutors: {!!$tutor_request->requested_tutors!!}</h4></b>
		@endif
		<div class="col-md-4">
			
			@include('btn.student_tutor_contracts.admin.partials.tutor_request_details')

			<br><br>
			{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'unassign', 'id' => 'unassign-'.$tutor_request->id]) !!}

			    <div class="btn-group pull-right">
			        {!! Form::submit("Unassign", ['class' => 'btn btn-warning']) !!}
			    </div>

			{!! Form::close() !!}

			{!! Form::open(['method' => 'DELETE', 'route' =>['delete.tutor.request', $tutor_request->id], 'class' => 'delete']) !!}
			    <div class="btn-group pull-left">
			        <button type="submit" class="btn btn-sm btn-danger">
						<span class="glyphicon glyphicon-trash"></span>
					</button>
			    </div>
			
			{!! Form::close() !!}
		</div>
		<div class="col-md-8">
			<div class="row">
				{!! Form::open(['method' => 'POST', 'route' => ['create.tutor.contract', $tutor_request->id], 'class' => 'form-horizontal', 'id' => "form-$tutor_request->ref_id"]) !!}
					
					<?php 
						$panel_options = [0 => 'primary', 1 => 'success', 2 => 'danger']
					?>
				    @for ($i = 0; $i < 3; $i++)
					    @include('btn.student_tutor_contracts.admin.partials.form_select_tutor_for_contract')
				    @endfor
				    
				    <div class="btn-group pull-right col-md-4">
				        {!! Form::submit("Notify Tutors For {$tutor_request->student->first_name}", ['class' => 'btn btn-success']) !!}
				    </div>
				
				{!! Form::close() !!}	
			</div>
			
		</div>
		<br><br><br>
	
	</div>
</div>
@endforeach	