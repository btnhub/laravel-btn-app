@extends('layouts.josh-front.default', ['page_title' => "Tutor Requests"])

@section('title', 'Tutor Requests')
@section('content')
	
	<a href="{{ route('assignments.index') }}">Back to all assignments</a>

  <p>{{$tutor_requests_count}} Requests</p>

	<ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Unaddressed Requests ({{$unaddressed_tutor_requests->count()}})</a></li>
        <li><a data-toggle="tab" href="#menu1">Pending Tutor Response ({{$pending_requests_tutors->count()}})</a></li>

        <li><a data-toggle="tab" href="#menu2">Pending Student Response ({{$pending_requests_student->count()}})</a></li>
        <li><a data-toggle="tab" href="#menu3">All Tutors Declined / Missed Deadline ({{$missed_tutor_requests->count()}})</a></li>
        <li><a data-toggle="tab" href="#menu4">Student Declined Contracts ({{$student_declined_requests->count()}})</a></li>
      </ul>

      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          @include('btn.student_tutor_contracts.admin.partials.table_unaddressed_tutor_requests', ['tutor_requests' => $unaddressed_tutor_requests])
        </div>
        <div id="menu1" class="tab-pane fade">
 			    <h3>Pending Tutor Response</h3>
          <p>Tutor contracts without a response from a tutor (before deadline).</p>
          @include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $pending_requests_tutors])   
        </div>
       	<div id="menu2" class="tab-pane fade">
          <h3>Pending Student Response</h3>
          <p>Tutor responses awaiting student response (before student deadline).</p>
          @include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $pending_requests_student])    
       	</div>
       	<div id="menu3" class="tab-pane fade">
       	  <h3>Declined By Tutor</h3>
          <p>Tutor requests where all the contracted tutors declined or missed the deadline.</p>
          @include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $missed_tutor_requests])	 
        </div> 
        <div id="menu4" class="tab-pane fade">
          <h3>Declined By Student</h3>
          <p>Tutor requests where all contracts were declined by the student or the student missed the response deadline.</p>
          @include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $student_declined_requests])  
        </div> 
      </div>

@endsection

@push('scripts')
	<script>
	    $(".unassign").on("submit", function(){
	        return confirm("Are you sure you want to unassign this request?");
	    });

	    $(".delete").on("submit", function(){
	        return confirm("Are you sure you want to delete this contract?");
	    });

      $(".email-reminder").on("submit", function(){
          return confirm("Are you sure you want to send an email reminder?");
      });

      $(".sms-reminder").on("submit", function(){
          return confirm("Are you sure you want to send a TEXT reminder?");
      });
	</script>
@endpush

@push('scripts')
  <script>
    function updateTutorRate(tutor_request_id, iteration){
      
      var tutors_array_id = "tutors-"+ tutor_request_id + "-"+iteration;
      var student_rate_id = "student_rate-"+ tutor_request_id + "-"+iteration;
      var tutor_rate_id = "tutor_rate-"+ tutor_request_id + "-"+iteration;
      var tutor_fee_id = "tutor_fee-"+ tutor_request_id + "-"+iteration;

      var tutor_id = document.getElementById(tutors_array_id).value;
      var student_rate = document.getElementById(student_rate_id).value;

      
      var tutor_fees = {!!$tutor_fees!!};
    
      var tutor_rate = student_rate * (1- tutor_fees[tutor_id]);
      document.getElementById(tutor_rate_id).value = tutor_rate.toFixed(2); 
      document.getElementById(tutor_fee_id).innerHTML ="<small>Tutor Fee: "+ tutor_fees[tutor_id] * 100 +"%</small>";     
    }
    
    function calcBtnAmt(tutor_request_id, iteration){
      var student_rate_id = "student_rate-"+ tutor_request_id + "-"+iteration;
      var tutor_rate_id = "tutor_rate-"+ tutor_request_id + "-"+iteration;
      var btn_amt_id = "btn_amt-"+ tutor_request_id + "-"+iteration;
      var btn_percent_id = "btn_percent-"+ tutor_request_id + "-"+iteration;

      var student_rate = document.getElementById(student_rate_id).value;
      var tutor_rate = document.getElementById(tutor_rate_id).value;

      var btn_amt = student_rate - tutor_rate;
      var btn_fee =  btn_amt / student_rate;

      document.getElementById(btn_percent_id).value = btn_fee.toFixed(2);

      if (btn_fee >= {{$min_btn_percent}}) {
        document.getElementById(btn_amt_id).innerHTML ="<b><span style='color:darkgreen'>BTN Amt: $"+ btn_amt.toFixed(2) +"</span>, Fee: "+ btn_fee.toFixed(2) * 100 +"%</b></small>";  
      }
      else{
        document.getElementById(btn_amt_id).innerHTML ="<b><span style='color:red'>BTN Amt: $"+ btn_amt.toFixed(2) +"</span>, Fee: "+ btn_fee.toFixed(2) * 100 +"%</b></small>";
      }
      
    }

    function showRequestDetails(row_id){
      var all_details = document.getElementsByClassName('request-details');
      var specific_row = document.getElementById(row_id);
      for (var i = 0; i < all_details.length; i++) {
        all_details[i].style.display = "none";
      }
      specific_row.style.display = "table-row";
    }
  </script>
@endpush 