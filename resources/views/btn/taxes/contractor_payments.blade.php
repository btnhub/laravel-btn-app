@extends('layouts.josh-front.default')

@section('title')
	BuffTutor Payments
@stop

@section('content')

{{--
<h2>Tax Documents</h2>
<b>Total Earned:</b> ${{$last_year_payments->sum('amount_paid')}}
	@if ($last_year_payments->sum('amount_paid') < 600)
		<p>A 1099-MISC will not be created for you as you did not earn more than $600 from us directly.  This total does not include instant payments from your student using Stripe.  Refer to Stripe's Dashboard for tax documents related to those transactions. </p>
		<p>Here are <a href="https://support.stripe.com/questions/will-i-receive-a-1099-k-and-what-do-i-do-with-it" target="_blank">Stripe's criteria</a> for generating a 1099-K</p>
		<p>All of your payments are summarized below.</p>
	@else
		Please submit fill out an submit the following W-9.  We will then create a 1099-MISC for you.

	@endif
--}}

<h2>Payments</h2>
Listed below are payments submitted directly from BuffTutor/RamTutor to you.  These payments are either not listed on your Stripe DashBoard or will not be included when Stripe creates tax documents for you.  <b>Payments that were instantly transferred to you using Stripe are not included below. </b> Refer to your Stripe Dashboard for a list of payments instantly transferred and for tax documents for those transactions.

<div class="container">
	<div class="row">

		<div class="col-md-6">
			<h3>Year To Date Payments</h3>

			@if ($ytd_payments->count() == 0)
				<em>No Payments This Year</em>

			@else
				<b>Total Earned:</b> ${{$ytd_payments->sum('amount_paid')}}
				<table class="table">
					<thead>
						<tr>
							<th>Payment Date</th>
							<th>Amount Paid</th>
							<th>Method</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($ytd_payments as $payment)
							<tr>
								<td>{{date('m/d/Y', strtotime($payment->created_at))}}</td>
								<td>${{$payment->amount_paid}}</td>
								<td>{{$payment->method}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@endif
		</div>
		
		<div class="col-md-6">
			<h3>Last Year's Payments</h3>
			@if ($last_year_payments->count() == 0)
				<em>No Payments Last Year</em>

			@else
				<b>Total Earned:</b> ${{$last_year_payments->sum('amount_paid')}}
				<table class="table">
					<thead>
						<tr>
							<th>Payment Date</th>
							<th>Amount Paid</th>
							<th>Method</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($last_year_payments as $previous_payment)
							<tr>
								<td>{{date('m/d/Y', strtotime($previous_payment->created_at))}}</td>
								<td>${{$previous_payment->amount_paid}}</td>
								<td>{{$previous_payment->method}}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@endif
		</div>

	</div>
</div>


@stop