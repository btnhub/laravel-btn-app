@extends('layouts.josh-front.default')

@section('title')
	Taxes
@stop

@section('content')

	<h2>Tax Documents</h2>
	<h3>YTD ({{date('Y')}})</h3>
	<b>{{$ytd_payments->groupBy('contractor_id')->count()}} Contractors </b>

	<table class="table">
		<thead>
			<tr>
				<th>Contractor ID</th>
				<th>Contractor Name</th>
				<th>SSN Last 4</th>
				<th>YTD Earnings ({{date('Y')}})</th>
				<th>W9 Submitted</th>
				<th>1099 Created</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($contractors as $contractor)
				@if ($ytd_payments->where('contractor_id', $contractor->id)->sum('amount_paid') > 0)
					<tr>
						<td>{{$contractor->id}}</td>
						<td>{{$contractor->full_name}}</td>
						<td></td>
						<td>${{$ytd_payments->where('contractor_id', $contractor->id)->sum('amount_paid')}}</td>
						<td>
							@if ($ytd_payments->where('contractor_id', $contractor->id)->sum('amount_paid') < 600)
								N / A
							
							@else
								<span style="color:green">Y</span> / <span style="color:red">N</span>
							@endif
						</td>
						<td>
							@if ($ytd_payments->where('contractor_id', $contractor->id)->sum('amount_paid') < 600)
								N / A
							
							@else
								<span style="color:red">Year (e.g. 2016-1099MISC)</span>

							@endif
						</td>
					</tr>	
				@endif
			@endforeach
			
		</tbody>
	</table>

	<hr>

	<h3>Previous Year ({{date('Y', strtotime($last_year_payments->first()->created_at))}})</h3>
	<b>{{$last_year_payments->groupBy('contractor_id')->count()}} Contractors</b>
	<table class="table">
		<thead>
			<tr>
				<th>Contractor ID</th>
				<th>Contractor Name</th>
				<th>SSN Last 4</th>
				<th>Last Year Earnings ({{date('Y', strtotime($last_year_payments->first()->created_at))}})</th>
				<th>W9 Submitted</th>
				<th>1099 Created</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($contractors as $contractor)
				@if ($last_year_payments->where('contractor_id', $contractor->id)->sum('amount_paid') > 0)
					<tr>
						<td>{{$contractor->id}}</td>
						<td>{{$contractor->full_name}}</td>
						<td></td>
						<td>${{$last_year_payments->where('contractor_id', $contractor->id)->sum('amount_paid')}}</td>
						<td>
							@if ($last_year_payments->where('contractor_id', $contractor->id)->sum('amount_paid') < 600)
								N / A
							
							@else
								<span style="color:green">Y</span> / <span style="color:red">N</span>
							@endif
						</td>
						<td>
							@if ($last_year_payments->where('contractor_id', $contractor->id)->sum('amount_paid') < 600)
								N / A
							
							@else
								<span style="color:red">Year (e.g. 2015-1099MISC)</span>

							@endif
						</td>
					</tr>
				@endif
			@endforeach
			
		</tbody>
	</table>

@stop