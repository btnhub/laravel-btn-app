<?php
	//Filters tutors collection by location and return array for select field
	$tutors = $tutors->filter(function($tutor) use ($location_id) {
							return $tutor->locations->pluck('id')->contains($location_id) ? $tutor : null ;
						})
					->pluck('full_name', 'id')->toArray();
	$tutors = [null => "Select A $city Tutor"] + $tutors;
?>
<div class="container-fluid">	
	{!! Form::open(['method' => 'PATCH', 'route' => ['make.assignment', 'tutor_request_id' => $tutor_request->id], 'class' => 'form-horizontal', 'id' => $tutor_request->id]) !!}
			<div class="form-group{{ $errors->has('tutor_id') ? ' has-error' : '' }}">
			    {!! Form::label('tutor_id', 'Select A Tutor') !!}
			    {!! Form::select('tutor_id',$tutors, null, ['id' => 'tutor_id', 'class' => 'form-control', 'required']) !!}
			    <small class="text-danger">{{ $errors->first('tutor_id') }}</small>
			</div>

	    <div class="btn-group pull-left">      
	        {!! Form::submit("Make Assignment", ['class' => 'btn btn-primary form-control']) !!}
		</div>
	{!! Form::close() !!}
</div>