@extends('layouts.josh-front.default', ['page_title' => "Declined Tutor Requests"])

@section('content')
	<a href="{{ route('assignments.index') }}">Back to all assignments</a>
	
	<h2>Declined Requests</h2>
	@include('btn.assign.partials.table_declined')

@stop