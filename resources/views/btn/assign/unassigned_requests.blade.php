@extends('layouts.josh-front.default', ['page_title' => "Unassigned Tutor Requests"])

@section('content')
	<a href="{{ route('assignments.index') }}">Back to all assignments</a>
	
	<h2>Unassigned Requests</h2>
	@include('btn.assign.partials.table_unassigned')

@stop