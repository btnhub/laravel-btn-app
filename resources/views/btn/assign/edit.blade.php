@extends('layouts.josh-front.default')

@section('content')
<h1>Edit Assignment </h1>
<p class="lead">Edit this assignment below. <a href="{{ route('assignments.index') }}">Go back to all assignments.</a></p>
<hr>
		<div class="container-fluid">	
			
			@include('btn.assign.partials.assign_details')
			
		<hr>
		{!! Form::model($assignment, ['method' => 'PATCH', 'action' => ['AssignmentController@update', $assignment->id], 'class' => 'form-horizontal']) !!}

		<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
		    {!! Form::label('end_date', 'End Date') !!}
		    {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
		    <small class="text-danger">{{ $errors->first('end_date') }}</small>
		</div>

   			@include ('btn.assign.partials.form_assign', ['submitBtnText' =>'Update Assignment'])
		{!! Form::close() !!}

		</div>
@stop
