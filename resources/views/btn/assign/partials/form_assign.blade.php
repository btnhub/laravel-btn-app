	<div class="form-group{{ $errors->has('student_id') ? ' has-error' : '' }}">
	    {!! Form::label('student_id', 'Students') !!}
	    {!! Form::select('student_id', $students, null, ['id' => 'student_id', 'class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('student_id') }}</small>
	</div>

	<div class="form-group{{ $errors->has('tutor_id') ? ' has-error' : '' }}">
	    {!! Form::label('tutor_id', 'Select A Tutor') !!}
	    {!! Form::select('tutor_id',$tutors, null, ['id' => 'tutor_id', 'class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('tutor_id') }}</small>
	</div>

	<div class="radio{{ $errors->has('location_id') ? ' has-error' : '' }}">
	    @foreach ($locations as $id => $city)
		    <label>
		        {!! Form::radio('location_id', $id,  null) !!} {{$city}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		    </label>
	    @endforeach
	    <small class="text-danger">{{ $errors->first('location_id') }}</small>
	</div>


	<div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
	    {!! Form::label('course', 'Course') !!}
	    {!! Form::text('course', null, ['class' => 'form-control']) !!}
	    <small class="text-danger">{{ $errors->first('course') }}</small>
	</div>

    <div class="btn-group pull-left">      
        {!! Form::submit($submitBtnText, ['class' => 'btn btn-primary form-control']) !!}
    </div>

