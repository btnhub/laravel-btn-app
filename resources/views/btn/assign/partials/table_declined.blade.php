{{-- For Admin: Lists all declined requests --}}
<div class="table-responsive">
	<p>Listed below are direct tutor requests that were declined by the requested tutor.</p>
	There Are {{ $declined_requests->count() }} Declined {{ str_plural('Request', $declined_requests->count()) }}
	
	@foreach ($locations as $location_id => $city)
		<h2>{{$city}}</h2>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Student Name</th>
					<th>Requested Tutor</th>
					<th>Course</th>
					<th>Request Date</th>
					<th>Declined Date</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($declined_requests as $declined)
				@if ($declined->location_id == $location_id && !$declined->duplicateRequest())
					<tr>
						<td>
						<a href="{{ route('tutor_request.show', $declined->ref_id) }}">{{$declined->id}}</a>
						</td>
						<td>{{date('m/d/Y', strtotime($declined->start_date))}}</td>
						<td>{{date('m/d/Y', strtotime($declined->end_date))}}</td>
						<td>
							@if ($declined->student)
								{{-- Make sure user hasn't been deleted --}}						
								{{$declined->student->first_name}} {{$declined->student->last_name}}
									@if ($declined->child_name)
									 	<br>
									 	{{ $declined->child_name }}
									@endif
								
							@endif
						</td>						
						<td>
							@if ($declined->student)
								{{-- Make sure user hasn't been deleted --}}						
								{{$declined->requested_tutor->full_name ?? ''}}
							@endif
						</td>
						<td>{{$declined->course}}</td>
						<td>{{$declined->created_at->toDateString()}}</td>
						<td>{{$declined->updated_at->toDateString()}}</td>
					</tr>
				@endif
			@endforeach

			</tbody>
		</table>
		<hr>	
	@endforeach
</div>	
