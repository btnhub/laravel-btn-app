{{-- For Admin: Lists all active assignments --}}
<div class="table-responsive">
	<p><b>There are {{ $assignments->count() }} Active {{ str_plural('Assignment', $assignments->count()) }}, {{ $assignments->where('declined', 1)->count() }} Declined {{ str_plural('Assignment', $assignments->where('declined', 1)->count()) }}</b></p>
	<p>Assignments declined by the student are highlighted in red.</p>
	<ul class="nav nav-tabs">
	  @foreach ($locations as $array_key => $location)
	 	 <li {{($array_key == 0)? 'class=active':""}}><a data-toggle="tab" href="#{{$location->id}}" >{{$location->city}} - {{$assignments->filter(function ($key, $value) use ($location) { if($key->location_id == $location->id) return $key;})->count()}}</a></li>
	  @endforeach
	  @if ($loc_without_assign->count())
	  	<li><a data-toggle="tab" href="#loc_without_assign" >Locations w/o Assign - {{$loc_without_assign->count()}} </a></li>
	  @endif
	</ul>

	<div class="tab-content">
		<?php $total_prepaid = 0;?>
		@foreach ($locations as $key => $location)
			<?php $city_prepaid = 0;?>

			<div id="{{$location->id}}" class="tab-pane {{($key == 0)? 'fade in active':'fade'}}">
				<h2>{{$location->city}}</h2>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Student Name</th>
							<th>Tutor Name</th>
							<th>Course</th>
							<th># Of Sessions</th>
							<th>Rate</th>
							<th>Match Created By</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($assignments as $assignment)
						@if ($assignment->location_id == $location->id)
							<tr @if ($assignment->declined == 1)
									style = "color:red"
								@endif
							>
								<td>
								<a href="{{ route('assignments.show', $assignment->ref_id) }}">{{$assignment->id}}</a>
								</td>
								<td>{{date('m/d/Y', strtotime($assignment->start_date))}}<br>
								</td>
								<td>{{date('m/d/Y', strtotime($assignment->end_date))}}
									<br>
									@if ($assignment->tutorSessions->count() && $assignment->tutorSessions->sortbydesc('id')->first()->created_at->diffInDays() > 10)
										<small style="color:darkred">Last Session: 
											<br>
											{{$assignment->tutorSessions->sortbydesc('id')->first()->created_at->format('m/d/Y')}}</small>
									@endif
									
								</td>
								<td>
									@if ($assignment->student)
										{{-- Make sure user hasn't been deleted --}}

										@if (!$assignment->student->customer)
											<span style="color:darkred">{{$assignment->student->full_name}}</span>
										@else
											{{$assignment->student->full_name}}
										@endif					
											@if ($assignment->child_name)
											 	<br>
											 	{{ $assignment->child_name }}
											@endif
										
									@endif
								</td>						
								<td>
									@if ($assignment->tutor)
										{{-- Make sure user hasn't been deleted --}}						
										{{$assignment->tutor->full_name ?? ''}}
										{!!$assignment->online ? "<br><em>(Online)</em>" : null!!}
									@endif
								</td>
								<td>{{$assignment->course}}
									@if ($assignment->prepaid_balance())
										<br> 
										<span style="color:green;font-weight: bold">Prepaid Balance: ${{$assignment->prepaid_balance()}}</span>
										<?php $city_prepaid += $assignment->prepaid_balance(); ?>
									@endif
								</td>
								<td>{{$assignment->tutorSessions()->count()}}</td>
								<td>
									@if ($assignment->tutorContract->student_rate)
										<?php
											$student_rate = $assignment->tutorContract->student_rate;
											$tutor_rate = $assignment->tutorContract->tutor_rate;
											$btn_rate = $student_rate - $tutor_rate;
											$btn_percent = $btn_rate / $student_rate;
										?>
										<b>Student:</b> ${{number_format($student_rate) }}/hr<br>
										<b>Tutor:</b> ${{number_format($tutor_rate) }}/hr<br>
										<b>BTN:</b> ${{number_format($btn_rate) }}/hr ({{number_format($btn_percent * 100)}}%)<br>
									@endif
								</td>
								<td>
									{{App\User::find($assignment->assigned_by)->fullName}}
									<br>
									<small>{{$assignment->created_at->format('m/d/Y')}}</small>
								</td>
							</tr>
						@endif
					@endforeach

					</tbody>
				</table>
				@if ($city_prepaid > 0)
					<?php $total_prepaid += $city_prepaid; ?>
					<h3 style="color:darkgreen;">{{$location->city}} Prepaid: ${{$city_prepaid}}</h3>

				@endif
			</div>
		@endforeach
		@if ($loc_without_assign->count())
		  	<div id="loc_without_assign" class="tab-pane">
		  		<h2>Locations w/o Assignments</h2>
		  		@foreach ($loc_without_assign as $loc)
					<p style="padding-left:20px">{{$loc->city}} - {{$loc->active_tutors_count}} Tutors</p>
				@endforeach
		  	</div>
		  @endif
		<h3 style="color:blue;">Total Prepaid: ${{$total_prepaid}}</h3>
	</div>
</div>	
