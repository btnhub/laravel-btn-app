<h3>Assignment Details </h3>
<b>Ref #:</b> {{ $assignment->ref_id}}<br>
<b>Start Date:</b> {{ $assignment->start_date }}<br>
<b>End Date:</b> {{ $assignment->end_date }}<br>
<b>Student:</b> {{ $assignment->student->full_name ?? ''}}<br>
<b>Tutor:</b> {{ $assignment->tutor->full_name ?? ''}}<br>
<b>Course:</b> {{ $assignment->course }}<br>