{{-- For Admin: Lists all unassigned requests --}}
<div class="table-responsive">
	There Are {{ $unassigned_requests->count() }} Unassigned {{ str_plural('Request', $unassigned_requests->count()) }}
	
	@foreach ($locations as $location_id => $city)
		<h2>{{$city}}</h2>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Student Name</th>
					<th>Course</th>
					<th>Details</th>
					<th>Request Date</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($unassigned_requests as $unassigned)
				@if ($unassigned->location_id == $location_id && !$unassigned->duplicateRequest())
					<tr>
						<td>
						{{-- <a href="{{ route('tutor_request.show', $unassigned->ref_id) }}"> --}}{{$unassigned->id}}{{-- </a> --}}
						</td>
						<td>{{date('m/d/Y', strtotime($unassigned->start_date))}}</td>
						<td>{{date('m/d/Y', strtotime($unassigned->end_date))}}</td>
						<td>
							@if ($unassigned->student)
								{{-- Make sure user hasn't been deleted --}}						
								<a href="{{route('user.details.get', $unassigned->student->id)}}" target="_blank">{{$unassigned->student->full_name}}</a>  								
							@endif
						</td>						
						<td>
							{{$unassigned->course}} - ${{$unassigned->max_rate}}/hr<br>							
						</td>
						<td>
							{{$unassigned->session_time}}<br>
							{{$unassigned->frequency}}<br>
							{{$unassigned->requested_tutors}}
						</td>
						<td>{{$unassigned->created_at->toDateString()}}</td>
					</tr>
				@endif
			@endforeach

			</tbody>
		</table>
		<hr>	
	@endforeach
</div>	
