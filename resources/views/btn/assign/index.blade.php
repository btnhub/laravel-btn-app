@extends('layouts.josh-front.default', ['page_title' => "Tutor Assignments"])

@section('title', 'Assignments')

@section('content')
	<h1>List of All Assignments</h1>
	{{-- <h4><a href="{{route('assignments.create')}}">Unaddressed Tutor Requests</a></h4> --}}
	<h5 {{-- align="right" --}}>
		<a href="{{route('unassigned.requests')}}">Unassigned Tutor Requests</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		{{-- <a href="{{route('declined.requests')}}">Declined Tutor Requests</a> --}}
	</h5>

	
	@include('btn.assign.partials.table_admin')
	{{-- @include('btn.assign.partials.table_students')--}}
@stop