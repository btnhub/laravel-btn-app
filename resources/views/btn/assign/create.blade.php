@extends('layouts.josh-front.default')

@section('content')
	<a href="{{ route('assignments.index') }}">Back to all assignments</a>
	<h2>Unaddressed Tutor Requests</h2>
	{{$tutor_requests->count()}} Unaddressed Tutor Requests
<ul class="nav nav-tabs">
  @foreach ($locations as $location_id => $city)
 	 <li {{($location_id ==1)? 'class=active':""}}><a data-toggle="tab" href="#{{$location_id}}" >{{$city}} - {{$tutor_requests->filter(function ($key, $value) use ($location_id) { if($key->location_id == $location_id && !$key->duplicateRequest() && strpos($key->admin_notes, 'follow') === false) return $key;})->count()}}</a></li>
  @endforeach
</ul>

<div class="tab-content">
  @foreach ($locations as $location_id => $city)
  
  <div id="{{$location_id}}" class="tab-pane {{($location_id ==1)? 'fade in active':'fade'}}">
    <h3>{{$tutor_requests->filter(function ($key, $value) use ($location_id) { if($key->location_id == $location_id && !$key->duplicateRequest() && strpos($key->admin_notes, 'follow') === false) return $key;})->count()}} {{$city}} Tutor Requests</h3>

    <h4>{{$tutor_requests->filter(function ($key, $value) use ($location_id) { if($key->location_id == $location_id && $key->tutor_proposal->count() == 0 && !$key->requested_tutor) return $key;})->count()}} Requests Without Proposals</h4>

    <table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Student Name</th>
				<th>Course</th>
				<th>Max Rate</th>
				<th>Details</th>
				<th>Make Assignment</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($tutor_requests as $tutor_request)
			@if($tutor_request->tutor_proposal->count() == 0 && !$tutor_request->requested_tutor)
				@if ($tutor_request->location_id == $location_id)
					<tr>
						<td>
							<a href="{{route('tutor_request.show', ['ref_id' => $tutor_request->ref_id])}}">{{$tutor_request->id}}</a>
						</td>
						<td>
							{{$tutor_request->created_at->format('m/d/Y')}} 
							<br>({{$tutor_request->created_at->diffForHumans()}})
							<br>{!!$tutor_request->mkt_visibility ? "<span style='color:green'>Mkt Visible</span>" : "Mkt Hidden"!!}
						</td>					
							<td>
								<a href="{{ route('user.details.get', ['user_id' => $tutor_request->student->id])}}" target="_blank">
									{{$tutor_request->student->full_name}}
								</a>
								@if ($tutor_request->child_name)
								 	<br>
								 	{{ $tutor_request->child_name }}
								@endif
								<br>
								{{$tutor_request->student->roles->implode('role', ', ')}}
								<br>
								<b>Balance:</b> {!!($tutor_request->student->studentBalance() < 0)?"<span style='color:red'>$".$tutor_request->student->studentBalance()."</span>" :"$".$tutor_request->student->studentBalance()!!}
							</td>
						
						<td>
							{{$tutor_request->course}}
							@if ($tutor_request->requested_tutor)
								<br><b>Requested Tutor:</b> {{$tutor_request->requested_tutor->full_name}}
							@endif
						</td>
						<td>${{$tutor_request->max_rate}}</td>
						<td class="col-md-4">
							{{$tutor_request->frequency}}<br>
							{{$tutor_request->session_time}}<br>
							@if ($tutor_request->scholarship_program)
								<b>Scholarship:</b> {{$tutor_request->scholarship_program}}
								<br>
							@endif
							{{$tutor_request->concerns}}
						</td>
						<td class="col-md-3">
							{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'form-horizontal', 'id' => 'unassign-'.$tutor_request->id]) !!}

							    <div class="pull-right">
							        {!! Form::submit("Unassign", ['class' => 'btn btn-danger']) !!}
							    </div>

							{!! Form::close() !!}
							
							<a data-toggle="collapse" href="#tutor_request_{{$tutor_request->id}}" aria-expanded="false" aria-controls="tutor_request_{{$tutor_request->id}}"><b>Make Assignment</b></a>
							

							<div id="tutor_request_{{$tutor_request->id}}" class="collapse">
								@include('btn.assign.form_create_assignment')
							</div>
						</td>	
					</tr>
				@endif
			@endif
		@endforeach
		</tbody>
	</table>

	<hr>

    <h4>{{$tutor_requests->filter(function ($key, $value) use ($location_id) { if($key->location_id == $location_id && $key->tutor_proposal->count() == 0 && $key->requested_tutor && !$key->duplicateRequest() && strpos($key->admin_notes, 'follow') === false) return $key;})->count()}} Direct Tutor Requests</h4>
    <table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Student Name</th>
				<th>Course</th>
				<th>Max Rate</th>
				<th>Details</th>
				<th>Make Assignment</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($tutor_requests as $tutor_request)
			@if($tutor_request->tutor_proposal->count() == 0 && $tutor_request->requested_tutor && !$tutor_request->duplicateRequest() && strpos($tutor_request->admin_notes, 'follow') === false)
				@if ($tutor_request->location_id == $location_id)
					<tr>
						<td>
							<a href="{{route('tutor_request.show', ['ref_id' => $tutor_request->ref_id])}}">{{$tutor_request->id}}</a>
						</td>
						<td>
							{{$tutor_request->created_at->format('m/d/Y')}} 
							<br>({{$tutor_request->created_at->diffForHumans()}})
							<br>{!!$tutor_request->mkt_visibility ? "<span style='color:green'>Mkt Visible</span>" : "Mkt Hidden"!!}
						</td>					
							<td>
								<a href="{{ route('user.details.get', ['user_id' => $tutor_request->student->id])}}" target="_blank">
									{{$tutor_request->student->full_name}}
								</a>
								@if ($tutor_request->child_name)
								 	<br>
								 	{{ $tutor_request->child_name }}
								@endif
								<br>
								{{$tutor_request->student->roles->implode('role', ', ')}}
								<br>
								<b>Balance:</b> {!!($tutor_request->student->studentBalance() < 0)?"<span style='color:red'>$".$tutor_request->student->studentBalance()."</span>" :"$".$tutor_request->student->studentBalance()!!}
							</td>
						
						<td>
							{{$tutor_request->course}}
							@if ($tutor_request->requested_tutor)
								<br><b>Requested Tutor:</b> {{$tutor_request->requested_tutor->full_name}}
							@endif
						</td>
						<td>${{$tutor_request->max_rate}}</td>
						<td class="col-md-4">
							{{$tutor_request->frequency}}<br>
							{{$tutor_request->session_time}}<br>
							@if ($tutor_request->scholarship_program)
								<b>Scholarship:</b> {{$tutor_request->scholarship_program}}
								<br>
							@endif
							{{$tutor_request->concerns}}
						</td>
						<td class="col-md-3">
							{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'form-horizontal', 'id' => 'unassign-'.$tutor_request->id]) !!}

							    <div class="pull-right">
							        {!! Form::submit("Unassign", ['class' => 'btn btn-danger']) !!}
							    </div>

							{!! Form::close() !!}
							
							<a data-toggle="collapse" href="#tutor_request_{{$tutor_request->id}}" aria-expanded="false" aria-controls="tutor_request_{{$tutor_request->id}}"><b>Make Assignment</b></a>
							

							<div id="tutor_request_{{$tutor_request->id}}" class="collapse">
								@include('btn.assign.form_create_assignment')				
							</div>
						</td>	
					</tr>
				@endif
			@endif
		@endforeach
		</tbody>
	</table>

	<hr>
    <h4>{{$tutor_requests->filter(function ($key, $value) use ($location_id) { if($key->location_id == $location_id && $key->tutor_proposal->count() > 0 && !$key->duplicateRequest() && strpos($key->admin_notes, 'follow') === false) return $key;})->count()}} Proposals Submitted</h4>
    <table class="table table-striped table-hover">
		<thead>
			<tr>
				<th>ID</th>
				<th>Date</th>
				<th>Student Name</th>
				<th>Course</th>
				<th>Max Rate</th>
				<th>Details</th>
				<th>Make Assignment</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($tutor_requests as $tutor_request)
			@if($tutor_request->tutor_proposal->count() > 0 && !$tutor_request->duplicateRequest() && strpos($tutor_request->admin_notes, 'follow') === false)
				@if ($tutor_request->location_id == $location_id)
					<tr>
						<td>
							<a href="{{route('tutor_request.show', ['ref_id' => $tutor_request->ref_id])}}">{{$tutor_request->id}}</a>
						</td>
						<td>
							{{$tutor_request->created_at->format('m/d/Y')}} 
							<br>({{$tutor_request->created_at->diffForHumans()}})
							<br>{!!$tutor_request->mkt_visibility ? "<span style='color:green'>Mkt Visible</span>" : "Mkt Hidden"!!}
						</td>					
							<td>
								<a href="{{ route('user.details.get', ['user_id' => $tutor_request->student->id])}}" target="_blank">
									{{$tutor_request->student->full_name}}
								</a>
								@if ($tutor_request->child_name)
								 	<br>
								 	{{ $tutor_request->child_name }}
								@endif
								<br>
								{{$tutor_request->student->roles->implode('role', ', ')}}
								<br>
								<b>Balance:</b> {!!($tutor_request->student->studentBalance() < 0)?"<span style='color:red'>$".$tutor_request->student->studentBalance()."</span>" :"$".$tutor_request->student->studentBalance()!!}
							</td>
						
						<td>
							{{$tutor_request->course}}
							@if ($tutor_request->requested_tutor)
								<br><b>Requested Tutor:</b> {{$tutor_request->requested_tutor->full_name}}
							@endif
							@if ($tutor_request->tutor_proposal->count() > 0)
								<br><b>Proposals submitted by:</b>
									@foreach ($tutor_request->tutor_proposal as $proposal)
										<br>{{$proposal->tutor->full_name}} - ${{$proposal->proposed_rate}} ({{$proposal->created_at->diffForHumans()}} )
									@endforeach
							@endif
						</td>
						<td>${{$tutor_request->max_rate}}</td>
						<td class="col-md-4">
							{{$tutor_request->frequency}}<br>
							{{$tutor_request->session_time}}<br>
							@if ($tutor_request->scholarship_program)
								<b>Scholarship:</b> {{$tutor_request->scholarship_program}}
								<br>
							@endif
							{{$tutor_request->concerns}}
						</td>
						<td class="col-md-3">
							{!! Form::open(['method' => 'PUT', 'route' => ['unassign.student', 'tutor_request_id' => $tutor_request->id], 'class' => 'form-horizontal', 'id' => 'unassign-'.$tutor_request->id]) !!}

							    <div class="pull-right">
							        {!! Form::submit("Unassign", ['class' => 'btn btn-danger']) !!}
							    </div>

							{!! Form::close() !!}
							
							<a data-toggle="collapse" href="#tutor_request_{{$tutor_request->id}}" aria-expanded="false" aria-controls="tutor_request_{{$tutor_request->id}}"><b>Make Assignment</b></a>
							

							<div id="tutor_request_{{$tutor_request->id}}" class="collapse">
								@include('btn.assign.form_create_assignment')				
							</div>
						</td>	
					</tr>
				@endif
			@endif
		@endforeach
		</tbody>
	</table>
  </div>
  @endforeach
</div>

@stop