@extends('layouts.josh-front.default')

@section('content')
	
	@include('btn.assign.partials.assign_details')

	<br>

	<a href="{{ route('assignments.index') }}">Back to all assignments</a>
	<br><br>

	<div class="pull-left">
	    <a href="{{ route('assignments.edit', $assignment->ref_id) }}"><button type="button" class="btn btn-primary">Edit Assignment</button></a>
	</div>
	<div class="col-md-6 text-right">
        {!! Form::open([
            'method' => 'DELETE',
            'action' => ['AssignmentController@destroy', $assignment->id]
        ]) !!}
            {!! Form::submit('Delete Assignment', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>

@stop