@extends('layouts.josh-front.default')

@section('title')
	Stripe Connect Failure
@stop

@section('content')
	<h3>Failed To Connect To Platform</h3>
	<p>Your Stripe account was not connected to our Platform for the reason listed above. </p>

	<p>Contact Stripe or us with any questions or concerns.</p>
	<p>To try again, <a href="{{route('stripe.connect.signup')}}">click here</a></p>
@stop