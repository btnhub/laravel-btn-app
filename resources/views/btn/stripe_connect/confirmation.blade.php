@extends('layouts.josh-front.default')

@section('title')
	Stripe Connect Authorized
@stop

@section('content')
	<h3>Successfully Connected To Platform</h3>

	<p>You have successfully connected your Stripe account to our Platform.  You will now be able to receive payments directly from students.  </p>

	<p><b>Please Note:</b>

	<li>Our Service Fee will be applied to all charges</li>
	<li>Funds will be released to you immediately.  Stripe will then deposit the funds into your account.  Refer to Stripe's website for their deposit schedule (typically within 2 days).</li>
	</p>
@stop