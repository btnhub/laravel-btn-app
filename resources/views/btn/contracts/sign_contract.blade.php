@extends('layouts.josh-front.default')

@section('title')
	Tutor Contract
@stop

@section('content')
	@include('btn.contracts.partials.tutor_contract')
	<div class="container">
		<div class="row">
	
			<div class="col-md-4 pull-right">
				@include('btn.contracts.partials.tutor_contract_signature_form')			
			</div>
	
		</div>
	</div>
	
@stop