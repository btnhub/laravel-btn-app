@include('btn.contracts.partials.tutor_contract')

<br><br>
Signed and Acknowledged: 
<table class="table">
	<tbody>
		<tr>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>CONTRACTOR:</td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td>COMPANY:</td>
		</tr>
		<tr class="table-bordered">
			<td></td>
			<td><u>	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$tutor_name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mogi Mogaka &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
		</tr>
		<tr>
			<td></td>
			<td>Date:<u> {{$contract_date->format('m/d/Y')}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
			<td></td>
			<td>
				President, Mosaikz, LLC <br>
				d/b/a BuffTutor and RamTutor
			</td>
		</tr>
		
	</tbody>
</table>
