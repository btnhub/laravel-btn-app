<center><h3>INDEPENDENT CONTRACTOR AGREEMENT</h3></center>
<br>

This Independent Contractor Agreement ("Agreement") is entered into this <u>&nbsp;&nbsp;&nbsp;&nbsp;{{isset($contract_date) ? $contract_date->day : "&nbsp;&nbsp;&nbsp;&nbsp;"}}&nbsp;&nbsp;&nbsp;&nbsp;</u> day of <u>&nbsp;&nbsp;&nbsp;&nbsp;{{isset($contract_date) ? $contract_date->format('F') : "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}}&nbsp;&nbsp;&nbsp;&nbsp;</u>, {{date('Y')}}, by and between Mosaikz, LLC, a Colorado limited liability company, d/b/a BuffTutor and RamTutor, with a mailing address of 1905 15th Street, # 1932, Boulder, Colorado 80306 ("Company"), and <br><u>&nbsp;&nbsp;&nbsp;&nbsp; {{$tutor_name or "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>, who resides at <u>&nbsp;&nbsp;&nbsp;&nbsp; {{$address or "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"}} &nbsp;&nbsp;&nbsp;&nbsp;</u> ("Contractor"). In consideration of the promises and the mutual agreements, covenants, and provisions contained in this Agreement, the parties agree and declare as follows:
<br><br>

1. <u>Contractor's Services.</u>  Contractor agrees to join Company's Network of Tutors to provide tutoring services to assigned client customers ("Student" or "Students").  Contractor shall provide biographical and educational background information as requested by Company to be shown on Company website for Contractor's profile, which will be made public for review by potential Students.  Contractor shall maintain appropriate records as well as other records appurtenant to the provision of services including days and times spent tutoring, locations and Student names.  In providing the services, Contractor shall not take any action, nor cause Company to take any action, which would be in violation of any applicable federal, state or local laws, including but not limited to the federal false claims, anti-kickback and self-referral laws.  Company agrees to provide Contractor with all information reasonably necessary to assist Contractor to create a tutoring relationship with assigned Students and other services outlined in this Agreement.  However, all tools, knowledge, training, supplies, know-how and experience must be provided by Contractor. The parties acknowledge and agree that, nothing to the contrary, Contractor shall retain control and the right to exercise judgment over the manner and means by which the services will be provided.  Company makes no guarantee or warranty as to the number of hours or Students to be assigned to Contractor to perform services for Company hereunder.  
<br><br>

2. <u>Compensation.</u> 
{{-- Contractor's self-selected billable rate for tutoring services is set in conjunction with each Student and will be entered online by Contractor at the outset of each Tutoring session with Student.   --}}
Contractor's hourly earnings for tutoring services for each Student will be accepted by Contractor prior to the outset of the first Tutoring session with Student.  Contractor is responsible for logging working hours and tutoring sessions on Company website within 48 hours of the session. 

Upon submission of billing by Contractor and confirmation by Student to Company, Company shall initiate payment to {{-- Company --}} Contractor from Student's account via its website.  Payment to Contractor {{-- (equal to the billing rate less a <u> {{Auth::user()->profile->tutor_fee * 100}} </u>% administrative service fee to Company) --}} will be made by Company to Contractor within three (3) days of Company's receipt of payment from the Student for services rendered by Contractor. 
{{-- This administrative fee is set by Company and subject to change at Company's discretion provided, however, that two-weeks' notice will be given to Contractor of any change in the percentage.  --}}
Company reserves the right to deny payment for unreported, untimely, faulty or incomplete work done by Contractor.  In the event Company is not paid by Student for services rendered by Contractor, and Company is unable to collect such debt within sixty (60) days of the date that services were performed by Contractor, then Contractor shall not be entitled to receive any compensation for such services.  Contractor agrees and acknowledges that Company shall not be liable or responsible for collecting past due amounts from Students or compensating Contractor for uncollected or past due amounts.  
{{-- If desired, in exchange for payment of the outstanding administrative fee due to Company by Contractor, Company shall assign all rights to collect debts over 60 days past due from Student to Contractor.   --}}
If desired, in exchange for payment of any outstanding administrative fees due to Company by Contractor, Company shall assign all rights to collect debts over 60 days past due from Student to Contractor.  
<br><br>

3. <u>Holiday, Sickness & Benefit Entitlement.</u>  As Contractor is not an employee of Company, Contractor is not eligible for any holiday or sick pay or to receive any other benefits.  Contractor shall be responsible for and pay all expenses incurred by Contractor in connection with the performance of duties hereunder.
<br><br>

4. <u>Representations and Warranties of Contractor.</u>  Contractor represents and warrants to Company that:
<ol type="a">
	<li>All information provided by Contractor on any application to Company is true, accurate and correct;</li>
	<li>Company may conduct a background check on Contractor at initial application and at any time and from time to time;</li>
	<li>Contractor shall abide by all rules and procedures of Company that may be promulgated by Company from time to time and as set forth on Company's website;  </li>
	<li>All services performed by Contractor hereunder shall be performed in a professional manner and shall meet deadlines agreed upon between Contractor, Company and assigned Students.</li>
	<li>Contractor is a citizen of the United States of America ("U.S.A.") or is legally permitted to work in the U.S.A.;</li>
	<li>Contractor is proficient in the desired field or area of instruction and tutoring, which academic knowledge may be established by grades and/or passing of a Company-administered proficiency exam for selected courses or classwork.  </li>
	<li>Contractor needs no training, tools or additional education to perform the services contemplated hereunder.</li>
	<li>Contractor's performance of services does not violate any agreement of third parties and does not infringe upon any copyright, patent, trade secret or other proprietary right held by any third party.</li>
</ol>
5. <u>Term and Inclusion on Company Network of Tutors.</u>  The Agreement shall commence and be effective on the date of the mutual execution of this Agreement, at which time Contractor will be added to the Company Network of Tutors by placing the Contractor's information on Company's website(s).  Participation in the Network will continue until from such date until then final day of the current school semester and shall thereafter automatically renew for successive one (1) semester periods ("Term") unless Contractor submits written notice of removal from the Network of Tutors at least thirty (30) days prior to the end of the current school semester.  Upon written request or involuntary termination by Company, Contractor's name will be removed from the Network of Tutors.  Once removed from the Network of Tutors for any reason, if Contractor desires to be reinstated into Company's Network of Tutors, Contractor must reapply for reinstatement and pay all costs normally associated with registering a new Contractor.
<br><br>

6. <u>Termination.</u>  Nothing to the contrary, either party may terminate this Agreement at the conclusion of a term or school semester by giving the non-terminating party no less than thirty (30) days prior written notice of such intent to terminate.  Furthermore, in its sole discretion after investigation, Company may terminate this Agreement immediately under the following circumstances of bad conduct by Contractor:
<ol type="a">
	<li>Contractor is convicted of any crime or offense;</li>
	<li>Contractor falsifies Contractor's work status, academic transcripts, or previous teaching experience;</li>
	<li>Contractor is accused of or exhibits inappropriate conduct and/or behavior (including, but not limited to, berating Students, abusing and/or assaulting Students, escalating a conflict with a Student, repeatedly canceling sessions, or not showing up to scheduled sessions);</li>
	<li>Contractor behaves in threatening or hostile or verbally/physically abusive manner toward Students, Company, Company's officers, agents or employees.</li>
	<li>Contractor engages in any unprofessional relationship with any Student or client of Company;</li>
	<li>Contractor falsifies and/or exaggerates Contractor's knowledge of any materials or subject areas (i.e., if Contractor offers to tutor a course Contractor is not proficient in);</li>
	<li>Contractor violates, or assists the Student in violating, the Student's school honor code;</li>
	<li>Contractor accepts direct payment from or on behalf of any Student or client of Company;</li>
	<li>Contractor fails to report sessions with Students or with friends of Students or expands the tutoring sessions to include additional persons who are not registered with Company;</li>
	<li>Contractor purposefully charges a Student's account with Company falsely or incorrectly;</li>
	<li>Contractor fails to alert Company to an incorrect charge;</li>
	<li>Company receives a valid complaint from any Students that Contractor performs services for hereunder;</li>
	<li>Contractor steals, copies or otherwise misuses content from Company's website to Company's detriment;</li>
	<li>Contractor abuses or maliciously misuses Company's online tools;</li>
	<li>Contractor misuses or sells or negligently discloses any Student's personal, academic, or other private or confidential information;</li>
	<li>Contractor fails to contact a newly or continuing assigned Student in a timely manner when Contractor agreed Contractor was able to meet (or when Contractor's online profile provides that Contractor is available and accepting Students);</li>
	<li>Contractor fails to notify Company that Contractor cannot immediately begin sessions with a newly assigned Student or perform services within the time frame the Student needs assistance;</li>
	<li>Contractor fails to inform Company of Contractor's availability or travel plans that hinder, interfere or prevent Contractor from providing services hereunder with assigned Students, e.g., previously undisclosed travel preceding or during testing periods, during the term of this Agreement;</li>
	<li>Contractor reassigns a Student to persons not part of Company's network of Contractors or other tutors without the prior written consent of Company; or </li>
	<li>Contractor breaches any representation or warranty of Contractor set forth herein above.</li>
	<li>Contractor fails to provide services to assigned Students until the end of each semester.</li>
</ol>
<br><br>

7. <u>Liquidated Damages.</u> Contractor acknowledges that a failure by Contractor to perform and abide by this Agreement is a breach that results in reputational and economic harm to Company as well as causing mental anguish, stress and emotional harm to Students who have relied on Contractor and Company to assist them.  In the following events of breach, Contractor agrees to pay as liquidated damages to Company:  
<ol type="a">
	<li>If Contractor fails to perform assigned services for the entire duration of each school semester or term (with exceptions for written approval for leave from Company in the case of a medical, family or other documented emergency or any requirement under state and federal employment law) due to any voluntary termination by Contractor or involuntary termination of Contractor by Company or any discharge of Contractor due to his or her breach of this Agreement or any representation and warranty herein prior to the final day of the currently-enrolled semester: </li>

	<ol type="i">
		<li>The difference of any increase in Student billing rate by the replacement Contractor; and</li>
		<li>ii. The greater of:</li>
		<ol>
			<li> All Company expenses related to finding and registering new tutors to cover subjects taught by Contractor, including any and all of Company's registration, advertising, and administrative processing fees, or </li>
			<li> $500 minimum replacement fee per assigned Student.</li>
		</ol>
	</ol>
	<li>If Contractor performs tutoring services outside the scope of this Agreement with assigned Students, or if assigned Student cancels as a client with Company yet is continued to be tutored by Contractor, Contractor shall pay a liquidated damages fee of $1000.00 to Company as payment for tortious interference of the agreement between Company and Student.</li>
</ol>

8. <u>Confidentiality.</u> Contractor shall hold in confidence and shall not disclose, distribute, sell, copy, share or otherwise use any information obtained by Contractor while performing services under this Agreement, which is related to Company's employees, research, development, business affairs, records, processes, techniques or types of equipment, whether past, present or future, except as may be contemplated by this Agreement or authorized by Company in writing. Upon completion of its work under this Agreement, Contractor shall return to Company or destroy all confidential information, whether or not it is marked "confidential", and all records or documents received from Company, including but not limited to, any and all copies thereof that may have been made. 
<br><br>

9. <u>Non-Discrimination.</u> Contractor shall not discriminate against any client or Student of Company on the basis of race, creed, color, national origin, handicap, age, sexual preference or gender.
<br><br>

10. <u>Non-Exclusivity.</u> Company retains the right to contract with other independent contractors for services the same as or similar to those provided by Contractor, or to provide such services through its employees. Contractor shall have the right and is encouraged to provide tutoring services directly, or indirectly, with another agency or otherwise so long as it will not interfere with this Agreement or the Company's Student agreement and relationship.
<br><br>

11. <u>Non-Solicitation Covenant.</u>  For a period of three (3) years after the effective date of this agreement, Contractor will not directly or indirectly solicit business from, or attempt to sell, license, or provide the same or similar products or services as are now provided to, any Student, customer or client of Company, nor shall Contractor use Company's existing client's demographic and confidential information to solicit and provide quotes and/or transfer business to any competing entity. Further, for a period of three (3) years after the effective date of this Agreement, Contractor will not directly or indirectly solicit, induce, or attempt to induce any independent contractor of Company to terminate his or her contract with Company.
<br><br>

12. <u>Indemnification.</u>  Contractor shall indemnify, defend and hold harmless Company, and its owners, managers, representatives, agents, and employees, against and from all claims, actions, suits, proceedings, costs, expenses, damages, claims and liabilities, including but not limited to attorneys' fees, caused by an act or omission of Contractor.
<br><br>

13. <u>Acknowledgements of Independent Contractor Status.</u> THE PARTIES HERETO ACKNOWLEDGE AND AGREE THAT CONTRACTOR HAS BEEN RETAINED SOLELY TO PROVIDE THE SERVICES SET FORTH IN THIS AGREEMENT AND IS NOT AN AGENT OF COMPANY.  CONTRACTOR ACKNOWLEDGES AND AGREES THAT CONTRACTOR SHALL ACT AS AN INDEPENDENT CONTRACTOR, FROM WHICH ANY DUTIES ARISING OUT OF ITS ENGAGEMENT HEREUNDER SHALL BE OWED SOLELY TO COMPANY AND SHALL NOT REPRESENT TO ANY THIRD PARTY THAT THEY HAVE AGENCY AUTHORITY.  CONTRACTOR SHALL BE RESPONSIBLE FOR PAYING ANY FEDERAL AND STATE TAXES OWED ON THE COMPENSATION RECEIVED HEREUNDER AND IS NOT ELIGIBLE FOR WORKER'S COMPENSATION BENEFITS OR ANY OTHER EMPLOYMENT BENEFITS GRANTED BY COMPANY TO ITS EMPLOYEES, IF ANY. 
<br><br>

14. <u>DISCLAIMER.</u> CONTRACTOR ASSUMES ALL RISKS OF PERFORMING THE SERVICES UNDER THIS AGREEMENT. Absent Company's gross negligence or willful misconduct, Company shall in no way be liable for any PERSONAL injury, harm oR DEATH suffered by Contractor while performing services under this Agreement or for any other reason. In no event shall Company be responsible for special, indirect, consequential or other specialized measure of damages.
<br><br>

15. <u>Injunctive Relief.</u>  A breach of any of the promises or agreements contained herein will result in irreparable and continuing damage to Company for which there will be no adequate remedy at law, and Company is entitled to injunctive relief and/or a decree for specific performance, and such other relief as may be proper (including monetary damages if appropriate). 
<br><br>

16. <u>Arbitration.</u>  Any dispute arising out of this Agreement that the parties cannot resolve between themselves shall be submitted to binding arbitration in Boulder, Colorado before a mutually agreeable arbitrator who shall not be affiliated with either party.  The arbitrator shall determine the rules of the arbitration with express instructions to define rules that are most expeditious and cost-effective for the parties. If a mutually agreeable arbitrator cannot be determined by the parties, each party shall individually nominate an arbitrator and both nominated arbitrators shall determine an adequate arbitrator.  Any decision by the arbitrator may be reduced to judgment in a competent jurisdiction.  
<br><br>

17. <u>Notice.</u> Any notice required by this Agreement or given in connection with it, shall be in writing and shall be given to the appropriate party by personal delivery or by certified mail, postage prepaid, or recognized overnight delivery services at the addresses forth above. 
<br><br>

18. <u>Non-Assignability.</u>  Contractor shall not assign or transfer this Agreement or any rights or obligations hereunder. 
<br><br>

19. <u>Counterparts.</u>  This Agreement may be executed in two or more counterparts, each of which will be deemed an original, but all of which together will constitute one and the same instrument.
<br><br>

20. <u>Headings.</u>  The descriptive headings of the sections and subsections of this Agreement are intended for convenience only and do not constitute parts of this Agreement.
<br><br>

21. <u>Attorneys' Fees.</u>  In case of any action or proceeding to compel compliance with, or for a breach of any of the terms and conditions of this Agreement, the prevailing party will be entitled to recover from the non-prevailing party all costs of such action or proceeding, including, without limitation, reasonable attorneys' fees, accountants' fees, costs, and disbursements.
<br><br>

22. <u>Entire Agreement.</u>   This Agreement constitutes the parties' entire agreement with respect to the subject matter hereof.  There are no restrictions, promises, representations, warranties, covenants, or understandings other than those expressly set forth herein. This Agreement supersedes all prior agreements or understandings between the parties, and may not be modified or amended in any manner other than as set forth herein.
<br><br>

23. <u>Amendment, Modification, or Waiver of Agreement.</u>  No amendment, modification, or waiver of this Agreement will be valid unless the amendment, modification, or waiver is in writing and signed by Contractor and by Company. The failure of any party at any time to insist upon the strict performance of any provision of this Agreement will not be construed as a waiver of the right to insist upon the strict performance of the same provision at any future time.
<br><br>

24. <u>No Third-Party Beneficiaries.</u>  Nothing in this Agreement will be construed to give any rights or benefits in this Agreement to anyone other than Contractor and Company. All duties and responsibilities undertaken under this Agreement will be for the sole and exclusive benefit of the Contractor and Company, and not for the benefit of any other party.
<br><br>

25. <u>Governing Law.</u>  This Agreement and any dispute or controversy arising from this Agreement will be construed in accordance with and governed by the laws of the State of Colorado.
<br><br>

26. <u>Severability.</u>  If any provision in this Agreement is determined to be invalid or unenforceable by a court or arbitrator of competent jurisdiction, the parties desire and agree that the remaining provisions of the Agreement will nevertheless continue to be valid and enforceable. 
<br><br>

27. <u>Survival.</u>  The rights and obligations of the parties that by their nature must survive termination or expiration of this Agreement in order to achieve its fundamental purposes including, without limitation, the provisions regarding indemnification, shall survive any termination of this Agreement.
	
