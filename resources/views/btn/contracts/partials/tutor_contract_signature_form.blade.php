{!! Form::open(['method' => 'POST', 'route' => 'submit.tutor.contract', 'class' => 'form-horizontal', 'target' => '_blank']) !!}
    
    <div class="row">
        <div class="col-md-5">
           <div class="form-group">
                <div class="checkbox{{ $errors->has('terms_1') ? ' has-error' : '' }}">
                    <label for="terms_1">
                        {!! Form::checkbox('terms_1', '1', null, ['id' => 'terms_1', 'required' => 'required']) !!} I understand that I must be available and work with assigned students until the end of the academic term/semester in which I began working with the assigned student.
                    </label>
                    <label for="terms_2">
                        {!! Form::checkbox('terms_2', '1', null, ['id' => 'terms_2', 'required' => 'required']) !!} I understand that I must report <b>all</b> sessions with assigned students using the BuffTutor platform.
                    </label>
                    <label for="terms_3">
                        {!! Form::checkbox('terms_3', '1', null, ['id' => 'terms_3', 'required' => 'required']) !!} I understand that <b>I cannot accept a direct payment of any form from assigned students</b> and doing so will result in fines and legal action. All sessions must be charged using the BuffTutor platform.
                    </label>
                </div>
                <small class="text-danger">{{ $errors->first('terms_1') }}</small>
            </div> 
        </div>
        <div class="col-md-5 col-md-offset-1">
            <div class="form-group">
                <div class="checkbox{{ $errors->has('legal') ? ' has-error' : '' }}">
                    <label for="legal">
                        {!! Form::checkbox('legal', '1', null, ['id' => 'legal', 'required']) !!} I understand that my electronic signature below is legally binding
                    </label>
                </div>
                <small class="text-danger">{{ $errors->first('legal') }}</small>
            </div>
            <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                {!! Form::label('full_name', 'Full Legal Name') !!}
                {!! Form::text('full_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('full_name') }}</small>
            </div>

            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                {!! Form::label('address', 'Your Address') !!}
                {!! Form::textarea('address', null, ['class' => 'form-control', 'required' => 'required', 'size' => "40x3"]) !!}
                <small class="text-danger">{{ $errors->first('address') }}</small>
            </div>

            
            {{-- <div class="form-group">
                <div class="checkbox{{ $errors->has('positions') ? ' has-error' : '' }}">
                    <b>Positions(s) you will work as:</b><br>        
                    <label>
                        {!! Form::checkbox('positions[]', 'tutor', 'checked') !!} Tutor &nbsp;&nbsp;&nbsp;&nbsp;
                    </label>
                    <label>
                        {!! Form::checkbox('positions[]', 'music-teacher', null) !!} Music Teacher
                    </label>
                </div>
                <small class="text-danger">{{ $errors->first('positions') }}</small>
            </div> --}}

            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                {!! Form::label('date', 'Date') !!}
                {!! Form::date('date', Carbon\Carbon::now(), ['class' => 'form-control', 'readonly']) !!}
                <small class="text-danger">{{ $errors->first('date') }}</small>
            </div>

            <div class="btn-group pull-right">
                {!! Form::submit("Sign Contract", ['class' => 'btn btn-primary']) !!}
            </div>        
        </div>
    </div>
    

    <br> <br>After you sign, download your contract and return to the <a href="{{route('tutor.checklist')}}">Tutor Checklist</a> for the next steps.
{!! Form::close() !!}