@extends('layouts.josh-front.default', ['page_title' => "Promotions & Deals"])

@section('title')
Promotions & Deals
@stop

@section('content')

<h3>{{number_format(DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100)}}% Off First Session</h3>

<p>Get {{number_format(DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100)}}% off the first hour of your first session with one of our tutors!</p>
<li>Available to new students only (students who have not met with one of our tutors before).</li>
<li>Automatically applied to your first session, no coupon necessary.</li>
<li>Redeemable only once.</li>
<li>Not redeemable for cash.</li>
 
@stop