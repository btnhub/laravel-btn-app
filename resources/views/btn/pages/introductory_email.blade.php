@extends('layouts.josh-front.default', ['page_title' => "Introductory Email"])

@section('title')
	Introductory Email
@stop

@section('content')
<h3>Introductory Email</h3>

	<p>To increase the likelihood that students will schedule a session with you, your first e-mail should be professional, informative and compelling. Though a student has been assigned to you, he/she may still be searching for a tutor using other means, so you want your initial contact to stand out. In the first e-mail, you should:</p>
	<li>introduce yourself and explain that you were assigned to them through BuffTutor,</li>
	<li>briefly describe your qualifications and experience (include a link to your BuffTutor profile),</li>
	<li> state where sessions will occur, if you have a preference,</li>
	<li> state when you are available for sessions, include a link to your BuffTutor Calendar,</li>
	{{-- <li>state your rate, and if it's flexible, mention that,</li> --}}
	<li>ask the student questions that will encourage a response, e.g. When would you like to begin?  How often would you like to meet? What are you struggling with the most?</li>
	<hr />
	
	<p><strong>Sample Draft</strong></p>
	<p>Dear <span style="color: blue;">___Student Name___</span>,</p>
	{{-- <p>Thank you for choosing the BuffTutor Network. I have just been informed that you are in need of assistance in <span style="color: blue">___COURSE____</span> and I am more than happy to assist you in this course. My background is in <span style="color: blue">___Major/Degree/Year___</span> and I have been tutoring this course for <span style="color: blue">__###__</span> years. You may read about my experience and read references here: <span style="color: blue">___link to profile___</span>.</p>
	 --}}

	<p>Thank you for choosing the BuffTutor Network and for expressing interest in working with me. I am more than happy to assist you in <span style="color: blue">___COURSE____</span> this semester. My background is in <span style="color: blue">___Major/Degree/Year___</span> and I have been tutoring this course for <span style="color: blue">__###__</span> years. You may read about my experience and read references here: <span style="color: blue">___link to profile___</span>.</p>

	<p>Let me know when you would like to begin sessions. My calendar is posted on my profile.  I prefer to meet<span style="color: blue"> ___details on days and times that work for you___</span>. Sessions with me will be held in <span style="color: blue">___session location___</span>, however we can also meet elsewhere if you have a preference.</p>

	{{-- <p>My rate is <span style="color: blue">$##/hr</span>, however if this is more than you can afford, let me know what you can afford and I am happy to work something out with you or to help you get set up with a different tutor in our network.</p> --}}

	<p>I look forward to hearing from you and working with you!</p>
	<div>Sincerely,</div>
	<div><span style="color: blue">Your Name</span>
	</div>
	<hr />

	<p><strong>Example</strong>
	
	</p>
	<p>Dear John,</p>
	{{-- <p>Thank you for choosing the BuffTutor Network. I have just been informed that you are in need of assistance in Calc 1 (MATH 1300) and I am more than happy to assist you in this course. My background is in Chemical Engineering, I received an M.S. from CU, and I have been tutoring this course for over 5 years. You may read about my experience and read references here: <a href="#">Include link to your profile</a>
	</p> --}}

	<p>Thank you for choosing the BuffTutor Network and for expressing interest in working with me. I am more than happy to assist you with Calc 1 this semester. My background is in Mechanical Engineering and I have been tutoring this course for over 5 years. You may read about my experience and read references here: <a href="#">Include link to your profile</a>.</p>

	<p>Let me know when you would like to begin. My calendar is posted on my profile, I prefer to meet weekdays before 5pm. Sessions with me will be held in the Math Library, however we can also meet elsewhere if you have a preference.</p>

	{{-- <p>My rate is $###/hr, however if this is more than you can afford, let me know what you can afford and I am happy to work something out with you or to help you get set up with a different tutor in our network.</p> --}}

	<p>I look forward to hearing from you and working with you!</p>
	<div>Sincerely,</div>
	<div>John Doe</div>

@stop