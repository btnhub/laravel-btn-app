@extends('layouts.josh-front.default', ['page_title' => "Tutor Orientation"])

@section('title')
	Tutor Orientation
@stop

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
			<p>Welcome Aboard!  This page will cover the layout of our website and the tools you'll need while working with our students.</p>
				<ol>
					{{-- <li><a href="#account">Account Tour</a></li> --}}
					<li><a href="#getting-students">How To Get Students</a></li>
					<li><a href="#getting-paid">How To Get Paid</a></li>
					<li><a href="#policies">Policies</a></li>
					<li><a href="#faqs">FAQs</a></li>
				</ol>
				<p>Before you get started, make sure you complete the <a href="{{route('tutor.checklist')}}">Tutor Checklist</a>.</p>
			</div>
		</div>
		
		{{-- <div class="panel panel-warning">
            <div class="panel-heading" id="account">My Account</div>
            <div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
						<p>Everything you need will be under the Tutors menu or laid out on <a href="{{route('my.account')}}">My Account</a>.  Your account page will contain following information:</p>
						<ol>
							<li><b>Student List: </b>  a list of your assigned students and their contact information</li>
							<li><b>Sessions: </b> a record of all the sessions you have charged your students in the current semester</li>
							<li><b>Evaluations:</b> a summary of the evaluations your students have submitted</li>
							<li><b>Refunds: </b> a list of any disputed charges or requested refunds.</li>

						</ol>
					</div>
					<div class="col-sm-6">
						<img src="{{asset('btn/images/my-account-tabs.png')}}" width="100%">	
					</div> 
				</div>
			</div>
		</div>
		<br><br> --}}

		<div class="row">
			<div class="col-sm-6">
				<div class="panel panel-primary">
	                <div class="panel-heading" id="getting-students">How To Get Students</div>
	                <div class="panel-body">
		                <center><video width="400px" controls>
						  <source src="{{asset('btn/videos/04-How-To-Get-A-Student.mp4')}}" type="video/mp4">
						  Your browser does not support HTML5 video.
						</video></center>
					<br>
					<ol>
						<li><b>Request A Tutor:</b>	To start, the student will submit a tutor request.  We will then reach out to 1-3 tutors who meet the student's criteria (course, schedule, rate, etc). If you're selected, you will receive an e-mail alert.</li>
						<br>
						<li><b>Tutor Review:</b> Visit <a href="{{route('my.tutor.contracts', Auth::user()->ref_id)}}">My Tutor Requests</a> to review the request details and to indicate whether you want to work with the student (image below). <br><br>
						<b style="color:darkred">A note about the rate.</b>The listed rate is your net take-home hourly rate (our service fee has already been deducted).  We have negotiated this rate with the student.  Once you accept the assignment, <b>the rate cannot be changed.</b></li>
						<br>
						<li><b>Student Review & Selection:</b> We'll send the student a link to your profile and let the student decide whether to work with you. If the student selects you...</li>
						<br>
						<li><b>Get To Work!</b> You will receive an e-mail alert and the student's contact information will show up on <a href="{{route('my.account')}}">your list of students</a>. Contact the student to schedule a session.  Make your first e-mail professional! <a href="{{route('intro.email')}}" target="_blank">Sample Intro Email</a> </li>
					</ol>
					<img src="{{asset('btn/images/review-tutor-requests.png')}}" width="100%">
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-success">
	                <div class="panel-heading" id="getting-paid">How To Get Paid</div>
	                <div class="panel-body">
	                	<center><video width="400px" controls>
						  <source src="{{asset('btn/videos/05-How-To-Charge-Session.mp4')}}" type="video/mp4">
						  Your browser does not support HTML5 video.
						</video></center>
						<br>
						<p>Getting paid is incredibly easy, just 2 steps:
							<ol>
								<li>On <a href="{{route('my.account')}}">your list of students</a>, simply click the "Charge __Student_Name's__ Credit Card" link under the student's name.</li>
								<li>Submit the provided form (image below).</li>
							</ol>  
							That's it.  You will be paid immediately for the session, seriously, check your Stripe account!</p>
							<center><b>Charge a session immediately AFTER it occurs!</b></center>
							<br>
						<img src="{{asset('btn/images/charge-form.png')}}" width="100%">
					</div>
				</div>
			</div>
	
		</div>
		<br><br>
		<div class="row">
			<div class="col-sm-12">
				<h4 id="policies">Policies</h4>
				<h5>Cancellation Policy</h5>
				<p><b>If A Student Cancels: </b>Students must provide at least 24 hour notice to cancel a session.  Should a student fail to provide 24 hour notice, <b>the tutor may <u>choose</u> whether or not to charge the missed/cancelled session.</b>
				</p>
				<p><b>If A Tutor Cancels: </b> Tutors must provide 24 hour notice to cancel a session.  Should a tutor fail to provide 24 hour notice, <b>the tutor must provide a <u>free</u> tutor session for the duration of the missed session.</b>  For example, if a tutor cancels a 2 hour session without 24 hour notice, the tutor must provide a free 2 hour session or accummulate 2 hours for free before charging the student again.</p>

				<br>
				<h5>Refund Policy</h5>
				<p><b>First Session Refund: </b>If a student is unsatisfied with a tutor's performance in the first session, the student will receive a refund for the entire first session.</p>
				<p><b>Last Session Refund: </b> If a tutor was great in the beginning, but becomes ineffective, the student can request a refund of the <b><em>last hour of the last session</em></b> with the tutor.
				</p>
				<p><b>In Summary: </b> Prepare for your sessions and bring your "A-game" each time!  If a refund is issued, the tutor will not be paid for that session.</p>
			</div>
		</div>
		<br><br>
		<div class="row">
			<div class="col-sm-12">
				<h4 id="faqs">FAQs</h4>
				<ol>
					<li><b>Why isn't my profile showing up?</b>
						<p>Make sure you fill in all the fields when you edit your profile.  Your experience and courses section must be filled in.</p>
					</li>
					<li>
						<b>How many students will I get and how do I get more students?</b>
						<p>
							We cannot guarantee the number of students you will receive or the number of hours you will work.  However if you want to increase your chances of being recommended to a student:
							<ul style="list-style-type:disc">
								<li>Encourage your students to evaluate your performance after the first session.  They will receive a link to the evaluation form when you charge a session.</li>
								<li>Report all your hours and charge all the sessions using our platform.  It's not only <b>mandatory</b>, but more hours will improve your ranking in our tutor list.  Also, more hours will serve as an informal indication that your students are happy with your performance.</li>
							</ul>
						</p>

					</li>
					<li>
						<b>What do I do if my student wants to pay me directly?</b>
						<p>Just say no!  Direct payment (e.g. by cash, check, PayPal, Venmo, etc) is a violation of your contract and will result in legal action and hefty fees applied to both parties.  Students will all have to add a credit/debit card to their account before they can view your contact information.  If an unusual situation arises, please contact us immediately and we'll instruct you on how to proceed.</p>

					</li>	
					<li><b>Will I receive tax documents (e.g. 1099)?</b>
						<p>We use Stripe to process payments for you and to immediately relay the payment to you.  As such, Stripe is responsible for generating 1099 forms.  <a href="https://support.stripe.com/questions/will-i-receive-a-1099-k-and-what-do-i-do-with-it" target="_blank">More information</a>.</p>
					</li>
				</ol>
			</div>
		</div>
	</div>
	
@endsection