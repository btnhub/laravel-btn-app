@extends('layouts.josh-front.default', ['page_title' => "What's New"])

@section('title')
	What's New
@stop

@section('content')

<h2>What's New</h2>

<p>A lot has changed, please review the following to familiarize yourself with our new procedures and features.</p>

<h3>Tour & Tools</h3>
<p>This video provides a quick tour of your account and demonstrates how to charge a student and get paid.</p>

<center><video width="800" controls>
  <source src="{{asset('btn/videos/01-Tour-Charge-Student.mp4')}}" type="video/mp4">
  Your browser does not support HTML5 video.
</video></center>

<hr>

<h3>Faster & Easier Payments!</h3>
<p>Goodbye PayPal, Hello Stripe!  Gone are the days of waiting to get paid every 2 weeks.  We've developed a platform with Stripe to allow tutors to get paid immediately.</p>

<p><b>How it Works</b></p>
<ol>
	<li>Create a Stripe account and connect to our Platform.  Instructions under <a href="{{route('tutor.checklist')}}">Tutor Checklist</a>. </li>
	<li>After each session, use our online tools to charge the student for the session.</li>
	<li>You just got paid!  Stripe will automatically transfer the funds into your bank account.  Review <a href="https://support.stripe.com/topics/transfers-and-deposits" target="_blank">Stripe's Transfers & Deposits</a> to learn more about Stripe's transfer schedule.</li>
</ol>

<p><b>What's Gone</b></p>
<ul>
	<li>Upfront payments from students.  </li>
		<ul>
			<li>Instead, students will need to add a Credit Card to their account and then you can charge their card <b>after</b> each session.</li>
			<li>Rather than checking if a student has prepaid before each session, your account will show whether or not a student has added a card to his/her account.  Make sure a card has been added to a student's account <b>before the first session</b> with the student.</li>

		</ul>
	<li> </li>

</ul>

<hr>

<h3>Marketplace</h3>
Cut out the middle man.  Submit a proposal directly to a student and communicate with students directly before an assignment is made to make sure that you're a good fit.<br>
<center><video width="800" controls>
  <source src="{{asset('btn/videos/03-Marketplace.mp4')}}" type="video/mp4">
  Your browser does not support HTML5 video.
</video></center>

<hr>

<h3>How To Get A Student</h3>
<p>There are 3 ways to get set up with a student:</p>
<ol>
	<li>Student submits a tutor request and we match you with the student.</li>
	<li>A student submits a tutor request directly to you:</li>		
	<li>Student submits a tutor request and you submit a proposal</li>
</ol>
<center><video width="800" controls>
  <source src="{{asset('btn/videos/02-How_to_get_a_student.mp4')}}" type="video/mp4">
  Your browser does not support HTML5 video.
</video></center>

<hr>

<h3>More Detailed Profile</h3>
<p>We're adding the following to your profile:</p>
<ul>
	<li>Start and End Dates</li>
		<ul>
			<li>Can't start at the beginning of the semester?  No problem, change your start date and your profile will not show up on our list until you're ready.</li>
			<li>Rather than adding yourself to our tutor list every semester, simply edit your end date on your profile.  </li>
		</ul>
	<li>Evaluations</li> 
		<ul>
			<li>A count of all the evaluations you've received</li>
			<li>We will allow students to post a positive or negative website reference on your profile.</li>
			</ul>
		</ul>
	<li>Direct Tutor Request: Students can submit a tutor request directly to you through your profile.</li>	

	<li>Coming Soon: Profile Pictures</li>
</ul>

<hr>

<h3>Inbox</h3>
<p>Communicate with potential students through our messaging platform.  Your contact information will not be shared with a student until you are matched with a student.</p>

<hr>

<h3>Ranked Tutor List</h3>
<p>Rather than listing tutors alphabetically, we will rank tutors based on:</p>
<ul>
	<li>Evaluations </li>
		<ul>
			<li>The more positive evaluations you receive, the higher your ranking.</li>
			<li>End of semester evaluations are weighted more heavily</li>
			<li>Negative evaluations will lower your rank </li>
			<li>We will no longer remind students to evaluate your performance.</li>
		</ul>
	<li>Number of Hours Recorded: The more hours you record, the higher your rank is.  If your students don't evaluate you, the number of hours you work is taken as a positive sign that you're doing a good job.</li>
	<li>Refunds & Complaints</li>
		</ul>
</ul>
@stop

@push('css')
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('btn/css/stripe-connect.css') }}">
@endpush