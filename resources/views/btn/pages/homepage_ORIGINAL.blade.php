@extends('btn.layouts.home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Your Application's Landing Page.<br><br>
                    <a href="{{route('user.manager')}}">User Manager</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection