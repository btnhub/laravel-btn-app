@extends('layouts.josh-front.default', ['page_title' => "Online Tutoring"])

@section('title')
Online Tutoring
@stop

@section('content')

<h3>Tutor from the comfort of your own home!</h3>

If you would like to tutor online, the following are required (READ CAREFULLY).  If you can provide ALL the items in an online tutor session, <a href="{{route('profile.edit', Auth::user()->ref_id)}}">update your profile</a>:
<ol>
	<li><b>Location: </b> Select "Online" as a location you tutor (under the Details section).  Also add "Online" in the "Where you can meet" descriptor.</li>
	<li><b>Experience:</b> At the end of your Experience section, briefly describe how you teach online and the tools you will use for online tutoring. </li>
	<li><b>Rate:</b> If you have a different rate for online tutoring, describe how your online tutoring rate in the Rate Details section.</li>
</ol>

<hr>

<h3>Online Tutoring Requirements</h3>
<ul>
	<li>
		<h4>Video Conferencing</h4>
		Your online sessions should preferably be a video conference call with both an audio and video feed.  If a video feed is not possible (or unreliable) the session should at the very least be voice call. Examples: 
			<ol type="a">
				<li>Google Hangouts</li>
				<li>Skype</li>
			</ol>
		<b>Select a platform that is FREE for the student and, preferably, one that will not require the student to create an account.</b>  For example, anyone can join any Google Hangout simply with a link (a Google account is not required). 
		<br>
		<p><b style="color:darkred">Respect Your Student's Privacy</b> If you plan on recording sessions and the student's audio/video feed will also be captured, <b>you must obtain consent from the student beforehand.</b> </p>

	</li>
	<br>
	<li>
		<h4>Collaborative Capability</h4>
		Your online session <b>MUST</b> include either a collaborative software or have screensharing capabilities. Examples:
		<ol type="a">
			<li><b>Google Docs:</b> shared documents (e.g. for editing papers)</li>
			<li><b>Google Sheets:</b> shared spreadsheets</li>
			<li><b>Google Hangouts Screensharing</b></li>
			<li><b>Skype Screensharing</b></li>
			<li><b>Skype Whiteboard</b> - If you use a whiteboard or drawing canvas, consider using a <a href="https://www.wacom.com" target="_blank">writing/drawing tablet</a> to quickly and easily write  & draw on online boards.</li>
			<li><b>Collaborative Whiteboard:</b> for example <a href="https://www.bitpaper.io/" target="_blank">BitPaper</a>, <a href="https://whiteboardfox.com/" target="_blank">Whiteboard Fox</a> </li>
			<li><b>Physical Whiteboard/Chalkboard:</b> while not collaborative, some tutors teach with a video feed and a real whiteboard that they write on.</li>
		</ol>
		<b>Select a platform that is FREE for students to use{{--  and will not require the student to create an account --}}.</b>  Feel free to research other options for screensharing or collaborative whiteboards.
	</li>

	<br>

	<li>
		<h4>Deliverables</h4>
		You must provide students with a copy of the work accomplished at the end of each session. Examples:
		<ol type="a">
			<li><b>Collaborative Tools:</b> Students must be able to have access to and save a copy of the shared document. Most collaborative tools have this capability. </li>
			<li><b>Screensharing: </b> You must provide the student with a copy of the work you did on your screen. At the very least, take a screenshot of the work and e-mail the student a copy at the end of each session.</li>
			<li><b>Physical Whiteboards: </b>If you teach by broadcasting a video feed of you explaining concepts on a physical whiteboard, you must provide either a video recording of that session or a picture (or pictures) of the work on the whiteboard at the end of each session.</li>
		</ol>
	</li>
	
	<br>
	
	<li>
		<h4>Mastery Of Platform</h4>
		Practice, Practice, Practice!  You must be able to easily use your chosen online tutoring tools and you must be able to explain how to use the platform and tools to students. Consider practicing with a friend a few times.
	</li>
</ol>
	
<br>

<hr>

<h4 style="color:darkblue;font-weight: bold">COMING SOON: BUFFTUTOR ONLINE</h4>
<p>We are developing an online tutoring platform that will have all the above listed capabilities. Stay tuned!</p>
 
@endsection