<?php $cities = App\Location::studentsVisible()->where('city', 'not like', '%online%')->orderBy('website_order')->pluck('city')->implode(', '); ?>

@extends('layouts.josh-front.default_home')

{{-- Page title --}}
@section('title')
    BuffTutor: Outstanding College & High School Tutoring
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    {{-- <link href="{{ asset('assets/vendors/animate/animate.min.css') }}" rel="stylesheet" type="text/css"/> --}}
    <!--end of page level css-->
@stop

{{-- slider --}}
@section('top')
    <div style="background-color:lightgray; font-size:18px;padding:3px">
        <center><b>Welcome To BuffTutor / RamTutor!</b></center>
    </div>
    <!--Carousel Start -->
    <div id="owl-demo" class="owl-carousel owl-theme">
        <div class="item"><img src="{{asset('btn/images/couple_studying_home_page.jpg')}}"  max-height="70px" alt="Connecting Students With The Best Tutors"></div>
    </div>
    <!-- //Carousel End -->
@stop

{{-- content --}}
@section('content')
    <div class="container">
        <section class="purchas-main">
            <div class="container bg-border wow" data-wow-duration="2.5s">
                <div class="row">
                    <div class="col-md-7 col-sm-7 col-xs-12">
                        <span class="purchae-hed">{{number_format(DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100)}}% Off First Hour!</span>
                        <p>Try out one of our tutors and get {{number_format(DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100)}}% off your first hour.  <a href="{{url('/promotions')}}" title="BuffTutor Promotions & Deals">Learn More</a><br></p>
                </div>
            </div>
        </section>
        <!-- Service Section Start-->
        <div class="row">
            <!-- Responsive Section Start -->
            <div class="text-center">
                <h3 class="border-primary"><span class="heading_border bg-primary">How It Works: 3 Easy Steps</span></h3>
            </div>
            <div class="col-sm-6 col-md-4 wow bounceInLeft" data-wow-duration="3.5s">
                <div class="box">
                    <div class="box-icon">
                        <a href="{{route('request.get')}}" title="Tutor Request Form"><i class="livicon icon" data-name="desktop" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i></a>
                    </div>
                    <div class="info">
                        <h1 class="success text-center" style="font-size: 2em"><a href="{{route('request.get')}}" title="BuffTutor Tutor Request Form">1) Request A Tutor</a></h1>
                        <p>Submit our <a href="{{route('request.get')}}" title="BuffTutor Tutor Request Form">Tutor Request</a> form and let us know which course you need assistance with. We'll search our network for a tutor fits who your schedule and we'll negotiate a rate that fits your budget.</p>
                    </div>
                </div>
            </div>
            <!-- //Responsive Section End -->
            <!-- Easy to Use Section Start -->
            <div class="col-sm-6 col-md-4 wow bounceInDown" data-wow-duration="3s" data-wow-delay="0.4s">
                <!-- Box Start -->
                <div class="box">
                    <div class="box-icon box-icon1">
                       <a href="{{route('list.tutors')}}" title="Tutor Profiles"><i class="livicon icon1" data-name="doc-portrait" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i></a>
                    </div>
                    <div class="info">
                        {{-- <h3 class="primary text-center"><a href="{{route('list.tutors')}}">2A) Find A Tutor</a></h3> --}}
                        <h2 class="primary text-center" style="font-size: 2em"><a href="{{route('list.tutors')}}" title="Tutor Profiles">2) Select A Tutor</a></h2>
                        <p>You will receive a proposal from 1-3 tutors in our network who fit your criteria. Review each tutor's qualifications and select a tutor you want to work with. Then... </p>
                    </div>
                </div>
            </div>
            <!-- //Easy to Use Section End -->
            <!-- Clean Design Section Start -->
            {{-- <div class="col-sm-6 col-md-3 wow bounceInUp" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="box">
                    <div class="box-icon box-icon2"><a href="{{route('request.get')}}">
                        <i class="livicon icon1" data-name="doc-portrait" data-size="55" data-loop="true" data-c="#f89a14" data-hc="#f89a14"></i></a>
                    </div>
                    <div class="info">
                        <h3 class="warning text-center"><a href="{{route('request.get')}}">2B) Submit A Tutor Request</a></h3>
                        <p><b>Alternatively,</b> instead of contacting tutors directly, submit a <a href="{{route('request.get')}}">tutor request</a> and reach out to all our tutors who offer assistance in the subject.  You will then receive proposals from tutors and you can use our online messaging app to communicate with tutors before selecting the right one for you.
                    </div>
                </div>
            </div> --}}
            <!-- //Clean Design Section End -->
            <!-- 20+ Page Section Start -->
            <div class="col-sm-6 col-md-4 wow bounceInRight" data-wow-duration="5s" data-wow-delay="1.2s">
                <div class="box">
                    <div class="box-icon box-icon3">
                        <i class="livicon icon1" data-name="check" data-size="55" data-loop="true" data-c="#d9534f" data-hc="#d9534f"></i>
                    </div>
                    <div class="info">
                        <h3 class="danger text-center" style="font-size: 2em">3) Get To Work!</h3>
                        <p>Once you've selected the right tutor for you, you'll be able to acces the tutor's contact information.  Reach out to the tutor to schedule a session and get to work!  </p>
                    </div>
                </div>
            </div>
            <!-- //20+ Page Section End -->
        </div>
        <!-- //Services Section End -->
    </div>

    <!-- Accordions Section End -->
    <div class="container">
        <div class="row">
            <!-- Accordions Start -->
            <div class="text-center wow flash" data-wow-duration="3s">
                <h3 class="border-success"><span class="heading_border bg-success">Who We Are</span></h3>
            </div>
            <!-- Accordions End -->
            <div class="col-md-6 col-sm-12 wow slideInLeft" data-wow-duration="1.5s">
                <!-- Tabbable-Panel Start -->
                <div class="tabbable-panel">
                    <!-- Tabbablw-line Start -->
                    <div class="tabbable-line">
                        <!-- Nav Nav-tabs Start -->
                        <!-- <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#tab_default_1" data-toggle="tab">
                                Web </a>
                            </li>
                            <li>
                                <a href="#tab_default_2" data-toggle="tab">
                                Html 5 </a>
                            </li>
                            <li>
                                <a href="#tab_default_3" data-toggle="tab">
                                CSS 3 </a>
                            </li>
                            <li>
                                <a href="#tab_default_4" data-toggle="tab">
                                Bootstrap </a>
                            </li>
                        </ul> -->
                        <!-- //Nav Nav-tabs End -->
                        <!-- Tab-content Start -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_default_1">
                                <div class="media">
                                    <div class="media-left tab col-sm-12">

                                        <img class="media-object img-responsive" src="{{ asset('btn/images/student_teacher_studying.jpg') }}" alt="Tutor Session">
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                        <!-- Tab-content End -->
                    </div>
                    <!-- //Tabbablw-line End -->
                </div>
                <!-- Tabbable_panel End -->
            </div>
            <div class="col-md-6 col-sm-12 wow slideInRight" data-wow-duration="3s">
                <!-- Panel-group Start -->
                <div class="panel-group" id="accordion">
                    <!-- Panel Panel-default Start -->
                    <div class="panel panel-default">
                        <!-- Panel-heading Start -->
                        <div class="panel-heading text_bg">
                            <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" title="About BuffTutor">
                                <span class=" glyphicon glyphicon-minus success"></span>
                                <span class="success">About BuffTutor</span></a>
                            </h4>
                        </div>
                        <!-- //Panel-heading End -->
                        <!-- Collapseone Start -->
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <p>
                                    BuffTutor is a network of experienced tutors in {{$cities}} with a passion for teaching. We tutor a wide variety of subjects from Math & Science to Writing & Foreign Languages for College and High School classes. Our goal is to make the process of finding the right tutor as quick, easy and effective as possible, and since 2009, we've assisted thousands of students connect with the right tutors.

                                    <br>

                                    <center><b style="font-size: 1.25em"><em>In-person And Online Tutoring Available</em></b></center>
                                    <center>
                                        @foreach ($cities_array as $city)
                                            
                                            <a href="{{route('city.tutor.list', strtolower(str_replace(' ','_',$city)))}}">{{$city}} Tutors</a><br>
                                        @endforeach
                                        <a href="{{route('city.tutor.list', 'online')}}">Online Tutors</a>
                                    </center>
                                </p>
                            </div>
                        </div>
                        <!-- Collapseone End -->
                    </div>
                    <!-- //Panel Panel-default End -->
                    <div class="panel panel-default">
                        <div class="panel-heading text_bg">
                            <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" title="Reasons To Choose BuffTutor">
                                <span class=" glyphicon glyphicon-plus success"></span>
                                <span class="success">Why Choose Us</span>
                            </a>
                        </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    <b>Experienced Tutors</b><br>
                                    All of our tutors have not only excelled in the courses they tutor, but also have at least one year of teaching and tutoring experience. We also perform a background check on all our tutors.

                                    <br><br>

                                    <b>Detailed Tutor Profiles </b><br>
                                    Search our <a href="{{route('list.tutors')}}" title="BuffTutor List of College & High School Tutors">tutor list</a> for your perfect tutor. Each tutor's profile includes his/her experience, accomplishments and availability.<br><br>

                                    <b>Rapid Turnaround Time</b><br>
                                    We strive to get students matched with a tutor within 48 hours, typically students are paired with tutors within 12-24 hours.<br><br>

                                    <b>Painless Payments & Refunds</b><br>
                                    Add a credit/debit card to your BuffTutor account and it will be charged <b>after</b> the session occurs.  Didn't love your tutor?  Let us know and we'll refund the session.<br><br>

                                    <b>Easy Online Tools</b><br>
                                    Use our online tools to keep track of all your payments and sessions.<br><br>

                                    <b>Satisfaction Guaranteed</b><br>
                                    With no contracts, students can meet as often with their tutors as needed. Students are encouraged to let us know how their tutors are doing, and if a student isn't satisfied with a tutor, we'll refund them for the session and match them with a different tutor, if desired.
                                </p>
                            </div>
                        </div>
                    </div>
                    
                    {{-- <div class="panel panel-default">
                        <div class="panel-heading text_bg">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                <span class=" glyphicon glyphicon-plus success"></span>
                                <span class="success">BuffTutor Vs. The Rest</span></a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>
                                    So, how do we compare to other tutoring services (both in town and online)?
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>BuffTutor</th>
                                                <th>The Rest*</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Tutor Quality</td>
                                                <td>Tutors must have excelled in the course they are tutoring, pass our rigorous proficiency exams (based on college level classes), pass a background check and an interview. Tutors also must have 1+ years of relevant teaching/tutoring experience. </td>
                                                <td>Typically the minimum requirement is a receiving a B or better in the class, no previous teaching experience is usually required.  </td>
                                            </tr>
                                            <tr>
                                                <td>Rate</td>
                                                <td>
                                                    Tutor sets his/her own rate starting as low as $20-25/hr. Rate tends to reflect the tutor's education and teaching experience.  Students/parents can discuss this with tutors and negotiate a rate that works for both parties.
                                                </td>
                                                <td>
                                                    Company sets the rate (typically starting at ~$50-60/hr) regardless of tutor's background and experience.  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tutor Compensation</td>
                                                <td><b>Tutor keeps 65-75% of earnings.</b></td>
                                                <td>Tutor keeps 20-40% of earnings.</td>
                                            </tr>
                                            <tr>
                                                <td>Contract Restrictions</td>
                                                <td>No obligation to meet with tutors, we do not sell packages.  Meet with a tutor as often as is needed.  Pay for sessions <b>after</b> they occur.</td>
                                                <td>Complicated prepaid packages where a student or parent feels obligated to use up prepaid hours even if the tutor is not being effective.</td>
                                            </tr>
                                            <tr>
                                                <td>Guarantee</td>
                                                <td>
                                                    If you're not happy with your tutor's performance, tell us and we will refund the most recent session.<br><br>
                                                    <b>We make NO PROMISES about your performance and final grade</b>.  All we promise is to connect you with experienced and passionate tutors.
                                                </td>
                                                <td>
                                                    Grade guarantees with strict (almost impossible) rules to adhere to.  <br>
                                                    No refunds, or difficult process to receive refund (typically students are placed with other tutors within the company's network before a refund is offered).
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <small>*Based on other services websites and complaints from students & parents who have worked with other tutoring companies</small>*
                                </p>
                            </div>
                        </div>
                    </div> --}}
                    
                </div>
                <!-- //Panle-group End -->
            </div>
        </div>
        <!-- //Accordions Section End -->

        {{-- <div class="row">
            <!-- Testimonial Section -->
            <div class="text-center">
                <h3 class="border-primary"><span class="heading_border bg-primary">BuffTutor Vs. The Rest</span></h3>
            </div>
            <p>
                
                <center><h4>So, how do we compare with other tutoring services (both in town and online)?</h4></center>
                
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th><center>BuffTutor</center></th>
                                    <th><center>The Rest*</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tutor Quality</td>
                                    <td>Tutors must have excelled in the course they are tutoring, pass our rigorous proficiency exams (based on college level classes), pass a background check and an interview. Tutors also must have 1+ years of relevant teaching/tutoring experience. </td>
                                    
                                    <td>
                                    Freelance tutors simply create a flier, a Craigslist ad or a profile on an online tutoring website.<br>

                                    Other tutoring companies (including a school's tutor list) typically require a tutor take and pass the course he/she is tutoring (sometimes requiring a B or better in the class), no previous teaching experience is usually required.  </td>
                                </tr>
                                <tr>
                                    <td>Rate</td>
                                    <td>
                                        <b>Tutor sets his/her own rate</b>, starting as low as $20-25/hr. Rate tends to reflect the tutor's education level  and teaching experience.  Students/parents can discuss this with tutors and negotiate a rate that works for both parties.
                                    </td>
                                    <td>
                                        Company sets the rate (typically starting at ~$50-60/hr) regardless of tutor's background and experience.  
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tutor Compensation</td>
                                    <td><b>Tutor keeps 65-80% of earnings.</b></td>
                                    <td>Tutor keeps 20-40% of earnings.</td>
                                </tr>
                                <tr>
                                    <td>Contract Restrictions</td>
                                    <td>We do not sell packages, students can meet with a tutor as often as is needed.  Pay for sessions <b>after</b> they occur.</td>
                                    <td>Companies offer complicated prepaid packages where a student or parent feels obligated to use up prepaid hours even if the tutor is not being effective.</td>
                                </tr>
                                <tr>
                                    <td>Guarantee</td>
                                    <td>
                                        If you're not happy with your tutor's performance, tell us and we will refund the most recent session.  Feedback from students helps us keep the best tutors in our network.  <br><br>
                                        <b>We make NO PROMISES about a student's performance and final grade</b>.  All we promise is to connect you with experienced and passionate tutors.
                                    </td>
                                    <td>
                                        No refunds, or difficult process to receive refund. <br><br><br><br>
                                        Grade guarantees with strict (almost impossible) rules to adhere to.  
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <small>*Based on other tutoring services websites and complaints from students & parents who have worked with other tutoring companies</small>*
                    </div>
                </div>
                
                        </p>
            <!-- Testimonial Section End -->
        </div> --}}
        <!-- Testimonial Start -->
        <div class="row">
            <!-- Testimonial Section -->
            <div class="text-center">
                <h3 class="border-danger"><span class="heading_border bg-danger">Testimonials</span></h3>
            </div>
            <div class="col-md-4 wow bounceInLeft" data-wow-duration="3s">
                <div class="author">
                    <div>
                    <label>"Luis was a great tutor. I meet with him once a week and easily passed a class I failed in a previous semester. He made everything easy to understand and would make sure I fully understood the concepts I was struggling with."</label>

                    <p class="text-right">
                        Carl H.
                    </p>
                </div>
                <div>
                    <p>
                        <label>"BuffTutor saved my semester! It can save yours too!"</label>
                    </p>
                    <p class="text-right">
                        Jeremy B.
                    </p>
                </div>
                </div>
            </div>
            <div class="col-md-4 wow bounceIn" data-wow-duration="3s">
                <div class="author">
                    <div>
                    <p>
                        <label>"AriG was the perfect match for me, and I would not have done as well in class without her. I would definitely recommend her to all of my firends!"</label>
                    </p>
                    <p class="text-right">
                        Erin S.
                    </p>
                </div>
                <div>
                    <p>
                        <label>"AlexH was fantastic. He truly made the difference between passing Econ and failing."</label>
                    </p>
                    <p class="text-right">
                        Joseph A.
                    </p>
                </div>
                </div>
            </div>
            <div class="col-md-4 wow bounceInRight" data-wow-duration="3s">
                <div class="author">
                    <div>
                    <p>
                        <label>"Best tutor in the world! My tutor, JessicaY, was very patient, reasonable and reliable. She was able to articulate in a manner that broke down complicated topics while making them enjoyable!"</label>
                    </p>
                    <p class="text-right">
                        Katie G. 
                    </p>
                </div>
                <div>
                    <p>
                        <label>"Very organized and easy to find a tutor."</label>
                    </p>
                    <p class="text-right">
                        Allison L.
                    </p>
                </div>
                </div>
            </div>
            <!-- Testimonial Section End -->
        </div>
        <!-- Testimonial End -->
    </div>
    <!-- //Container End -->
@stop

{{-- footer scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    {{-- <script type="text/javascript" src="{{ asset('assets/js/frontend/index.js') }}"></script> --}}
    <!--page level js ends-->

@stop
