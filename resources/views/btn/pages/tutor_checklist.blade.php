@extends('layouts.josh-front.default', ['page_title' => "Tutor Checklist"])

@section('title')
	Tutor Checklist
@stop

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6">
	    	<h3>Account Checklist</h3>
	    	@include('btn.tables.tutors.checklist_account')
	    </div>
	    @if (Auth::user()->isTutor())
	    	<div class="col-md-6">
		    	<h3>New Assignment Checklist</h3>
		    	@include('btn.tables.tutors.checklist_assignment')

		    	<h3>End Of Semester Checklist</h3>
		    	@include('btn.tables.tutors.checklist_end_semester')
		    </div>
	    @endif
	    

	</div>
</div>

@stop