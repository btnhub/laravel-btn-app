@extends('layouts.josh-front.default', ['page_title' => "Terms Of Use"])

@section('title')
	Terms Of Use
@stop

@section('content')
	@include('btn.pages.partials.terms_of_use')
@stop