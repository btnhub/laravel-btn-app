@extends('layouts.josh-front.default', ['page_title' => "Request A Tutor"])

@section('title')
Request A Tutor
@stop

@section('meta-description')
Get the help you need, submit our tutor request form and find the right tutor for you!
@stop

@section('content')
	{{-- <h3>Request A Tutor</h3> --}}

	<div class="container">
	    <div class="row">
	        <div class="col-md-10 col-sm-10">
	            <p>Let us know what you need assistance with and we'll match you with a tutor within 48 hours, typically within 1 day. We recommend you first search our <a href="{{route('list.tutors')}}">list of tutors</a> for someone you'd like to work with and include the tutor's name in this form.</p>

	            <p>Please submit this form <b>once</b> for <b>each</b> course you need assistance with.</b>  </p>
	            
	            {{-- <p><b style="color:red">10/19/2017:</b> Please note that most of our Math & Science tutors are fully booked and we are in the process of hiring more tutors.  Feel free to submit our request form and if we hire an experienced tutor who can assist you in the near future (or if one of our current tutor's schedule opens up), we'll reach out to you immediately.</p> --}}
	        </div>
	    </div>
	    <div class="row">
	    	@if ($errors->any())
		        <div class="alert alert-danger">
		            @foreach ($errors->all() as $error)
		                {{ $error }}<br>        
		            @endforeach
		        </div>
	        @endif
			@include('btn.forms.request_tutor_general')
	    </div>
	</div>
@stop