@extends('layouts.citybook.dashboard')

@section('page_title', 'Edit Profile')

@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places{{-- &callback=initAutocomplete --}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-map-add.js')}}"></script>
@endsection

@section('dashboard_content')
    
    @if ($user->isTutor() && !$user->isActiveTutor())
        <div class="list-single-main-item fl-wrap">
            <div class="list-single-main-item-title fl-wrap">
                <h3><span>This account is not active!</span></h3>
            </div>
            <div class="row">
                <div class="col-md-12" style="border:2px solid gray;border-radius:3px;margin-bottom: 15px;padding:10px">
                    <b style="color:darkred">Your profile is incomplete.  Please address the following:</b>  
                    @if (!$user->profile->show_profile_tutor)
                        <p style="color:black"><i class="fa fa-arrow-right"></i>Your profile is listed as "Hidden", please e-mail us to change this status.</p>
                    @endif
                    @if ($user->profile->tutor_contract == NULL)
                        <p style="color:black"><i class="fa fa-arrow-right"></i>You need to <a href="{{route('get.tutor.contract')}}" style="text-decoration: underline">sign our contract</a>.</p>
                    @elseif($user->profile->tutor_contract < $tutor_contract_date)
                        <p style="color:black"><i class="fa fa-arrow-right"></i>You need to <a href="{{route('get.tutor.contract')}}" style="text-decoration: underline">sign the updated contract</a>.</p>
                    @endif
                    @if ($user->profile->background_check == NULL || $user->profile->background_check == "")
                        <p style="color:black"><i class="fa fa-arrow-right"></i>You must pass the background check. Contact us for more information.</p>
                    @endif
                    @if ($user->profile->tutor_experience == '')
                        <p style="color:black"><i class="fa fa-arrow-right"></i>You must fill in the Tutor Experience section.</p>
                    @endif
                    @if ($user->profile->tutor_education == '')
                        <p style="color:black"><i class="fa fa-arrow-right"></i>You must select an option in the Current Education field.</p>
                    @endif
                    @if ($user->profile->courses == '')
                        <p style="color:black"><i class="fa fa-arrow-right"></i>You must list the courses you can tutor.</p>
                    @endif
                    @if ($user->profile->tutor_start_date > Carbon\Carbon::now() || $user->profile->tutor_end_date < Carbon\Carbon::now())
                        <p style="color:black"><i class="fa fa-arrow-right"></i> Your profile will be visible from <b>{{$user->profile->tutor_start_date}} to {{$user->profile->tutor_end_date}}</b>.  Change your Start/End dates to indicate when you will be available to tutor.  <b>You MUST complete the semester/quarter you begin working.</b></p>
                    @endif 
                </div>
                
            </div>
        </div>
    @endif
    
    
    <div class="col-md-12">
        @if ($user->isTutor())
            <div class="scroll-nav-wrapper fl-wrap">
                <div class="container">
                    <nav class="scroll-nav scroll-init">
                        <ul>
                            <li><a class="act-scrlink" href="#sec1">Contact</a></li>
                            <li><a href="#details">Details</a></li>
                            <li><a href="#experience">Experience</a></li>
                            <li><a href="#courses">Courses</a></li>
                            <li><a href="#location">Location</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        @endif
        <!-- profile-edit-container--> 
        <div id="contact" class="profile-edit-container add-list-container">
            <div class="profile-edit-header fl-wrap">
                <h4 style="color:black">My Contact Info</h4>
            </div>
            <div class="custom-form">
                {!! Form::model($user, ['method' => 'PATCH', 'route' => ['profile.edit_contact', 'user_ref_id' => $user->ref_id], 'class' => 'form-horizontal']) !!}
                    <div class="row">
                        <div class="col-sm-6">
                            <label>First Name <i class="fa fa-user"><small class="text-danger" style="color:red">{{ $errors->first('first_name') }}</small></i></label>
                            {!! Form::text('first_name', null, []) !!}
                        </div>
                        <div class="col-sm-6">
                            <label>Last Name <i class="fa fa-user"><small class="text-danger" style="color:red">{{ $errors->first('last_name') }}</small></i></label>
                            {!! Form::text('last_name', null, []) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Email Address<i class="fa fa-envelope-o"></i> <small class="text-danger" style="color:red">{{ $errors->first('email') }}</small> </label>
                            {!! Form::email('email', null, []) !!}
                        </div>
                        <div class="col-sm-6">
                            <label>Phone <i class="fa fa-phone"><small class="text-danger" style="color:red">{{ $errors->first('phone') }}</small></i></label>
                            {!! Form::text('phone', null, []) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Password <i class="fa fa-eye"><small class="text-danger" style="color:red">{{ $errors->first('password') }}</small></i></label>
                            {!! Form::password('password', []) !!}
                        </div>
                        <div class="col-sm-6">
                            <label>Confirm Password <i class="fa fa-eye"><small class="text-danger" style="color:red">{{ $errors->first('password_confirmation') }}</small></i></label>
                            {!! Form::password('password_confirmation', []) !!}
                        </div>
                    </div>
                    <button class="btn  big-btn  color-bg flat-btn">Update Contact Info<i class="fa fa-angle-right"></i></button>
                {!! Form::close() !!}
            </div>
        </div>    
        
        @if ($user->isTutor())
            {!! Form::model($profile, ['method' => 'PATCH', 'route' => ['profile.edit', 'user_ref_id' => $profile->user->ref_id], 'class' => 'form-horizontal']) !!}
                <div id="details" class="profile-edit-container add-list-container">
                    <div class="profile-edit-header fl-wrap">
                        <h4 style="color:black">Profile Details</h4>
                    </div>
                    <div class="custom-form">
                        <div class="row">
                            <div class="col-sm-6">
                                <label> Your Majors & Minors <i class="fa fa-graduation-cap"></i> <small class="text-danger" style="color:red">{{ $errors->first('major') }}</small>  </label>
                                {!! Form::text('major', null, []) !!}
                            </div>
                            <div class="col-sm-6">
                                <label> Current Education Level <small class="text-danger" style="color:red">{{ $errors->first('tutor_education') }}</small> </label>
                                {!! Form::select('tutor_education', $profile->education_levels(), null, ['id' => 'tutor_education', 'class' => 'chosen-select']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label> Languages You Speak/Teach In <i class="fa fa-comments"></i> <small class="text-danger" style="color:red">{{ $errors->first('languages') }}</small>  </label>
                                {!! Form::text('languages', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-sm-6">
                                <label>Tutoring You Offer</label>
                                <div class="fl-wrap filter-tags" style="margin-top:0px">
                                    <div class="col-md-6">
                                        <label class="inline"> 
                                            <input type="checkbox" name="in_person" value="1" @if ($profile->in_person == 1)
                                                checked
                                            @endif>
                                            <span>&nbsp;&nbsp;In-Person</span> 
                                        </label>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <label class="inline"> 
                                            <input type="checkbox" name="online" value="1" @if ($profile->online === 1)
                                                checked
                                            @endif>
                                            <span>&nbsp;&nbsp;Online</span> 
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Levels You Tutor (only select levels that you are proficient in or that you have experience teaching/tutoring)</label>
                                <div class="fl-wrap filter-tags" style="margin-top:10px">
                                    @foreach ($course_levels as $id => $course_level)
                                        <label for="course-{{$id}}" class="inline">
                                        <input type="checkbox" name="course_levels[]" id="course-{{$id}}" {{in_array($id, $tutor_course_levels) ? "checked" : ""}} value="{{$id}}">
                                        <span> &nbsp;&nbsp;{{$course_level}}</span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label> Start Date <i class="fa fa-calendar"></i> <small class="text-danger" style="color:red">{{ $errors->first('tutor_start_date') }}</small>  </label>
                                {!! Form::date('tutor_start_date', null, ['class' => 'form-control']) !!}

                            </div>
                            <div class="col-sm-6">
                                <label> End Date (do not leave blank) <i class="fa fa-calendar"></i> <small class="text-danger" style="color:red">{{ $errors->first('tutor_end_date') }}</small>  </label>
                                    {!! Form::date('tutor_end_date', null, ['class' => 'form-control']) !!}
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-6">
                                <p>Create a Google Calendar, <b>make it Public</b>, and include the Calendar ID below. {{-- </p>
                                <p> --}}
                                    <u><a href="https://support.google.com/calendar/answer/37083" target="_blank">How To Create A Public Calendar</a></u> and
                                    <u><a href="https://help.risevision.com/hc/en-us/articles/115002181123-Make-a-Google-Calendar-public-and-get-the-Calendar-ID" target="_blank">Finding Your Google Calendar ID</a></u>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <label> Google Calendar ID <i class="fa fa-calendar"></i> <small class="text-danger">{{ $errors->first('calendar_id') }}</small> <small class="text-danger" style="color:red">{{ $errors->first('calendar_id') }}</small>  </label>
                                {!! Form::text('calendar_id', null, ['placeholder' => 'e.g. abc123-xyz@group.calendar.google.com or me@gmail.com']) !!}
                                <br>
                                <label>Time Zone</label>
                                {!! Form::select('timezone', $timezones, null, ['id' => 'timezone', 'class' => 'chosen-select']) !!}
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <div class="edit-profile-photo fl-wrap">
                                <div class="col-sm-6">
                                    <img src="{{$profile->avatar ? asset('btn/avatars/'.$profile->avatar) : asset('btn/avatars/default.jpg')}}" {{-- class="respimg" --}} width="200px">
                                </div>
                                <div class="col-sm-6">
                                    <p style="color:black">Upload a <u>professional</u> square profile pic (e.g. 250x250 px). Upon review and approval, we'll add the picture to your profile.</p>
                                    <div class="change-photo-btn">
                                        <div class="photoUpload">
                                            <a href="{{$dropbox_avatars}}" target="_blank"><i class="fa fa-upload"></i> Upload Photo</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <button class="btn  big-btn  color-bg flat-btn">Update Profile <i class="fa fa-angle-right"></i></button>
                        </div>
                        <div class="row" style="margin-top:30px">
                            <div class="col-sm-12">
                                <div class="profile-edit-header fl-wrap">
                                    <h4 style="color:black">Session Details</h4>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label>Status</label>    
                                    </div>
                                    <div class="col-sm-12 col-md-8 ">
                                        <div class="col-xs-6">
                                            <div class="add-list-media-header">
                                                <label class="radio inline"> 
                                                    <input type="radio" name="tutor_status" value="1" @if ($profile->tutor_status === 1)
                                                        checked
                                                    @endif>
                                                    <span>Accepting Students</span> 
                                                </label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-xs-6">
                                            <div class="add-list-media-header">
                                                <label class="radio inline"> 
                                                    <input type="radio" name="tutor_status" value="0" @if ($profile->tutor_status === 0)
                                                        checked
                                                    @endif>
                                                    <span>Not Accepting Students</span> 
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6" style="margin-top: 30px">
                                    <label>Group Tutoring Size: Most sessions are one-on-one, but list max number of students you will work with in a session. (1 = Individual session)<small class="text-danger" style="color:red">{{ $errors->first('group_size') }}</small></label>
                                    {!! Form::number('group_size', $profile->group_size ?? 1, ['min' => 1]) !!}
                                </div>
                            </div>
                            <div class="col-sm-12" style="margin-top: 30px">
                                <label> When you can meet for sessions (Daytime, Evenings, Nights = weekdays): <small class="text-danger" style="color:red">{{ $errors->first('tutor_availability') }}</small>  </label>
                                <div class="fl-wrap filter-tags" style="margin-top:10px">
                                    @foreach ($profile->tutor_availability_options() as $key => $availability)
                                        <label for="{{$key}}" class="inline">
                                        <input type="checkbox" name="tutor_availability[]" id="{{$key}}" {{in_array($key, $tutor_availability) ? "checked" : ""}} value="{{$key}}">
                                        <span> &nbsp;&nbsp;{{$availability}}</span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="profile-edit-header fl-wrap" style="padding:0px 25px">
                                    <h4 style="color:black">Earnings</h4>
                                </div>
                            <div class="col-sm-12">
                                <p><b style="color:darkred">Please enter your desired take home earnings (your net income after our service fee has been deducted).</b></p>
                            </div>
                            <div class="col-sm-12 col-md-8">
                                <div class="col-xs-6">
                                    <label>Min Earnings ($/hr) <small class="text-danger" style="color:red">{{ $errors->first('rate_min') }}</small></label>
                                    {!! Form::text('rate_min', null, ['placeholder' => 'Min Rate']) !!}
                                </div>
                                <div class="col-xs-6">     
                                    <label>Max Earnings ($/hr) <small class="text-danger" style="color:red">{{ $errors->first('rate_max') }}</small></label>
                                    {!! Form::text('rate_max', null, ['placeholder' => 'Min Rate']) !!}
                                </div>    
                            </div>
                            <div class="col-sm-12">
                                <label>Details (Optional) <small class="text-danger" style="color:red">{{ $errors->first('rate_details') }}</small></label>
                                {!! Form::textarea('rate_details', null, ['class' => 'form-control', 'size'=> '40x4', 'placeholder' => 
'Examples:
1) My range is $20-$30.  $20/hr if the student wants to meet multiple times in a week, $30/hr for the following upper division courses: ...
                            
2) My group rate is $20/hr per student for a group of 2 and $15/hr per student for groups of  3 or more."
                            
3) My rate for online tutoring is $15-20/hr.']) !!}
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <button class="btn  big-btn  color-bg flat-btn">Update Profile <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                </div>
                
                <div id="experience" class="profile-edit-container add-list-container">
                    <div class="profile-edit-header fl-wrap">
                        <h4 style="color:black">Experience</h4>
                    </div>
                    <div class="custom-form">
                        <div class="row">
                            <div class="col-sm-12">   
                                <p style="color:black">Briefly describe your teaching/tutoring experience and academic accomplishments.  Use this section to demostrate your proficiency in the areas you wish to tutor.  Write this in 3rd person, avoid "I", "you", etc.  Be sure to include any experience as a test prep (e.g. SAT) instructor or tutor.  </p>

                                <p style="color:black">Keep this section professional and relevant.  <b>Do NOT include your full name, contact info or external links of any kind</b>.  We will shut down any account that includes this information in the profile.  </p>

                                <p style="color:darkred">If you offer online tutoring, be sure to breifly describe the setup, e.g. the platform you use (e.g. Skype, Zoom), online tools you use (e.g. a shared whiteboard), how you provide students with a copy of the lesson or material discussed (e.g. Google Docs).  Your selected platform must be <b>FREE</b> for students to use.</p>


                                <label>Experience <small class="text-danger" style="color:red">{{ $errors->first('tutor_experience') }}</small></label>
                                {!! Form::textarea('tutor_experience', null, ['placeholder' => 'Previous teaching/tutoring experience']) !!}
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px">
                            <button class="btn  big-btn  color-bg flat-btn">Update Profile <i class="fa fa-angle-right"></i></button>
                        </div>
                    </div>
                </div>
                <div id="courses" class="profile-edit-container add-list-container">
                    <div class="profile-edit-header fl-wrap">
                        <h4 style="color:black">Courses</h4>
                    </div>
                    <div class="custom-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <p style="color:black">Make sure that you list only the courses you have passed our online proficiency test in.  If a course was not included on our exams, include courses that you are proficient in and can teach, not just courses you did well in.</p>  
                                <span style="color:blue">For College Courses</span>
                                <p style="color:black">Search the course catalog for the university (universities) near you for courses you can tutor.  Include both the course number and course name.  </p>
                                <p style="color:black">List the course number as the 2-4 letter department and 3-4 digit course number (do not include section numbers).  Add a space between the department letters and course number.  <b>Example: CHEM 1133 - General Chemistry 2</b> </p>
                                <p style="color:black">OPTIONAL: You may also include commonly used names of the courses, e.g. MATH 161  Calculus for Physical Scientists II (Calc 2)</p>
                                
                                <span style="color:blue">For High/Middle/Elementary School Classes</span>
                                <p style="color:black">Include the class level and a description of the class. For example:  High School Physics, Chemistry, Algebra, Middle School Writing, Elementary School Reading</p>
                                <p style="color:black">Do not simply state High School Math, instead: High School Algebra, Geometry, Precalculus.  </p>
                                
                                <span style="color:blue">For Other Tutoring Areas</span>
                                <p style="color:black">If you are proficient in a software package or programming language and can teach it, be sure to include that.  For example "PHP, C++, MatLab, Mathematica, R"</p>
                                <p style="color:black">Include standardized tests, e.g. SAT Writing, GRE Chemistry, if you are familiar and proficient in both the content and test taking  procedures / strategies.</p>
                                <p style="color:black">Feel free to add General Tutoring/Mentoring if you wish to do that.  We have received requests for tutors who can help with mentoring, organization, study skills.</p>
                                <br>
                                {!! Form::label('courses', 'Course List') !!}
                                {!! Form::textarea('courses', null, ['placeholder' => 
'MATH 1300 / APPM 1350 - Analytic Geometry & Calculus 1 (Calc 1)
PHYS 1110 / PHYS 2010 - General Physics 1

High School Biology

GRE Chemistry
MatLab, R programming, C++']) !!}
                                
                                <small class="text-danger">{{ $errors->first('courses') }}</small>
                            </div>
                        </div>
                        <button class="btn  big-btn  color-bg flat-btn">Update Profile <i class="fa fa-angle-right"></i></button>
                    </div>
                
                </div>    
            {!! Form::close() !!}

            <div id="location" class="profile-edit-container add-list-container">
                <div class="profile-edit-header fl-wrap">
                    <h4 style="color:black">Location</h4>
                </div>
                {!! Form::open(['method' => 'POST', 'route' => ['update.geolocations', $user->ref_id], 'class' => 'custom-form']) !!}
                    <label>Session Locations: Briefly describe where you meet for sessions (e.g. towns, campuses, neightborhoods)<i class="fa fa-map-marker"></i></label>
                    <input type="text" name="session_locations" placeholder="CU Boulder Campus, The Hill, 5 mile radius around Denver" value="{{$profile->session_locations}}"/>

                    <div class="row">
                        <div class="col-md-8">
                            <center><i class="fa fa-plus"></i> Click to add & drag a new location</center>
                            <center><i class="fa fa-times"></i> Double click marker to remove location</center>
                            <div class="map-container">
                                <div id="singleMap" data-geo="{{$geolocations}}" data-center={{$map_center}}></div>
                            </div>        
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <p>Indicate the radius around each location you can meet</p>
                                <div class="col-xs-6">
                                    <div class="add-list-media-header">
                                        <label class="radio inline"> 
                                            <input type="radio" name="radius_unit" value="miles" 
                                            @if ($radius_unit == "miles")
                                                checked
                                            @endif
                                            >
                                            <span>miles</span> 
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="col-xs-6">
                                    <div class="add-list-media-header">
                                        <label class="radio inline"> 
                                           <input type="radio" name="radius_unit" value="km" @if ($radius_unit == "km")
                                                checked
                                            @endif>
                                            <span>km</span>  
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="markers"></div>
                            <div class="row">
                                <button class="btn  big-btn  color-bg flat-btn">Update Location<i class="fa fa-angle-right"></i></button>
                            </div> 
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>		                                        
        @endif
    </div>
@endsection