@extends('layouts.citybook.dashboard')

@section('page_title', 'Payment Settings')

@push('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
@endpush

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('dashboard_content')
	<div class="list-single-main-item fl-wrap" style="text-align:left">
		@if (Auth::user()->customer)
			<div class="row">
				<div class="col-md-4">
					<table class="table">
					    <tbody>
					      <tr>
					        <td><b>Card Type</b></td>
					        <td>{{auth()->user()->customer->card_brand}}</td>
					      </tr>
					      <tr>
					        <td><b>Card #</b></td>
					        <td>xxxx {{str_pad(auth()->user()->customer->card_last_four, 4, 0, STR_PAD_LEFT)}}</td>
					      </tr>
					      <tr>
					        <td><b>Expiration</b></td>
					        <td>{{str_pad(auth()->user()->customer->exp_month, 2, 0, STR_PAD_LEFT)}} / {{auth()->user()->customer->exp_year}}</td>
					      </tr>
					    </tbody>
					  </table>
				</div>
				<div class="col-md-8">
					{!! Form::open(['method' => 'POST', 'route' => 'update.customer.card', 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}
						
			            <div>
			              @include('btn.stripe.card_form_partial')
			            </div>

			            @if (Auth::user()->isTutor())
							<br>
							<div class="form-group">
							    <div class="col-md-10 col-md-offset-1 checkbox{{ $errors->has('permission') ? ' has-error' : '' }}">
							        <label for="permission">
							            {!! Form::checkbox('permission', '1', null, ['id' => 'permission', 'required' => 'required']) !!} By adding this card, I give my express permission to BuffTutor to charge <b>fines resulting from contract violations</b> to this card.
							        </label>
							    </div>
							    <small class="text-danger">{{ $errors->first('permission') }}</small>
							</div>
						@endif
			            <div class="col-sm-4 col-md-4">      
			                <br><br>
			                {!! Form::submit("Update Card", ['class' => 'btn btn-primary']) !!}
			            </div>
		            
		          	{!! Form::close() !!}
				</div>
			</div>
		@else
			{!! Form::open(['method' => 'POST', 'route' => 'store.customer.card', 'id' => 'payment-form','class' => 'form-horizontal']) !!}
				<h4>Add Card To Account</h4>
	            <div>
	              @include('btn.stripe.card_form_partial')
	            </div>

	            @if (Auth::user()->isTutor())
					<br>
					<div class="form-group">
					    <div class="col-md-10 col-md-offset-1 checkbox{{ $errors->has('permission') ? ' has-error' : '' }}">
					        <label for="permission">
					            {!! Form::checkbox('permission', '1', null, ['id' => 'permission', 'required' => 'required']) !!} By adding this card, I give my express permission to BuffTutor to charge <b>fines resulting from contract violations</b> to this card.
					        </label>
					    </div>
					    <small class="text-danger">{{ $errors->first('permission') }}</small>
					</div>
				@endif
	            <div class="col-sm-4 col-md-4">      
	                <br><br>
	                {!! Form::submit("Add Card", ['class' => 'btn btn-primary']) !!}
	            </div>
            
          	{!! Form::close() !!}
		@endif
	</div>
@endsection

@push('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush