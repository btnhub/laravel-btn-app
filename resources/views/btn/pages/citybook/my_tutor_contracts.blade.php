@extends('layouts.citybook.dashboard')

@section('page_title', 'My Tutor Requests')

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('dashboard_content')

	<div class="list-single-main-item fl-wrap">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>My Tutor Requests</span></h3>
	    </div>
	    <div class="row">
	    	<div class="col-md-12">
	    		<p style="color:black">Here are the students we've found for you. Review & respond to each request.</p>

				<p style="color:black"><b>If you can help out,</b> select "Yes" and we'll send the student a link to your profile to review .  The student will then determine whether he/she would like to work with you.</p>

				<p style="color:black"><b>If you cannot help,</b> select "No" and briefly explain why (e.g. schedule conflict, not a course you offer, rate too low, etc).</p>

				<p style="color:black"><b>Please do not ignore these requests!</b> We may stop sending them to you if you do not respond in a timely manner.</p>
				
				<p style="color:black"><b style="color:darkgreen">Net Earnings are your take home earnings.  Our service fee has already been deducted. </b></p>

				<p style="color:black"></p>
				<hr>
				<p style="color:black"></p>
	    	</div>
	    </div>
	    <div class="accordion">
	        <a class="toggle act-accordion" href="#"> Unreviewed Requests ({{$unreviewed_tutor_contracts->count()}}) <i class="fa fa-angle-down"></i></a>
	        <div class="accordion-inner visible" style="overflow-x: auto;padding:15px 0px">
	        	@include('btn.student_tutor_contracts.tutors.table_unreviewed_requests', ['tutor_contracts' => $unreviewed_tutor_contracts])
	        </div>

	        <a class="toggle" href="#"> Declined Requests ({{$declined_tutor_contracts->count()}}) <i class="fa fa-angle-down"></i></a>
	        <div class="accordion-inner" style="overflow-x: auto;padding:15px 0px">
	        	@include('btn.student_tutor_contracts.tutors.table_declined_requests', ['tutor_contracts' => $declined_tutor_contracts])	
	        </div>

	        <a class="toggle" href="#"> Accepted Requests ({{$accepted_tutor_contracts->count()}}) <i class="fa fa-angle-down"></i></a>
	        <div class="accordion-inner" style="overflow-x: auto;padding:15px 0px">
	        	@include('btn.student_tutor_contracts.tutors.table_accepted_requests', ['tutor_contracts' => $accepted_tutor_contracts])
	        </div>

	        <a class="toggle" href="#"> Missed Requests ({{$missed_tutor_contracts->count()}}) <i class="fa fa-angle-down"></i></a>
	        <div class="accordion-inner" style="overflow-x: auto;padding:15px 0px">
	        	@include('btn.student_tutor_contracts.tutors.table_missed_requests', ['tutor_contracts' => $missed_tutor_contracts])
	        </div>
	    </div>
	</div>
@endsection

@push('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush