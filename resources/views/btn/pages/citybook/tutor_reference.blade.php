@extends('layouts.citybook.default')

@section('title')
Tutor Reference
@endsection

@section('content')
	<section class="parallax-section single-par list-single-section" data-scrollax-parent="true" id="sec1">
	    <div class="bg par-elem "  data-bg="{{asset('btn/images/BuffTutor-Group-of-Students.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
	    <div class="overlay"></div>
	    <div class="bubble-bg"></div>
	    <div class="list-single-header absolute-header fl-wrap">
	        <div class="container">
	            <div class="list-single-header-item">
	                <h2>Reference For {{$applicant->full_name}}</h2>
	                <span class="section-separator"></span>
	            </div>
	        </div>
	    </div>
	</section>
	<div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav scroll-init">
                <ul>
                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                    <li><a href="#contact-info">Contact Info</a></li>
                    <li><a href="#reference-details">Reference</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!--  section  --> 
    <section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- profile-edit-container--> 
                    <div class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>Tell Us About {{$applicant->full_name}}</h4>
                            <p style="color:black">{{$applicant->first_name}} recently applied to join <u><a href="{{url('/')}}" target="_blank">BuffTutor</a></u>. We thoroughly vet all applicants and one part of the process is feedback from previous students.  Please submit the following form to tell us about your experience working with {{$applicant->first_name}}. </p>

                            <p><b style="color:darkblue">Please be brutally honest!</b></p>
                        </div>
                    </div>
                   	{!! Form::open(['method' => 'POST', 'route' => ['submit.reference.form', $applicant->ref_id]]) !!}
                   		<div id="contact-info" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>Contact Info</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>First Name<i class="fa fa-user"></i></label>
	                                    <input type="text" name="first_name" placeholder="Your First Name" value="" required/>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Last Name<i class="fa fa-user"></i></label>
	                                    <input type="text" name="last_name" placeholder="Your Last Name" value="" required/>
	                                </div>    
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>Email<i class="fa fa-envelope-o"></i></label>
	                                    <input type="text" name="email" placeholder="Your Email Address" value="" required/>     
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Phone<i class="fa fa-phone"></i></label>
	                                    <input type="text" name="phone" placeholder="Your Phone Number" value="" required/>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end--> 

	                    <!-- profile-edit-container--> 
	                    <div id="reference-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>Reference</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>How do you know {{$applicant->first_name}}?<i class="fa fa-users"></i></label>
	                                    <input type="text" name="relationship" placeholder="" value="" required/>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>When and how long did you work with {{$applicant->first_name}}? <i class="fa fa-clock-o"></i></label>
	                                    <input type="text" name="duration" placeholder="" value="" required/>
	                                </div>    
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>What did {{$applicant->first_name}} teach? Please include the level<i class="fa fa-university"></i></label>
	                                    <input type="text" name="course" placeholder="E.g. High School Chemistry" value="" required/>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>How did {{$applicant->first_name}} improve your academic success?<i class="fa fa-graduation-cap"></i></label>
	                                    <input type="text" name="effect" placeholder="" value="" required/>
	                                </div>    
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>{{$applicant->first_name}}'s Strengths</label>
	                                    <p>
	                                        Describe what {{$applicant->first_name}} does well.
	                                    </p>
	                                    <textarea name="strengths" cols="40" rows="3" placeholder="What makes {{$applicant->first_name}} a great teacher" required></textarea> 
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Areas Of Improvement</label>
	                                    <p>
	                                        List what {{$applicant->first_name}} needs to improve on. 
	                                    </p>
	                                    <textarea name="improvements" cols="40" rows="3" placeholder="What {{$applicant->first_name}} could do better" required></textarea> 
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-12">
	                                    <div class=" fl-wrap filter-tags">
	                                        <input id="confirm" type="checkbox" name="confirm" required>
	                                        <label for="confirm">The provided information is honest and accurate.  I have not been asked by {{$applicant->first_name}} to falsify any information.</label>
	                                    </div>         
	                                </div>

	                                <div class="add-comment custom-form">
	                                    <button class="btn  big-btn  color-bg flat-btn">Submit Reference <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>    
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->
                   	{!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!--  section end --> 
    <div class="limit-box fl-wrap"></div>
@endsection