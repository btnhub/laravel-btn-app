@extends('layouts.citybook.default')

@section('meta-description')
	Love teaching?  Join BuffTutor today!  We're always hiring passionate tutors.
@endsection

@section('title')
Become A Tutor
@endsection

@push('header')
    {{--Test Google structure: https://search.google.com/structured-data/testing-tool/u/0/?utm_campaign=devsite&utm_medium=jsonld&utm_source=job-posting--}}
    <?php
        $job_titles = ["College Math Tutor Jobs", "College Algebra / Precalculus Tutor Jobs", "College Geometry / Trigonometry Tutor Jobs", "Calculus Tutor Jobs", "Differential Equations / Linear Algebra Tutor Jobs", "Chemistry Tutor Jobs", "Organic / Inorganic / Physical Chemistry Tutor Jobs", "Physics Tutor Jobs", "Classical / Quantum Mechanics Physics Tutor Jobs", "Electricity & Magnetism Physics Tutor Jobs", "Astronomy Tutor Jobs", "Astrophysics Tutor Jobs", "Biology Tutor Jobs", "Genetics Tutor Jobs", "Microbiology / Molecular / Cell Biology Tutor Jobs", "Anatomy / Physiology Tutor Jobs", "College Economics / Accounting / Finance Tutor Jobs", "Statistics Tutor Jobs"/*, "Physical Chemistry Tutor Jobs", "Computer Science Tutor Jobs", "Thermodynamics Tutor Jobs", "Fluid Dynamics Tutor Jobs", "Statics Tutor Jobs", "Solid Mechanics Tutor Jobs", "Mechanical Engineering Tutor Jobs", "Chemical Engineering Tutor Jobs", "Electrical / Computer Engineering Tutor Jobs", "Aeronautical /Aerospace Engineering Tutor Jobs", "Biomedical Engineering Tutor Jobs", "Civil Engineering Tutor Jobs", "Material Science Engineering Tutor Jobs", "Biochemical Engineering Tutor Jobs", "Software Engineering Tutor Jobs", "Abstract / Linear Algebra Tutor Jobs", "Discrete Math Tutor Jobs", "Analytical Chemistry Tutor Jobs", "Inorganic Chemistry Tutor Jobs", "College Ecology Tutor Jobs", "Writing Tutor Jobs", "Humanities Tutor Jobs", "College Finance Tutor Jobs", "College Language Tutor Jobs", "College Spanish Tutor Jobs" */];

        $job_titles = array_reverse($job_titles);
        
        $locations = App\Location::tutorsVisible()->where('city', '!=', 'Online')->where('company_name', '!=', 'RamTutor' )->get();
        
        //$final_array = [];
        //foreach ($locations as $location) {
            //$location_details = ["@type"  => "Place",
            //                        "address" => ["@type" => "PostalAddress",
            //                                        "addressLocality" => $location->city,
            //                                        "addressRegion" => $location->state,
            //                                        "addressCountry" => "US"]
            //                        ];
            //array_push($final_array, $location_details);
        //}
        //$location_json = json_encode($final_array);
    ?>

    @foreach($job_titles as $job_title)
        @foreach ($locations as $location)
            <script type="application/ld+json">
            {
                "@context" : "https://schema.org/",
                "@type" : "JobPosting",
                "title" : "{{$location->city}} {{$job_title}}",
              
                "description" : "<p>BuffTutor is dedicated to providing exceptional academic assistance to college and high school students.  To that end, we are hiring experienced tutors with a passion for teaching.  We focus on in-person tutoring and we are primarily looking for Math, Science, Engineering & Economics tutors.  </p><p>To quickly and easily connect with students, apply today! Visit our website and carefully review our eligibility criteria.</p> <p>Anyone can call themselves a tutor, only the best can be a BuffTutor!</p><p><a href='{{route('become.tutor')}}'>Become A Tutor</a></p><p></p><p>Why Join:<ul><li>Quickly and easily connect with students in {{$location->city}} for in-person sessions.</li><li>Find extra students at one of our other locations to tutor online.</li><li>Set your own rate (tutors typically earn $15-40+/hr)</li><li>Set your own schedule</li><li>Take on as many or as few students as you'd like</li><li>We handle all the messy stuff (client acquisition, billing, payment processing, student management) so you can focus on being the BEST TUTOR EVER!!!</li></ul></p><p><a href='{{route('become.tutor')}}'>Become A Tutor</a></p>",
              
                "datePosted" : "{{Carbon\Carbon::now()->subDays(2)}}",
                "employmentType" : ["PART_TIME", "CONTRACTOR"],
                "hiringOrganization" : {
                    "@type" : "Organization",
                    "name" : "BuffTutor - {{$location->city}} University Tutors",
                    "url" : "https://bufftutor.com/tutors/become-tutor",
                    "logo": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg"
                },

                "jobLocation": {
                    "@type": "Place",
                    "address": {
                        "@type" : "PostalAddress",
                        "addressLocality" : "{{$location->city}}",
                        "addressRegion" : "{{$location->state}}",
                        "addressCountry" : "US"
                    },
                    "geo" : {
                        "@type" : "GeoCoordinates",
                        "latitude" : "{{$location->lat}}",
                        "longitude" : "{{$location->lng}}"
                    }
                }           
            }
        </script>
        @endforeach
        {{-- <script type="application/ld+json">
            {
              "@context" : "https://schema.org/",
              "@type" : "JobPosting",
              "title" : "{{$job_title}}",
              
              "description" : "<p>BuffTutor is dedicated to providing exceptional academic assistance to college and high school students.  To that end, we are hiring experienced tutors with a passion for teaching.  We focus on in-person tutoring and we are primarily looking for Math, Science, Engineering & Economics tutors.  </p><p>To quickly and easily connect with students, apply today! Visit our website and carefully review our eligibility criteria.</p> <p>Anyone can call themselves a tutor, only the best can be a BuffTutor!</p>",
              
              "datePosted" : "{{Carbon\Carbon::now()->subDays(2)}}",
              "employmentType" : ["PART_TIME", "CONTRACTOR"],
              "hiringOrganization" : {
                "@type" : "EducationalOrganization",
                "name" : "BuffTutor",
                "sameAs" : "http://www.BuffTutor.com",
                "logo": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg"
              },

              "jobLocation": {!!$location_json!!}
            }
        </script> --}}
    @endforeach
    @foreach($job_titles as $job_title)
        <script type="application/ld+json">
            {
                "@context" : "https://schema.org/",
                "@type" : "JobPosting",
                "title" : "Fort Collins {{$job_title}}",
              
                "description" : "<p>RamTutor is dedicated to providing exceptional academic assistance to college and high school students in Fort Collins.  To that end, we are hiring experienced tutors with a passion for teaching.  We focus on in-person tutoring and we are primarily looking for Math, Science, Engineering & Economics tutors.  </p><p>To quickly and easily connect with students, apply today! Visit our website and carefully review our eligibility criteria.</p> <p>Anyone can call themselves a tutor, only the best can be a RamTutor!</p>",
              
                "datePosted" : "{{Carbon\Carbon::now()->subDays(2)}}",
                "employmentType" : ["PART_TIME", "CONTRACTOR"],
                "hiringOrganization" : {
                    "@type" : "EducationalOrganization",
                    "name" : "RamTutor",
                    "sameAs" : "http://www.RamTutor.com"
                },

                "jobLocation": {
                    "@type": "Place",
                    "address": {
                        "@type": "PostalAddress",
                        "addressLocality": "Fort Collins",
                        "addressRegion": "CO",
                        "addressCountry": "US"
                    },
                    "geo" : {
                        "@type" : "GeoCoordinates",
                        "latitude" : "40.57498240",
                        "longitude" : "-105.08386750"
                    }
                },
                "baseSalary": {
                    "@type": "MonetaryAmount",
                    "currency": "USD",
                    "value": {
                        "@type": "QuantitativeValue",
                        "minValue": 15.00,
                        "maxValue": 30.00,
                        "unitText": "HOUR"
                    }
                }
            }
        </script>
    @endforeach
@endpush

@section('content')
	<!--section --> 
    <section class="parallax-section" data-scrollax-parent="true" id="sec1">
        <div class="bg par-elem "  data-bg="{{asset('btn/images/BuffTutor-Become-A-Tutor.jpg')}}"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align">
                <h1><span>Become A Tutor</span></h1>
                
                <span class="section-separator"></span>
            </div>
        </div>
    </section>
    <!-- section end -->
    <div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav scroll-init inline-scroll-container">
                <ul>
                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                    <li><a href="#how-it-works">How It Works</a></li>
                    <li><a href="#eligibility">Benefits & Eligibility</a></li>
                    <li><a href="#faqs">FAQs</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <!--section --> 
    <!--section -->  
    <section  id="how-it-works">
        <div class="container">
            <div class="section-title">
                <h2> How It Works</h2>
                <div class="section-subtitle">How It Works</div>
                <span class="section-separator"></span>
                <p>Quickly and easily connect with students in your area.</p>
            </div>
            <div class="time-line-wrap fl-wrap">
                <!--  time-line-container  --> 
                <div class="time-line-container">
                    <div class="step-item">Step 1</div>
                    <div class="time-line-box tl-text tl-left">
                        <span class="process-count">01 . </span>
                        <div class="time-line-icon">
                            <i class="fa fa-map-o"></i>
                        </div>
                        <h3>Join Us!</h3>
                        <p>Have what it takes to be a BuffTutor?  </p>
                        <p>By carefully handpicking tutors, we have become a reliable source of exceptional tutors for students and parents. We review tutors' academic performance, previous teaching experience, performance on our (purposefully) tough proficiency exams. <a href="{{route('tutor.apply')}}">Apply Today!</a></p>
                    </div>
                    <div class="time-line-box tl-media tl-right">
                        <img src="{{asset('btn/images/BuffTutor-Apply-Online.jpg')}}" alt="">
                    </div>
                </div>
                <!--  time-line-container --> 
                <!--  time-line-container  --> 
                <div class="time-line-container lf-im">
                    <div class="step-item">Step 2</div>
                    <div class="time-line-box tl-text tl-right">
                        <span class="process-count">02 . </span>
                        <div class="time-line-icon">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <h3>Review Tutor Requests & Select Students</h3>
                        <p>We take care of all the hard work.  Our tutors simply receive an e-mail alert when a new request comes.  Tutors are free to accept the students they want to work with.  Tutors are under no pressure to accept all proposals they receive, they simply select the ones that work best.</p>
                    </div>
                    <div class="time-line-box tl-media tl-left">
                        <img src="{{asset('btn/images/btn-tools/review-tutor-requests.png')}}" alt="BuffTutor - Easily Find Students" style="border:2px solid lightgray;padding:15px">
                    </div>
                </div>
                <!--  time-line-container -->                             
                <!--  time-line-container  --> 
                <div class="time-line-container">
                    <div class="step-item">Step 3</div>
                    <div class="time-line-box tl-text tl-left">
                        <span class="process-count">03 . </span>
                        <div class="time-line-icon">
                            <i class="fa fa-usd"></i>
                        </div>
                        <h3>Get To Work</h3>
                        <p>Once you've been matched with a student, what are you waiting for?  Get to work!  You focus on being the best tutor ever, and we'll handle the rest.  Our online payment system makes it easy for tutors to get paid after each session.</p>
                    </div>
                    <div class="time-line-box tl-media tl-right">
                        <img src="{{asset('btn/images/student_teacher_studying.jpg')}}" alt="">
                    </div>
                </div>
                <!--  time-line-container -->                             
                <div class="timeline-end"><i class="fa fa-check"></i></div>
            </div>
        </div>
    </section>
    <!-- section end --> 

    <!--section -->  
    <section class="color-bg" id="eligibility" style="padding-top:30px">
        <div class="shapes-bg-big"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="color-bg-text">
                        <h3>Benefits of Joining BuffTutor</h3>
                        <p><i class="fa fa-check"></i> Set your own hours and schedule.</p>
                        <p><i class="fa fa-check"></i> Set your own rate.</p>
                        <p><i class="fa fa-check"></i> Take on as many or as few students as you want or can handle.</p>
                        <p><i class="fa fa-check"></i> Work wherever you want.  Don't commute if you don't want to!</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="color-bg-text">
                        <h3>Eligibility Criteria</h3>
                        <p><i class="fa fa-check"></i> Have <b>at least one year (or 2 semesters)</b> of teaching/tutoring experience</p>
                        <p><i class="fa fa-check"></i> Have <b>3.0 or better major GPA (out of 4.0)</b> in your field of study, not just a 3.0 cumulative GPA</p>
                        <p><i class="fa fa-check"></i> Have completed the course you wish to tutor and have earned at least a <b>B or better</b> in the course.</p>
                        <p><i class="fa fa-check"></i> Be able to work with assigned students <b>through the end of the semester</b> and be around until the end of finals. If you have plans to travel during the semester, you cannot be away for more than 7 days in the semester (this does not include school holidays).</p>
                        <p><i class="fa fa-check"></i> Have work permissions to work in the country (we cannot sponsor tutors) and permission to work in an off-campus position (f you're not sure, contact your advisor).  </p>
                    </div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-4 col-md-offset-5">
            	<a href="{{route('tutor.apply')}}" class="color-bg-link" style="">Apply Now!</a>
            	</div>
            </div>
        </div>
    </section>
    <!-- section end -->
    <!--section -->  
    <section class="gray-bg" id="faqs" style="background-color: #e9e9e9">
        <div class="container">
            <div class="section-title">
                <h2> FAQs</h2>
                <div class="section-subtitle">Common Questions</div>
                <span class="section-separator"></span>
                <p>Commonly asked questions & concerns.</p>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="accordion">
                        <a class="toggle act-accordion" href="#"> I'm an international student, can I join?  <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner visible">
                            <p>It depends. All tutors must have work permissions to join BuffTutor.  We do not sponsor tutors.</p>
                        </div>
                        <a class="toggle" href="#"> How soon will I get a student?  How many hours can I expect? <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>We do not guarantee hours.  All we can guarantee is that we work hard to get tutors as many students as they want.  That said, Math/Science courses are requested the most.  While we do get requests for Humanities courses, they are much fewer.</p>
                        </div>
                        <a class="toggle" href="#"> I am a teaching assistant for a course this semester, can I tutor it? <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>That's a hard NO!  During the semester you are employed (in any capacity) to assist with a course, you cannot tutor that course at that school. You are welcome to tutor that course in a following semester, as long as you are not employed to assist with that course. </p>
                        </div>
                        <a class="toggle" href="#"> How do I get paid? How do I pay for BuffTutor's services?<i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>We use Stripe to charge students and pay tutors.  To protect tutors, we require all students to add a credit/debit card to their account.  After each session, the tutor uses our platform to charge the student's card.  We then deduct our service fee and immediately forward the funds to the tutor.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="accordion">
                        <a class="toggle" href="#"> Are there additional fees? <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>No.  Seriously, no.  When we found a student for you, we will forward details of the tutor request to you to review.  Included will be your net earnings.  That's your take home amount, our fees have already been deducted. </p>
                            <p>So, what is our fee? The simple answer is it varies.  Typically, 20-35% (well under industry standard of 40-60%).  We try to keep it low to accomodate what the student can afford and what the tutor deserves to make.  The longer the tutor has been with us, the lower the percent is and the more the tutor earns. </p>
                        </div>
                        
                        <a class="toggle" href="#"> I'm not currently a student, can I join? <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>The real question is, do you love teaching?  If yes, then welcome aboard! As long as you meet our eligibility criteria then you're welcome to apply.</p>
                        </div>
                        <a class="toggle act-accordion" href="#"> I didn't earn a B but I really understand the material, can I tutor the course? <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner visible">
                            <p>Go ahead and apply and point this out on your application.  If you can ace our proficiency exams, then we'll consider overlooking the grade you earned when you took the course.</p>
                        </div>
                        {{-- <a class="toggle act-accordion" href="#"> Can I create a profile page for business <i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner visible">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                        </div>
                        <a class="toggle" href="#">How do I post my listing<i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                        </div>
                        <a class="toggle" href="#"> Coooperation Customers<i class="fa fa-angle-down"></i></a>
                        <div class="accordion-inner">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra.</p>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section end -->
    <div class="limit-box fl-wrap"></div>
@endsection