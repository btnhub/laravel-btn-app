@push('scripts')
<script type="text/javascript" src="{{asset('assets/js/btn/calc_charge.js')}}"></script>
@endpush

<div class="list-single-main-item fl-wrap">
    <div class="list-single-main-item-title fl-wrap">
        <h3>Welcome, <span>{{$tutor->first_name}}!</span></h3>
    </div>
    <center><b>Complete the <a href="{{route('tutor.checklist')}}" style="text-decoration: underline;color:darkblue">Tutor Checklist</a>, then carefully review the <a href="{{route('tutor.orientation')}}" style="text-decoration: underline;color:darkblue">Tutor Orientation</a> page for an account tour and a summary of the tools available.</b></center>
    <br>
    
    <center>Simply click the links to submit forms or to update values on this page.  <b>Note:  Your student will be e-mailed the moment you submit any form listed on this page!</b></center>

    @if ($tutor && $student)
      Looks like you're signed up as a student and a tutor.  The top half of this document has the tools and tables you'll need as a tutor and the bottom half are the tools you'll need as a student.
      <br>
      If you are not a student, <a href="{{route('remove.role', ['user_ref_id' => $student->ref_id, 'role' => 'student'])}}">click here</a>.
      <hr>

    @endif

    @if (!$tutor->isActiveTutor())
        <p></p>
        <hr>
        <p></p>
        <div class="list-single-main-item-title fl-wrap">
            <h3><span>This account is not active!</span></h3>
        </div>
        <p>If you are ready to tutor, <a href="{{route('profile.edit', $tutor->ref_id)}}">edit your profile</a> to provide the missing information.</p>      
    @endif

    @if ($unreviewed_contracts)
      <p style="padding:10px;"><b style="font-size: 1.25em; color:darkred">You have <a href="{{route('my.tutor.contracts', $tutor->ref_id)}}" style="text-decoration:underline;color:darkblue">{{$unreviewed_contracts}} new tutor {{str_plural('request', $unreviewed_contracts)}}</a>!</b></p>
    @endif
</div>

<div class="list-single-main-item fl-wrap">
    <div class="accordion">
        <a class="toggle @if (session('assign_tab'))
            act-accordion
        @endif" href="#"> My Students ({{$assignments_tutors->count()}}) <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner @if (session('assign_tab'))
            visible
        @endif">
            <div class="dashboard-list-box fl-wrap">
                <div class="dashboard-header fl-wrap">
                    <h3>My Students ({{$assignments_tutors->count()}})</h3>
                </div>
                @foreach ($assignments_tutors as $assignment)
                    <?php 
                        $df_assignment = $assignment->student->danielsFundAssignments->where('assignment_id', $assignment->id)->where('tutor_id', $assignment->tutor->id)->first();
                    ?>
                    <div class="dashboard-list">
                        <div class="dashboard-message">
                            <div class="dashboard-message-text" style="padding-left: 5px;margin-left:5px">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <h4>{{$assignment->student->customer && $assignment->end_date->isFuture() ? $assignment->student->full_name : $assignment->student->user_name}} - {{$assignment->course}} {!! $assignment->online ? "<span>(Online)</span>" : null !!} (${{$assignment->tutorContract->tutor_rate}}/hr)</h4>
                                        <div class="booking-details fl-wrap">
                                            <span class="booking-title">Dates</span> :
                                            {{$assignment->start_date->format('m/d/Y')}} - {{$assignment->end_date->format('m/d/Y')}}
                                            @if ($assignment->end_date->diffInDays(Carbon\Carbon::now()) < 14)
                                                <em><span style="color:red"> Ending Soon! </span></em>
                                            @endif
                                        </div>
                                        @if($assignment->end_date->isFuture())
                                            <div class="booking-details fl-wrap">
                                                <span class="booking-title"><i class="fa fa-envelope-o"></i></span> : 
                                                <span class="booking-text"><a href="{{$assignment->student->customer ? 'mailto:' . $assignment->student->email : "#"}}" target="_top">{{$assignment->student->customer? $assignment->student->email : '...waiting for student to add credit card to account...'}}</a></span>
                                                <span class="booking-title" style="padding-left: 10px"><i class="fa fa-phone"></i></span> : 
                                                <span class="booking-text"><a href="{{$assignment->student->customer ? 'tel:'.$assignment->student->phone : "#"}}" target="_top">{{$assignment->student->customer ? $assignment->student->phone : '...'}}</a></span>
                                            </div>
                                        @endif
                                        
                                        @if($df_assignment)
                                            <?php $df_balance = $df_assignment->total - $df_assignment->session_charges->sum('amt_charged'); 
                                                $remaining_hours = $df_balance / $df_assignment->rate;
                                            ?>
                                            <div class="booking-details fl-wrap">
                                                <b>
                                                {!!($remaining_hours < 0) ? "<span style='color:red'>-".number_format($remaining_hours,2)." Hours Remaining</span>" : "<span style='color:green'>".number_format($remaining_hours,2)." Hours Remaining</span>"!!}
                                                </b>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="booking-details fl-wrap" id="charge-show-{{$assignment->ref_id}}" name="charge-btn">
                                            <a  class="trs-btn" style="color:white" onclick="showChargeForm('{{$assignment->ref_id}}');">
                                                <b>Charge {{$assignment->student->first_name}}@if($df_assignment && $remaining_hours > 0)'s Scholarship @endif</b>
                                            </a>
                                        </div>
                                    </div>    
                                </div>
                                
                                <div id="charge-row-{{$assignment->ref_id}}" name="charge-row" class="row" style="display:none;border: 2px solid  #566985;border-radius:5px;padding:10px">
                                    <div class="col-sm-12">
                                        @if($df_assignment && $remaining_hours > 0)
                                            {!! Form::open(['method' => 'POST', 'route' => ['charge.daniels.scholar', 'df_id' => $df_assignment->id], 'id' => "charge-form-$assignment->ref_id", 'class' => 'custom-form', 'onSubmit' => "calcCharge('$assignment->ref_id', {$assignment->tutorContract->tutor_rate});disableSubmitButton('$assignment->ref_id')"]) !!}
                                            <span style="color:darkred;font-weight: bold">Do not use this form to charge a missed session! Please contact us for instructions.</span>
                                        @else
                                            {!! Form::open(['method' => 'POST', 'route' => ['session.charge.customer', 'assign_id' => $assignment->ref_id], 'id' => "charge-form-$assignment->ref_id", 'class' => 'custom-form', 'onSubmit' => "calcCharge('$assignment->ref_id', {$assignment->tutorContract->tutor_rate});disableSubmitButton('$assignment->ref_id')"]) !!}
                                        @endif
                                        
                                            <div class="row" style="margin-bottom:20px">
                                                <div style="text-align: right;font-size:0.75em;color:darkred;font-weight:bold"><span onclick="hideChargeForm('{{$assignment->ref_id}}')">[X] Close</span></div>
                                                <div class="col-md-4 form-group{{ $errors->has('duration') ? ' has-error' : '' }}">
                                                    {!! Form::label('duration', 'Session Duration') !!}
                                                    {!! Form::select('duration',$session_duration_array, null, ['id' =>"duration-$assignment->ref_id", 'class' => 'form-control', 'required' => 'required', 'onChange' => "calcCharge('$assignment->ref_id', {$assignment->tutorContract->tutor_rate})"]) !!}
                                                    <small class="text-danger" style="color:red">{{ $errors->first('duration') }}</small>
                                                </div>  
                                                <div class="col-md-8 form-group{{ $errors->has('outcome') ? ' has-error' : '' }}">
                                                    {!! Form::label('outcome', 'Session Outcome') !!}
                                                    {!! Form::select('outcome',[null=> 'Select An Outcome', 'Student & Tutor Met As Scheduled' => 'Student & Tutor Met As Scheduled', 'Student Cancelled Without 24 Hours Notice' => 'Student Cancelled Without 24 Hours Notice', 'Tutor Cancelled Without 24 Hours Notice' => 'Tutor Cancelled Without 24 Hours Notice', 'Free Session' => 'Free Session'], null, ['id' => "outcome-$assignment->ref_id", 'class' => 'form-control', 'required' => 'required', 'onChange' => "calcCharge('$assignment->ref_id', {$assignment->tutorContract->tutor_rate})"]) !!}
                                                    <small class="text-danger" style="color:red">{{ $errors->first('outcome') }}</small>
                                                </div> 
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4 form-group{{ $errors->has('session_date') ? ' has-error' : '' }}">
                                                    {!! Form::label('session_date', 'Session Date') !!}
                                                    {!! Form::date('session_date', Carbon\Carbon::now(), ['required' => 'required', 'style' => 'padding:15px']) !!}
                                                    <small class="text-danger" style="color:red">{{ $errors->first('session_date') }}</small>
                                                </div>
                                                <div class="col-md-4 form-group{{ $errors->has('tutor_pay') ? ' has-error' : '' }}">

                                                    {!! Form::label('tutor_pay', 'Your Earnings') !!}
                                                    <span style="color:darkgreen;font-weight:bold;font-size:1.25em"><span name="net-pay" id="tutor_pay-{{$assignment->ref_id}}">$0</span></span>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="add-comment custom-form">
                                                        <button id="button-{{$assignment->ref_id}}" class="btn big-btn  color-bg flat-btn"><b>Charge {{$assignment->student->first_name}}@if($df_assignment && $remaining_hours > 0)'s Scholarship @endif</b></button>
                                                    </div>    
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>    
                                </div>
                                
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        
        <a class="toggle" href="#"> Session Charges ({{$tutor->activeTutorSessions->count()}})<i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <div style="overflow-x: auto">
                @include('btn.tables.sessions_tutor')     
            </div>
        </div>

        <a class="toggle" href="#"> Evaluations ({{$tutor->evaluations->count()}})<i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <div style="overflow-x: auto">
                @include('btn.tables.evaluations_tutor')      
            </div>
        </div>

        <a class="toggle" href="#"> Refunds ({{$tutor->refunds->count()}})<i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <div style="overflow-x: auto">
                @include('btn.tables.refunds_tutor')      
            </div>
        </div>
    </div>
</div>
