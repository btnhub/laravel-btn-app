<div class="list-single-main-item fl-wrap">
    <div class="list-single-main-item-title fl-wrap">
        <h3>Tutor Application Status</h3>
    </div>
    <p>Please address the items below to complete your application package.</p>
    
    <div class="accordion">
        <a class="toggle" href="#"> <b>Step 1:</b> Academic Transcripts <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <p style="color:black">Please upload your most recent academic transcript (an unofficial one is sufficient). Also include any transcript that shows your performance in the courses you wish to tutor.  <b>Do not upload high school transcripts.</b></p>
            
            <p><u><a href="{{$dropbox_transcripts}}" target="_blank">Upload Transcripts Here</a></u></p>
        </div>
        <a class="toggle" href="#"> <b>Step 2:</b> Proficiency Exams ({{$previous_exams->count()}})<i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <h3><b>Proficiency Exam List</b></h3>
            <p style="color:black">To demonstrate your proficiency in the courses you wish to tutor, pass the following exams.  You will have 3 hours to complete the exam.  </p>
            {{-- <p style="color:black"><b>***If you leave this page open for longer than 3 hours, refresh the page before you start an exam or else you will receive an error message. *** </b></p> --}}
            <p style="color:black">If you tutor courses that are in a series, take the exam for the highest level course in the series that you tutor.  For example, if you tutor Calc 1-3, just take Calc 3. </p>
            <p style="color:black">If the courses you offer are not listed below, proceed to the next step.</p>

            @if ($previous_exams->count())
            	<div class="row" style="margin:20px 30px">
	            	<div class="dashboard-list-box fl-wrap" style="border:1px solid lightgray">
		                <div class="dashboard-header fl-wrap" style="background:lightgray">
		                    <h3>Your Results</h3>
		                </div>
		                <div class="dashboard-list">
		                    <div class="dashboard-message">
		                        <div class="dashboard-message-text" style="padding-left: 5px;margin-left:5px">
	                				@foreach ($previous_exams as $previous_exam)            	
	                            		<h4>{{$previous_exam->exam}} - {{$previous_exam->grade}}% &nbsp; <span style="font-size: 0.75em">({{$previous_exam->created_at->diffForHumans()}})</span></h4>
	                            	@endforeach
		                        </div>
		                    </div>
		                </div>
		            </div>
	            </div>	
	            <hr>
            	<br>
            @endif
            

            <h3><b>Select An Exam:</b></h3>
            <br>
            {!! Form::open(['method' => 'POST', 'route' => 'load.selected.exam', 'id' =>'start-exam']) !!}
            
            	@foreach (collect($exam_list)->chunk(3) as $exam_chunk)
			      <div class="row" style="background:#f8fbfe;padding:20px;border:2px solid #ddd;">
			        @foreach ($exam_chunk as $dept => $exams)
			          <div class="col-md-4 col-sm-4">        
			            <b>{{ucfirst($dept)}}</b>
			            
			            <div class="radio{{ $errors->has('exam') ? ' has-error' : '' }}">
			                @foreach ($exams as $exam => $exam_name)
			            
			            		<div class="add-list-media-header" style="border:none">
		                            <label class="radio inline" style="color:black;font-weight:500"> 
		                            	<input type="radio" name="exam" value="{{$exam}}" required>
		                            	<span>{{$exam_name}}</span> 
		                            </label>
		                        </div>
			                  <br>
			                @endforeach
			                <small class="text-danger">{{ $errors->first('exam') }}</small>
			            </div>
			          </div>
			        @endforeach
			      </div>
			    
			    @endforeach    
            	<div class="add-comment custom-form">
		        	<button class="btn big-btn  color-bg flat-btn">Start Exam</button>  
		    	</div>

            {!! Form::close() !!}
        </div>

        {{-- <a class="toggle" href="#"> <b>Step 3:</b> Student References ({{$references->count()}})<i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <p style="color:black">We would like to hear from your students.  Please e-mail the following link to 2-3 of your previous students or parents of previous students. If you did not work one-on-one with students, e-mail the link to your supervisor. <u><a href="{{route('get.reference.form', $user->ref_id)}}" target="_blank">{{route('get.reference.form', $user->ref_id)}}</a></u></p>
            <p></p>
            <p></p>
            <p style="color:black">{{$references->count()}} {{str_plural('Reference', $references->count())}} Submitted</p>
            @foreach ($references as $reference)
                <p>{{$reference->first_name}} {{$reference->last_name}}</p>
            @endforeach
        </div>
        <a class="toggle" href="#"> <b>Step 4:</b> ID Verification <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
            <p style="color:black">To verify your identification, please provide one of the following: a copy of a photo ID (e.g. driver's license or state ID, passport) <u>or</u> a link to a professional page, such as:</p>
            
            <p>Social media account (e.g. LinkedIn)</p>
            <p>Your profile or page on your school's or your company's website.</p>
            <p>Your profile on a different tutoring website.</p>
            
            <p></p>
            <hr>
            <p></p>
            <p style="color:black">1) <u><a href="{{$dropbox_applicant_ids}}" target="_blank">Upload Photo ID Here</a></u></p>
            @if (!$application->professional_link)
                <p></p>
                <p></p>
                {!! Form::open(['method' => 'POST', 'route' => ['add.prof.link', $user->ref_id], 'class' => 'form-horizontal']) !!}
                
                    <div class="custom-form">
                        <div class="row">
                            <div class="col-sm-6">
                                <label style="color:black">2) Professional URL<i class="fa fa-link"></i></label>
                                <input type="text" name="professional_link" placeholder="FULL URL (include http)" value="" required />
                            </div>
                            <div class="col-sm-6">
                                <div class="add-comment custom-form">
                                    <button class="btn big-btn color-bg flat-btn" style="background: #566985">Submit Link</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            @else
                <p style="color:black">2) Professional Page: <u><a href="{{$application->professional_link}}" target="_blank">{{$application->professional_link}}</a></u></p>
            @endif          
        </div> --}}

        @if (!$application->submitted && $application->offered !== 1)
            <div class="add-comment custom-form" style="margin-bottom: 30px">
                <p>Once you've completed the steps above, submit your application package for review. If you receive an offer, complete the steps below.</p>
                {!! Form::open(['method' => 'POST', 'route' => ['submit.application.package', $user->ref_id]]) !!}
                
                    <button class="btn big-btn color-bg flat-btn col-xs-offset-3 col-sm-offset-4" style="background: #566985">Submit Completed Package <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                
                {!! Form::close() !!}
            </div>
        @else
            <p style="color:black">Application Status: 
                @if ($application->offered === 1)
                    <span style="color:green">Approved!</span></p>
                    <p>Please proceed with the steps below.
                @elseif($application->offered === 0)
                    <span style="color:red">Denied.</span></p> 
                    <p>Unfortunately we will not be moving forward with your application. Common reasons include: inadequate or irrelevant teaching experience, a lack of demand for the courses you tutor, failing proficiency exams, bad grades, poor references.
                @else
                    <span style="color:darkblue">Under Review</span> </p>
                    <p>Submitted {{$application->submitted->format('m/d/Y')}}.
                @endif
            </p>
            
        @endif
        
        @if ($application->offered !== 0)
            <a class="toggle @if (session('background_tab'))
                act-accordion
            @endif" href="#"> <b>Step 3:</b> Pass Background Check (Fee Applies)<i class="fa fa-angle-down"></i></a>
            <div class="accordion-inner @if (session('background_tab'))
                visible
            @endif">
                @if ($user->profile->background_check != NULL)
                    <p>Background Check Passed on {{$user->profile->background_check->format('m/d/Y')}}.</p>

                @elseif(($user->backgroundCheck ?? NULL) && $user->backgroundCheck->paid == 1 && $user->backgroundCheck->cleared == 0)
                    <p>Payment submitted on {{$user->backgroundCheck->payment_date->format('m/d/Y')}}</p>

                    <p><u><a href="{{$checkr_url}}" target="_blank">Click here</a></u> to submit your background check.  

                @else
                    @push('header')
                      <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
                      <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
                    @endpush
                    <p style="color:black">We use <u><a href="https://checkr.com/" target="_blank">Checkr</a></u> to perform background checks. Please allow 1-3 days to process and review your background check.  Tutors are responsible for the background check fee (${{$background_check_fee}}).</p>
                    <p></p>
                    <p></p>
                    @if ($application->offered === 1)
                        {!! Form::open(['method' => 'POST', 'route' => 'background.process.payment', 'id' => 'payment-form']) !!}
                            <div class="row">
                                <div class="col-sm-8 col-md-8">
                                  @include('btn.stripe.card_form_partial')
                                </div>  
                            </div>

                            <div class="add-comment custom-form">
                                <button class="btn big-btn color-bg flat-btn" style="background: #566985">Submit Background Check Fee (${{$background_check_fee}})</button>
                            </div>
                        {!! Form::close() !!}
                    @endif
                @endif
            </div>
            <a class="toggle @if ($application->hired)
                act-accordion
            @endif" href="#"> <b>Step 4:</b> Sign Contract <i class="fa fa-angle-down"></i></a>
            <div class="accordion-inner @if ($application->hired)
                visible
            @endif">
                <p style="color:black">Please carefully review and <u><a href="{{route('get.tutor.contract')}}">sign the contract</a></u>. Then complete the <a href="{{route('tutor.checklist')}}">Tutor Checklist</a> to setup your account and profile.</p>
                @if ($application->hired !== 1)
                    <p>You will be able to sign the contract after you have passed the background check.</p>
                @endif
            </div>
        @endif
    </div>
</div>