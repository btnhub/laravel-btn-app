<?php 
    $course_levels = App\CourseLevel::all()->pluck('description', 'id')->toArray();
    $course_levels = ['' => "Any Level"] + $course_levels;

    $tutor_availability_options = [
        null => 'Session Time',
        'Daytime' => 'Daytime (before 5pm)',
        'Evenings' => 'Evenings (5-7pm)',
        'Nights' => 'Nights (after 7pm)',
        'Saturdays' => 'Saturdays',
        'Sundays' => 'Sundays'
        ];
?>
{{--Requires $course_levels, $tutor_availablility_options arrays--}}
{!! Form::open(['method' => 'POST', 'route' => 'search.map']) !!}
    <div class="listsearch-input-wrap fl-wrap">
        <div class="listsearch-input-item">
            <i class="mbri-key single-i"></i>
            <input type="text" placeholder="Course e.g. Math or CHE_110" name="course" @if ($request ?? '') value="{{$request->course}}" @endif required/>
        </div>

        <div class="listsearch-input-item">
                {!! Form::select('course_level_id', $course_levels, ($request ?? '' ? $request->course_level_id : ''), ['id' => 'course_level_id', 'class' => 'chosen-select' ]) !!}
        </div>
        <div class="listsearch-input-item">
            <select id="tutor_availability[]" class="chosen-select" multiple="multiple" name="tutor_availability[]">

                @foreach ($tutor_availability_options as $key => $tutor_availability)
                    @if ($key == '')
                        <option value="{{$key}}" @if (!isset($request) || !isset($request->tutor_availabililty)) 
                            selected="selected"
                        @endif>{{$tutor_availability}}</option>    
                    @else
                        <option value="{{$key}}" @if (($request ?? '') && $request->tutor_availability && in_array($key, $request->tutor_availability))
                            selected="selected"
                        @endif>{{$tutor_availability}}</option>    
                    @endif
                    
                @endforeach                
            </select>
        </div>
        <div class="listsearch-input-text" id="autocomplete-container">
            <label><i class="mbri-map-pin"></i> Location </label>
            <input type="text" name="location" placeholder="Where you'd like to meet" id="autocomplete-input" class="qodef-archive-places-search" @if ($request ?? '') value="{{$request->location}}" @endif/>
        </div> 
        
        @if (Auth::user() && Auth::user()->isSuperUser())
            <div class="listsearch-input-item">
                {!! Form::select('status',['' => 'Any Availability', '1' => 'Available', '0' => 'Unavailable'], ($request ?? '' ? $request->status : ''), ['id' => 'status', 'class' => 'chosen-select']) !!}
            </div>
        @endif
        
        <div class=" fl-wrap filter-tags" style="margin-bottom: 30px; margin-top:30px">
            <input id="online_tutors" type="checkbox" name="include_online" value="1" @if (($request ?? '') && $request->include_online)
                checked
            @endif>
            <label for="online_tutors">Include Online Tutors</label>
            <br>
        </div>
        @if (Auth::user() && Auth::user()->isSuperUser())
            <div class="row" style="margin-bottom: 30px">
                <div class="listsearch-input-item">
                    <input type="text" placeholder="Min Rate" name="min_rate" @if ($request ?? '') value="{{$request->min_rate}}" @endif/>
                </div>    
                <div class="listsearch-input-item">
                    <input type="text" placeholder="Max Rate" name="max_rate" @if ($request ?? '') value="{{$request->max_rate}}" @endif/>
                </div>
            </div>
        @endif
        
            <input type="hidden" name="formatted_address" id="formatted_address" @if ($request ?? '') value="{{$request->formatted_address}}" @endif>
            <input type="hidden" name="lat" id="lat" @if ($request ?? '') value="{{$request->lat}}" @endif>
            <input type="hidden" name="lng" id="lng" @if ($request ?? '') value="{{$request->lng}}" @endif>
            <input type="hidden" name="city" id="city" @if ($request ?? '') value="{{$request->city}}" @endif>
            <input type="hidden" name="region" id="region" @if ($request ?? '') value="{{$request->region}}" @endif>
            <input type="hidden" name="country" id="country" @if ($request ?? '') value="{{$request->country}}" @endif>
            <input type="hidden" name="postal_code" id="postal_code" @if ($request ?? '') value="{{$request->postal_code}}" @endif>

        {{-- <!-- hidden-listing-filter -->
        <div class="hidden-listing-filter fl-wrap">
            <div class="distance-input fl-wrap">
                <div class="distance-title"> Radius around destination: <span></span> miles</div>
                <div class="distance-radius-wrap fl-wrap">
                    <input class="distance-radius rangeslider--horizontal" type="range" min="0" max="50" step="5" value="10" data-title="Radius around selected destination">
                </div>
            </div>
            <!-- Checkboxes -->
            <div class=" fl-wrap filter-tags">
                <!-- <h4>Filter by Tags</h4> -->
                <input id="online-tutors" type="checkbox" name="check">
                <label for="online-tutors">Include Online Tutors</label>
            </div>
        </div>
        <!-- hidden-listing-filter end --> --}}
        <button class="button fs-map-btn">Search</button>
        {{-- <div class="more-filter-option">More Filters <span></span></div> --}}
    </div>                    
{!! Form::close() !!} 