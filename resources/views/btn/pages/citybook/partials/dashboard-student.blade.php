@push('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
  <script type="text/javascript" src="{{asset('assets/js/btn/show_evaluation.js')}}"></script>
@endpush

<div class="list-single-main-item fl-wrap">
    <div class="list-single-main-item-title fl-wrap">
        <h3>Welcome, <span>{{$student->first_name}}</span></h3>
    </div>
    
    @if ($pending_proposals)
	    <p style="color:darkred;text-align: center;"><b style="font-size: 1.25em">You have <a href="{{route('my.tutor.requests', $student->ref_id)}}" style="color:darkblue;text-decoration: underline">{{$pending_proposals}} new tutor {{str_plural('proposal', $pending_proposals)}}</a>!</b></p>
	@endif

    <p style="color:black">On this page, you will find your tutor's contact information, you can also track the status of your tutor requests, review and respond to tutor proposals and view session charges, add or update your debit/credit card.</p>
    
    <p>Please review our <a href="{{ route('cancellation.policy') }}">cancellation</a> & <a href="{{ route('refund.policy') }}">refund</a> policies. If you need to cancel a session, be sure to give your tutor 24hr notice.</p>
    
    <p style="color:black;text-align:center">
    	Need Help? <a href="{{route('request.get')}}" style="text-decoration: underline;color:darkblue">Request A Tutor</a> Today!
    </p>
    
    @if ($student->tutorSessions->count() > 3)
        <p></p>
        <p style="color:black">
	        <i class="fa fa-star" aria-hidden="true" style="color:orange"></i>
	        <i class="fa fa-star" aria-hidden="true" style="color:orange"></i>
	        <i class="fa fa-star" aria-hidden="true" style="color:orange"></i>
	        <i class="fa fa-star" aria-hidden="true" style="color:orange"></i>
	        <i class="fa fa-star-half-o" aria-hidden="true" style="color:orange"></i>
	        @unless ($student->locations->where('company_name', 'RamTutor')->count())
	          {{--BuffTutor URL--}}
	          <span> 
	            <u><a href="https://g.page/bufftutor/review?rc" target="_blank">Review BuffTutor</a></u> On Google! It only takes a second, but helps us immensely.
	          </span>
	        @else
	          {{--RamTutor URL--}}
	          <span>
	            <u><a href="https://g.page/ramtutor/review?rc" target="_blank"> Review RamTutor</a></u> On Google! It only takes a second, but helps us immensely.
	          </span>
	        @endunless
	    </p>
	    <p></p>
      @endif
    <div class="accordion">
        <a class="toggle @if (session('assign_tab'))
			act-accordion
		@endif" href="#"> My Tutors ({{$assignments_students->count()}}) <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner @if (session('assign_tab'))
			visible
		@endif" style="padding:15px 0px">
        	@if ($assignments_students->count() && !$student->customer)
        		<p style="color:black">To access your tutor's contact information, please add a debit/credit card to your account.  You will not be charged now but <u>after</u> each session occurs.  This is how you will pay for sessions.  <u>Do not pay tutors directly for sessions, e.g. by cash, check, PayPal, Venmo, etc.</u></p>
        		<div class="row">
        			{!! Form::open(['method' => 'POST', 'route' => 'store.customer.card', 'id' => 'payment-form']) !!}

			            <div class="col-sm-12">
			              @include('btn.stripe.card_form_partial')
			            </div>
			            <div class="add-comment custom-form">
	                        <button class="btn big-btn color-bg flat-btn" style="background: #566985">Add Card</button>
	                    </div>
			            
		          	{!! Form::close() !!}

        		</div>
        		<p></p>	
        		<p></p>
        	@endif
            <div class="dashboard-list-box fl-wrap">
		        <div class="dashboard-header fl-wrap">
		            <h3>My Tutors</h3>
		        </div>
		        @forelse ($assignments_students as $assignment)
					@if ($assignment->tutor)   
				        <div class="dashboard-list">
				            <div class="dashboard-message">
				                <div class="dashboard-message-avatar">
				                    <img src="/btn/avatars/{{$assignment->tutor->profile->avatar ?? 'default.jpg'}}" alt="">
				                </div>
				                <div class="dashboard-message-text">
				                    <div class="col-sm-12">
				                    	<h4>{{$student->customer && $assignment->end_date->isFuture() ? $assignment->tutor->full_name : $assignment->tutor->user_name}} - {{$assignment->course}} {!! $assignment->online ? "<span>(Online)</span>" : null !!} (${{$assignment->tutorContract->student_rate}}/hr) 

				                    		@if($assignment->prepaid_balance() > 0)
				                    			<br><span style="color:blue">Prepaid Balance: ${{$assignment->prepaid_balance()}}</span> 
				                    		@endif
				                    	</h4>
				                        <div class="booking-details fl-wrap">
				                            <span class="booking-title">Dates</span> :
				                            {{$assignment->start_date->format('m/d/Y')}} - {{$assignment->end_date->format('m/d/Y')}}
				                            @if ($assignment->end_date->diffInDays(Carbon\Carbon::now()) < 14)
												<em><span style="color:red"> Ending Soon! </span></em>
											@endif
				                        </div>
				                        @if($assignment->end_date->isFuture())

										<div class="booking-details fl-wrap">
				                            <span class="booking-title"><i class="fa fa-envelope-o"></i></span> : 
				                            <span class="booking-text"><a href="{{$student->customer ? 'mailto:' . $assignment->tutor->email : "#"}}" target="_top">{{$student->customer? $assignment->tutor->email : '...add credit/debit card above...'}}</a></span>
				                            <span class="booking-title" style="padding-left: 10px"><i class="fa fa-phone"></i></span> : 
				                            <span class="booking-text"><a href="{{$student->customer ? 'tel:'.$assignment->tutor->phone : "#"}}" target="_top">{{$student->customer ? $assignment->tutor->phone : '...'}}</a></span>
				                        </div>
				                    	@endif
				                    </div>

				                </div>
				            </div>
				        </div>
		        	@endif
		        @empty
		        	<div class="row" style="margin: 30px">
		        		<p>Need A Little Help? <u><a href="{{route('request.get')}}">Request A Tutor Today!</a></u></p>	
		        	</div>
		        @endforelse
		    </div>
        </div>

        <a class="toggle @if (session('requests_tab'))
			act-accordion
		@endif" href="#"> Requests ({{$tutor_requests->count()}})<i class="fa fa-angle-down"></i></a>
		<div class="accordion-inner @if (session('requests_tab'))
			visible
			@endif">
			<div style="border:solid gray 2px; border-radius:3px; padding: 20px;overflow-x: auto">
	        	<p style="color:black">Keep track of the progress of your tutor request below. </p>

				<table class="table">
					<thead>
						<tr>
							<th>Request Details</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($tutor_requests as $tutor_request)
							<tr>
								<td>
									<b>Ref ID:</b>{{$tutor_request->ref_id}}<br>
									<b>Course:</b> {{$tutor_request->course}}<br>
									<b>Time:</b> {{$tutor_request->session_time}}<br>
									<b>Frequency:</b> {{$tutor_request->frequency}}<br>
									<b>Max Rate:</b> ${{$tutor_request->max_rate}}/hr<br>
									@if ($tutor_request->requested_tutors)
										<b>Requested Tutors:</b> {{$tutor_request->requested_tutors}}<br>
									@endif
									@if ($tutor_request->include_online_tutors)
										<span style="color:purple">Include Online Tutors</span>
									@endif
								</td>
								<td>
									<?php 
									$tutor_contracts = $tutor_request->tutor_contracts->where('tutor_decision', 1);
									?>
									@if ($tutor_request->unassigned)
										<p style="color:darkred">Unfortuntately we do not currently have a tutor who can assist you.</p>
									@elseif (!$tutor_contracts->count())
										<span style="color:darkgreen;font-weight: bold">Under Review.</span>  
										<p style="color:black">Please allow 1-2 days for us to find a suitable tutor for you.</p>
									@else
										<?php 
											$unreviewed_proposals = $tutor_request->tutor_contracts->where('tutor_decision', 1)->where('student_decision', NULL)->count();;
											?>
										@if ($unreviewed_proposals)
											<a href="{{route('my.tutor.requests', $student->ref_id)}}" style="text-decoration: underline;color:blue">{{$unreviewed_proposals}} Unreviewed {{str_plural('Proposal', $unreviewed_proposals)}}</a>
										@endif
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
	        </div>
        </div>
        <a class="toggle" href="#"> Session Charges ({{$student->activeTutorSessions->count()}}) <i class="fa fa-angle-down"></i></a>

        <div class="accordion-inner">
        	<div style="overflow-x: auto">
        		@include('btn.tables.sessions_student')		
        	</div>
        </div>

        <a class="toggle @if (session('eval_tab'))
			act-accordion
			@endif" href="#"> Evaluations <i class="fa fa-angle-down"></i></a>
		<div class="accordion-inner @if (session('eval_tab'))
			visible
			@endif">
			<p style="color:black">Leave a review for your tutor(s).</p>
			{{-- Show End of Semester Evaluation 1 week before the end date,  Show 1st semester eval after 1st session --}}
			@foreach ($assignments_students->merge($student->assignments()->previous()->get()) as $assignment)
				@unless($student->tutorSessions()->where('assignment_id', $assignment->id)->count() == 0)

					@if (strtotime(date('m/d/Y')) > (strtotime($assignment->end_date) - 10*24*3600) && $student->tutorSessions()->where('assignment_id', $assignment->id)->count() >= 3 && !count($assignment->evaluation->where('type', 'End')) )
						<p style="color:black">{{$assignment->tutor->full_name}} - {{$assignment->course}} <a style="color:blue;text-decoration: underline" onclick="showEvaluation('{{$assignment->ref_id}}', '{{$assignment->tutor->first_name}}', 'end')">Evaluate {{$assignment->tutor->first_name}}</a> (End of Semester Evaluation)</p> 
					@elseif ($student->tutorSessions()->where('assignment_id', $assignment->id)->count() >= 1 && !count($student->evaluations->where('assignment_id', $assignment->id)->where('type', 'First')) && !count($assignment->evaluation->where('type', 'End')))
						<p style="color:black">{{$assignment->tutor->full_name}} - {{$assignment->course}} <a style="color:blue;text-decoration: underline" onclick="showEvaluation('{{$assignment->ref_id}}', '{{$assignment->tutor->first_name}}', 'first')">Evaluate {{$assignment->tutor->first_name}}</a> (First Session Evaluation)</p> 
					@else 
						<p>{{$assignment->tutor->full_name}} - {{$assignment->course}}: Evaluation Submitted</p>
					@endif
				@endunless
			@endforeach

			<div id="eval-div" style="display:none;margin-top:20px">
				<div class="list-single-main-item-title fl-wrap">
	                <h3>Evaluate <span name="eval-tutor-name"></span> <div id="eval-type"></div></h3>
	            </div>
	            <div id="add-review" class="add-review-box">
	                <div class="leave-rating-wrap">
	                    <span class="leave-rating-title">Your rating for <span name="eval-tutor-name"></span> : </span>
	                    <div class="leave-rating" id="star-ratings">
	                        <input type="radio" name="rating" id="rating-1" value="1" onclick="starCount(5)"/>
	                        <label name="rating-label" for="rating-1" class="fa fa-star-o"></label>
	                        <input type="radio" name="rating" id="rating-2" value="2" onclick="starCount(4)"/>
	                        <label name="rating-label" for="rating-2" class="fa fa-star-o"></label>
	                        <input type="radio" name="rating" id="rating-3" value="3" onclick="starCount(3)"/>
	                        <label name="rating-label" for="rating-3" class="fa fa-star-o"></label>
	                        <input type="radio" name="rating" id="rating-4" value="4" onclick="starCount(2)"/>
	                        <label name="rating-label" for="rating-4" class="fa fa-star-o"></label>
	                        <input type="radio" name="rating" id="rating-5" value="5" onclick="starCount(1)"/>
	                        <label name="rating-label" for="rating-5" class="fa fa-star-o"></label>
	                    </div>
	                </div>
	            </div>
				<div id="eval-form"></div>	
			</div>
        </div>

        <a class="toggle" href="#"> Payment Settings <i class="fa fa-angle-down"></i></a>
		<div class="accordion-inner">
        	@if ($student->customer)
	    		<div class="row">
	    			<div class="col-sm-4">
	    				<p style="color:black">Card on Account</p>
	    				<p>{{$student->customer->card_brand}} xxxx{{str_pad($student->customer->card_last_four, 4, 0, STR_PAD_LEFT)}}</p>
	    				<p>Exp: {{str_pad($student->customer->exp_month, 2, 0, STR_PAD_LEFT)}} / {{$student->customer->exp_year}}</p>
	    			</div>
	    			<div class="col-sm-8">
	    				<p style="color:black">Add Different Card</p>
	    				{!! Form::open(['method' => 'POST', 'route' => 'update.customer.card', 'id' => 'payment-form']) !!}

				            <div class="col-sm-12">
				              @include('btn.stripe.card_form_partial')
				            </div>
				            <div class="add-comment custom-form">
		                        <button class="btn big-btn color-bg flat-btn" style="background: #566985">Update Card</button>
		                    </div>
			          	{!! Form::close() !!}
	    			</div>
	    		</div>
	    		<p></p>	
	    		<p></p>
	    	@endif
        </div>
    </div>
</div>

@push('scripts')
	<script>
		function showHideReason(student_decision){
			if (student_decision == 1) {
				document.getElementById('reject_reason').style.display = 'none';

				document.getElementById('submit_button').value = "Accept Assignment";
			}
			else
			{
				document.getElementById('reject_reason').style.display = 'block';
				document.getElementById('submit_button').value = "Reject Assignment";
			}
		}

	</script>
@endpush