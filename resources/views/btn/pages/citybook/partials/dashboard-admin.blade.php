@push('css')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo-school.js')}}"></script>
@endsection

<div class="list-single-main-item fl-wrap">
    <div class="list-single-main-item-title fl-wrap">
        <h3>Admin Tools</h3>
    </div>
    
    <div class="accordion">
        <a class="toggle" href="#">Add Location To User({{$incomplete_users->count()}})<i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner @if ($incomplete_users->count())
            visible
        @endif">
            <h3><b>Users w/o locations</b></h3>
            <p style="color:black"></p>
            <table class="table">
                <tbody>
                    @foreach ($incomplete_users as $incomplete_user)
                        <tr>
                            <td>
                                {{$incomplete_user->full_name}} ({{$incomplete_user->id}}) - {{$incomplete_user->formatted_address}}
                                <br>
                                {{$incomplete_user->locations()->pluck('city')->implode(', ')}}, {{$incomplete_user->roles()->pluck('role')->implode(', ')}}
                            </td>
                            <td>
                                {!! Form::open(['method' => 'POST', 'route' => ['update.user.location', $incomplete_user->id], 'id' => "form-$incomplete_user->id"]) !!}

                                    <input type="text" name="location" placeholder="Where you'd like to meet" id="autocomplete-input" class="qodef-archive-places-search form-control" required/>
                                    
                                    <input type="hidden" name="formatted_address" id="formatted_address">
                                    <input type="hidden" name="lat" id="lat">
                                    <input type="hidden" name="lng" id="lng">
                                    <input type="hidden" name="city" id="city">
                                    <input type="hidden" name="region" id="region">
                                    <input type="hidden" name="country" id="country">
                                    <input type="hidden" name="postal_code" id="postal_code">

                                    <div class="btn-group pull-right">
                                        {!! Form::submit("Add $incomplete_user->username's Location", ['class' => 'btn btn-primary']) !!}
                                    </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        <a class="toggle" href="#">Add Location To Request({{$incomplete_requests->count()}})  <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner @if ($incomplete_requests->count())
            visible
        @endif">
            <h3><b>Requests w/o locations</b></h3>
            <p style="color:black"></p>
            <table class="table">
                <tbody>
                    @foreach ($incomplete_requests as $tutor_request)
                        <tr>
                            <td>
                                {{$tutor_request->id}} - {{$tutor_request->student->full_name}} - {{$tutor_request->course}}
                                <br>
                                <b>Request Location:</b> {{$tutor_request->formatted_address}}
                                <br>
                                <b>School:</b> {{$tutor_request->school}}
                                <br>
                                <b>User Location(s):</b> {{$tutor_request->student->locations()->pluck('city')->implode(', ')}} 
                            </td>
                            <td>
                                {!! Form::open(['method' => 'POST', 'route' => ['update.request.location', $tutor_request->id], 'id' => "form-$tutor_request->id"]) !!}

                                    <input type="text" name="location" placeholder="Where you'd like to meet" id="autocomplete-input" class="qodef-archive-places-search form-control" required/>
                                    
                                    <input type="hidden" name="formatted_address" id="formatted_address">
                                    <input type="hidden" name="lat" id="lat">
                                    <input type="hidden" name="lng" id="lng">
                                    <input type="hidden" name="city" id="city">
                                    <input type="hidden" name="region" id="region">
                                    <input type="hidden" name="country" id="country">
                                    <input type="hidden" name="postal_code" id="postal_code">

                                    <div class="btn-group pull-right">
                                        {!! Form::submit("Add Location", ['class' => 'btn btn-primary']) !!}
                                    </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="list-single-main-item fl-wrap">
    <div class="list-single-main-item-title fl-wrap">
        <h3>Create New BTN Location</h3>
    </div>
    <div>
        <p><b>Step 1:</b> Add a location using form below</p>
        <p>
            <b>Step 2:</b> Create a new sitemap index:
            {!! Form::open(['method' => 'POST', 'route' => 'create.sitemaps', 'class' => 'form-horizontal']) !!}
                <div class="pull-left">
                    {!! Form::submit("Create Index", ['class' => 'btn btn-sm btn-success']) !!}
                </div>
            {!! Form::close() !!}
            <br><br>

        </p>
        <p><b>Step 3:</b>  "Ping" Google Jobs to add new tutor job for new location by clicking here: <br>
            Sitemap Index: <a href="http://www.google.com/ping?sitemap=https://bufftutor.com/sitemap_index.xml" target="_blank">http://www.google.com/ping?sitemap=https://bufftutor.com/sitemap_index.xml</a> <br>
            <small>Original Sitemap: <a href="http://www.google.com/ping?sitemap=https://bufftutor.com/sitemap.xml" target="_blank">http://www.google.com/ping?sitemap=https://bufftutor.com/sitemap.xml</a></small> 
            <br>
            REF: <a href="https://developers.google.com/search/docs/data-types/job-posting" target="_blank">Google Job Posting</a>
        </p>

        {!! Form::open(['method' => 'POST', 'route' => ['create.new.location'], 'id' => "new_btn_location"]) !!}

            <div class="col-sm-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                City: <input type="text" name="location" placeholder="New Btn City" id="autocomplete-input" class="qodef-archive-places-search form-control" required/>
                <small>To select, click city from dropdown, DO NOT PRESS ENTER</small>
                <br><br>
                Formatted Address: <input type="text" name="formatted_address" id="formatted_address" class="qodef-archive-places-search form-control" readonly>
                <small>Formatted Address must be: City, State Abbrev, Country.  E.g. Boston, MA, USA</small>
                
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">
                <input type="hidden" name="city" id="city">
            </div>
            <div class="col-sm-6">
               <div class="btn-group">
                    {!! Form::submit("Add New Location", ['class' => 'btn btn-primary']) !!}
                </div> 
            </div>
            
        {!! Form::close() !!}
    </div>
</div>

<div class="list-single-main-item fl-wrap">
    <div class="list-single-main-item-title fl-wrap">
        <h3>Add New School</h3>
    </div>
    <div>
        <p><b>Step 1:</b> Add a school using form below</p>
        <p>
            <b>Step 2:</b> Create a new sitemap index:
            {!! Form::open(['method' => 'POST', 'route' => 'create.sitemaps', 'class' => 'form-horizontal']) !!}
                <div class="pull-left">
                    {!! Form::submit("Create Index", ['class' => 'btn btn-sm btn-success']) !!}
                </div>
            {!! Form::close() !!}
            <br><br>

        </p>
        <p><b>Step 3:</b>  "Ping" Google Jobs to add new tutor job for new location by clicking here: <br>
            Sitemap Index: <a href="http://www.google.com/ping?sitemap=https://bufftutor.com/sitemap_index.xml" target="_blank">http://www.google.com/ping?sitemap=https://bufftutor.com/sitemap_index.xml</a> <br>
        </p>

        {!! Form::open(['method' => 'POST', 'route' => ['add.new.university'], 'id' => "new_school"]) !!}

            <div class="col-sm-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                School: <input type="text" name="school-input" placeholder="New School" id="autocomplete-school" class="qodef-archive-places-search form-control" required/>
                <small>To select, click school from dropdown, DO NOT PRESS ENTER</small>
                Alt Names: <input type="text" name="alt_names" placeholder="Alternative Names" id="alt_names" class="qodef-archive-places-search form-control"/>
                <small>DO NOT USE "-".  Separate multiple names with ";", for example: UCSD;UC San Diego</small>
                <br><br>
                School Name: <input type="text" name="name" id="school-name" class="qodef-archive-places-search form-control" readonly>
                City: <input type="text" name="city" id="school-city" class="qodef-archive-places-search form-control" readonly>

                <input type="hidden" name="lat" id="school-lat">
                <input type="hidden" name="lng" id="school-lng">
                <input type="hidden" name="state" id="school-state">
                <input type="hidden" name="state_abbr" id="school-state_abbr">
                <input type="hidden" name="google_place_id" id="school-google_place_id">
                <input type="hidden" name="website" id="school-website">
                <input type="hidden" name="country" id="school-country">
                <input type="hidden" name="postal_code" id="school-postal_code">
            </div>
            <div class="col-sm-6">
               <div class="btn-group">
                    {!! Form::submit("Add New Uni", ['class' => 'btn btn-primary']) !!}
                </div> 
            </div>
            
        {!! Form::close() !!}
    </div>
</div>
