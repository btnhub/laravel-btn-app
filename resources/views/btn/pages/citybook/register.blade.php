@extends('layouts.citybook.default')

@section('title')
Register
@endsection

@section('meta-description')
	Create a BuffTutor account.
@endsection

@section('content')
	<section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
 			<div class="row">
                <div class="col-sm-8 col-md-offset-2">
                    <!-- profile-edit-container--> 
                    <div id="qualification" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>Create An Account</h4>
                            <p>Already have an account? <a href="{{url('/login')}}">Login Here</a> </p>
                        </div>
						<div class="custom-form">
					        {!! Form::open(['method' => 'POST', 'url' => 'register']) !!}
					        	<div class="row">
					        		<div class="col-md-6">
					        			<label >First Name * <i class="fa fa-user"></i><small class="text-danger" style="color:red">{{ $errors->first('first_name') }}</small></label>
                                		<div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                                		    {!! Form::text('first_name', old('first_name'), ['placeholder' => "First Name", 'autofocus' => 'autofocus']) !!}
                                		</div>
					        		</div>
					        		<div class="col-md-6">
					        			
		                                <label>Last Name *<i class="fa fa-user"></i> <small class="text-danger" style="color:red">{{ $errors->first('last_name') }}</small></label>
		                                <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
		                                    {!! Form::text('last_name', old('last_name'), ['placeholder' => "Last Name"]) !!}
		                                </div>
					        		</div>
					        	</div>
					        	<div class="row">
					        		<div class="col-md-6">
					        			<label>Email Address * <i class="fa fa-envelope"></i> <small class="text-danger" style="color:red">{{ $errors->first('email') }}</small></label>
					        			<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					        			    {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email Address']) !!}
					        			</div>
					        		</div>
					        		<div class="col-md-6">
					        			<label>Phone * <i class="fa fa-phone"></i> <small class="text-danger" style="color:red">{{ $errors->first('phone') }}</small></label>
					        			<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
					        			    {!! Form::text('phone', old('phone'), ['placeholder' => "Phone Number"]) !!}
					        			</div>
					        		</div>
					        	</div>
					        	<div class="row">
					        		<div class="col-md-6">
					        			<label >Password * <i class="fa fa-eye"></i> <small class="text-danger" style="color:red">{{ $errors->first('password') }}</small></label>
                                		<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                		    {!! Form::password('password', ["placeholder" => "Password"]) !!}
                                		</div>	
					        		</div>
					        		<div class="col-md-6">
					        			<label >Confirm Password * <i class="fa fa-eye"></i> <small class="text-danger" style="color:red">{{ $errors->first('password_confirmation') }}</small></label>
					        			<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
					        			    {!! Form::password('password_confirmation', ["placeholder" => "Re-enter Password"]) !!}
					        			</div>
					        		</div>
					        	</div>
					        	<div class="row">
					        		<div class="col-md-6">
					        			<label >Register As A Student Or Tutor *<small class="text-danger" style="color:red">{{ $errors->first('role') }}</small></label>
					        			<div class="filter-tags">
	                                    	<input id="student" type="radio" name="role" value="student" @if (old('role') == 'student')
	                                    		checked
	                                    	@endif>
	                                    	<label for="student">Student</label>	
		                                   
		                                    <input id="tutor" type="radio" name="role" value="tutor-applicant" @if (old('role') == 'tutor-applicant')
		                                    	checked
		                                    @endif>
		                                    <label for="tutor">Tutor</label>
		                                </div>
					        		</div>
					        		<div class="col-md-6">
					        			<label >Marketing * <i class="fa fa-comments"></i> <small class="text-danger" style="color:red">{{ $errors->first('marketing') }}</small></label>
					        			<div class="form-group{{ $errors->has('marketing') ? ' has-error' : '' }}">
					        			    {!! Form::text('marketing', null, ['placeholder' => "How did you hear about us?"]) !!}
					        			    
					        			</div>
					        		</div>
					        	</div>
					        	<div class="row">
					        		<div class="col-md-12">
					        			<div class=" fl-wrap filter-tags">
	                                        <label for="terms_of_use">
		                                        {!! Form::checkbox('terms_of_use', '1', null, ['id' => 'terms_of_use']) !!}  &nbsp;  &nbsp;I agree to the <a href="{{route('terms')}}">Terms of Use</a>. 
		                                    </label>
		                                    <small class="text-danger" style="color:red">{{ $errors->first('terms_of_use') }}</small>
	                                    </div>  
					        		</div>
					        	</div>
                                <button type="submit"     class="log-submit-btn"  ><span>Register</span></button>
					        {!! Form::close() !!}
					    </div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection