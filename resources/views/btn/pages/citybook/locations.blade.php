@extends('layouts.citybook.default')

@section('title')
Locations - BuffTutor
@endsection

@section('meta-description')
Connect with an experienced & passionate tutor in one of our many locations and online.
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;font-family: serif;text-align: left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3 style="font-size:1.75em">Locations</h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        <p>NOTE: Our list of tutors and the locations where they can meet vary each semester.  Please <a style="color: #337ab7" href="{{route('request.get')}}">request a tutor</a> or contact us to inquire into the availability of tutors in a specific location.</p>
                        <br>
                        <div class="row">
                            @foreach ($states->chunk($chunk_size) as $state_chunk)
                                <div class="col-sm-3">
                                    @foreach ($state_chunk as $state_abbr => $state)
                                        <div class="listsearch-header fl-wrap" style="padding-top:1em">
                                            <h3>{{$state}}</h3>
                                        </div>
                                        <ul>
                                            @foreach ($locations->where('state', $state)->sortBy('city') as $location)
                                                <li style="padding: 5px 10px"><span><a style="color: #337ab7" href="{{route('home.city', strtolower(str_replace(" ", "_",$location->city)."-$state_abbr-tutors"))}}">{{$location->city}} Tutors</a></span></li>
                                            @endforeach     
                                        </ul>
                                    @endforeach    
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection