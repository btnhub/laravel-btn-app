@extends('layouts.citybook.default')

@section('title')
BuffTutor Proficiency Exam
@endsection

@section('content')
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML"></script>

	<section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>Instructions</h4>
                            <p style="color:black">This is a proficiency test so please DO NOT GUESS. You  may submit each exam ONCE, all other attempts will be disregarded.</p>
							<p style="color:black">You may use other resources, e.g. textbooks, during the exam.  You may use the internet to quickly look up equations or definitions, but do not search the entire question for an answer.</p>
							<p style="color:black">For each problem, recognize the underlying concepts covered in the question, consider what students will likely struggle with and what you would do to assist students grasp the concepts or tricks of the problem.</p>

							<p></p>
							<p></p>
							<h4><span>For Math Exams</span></h4>
							<p style="color:black">Do <b>NOT</b> use a calculator, unless absolutely necessary. </p>
							<p style="color:black">Math equations and formulas may take a few moments to load.  To enlarge them: Right-Click the equation --> Math Settings --> Scale All Math, set to desired size. </p>

							<p></p>
							<p></p>

							<p style="color:black">If there is an error in a question statement, equations or grading, please notify us via e-mail.  Include the question's reference number and a brief description of the problem.</p>
                        </div>
					</div>
					<div id="course-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
                        <div class="profile-edit-header fl-wrap">
                            <h4>{{count($questions)}} Questions</h4>
                        </div>

	                	{!! Form::open(['method' => 'POST', 'route' => 'grade.exam']) !!}
					    <?php $question_no = 1;?>
					    <ol type="1">
					        @foreach ($questions as $QuestionNo => $Value)
					            <p style="color:black">
					                <span style="color:blue;">{{$question_no}})</span> <b><?php 
					                        $reference= $Value['School'].$Value['Semester'].substr($Value['Year'], -2).$Value['Exam'].$Value['Exam_Question_No'];
					                        $Question_Stmt = $Value['Question'];

					                        if ($Value['Question_Image'] !== "") { // if an image is present
					                            $img_location = "$img_dir/".$Value['Question_Image'];
					                            $img_tag = "<br><center><img src='$img_location' alt='Please inform us that you were unable to view this image.  Include the reference number for this question.'></center>";
					                            $Question_Stmt .= $img_tag;
					                        }
					                        $Question_Stmt .= "<br><p>(Ref #: ".$reference.")</p>";
					                        echo $Question_Stmt;
					                    ?></b>

					                    <input type="hidden" name="questions[<?php echo $QuestionNo; ?>]" value="<?php echo $Question_Stmt;?>"/>
					                    {!! Form::hidden("cans[$QuestionNo]", Crypt::encrypt($Value['CorrectAnswer'])) !!}
					                    {!! Form::hidden('reference[]', $reference) !!}

					                <?php 
					                foreach ($Value['Answers'] as $Letter => $Answer){ 
					                    $Label = 'question-'.$QuestionNo.'-answers-'.$Letter; ?>
					                    <p style="color:black">
					                    <input type="radio" name="answers[<?php echo $QuestionNo; ?>]" id="<?php echo $Label; ?>" value="<?php echo $Answer; ?>" />
					                    <label for="<?php echo $Label; ?>"><?php echo $Letter; ?>) <?php echo $Answer; ?> </label>                          
					                    </p>
					                <?php } /*foreach*/ ?>
					               <br>
					            </p>
					            <?php $question_no++;?>
					        @endforeach

					    </ol>
					    {!! Form::hidden('exam', $exam) !!}

					    <div class="add-comment custom-form">
                            <button class="btn  big-btn  color-bg flat-btn">Submit Exam <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>    
                        </div>
					{!! Form::close() !!}
	                </div>
				</div>
			</div>
		</div>
	</section>
@endsection