@extends('layouts.citybook.default')

@section('title')
Refund Policy
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Refund Policy</h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap" style="font-family:serif;">
                        {{-- <h3>Session Refund</h3> --}}
                        
                        <p style="color:black;text-align:left"><b><u>Prepaid First Hour:</u></b> If you prepaid for the first hour and you did not meet with your tutor (for example, if the tutor was unresponsive or the tutor's schedule does not work with yours), you have <b>2 weeks</b> from the payment date to contact us for a full refund.  <b>Partial refunds are not granted.</b>  If you met with a tutor for less than an hour and there is a balance on your first prepaid hour, that balance will be applied to your next session with the tutor. A prepaid first hour cannot be transferred to another student or applied to a session with a different tutor. A prepaid first hour payment expires at the end of the semester/quarter in which it was purchased and cannot be carried over to another semester/quarter.</b></p>

                        <p style="color:black;text-align:left"><b><u>Dissatisfactory First Session:</u></b> If you are unhappy with your tutor's performance in the <b>first</b> session, you may request a refund of the entire session.  If you meet with the tutor again before requesting (or receiving) a refund of the first session, you will <b>not</b> be refunded for the first session. Your decision to meet with your tutor again will be interpreted as acknowledgement of your tutor's satisfactory performance in the first session.  No processing fee is applied to a refund of the first session.</p>

                        <p style="color:black;text-align:left"><b><u>Terminating Sessions</u></b>: If you were initially satisfied with your tutor, but later you choose to stop working with your tutor due to your tutor's unsatisfactory performance, you may request a refund for the last session (subject to a processing fee).  If the last session was longer than 1 hour, we will refund <b><em>the last hour of the last session</em></b> with your tutor (subject to a processing fee).  </p>


                        <p style="color:black;text-align:left">Please note that a request for a refund of an unsatisfactory session must be made within 7 days of the session in dispute.  We may email or call you to discuss what happened during the session and why your tutor's performance was unsatisfactory.  It is your responsibility to end a session (or cancel future sessions) immediately if a tutor's performance is unsatisfactory.  You are more than welcome to work with a different tutor in our network as soon as you would like.</p>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection