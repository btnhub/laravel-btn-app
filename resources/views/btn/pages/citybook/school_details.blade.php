@extends('layouts.citybook.dashboard')

@section('page_title', 'Course Catalogs & Calendars')

@section('dashboard_content')

	<div class="list-single-main-item fl-wrap">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>Course Catalogs / <span>Academic Calendars</span></h3>
	    </div>
		<p>Please carefully review the start and end dates to make sure you will be around to assist students until the end of the academic term.</p>
		<p><b>A Couple Notes</b></p>
		<p style="color:black;padding-left:20px"><i class="fa fa-arrow-right"></i> Universities use the semester OR term system.  Carefully review the calendars for each school near you before you take on a student from that school.</p>
		<p style="color:black;padding-left:20px"><i class="fa fa-arrow-right"></i> High Schools do not have the same academic term start and end date as universities.  Carefully review their calendars before you take on a High School student. </p>
		<p style="color:black;padding-left:20px"><i class="fa fa-arrow-right"></i> At CU-Boulder, Math courses are offered by the MATH and APPM (applied math) departments.</p>
		<div style="overflow-x: auto">
			<table class="table">
				<thead>
					<tr>
						<th>School</th>
						<th>Town</th>
						<th>Acadmic Calendar</th>
						<th>Course Catalog</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($schools as $school)
						<tr>
							<td>{{$school->school_name}}</td>
							<td>{{$school->town}}</td>
							<td>
								@if ($school->calendar)
									<a href="{{$school->calendar}}" style="color:darkblue;text-decoration: underline;" target="_blank">{{$school->school_name}}'s Calendar</a>	
								@endif
							</td>
							<td>
								@if ($school->course_catalog)
									<a href="{{$school->course_catalog}}" style="color:darkblue;text-decoration: underline;" target="_blank">{{$school->school_name}}'s Course Catalog</a>	
								@endif
								
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>	
		</div>
	</div>
@stop