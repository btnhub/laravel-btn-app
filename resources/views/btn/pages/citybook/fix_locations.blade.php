@extends('layouts.citybook.default')

@section('title')
Fix User Locations
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Fix User Locations</h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap" style="font-family:serif;">
                        <p style="color:black;text-align:left">Associate Users with "Other" location with the nearest Buff location.</p>

                        {!! Form::open(['method' => 'POST', 'route' => 'fix.users.location']) !!}
                        
                            <br>
                            <p style="color:black;text-align:left">{{$users->count()}} Users</p>
                            <table>
                                <tr>
                                    <th>User</th>    
                                    <th>User Location</th>
                                    <th>Nearest BTN</th>
                                </tr>
                                
                                @foreach ($users as $user)
                                    <tr>
                                        
                                        <td>{{$user->id}}: <a href="{{route('user.details.get', $user->id)}}" style="color:blue" target="_blank">{{$user->full_name}}</a></td>
                                        <td> {{$user->city}}, {{$user->region}}
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <div class="checkbox{{ $errors->has('user_ids[]') ? ' has-error' : '' }}">
                                                    <label>
                                                        {!! Form::checkbox('user_ids[]',$user->id,1, ['id' => $user->id]) !!} {{$user->nearest_btn_location()->city}}, {{$user->nearest_btn_location()->state}}
                                                    </label>
                                                </div>
                                                <small class="text-danger">{{ $errors->first('user_ids[]') }}</small>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            
                            <div class="btn-group pull-left">
                                <br>
                                {!! Form::submit("Fix Locations", ['class' => 'btn btn-success']) !!}
                            </div>
                        
                        {!! Form::close() !!}
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection