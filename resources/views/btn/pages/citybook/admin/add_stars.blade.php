@extends('layouts.citybook.default')

@section('title')
Add Stars
@endsection

@section('content')
    <section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-input-wrap fl-wrap">                       
                        <div class="section-title center-align">
                            <h2><span>Add Stars</span></h2>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <div class="list-single-main-item fl-wrap" id="reviews">
                                <div class="list-single-main-item-title fl-wrap">
                                    <h3>Evaluations  <span> {{$eval_count}} </span></h3>
                                    <h3>EVALUATE IN ORDER!</h3>
                                    <h3 id="star_result" style="color:purple;font-weight: bold"></h3>
                                </div>
                                <div class="list-single-main-item-title fl-wrap">
                                    <!-- reviews-comments-item -->
                                    @foreach ($evaluations as $evaluation)
                                        <div id="eval-div-{{$evaluation->id}}">
                                            <div class="row">
                                                <div class="list-single-main-item-title fl-wrap">
                                                    <h3>{{$evaluation->tutor->user_name}} - 
                                                    @if ($evaluation->satisfied)
                                                        <span style="color:darkgreen">Satisfied</span>
                                                    @else
                                                        <span style="color:red">Not Satisfied</span>
                                                    @endif 
                                                    <br>
                                                    ID: {{$evaluation->id}}
                                                    </h3>
                                                </div>

                                                <div class="col-md-6">
                                                    <p style="color:black;font-weight:bold">Strengths</p>
                                                    <p>{{$evaluation->tutor_strength}}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p style="color:black;font-weight:bold">Weakness</p>
                                                    <p>{{$evaluation->tutor_weakness}}</p>
                                                </div>
                                                @if ($evaluation->website_reference != "")
                                                    <div class="col-md-12">
                                                        <p style="color:black;font-weight:bold">Website Reference</p>
                                                        <p>{{$evaluation->website_reference}}</p>
                                                    </div>
                                                @endif
                                                
                                                <div id="add-review" class="add-review-box">
                                                    <div class="leave-rating-wrap">
                                                        <span class="leave-rating-title">Your rating for {{$evaluation->tutor->user_name}}: </span>
                                                        <div class="leave-rating" id="star-ratings">
                                                            <input type="radio" name="rating" id="rating-1" value="1" onclick="saveStars({{$evaluation->id}}, 5)"/>
                                                            <label name="rating-label" for="rating-1" class="fa fa-star-o"></label>
                                                            <input type="radio" name="rating" id="rating-2" value="2" onclick="saveStars({{$evaluation->id}}, 4)"/>
                                                            <label name="rating-label" for="rating-2" class="fa fa-star-o"></label>
                                                            <input type="radio" name="rating" id="rating-3" value="3" onclick="saveStars({{$evaluation->id}}, 3)"/>
                                                            <label name="rating-label" for="rating-3" class="fa fa-star-o"></label>
                                                            <input type="radio" name="rating" id="rating-4" value="4" onclick="saveStars({{$evaluation->id}}, 2)"/>
                                                            <label name="rating-label" for="rating-4" class="fa fa-star-o"></label>
                                                            <input type="radio" name="rating" id="rating-5" value="5" onclick="saveStars({{$evaluation->id}}, 1)"/>
                                                            <label name="rating-label" for="rating-5" class="fa fa-star-o"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>    
                                            <hr>
                                            <hr>
                                            <br>
                                        </div>
                                    @endforeach
                                </div>
                                <a class="load-more-button" href="{{route('get.evals')}}">Load more <i class="fa fa-circle-o-notch"></i> </a> 
                            </div>
                        </div>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection

@push('scripts')
    <script type="text/javascript">
        function saveStars(eval_id, rating){
           $.ajax({
                type:'POST',
                url: "{{route('save.stars')}}",
                data:{
                    _token: "{{csrf_token()}}", 
                    rating: rating,
                    eval_id: eval_id
                },
                success:function(data){
                    console.log(data.msg);
                    var eval_div = document.getElementById("eval-div-" + eval_id);
                    eval_div.parentNode.removeChild(eval_div);

                    document.getElementById('star_result').innerHTML = data.msg;
                },
                error: function(xhr, status, error){
                    console.log(data.error);
                }
            });  
        }
    </script>
@endpush