@extends('layouts.citybook.default')

@section('title')
Mogi's Calendar
@endsection


@section('content')
    <section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;color:black;text-align:left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8; overflow-x: auto">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Mogi's Calendar</span></h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        @include('btn.profile.calendar', ['timezone' => "America/Denver", 'calendar_id' => "bufftutor@gmail.com"])
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection