@extends('layouts.citybook.dashboard')

@section('title')
	{{$applicant->full_name}}'s Application
@endsection

@section('page_title')
	{{$applicant->full_name}}'s Application
@endsection

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('dashboard_content')
	<div class="list-single-main-item fl-wrap" style="text-align:left">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>{{$applicant->full_name}}'s Application <br>ID: {{$applicant->id}}</h3>
	    </div>

	    <a href="{{ route('applicants')}}"><-- Back to Applicants</a><br>
		<div class="col-md-12">
			<h2></h2>
			<div class="row">
				<div class="col-md-6">
					@if ($applicant->user)
						<a href="{{route('user.details.get', $applicant->user->id)}}" target="_blank">{{$applicant->full_name}}</a> (click to update background check)<br>
					@endif
					{{$applicant->email}}  <br>
					{{$applicant->phone}}<br>
				</div>
				<div class="col-md-6">
					{!! Form::model($applicant, ['method' => 'DELETE', 'route' => ['applicant.remove', 'id' => $applicant->id], 'class' => 'form-horizontal', 'id' => 'delete-'.$applicant->id]) !!}
					<div class="btn-group pull-right">
					        {!! Form::submit("Delete Application", ['class' => 'btn btn-danger']) !!}
					    </div>
					{!! Form::close() !!}	
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">	
					@if ($applicant->offered == 1)
						<table class="table">
							<tbody>
								<tr>
									<td>
										{!! Form::open(['method' => 'PATCH', 'route' => ['applicant.update', 'id' => $applicant->id], 'class' => 'form-horizontal']) !!}	
								   			 <div class="btn-group pull-left">
								        		{!! Form::submit("Hire $applicant->first_name", ['class' => 'btn btn-primary']) !!}
								    		</div>
										{!! Form::close() !!}
									</td>
									<td>
										{!! Form::model($applicant, ['method' => 'DELETE', 'route' => ['applicant.remove', 'id' => $applicant->id], 'class' => 'form-horizontal']) !!}	
										    <div class="form-group{{ $errors->has('admin_notes') ? ' has-error' : '' }}">
										        {!! Form::label('admin_notes', 'Notes (Reason For Not Hiring Tutor)') !!}
										        {!! Form::textarea('admin_notes', null, ['class' => 'form-control', 'size' => "10x3"]) !!}
										        <small class="text-danger">{{ $errors->first('admin_notes') }}</small>
										    </div>
										    <div class="btn-group pull-right">
										        {!! Form::submit("Do Not Hire", ['class' => 'btn btn-danger']) !!}
										    </div>
										{!! Form::close() !!}	
									</td>
								</tr>
							</tbody>
						</table>
					@endif
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<td><b>Transcript:</b></td>
						<td><a href="https://www.dropbox.com/home/Tutor/Tutor%20Applications/Transcripts" target="_blank">Dropbox</a></td>
					</tr>
					<tr>
						<td><b>URL:</b></td>
						<td><a href="{{$applicant->professional_link}}" target="_blank">{{$applicant->professional_link}}</a></td>
					</tr>
					<tr>
						<td><b>Hired:</b></td>
						<td>@if ($applicant->hired)
							<span style="color:green">Yes</span>
							@else
								<span style="color:red">No</span>
								@if ($applicant->offered)
									<br>Remember to click Hired below when background check is passed.
								@endif
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Offered:</b></td>
						<td>@if ($applicant->offered == 1)
								<span style="color:green">Yes</span>
							@elseif($applicant->offered === 0)
								<span style="color:red">Passed On Applicant</span>
							@else
								No Decision
							@endif
						</td>
					</tr>
					<tr>
						<td><b>Application Date:</b></td>
						<td>{{$applicant->created_at->format('m/d/Y')}} ({{$applicant->created_at->diffForHumans()}})</td>
					</tr>
					<tr>
						<td><b>Start / End Dates:</b></td>
						<td>{{Carbon\Carbon::parse($applicant->start_date)->format('m/d/Y') . " -> " . Carbon\Carbon::parse($applicant->end_date)->format('m/d/Y')}}</td>
					</tr>
					<tr>
						<td><b>Location:</b></td>
						<td>{{$applicant->company_locations == "Other" ? "Other - " . ($applicant->city != ''? "$applicant->city, $applicant->region" : $applicant->formatted_address) : $applicant->company_locations}}</td>
					</tr>
					<tr>
						<td><b>Session Locations:</b></td>
						<td>{{$applicant->session_locations}}</td>
					</tr>
					<tr>
						<td><b>Major/Year:</b></td>
						<td>{{$applicant->major}} / {{$applicant->year}}</td>
					</tr>
					<tr>
						<td><b>Availability:</b></td>
						<td>{{$applicant->availability}}</td>
					</tr>
					<tr>
						<td><b>Courses:</b></td>
						<td>{!! nl2br(e($applicant->courses)) !!}</td>
					</tr>
					<tr>
						<td><b>Experience:</b></td>
						<td>{!! nl2br(e($applicant->experience)) !!}</td>
					</tr>
					<tr>
						<td><b>Positive Attributes</b></td>
						<td>{!! nl2br(e($applicant->best_practices)) !!}</td>
					</tr>
					<tr>
						<td><b>Negative Attributes</b></td>
						<td>{!! nl2br(e($applicant->worst_practices)) !!}</td>
					</tr>
					<tr>
						<td><b>Concerns:</b></td>
						<td>{{$applicant->concerns}}</td>
					</tr>
					<tr>
						<td><b>Marketing:</b></td>
						<td>{{$applicant->marketing}}</td>
					</tr>
					<tr>
						<td><b>Notes:</b></td>
						<td>{{$applicant->admin_notes}}</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="row">
			<div class="col-md-8">
				<h3>References ({{$applicant->student_references->count()}})</h3>
				@foreach ($applicant->student_references as $reference)
					<b>{{$reference->full_name}} ({{$reference->email}} / {{$reference->phone}})</b> <br>
					@if ($reference->user)
						 <span style="color:red;font-weight:bold">Submitted By {{$reference->user->full_name}}</span><br>
					@endif
					<b>Course: </b>{{$reference->course}}<br>
					<b>Relationship: </b> {{$reference->relationship}} - {{$reference->duration}}<br>
					<b>Overall Effect: </b> {{$reference->effect}}<br>
					<b>Strengths: </b><br>
					{{$reference->strengths}}
					<br><br>

					<b>Weakness: </b><br>
					{{$reference->improvements}}
					<br><br>
				@endforeach
			</div>
			<div class="col-md-4">
				<h3>Exam Results</h3>
				<table class="table">
					<thead>
						<tr>
							<th>Exam</th>
							<th>Grade</th>
						</tr>
					</thead>
					<tbody>
						@if ($applicant->exam_results())
							@foreach ($applicant->exam_results() as $result)
								<tr>
									<td>{{$result->exam}}</td>
									<td>{{$result->grade}}</td>
								</tr>
							@endforeach
						@endif
					</tbody>
				</table>
				{!! Form::open(['method' => 'POST', 'route' => 'email.applicants', 'class' => 'form-horizontal', 'onsubmit' => "return confirm('Are you sure you want to email applicants?');"]) !!}
				
				    <input type="hidden" name="applicant_ids[]" value="{{$applicant->id}}">
					<input type="hidden" name="email_option" value="proficiency">
				    <div class="btn-group pull-right">
				        {!! Form::submit("Email Reminder", ['class' => 'btn btn-primary']) !!}
				    </div>
				
				{!! Form::close() !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				@if ($applicant->offered != 1)
					{{-- <table class="table">
						<tbody>
							<tr>
								<td>
									{!! Form::open(['method' => 'PATCH', 'route' => ['applicant.update', 'id' => $applicant->id], 'class' => 'form-horizontal']) !!}	
							   			 <div class="btn-group pull-left">
							        		{!! Form::submit("Hire $applicant->first_name", ['class' => 'btn btn-primary']) !!}
							    		</div>
									{!! Form::close() !!}
								</td>
								<td>
									{!! Form::model($applicant, ['method' => 'DELETE', 'route' => ['applicant.remove', 'id' => $applicant->id], 'class' => 'form-horizontal']) !!}	
									    <div class="form-group{{ $errors->has('admin_notes') ? ' has-error' : '' }}">
									        {!! Form::label('admin_notes', 'Notes (Reason For Not Hiring Tutor)') !!}
									        {!! Form::textarea('admin_notes', null, ['class' => 'form-control', 'size' => "10x3"]) !!}
									        <small class="text-danger">{{ $errors->first('admin_notes') }}</small>
									    </div>
									    <div class="btn-group pull-right">
									        {!! Form::submit("Do Not Hire", ['class' => 'btn btn-danger']) !!}
									    </div>
									{!! Form::close() !!}	
								</td>
							</tr>
						</tbody>
					</table>
				@else --}}
					@if ($applicant->offered === 0)
						Previous Decision: Do Not Offer Position<br>
					@endif
					{!! Form::open(['method' => 'POST', 'route' => ['applicant.offer', $applicant->id], 'class' => 'custom-form']) !!}
						<div class="add-list-media-header">
						    <div class="{{ $errors->has('offered') ? ' has-error' : '' }}">
						        <label for="offered_yes">
						            {!! Form::radio('offered',1,  null, ['id' => 'offered_yes', 'required' => 'required']) !!} Offer Position
						        </label>
						        @if ($applicant->offered !== 0)
						        	<label for="offered_no">
							            {!! Form::radio('offered',0,  null, ['id' => 'offered_no', 'required' => 'required']) !!} DO NOT Offer Position
							        </label>
						        @endif
						        
						        <small class="text-danger">{{ $errors->first('offered') }}</small>
						    </div>
						</div>
					    <div class="btn-group pull-right">
					        {!! Form::submit("Submit", ['class' => 'btn btn-success']) !!}
					    </div>
					{!! Form::close() !!}
				@endif
				
			</div>	
		</div>
	</div>
@endsection


@push('scripts')
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush