@extends('layouts.citybook.default')

@section('title', 'Tutor Requests')

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;text-align: left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
				<div class="listsearch-maiwrap box-inside fl-wrap">
				    <div class="list-single-main-item-title fl-wrap">
				        <h3>Tutor Requests</h3>
				    </div>

				    <div class="accordion">
				        <a class="toggle act-accordion" href="#"> Unaddressed Requests ({{$unaddressed_tutor_requests->count()}}) <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner visible">
				        	@include('btn.student_tutor_contracts.admin.partials.table_unaddressed_tutor_requests', ['tutor_requests' => $unaddressed_tutor_requests])
				        </div>

				        <a class="toggle" href="#"> Pending Tutor Response ({{$pending_requests_tutors->count()}}) <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner">
				        	<h3>Pending Tutor Response</h3>
				          	<p>Tutor contracts without a response from a tutor (before deadline).</p>
				          	@include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $pending_requests_tutors]) 
				        </div>

				        <a class="toggle" href="#"> Pending Student Response ({{$pending_requests_student->count()}}) <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner">
				        	<h3>Pending Student Response</h3>
			          		<p>Tutor responses awaiting student response (before student deadline).</p>
			          		@include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $pending_requests_student])    
				        </div>

				        <a class="toggle" href="#"> All Tutors Declined / Missed Deadline ({{$missed_tutor_requests->count()}}) <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner">
				        	<h3>Declined By Tutor</h3>
			          		<p>Tutor requests where all the contracted tutors declined or missed the deadline.</p>
			          		@include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $missed_tutor_requests])	 
				        </div>

				        <a class="toggle" href="#"> Student Declined Contracts ({{$student_declined_requests->count()}}) <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner">
				        	<h3>Declined By Student</h3>
			          		<p>Tutor requests where all contracts were declined by the student or the student missed the response deadline.</p>
			          		@include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $student_declined_requests])  
				        </div>

				        <a class="toggle" href="#"> Unassigned Requests ({{$unassigned_requests->count()}}) <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner">
				        	<h3>Unassigned Tutor Requests</h3>
				          	<p>Tutor requests with no available tutor.</p>
				          	@include('btn.student_tutor_contracts.admin.partials.table_pending_tutor_requests', ['tutor_requests' => $unassigned_requests]) 
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</section>
@endsection

@push('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

	<script>
	    $(".unassign").on("submit", function(){
	        return confirm("Are you sure you want to unassign this request?");
	    });

	    $(".delete").on("submit", function(){
	        return confirm("Are you sure you want to delete this contract?");
	    });

      $(".email-reminder").on("submit", function(){
          return confirm("Are you sure you want to send an email reminder?");
      });

      $(".sms-reminder").on("submit", function(){
          return confirm("Are you sure you want to send a TEXT reminder?");
      });
	</script>
	<script>
	    function updateTutorRate(tutor_request_id, iteration){
	      
	      var tutors_array_id = "tutors-"+ tutor_request_id + "-"+iteration;
	      var student_rate_id = "student_rate-"+ tutor_request_id + "-"+iteration;
	      var tutor_rate_id = "tutor_rate-"+ tutor_request_id + "-"+iteration;
	      var tutor_fee_id = "tutor_fee-"+ tutor_request_id + "-"+iteration;

	      var tutor_id = document.getElementById(tutors_array_id).value;
	      var student_rate = document.getElementById(student_rate_id).value;

	      
	      var tutor_fees = {!!$tutor_fees!!};
	    
	      var tutor_rate = student_rate * (1- tutor_fees[tutor_id]);
	      document.getElementById(tutor_rate_id).value = tutor_rate.toFixed(2); 
	      document.getElementById(tutor_fee_id).innerHTML ="<small>Tutor Fee: "+ tutor_fees[tutor_id] * 100 +"%</small>";     
	    }
    
	    function calcBtnAmt(tutor_request_id, iteration){
	      var student_rate_id = "student_rate-"+ tutor_request_id + "-"+iteration;
	      var tutor_rate_id = "tutor_rate-"+ tutor_request_id + "-"+iteration;
	      var btn_amt_id = "btn_amt-"+ tutor_request_id + "-"+iteration;
	      var btn_percent_id = "btn_percent-"+ tutor_request_id + "-"+iteration;

	      var student_rate = document.getElementById(student_rate_id).value;
	      var tutor_rate = document.getElementById(tutor_rate_id).value;

	      var btn_amt = student_rate - tutor_rate;
	      var btn_fee =  btn_amt / student_rate;

	      document.getElementById(btn_percent_id).value = btn_fee.toFixed(2);

	      if (btn_fee >= {{$min_btn_percent}}) {
	        document.getElementById(btn_amt_id).innerHTML ="<b><span style='color:darkgreen'>BTN Amt: $"+ btn_amt.toFixed(2) +"</span>, Fee: "+ btn_fee.toFixed(2) * 100 +"%</b></small>";  
	      }
	      else{
	        document.getElementById(btn_amt_id).innerHTML ="<b><span style='color:red'>BTN Amt: $"+ btn_amt.toFixed(2) +"</span>, Fee: "+ btn_fee.toFixed(2) * 100 +"%</b></small>";
	      }
	      
	    }

	    function showRequestDetails(row_id){
	      var all_details = document.getElementsByClassName('request-details');
	      var specific_row = document.getElementById(row_id);
	      for (var i = 0; i < all_details.length; i++) {
	        all_details[i].style.display = "none";
	      }
	      specific_row.style.display = "table-row";
	    }

	    function showDuplicateForm(row_id){
	      var all_details = document.getElementsByClassName('duplicate');
	      var specific_row = document.getElementById("duplicate-" + row_id);
	      for (var i = 0; i < all_details.length; i++) {
	        all_details[i].style.display = "none";
	      }
	      specific_row.style.display = "block";
	    }
  	</script>
@endpush
