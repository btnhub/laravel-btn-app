@extends('layouts.citybook.dashboard')
@section('title', 'Tutor Applications')
@section('page_title', 'Applications')

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('dashboard_content')
	<div class="list-single-main-item fl-wrap" style="text-align:left">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>Tutor Applications</h3>
	    </div>
	    
	    <div id="prof-div" style="display:none">
	    	
	    	<h3>Exam Results <span id="results-count"></span></h3>
	    	<a href="#" onclick="document.getElementById('prof-div').style.display = 'none'">Hide Results</a>
	    	<p id="prof-results" style="color:black;font-size: 0.85em"></p>
	    	<table class="table table-bordered" id="exam-table">
	    		<tbody>
	    			<thead>
	    				<tr>
	    					<th>Appl ID</th>
	    					<th>Date</th>
	    					<th>Name</th>
	    					<th>Location</th>
	    					<th>Exam</th>
	    				</tr>
	    			</thead>
	    	</table>
	    	<hr>
	    </div>

	    <div class="row">
        	<div class="col-md-12">
        		@foreach ($errors->all() as $error)
				    <p style="color:red;">{{ $error }}</p>
				@endforeach
        	</div>
        </div>
	    {!! Form::open(['method' => 'POST', 'route' => 'email.applicants', 'class' => 'form-horizontal', 'onsubmit' => "return confirm('Are you sure you want to email applicants?');"]) !!}
	    	
		    <table class="table table-bordered" id="applicants-table">
		        <thead>
		            <tr>
		                <th>Id</th>
		                <th>Applicant Name</th>
		                <th>Location</th>
		                <th>Major</th>
		                <th>Year</th>
		                <th>Dates</th>
		                <th>Created At</th>
		            </tr>
		        </thead>
		        <tbody>
		        </tbody>
		    </table>
		    <br>
		    <hr>
		    <p style="color:black">Send email to selected applicants</p>
		    <div class="row">
			    <div class="add-list-media-header col-xs-4">
	                <label class="radio inline"> 
	                    <input type="radio" name="email_option" value="proficiency" required checked>
	                    <span>Proficiency Exam Reminder</span> 
	                </label>
	            </div>

	            {{-- <div class="add-list-media-header col-xs-4 col-xs-offset-1">
	                <label class="radio inline"> 
	                    <input type="radio" name="email_option" value="proficiency" required>
	                    <span>Proficiency Exam Reminder</span> 
	                </label>
	            </div> --}}
	        </div>
	        <br><br>
	    	<div class="btn-group">
	            {!! Form::submit("Send Emails", ['class' => 'btn btn-success', 'id' => 'email-send']) !!}
	        </div>
	    
	    {!! Form::close() !!}
	</div>
@endsection


@push('scripts')
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" charset="utf8" src="{{ URL::asset('btn/DataTables/datatables.js') }}"></script>

	<script>
		$(function() {
		    $('#applicants-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!! route('applicants.data') !!}',
		        columns: [
		            { data: 'id', name: 'id' },
		            { data: 'first_name', name: 'first_name' },
		            { data: 'company_locations', name: 'company_locations' },
		            { data: 'major', name: 'major' },
		            { data: 'year', name: 'year' },
		            { data: 'start_date', name: 'start_date' },
		            
		            { data: 'created_at', name: 'created_at' },
		        ]
		    });
		});
	</script>

	<script type="text/javascript">
		$.ajax({
		    method: 'POST', // Type of response and matches what we said in the route
		    url: "{{route('applicants.exam.results')}}", // This is the url we gave in the route
		    data: {'_token' : "{{csrf_token()}}"}, // a JSON object to send back
		    success: function(response){ // What to do if we succeed
		        var details = response.details;
		        var prof_results_div = document.getElementById('prof-results');
		        var exam_table = document.getElementById('exam-table');

		        if(details.length){
		        	document.getElementById('prof-div').style.display = "block";
		        	document.getElementById('results-count').innerHTML = " (" + details.length + ")";
		        }
		        for (var i = 0; i < details.length; i++) {
		        	var row = exam_table.insertRow(1);
		        	var cellId = row.insertCell(0);
		        	cellId.innerHTML = details[i].id;
		        	var cellDate = row.insertCell(1);
		        	cellDate.innerHTML = details[i].date;
		        	var cellName = row.insertCell(2);
		        	cellName.innerHTML = details[i].name;
		        	var cellLoc = row.insertCell(3);
		        	cellLoc.innerHTML = details[i].location;
		        	var cellExam = row.insertCell(4);
		        	cellExam.innerHTML = details[i].exam + " - " + details[i].grade + "%";
		        }
		    },
		    error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
		        console.log(JSON.stringify(jqXHR));
		        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
		    }
		});
	</script>
@endpush