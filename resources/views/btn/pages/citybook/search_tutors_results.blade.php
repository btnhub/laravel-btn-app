@extends('layouts.citybook.default')

@section('title')
Search Tutors - BuffTutor
@endsection

@section('meta-description')
	Search our network for your perfect tutor.
@endsection

@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/map_infobox.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/markerclusterer.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-maps.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
    
@endsection

@section('content')
	<!-- Map -->
    <div class="map-container fw-map">
        <div id="map-main" data-tutors="{{$tutors_collection->toJSON()}}" data-latlng = {{$search_latlng}} data-search_radius={{$search_radius}}></div>
        @if ($tutors_collection->count() >0)
            <ul class="mapnavigation">
                <li><a href="#" class="prevmap-nav">Prev</a></li>
                <li><a href="#" class="nextmap-nav">Next</a></li>
            </ul>
        @endif
    </div>
    <!-- Map end --> 
     <section class="gray-bg no-pading no-top-padding">
        <div class="col-list-wrap fh-col-list-wrap  left-list">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="listsearch-header fl-wrap">
                            <h3>Results : <span> {{$tutors_collection->count()}} {{$request->course ? ucwords($request->course) : ''}} {{str_plural("Tutors", $tutors_collection->count())}}</span></h3>
                        </div>
                        <!-- listsearch-input-wrap  --> 
                        @if (isset($request) && !$request->lat)
                             <div class="row">
                                 <span style="color:darkred;font-weight: bold">Please select a city from the dropdown menu.</span>
                             </div>
                         @endif 
                        @if ($tutors_collection->count())
                            <div class="row" style="margin: 30px 20px 0px 20px;padding-top: 30px">
                                If you're interested in working with a tutor, save his/her profile and select the tutor when you <a href="{{route('request.get')}}" style="text-decoration: underline; color:darkblue"> request a tutor</a>.    
                            </div>
                        @else
                            <div class="row" style="margin: 30px 20px 0px 20px;padding-top: 30px">
                                We may still have a tutor who can help you! <br><a href="{{route('request.get')}}" style="text-decoration: underline; color:darkblue"> Request a tutor</a> and we'll reach out to all our tutors.    
                            </div>
                        @endif

                        <div class="list-main-wrap fl-wrap card-listing"> 
                            <div class="container">
                                <!-- listing-item -->
                                @foreach ($tutors_collection as $tutor_listing)
                                    <div class="listing-item list-layout">
                                        <article class="geodir-category-listing fl-wrap">
                                            <div class="geodir-category-img" style="width:30%">
                                                <img src="{{$tutor_listing->avatar}}" alt="">

                                                <div class="overlay"></div>
                                                {{-- <div class="list-post-counter"><span>Save</span><i class="fa fa-heart"></i></div> --}}
                                            </div>
                                            <div class="geodir-category-content fl-wrap" style="width:70%">
                                                <a class="listing-geodir-category">{{$tutor_listing->tutor_status}}</a>
                                                <h3><a href="{{$tutor_listing->url}}">{{$tutor_listing->name_education}}</a></h3>
                                                <p>{{$tutor_listing->experience}}</p>
                                                <div class="geodir-category-options fl-wrap">
                                                    
                                                    <div class="listing-rating card-popup-rainingvis" 
                                                    @if ($tutor_listing->reviews)
                                                    data-starrating2="{{$tutor_listing->rating}}"
                                                    @endif>
                                                        <span>({{$tutor_listing->reviews}} reviews)</span>
                                                    </div>

                                                    <div class="geodir-category-location"><a  href="#{{$tutor_listing->map_position}}" class="map-item"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$tutor_listing->session_locations}}</a></div>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
                                @endforeach
                            </div>
                            {{-- <a class="load-more-button" href="#">Load more <i class="fa fa-circle-o-notch"></i> </a> --}}  
                        </div>
                    </div>
                     <div class="col-md-4">   
                        <div class="listsearch-input-wrap fl-wrap" style="margin-top: 0px">
                            @include('btn.pages.citybook.partials.search_tutors_form')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--col-list-wrap -->  
    <div class="limit-box fl-wrap"></div> 

@endsection
