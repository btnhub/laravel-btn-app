@extends('layouts.citybook.dashboard')

@section('page_title', 'Add Card')

@push('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
@endpush

@section('dashboard_content')
	
	<div class="list-single-main-item fl-wrap">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>Add Debit/Credit Card To Account</span></h3>
	    </div>
	    <p style="color:black">To complete this assignment and access {{$tutor_contract->tutor->user_name}}'s contact information, please add a credit/debit card to your account.  
	    	Your card will {{-- only --}} be charged <b>after</b> each session with {{$tutor_contract->tutor->first_name}}.
	    	@if($daniels_scholar)
	    		<br>
	    		<span style="color:darkred;font-weight: bold">As a Daniels Fund scholar, your card will NOT be charged when you add it to your account.</span>
	    	@else
	    		<span style="color:darkred;font-weight: bold">The first hour will be charged now.</span>
	    	@endif
	    	
	    </p> 
		<div class="row">
			{!! Form::open(['method' => 'POST', 'route' => ['tutor.contracts.submit.card', $tutor_contract->ref_id], 'id' => 'payment-form','class' => 'form-horizontal']) !!}

	            <div class="col-sm-12">
	              @include('btn.stripe.card_form_partial')
	            </div>
	            <div class="add-comment custom-form">
	                <button class="btn big-btn color-bg flat-btn" style="background: #566985">Add Card</button>
	            </div>
	      	{!! Form::close() !!}
		</div>	
	</div>	
@endsection