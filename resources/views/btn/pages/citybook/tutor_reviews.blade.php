@extends('layouts.citybook.default')

@section('title')
{{$tutor->user_name}}'s Reviews
@endsection

@section('content')
    <section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="row">
                        <a href="{{route('profile.index', $tutor->ref_id)}}" style="color:blue;text-decoration: underline">Back To {{$tutor->first_name}}'s Profile</a>
                    </div>
                    <div class="listsearch-input-wrap fl-wrap">
                        
                        <div class="section-title center-align">
                            
                            @if (auth()->user() && auth()->user()->isStaff())   
                                <h2><span><a href="{{ route('user.details.get', ['user_id' => $tutor->id])}}">{{$tutor->full_name}}'s Reviews</a></span></h2>
                            @else
                                <h2><span>{{$tutor->user_name}}'s Reviews</span></h2>
                            @endif
                            
                            @if ($tutor->id == 63)
                                <b>BuffTutor Director</b><br>
                            @endif

                            @if ($tutor->profile->avatar)
                                <div class="user-profile-avatar"><img src="/btn/avatars/{{$tutor->profile->avatar}}" alt=""></div>
                            @endif
                            
                            @if ($references->count())
                                <div class="user-profile-rating clearfix">
                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="{{round($tutor->rating())}}">
                                    </div>
                                </div>
                            @endif
                            
                        </div>
                        <div class="row">
                            @if ($references->count())
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="list-single-main-item fl-wrap" id="reviews">
                                        <div class="list-single-main-item-title fl-wrap">
                                            <h3>Reviews -  <span> {{$reviews_count}} </span></h3>
                                        </div>

                                        <div class="reviews-comments-wrap">
                                            <!-- reviews-comments-item -->
                                            @foreach ($references as $reference)
                                                @if ($reference->assignment)
                                                    <div class="reviews-comments-item" style="padding: 0px 0px 30px 10px">
                                                        <div class="reviews-comments-item-text">
                                                            <h4>{{$reference->student->user_name}} - {{$reference->assignment->course}}</h4>
                                                            @if ($reference->rating)
                                                                <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$reference->rating}}"> </div>
                                                            @endif
                                                            
                                                            <div class="clearfix"></div>
                                                            <p>" {{$reference->website_reference}} "</p>
                                                            <span class="reviews-comments-item-date"><i class="fa fa-calendar-check-o"></i>{{$reference->created_at->format('F Y')}}</span>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach

                                            @if ($tutor->id == 63)
                                                @foreach ($prior_references as $reference)
                                                    <div class="reviews-comments-item" style="padding: 0px 0px 30px 10px">
                                                        <div class="reviews-comments-item-text">
                                                            <h4>{{$reference->first_name}} - {{$reference->course}}</h4>
                                                            <div class="clearfix"></div>
                                                            <p>" {{$reference->website_reference}} "</p>
                                                            <span class="reviews-comments-item-date"><i class="fa fa-calendar-check-o"></i>{{ date('F Y', strtotime($reference->created_at)) }}</span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif 
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection