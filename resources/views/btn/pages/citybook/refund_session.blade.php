@extends('layouts.citybook.dashboard')

@section('page_title', 'My Tutor Requests')

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('dashboard_content')
	<div class="list-single-main-item fl-wrap">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>Request A Refund</span></h3>
	    </div>
		<div class="row">
			<div class="col-md-6">
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><b>Session Ref ID:</b></td>
							<td>{{$tutor_session->ref_id}}</td>
						</tr>
						<tr>
							<td><b>Session Date:</b></td>
							<td>{{date('m/d/Y', strtotime($tutor_session->session_date))}}</td>
						</tr>
						<tr>
							<td><b>Tutor:</b></td>
							<td>{{$tutor_session->tutor->first_name}} {{$tutor_session->tutor->last_name}}</td>
						</tr>
						<tr>
							<td><b>Amount Charged:</b></td>
							<td>${{$tutor_session->amt_charged}}</td>
						</tr>
						@if ($tutor_session->isSoleSession($tutor_session->assignment_id))
						
							<tr>
								<td><b>Refund Amount:</b></td>
								<td><b>${{$requested_amount}}</b></td>
							</tr>
						@else
							<tr>
								<td><b>Refundable Amount:</b></td>
								<td>${{$requested_amount}}</td>
							</tr>
							<tr>
								<td><b>Processing Fee:</b></td>
								<td>${{$deductions}}</td>
							</tr>
							<tr>
								<td><b>Refund Amount:</b></td>
								<td><b>${{number_format($requested_amount - $deductions, 2)}}</b></td>
							</tr>
							<tr><td colspan="2">Since you have met with {{$tutor_session->tutor->first_name}} more than once, the full amount charged may not be refundable.  Please review our <a href="{{route('refund.policy')}}">Refund Policy</a> for details.</td></tr>
						@endif
						
					</tbody>
				</table>
				</div>	

				@unless($tutor_session->refundRequestSubmitted($tutor_session->assignment_id))
					
					<div class="col-md-6"> 
					<p>After you submit this request, please evaluate {{$tutor_session->tutor->first_name}}'s performance.</p>
					{!! Form::open(['method' => 'POST', 'route' => ['refund.post', 'session_ref_id' => $tutor_session->ref_id], 'class' => 'form-horizontal']) !!}
						

						{!! Form::hidden('deductions', $deductions) !!}
					    {!! Form::hidden('requested_amount', $requested_amount) !!}

					    <div class="btn-group pull-left">
					        {!! Form::submit("Request Refund", ['class' => 'btn btn-primary']) !!}
					    </div>
					
					{!! Form::close() !!}
				</div>
				@else
					<button type="button" class="btn btn-primary" disabled>Refund Has Already Been Requested</button>
				@endunless
		</div>
	</div>
@endsection

@push('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush