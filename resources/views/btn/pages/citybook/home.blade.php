@extends('layouts.citybook.default')

@if (isset($title))
	@section('title', $title)
	@section('meta-description', $meta)
@else
	@section('title', "BuffTutor | Elite College Tutors")
	@section('meta-description', "Dedicated to your success since 2009. Network of experienced, affordable & vetted private tutors. Satisfaction Guaranteed. 10% Off 1st Hour! In-Person & Online.")
@endif

@if (isset($btn_location) && $btn_location)
	@push('scripts')
		{{--For Google Search Results 04/02/2020--}}
        <script type='application/ld+json'> 
            {
              "@context": "http://www.schema.org",
              "@type": "LocalBusiness",
              "additionalType": ["Tutoring Service","Tutoring Agency", "In-Person Tutoring", "College / University Tutoring", "High School Tutoring", "Online Tutoring"],
              "name": "BuffTutor",
              "image": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg",
              "url": "https://www.BuffTutor.com",
              "email": "Contact@BuffTutor.com",
              {{-- "telephone": "+1-765-543-7542", --}}
              "foundingDate": "2009",
              "foundingLocation": "Boulder, CO",
              "logo": "https://bufftutor.com/btn/logo/BuffTutor_Logo_Small.jpg",
              "description": "Connecting students with the very best tutors in {{$btn_location->city}}, {{$btn_location->state}}! Easily find private tutors for College and High School Math, Chemistry, Physics, Biology, Calculus, Algebra, Statistics, Economics, Spanish and more.",
              "aggregateRating": {
                "@type": "aggregateRating",
                "ratingValue": "4.78",
                "ratingCount": "1347",
                "reviewCount": "1347",
                "bestRating": "5",
                "worstRating": "1"
              },
              "address": {
                "@type": "PostalAddress",
                "addressLocality": "Boulder",
                "addressRegion": "Colorado",
                "postalCode": "80306"
              },
              "location": { 
                    "@type": "LocalBusiness", 
                    "parentOrganization": {
                        "name": "BuffTutor"
                    },
                    "additionalType": ["Tutoring Service","Tutoring Agency", "In-Person Tutoring", "College / University Tutoring", "High School Tutoring", "Online Tutoring"],
                    "name" : "{{$btn_name}}",
                    "image": "{{$btn_logo}}",
                    "address": {
                         "@type": "PostalAddress",
                         "addressLocality": "{{$btn_location->city}}",
                         "addressRegion": "{{$btn_location->state}}"
                    },
                    "email" : "{{$btn_location->email}}",
                    "geo": {
                        "@type": "GeoCoordinates",
                        "latitude": "{{$btn_location->lat}}",
                        "longitude": "{{$btn_location->lng}}"
                     
                	}
              },
              "sameAs": ["https://www.facebook.com/BuffTutor", "https://www.twitter.com/BuffTutor", "https://www.instagram.com/BuffTutor"],
              "geo": {
                "@type": "GeoCoordinates",
                "latitude": "40.014984",
            	"longitude": "-105.270546"
              }
            }
        </script>
	@endpush
@endif
@section('footer_scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places"></script>
	<script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
@endsection

@section('content')
	<section class="scroll-con-sec hero-section" data-scrollax-parent="true" id="sec1">
	    <div class="bg"  data-bg="{{url('btn/images/homepage/BuffTutor-tutor-session.jpg')}}" data-scrollax="properties: { translateY: '200px' }"></div>
	    <div class="overlay"></div>
	    <div class="hero-section-wrap fl-wrap">
	        <div class="container">
	            <div class="intro-item fl-wrap">
	                <h1>Find {{isset($course) ? "$course Tutors": 'Your Perfect Tutor'}} 

	                	@if (isset($online) && $online)
	                		Online
	                	@else
	                		{{isset($city) ? "in $city, $state": ""}} 
	                	@endif

	                	Today!
	                </h1>
	                {{-- <h2>Search our network of experienced & passionate tutors</h2> --}}
	                @if (isset($university))
	                	@if (isset($online) && $online)
	                		<h2>Connect with online tutors for {{$university}} courses</h2>
	                	@else
	                		<h2>Connect with tutors at or near {{$university}}</h2>
	                	@endif
	                	
	                @else
	                	<h2>Connecting students with exceptional in-person & online tutors since 2009.</h2>
	                @endif
	            </div>
	            {{-- <div class="main-search-input-wrap">
	                <div class="main-search-input fl-wrap">
	                    {!! Form::open(['method' => 'POST', 'route' => 'search.map']) !!}
	                    	<div class="main-search-input-item">
		                        <input type="text" name="course" placeholder="Course e.g. Math or CHE_110" value="" required/>
		                    </div>
		                    <div class="main-search-input-item location" id="autocomplete-container">
		                        <input type="text" name="location" placeholder="Location" id="autocomplete-input" value="" required/>
		                    </div>
		                    <div class="main-search-input-item">
		                        {!! Form::select('course_level_id',$course_levels, null, ['id' => 'course_level_id', 'class' => 'chosen-select']) !!}
		                    </div>

		                    <input type="hidden" name="formatted_address" id="formatted_address">
                            <input type="hidden" name="lat" id="lat">
                            <input type="hidden" name="lng" id="lng">
                            <input type="hidden" name="city" id="city">
                            <input type="hidden" name="region" id="region">
                            <input type="hidden" name="country" id="country">
                            <input type="hidden" name="postal_code" id="postal_code">

		                    <button class="main-search-button">Find A Tutor</button>
	                    
	                    {!! Form::close() !!}
	                </div>
	            </div> --}}

	            
	            <div {{-- style="padding-top: 250px" --}} style="padding-top:9em">
	            	{{-- 
	            	<p style="color:white; font-size:24px"><b>OR</b></p><br> --}}
	            		
	            	<a href="{{route('request.get')}}" class="join-wrap-btn">Request A Tutor</a>
	            </div>
	                
	        </div>
	    </div>
	    <div class="bubble-bg"> </div>
	</section>
	<!-- section end -->
	<!--section -->
	{{-- <section id="sec2">
	    <div class="container">
	        <div class="section-title">
	            <h2>BuffTutor Network <br> The Very Best Tutors</h2>
	            <div class="section-subtitle">Course Categories</div>
	            <span class="section-separator"></span>
	            <p><b>buff (n) /'b&#477;f'/:</b> a person who is enthusiastically interested in and very knowledgeable about a particular subject.</p>
	        </div>
	        <!-- portfolio start -->
	        <div class="gallery-items fl-wrap mr-bot spad">
	            <!-- gallery-item-->
	            <div class="gallery-item">
	                <div class="grid-item-holder">
	                    <div class="listing-item-grid">
	                        <img  src="{{asset('btn/images/homepage/BuffTutor-tutors-chemistry-800x530.jpg')}}"   alt="BuffTutor - Chemistry Tutors">
	                        <div class="listing-counter"><span>10 </span> Tutors</div>
	                        <div class="listing-item-cat">
	                            <h3><a href="listing.html">Chemistry Buffs</a></h3>
	                            <p></p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- gallery-item end-->
	            <!-- gallery-item-->
	            <div class="gallery-item gallery-item-second">
	                <div class="grid-item-holder">
	                    <div class="listing-item-grid">
	                        <img  src="{{asset('btn/images/homepage/BuffTutor-tutors-mathematics-1892x619.jpg')}}"   alt="">
	                        <div class="listing-counter"><span>6 </span> Locations</div>
	                        <div class="listing-item-cat">
	                            <h3><a href="listing.html">Math & Statistics Buffs</a></h3>
	                            <p></p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- gallery-item end-->
	            <!-- gallery-item-->
	            <div class="gallery-item">
	                <div class="grid-item-holder">
	                    <div class="listing-item-grid">
	                        <img  src="{{asset('btn/images/homepage/BuffTutor-tutors-physics-800x530.jpg')}}"   alt="BuffTutor - Physics Tutors">
	                        <div class="listing-counter"><span>21 </span> Locations</div>
	                        <div class="listing-item-cat">
	                            <h3><a href="listing.html">Physics & Astronomy Buffs</a></h3>
	                            <p></p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- gallery-item end-->
	            <!-- gallery-item-->
	            <div class="gallery-item">
	                <div class="grid-item-holder">
	                    <div class="listing-item-grid">
	                        <img  src="{{asset('btn/images/homepage/BuffTutor-tutors-biology-800x530.jpg')}}"   alt="BuffTutor - Biology Tutors">
	                        <div class="listing-counter"><span>7 </span> Locations</div>
	                        <div class="listing-item-cat">
	                            <h3><a href="listing.html">Biology Buffs</a></h3>
	                            <p></p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- gallery-item end-->
	            <!-- gallery-item-->
	            <div class="gallery-item">
	                <div class="grid-item-holder">
	                    <div class="listing-item-grid">
	                        <img  src="{{asset('btn/images/homepage/BuffTutor-tutors-humanities-800x530.jpg')}}"   alt="BuffTutor - Humanities Tutors">
	                        <div class="listing-counter"><span>15 </span> Locations</div>
	                        <div class="listing-item-cat">
	                            <h3><a href="listing.html">Humanity Buffs</a></h3>
	                            <p></p>
	                        </div>
	                    </div>
	                </div>
	            </div>
	            <!-- gallery-item end-->
	        </div>
	        <!-- portfolio end -->
	        <a href="listing.html" class="btn  big-btn circle-btn dec-btn  color-bg flat-btn">Request A Tutor<i class="fa fa-eye"></i></a>
	    </div>
	</section> --}}
	<!-- section end -->
	<section class="gray-section" style="padding:40px 0">
		<div class="container">
			<div class="join-wrap fl-wrap">
				<div class="row">
					<div class="col-md-12">
						<h3 style="color:black"><center>Get {{$discount}}% Off Your First Hour!</center></h3>
						<center>Automatically applied to your very first session.</center>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--section -->
	<section>
	    <div class="container">
	        <div class="section-title">
	            <h2>How it works</h2>
	            <div class="section-subtitle">Discover & Connect </div>
	            <span class="section-separator"></span>
	            <p>Connect with
	            	@if (isset($university))
	            		{{$university}}
	            	@endif

	            	@if (isset($course))
	            		{{$course}}
	            	@endif
	            	tutors
	            	
	            	@if (isset($online) && $online)
	            		online
	            	@elseif(isset($city))
	            		in {{$city}}
	            	@endif
	            in 3 easy steps.</p>
	        </div>
	        <!--process-wrap  -->
	        <div class="process-wrap fl-wrap">
	            <ul>
	                <li>
	                    <div class="process-item">
	                        <span class="process-count">01 . </span>
	                        <div class="time-line-icon"><i class="fa fa-desktop"></i></div>
	                        <h4> Request A Tutor</h4>
	                        <p>Submit our <a href="{{route('request.get')}}" style="text-decoration: underline">tutor request form</a> and we'll {{-- search our network for --}} alert @if (isset($course))
	            		{{$course}}
	            	@endif
	            	tutors
	            	
	            	@if (isset($city))
	            		in {{$city}} & online
	            	@endif who fit <b>your budget and your schedule</b>.</p>
	                    </div>
	                    <span class="pr-dec"></span>
	                </li>
	                <li>
	                    <div class="process-item">
	                        <span class="process-count">02 .</span>
	                        <div class="time-line-icon"><i class="fa fa-users"></i></div>
	                        <h4> Select A Tutor</h4>
	                        <p>Tutors will submit a proposal to you. Review the proposals and select the tutor that is the best match for you.</p>
	                    </div>
	                    <span class="pr-dec"></span>
	                </li>
	                <li>
	                    <div class="process-item">
	                        <span class="process-count">03 .</span>
	                        <div class="time-line-icon"><i class="fa fa-thumbs-up"></i></div>
	                        <h4> Get To Work!</h4>
	                        <p>Don't wait, start sessions at your earliest conveneince.</p>
	                        <p>Good Luck!</p>
	                        {{-- <p style="color:black"><b style="color:green">How To Pay For Sessions:</b> Simply add a credit/debit card to your account and your tutor will charge sessions <b><u>after</u></b> they occur.</p> --}}
	                    </div>
	                </li>
	            </ul>
	            <div class="process-end"><i class="fa fa-check"></i></div>
	        </div>
	        <!--process-wrap   end-->
	    </div>
	</section>
	<!-- section end -->
	
	<!--section -->
	<section class="color-bg">
	    <div class="shapes-bg-big"></div>
	    <div class="container">
	        <div class="row">
	            <div class="col-md-6">
	                <div class="images-collage fl-wrap">
	                    <div class="images-collage-title">Buff<span>Tutor</span></div>
	                    <div class="images-collage-main images-collage-item"><img src="{{asset('btn/avatars/tutor-01.jpg')}}" alt="BuffTutor - Become A Tutor"></div>
	                    <div class="images-collage-other images-collage-item" data-position-left="23" data-position-top="10" data-zindex="2"><img src="{{asset('btn/avatars/tutor-03.jpg')}}" alt="BuffTutor - Become A Tutor"></div>
	                    <div class="images-collage-other images-collage-item" data-position-left="62" data-position-top="54" data-zindex="5"><img src="{{asset('btn/avatars/tutor-04.jpg')}}" alt="BuffTutor - Become A Tutor"></div>
	                    <div class="images-collage-other images-collage-item anim-col" data-position-left="18" data-position-top="70" data-zindex="11"><img src="{{asset('btn/avatars/tutor-06.jpg')}}" alt="BuffTutor - Become A Tutor"></div>
	                    <div class="images-collage-other images-collage-item" data-position-left="37" data-position-top="90" data-zindex="1"><img src="{{asset('btn/avatars/tutor-02.jpg')}}" alt="BuffTutor - Become A Tutor"></div>
	                </div>
	            </div>
	            <div class="col-md-6">
	                <div class="color-bg-text" style="padding-top: 10px">
	                    <h3>What Makes BuffTutor The Best</h3>
	                    <p style="font-weight:bold;color:black">BuffTutor is not just another tutor list, we're a full-service tutoring company:</p>
	                    <p style="font-weight:bold;color:black">&#10003; We thoroughly vet tutors.</p>
	                    <p style="font-weight:bold;color:black">&#10003; We make tutor recommendations.</p>
	                    <p style="font-weight:bold;color:black">&#10003; We handle all payments securely using Stripe.</p>
	                    <p style="font-weight:bold;color:black">&#10003; We're responsive to concerns raised by clients and quickly issue refunds where warranted.</p>
	                    <p style="font-weight:bold;color:black">&#10003; We review ALL evaluations and retain/dismiss tutors based on their performance</p>
	                    {{-- <a href="#" class="color-bg-link modal-open">Sign In Now</a>  --}}
	                    <br>
	                    <h3 >Your Success Is Our Success!</h3>
	                    <a href="{{route('request.get')}}" class="join-wrap-btn pull-left">Get Started!</a>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
	<!--section   end -->
	<section class="gray-section" style="padding:40px 0">
    	<div class="container">
    		<div class="join-wrap fl-wrap">
    			<div class="row">
    				<div class="col-md-12">
    					<h3 style="color:black"><center>Satisfaction Guaranteed!</center></h3>
    					<div class="row">
    						<div class="col-md-3 col-md-offset-3">
    							<p style="color:black; font-size: 1.5em;font-weight:bold">Name Your Price</p>  
    							<p style="color:black">Tell us what you can afford and we'll match you with a tutor in your budget.</p>		
    						</div>
    						<div class="col-md-3">
    							<p style="color:black; font-size: 1.5em;font-weight:bold">No Contracts</p>
    							<p style="color:black">Meet with as many or as few tutors as you'd like, as often or as little as you'd like.</p>	
    						</div>
    					</div>
    					<div class="row">
    						<div class="col-md-4 col-md-offset-4">
    							<p style="color:black; font-size: 1.5em;font-weight:bold">Didn't love your tutor?</p>  
    							<p style="color:black">If your tutor wasn't helpful or effective, let us know and we'll fully refund the first session.</p>		
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
	<!--section -->
    <section>
        <div class="container">
            <div class="section-title">
                <h2>Testimonials</h2>
                <div class="section-subtitle">Clients Reviews</div>
                <span class="section-separator"></span>
                <p>What students & parents have to say about our tutors</p>
            </div>
        </div>
        <!-- testimonials-carousel -->
        <div class="carousel fl-wrap">
            <!--testimonials-carousel-->
            <div class="testimonials-carousel single-carousel fl-wrap">
                <!--slick-slide-item-->
                @foreach ($reviews as $review)
	                @if ($review->student)
		                <div class="slick-slide-item">
		                    <div class="testimonilas-text">
		                        <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$review->rating ?? 5}}"> </div>
		                        <p>{{$review->website_reference}}</p>
		                    </div>
		                    <div class="testimonilas-avatar-item">
		                        {{-- <div class="testimonilas-avatar"><img src="images/avatar/1.jpg" alt=""></div> --}}
		                        <h4>{{$review->student->username}}.</h4>
		                        {{-- <span>University of Colorado, Boulder</span> --}}
		                    </div>
		                </div>
		            @endif
                @endforeach
                <!--slick-slide-item end-->
            </div>
            <!--testimonials-carousel end-->
            <div class="swiper-button-prev sw-btn"><i class="fa fa-long-arrow-left"></i></div>
            <div class="swiper-button-next sw-btn"><i class="fa fa-long-arrow-right"></i></div>
        </div>
        <!-- carousel end-->
    </section>
    <!-- section end -->

    <section class="gradient-bg">
    	<div class="container">
    		<div class="join-wrap fl-wrap">
    			<div class="row">
    				<div class="col-md-12">
    					<h3><center>Love Teaching? Join BuffTutor Today!</center></h3>
    					{{-- <center style="font-weight:bold; color:white">
		                    Apply today to connect with students near 
			            	@if (isset($city))
			            		{{$city}} & online.
			            	@else
			            	you!	
			            	@endif
		                </center> --}}
    					<center style="color:white">Anyone can call themselves a tutor, but only the best can be a BuffTutor.</center>
    					<br>
    					<center style="font-weight:bold; color:white">Do you have what it takes?</center>
    					<br>
    					<a href="{{route('become.tutor')}}" class="join-wrap-btn">Apply Today!</a>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
@endsection