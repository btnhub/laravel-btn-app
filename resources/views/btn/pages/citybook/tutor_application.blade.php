<?php $show_covid = strtoupper(DB::table('settings')->where('field', 'show_covid')->first()->value) === 'TRUE' ? true: false;?>

@extends('layouts.citybook.default')

@section('meta-description')
	Love teaching?  Join BuffTutor today!  We're always hiring passionate tutors.
@endsection

@section('title')
Tutor Application
@endsection

@section('footer_scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places{{-- &callback=initAutocomplete --}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
@endsection

@section('content')
	<!--  section  --> 
    <section class="parallax-section single-par list-single-section" data-scrollax-parent="true" id="sec1">
        <div class="bg par-elem "  data-bg="{{asset('btn/images/BuffTutor-Become-A-Tutor.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="list-single-header absolute-header fl-wrap">
            <div class="container">
                <div class="list-single-header-item">
                    <h2>Tutor Application</h2>
                    <span class="section-separator"></span>
                </div>
            </div>
        </div>
    </section>
    <!--  section end --> 
    <div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav scroll-init">
                <ul>
                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                    <li><a href="#qualification">Eligibility</a></li>
                    <li><a href="#contact-info">Contact Info</a></li>
                    <li><a href="#session-details">Session Details</a></li>
                    <li><a href="#experience">Previous Experience</a></li>
                    <li><a href="#additional-details">Additional Details</a></li>
                </ul>
            </nav>
            <!-- <a href="#" class="save-btn"> <i class="fa fa-heart"></i> Save </a> -->
        </div>
    </div>
    <!--  section  --> 
    <section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
         	{!! Form::open(['method' => 'POST', 'route' => 'tutor.apply.submit']) !!}
     			<div class="row">
	                <div class="col-md-10 col-md-offset-1">
	                    <!-- profile-edit-container--> 
	                    <div id="qualification" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="row">
	                        	<div class="col-md-12">
	                        		@foreach ($errors->all() as $error)
									    <p style="color:red">{{ $error }}</p>
									@endforeach
	                        	</div>
	                        </div>
	                        @if ($show_covid)
		                        <div id="covid" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
			                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
			                            <h4 style="padding-bottom: 0px">A Note About COVID-19</h4>
			                        </div>
			                     	<div class="custom-form">
			                    		<div class="row">
			                        		<div class="col-md-12">
			                        			<p style="color:black;text-align:left">
			                        			While campuses close and move courses online, we're still in busienss!  During this period, we are encouraging all our tutors to offer online tutoring.  Be sure to indicate below whether you are interested in offering teaching online.
			                        			</p>
			                        			<p style="color:black;text-align:left">We are anticipating a disruption in business, but we will continue to work around school closures and continue to reach out to students who are completing their current courses online.</p>
			                        		</div>
			                        	</div>
			                        </div>
				                </div>
			                @endif
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>Eligibility</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                               <div class="col-md-12">
		                               	<p style="color:black">Before you submit this application, please confirm that you meet our eligibility criteria.  If you do not meet <b><u>all</u></b> of the criteria but would still like to apply, please <a href="mailto:Contact@BuffTutor.com">e-mail us</a> first and describe your situation.</p>
		                               	<br>
		                               	<label><b><u>Criteria</u></b></label>
		                               	<p style="color:black">1) You must be legally allowed to work in the country that you currently reside in <b><u>and you must be allowed to work in an off-campus position</u></b>.  We do not sponsor tutors; J-1 or F-1 international students or any non-immigrants who do not have work permission cannot be a part of the BuffTutor Network.</p>
										
										<p style="color:black"> 2) You must have a 3.0 or better major GPA (out of 4.0) in your field of study, not just a 3.0 cumulative GPA</p>
										
										<p style="color:black">3) You must have earned a B or better in the course you wish to tutor. A transcript must be provided.</p>

										<p style="color:black">4) You must have at least one year (or 2 semesters) experience as a tutor, teacher, teaching/learning assistant or instructor. <b><u>This is non-negotiable!</u></b> Simply assisting friends with a class or homework or working as a grader does NOT count as teaching experience.</p>
										
										<p style="color:black">5) You must have excellent communication skills and you must be fluent in the language that you teach in. Foreign Language tutors must be fluent in the Foreign Language they wish to tutor.</p>
										
										<p style="color:black"> 6) You must be able to work with assigned students <b><u>through the end of the semester and be around until the end of finals.</u></b> If you have plans to travel during the semester, you cannot be away for more than 7 days in the semester (this does not include school holidays). You will <b><u>not</u></b> be required to be available or to work during school holidays/breaks.</p>

										<br>

										<label><b>NOTE</b></label>
										<p style="color:black"> - You cannot tutor the same course/class that you are currently employed by the university or school in any capacity, such as a professor, teacher, teaching assistant, learning assistant, course assistant, grader, etc. For example, if you are hired to assist with course XYZ at School A, while employed in that position, you cannot tutor that specific course for students attending School A</p>

										<p style="color:black">- To tutor a course that is part of a series, you must have completed all courses in the series with a B or better. In other words, to tutor Calc 1, you must have completed Calc 1-3, achieved at least B each course. </p>

										<p style="color:black">- You can only tutor courses you are proficient in, that are directly related to your field of study, or that you have experience tutoring/teaching; in other words, simply taking a writing course does not make you qualified to be a writing tutor.</p>
										<div class="fl-wrap filter-tags" >    
	                                        <input type="checkbox" name="eligibility" id="eligibility" required {{old('eligibility') ? "checked" : ""}}>
	                                        <label for="eligibility" style="color:darkred">I meet the listed eligibility criteria. <small class="text-danger" style="color:red">{{ $errors->first('eligibility') }}</small></label>
	                                   </div>
	                               </div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end--> 
	                    <!-- profile-edit-container--> 
	                    <div id="contact-info" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>My Info</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>First Name<i class="fa fa-user"></i> <small class="text-danger" style="color:red">{{ $errors->first('first_name') }}</small></label>
	                                    <input type="text" name="first_name" placeholder="Your First Name" value="{{Auth::user()->first_name ?? old('first_name')}}" required />
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Last Name<i class="fa fa-user"></i><small class="text-danger" style="color:red">{{ $errors->first('last_name') }}</small></label>
	                                    <input type="text" name="last_name" placeholder="Your Last Name" value="{{Auth::user()->last_name ?? old('last_name')}}" required/>
	                                </div>    
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>Email<i class="fa fa-envelope-o"></i> <small class="text-danger" style="color:red">{{ $errors->first('email') }}</small></label>
	                                    <input name="email" type="email" placeholder="Your Email" value="{{Auth::user()->email ?? old('email')}}" required/>        
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Phone<i class="fa fa-phone"></i><small class="text-danger" style="color:red">{{ $errors->first('phone') }}</small></label>
	                                    <input name="phone" type="text" placeholder="Your Phone" value="{{Auth::user()->phone ?? old('phone')}}" required/>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>Major<i class="fa fa-university"></i> <small class="text-danger" style="color:red">{{ $errors->first('major') }}</small></label>
	                                    <input name="major" type="text" placeholder="List all majors & minors" value="{{Auth::user()->profile->major ?? old('major')}}" required/>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Highest Level Of Education<i class="fa fa-graduation-cap"></i> <small class="text-danger" style="color:red">{{ $errors->first('year') }}</small></label>
	                                    <input name="year" type="text" placeholder="2nd Year PhD Student, Graduated (B.S.)" value="{{Auth::user()->profile->year ?? old('year')}}" required/>
	                                </div>    
	                            </div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end--> 

	                    <!-- profile-edit-container--> 
	                    <div id="session-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>Session Details</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                                <div class="col-md-5">
	                                    <label>Select all tutoring services you offer:</label>
	                                 </div>
	                                 <div class="col-md-7">
	                                    <div class="fl-wrap filter-tags" style="margin-top:0px">    
	                                        <input id="in_person_tutoring" type="checkbox" name="services[]" value="in-person" onclick="showLocation()" {{old('services') ? (in_array('in-person', old('services')) ? "checked" : "") : "checked" }}>
	                                        <label for="in_person_tutoring">In-Person Tutoring &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	                                        <input id="online_tutoring" type="checkbox" name="services[]" value="online" onclick="showLocation()" {{old('services') && in_array('online', old('services')) ? "checked" : ""}}>
	                                        <label for="online_tutoring">Online Tutoring</label>
	                                    </div> 
	                                </div>
                                </div>
                                <div class="row" id="location_row">
                                	<div class="col-md-6">
	                                    <label>City for <b>in-person sessions</b><i class="fa fa-map-marker"></i></label>
	                                    <input name="session_locations" id="autocomplete-input" id="session_locations" type="text" placeholder="Where you tutor in-person" value="{{old('session_locations')}}"/>
	                                </div>
	                                {{-- <div class="col-md-6">
	                                    <label>Radius around location</label>
	                                    <div class="col-xs-6">
	                                    	<input id="radius" name="radius" type="number" min="0" style="padding:15px 20px" value="{{old('radius')}}"/>
	                                    </div>
	                                    <div class="col-xs-6">
		                                    <div class="add-list-media-header">
	                                            <label class="radio inline"> 
	                                            <input type="radio" name="radius_unit" value="miles" checked>
	                                            <span>miles</span> 
	                                            </label>
	                                            <label class="radio inline"> 
	                                            <input type="radio" name="radius_unit" value="km">
	                                            <span>km</span> 
	                                            </label>
	                                        </div>
	                                    </div>
	                                </div> --}}
                                </div>
	                            <div class="row" id="online_row" style="display:none">
		                            <div class="col-md-12">
		                            	<label><b>Describe How You Tutor Online.</b> Be sure to include the tools you will use and, if you've had experience tutoring online, how long you have been tutoring online. If you are a Math/Science tutor, describe how you will type equations and draw diagrams.</label>
		                            	<textarea id="online_method" name="online_method" cols="40" rows="3" placeholder="Online tutoring approach.">{{old('online_method')}}</textarea>
		                            </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>How many hours a week are you available?<i class="fa fa-clock-o"></i></label>
	                                    <input name="availability" type="text" placeholder="Availability per week" value="{{old('availability')}}" required/>
	                                </div>
	                            </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label><b>Courses Tutored.</b> List each course you can tutor and avoid vague statements such as "any Physics course"  because that is a false and useless statment. If you have experience tutoring/teaching a listed course, indicate that below.</label>
                                        <textarea name="courses" cols="40" rows="3" placeholder="  
Precalc
Calc 1 (2 semesters as lead T.A.)
Calc 2
Organic Chemistry 1 (1 semester as a tutor)
                                        " required>{{old('courses')}}</textarea>        
                                    </div>
                                </div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->

	                    <!-- profile-edit-container--> 
	                    <div id="experience" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>Previous Experience</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                                <div class="col-md-12">
	                                    <label>Past Teaching/Tutoring Experience</label>
	                                    <p>Detail your past teaching and tutoring experience. Be sure to include:<br> 
	                                    1) Duration at each position (1 semester, 1 year, 6 months) <br>
	                                    2) Challenges you faced and how you handled them <br>
	                                    3) How your past experience has influenced your teaching/tutoring approach and what makes you an exceptional tutor.
	                                    <br>
	                                    4) What qualities make a great teacher?  What are characteristics of bad teachers?
	                                    </p>
	                                    <textarea name="experience" cols="40" rows="3" placeholder="Previous Teaching Experience" required>{{old('experience')}}</textarea>    
	                                </div>
	                                
	                            </div>
	                            {{-- <div class="row">
	                                <div class="col-md-6">
	                                    <label>Positive Teaching Qualities</label>
	                                    <p>
	                                        List 5 positive qualities of previous teachers/mentors that you've had and how you (or how you will) incorporate those attributes when you work with students.
	                                    </p>
	                                    <textarea name="best_practices" cols="40" rows="3" placeholder="What makes a great teacher" required>{{old('best_practices')}}</textarea> 
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Negative Teaching Qualities</label>
	                                    <p>
	                                        List 5 negative qualities of previous teachers/mentors that you've had, why they were ineffective  and how you do (or would do) better.
	                                    </p>
	                                    <textarea name="worst_practices" cols="40" rows="3" placeholder="Negative teaching attributes" required>{{old('worst_practices')}}</textarea> 
	                                </div>
	                            </div> --}}
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->                                            
	                    <!-- profile-edit-container--> 
	                    <div id="additional-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4>Additional Details</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>Start Date (when you'd like to begin)<i class="fa fa-calendar"></i></label>
	                                    <input type="date" name="start_date" min="{{date('Y-m-d')}}" value="{{old('start_date')}}" required>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>End Date (e.g. end of school year)<i class="fa fa-calendar"></i></label>
	                                    <input type="date" name="end_date" min="{{Carbon\Carbon::now()->addWeek()->format('Y-m-d')}}" value="{{old('end_date')}}" required>
	                                </div>
	                            </div>
	                            <div class="row">
	                                <div class="col-md-12">
	                                    <label>Concerns</label>
	                                    <p>
	                                        Any thing we need to keep in mind when reviewing your application. For example, "I am job searching and will leave mid-semester when I get my dream job". 
	                                    </p> 
	                                    <input type="text" name="concerns" placeholder="Concerns" style="padding: 15px 20px" value="{{old('concerns')}}">        
	                                </div>
	                            </div>
	                            <div class="row">
	                            <div class="col-md-12">
	                                    <label>How did you hear about us? *</label>
	                                    <input type="text" name="marketing" placeholder="Marketing" style="padding: 15px 20px" value="{{old('marketing')}}" required>
	                                </div>
	                            </div>
	                            @if (Auth::guest())
	                            	<div class="row">
		                            	<div class="col-md-12">
		                            		<p>Create an account to keep track of your application</p>
		                            		<div class="col-md-6">
		                            			<label>Password * <small class="text-danger" style="color:red">{{ $errors->first('password') }}</small><i class="fa fa-eye"></i></label>
		                            				{!! Form::password('password', ['placeholder' => "Password"]) !!}
		                            		</div>
		                            		<div class="col-md-6">
		                            			<label>Password Confirmation * <small class="text-danger" style="color:red">{{ $errors->first('password_confirmation') }}</small><i class="fa fa-eye"></i></label>
		                            			{!! Form::password('password_confirmation', ['placeholder' => "Re-enter Password"]) !!}
		                            		</div>
		                            	</div>
		                            </div>
	                            @endif
	                            
	                            <div class="row">
	                                <div class="col-md-12">
	                                    <div class=" fl-wrap filter-tags">
	                                        <input id="terms" type="checkbox" name="terms" value="{{old('terms') ? "checked" : ""}}" required>
	                                        <label for="terms">I agree to the <a href="{{route('terms')}}">terms of use</a></label>
	                                    </div>         
	                                </div>
	                            </div>

	                            <input type="hidden" id="lat" name="lat" value="{{old('lat')}}">
	                            <input type="hidden" id="lng" name="lng" value="{{old('lng')}}">
	                            <input type="hidden" id="formatted_address" name="formatted_address" value="{{old('formatted_address')}}">
	                            <input type="hidden" id="city" name="city" value="{{old('city')}}">
	                            <input type="hidden" id="region" name="region" value="{{old('region')}}">
	                            <input type="hidden" id="country" name="country" value="{{old('country')}}">
	                            <input type="hidden" id="postal_code" name="postal_code" value="{{old('postal_code')}}">

	                            <div class="add-comment custom-form">
	                            	<button class="btn big-btn  color-bg flat-btn">Submit Request <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>  
	                        	</div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->
	                </div>
	            </div>               	   
         	{!! Form::close() !!}   
        </div>
    </section>
    <!--  section end --> 
    <div class="limit-box fl-wrap"></div>
@endsection

@push('scripts')
	<script type="text/javascript">
    	function showLocation(){
    		/*if (document.getElementById('in_person_tutoring').checked == true) {
    			document.getElementById('location_row').style.display = "block";
    			document.getElementById('location').required = true;
    			document.getElementById('radius').required = true;
    		}

    		else{
    			document.getElementById('location_row').style.display = "none";
    			document.getElementById('location').required = false;
    			document.getElementById('radius').required = false;
    		}*/

    		if (document.getElementById('online_tutoring').checked == true) {
    			document.getElementById('online_row').style.display = "block";
    			document.getElementById('online_method').required = true;
    		}

    		else{
    			document.getElementById('online_row').style.display = "none";
    			document.getElementById('online_method').required = false;
    		}
    	}
    </script>
@endpush