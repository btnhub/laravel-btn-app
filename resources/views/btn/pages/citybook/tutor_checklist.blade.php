@extends('layouts.citybook.dashboard')

@section('page_title', 'Tutor Checklist')

@push('css')
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('dashboard_content')
	<div class="list-single-main-item fl-wrap">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>Tutor Checklist</h3>
	    </div>

    <div class="accordion">
        <a class="toggle" href="#"> Account Checklist <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
        	@include('btn.tables.tutors.checklist_account')
        </div>

        <a class="toggle" href="#"> New Student Checklist <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
        	@include('btn.tables.tutors.checklist_assignment')
        </div>

        <a class="toggle" href="#"> End Of Semester Checklist <i class="fa fa-angle-down"></i></a>
        <div class="accordion-inner">
        	@include('btn.tables.tutors.checklist_end_semester')
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush