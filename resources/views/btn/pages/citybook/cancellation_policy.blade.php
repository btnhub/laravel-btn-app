@extends('layouts.citybook.default')

@section('title')
Cancellation Policy
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Cancellation Policy</h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        <p style="color:black;text-align:left">Please carefully review our cancellation policy to understand the consequences if a student or a tutor misses a session.  To avoid fees or penalties, please provide <b>24 hour notice</b> to cancel or reschedule a session.</p>

                        <p></p>
                        <hr>
                        <p></p>

                        <h3 style="color:darkblue;font-weight:bold;font-size: 1.25em">If A Student Misses A Session</h3>

                        <h4 style="text-decoration: underline;font-weight: bold;text-align: left">Cancelling A Session</h4>

                        <p style="color:black;text-align:left">If a student cancels a scheduled session: 
                            <p style="color:black;text-align:left"><i class="fa fa-arrow-right"></i> at least 24 hours in advance, the student will not be charged </p>
                            <p style="color:black;text-align:left"><i class="fa fa-arrow-right"></i> less than 24 hours before the scheduled session or simply does not show up to a scheduled session, the student will be charged for the <b>total scheduled time</b>.  For example, if the student cancels a 2 hour session without 24 hour notification, the student will be charged for the 2 hours scheduled.</p>
                        </p>
                         
                        <h4 style="text-decoration: underline;font-weight: bold;text-align: left">Late Arrival</h4>

                        <p style="color:black;text-align:left">If a student is running late, he/she should contact his/her tutor (e.g. call or text) to inform the tutor that the student is on the way.  The tutor will wait 15 minutes for the student.  If in that time the tutor has not heard from the student, the session will be cancelled <b>and the student will be charged for the total time scheduled</b>. </p>

                        <h4 style="text-decoration: underline;font-weight: bold;text-align: left">Rescheduling a session</h4>

                        <p style="color:black;text-align:left">If a student needs to reschedule a session, the student should contact his/her tutor as soon as possible.  If the student contacta his/her tutor less than 24 hours before the scheduled session, the tutor may choose to charge the student for the last minute cancellation. </p>

                        <p></p>
                        <hr>
                        <p></p>

                        <h3 style="color:darkblue;font-weight:bold;font-size: 1.25em">If A Tutor Misses A Sessions</h3>
                        <p style="color:black;text-align:left">Tutors must provide 24 hour notice to cancel or reschedule a session.  If a tutor does not show up or cancels without 24hr notice, he/she <b>must</b> provide the student with a <b>free session </b> of equal duration as the missed session.  For example, if a tutor cancels a 2 hour session without 24 hour notification, the tutor must provide his/her student with a free 2 hour session or accumulate 2 hours for free before the tutor can resume charging for sessions with the student. </p>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection