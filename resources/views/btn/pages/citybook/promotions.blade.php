@extends('layouts.citybook.default')

@section('title')
Promotions
@endsection

@section('meta-description')
Work with our tutors and take advantage of these deals!
@endsection

@section('content')
    <section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;color:black;text-align:left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Promotions</span></h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        <h3 style="font-weight:bold">{{number_format(DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100)}}% Off First Session</h3>

                        <p>Get {{number_format(DB::table('settings')->where('field', 'first_session_discount')->first()->value * 100)}}% off the first hour of your first session with one of our tutors!</p>
                        <li>Available to new students only (students who have not met with one of our tutors before).</li>
                        <li>Automatically applied to your first session, no coupon necessary.</li>
                        <li>Redeemable only once.</li>
                        <li>Not redeemable for cash.</li>                        
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection