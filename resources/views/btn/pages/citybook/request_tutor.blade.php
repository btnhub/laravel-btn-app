<?php $show_covid = strtoupper(DB::table('settings')->where('field', 'show_covid')->first()->value) === 'TRUE' ? true: false;?>

@extends('layouts.citybook.default')

@section('meta-description')
Submit our tutor request form to connect with a tutor.
@endsection

@section('title')
Request A Tutor
@endsection

@push('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endpush

@section('footer_scripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places&callback=initAutocomplete"></script>
<script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
@endsection

@section('content')
    <!--  section  --> 
    <section class="parallax-section single-par list-single-section" data-scrollax-parent="true" id="sec1">
        <div class="bg par-elem "  data-bg="{{asset('btn/images/BuffTutor-Request-A-Tutor.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="bubble-bg"></div>
        <div class="list-single-header absolute-header fl-wrap">
            <div class="container">
                <div class="list-single-header-item">
                    <h1>Request A Tutor</h1>
                    <span class="section-separator"></span>
                </div>
            </div>
        </div>
    </section>
    <!--  section end --> 
    {{-- <div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav scroll-init">
                <ul>
                    <li><a class="act-scrlink" href="#sec1">Top</a></li>
                    <li><a href="#contact-info">Contact Info</a></li>
                    <li><a href="#course-details">Course Details</a></li>
                    <li><a href="#session-details">Session Details</a></li>
                    <li><a href="#additional-details">Additional Details</a></li>
                    <li><a href="#sms-alerts">Text Alerts</a></li>
                    <li><a href="#submit-form">How To Pay</a></li>
                </ul>
            </nav>
            <!-- <a href="#" class="save-btn"> <i class="fa fa-heart"></i> Save </a> -->
        </div>
    </div> --}}
    <!--  section  --> 
    <section class="gray-section no-top-padding" style="background-color: #e8e8e8;padding:40px 0px">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <!-- profile-edit-container--> 
                    @if ($show_covid)
	                    <div id="covid" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
	                            <h4 style="padding-bottom: 0px">A Note About COVID-19</h4>
	                        </div>
	                     	<div class="custom-form">
	                    		<div class="row">
	                        		<div class="col-md-12">
	                        			<p style="color:black;text-align:left">
	                        			While campuses close and move courses online, we're still here for you!  During this period, we are encouraging all our tutors to offer online tutoring.  Simply select the "Include Online Tutors" option if you're open to meeting online.
	                        			</p>
	                        		</div>
	                        	</div>
	                        </div>
		                </div>
	                @endif

                    {!! Form::open(['method' => 'POST', 'route' => 'request.post', 'class' => 'form-horizontal']) !!}
                    
	                    <div id="contact-info" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        {{-- <div class="custom-form">
	                        <p></p>
	                        <p style="color:black"><b>04/02/2019: Group Review Sessions</b> - Our director, <a href="{{route('profile.index', App\User::find(63)->ref_id)}}" target="_blank">Mogi M</a>, will be holding affordable group review sessions for CU-Boulder courses for the rest of the Spring semester & during the Summer semester.  Learn more & sign up on our Meetup page: <a href="https://www.meetup.com/Boulder-Tutors/" target="_blank">https://www.meetup.com/Boulder-Tutors</a>.  For private sessions, please submit the form below: </p>
	                        </div> --}}
	                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
	                            <h4 style="padding-bottom: 0px">My Contact Info</h4>
	                        </div>
	                        <div class="row">
	                        	<div class="col-md-12">
	                        		@foreach ($errors->all() as $error)
									    <p style="color:red">{{ $error }}</p>
									@endforeach
	                        	</div>
	                        </div>
	                        
	                        @if (Auth::user())
                        		<p>{{Auth::user()->full_name}}</p>
                        		<p>{{Auth::user()->email}}</p>
                        		<p>{{Auth::user()->phone}}</p>
                        		<p>If the above is incorrect, please <a href="{{route('profile.edit', Auth::user()->ref_id)}}">edit your account</a>.</p>
	                        @else
	                        	<div class="custom-form">
		                            <p>If you already have an account, <a href="{{url('/login')}}"><u>please login first</u></a>. If not, let's create an account to keep track of your tutor request.</p>
		                            <p></p>
		                            <p></p>
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <label>First Name *<i class="fa fa-user"></i><small class="text-danger" style="color:red">{{ $errors->first('first_name') }}</small></label>
		                                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-md-11">
		                                        {!! Form::text('first_name', old('first_name'), ['required' => 'required', 'placeholder' => 'Your First Name']) !!}
		                                    </div>		
		                                </div>
		                                <div class="col-md-6">
		                                    <label>Last Name *<i class="fa fa-user"></i><small class="text-danger" style="color:red">{{ $errors->first('last_name') }}</small></label>
		                                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-md-11">
		                                        {!! Form::text('last_name', old('last_name'), ['required' => 'required', 'placeholder' => 'Your Last Name']) !!}
		                                    </div>
		                                </div>    
		                            </div>
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <label>Email *<i class="fa fa-envelope-o"></i> <small class="text-danger" style="color:red">{{ $errors->first('email') }}</small></label>
		                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-11">
		                                       {!! Form::email('email', old('email'), ['required' => 'required', 'placeholder' => 'Your Email Address']) !!}
		                                   </div>       
		                                </div>
		                                <div class="col-md-6">
		                                    <label>Phone *<i class="fa fa-phone"></i><small class="text-danger" style="color:red">{{ $errors->first('phone') }}</small></label>
		                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-md-11">
		                                        {!! Form::text('phone', old('phone'), ['required' => 'required', 'placeholder' => 'Your Phone Number']) !!}
		                                    </div>
		                                </div>
		                            </div>
		                            <div class="row">
		                                <div class="col-md-6">
		                                    <label>Password *<i class="fa fa-eye"></i><small class="text-danger" style="color:red">{{ $errors->first('password') }}</small></label>
		                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} col-md-11">
		                                        {!! Form::password('password', ['required' => 'required', 'placeholder' => 'Password']) !!}
		                                        
		                                    </div>
		                                </div>
		                                <div class="col-md-6">
		                                    <label>Confirm Password *<i class="fa fa-eye"></i><small class="text-danger" style="color:red">{{ $errors->first('password_confirmation') }}</small></label>
		                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} col-md-11">
		                                        {!! Form::password('password_confirmation', ['required' => 'required', 'placeholder' => 'Re-enter Your Password']) !!}
		                                    </div>
		                                </div>    
		                            </div>
		                        </div>
	                        @endif
	                    </div>
	                    <!-- profile-edit-container end--> 

	                    <!-- profile-edit-container--> 
	                    <div id="course-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
	                            <h4 style="padding-bottom: 0px">Course Details</h4>
	                        </div>
	                        <div class="custom-form">
	                            {{-- <div class="row">
	                                <div class="col-md-6">
	                                    <label>Course Level *<small class="text-danger" style="color:red">{{ $errors->first('level') }}</small></label>
	                                    <div class="form-group{{ $errors->has('level') ? ' has-error' : '' }}">
	                                        {!! Form::select('level', $course_level_options, null, ['id' => 'level', 'class' => 'form-control', 'required' => 'required']) !!}
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>Course Name (include course number, if known) *<i class="fa fa-book"></i><small class="text-danger" style="color:red">{{ $errors->first('course') }}</small></label>
	                                    <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
	                                        {!! Form::text('course', old('course'), ['required' => 'required', 'placeholder' => 'Ex: Calc 2 - MATH 2300']) !!}
	                                    </div>
	                                </div>
	                            </div> --}}
	                            <div class="row">
	                            	<div class="col-md-6">
	                                    <label>Course Name (include course number, if known) *<i class="fa fa-book"></i><small class="text-danger" style="color:red">{{ $errors->first('course') }}</small></label>
	                                    <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }} col-md-11">
	                                        {!! Form::text('course', old('course'), ['required' => 'required', 'placeholder' => 'Ex: Calc 2 - MATH 2300']) !!}
	                                    </div>
	                                </div>
	                            	<div class="col-md-6">
	                            		<label>School (the school you are taking the course) *<i class="fa fa-building"></i><small class="text-danger" style="color:red">{{ $errors->first('school') }}</small></label>
			                            <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }} col-md-11">
			                                {!! Form::text('school', old('school'), ['required' => 'required', 'placeholder' => "School Name"]) !!}
			                            </div>
	                            	</div>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->

	                    <!-- profile-edit-container--> 
	                    <div id="course-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
	                            <h4 style="padding-bottom: 0px">Budget</h4>
	                        </div>
	                        <div class="custom-form">
	                        	<div class="row">
	                            	<div class="col-md-12">
	                            		<p style="color:black">Each tutor sets his/her own rate, let us know what you can afford and we'll match you with tutors in your budget. <br>
	                            			<small>If no one is available at a lower rate, we may recommend a tutor $5-10/hr higher than your selected rate.</small>{{-- Review rate guide below to see what tutors typically charge. --}}</p>

	                                    {{-- Rate Guide Table --}}
	                                    {{-- <div class="row"> --}}
	                                        {{-- <div class="col-md-6 col-sm-6">
	                                            <div class="card card-block">
                                       				This is just a guide, rates vary based on a tutor's education, years of experience, location, the course level, how often you want to meet, etc.
                                        			@include('btn.forms.partials.rate_guide')
	                                        	</div>
	                                    	</div> --}}
	                            	{{-- </div>
	                            	<div class="col-md-6"> --}}
	                            		<label>Max Affordable Rate* <small class="text-danger" style="color:red">{{ $errors->first('max_rate') }}</small></label>
										<div class="form-group{{ $errors->has('max_rate') ? ' has-error' : '' }} col-sm-6">
									       {!! Form::select('max_rate', $rate_options, old('max_rate'), ['id' => 'max_rate','class' => 'form-control', 'required' => 'required']) !!}
									   	</div>  
									   	<div class="col-sm-6">
	                                            {{-- <div class="card card-block"> --}}
                                            	<div class="accordion">
				                                    <a class="toggle">Click For Rate Guide</a>
													<div class="accordion-inner">
                                               			This is just a guide; rates vary based on a tutor's location, education, years of experience, the course level, how often you want to meet, etc.
                                                		@include('btn.forms.partials.rate_guide')
                                                	</div>
	                                            </div>
	                                        </div> 
	                            	</div>
	                            </div> 
	                    	</div>
	                    </div>
	                    <!-- profile-edit-container end-->

	                    <!-- profile-edit-container--> 
	                    <div id="session-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
	                            <h4 style="padding-bottom: 0px">Session Details</h4>
	                        </div>
	                        <div class="custom-form">
	                            <div class="row" style="margin-bottom:10px">
	                            	<div class="col-md-6">
	                            		<label>Session Location (where you would prefer to meet) * <i class="fa fa-map-marker"></i></label>
	                            		<input name="location" id="autocomplete-input" type="text" placeholder="Your City" value="{{old('location')}}"/>
	                            	</div>
	                            	<div class="col-md-6">
	                            		<div class="fl-wrap filter-tags" style="padding-bottom: 20px">
	                            			<input style="margin-bottom: 0px" id="include_online_tutors" type="checkbox" name="include_online_tutors" value="1" 
	                            			@if (old('include_online_tutors'))
	                            				checked
	                            			@endif>
	                                		<label for="include_online_tutors">Include Online Tutors (Recommended)</label>
	                                	</div>
	                            	</div>
	                            </div>
	                            
	                            <div class="row">
	                                <div class="col-md-6">
	                                    <label>When would you like to meet? Select all that work for you * <small class="text-danger" style="color:red">{{ $errors->first('session_time[]') }}</small></label>
	                                    <div class="form-group{{ $errors->has('session_time[]') ? ' has-error' : '' }} col-md-11">
	                                        {!! Form::select('session_time[]', $session_times_options, null, ['id' => 'session_time[]', 'class' => 'form-control', 'required' => 'required', 'multiple']) !!}
	                                    </div>
	                                </div>
	                                <div class="col-md-6">
	                                    <label>How often would you like to meet? *<i class="fa fa-book"></i><small class="text-danger" style="color:red">{{ $errors->first('frequency') }}</small></label>
	                                    <div class="form-group{{ $errors->has('frequency') ? ' has-error' : '' }}">
	                                        {!! Form::text('frequency', null, ['required' => 'required', 'placeholder' => 'Twice A Week, Only before exams, Mondays after 3pm']) !!}
	                                        
	                                    </div>
	                                </div>
	                            </div>                           
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->

	                    <!-- profile-edit-container--> 
	                    @if ($favorites && $favorites->count())
		                    <div id="additional-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
		                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
		                            <h4 style="padding-bottom: 0px">Select Tutors</h4>
		                        </div>
		                        <div class="custom-form">
		                            <!-- Checkboxes -->
		                            <div class="fl-wrap filter-tags" style="margin-top:0px">
		                                <h4>Select tutors to work with from your list of saved tutors (optional):</h4>
		                                @forelse ($favorites as $favorite)
		                                	@if ($favorite->tutor)
		                                		<label for="fav-{{$favorite->tutor->ref_id}}">
		                                			<input id="fav-{{$favorite->tutor->ref_id}}" type="checkbox" name="requested_tutors[]" value="{{$favorite->tutor->ref_id}}" 
			                                		@if (old('requested_tutors') && in_array($favorite->tutor->ref_id,old('requested_tutors')))
			                                			checked
			                                		@endif>
			                                		<span>&nbsp;&nbsp;{{$favorite->tutor->user_name}} - {{$favorite->tutor->profile->major}}</span>
		                                		</label>
		                                	@endif
		                                @empty
		                                	<span style="color:darkblue; font-weight:bold;padding-bottom: 10px">0 Tutors Saved</span>
		                                	<p></p>
		                                	<p></p>
		                                	<p>Feel free to <a href="{{route('get.search.map')}}"><u>search our network</u></a> for a tutor you'd like to work with, or simply submit this form and we'll reach out to all qualified tutors.</p>
		                                @endforelse
		                            </div>
		                        </div>
		                    </div>
	                    @endif
	                    <!-- profile-edit-container end-->

	                    <!-- profile-edit-container--> 
	                    <div id="additional-details" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">
	                            <h4 style="padding-bottom: 0px">Additional Details</h4>
	                        </div>
	                        <div class="custom-form">
	                            <label>Other Requests or Concerns (Optional)</label>
	                            <textarea id="concerns" name="concerns" cols="40" rows="3" placeholder="I prefer to meet in my house, My scholarship is paying for my tutoring, I need to meet before Thursday!">{{old('concerns')}}</textarea>   
	                            @if (Auth::guest())
	                            	<div class="row">
			                        	<div class="col-md-12">
			                        		<label>Marketing *<i class="fa fa-comments"></i><small class="text-danger" style="color:red">{{ $errors->first('marketing') }}</small></label>
			                        		<div class="form-group{{ $errors->has('marketing') ? ' has-error' : '' }}">
			                        		    {!! Form::text('marketing', old('marketing'), [ 'required' => 'required', 'placeholder' => "How did you hear about us?"]) !!}
			                        		</div>
			                        	</div>
			                        </div>
	                            @endif
	                        </div>
	                    </div>
	                    <!-- profile-edit-container end-->

	                    <!-- profile-edit-container--> 
	                    <div id="sms-alerts" class="profile-edit-container add-list-container list-single-main-item fl-wrap">
	                        <div class="profile-edit-header fl-wrap">
	                            <h4 style="padding-bottom: 0px">Text Alerts</h4>
	                            <p>We respond to all requests within 2 days (even if we do not have a tutor match), however e-mail notifications can end up in your Spam/Junk. Sign up and we'll send you a text the moment we find a tutor for you. </p>

	                            <p><b>We will only text you about tutor matches, nothing else!</b></p>

	                            <div class=" fl-wrap filter-tags">
	                                <input id="sms_alert" type="checkbox" name="sms_alert" value="1" checked>
	                                <label for="sms_alert">Text Me When A Match Is Found</label>
	                            </div> 
	                        </div>
	                    </div>
	                    <div id="submit-form" class="profile-edit-container add-list-container list-single-main-item fl-wrap"> 
	                    	<div class="profile-edit-header fl-wrap" style="padding-bottom: 0px">       
	                            <h4 style="padding-bottom: 0px"><span>How To Pay For Sessions</span></h4>
	                            <p>After you've been matched with a tutor, to pay for sessions, simply add a credit/debit card to your account and your card will be charged <b>after</b> each session.</p>

	                            <small>We use <a href="https://stripe.com/" target="_blank"><u>Stripe</u></a> to securely process payments.  We do not store your CC info in our database.</small>
	                            <br><br>
	                            <p><b style="color:darkred">Please do not pay tutors directly by cash, check, PayPal, Venmo or any other means.</b> This is for your own protection!  If you're not satisfied with a tutor's performance, we are happy to issue a refund, but we cannot do so if you pay the tutor directly.</p>
	                        </div>
	                        
	                        @if (Auth::guest())
	                        	<div class=" fl-wrap filter-tags">
		                            <input id="terms_of_use" type="checkbox" name="terms_of_use"  value="1" required
		                            @if (old('checked'))
		                            	checked
		                            @endif>
		                            <label for="terms_of_use">I agree to the <a href="{{route('terms')}}">Terms of Use</a> and I understand that <span style="color:darkred">I <b><u>cannot</u></b> pay my tutor directly for sessions.</label>
		                        </div>
		                        <div class=" fl-wrap filter-tags">
		                            <label for="bot">Prove you're human. <span style="color:darkblue;font-weight: bold">Simply uncheck this box ----></span></label>
		                            <input id="bot" type="checkbox" name="bot"  value="1" checked>
		                        </div>
	                        @endif
	                        
	                        <input type="hidden" id="lat" name="lat" value="{{old('lat')}}">
	                        <input type="hidden" id="lng" name="lng" value="{{old('lng')}}">
	                        <input type="hidden" id="formatted_address" name="formatted_address" value="{{old('formatted_address')}}">
	                        <input type="hidden" id="city" name="city" value="{{old('city')}}">
	                        <input type="hidden" id="region" name="region" value="{{old('region')}}">
	                        <input type="hidden" id="country" name="country" value="{{old('country')}}">
	                        <input type="hidden" id="postal_code" name="postal_code" value="{{old('postal_code')}}"> 

	                        <div class="add-comment custom-form">
	                        	{{-- <br>
	                             <div class="g-recaptcha" data-sitekey="{{config('services.google_recaptcha.key')}}"></div>
                                    <small class="text-danger">{{ $errors->first('g-recaptcha-response') }}</small> --}}
	                            <button class="btn  big-btn  color-bg flat-btn">Submit Request <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>    
	                        </div>
	                        
	                    </div>
	                    <!-- profile-edit-container end-->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
    <!--  section end --> 
    <div class="limit-box fl-wrap"></div>

@endsection