@extends('layouts.citybook.default')

@section('title')
College Courses Tutored - BuffTutor
@endsection

@section('meta-description')
Connect with an experienced & passionate tutor near your college and online.
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;font-family: serif;text-align: left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3 style="font-size:1.75em">Colleges Courses</h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        <p>NOTE: Available tutors and the courses they offer vary each semester.  Please <a style="color: #337ab7" href="{{route('request.get')}}">request a tutor</a> or contact us to inquire into the availability of tutors for a specific course.</p>
                        <br>
                        <div class="row">
                            <?php $count = 0;?>
                            <div class="col-sm-3">
                                @foreach ($uni_array as $uni_id => $uni_name)
                                    <div class="listsearch-header fl-wrap" style="padding-top:1em">
                                        <h3>{{$uni_name}}</h3>
                                    </div>
                                    @foreach ($courses[$uni_id] as $course)
                                        <ul>
                                            <li style="padding: 5px 10px"><span><a style="color: #337ab7" href="{{route('home.course', [$course->id, strtolower(str_replace(" ", "_",$course->course_number)."-tutors-".str_replace(" ", "_",$uni_name))])}}">{{$course->course_number}} {{$course->name}} Tutors</a></span></li>
                                        </ul>
                                        <?php $count++;?>
                                        @if (!($count % $chunk_size))
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="listsearch-header fl-wrap" style="padding-top:1em">
                                                    <h3 style="font-size: 0.85em">{{$uni_name}} (cont)</h3>
                                                </div>
                                        @endif
                                    @endforeach    
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection