@extends('layouts.citybook.default')

@section('title', 'My Tutor Requests')

@push('css')
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
@endpush

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;text-align: left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8; color:black">
            <div class="container">
				<div class="listsearch-maiwrap box-inside fl-wrap">
				    <div class="list-single-main-item-title fl-wrap">
				        <h3>My Tutor Requests</h3>
				    </div>

				    <a href="{{route('profile.account', Auth::user()->ref_id)}}"><-- Back To My Account</a>

				    <h3>Pending Tutor Proposals</h3>
					<p>Below are tutors we've found for your recent tutor requests. Please review each tutor's profile and indicate whether or not you'd like to work with the tutor. </p>
					
					@if(!$student->hasRole('daniels-fund-student'))
						<p style="color:black;border-style: solid;border-width: 3px; padding:15px"><span style="color:darkred;font-weight: bold">Students must PREPAY for the first hour with a selected tutor.</span>  When you accept a tutor's proposal, your stored credit/debit card will <u>automatically</u> be charged the first hour with that specific tutor.  Each subsequent session will be charged AFTER it occurs. A prepaid first hour refund will be granted if you do not meet with your tutor, but must be requested within 2 weeks.  Here's our <a href="{{route('refund.policy')}}" target="_blank">refund policy</a>. </p>
					@endif
					
					<center><b style="color:darkblue">Feel free to try out or work with as many tutors as you'd like!</b></center>
					<br>
					@unless($student->customer)
					
						<p style="color:darkred">Please note, you will need to <a href="{{route('payment.settings')}}">add a credit/debit card</a> to your account to access a tutor's contact information.</p>

					@endunless
				    <div class="accordion">
				        <a class="toggle act-accordion" href="#"> Unreviewed Proposals <b>({{$unreviewed_tutor_contracts->count()}})</b> <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner visible" style="overflow-x: auto;padding:15px 0px">
				        	@include('btn.student_tutor_contracts.students.table_unreviewed_proposals', ['tutor_contracts' => $unreviewed_tutor_contracts])
				        </div>

				        <a class="toggle" href="#"> Declined Proposals <b>({{$declined_tutor_contracts->count()}})</b> <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner" style="overflow-x: auto;padding:15px 0px">
				        	@include('btn.student_tutor_contracts.students.table_declined_proposals', ['tutor_contracts' => $declined_tutor_contracts]) 
				        </div>

				        <a class="toggle" href="#"> Missed Proposals <b>({{$missed_tutor_contracts->count()}})</b> <i class="fa fa-angle-down"></i></a>
				        <div class="accordion-inner" style="overflow-x: auto;padding:15px 0px">
				        	@include('btn.student_tutor_contracts.students.table_missed_proposals', ['tutor_contracts' => $missed_tutor_contracts]) 
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</section>
	
@endsection

@push('scripts')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
@endpush