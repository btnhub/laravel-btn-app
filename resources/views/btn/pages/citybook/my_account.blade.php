<?php $show_covid = strtoupper(DB::table('settings')->where('field', 'show_covid')->first()->value) === 'TRUE' ? true: false;?>

@extends('layouts.citybook.dashboard')

@section('page_title', 'My Account')

@section('dashboard_content')
	@if (Auth::user()->isSuperUser() && Auth::user()->id != $user->id)
		<div class="list-single-main-item fl-wrap">
		    <div class="list-single-main-item-title fl-wrap">
		        <h3>{{$user->full_name}}'s Account</h3>
		    </div>
		</div>
	@endif

    @if ($show_covid)
        <div class="list-single-main-item fl-wrap">
            <p style="color:darkred;text-align:left">03/11/2020: Regarding COVID-19 & School Closures 
            </p>
            <p style="color:black;text-align:left">
                We're still in business!  Please read
                <a href="{{url('/covid19-response')}}" style="text-decoration: underline;color:blue">this page</a> for more information.
            </p>
        </div>
    @endif
    
    @if ($user->hasRole('tutor-applicant') && !$user->isTutor())
        @include('btn.pages.citybook.partials.dashboard-tutor_applicant')
    @endif

    @if ($student)	    
    	@include('btn.pages.citybook.partials.dashboard-student')
    @endif

    @if ($tutor)
        @include('btn.pages.citybook.partials.dashboard-tutor')
    @endif

    @if ($incomplete_users)
        @include('btn.pages.citybook.partials.dashboard-admin')
    @endif
@endsection
