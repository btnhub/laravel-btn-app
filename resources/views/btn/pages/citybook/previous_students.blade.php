@extends('layouts.citybook.dashboard')

@section('page_title', 'Add Previous Student')

@section('dashboard_content')
	<div class="list-single-main-item fl-wrap">
	    <div class="list-single-main-item-title fl-wrap">
	        <h3>Add Previous Student</h3>
	        <div class="row">
            	<div class="col-md-12">
            		@foreach ($errors->all() as $error)
					    <p style="color:red">{{ $error }}</p>
					@endforeach
            	</div>
            </div>
	    </div>

    	{!! Form::open(['method' => 'POST', 'route' => 'search.students', 'class' => 'custom-form', 'id' => 'search_students']) !!}

		    <div class="col-md-8 form-group{{ $errors->has('search') ? ' has-error' : '' }}">
		        <label>Name of Previous Student<i class="fa fa-user"></i></label>
		        {!! Form::text('search', null, ['autofocus' => 'autofocus']) !!}
		        <small class="text-danger" style="color:red">{{ $errors->first('search') }}</small>
		        <div class="add-comment custom-form">
                	<button class="btn big-btn  color-bg flat-btn">Search</button>  
            	</div>
		    </div>

		{!! Form::close() !!}	

	    
	    @if (isset($students))
			<p>Simply click on the student and submit the form.</p>

			<table class="table">
				<thead>
					<tr>
						<th width="30%">Student / Course</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($students as $student)	
					<tr style="border-bottom: 1px solid gray">
						<td style="vertical-align:top;">
							<p style="color:black">{{$student->first_name}} {{$student->last_name}}</p>
							<p style="color:black">{{$assignments[$student->id]}}</p>
						</td>
						<td>
							{{-- <a href="#"data-toggle="collapse" data-target="#add_student_{{$student->ref_id}}">
										Add {{$student->first_name}}
							</a> --}}
							<div {{-- class="collapse" --}} id="add_student_{{$student->ref_id}}">	
								<div class="col-md-12">		
									{!! Form::open(['method' => 'POST', 'route' => ['add.previous.student', 'ref_id' => $student->ref_id], 'class' => 'custom-form', 'id' => "add_student_{{$student->_ref_id}}"]) !!}
								        <div class="row">
								        	<div class=" col-md-8 form-group{{ $errors->has('course') ? ' has-error' : '' }}">
									            <label>New Course<i class="fa fa-book"></i></label>
									            {!! Form::text('course', null, ['class' => 'form-control', 'required' => 'required']) !!}
									            <small class="text-danger">{{ $errors->first('course') }}</small>
									        </div>	
								        </div>
								        
								        <div class="row">
								        	<div class="col-md-6 form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
									            {!! Form::label('start_date', 'Start Date') !!}
									            &nbsp;&nbsp;An estimate is sufficient
									            {!! Form::date('start_date', Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
									            <small class="text-danger" style="color:red">{{ $errors->first('start_date') }}</small>
									        </div>
									        <div class="col-md-6 form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
									            {!! Form::label('end_date', 'End Date') !!}
									            &nbsp;&nbsp;An estimate is sufficient
									            {!! Form::date('end_date', null, ['class' => 'form-control', 'min' => Carbon\Carbon::now()->addWeek()->format('Y-m-d'), 'required' => 'required']) !!}
									            <small class="text-danger">{{ $errors->first('end_date') }}</small>
									        </div>
								        </div>
								        
								        

								        {{-- <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
								            {!! Form::label('location', 'Location') !!}
								            {!! Form::select('location',$locations, null, ['id' => 'location', 'class' => 'form-control', 'required' => 'required']) !!}
								            <small class="text-danger">{{ $errors->first('location') }}</small>
								        </div> --}}
								        <div class="add-comment custom-form">
						                	<button class="btn big-btn  color-bg flat-btn">Add {{$student->first_name}}</button>  
						            	</div>
									{!! Form::close() !!}
								</div>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		@endif
	</div>
@endsection