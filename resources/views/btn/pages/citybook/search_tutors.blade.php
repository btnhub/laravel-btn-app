@extends('layouts.citybook.default')

@section('title')
Find A Tutor - BuffTutor
@endsection

@section('meta-description')
Search our network for your perfect tutor.
@endsection

@section('footer_scripts')
	<script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places"></script>
	<script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
@endsection

@section('content')
    <section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Start Here : <span>Tutor Search</span></h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        @include('btn.pages.citybook.partials.search_tutors_form')                        
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection