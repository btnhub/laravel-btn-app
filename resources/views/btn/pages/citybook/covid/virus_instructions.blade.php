@extends('layouts.citybook.default')

@section('title')
A Note About COVID-19
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>A Note About COVID-19</h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                        @if (Auth::user() && Auth::user()->isTutor())
                            @include('btn.pages.citybook.covid.tutor_instructions')
                        @else
                            @include('btn.pages.citybook.covid.student_instructions')
                        @endif
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection