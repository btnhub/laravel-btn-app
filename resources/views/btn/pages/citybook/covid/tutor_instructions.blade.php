<p style="color:black;text-align:left">To mitigate the spread of COVID-19, many universities are moving classes online for the rest of the semester.  While we anticipate that these moves will impact our ability to reach new students and affect the number of tutor requests we receive, we're still open for business and will work on new strategies to connect with students wherever they are located. 
</p>
<p style="color:black;text-align:left">Please review the following about how this may affect you:</p>
<p></p>

<p style="color:black;text-align:left"><b>Reach Out To Your Students Immediately</b></p>

<p style="color:black;text-align:left">Talk to your students about their plans moving forward and determine if they can continue to meet as before, especially if you were meeting in-person for sessions.  Also, please be patient with and accommodate students who are not feeling well or students whose schedules have been upended due to school closures by being flexible with how you implement the 24hr Cancellation Policy.</p>
<p></p>

<p style="color:black;text-align:left"><b>Transition To Online Tutoring</b></p>
<p style="color:black;text-align:left">We are encouraging all tutors to look into and consider offering online tutoring. As universities implement fully remote teaching & learning, some students may decide against returning to campus after break. They will still need assistance and will likely turn to online tutoring options. If you've never taught online, here's an opportunity to try it out!  This is also a great way to connect with students in other locations.</p>
<p></p>

<p style="color:black;text-align:left"><b>Online Tutoring Tools</b></p>
<p style="color:black;text-align:left">While we do not have our own online tutoring platform, there are a plethora of online tools that facilitate fast & reliable video conferencing and some also offer whiteboard collaboration.  Below are a few options to consider:</p>
<p></p>
<p style="color:black;text-align:left">
    <a href="https://zoom.us/" target="_blank" style="text-decoration: underline;color:blue">Zoom</a> <br>
    <a href="https://ziteboard.com/" target="_blank" style="text-decoration: underline;color:blue">Ziteboard</a><br>
    <a href="https://awwapp.com/" target="_blank" style="text-decoration: underline;color:blue">Aww</a><br>
    <a href="https://www.bitpaper.io/" target="_blank" style="text-decoration: underline;color:blue">BitPaper</a><br>
    Skype
</p>
<p></p>

<p style="color:black;text-align:left">*** We have not thoroughly tested the options above, but are simply providing suggestions for you to research & try out ***</p>

<p style="color:black;text-align:left">We require that the online platform you choose be FREE for students to use.  We recommend selecting a platform that does NOT require students to create an account....but a free platform is more important.</p>

<p style="color:black;text-align:left">It also helps to use an external tablet such as those offered by <a href="https://www.wacom.com/en-us" target="_blank" style="text-decoration: underline;color:blue">Wacom</a>.</p>
<p></p>

<p style="color:black;text-align:left"><b>Update Your Profile</b></p>
<p style="color:black;text-align:left">For the rest of the semester & school year, we will be encouraging new students to consider online tutoring over in-person tutoring.  If you are open to online tutoring, please <a href="{{route('profile.edit', Auth::user()->ref_id)}}" style="text-decoration: underline;color:blue">edit your profile</a> and, under the Details section, select the "Online" option for "Tutoring You Offer."</p>
<p></p>

<p style="font-size: 1.25em;color:darkred;text-align:center;font-weight: bold">*** If your student wants to move sessions online but you cannot do so, please let us know immediately. ***</p>