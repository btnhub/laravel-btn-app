<p style="color:black;text-align:left">To mitigate the spread of COVID-19, many universities are moving classes online for the rest of the semester.  Please review the options available to you moving forward:
</p>
<p></p>

<p style="color:black;text-align:left"><b>Current Students</b></p>
<p style="color:black;text-align:left">
	If you are already working with one of our tutors and can no longer meet in person, please talk to your tutor about online tutoring.  If your tutor cannot continue sessions online, please <a href="mailto:Contact@BuffTutor.com" style="text-decoration: underline;color:blue">notify us immediately</a> and we'll work to get you matched with a different tutor.
</p>
<p></p>

<p style="color:black;text-align:left"><b>New Students</b></p>
<p style="color:black;text-align:left">
	Please consider working with a tutor online.  If you're open to online tutoring, be sure to select the option to "Include Online Tutors" when you submit our <a href="{{route('request.get')}}" style="text-decoration: underline;color:blue">tutor request form</a>.  We are encouraging all our tutors to offer online tutoring while universities implement fully remote teaching & learning.  
</p>