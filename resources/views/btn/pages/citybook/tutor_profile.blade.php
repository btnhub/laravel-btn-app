@extends('layouts.citybook.default')

@section('title')
    @if (auth()->user() && auth()->user()->isStaff())   
        {{$tutor->full_name}}'s
    @else
        {{$tutor->user_name}}'s
    @endif
    Profile - BuffTutor
@endsection

@section('meta-description')
Looking for a great tutor? Try {{$tutor->user_name}}!
@endsection

@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/map_infobox.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/markerclusterer.js')}}"></script>
    {{-- <script type="text/javascript" src="{{asset('assets/js/citybook/maps.js')}}"></script> --}}
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-maps.js')}}"></script>
@endsection
@section('content')
<div class="scroll-nav-wrapper fl-wrap">
    <div class="container">
        <nav class="scroll-nav scroll-init">
            <ul>
                <li><a class="act-scrlink" href="#about">About</a></li>
                <li><a href="#courses">Courses</a></li>
                <li><a href="#calendar">Availability</a></li> 
                {{-- <li><a href="#location">Location</a></li> --}}
                <li><a href="#reviews">Reviews</a></li>
                <li><a href="#request-session">Work With {{$tutor->first_name}}</a></li>
            </ul>
        </nav>
        <span id="save-button" class="save-tutor"><a class="save-btn" onclick="saveTutor('save', {{$tutor->ref_id}}, '{{csrf_token()}}')"> <i class="fa fa-heart"></i> Save </a></span>
        <span id="unsave-button" class="unsave-tutor" style="display:none"><a class="save-btn" onclick="saveTutor('unsave', {{$tutor->ref_id}}, '{{csrf_token()}}')"> <i class="fa fa-check"></i> Saved </a></span>
    </div>
</div>
<!-- section end -->
<!--section -->
<section class="gray-bg" style="background-color: #e8e8e8;padding:40px 0px">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="list-single-main-item fl-wrap">
                    <div class="section-title center-align" style="padding-bottom: 0px">
                        @if (auth()->user() && auth()->user()->isStaff())	
							<h2><span><a href="{{ route('user.details.get', ['user_id' => $tutor->id])}}">{{$tutor->full_name}}</a></span></h2>
						@else
							<h2><span>{{$tutor->user_name}}.</span></h2>
						@endif
						
						@if ($tutor->id == 63)
							<b>BuffTutor Director</b><br>
						@endif

                        @if ($tutor->profile->avatar)
                        	<div class="user-profile-avatar"><img src="/btn/avatars/{{$tutor->profile->avatar}}" alt=""></div>
                        @endif
                        
                        {{-- @if ($references->count())
                        	<div class="user-profile-rating clearfix">
	                            <a href="#reviews"><div class="listing-rating card-popup-rainingvis" data-starrating2="{{round($tutor->rating())}}">
	                                ({{$reviews_count}} reviews)
	                            </div></a>
	                        </div>
                        @endif --}}
                        
                        @if ($tutor->profile->tutor_status == 0)
                        	<div class="list-single-header-item-opt fl-wrap">                            
	                            <div class="list-single-header-cat fl-wrap" style="margin-left:40%;margin-top: 10px">
	                                <a style="font-weight: bold; background: -webkit-linear-gradient(top, #e30552, #d25681)">Unavailable</a>
	                            </div>
	                        </div>
                        @endif
                        
                    </div>

                    <div class="box-widget-content">
                        <div class="list-author-widget-contacts list-item-widget-contacts">
                            <ul>
                                <li><span><i class="fa fa-graduation-cap"></i>Education :</span> <a>{{ $tutor->profile->major }}  - {{ $tutor->profile->education() }}</a></li>
                                @if ($tutor->profile->session_locations)
                                    <li><span><i class="fa fa-globe"></i> Locations :</span> <a>{{$tutor->profile->session_locations}}</a>
                                    </li>
                                @endif
                                <li><span><i class="fa fa-clock-o"></i>Joined BT :</span> <a>{{ $tutor->created_at->format('F Y') }}</a></li>
                                <li><span><i class="fa fa-sign-in"></i> Last Login :</span> <a>{{$tutor->last_login_date->diffForHumans()}}</a></li>
                              
                              	@if (auth()->user() && auth()->user()->isStaff())
	                                <li><span><i class="fa fa-usd"></i> Rate :</span> <a>${{ $tutor->profile->rate_min }} / hr - ${{ $tutor->profile->rate_max }} / hr</a></li>
	                                @if ($tutor->profile->rate_details)
	                                	<li><span><i class="fa fa-money"></i>Rate Details :</span> <a>{!! nl2br(e($tutor->profile->rate_details)) !!}</a></li>
	                                @endif
                                @endif
                            </ul>
                        </div>
                    </div>

                    <div class="list-single-main-item-title fl-wrap" id="about">
                    
                        <h3>About <span> {{$tutor->first_name}} </span></h3>
                    </div>
                    @if ($students_assisted > 3 && $total_evaluations > 3)
                        <div class="list-single-facts fl-wrap gradient-bg">
                            <!-- inline-facts -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fa fa-users"></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <div class="num" data-content="0" data-num="{{$students_assisted}}">0</div>
                                        </div>
                                    </div>
                                    <h6>Students Assisted</h6>
                                </div>
                            </div>
                            <!-- inline-facts end -->
                            <!-- inline-facts  -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fa fa-clock-o"></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <div class="num" data-content="0" data-num="{{$hours_tutored}}">0</div>
                                        </div>
                                    </div>
                                    <h6>Hours Tutored</h6>
                                </div>
                            </div>
                            <!-- inline-facts end -->
                            <!-- inline-facts  -->
                            <div class="inline-facts-wrap">
                                <div class="inline-facts">
                                    <i class="fa fa-star"></i>
                                    <div class="milestone-counter">
                                        <div class="stats animaper">
                                            <div class="num" data-content="0" data-num="{{$total_evaluations}}">0</div>
                                        </div>
                                    </div>
                                    <h6>Positive Evaluations</h6>
                                </div>
                            </div>
                            <!-- inline-facts end -->
                        </div>
                    @endif
                    
                    <p>{!! nl2br(e($tutor->profile->tutor_experience)) !!}</p>
                    
                    <a id="save-button-clear" class="btn transparent-btn float-btn save-tutor" onclick="saveTutor('save', {{$tutor->ref_id}}, '{{csrf_token()}}')"><i class="fa fa-heart"></i> Save {{$tutor->first_name}}'s Profile</a>
                    <a id="unsave-button-clear" class="btn transparent-btn float-btn unsave-tutor" onclick="saveTutor('unsave', {{$tutor->ref_id}}, '{{csrf_token()}}')" style="display:none"><i class="fa fa-check"></i> Saved {{$tutor->first_name}}'s Profile</a>

                    <span class="fw-separator"></span>
                    <div class="list-single-main-item-title fl-wrap">
                        <h3>Details</h3>
                    </div>
                    <div class="listing-features fl-wrap">
                        <ul>
                            <li><i class="fa fa-comments-o"></i> Teaches In {{ $tutor->profile->languages }}</li>
                            @if ($tutor->profile->online)
                                <li><i class="fa fa-desktop"></i> Offers Online Tutoring </li>
                            @endif
                            <li><i class="fa fa-users"></i> Group Size: 
                            @if ($tutor->profile->group_size > 1)
                                1 - {{$tutor->profile->group_size}} Students
                            @else
                                1 Student
                            @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--box-widget-wrap -->
            <div class="col-md-4">
                <div class="fl-wrap"> 
                    <!--box-widget-item -->
                    <div class="box-widget-item fl-wrap">
                        <div class="box-widget-item-header">
                            <h3>Super Offer : </h3>
                        </div>
                        <div class="box-widget">
                            <div class="banner-wdget fl-wrap">
                                <div class="overlay"></div>
                                <div class="bg"  data-bg="{{asset('btn/images/BuffTutor-Happy-student.jpg')}}"></div>
                                <div class="banner-wdget-content fl-wrap">
                                    
                                    <h4>Work With {{$tutor->first_name}} & <br>
                                        Get {{$promo_discount}}% Off Your First Hour!</h4>
                                    <a href="{{route('promotions')}}" class="color-bg">Learn More</a>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <!--box-widget-item end -->
                </div>
                @if ($tutor->geolocations->count())
                    <div class="box-widget-item fl-wrap">
                        <div class="box-widget-item-header">
                            <h3>{{$tutor->first_name}}'s Location : </h3>
                        </div>
                        <div class="box-widget">
                            <div class="map-container">
                                <div id="tutorMap" data-latitude="{{(float)$tutor->geolocations()->first()->lat}}" data-longitude="{{(float)$tutor->geolocations()->first()->lng}}"
                                data-locations="{{$locations}}"></div>
                            </div>
                            @if ($tutor->profile->session_locations)
                                <div class="box-widget-content">
                                    <div class="list-author-widget-contacts list-item-widget-contacts">
                                        <ul>
                                            <li><span><i class="fa fa-map-marker"></i>Tutor Session Location(s):</span> <a>{{$tutor->profile->session_locations}}</a></li>
                                        </ul>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            </div>
            <!--box-widget-wrap end-->
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="accordion" id="courses">
                    <div class="list-single-main-item fl-wrap">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>Courses Tutored</h3>
                        </div>
                        @if (auth()->user() && auth()->user()->isSuperUser())
                            @foreach ($tutor->proficiency_exam_results() as $exam_result)
                                <p @if ($exam_result->grade < 75)
                                    style="color:red"
                                    @else
                                    style="color:blue"
                                @endif>{{$exam_result->exam}} - {{$exam_result->grade}}%</p>
                            @endforeach
                        @endif
                        <p>{!! nl2br(e($tutor->profile->courses)) !!}</p>
                    </div>
                </div>
                <!-- list-single-main-item end --> 
            </div>
            <div class="col-md-6">
               <div class="list-single-main-item fl-wrap" id="calendar">
                    <div class="col-md-12">
                        <div class="box-widget-item-header">
                            <h3>Availability : </h3>
                        </div>
                        <div class="list-author-widget-contacts list-item-widget-contacts">
                            <ul>
                                <li><span><i class="fa fa-clock-o"></i>Time of Day:</span> <a>{{ str_replace("|*|",", " , $tutor->profile->tutor_availability) }}</a></li>
                            </ul>
                        </di    v>
                        @if ($tutor->profile->calendar_id)
                            <div class="box-widget opening-hours">
                                <iframe src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;mode=WEEK&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src={{ $tutor->profile->calendar_id }}&amp;color=%232952A3&amp;ctz={{ $timezone }}" style="border-width:0" width="100%" height="400" frameborder="0" scrolling="no"></iframe>
                            </div>   
                        @endif
                        
                    </div>
                </div> 
            </div>
        </div>

        <div class="row">
            @if ($references->count())
                <div class="col-md-6">
                    <div class="list-single-main-item fl-wrap" id="reviews">
                        <div class="list-single-main-item-title fl-wrap">
                            <h3>Reviews -  <span> {{$reviews_count}} </span> </h3>
                            @if ($references->count() < $reviews_count)
                                <br>
                                <h3 style="font-size: 0.85em;text-decoration: underline"><a href="{{route('tutor.reviews', $tutor->ref_id)}}">(View All Reviews)</a></h3>
                            @endif
                        </div>
                        
                        <div class="reviews-comments-wrap">
                            <!-- reviews-comments-item -->
                            @foreach ($references as $reference)
                                <div class="reviews-comments-item" style="padding: 0px 0px 30px 10px">
                                    <div class="reviews-comments-item-text">
                                        <h4>{{$reference->student->user_name}} - {{$reference->assignment->course}}</h4>
                                        @if ($reference->rating)
                                            <div class="listing-rating card-popup-rainingvis" data-starrating2="{{$reference->rating}}"> </div>
                                        @endif
                                        
                                        <div class="clearfix"></div>
                                        <p>" {{$reference->website_reference}} "</p>
                                        <span class="reviews-comments-item-date"><i class="fa fa-calendar-check-o"></i>{{$reference->created_at->format('F Y')}}</span>
                                    </div>
                                </div>
                            @endforeach 
                        </div>
                    </div>
                </div>
            @endif

            <div class="col-md-6">
                <div class="list-single-main-item fl-wrap" id="request-session">
                    <div class="list-single-main-item-title fl-wrap">
                        <h3>Work With {{$tutor->first_name}}</h3>
                    </div>
                    <div class="box-widget opening-hours">
                        <div class="box-widget-content">
                            <p>Try a session with {{$tutor->first_name}} and, if you're not happy, we'll fully refund the first session! </p>
                            <div class="row">
                                <div class="col-md-7">
                                    <p>Simply save this profile and select {{$tutor->first_name}} when you submit our <a href="{{route('request.get')}}" style="text-decoration: underline">tutor request form</a>. </p> 
                                </div>
                                <div class="col-md-5">
                                    <button id="save-button-lg" class="btn  big-btn  color-bg flat-btn save-tutor" onclick="saveTutor('save', {{$tutor->ref_id}}, '{{csrf_token()}}')"><i class="fa fa-heart"></i> Save {{$tutor->first_name}}'s Profile</button>
                                    <button id="unsave-button-lg" class="btn  big-btn  color-bg flat-btn unsave-tutor" onclick="saveTutor('unsave', {{$tutor->ref_id}}, '{{csrf_token()}}')" style="display:none"><i class="fa fa-check"></i> Saved {{$tutor->first_name}}'s Profile</button>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 20px">
                                <div class="col-sm-12">
                                    <a href="{{route('favorite.tutors')}}" style="color:blue;text-decoration: underline"> View My Saved Tutors</a>    
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section end -->
<div class="limit-box fl-wrap"></div>
@endsection

@push('scripts')
    <script type="text/javascript">
        var saveButtons = document.getElementsByClassName('save-tutor');
        var unsaveButtons = document.getElementsByClassName('unsave-tutor');
        if ({{$saved}}) {
            for (var i = 0; i < saveButtons.length; i++) {
                saveButtons[i].style.display = "none";
            }
            for (var i = 0; i < unsaveButtons.length; i++) {
                unsaveButtons[i].style.display = "block";
            }
        }
        else{
            for (var i = 0; i < saveButtons.length; i++) {
                saveButtons[i].style.display = "block";
            }

            for (var i = 0; i < unsaveButtons.length; i++) {
                unsaveButtons[i].style.display = "none";
            }
        }
    </script>
	<script type="text/javascript" src="{{asset('assets/js/citybook/btn-save_tutor.js')}}"></script>
@endpush