@extends('layouts.citybook.default')

@section('title')
Favorite Tutors - BuffTutor
@endsection

@section('meta-description')
	List of your favorite tutors.
@endsection

@section('footer_scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('services.google_maps.key')}}&libraries=places"></script>
    <script type="text/javascript" src="{{asset('assets/js/citybook/btn-get_geo.js')}}"></script>
@endsection

@section('content')
	<!--  section  --> 
    <section class="parallax-section" data-scrollax-parent="true">
        <div class="bg par-elem "  data-bg="{{asset('btn/images/BuffTutor-Happy-Tutor.jpg')}}" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>
        <div class="container">
            <div class="section-title center-align">
                <h2><span>My Favorite Tutors</span></h2>
                <span class="section-separator"></span>
            </div>
        </div>
    </section>
    <!--  section  end--> 
    <!--  section  --> 
    <section class="gray-bg no-pading no-top-padding">
        <div class="col-list-wrap  center-col-list-wrap left-list">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3>Favorite <span>Tutors</span></h3>
                    </div>
                    <div class="listsearch-input-wrap fl-wrap">
                        @if ($favorites->count())
                            <p>Listed below are your saved tutors.  Submit our <a href="{{route('request.get')}}">tutor request form</a> to connect with them.</p>
                        
                        @else
                            <p>Search our network for the best tutor for you.</p>
                            @include('btn.pages.citybook.partials.search_tutors_form') 
                        @endif
                    </div>
                </div>

                <div class="list-main-wrap fl-wrap card-listing">
                    @foreach ($favorites as $favorite)
                        @if ($favorite->tutor)
                            <div id="{{$favorite->tutor->ref_id}}" class="listing-item">
                                <article class="geodir-category-listing fl-wrap">
                                    <div class="geodir-category-img">
                                        <img src="/btn/avatars/{{$favorite->tutor->profile->avatar ?? 'default.jpg'}}" alt="">
                                        <div class="overlay"></div>
                                        <div id="unsave-button-{{$favorite->tutor->ref_id}}" class="list-post-counter" onclick="saveTutor('unsave', {{$favorite->tutor->ref_id}}, '{{csrf_token()}}')"><i class="fa fa-check"></i><span> Saved</span></div>
                                        <div id="save-button-{{$favorite->tutor->ref_id}}" class="list-post-counter" style="display:none" onclick="saveTutor('save', {{$favorite->tutor->ref_id}}, '{{csrf_token()}}')"><i class="fa fa-heart"></i><span> Save</span></div>
                                    </div>
                                    <div class="geodir-category-content fl-wrap">
                                        <a class="listing-geodir-category">{{$favorite->tutor->profile->tutor_status ? "Available" : "UNAVAILABLE"}}</a>
                                        <h3><a href="{{route('profile.index', $favorite->tutor->ref_id)}}">{{$favorite->tutor->user_name}} - {{$education_levels[$favorite->tutor->profile->tutor_education]}}</a></h3>
                                        <p>{{mb_strimwidth($favorite->tutor->profile->tutor_experience, 0, 150)}}...<a href="{{route('profile.index', $favorite->tutor->ref_id)}}">(more)</a></p>
                                        <div class="geodir-category-options fl-wrap">
                                            <?php $reviews_count = $favorite->tutor->evaluations()->where('website_reference', '!=', NULL)->count();?>
                                            
                                            <div class="listing-rating card-popup-rainingvis" 
                                            @if ($reviews_count)
                                            data-starrating2="4"
                                            @endif >
                                                <span>({{$reviews_count}} reviews)</span>
                                            </div>

                                            <div class="geodir-category-location"><a><i class="fa fa-map-marker" aria-hidden="true"></i> {{$favorite->tutor->profile->session_locations}}</a></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        @endif
                        
                    @endforeach
                </div>
                <!-- list-main-wrap end-->                           
            </div>
        </div>
    </section>
    <!--  section  end--> 
    <div class="limit-box fl-wrap"></div>
@endsection

@push('scripts')
	<script type="text/javascript" src="assets/js/citybook/btn-save_tutor.js"></script>
@endpush