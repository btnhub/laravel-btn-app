@extends('layouts.citybook.default')

@section('title')
Courses & Subjects Tutored - BuffTutor
@endsection

@section('meta-description')
Connect with tutors for a variety of college & high school courses.
@endsection

@section('content')
	<section class="gray-bg no-pading no-top-padding" id="sec1" style="background-color: #e8e8e8;font-family: serif;text-align: left">
        <div class="col-list-wrap  center-col-list-wrap left-list" style="background-color: #e8e8e8">
            <div class="container">
                <div class="listsearch-maiwrap box-inside fl-wrap">
                    <div class="listsearch-header fl-wrap">
                        <h3 style="font-size:1.75em">Courses & Subjects Tutored 
                            @if (isset($university))
                                At Or Near {{$university}}
                            @elseif(isset($city))
                                In {{$city}}, {{$state}}
                            @endif
                        </h3>
                    </div>
                    <!-- listsearch-input-wrap  -->  
                    <div class="listsearch-input-wrap fl-wrap">
                    {{-- <div class="profile-edit-container add-list-container list-single-main-item fl-wrap"> --}}
                        <p>NOTE: Our list of tutors and the courses they offer varies each semester.  Please <a style="color: #337ab7" href="{{route('request.get')}}">request a tutor</a> or contact us to inquire into the availability of tutors for a specific course.</p>
                        <div class="row">
                            @foreach ($courses->chunk($chunk_size) as $course_chunk)
                                <div class="col-sm-2">
                                    @foreach ($course_chunk as $c)
                                        <ul>
                                            @if (isset($university))
                                                <li style="padding: 5px 10px"><span><a style="color: #337ab7" href="{{route('home.college', strtolower(str_replace(" ", "_",$university)."-".str_replace(" ", "_",$c->course)."-tutors"))}}">{{ucwords(str_replace("_", " ",$c->course))}} Tutors</span></a></li>
                                            @elseif(isset($city))
                                                <li style="padding: 5px 10px"><span><a style="color: #337ab7" href="{{route('home.city', strtolower(str_replace(" ", "_",$city)."-".$state."-".str_replace(" ", "_",$c->course)."-tutors"))}}">{{ucwords(str_replace("_", " ",$c->course))}} Tutors</span></a></li>
                                            @else
                                                <li style="padding: 5px 10px"><span><a style="color: #337ab7" href="{{route('home.general', strtolower(str_replace(" ", "_",$c->course)."-tutors"))}}">{{ucwords(str_replace("_", " ",$c->course))}} Tutors</a></span></li>
                                            @endif
                                        </ul>
                                    @endforeach    
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- listsearch-input-wrap end -->   
                </div>                          
            </div>
        </div>
    </section>
    <div class="limit-box fl-wrap"></div>
@endsection