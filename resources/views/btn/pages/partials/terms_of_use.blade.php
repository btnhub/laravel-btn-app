<center><h3>CLIENT Terms of Service Agreement</h3></center>
<br>

This CLIENT Terms of Service Agreement is entered into by and between Mosaikz, LLC, a Colorado limited liability company, d/b/a BuffTutor and/or RamTutor, with a mailing address of 1905 15th Street, # 1932, Boulder, Colorado 80306 (the "Company"), and the parent and/or guardian of the student, if the student is less than 18 years of age, or the student, if the student is over 18 years of age (collectively, "Client").  
<br><br>

WHEREAS, Company contracts with various persons who provide tutoring services to Clients of Company (collectively, the "Tutor(s)"); and
<br><br>

WHEREAS, Client desires to hire a Tutor registered with Company to provide certain tutoring services to Client.
<br><br>

NOW, THEREFORE, in consideration of the premises and mutual promises contained herein, and for other good and valuable consideration, the receipt and sufficiency of which are hereby acknowledged, the parties agree as follows: 
<br><br>

<b>1. Engagement.</b>  Client hereby engages Company, and Company hereby accepts such engagement, subject to the terms and conditions herein set forth, to provide a Tutor to perform certain tutoring services to Client (collectively, the "Services").  The Services to be performed hereunder are non-exclusive and Client is expressly aware that Company, or its principals, and the Tutors may perform similar or identical services for other customers.  Client shall provide such reasonable time availability and access as shall be reasonably needed by Tutors to perform the requested Services.
<br><br>

a. Client may submit a general request to Company for a Tutor in a specific course or subject for the Company to match up with a Tutor.  Company requires reasonable advance notice to locate and match a prospective Tutor to Client's needs.
<br><br>

b. Client may also, after registering on the Company website, directly contact Tutors listed on the website to evaluate potential Tutors and to schedule a tutoring session.
<br><br>

<b>2. Fees.</b>  
{{-- Tutors set their own billable hourly rate in connection with Client.  Client agrees to pay for the cost of any processing or administrative fees associated with Client's selected payment method.   --}}
Client shall agree to the billable hourly rate (which includes administrative and processing fees) prior to the first tutoring session with any Tutor.  In no event shall Client pay any Tutor directly nor shall Tutor receive pay directly from Client or Client's agent for the provision of Services hereunder.  In the event that any payment required to be paid by Client hereunder is not made within three (3) days of when due, Client shall pay to Company, in addition to such payment or other charges due hereunder, a "late fee" in the amount of Fifty Dollars ($50.00).  Company reserves the right in its sole discretion to investigate allegations of improper billing, issue refunds and/or charge additional fees as circumstances dictate.
<br><br>

{{-- a. <b><u>Prepay Method (Prior to August 15, 2016)</u></b>.  Client shall compensate Company for the provision of Services by the Tutor in advance of any tutoring session using Company's online payment system.  Client must verify the hours and services performed by Tutor using the Company website in order for payment to be transferred to the Tutor.  If Client has an ongoing balance, that balance may be charged against future tutoring sessions for that Client only (no substitutions) with any Tutor from Company.
<br><br> --}}

a. <b><u>Payment Information on File{{-- (After August 15, 2016) --}}</u></b>. Client must enter credit or debit card information on the secure website from which all future payments will be made.  Client shall compensate Company for the provision of Services by the Tutor of any tutoring session using Company's online payment system. Tutor shall submit the hours and rate through the online portal.  Client must verify the hours and services performed by Tutor using the BuffTutor.com website in order for payment to be transferred to the Tutor.  Client is responsible for additional fees caused by invalid card or insufficient funds, including chargeback fees.
<br><br>

b. <b><u>Collection.</u></b> For any outstanding amount or if the credit card on file fails to authorize payment of the entire amount billed, Company shall bill an additional late fee of 5% of the outstanding balance at the end of 30 days past due, in additional to additional fees necessitated by collection, including attorneys fees.
<br><br>

<b>3. Scheduling.</b>  All scheduling shall be done directly between Client and a Tutor.  No adjustment to fees shall be made for time lost because of the late arrival by Client or due to the early termination of session by Client. Upon the scheduled arrival time of the Tutor, if Client is not available, the Tutor will wait fifteen (15) minutes, after which time the Tutor may declare the session forfeited and Client will be charged for the total number of hours the missed session was originally scheduled for.
<br><br>

<b>4. Cancellation and Refund Policy.</b>
<br><br>

a. If Client is not satisfied with a Tutor's performance during the first session between the parties, Client may either request a full refund for that session or apply payment for the unsatisfactory session to another tutoring session with a different Tutor but no more than three (3) attempts to find the best fit without additional payment.  IF NO TUTOR IS ACCEPTED BY THE CLIENT, THE CLIENT IS ENTITLED TO A FULL REFUND, WITHOUT ANY PENALTIES OR FEES.  
<br><br>

b. IF A TUTOR IS ACCEPTED AND THE CLIENT SCHEDULES A FIRST TUTORING SESSION, THE FOLLOWING CANCELLATION AND REFUND POLICY APPLIES.  
<br><br>

i. Client must provide no less than twenty-four (24) hours prior notice to the Tutor for cancellations or to re-schedule an appointment; otherwise, Client's failure to do so will result in a forfeit of the tutoring session and Client will be charged for the total number of hours the missed session was originally scheduled for. 
<br><br>

ii. The Tutors have the option (in the Tutor's sole and absolute discretion) of canceling a home appointment for a tutoring session if there is not a person present that is 18 years of age or older and, in such case, Client will be charged for the total number of hours the missed session was originally scheduled for.
<br><br>

iii. The Tutor shall provide Client with no less than twenty-four (24) hours prior notice to cancel or to reschedule an appointment; if the Tutor fails to provide the foregoing notice to Client, Client will receive a FREE makeup session that will last as long as the missed session. 
<br><br>

iv. If Client cancels any future sessions with a Tutor due to the Tutor's illegal, inappropriate or unlawful behavior or actions or a decline in the Tutor's performance (as determined by Company in Company's sole and absolute discretion), Client will only be billed forthe administrative and processing fee of 25% to be applied and/or deducted from the billed amount or the amount to be refunded. In addition, if Client has any unused but prepaid tutoring sessions, Client will receive a full refund of the outstanding balance without any deduction for the administrative fee.
<br><br>

v. The Tutor or Company may suspend or terminate Services in the event that the Tutor or Company believes (after reasonable investigation) that the Tutor or Company is being subject to emotional or physical abuse or harassment by Client or being subject to behavior that leads the Tutor to feel uncomfortable during the tutoring session. In such case, Company may, at its option, suspend or terminate the provision of Services hereunder and declare that Client has forfeited any prepaid but unused tutoring sessions.  
<br><br>

vi. If Client and the Tutor mutually agree to terminate future sessions, Company shall make every attempt to provide another Tutor; however, if an alternative Tutor is unavailable to Client or Client disapproves of the alternative Tutor, Client may receive a full refund of any unused but prepaid tutoring sessions. 
<br><br>

c. If Client has any unused but prepaid tutor sessions remaining at the end of Client's semester, Client may request a transfer of the balance to the next semester; provided, however, such balance must be utilized by Client in that following semester otherwise such balance shall be forfeited.  For example, if Client has prepaid but unused sessions for the fall semester, such balance may be transferred to the spring semester; and, if Client does not use the balance during the spring semester, such balance will be forfeited.  
<br><br>

<b>5.	Covenants of Client.</b>  Client shall not be permitted to have relations with any assigned Tutor other than a professional tutor and student/client relationship.  Client shall not contact or conduct business with any Tutor of Company for reasons unrelated to the course/subject for which they are hired through Company.  Client shall not solicit any tutoring services from any Tutor for any course/subject during the term of Tutor's contract with Company and for up to two (2) years after the Tutor is no longer working with Company tutoring service.  IF THE CLIENT MAKES OR ENTERS INTO ANY SIDE DEAL OR ARRANGEMENT WITH ANY TUTOR OF THE COMPANY FOR PRIVATE TUTORING OUTSIDE THE COMPANY RULES OR NETWORK OF SERVICES, THE CLIENT SHALL FORFEIT ANY REMAINING PREPAID BUT UNUSED HOURS FOR TUTORING SESSIONS AND SHALL PAY THE COMPANY LIQUIDATED DAMAGES IN THE AMOUNT OF FIVE HUNDRED DOLLARS ($500.00) PER INFRACTION AND ALL ATTORNEYS' FEES AND COSTS INCURRED BY THE COMPANY TO COLLECT SUCH AMOUNTS.  Client shall not disparage Company, directly or indirectly, through innuendo or otherwise, , nor its products, services, agents, representatives, directors, owners, officers, attorneys, employees, vendors, affiliates, successors or assigns, nor any person acting by, through, under or in concert with any of them, with any written or oral statement.
<br><br>

<b>6.	Disclaimers.</b>  Client acknowledges and agrees that:
<br><br>

a. Switching Tutors may cause delays in the provision of Services.
<br><br>

b. Nothing to the contrary, Company makes absolutely no representations or warranties whatsoever with respect to the success, efficacy and/or results of the Services. Company makes no warranty or guarantee that Client will improve his/her academic performance or that Client and the Tutor will have an amicable relationship.    
<br><br>

c. It is beyond the Tutor's control or the control of Company to directly affect the outcome of grades, scores on exams, homework, or other assignments.  Tutors are not responsible for determining what is covered on tests or exams. 
<br><br>

d. Company does not guarantee or warrant that the information and knowledge the Tutors provide to Client is accurate or complete.
<br><br>

e. EXCEPT AS SET OUT HEREIN, THE COMPANY DOES NOT MAKE AND HEREBY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES REGARDING THE SERVICES, WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND WARRANTIES ARISING FROM COURSE OF DEALING OR COURSE OF PERFORMANCE. FURTHERMORE, THE COMPANY REITERATES THAT IT MAKES NO GUARANTEE WITH RESPECT TO THE SUCCESS, RESULTS, EFFICACY, ETC. OF THE SERVICES. 
<br><br>

f. The Tutors are independent contractors of Company and solely responsible for their own actions and performance.  Client acknowledges and agrees that Company is not responsible or liable for conduct or behavior of the Tutors and other contractors.
<br><br>

g. Company is not liable for any loss in property or data due to the activities or omissions of the Tutors and/or Clients. THE TUTORS ASSUME ALL RISKS OF PERFORMING THE SERVICES UNDER THIS AGREEMENT. Absent THE Company's gross negligence or willful misconduct, THE Company shall in no way be liable for any PERSONAL injury, harm oR DEATH suffered by THE CLIENT.  In no event shall Company be responsible for special, indirect, consequential or other specialized measure of damages, wHETHER OR NOT FORESEEABLE and whether based on breach of contract, tort (including negligence), strict liability, product liability, under statuTe or otherwise, even if it has been advised of the possibility of such damage. IN NO EVENT SHALL THE COMPANY'S TOTAL LIABILITY UNDER THIS AGREEMENT EXCEED THE FEES ACTUALLY PAID BY THE CLIENT FOR THE SERVICES PROVIDED HEREUNDER.  The Company is not responsible for any death, injury, loss or damage of or to persons or property by reason of non-performance of Company's obligations hereunder.    
<br><br>

h. Any copyrightable works, ideas, discoveries, inventions, patents, products, or other information (collectively, the "Work Product") developed by or in part by Company or the Tutor in connection with the Services shall be the exclusive property of Company. Upon request, Client shall sign all documents necessary to confirm or perfect the exclusive ownership of Company to the Work Product. Client may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer or sell any information, material, software, products or services without the prior written consent of Company.
<br><br>

<b>7.	Term.</b> The term of this Agreement shall commence upon Client's first use of Company's website and shall continue indefinitely until either party terminates this Agreement, for any reason or no reason, upon five (5) days' advance written notice to the other party. 
<br><br>

<b>8.	Indemnity.</b> Except in the case of Company's gross negligence or willful misconduct, Client agrees to indemnify, defend and hold Company, its officers, managers, principals, agents, employees and representatives, harmless from and against any and all claims, losses, damages, liabilities, costs and other expenses of any kind whatsoever (including but not limited to any attorneys' fees and collection or court costs) arising from or in connection with the rendering of the Services under this Agreement.  Client agrees to promptly reimburse Company for any legal or other expenses incurred in connection with the investigation or defending of any such loss, claim, damage or liability (or action in respect thereof).  
<br><br>

<b>9.	Mediation.</b> For all disputes that do not involve collection of past due billing, the parties hereto agree to participate in mediation before filing suit regarding any disagreement involving this Agreement.  Either party may initiate mediation by sending a written demand for mediation to the other party. If the other party does not respond to the demand within fourteen (14) days or fails to participate in any scheduled mediation agreed to by the parties, the party sending the demand may seek an order compelling mediation. In that event, the party that did not respond to the demand or participate in the scheduled mediation shall pay the actual attorney's fees and costs incurred by the party seeking an order to compel mediation.  The mediation will be held in Boulder, Colorado. The parties shall attempt to agree on the identity of the mediator, but if they cannot agree within fourteen (14) days after any party gives notice of its intent to proceed to mediation, then each party shall select a mediator.  The two selected mediators shall select a third mediator to preside over the mediation.  The mediation may not last longer than one full business day (approximately eight (8) hours) unless all the parties consent thereto. The parties will each pay one-half of the costs of mediation.  Each party shall bear its own attorneys' fees in connection with the mediation.  If the parties cannot resolve the dispute through mediation, any party may submit the dispute for resolution by litigation and each party consents to personal jurisdiction of the Boulder County Court.
<br><br>

<b>10.	Miscellaneous.</b>  This Agreement constitutes the entire agreement, and supersedes any and all prior agreements, between the parties hereto. No amendment, modification or waiver of any of provision of this Agreement will be valid unless set forth in a written instrument signed by both parties to be bound thereby. This Agreement will be interpreted, construed and enforced in all respects in accordance with the local laws of the State of Colorado, without reference to its choice of law rules.  In the event that either party consults or retains an attorney to enforce the terms of this Agreement, the prevailing party in any such dispute or litigation shall be entitled to recover from the other party its reasonable attorneys' fees and costs incurred.  If any of provision of this Agreement is held to be invalid or unenforceable, the remaining provisions shall nevertheless continue to be valid and enforceable to the extent permitted by law.  Both parties acknowledge that each has been given adequate opportunity to consult with an attorney concerning the subject matter hereof. The descriptive headings of the sections and subsections of this Agreement are intended for convenience only and do not constitute parts of this Agreement.


