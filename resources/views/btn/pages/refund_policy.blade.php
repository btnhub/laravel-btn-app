@extends('layouts.josh-front.default', ['page_title' => "Refund Policy"])

@section('title')
Refund Policy
@stop

@section('content')
<h2>Refund Policy</h2>

<h3>Session Refund</h3>
<ul>
	<li><b>Dissatisfactory First Session:</b> If you are unhappy with your tutor's performance in the <b>first</b> session, you may request a refund of the entire session.  If you meet with the tutor again before requesting (or receiving) a refund of the first session, you will <b>not</b> be refunded for the first session. Your decision to meet with your tutor again will be interpreted as acknowledgement of your tutor's satisfactory performance in the first session.  No processing fee is applied to a refund of the first session.</li>

	<li><b>Terminating Sessions</b>: If you were initially satisfied with your tutor, but later you choose to stop working with your tutor due to your tutor's unsatisfactory performance, you may request a refund for the last session (subject to a processing fee).  If the last session was longer than 1 hour, we will refund <b><em>the last hour of the last session</em></b> with your tutor (subject to a processing fee).  </li>

</ul>

<p>Please note that a request for a refund of an unsatisfactory session must be made within 7 days of the session in dispute.  We may email or call you to discuss what happened during the session and why your tutor's performance was unsatisfactory.  It is your responsibility to end a session (or cancel future sessions) immediately if a tutor's performance is unsatisfactory.  You are more than welcome to work with a different tutor in our network as soon as you would like.</p>

@stop