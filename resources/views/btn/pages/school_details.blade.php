@extends('layouts.josh-front.default', ['page_title' => "Course Catalogs & Calendars"])

@section('title')
	Course Catalogs & Calendars
@stop

@section('content')
	<p>Please carefully review the start and end dates to make sure you will be around to assist students until the end of the academic term.</p>
	<p><b>A Couple Notes</b></p>
	<li>Universities use the semester OR term system.  Carefully review the calendars for each school near you before you take on a student form that school.</li>
	<li>High Schools do not have the same academic term start and end date as universities.  Carefully review their calendars before you take on a High School student. </li>
	<li>At CU-Boulder, Math courses are offered by the MATH and APPM (applied math) departments.</li>
	<table class="table">
		<thead>
			<tr>
				<th>School</th>
				<th>Town</th>
				<th>Acadmic Calendar</th>
				<th>Course Catalog</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($schools as $school)
				<tr>
					<td>{{$school->school_name}}</td>
					<td>{{$school->town}}</td>
					<td>
						@if ($school->calendar)
							<a href="{{$school->calendar}}" target="_blank">{{$school->school_name}}'s Calendar</a>	
						@endif
					</td>
					<td>
						@if ($school->course_catalog)
							<a href="{{$school->course_catalog}}" target="_blank">{{$school->school_name}}'s Course Catalog</a>	
						@endif
						
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop