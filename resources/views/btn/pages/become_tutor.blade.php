@extends('layouts.josh-front.default', ['page_title' => "Become A Tutor"])

@section('title')
	Become A Tutor
@stop

@section('meta-description')
Love teaching?  Join BuffTutor today!  We're always hiring passionate tutors.
@stop

@section('content')
	<div class="row">
		<p>Established in 2009, the BuffTutor Network has rapidly grown into a fast and very effective tutor referral service.  Every semester, we receive hundreds of tutor requests from <strong>all fields of study </strong>(primarily Math & Science), so we are always looking for brilliant and exceptional tutors to add to our network!  </p>

		<p>We consist of full time and part-time tutors, so we are a great organization for experienced tutors who are still students or who have graduated and are in between jobs/careers. <b>Our tutors have the flexibility to set their own {{-- rate and --}} schedule</b>, taking on only as many students as they want or can handle.  Please read the following document for more information about how the program is structured and for eligibility criteria.</p>

		<h3>How Effective We Are</h3>
		<p>By hand-picking the tutors who join our network, thousands of students and parents have turned to the BuffTutor Network as a trusted source of experienced and knowledgeable tutors.  In the past few years, the number of students we work with has steadily increased and most of our tutors were fully booked by the second set of midterms. </p>
		
		{{-- <h4>Number Of Requests</h4>

		<div class="progress">
		  	<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 27%">
		    Fall 2013 (600)
		  	</div>

		  	<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 21%">
		    Spring 2014
	  		</div>
		  	
		  	<div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
		    Fall 2014
		  	</div>
		  	<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 22%">
		    Spring 2015
		  	</div>
		</div>
		
		
		<h4>Breakdown of Tutor Requests By Subject</h4>

		<div class="progress">
		  	<div class="progress-bar progress-bar" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
		    	Math (30%)
		  	</div>

		  	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 30%">
		    	Chemistry / Physics / Bio
	  		</div>
		  	
		  	<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 19%">
		    	Econ / Business /Stats
		  	</div>
		  	<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 21%">
		    	Other 
		  	</div>
		</div>
		<div align="right"><small>(Data From Boulder Location)</small></div> --}}

		<h3>Structure & Eligibility</h3>
		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#home">How It Works</a></li>
		  <li><a data-toggle="tab" href="#menu1">Eligibility Criteria</a></li>
		</ul>

		<div class="tab-content">
		  	<div id="home" class="tab-pane fade in active">
			    <strong>We Find The Student For you</strong><br>

				<p>We take care of all the marketing required to find the students.  {{-- Our tutors simply wait for an e-mail from us with the student’s contact information.  --}} We'll pass tutor requests along to you and, based on the request details (schedule, location, rate/earnings) you can accept or pass on the student. Once you've received a student, you can meet with him/her whenever and wherever works for both parties. <b>Our tutors get to set their own schedule.</b>  </p>

				<strong>Use Our Online Tools To Keep Track of Session Payments and Charges</strong>

				<p>We provide online tools to record and keep track of all sessions, payments and refunds. </p>

				<strong>Easy Payment System</strong>
				<p>We require students to add a credit/debit card to their account. This protects our tutors by eliminating the risk of students refusing or forgetting to pay for sessions.  When a tutor uses our online tools to charge a session, the payment is transferred to the tutor's bank account within a few days.</p>

				{{-- <strong>Fair Service Fee<br /></strong>
				<p>No monthly fees!  To pay for our services, we deduct a <b>30% service fee</b> when a tutor charges a session. <b style="color:darkred"><em>We value our tutors and keep our service fee well under industry standard (which can be as high as 60-70%)</em></b>. Since our tutors set their own rate and keep most of their earnings, they tend to earn more by working with us than by working with most in-town and online tutoring services <b>while still keeping their rates relatively low and affordable for students</b>.  --}}   {{-- All tutors start at a <b>35% service fee</b>; after the first 100 hours, the service fee is determined by the tutor's rate.  This encourages tutors to maintain a low rate while still earning their desired amount. --}}{{-- </p> --}}

				{{-- <p> </p>

				<div class="row">
					<div class="col-md-3 col-md-offset-1">
						<table class="table">
							<thead>
								<tr>
									<th>Tutor Rate</th>
									<th>Service Fee <br>
									<small>(after first 100 hours)</small></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td> <= $25/hr</td>
									<td>20%</td>
								</tr>
								<tr>
									<td> $26-40/hr</td>
									<td>25%</td>
								</tr>
								<tr>
									<td>$41+/hr</td>
									<td>30%</td>
								</tr>
							</tbody>
						</table>
					</div>	
				</div>
				
				
				
				<small>***It is the tutor's responsibility to contact us when he/she has reached the cutoff for a lower rate.  We will then review the tutor's evaluations and overall performance and determine whether or not to lower the rate.  <b>Only hours from students who have positively reviewed the tutor will count towards the total.</b></small> --}}

				{{-- <strong>Note To Private Music Teachers & Music Tutors<br /></strong>
				We do <strong>not</strong> provide studios for lessons.  --}}
		  	</div>
		  	<div id="menu1" class="tab-pane fade">
			  	
				<p>The BuffTutor Network consists primarily of undergraduate & graduate students, however any tutor holding at least a Bachelors degree (from any accredited University/College) is also allowed to apply.  As a tutor, you must:</p>
				
				<ol>	
					<li> be a U.S.A. citizen or be legally allowed to work in the U.S.A.  <strong>Unfortunately, J-1 or F-1 international students or any non-immigrants who do not have work permission cannot be a part of the BuffTutor Network.</strong></li>

					<li> have a <strong>3.0 or better major GPA</strong> (out of 4.0) in your field of study, not just a 3.0 cumulative GPA</li>

					<li> have earned a <strong>B+ or better</strong> in the course you wish to tutor.  A transcript must be provided.</li>

					<li> have at least <strong>one year (or 2 semesters) experience</strong> as a tutor, teacher, teaching/learning assistant or instructor.  This is non-negotiable!  Simply assisting friends with a class or homework or working as a grader does NOT count as teaching experience.</li>

					<li> have <strong>excellent communication skills</strong>, the English Language cannot be a barrier even if you are a Foreign Language tutor <em>(</em><em>Foreign Language tutors must be <strong>fluent</strong> in the Foreign Language they wish to tutor)</em></li>
					<li> be able to work with assigned students through the end of the semester and <strong>be around until the end of finals</strong>.  If you have plans to travel during the semester, you cannot be away for more than 7 days in the semester (this does not include school holidays).</li>

					<p><strong>Additional requirements for undergraduate students (including transfer students), you must:</strong></p>

					<li> be at least a <strong>junior</strong> in your field of study (not just by credit hours)</li>
					
					<li> have completed at least <strong>one year at your current college/university</strong></li>
					<br>
					<strong>Please Note:</strong>
					<p>- You cannot tutor a course that you are currently employed by the university as a teaching or learning assistant</p>
					<p>- To tutor a course that is part of a series, you must have completed all courses in the series with a B or better (and at least a B+ in the course you want to tutor).  In other words, to tutor Calc 1, you must have completed Calc 1-3, achieved at least B+ in Calc 1 and at least a B in Calc 2 and Calc 3.</p>
					<p>- You can only tutor courses you are <strong>proficient</strong> in, that are <strong>directly</strong> related to your field of study, or you have <strong>experience</strong> tutoring/teaching; in other words, just taking a writing course does not make you capable of being a writing tutor.</p>
				</ol>
			</div>
		</div>	
	</div>
	<div class="row">
		<center><span style="color:red"><b>Carefully review the Eligibility Criteria tab before proceeding</b></span></center><br>
		<center><a href="{{route('tutor.apply')}}"><button class="btn btn-primary">Tutor Application</button></a></center><br>
	</div>
@stop