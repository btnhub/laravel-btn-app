@extends('layouts.josh-front.default', ['page_title' => "Request Confirmation"])

@section('title')
Tutor Request Confirmation
@stop

@section('content')
	<div class="container">
	    <div class="row">
	        <div class="col-md-10 col-sm-10">
	        	<h2>Confirmation Page</h2>
	        	<p style="font-size: 1.25em">Whether or not we have a tutor for you, we will respond within 1-2 days.  If you do not hear from us, send us an email.</p>
	        	<br>
	        	<h4><b>Tutor Request Details</b></h4>
				<p style="font-size:1.15em">
					<b>Location:</b> {{$tutor_request->location}} {{$tutor_request->include_online_tutors ? "(Include Online Tutors)": ""}}<br>
					<b>Course:</b> {{$tutor_request->course}} ({{$tutor_request->level}})<br>
					<b>Time of Day:</b> {{$tutor_request->session_time}}<br>
					<b>Max affordable rate:</b> ${{$tutor_request->max_rate}}<br>

					@if ($tutor_request->requested_tutors)
						<b>Requested Tutors:</b> {{$tutor_request->requested_tutors}}
						<br>
					@endif
					@if ($tutor_request->concerns)
						<b>Concerns:</b> <br>
						{{$tutor_request->concerns}}
						<br>
					@endif
				</p>

				<br>
	        	<p style="font-weight: bold;font-size:1.25em">Submit another <a href="{{route('request.get')}}">tutor request</a> for a different course.</p>


		        
	        </div>
	    </div>
	   
	    <div class="row">
	        <div class="col-md-10 col-sm-10">
	        	<h2>Never Miss A Message</h2>
				<p style="font-size: 1.25em">We just e-mailed you a confirmation (sent to {{$tutor_request->student->email}}).  If you do not receive it within the next few minutes, check your Spam/Junk box: </p>

				<h3>Gmail</h3>
				<p style="font-size: 1.25em">
				1) At the top of the screen, in the search field, type “in:spam” to view the e-mails in the Spam box<br>
				2) Select our confirmation e-mail and click the “Not Spam” button at the top<br>
				</p>
				<img src="{{asset('btn/images/not_spam-gmail.png')}}" height="120">
				<br><br><br>

				<h3>Yahoo</h3>
				<p style="font-size: 1.25em">
				1) On the left side of the screen, click “Spam” link.<br>
				2) Select our confirmation e-mail and click the “Not Spam” button at the top of the page. <br>
				</p>
				<img src="{{asset('btn/images/not_spam-yahoo.png')}}" height="60px">
				<br><br><br>

				<h3>Hotmail</h3>
				<p style="font-size: 1.25em">
				1) On the left side of the screen, click “Junk Mail” link. <br>
				2) Select our confirmation e-mail and click the “Not Junk” button at the top of the page.<br>
				</p>
				<img src="{{asset('btn/images/not_spam-hotmail.png')}}" height="40">
	        </div>
	    </div>
	</div>
@endsection