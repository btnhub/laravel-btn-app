@extends('layouts.josh-front.default', ["Cancellation Policy"])

@section('title')
Cancellation Policy
@stop

@section('content')

<h2>Cancellation Policy</h2>

<p>Please carefully review our cancellation policy to understand the consequences if a student or a tutor misses a session.  To avoid fees or penalties, please provide <b>24 hour notice</b> to cancel or reschedule a session.</p>

<hr>

<h3>If A Student Misses A Session</h3>

<h4>Cancelling A Session</h4>

<p>If a student cancels a scheduled session: 
	<ol>
		<li> at least 24 hours in advance, the student will not be charged </li>
		<li> less than 24 hours before the scheduled session or simply does not show up to a scheduled session, the student will be charged for the <b>total scheduled time</b>.  For example, if the student cancels a 2 hour session without 24 hour notification, the student will be charged for the 2 hours scheduled.</li>
	</ol>
</p>
 
<h4>Late Arrival</h4>

<p>If a student is running late, he/she should contact his/her tutor (e.g. call or text) to inform the tutor that the student is on the way.  The tutor will wait 15 minutes for the student.  If in that time the tutor has not heard from the student, the session will be cancelled <b>and the student will be charged for the total time scheduled</b>. </p>

<h4>Rescheduling a session</h4>

<p>If a student needs to reschedule a session, the student should contact his/her tutor as soon as possible.  If the student contacta his/her tutor less than 24 hours before the scheduled session, the tutor may choose to charge the student for the last minute cancellation. </p>

<hr>

<h3>If A Tutor Misses A Sessions</h3>
<p>Tutors must provide 24 hour notice to cancel or reschedule a session.  If a tutor does not show up or cancels without 24hr notice, he/she <b>must</b> provide the student with a <b>free session </b> of equal duration as the missed session.  For example, if a tutor cancels a 2 hour session without 24 hour notification, the tutor must provide his/her student with a free 2 hour session or accumulate 2 hours for free before the tutor can resume charging for sessions with the student. </p>

@stop