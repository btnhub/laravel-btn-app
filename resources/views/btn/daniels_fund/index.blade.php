@extends('layouts.josh-front.default', ['page_title' => "Daniels Fund"])

@section('title')
	Daniels Fund
@stop

@section('content')
	<b>Scholar Relations Officer: </b> <br>
	Boulder: Josh Hause, jhause@danielsfund.org, (720) 941-4426 <br>
	Denver: Tracey Lovett, 	tlovett@danielsfund.org, (720) 941-4431<br>
	Payments: Kristen Batcho, payments@danielsfund.org, (303) 316-2103 (might be phone # for general use)
	<br><br>

	<b>Create New Assignment First !!! (If assignment does not already exist)</b>
	<br><br>
	
	To Do: Automate / Create Monthly Invoice
	<br><br>
	Click row to expand.
	<table class="table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Assign ID</th>
				<th>Location</th>
				<th>Student</th>
				<th>Tutor</th>
				<th>Course</th>
				<th>Rate</th>
				<th>Hours</th>
				<th>Total</th>
				<th>Charged</th>
				<th>Balance</th>
				<th>Unpaid</th>
				<th>Start Date</th>
				<th>End Date</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($scholars as $scholar)
				<tr data-toggle="collapse" data-target="#details_{{$scholar->id}}" class="clickable">
					<td>{{$scholar->id}}</td>
					<td>{{$scholar->assignment_id}}</td>
					<td>{{$scholar->student->locations->pluck('city')}}</td>
					<td><a href="{{ route('user.details.get', ['user_id' => $scholar->student->id])}}" target="_blank">{{$scholar->student->full_name}}</a></td>
					<td><a href="{{ route('user.details.get', ['user_id' => $scholar->assigned_tutor->id])}}" target="_blank">{{$scholar->assigned_tutor->full_name}}</a></td>
					<td>{{$scholar->course}}</td>
					<td>${{$scholar->rate}}</td>
					<td>{{$scholar->hours}}</td>
					<td>${{$scholar->total}}</td>
					<td>${{$scholar->session_charges->sum('amt_charged')}}</td>
					<td>${{$scholar->total - $scholar->session_charges->sum('amt_charged')}}</td>
					<td>
						@if ($scholar->session_charges()->where('payment_date', '=', NULL)->sum('amt_charged') > 0)
							<span style="color:red">
								${{$scholar->session_charges()->where('payment_date', '=', NULL)->sum('amt_charged')}}
							</span>							
						@else
							<center>-</center>
						@endif
					</td>
					<td>{{$scholar->start_date}}</td>
					<td>{{$scholar->end_date}}</td>
				</tr>
				<tr id="details_{{$scholar->id}}" class="collapse out">
					<td colspan="13">
					<table class="table" >
						<thead>
							<tr>
								<th>Invoice Month</th>
								<th>Session ID</th>
								<th>Session Date</th>
								<th>Duration (hrs)</th>
								<th>Charged</th>
								<th>Outcome</th>
								<th>Paid</th>
								<th>Notes</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($scholar->session_charges->sortBy('id') as $charge)
								<tr>	
									<td>
									
										{{$charge->created_at->format('F')}}
									</td>
									<td>{{$charge->tutor_session->id}}</td>
									<td>{{$charge->tutor_session->session_date->format('m/d/Y')}}</td>
									<td>{{$charge->tutor_session->duration}}</td>
									<td>${{$charge->amt_charged}}</td>
									<td>{{$charge->tutor_session->outcome}}</td>
									<td>
										@if ($charge->payment_date)
											{{$charge->payment_date}}
										@else
											{!! Form::open(['method' => 'PATCH', 'route' => ['update.payment.date', 'df_charge_id' => $charge->id], 'class' => 'form-horizontal']) !!}
												
									    		{{--
								    			<div class="form-group{{ $errors->has('payment_date') ? ' has-error' : '' }} col-md-9">
								    			    {!! Form::date('payment_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
								    			    <small class="text-danger">{{ $errors->first('payment_date') }}</small>
								    			    
								    			</div>
								    			--}}
									        	<div class="btn-group">
									        		{!! Form::submit("{$charge->created_at->format('F')} Paid", ['class' => 'btn btn-success']) !!}
									    		</div>
									
											{!! Form::close() !!}

										@endif
										</td>
									<td>{{$charge->notes}}</td>
								</tr>	
							@endforeach	
						</tbody>
					</table>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	<br><br>
	<hr>
	<h3>Add Scholar with approved hours</h3>
	@include('btn.daniels_fund.form_add_scholar')
	<br><br>

@stop