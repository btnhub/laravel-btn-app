	{!! Form::open(['method' => 'POST', 'route' => 'add.daniels.scholar', 'class' => 'form-horizontal']) !!}
			<div class="row">
			    <div class="form-group{{ $errors->has('student') ? ' has-error' : '' }} col-md-3">
			        {!! Form::label('student', 'Select Student') !!}
			        {!! Form::select('student', $students, null, ['id' => 'student', 'class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('student') }}</small>
			    </div>

			    <div class="form-group{{ $errors->has('tutor') ? ' has-error' : '' }} col-md-3">
			        {!! Form::label('tutor', 'Select A Tutor') !!}
			        {!! Form::select('tutor', $tutors, null, ['id' => 'tutor', 'class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('tutor') }}</small>
			    </div>

			    <div class="form-group{{ $errors->has('assignment_id') ? ' has-error' : '' }} col-md-3">
			        {!! Form::label('assignment_id', 'Assignment ID') !!}
			        {!! Form::number('assignment_id', null, ['class' => 'form-control']) !!}
			        <small class="text-danger">{{ $errors->first('assignment_id') }}</small>
			    </div>
		    </div>

		    <div class="row">
			    <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }} col-md-6">
			        {!! Form::label('course', 'Course') !!}
			        {!! Form::text('course', null, ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('course') }}</small>
			    </div>
		    </div>
		    <div class="row">
			    <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }} col-md-3">
			        {!! Form::label('start_date', 'Start Date') !!}
			        {!! Form::date('start_date', Carbon\Carbon::now(), ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('start_date') }}</small>
			    </div>

			    <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }} col-md-3">
			        {!! Form::label('end_date', 'End Date') !!}
			        {!! Form::date('end_date', null, ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('end_date') }}</small>
			    </div>
		    </div>
		    <div class="row">
			    <div class="form-group{{ $errors->has('rate') ? ' has-error' : '' }} col-md-3">
			        {!! Form::label('rate', 'Approved Rate') !!}
			        {!! Form::number('rate', null, ['class' => 'form-control', 'required' => 'required']) !!}
			        <small class="text-danger">{{ $errors->first('rate') }}</small>
			    </div>

				<div class="form-group{{ $errors->has('hours') ? ' has-error' : '' }} col-md-3">
				    {!! Form::label('hours', 'Approved Hours') !!}
				    {!! Form::number('hours', null, ['class' => 'form-control', 'required' => 'required']) !!}
				    <small class="text-danger">{{ $errors->first('hours') }}</small>
				</div>
			</div>
			<div class="row">
				<div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }} col-md-6">
				    {!! Form::label('notes', 'Additional Notes') !!}
				    {!! Form::textarea('notes', null, ['class' => 'form-control', 'size' => '30x2']) !!}
				    <small class="text-danger">{{ $errors->first('notes') }}</small>
				</div>
			</div>

			
		    <div class="btn-group pull-left">
		        {!! Form::submit("Create Assignment", ['class' => 'btn btn-primary']) !!}
		    </div>
		
	{!! Form::close() !!}
