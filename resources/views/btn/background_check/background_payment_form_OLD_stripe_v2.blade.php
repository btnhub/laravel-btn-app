@extends('layouts.josh-front.default', ['page_title' => "Background Check"])

@section('title')
  Background Check
@stop

@section('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
  <script src="{{ URL::asset('btn/js/stripe_billing.js') }}"></script>
  <script type="text/javascript">
    Stripe.setPublishableKey('{{ config('services.stripe.key')}}');
  </script>
@stop

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Background Check Payment: $30</div>
          <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => 'background.process.payment', 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}

              <div class="form-group">
                {!! Form::label(null, 'Card Number', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6 col-md-6">
                    {!! Form::number(null, null, ['class' => 'form-control', 'data-stripe' =>'number']) !!}
                </div>
              </div>
              <span class="payment-errors alert-danger"></span>

              <div class="form-row">
                <label class="col-md-3 control-label">Expiration Date</label>
                  {{ Form::selectMonth(null, null, ["data-stripe" => "exp-month"])}}
                <span> / </span>
                {{ Form::selectYear(null, date('Y'), date('Y') + 10, null, ["data-stripe" => "exp-year"])}}
              </div>

              <div class="form-group">
                <div class="col-md-9">
                    {!! Form::label(null, 'CVC', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
                  <div class="col-sm-3 col-md-3">
                      {!! Form::text(null, null, ["data-stripe" => "cvc" , 'class' => 'form-control']) !!}
                      <small class="text-danger">{{ $errors->first('amount') }}</small>
                  </div>
                </div>
              </div>

              <div class="btn-group pull-left col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">      
                  {!! Form::submit("Submit Payment", ['class' => 'btn btn-primary']) !!}
              </div>
            {!! Form::close() !!}
          </div> 
        </div> 
      </div> 
    </div>
  </div>
@stop