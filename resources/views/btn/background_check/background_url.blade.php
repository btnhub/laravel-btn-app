@extends('layouts.josh-front.default', ['page_title' => "Background Check"])

@section('content') 
	<br><br>
	
	<a href="https://checkr.com/apply/bufftutor/751e61081157">Click here</a> to submit your background check.  We will notify you if there is a problem with your report.  
@stop