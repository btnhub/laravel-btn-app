@extends('layouts.josh-front.default', ['page_title' => "Background Check"])

@section('title')
  Background Check
@stop

@section('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
  
  <script src="{{ URL::asset('btn/js/stripe_billing_v3.js') }}"></script>
  <script type="text/javascript">
    Stripe.setPublishableKey('{{ config('services.stripe.key')}}');
  </script>
@stop

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading"><b>Background Check Payment: ${{DB::table('settings')->where('field', 'background_check_fee')->first()->value}}</b></div>
          <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => 'background.process.payment', 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}

              <div class="row">
                <div class="col-sm-8 col-md-8">
                  @include('btn.stripe.card_form_partial')
                </div>  
              </div>
              
              <br>

              <div class="row">
                  <div class="col-sm-4 col-md-4">      
                    {!! Form::submit("Submit Payment", ['class' => 'btn btn-primary']) !!}
                  </div>
              </div>
            {!! Form::close() !!}

          </div> 
        </div> 
      </div> 
    </div>
  </div>
@stop