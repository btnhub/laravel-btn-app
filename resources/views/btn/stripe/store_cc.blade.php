<div class="container">
  <div class="row">
    <div class="col-md-9 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Add A Card</div>
        <div class="panel-body">
          <p>Please add a card to your account.  You will be charged for sessions <b>after</b> they occur.</p>
          {!! Form::open(['method' => 'POST', 'route' => 'store.customer.card', 'id' => 'payment-form','class' => 'form-horizontal']) !!}

            <div class="col-sm-6 col-md-6 col-sm-offset-2 col-md-offset-2">
              @include('btn.stripe.card_form_partial')
            </div>

            <div class="col-sm-4 col-md-4">      
                <br><br>
                {!! Form::submit("Add Card", ['class' => 'btn btn-primary']) !!}
            </div>
            
          {!! Form::close() !!}
        </div>  
      </div>
    </div>
  </div>
</div> 
