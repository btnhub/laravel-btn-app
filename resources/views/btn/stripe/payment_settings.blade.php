@extends('layouts.josh-front.default', ['page_title' => "Payment Settings"])

@section('title')
  Payment Settings
@stop

@section('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v3/"></script>
@stop

@section('content')
	@if (Auth::user()->customer)
		@include('btn.stripe.update_cc')
	@else
		@include('btn.stripe.store_cc')
	@endif
@stop