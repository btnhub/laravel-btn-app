<div class="container">
  <div class="row">
    <div class="col-md-9 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Add A Card</div>
        <div class="panel-body">
          <p>We no longer require upfront payments.  Please add a card to your account and your tutor will charge your account AFTER you meet with him/her.</p>
          {!! Form::open(['method' => 'POST', 'route' => 'store.customer.card', 'id' => 'payment-form','class' => 'form-horizontal']) !!}

              @include('btn.stripe.card_form_partial')

            <div class="btn-group pull-left col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">      
                {!! Form::submit("Add Card", ['class' => 'btn btn-primary']) !!}
            </div>
          {!! Form::close() !!}
        </div>  
      </div>
    </div>
  </div>
</div> 
