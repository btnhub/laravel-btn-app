@extends('layouts.josh-front.default', ['page_title' => "Payment Settings"])

@section('title')
  Payment Settings
@stop

@section('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
  <script src="{{ URL::asset('btn/js/stripe_billing.js') }}"></script>
  <script type="text/javascript">
    Stripe.setPublishableKey('{{ config('services.stripe.key')}}');
  </script>
@stop

@section('content')
	@if (Auth::user()->customer)
		@include('btn.stripe.update_cc')
	@else
		@include('btn.stripe.store_cc')
	@endif
@stop