<div class="row">
  <div class="col-md-4 col-sm-4">
  <legend>Stored Card</legend>
  
  <table class="table">
    <tbody>
      <tr>
        <td><b>Card Type</b></td>
        <td>{{auth()->user()->customer->card_brand}}</td>
      </tr>
      <tr>
        <td><b>Card #</b></td>
        <td>xxxx {{str_pad(auth()->user()->customer->card_last_four, 4, 0, STR_PAD_LEFT)}}</td>
      </tr>
      <tr>
        <td><b>Expiration</b></td>
        <td>{{str_pad(auth()->user()->customer->exp_month, 2, 0, STR_PAD_LEFT)}} / {{auth()->user()->customer->exp_year}}</td>
      </tr>
    </tbody>
  </table>
  </div>

<div class="container">
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-default">
        <div class="panel-heading">Update Card</div>
        <div class="panel-body">

          {!! Form::open(['method' => 'POST', 'route' => 'update.customer.card', 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}

              @include('btn.stripe.card_form_partial')

            <div class="btn-group pull-left col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">      
              {!! Form::submit("Update Card", ['class' => 'btn btn-primary']) !!}
            </div>
          {!! Form::close() !!}
        </div>  
      </div>
    </div>
  </div>
</div> 
