<div class="form-group">
    {!! Form::label(null, 'Card Number', ['class' => 'col-sm-3 control-label']) !!}
  <div class="col-sm-6 col-md-6">
      {!! Form::number(null, null, ['class' => 'form-control', 'data-stripe' =>'number']) !!}
  </div>
</div>
<span class="payment-errors alert-danger"></span>

<div class="form-row">
  <label class="col-md-3 control-label">Expiration Date</label>
    {{ Form::selectMonth(null, null, ["data-stripe" => "exp-month"])}}
  <span> / </span>
  {{ Form::selectYear(null, date('Y'), date('Y') + 10, null, ["data-stripe" => "exp-year"])}}
</div>

<div class="form-group">
  <div class="col-md-9">
      {!! Form::label(null, 'CVC', ['class' => 'col-sm-4 col-md-4 control-label']) !!}
    <div class="col-sm-3 col-md-3">
        {!! Form::text(null, null, ["data-stripe" => "cvc" , 'class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('amount') }}</small>
    </div>
  </div>
</div>