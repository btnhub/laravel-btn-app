@extends('layouts.josh-front.default')

@section('title')
	Payment Completed
@stop

@section('content')
	<div class="container">
		<div class="alert alert-success">
			Payment Completed!
		</div>
		<div>
			Your payment details are as follows:<br>
			<table class="table striped">
				<tbody>
					<tr>
						<td><b>Amount</b></td>
						<td>${{$payment->amount}}</td>
					</tr>
					<tr>
						<td><b>Processing Fee</b> </td>
						<td>${{$payment->processing_fee ?? 0}}</td>
					</tr>
					<tr>
					<td><b>Total Amount</b></td>
					<td>${{$payment->total_amount}}</td>
					</tr>
					<tr>
						<td><b>Transaction ID</b></td>
						<td>${{$payment->transaction_id}}</td>
					</tr>					
				</tbody>
			</table>
		</div>
	</div>
@stop