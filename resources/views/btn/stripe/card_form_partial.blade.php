@push('scripts')
  {{--MUST COME AFTER FORM--}}
  <script type="text/javascript">
      {{-- Create a Stripe client <- MUST COME BEFORE stripe_billing_v3.js call--}}
    var stripe = Stripe('{{ config('services.stripe.key') }}');
  </script>
  <script src="{{ URL::asset('btn/js/stripe_billing_v3.js') }}"></script>
@endpush

  <style type="text/css">
    .StripeElement {
      background-color: white;
      padding: 8px 12px;
      border-radius: 4px;
      border: 1px solid transparent;
      box-shadow: 0 7px 10px 0 #e6ebf1;
      -webkit-transition: box-shadow 150ms ease;
      transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
      box-shadow: 0 7px 10px 0 #cfd7df;
    }

    .StripeElement--invalid {
      border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
      background-color: #fefde5 !important;
    }
    
  </style>
  <div class="form-row">
    <label for="card-element">
      Credit or Debit Card (include billing zipcode)
     </label>
    <div id="card-element">
      <!-- a Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors -->
    <div id="card-errors"></div>
  </div>