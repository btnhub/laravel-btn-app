@extends('layouts.josh-front.default', ['page_title' => "Make A Payment"])

@section('title')
  Make A Payment
@stop

@section('header')
  <script src="{{ URL::asset('btn/js/jquery-3.0.0.js') }}"></script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
  <script src="{{ URL::asset('btn/js/stripe_billing.js') }}"></script>
  <script type="text/javascript">
    Stripe.setPublishableKey('{{ config('services.stripe.key')}}');
  </script>
@stop

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Make A Payment</div>
          <div class="panel-body">
            {!! Form::open(['method' => 'POST', 'route' => 'charge.stripe.card', 'id' => 'payment-form', 'class' => 'form-horizontal']) !!}
                
              <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                  {!! Form::label('amount', 'Payment Amount($)', ['class' => 'col-sm-3 control-label']) !!}
                <div class="col-sm-6 col-md-6">
                    {!! Form::number('amount', null, ['class' => 'form-control']) !!}
                    <small class="text-danger">{{ $errors->first('amount') }}</small>
                </div>
              </div>

              @include('btn.stripe.card_form_partial')

              <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                      <div class="checkbox{{ $errors->has('save_customer') ? ' has-error' : '' }}">
                          <label for="save_customer">
                              {!! Form::checkbox('save_customer', '1', null, ['id' => 'save_customer']) !!} Save card information
                          </label>
                      </div>
                      <small class="text-danger">{{ $errors->first('save_customer') }}</small>
                  </div>
              </div>

              <div class="btn-group pull-left col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">      
                  {!! Form::submit("Make Payment", ['class' => 'btn btn-primary']) !!}
              </div>
            {!! Form::close() !!}
          </div> 
        </div> 
      </div> 
    </div>
  </div>
@stop