@extends('layouts.josh-front.default', ['page_title' => "Payment Failed"])

@section('title')
	Payment Failed
@stop

@section('content')
	<div class="container">
		<h2>Payment Failed</h2>
		<div class="alert alert-danger">
			{{$error['message']}}
		</div>

	</div>
@stop