@extends('layouts.josh-front.default', ['page_title' => "Academic Advisors"])

@section('title')
  Academic Advisors
@stop

@section('content')
	<h3>Add An Advisor</h3>
		@include('btn.advisors.add_advisor')
	<br><br>
	<hr>
	
	<h3>Email Advisors</h3>
	<p>Email advisors who haven't been contacted within 1 month.</p>
		@foreach ($locations as $id => $city)
			<?php $advisor_count = $advisors->where('location_id', $id)->count();?>
			@if ($advisor_count > 0)
				<a data-toggle="collapse" data-target="#email_button_{{$id}}">Email {{$advisor_count}} {{$city}} Advisors</a>
				<div class="row">
					<div id="email_button_{{$id}}" class="collapse">
						<br>
						{!! Form::open(['method' => 'POST', 'route' => ['email.advisors', 'location_id' => $id], 'class' => 'form-horizontal col-md-offset-1', 'id' => 'email_advisors'.$id, 'style' => 'float:left']) !!}	
						    
						    <div class="btn-group pull-right">
						        {!! Form::submit("Email $advisor_count $city Advisors", ['class' => 'btn btn-primary']) !!}
						    </div>					

						{!! Form::close() !!}	
						<br>
					</div>
				</div>
				<br>
			@endif
		@endforeach

	<hr>

	<h3>Advisor List</h3>
	<table class="table table-bordered" id="advisors-table">
		<thead>
			<tr>
				<th>ID</th>
				<th>Location</th>
				<th>Name</th>
				<th>School</th>
				<th>Department</th>
				<th>Position</th>
				<th>BTN Group</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Last Contacted</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
@stop

@push('scripts')
	<script>
		$(function() {
		    $('#advisors-table').DataTable({
		        processing: true,
		        serverSide: true,
		        ajax: '{!! route('advisors.data') !!}',
		        columns: [
		            { data: 'id', name: 'id' },
		            { data: 'location_id', name: 'location_id' },
		            { data: 'first_name', name: 'first_name' },
		            { data: 'school', name: 'school' },
		            { data: 'department', name: 'department' },
		            { data: 'position', name: 'position' },
		            { data: 'btn_group', name: 'btn_group' },
		            { data: 'email', name: 'email' },
		            { data: 'phone', name: 'phone' },
		            { data: 'last_contacted', name: 'last_contacted' },
		        ]
		    });
		});
	</script>
@endpush