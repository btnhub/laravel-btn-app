{!! Form::open(['method' => 'POST', 'route' => 'add.advisor', 'class' => 'form-horizontal', 'id' => 'add_advisor']) !!}
		<div class="form-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
		    {!! Form::label('location_id', 'Location') !!}
		    {!! Form::select('location_id',$locations, null, ['id' => 'location_id', 'class' => 'form-control', 'required' => 'required']) !!}
		    <small class="text-danger">{{ $errors->first('location_id') }}</small>
		</div>
		<div class="row">
		    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }} col-md-3">
		        {!! Form::label('first_name', 'First Name') !!}
		        {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('first_name') }}</small>
		    </div>

		    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }} col-md-3 col-md-offset-1">
		        {!! Form::label('last_name', 'Last Name') !!}
		        {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('last_name') }}</small>
		    </div>
		    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} col-md-3">
		        {!! Form::label('email', 'Email') !!}
		        {!! Form::text('email', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('email') }}</small>
		    </div>
		    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }} col-md-3">
		        {!! Form::label('phone', 'Phone') !!}
		        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('phone') }}</small>
		    </div>
	    </div>
	    <div class="row">
		    <div class="form-group{{ $errors->has('school') ? ' has-error' : '' }} col-md-3">
		        {!! Form::label('school', 'School') !!}
		        {!! Form::select('school',$schools, null, ['id' => 'school', 'class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('school') }}</small>
		    </div>
		    <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('department', 'Department') !!}
		        {!! Form::text('department', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('department') }}</small>
		    </div>
		    <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }} col-md-4">
		        {!! Form::label('position', 'Position') !!}
		        {!! Form::text('position', null, ['class' => 'form-control']) !!}
		        <small class="text-danger">{{ $errors->first('position') }}</small>
		    </div>
		</div>

		<div class="row">
			<div class="form-group{{ $errors->has('source') ? ' has-error' : '' }} col-md-5">
			    {!! Form::label('source', 'Source (Website)') !!}
			    {!! Form::text('source', null, ['class' => 'form-control']) !!}
			    <small class="text-danger">{{ $errors->first('source') }}</small>
			</div>
			<div class="radio{{ $errors->has('btn_group') ? ' has-error' : '' }}">
			    <label>
			        {!! Form::radio('btn_group',"Graduate Advisor",  null, ['id' => 'radio_id']) !!} Graduate Advisor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			    </label>
			    <label>
			        {!! Form::radio('btn_group',"Undergraduate Advisor",  null, ['id' => 'radio_id']) !!} Undergraduate Advisor
			    </label>
			    <small class="text-danger">{{ $errors->first('btn_group') }}</small>
			</div>
		</div>

	    <div class="btn-group pull-left">
	        {!! Form::submit("Add Advisor", ['class' => 'btn btn-primary']) !!}
	    </div>
	
	{!! Form::close() !!}